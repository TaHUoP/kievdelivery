<?php

namespace Api\App\Contracts\Services;

/**
 * Contacts Service
 * @package App\Services
 */
interface ContactsService
{
    /**
     * Отправляет контакты на почту абменистраторерору
     * @param array $data
     */
    public function sendContacts(array $data);
}