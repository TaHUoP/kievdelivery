<?php

namespace Api\App\Contracts\Files;

/**
 * Image Api
 * @package Api\App\Contracts\Files
 */
interface ImageApi
{
    /**
     * ImageApi constructor.
     * @param $file
     */
    public function __construct($file);

    /**
     * Resize
     *
     * @param $width
     * @param $height
     * @return $this
     */
    public function resize($width, $height);

    /**
     * Insert
     *
     * @param $source
     * @param string $position
     * @param int $x
     * @param int $y
     * @return $this
     */
    public function insert($source, $position = 'top-left', $x = 0, $y = 0);

    /**
     * Save
     *
     * @param $path
     * @param $quality
     * @return $this
     */
    public function save($path, $quality);
}