<?php

namespace Api\App\Contracts\Helpers;

/**
 * Statics Helper
 * @package Api\App\Contracts\Helpers
 */
interface StaticsHelper
{
    /**
     * Возвращает url адрес ресурса с версией
     * Пример: /path/to/theme/style.min.css?1461583570
     *
     * @param $file
     * @param bool $globally
     * @return string
     */
    public static function asset($file, $globally = false);

    /**
     * Возвращает url адрес изображения
     * Пример: /path/to/theme/logo.png
     *
     * @param $file
     * @return string
     */
    public static function img($file);

    /**
     * Возвращает url темы
     *
     * @return string
     */
    public static function getUrlToTheme();

    /**
     * Возвращает полный путь к теме
     *
     * @return string
     */
    public static function getPathToTheme();
}