<?php

namespace Api\App\Contracts\Helpers;

/**
 * Content Image Helper
 * @package Api\App\Contracts\Helpers
 */
interface ContentImageHelper
{
    /**
     * Возвращает preview изображение
     *
     * @param $image
     * @param bool $onlyUrl
     * @return mixed
     */
    public function getImagePreview($image, $onlyUrl = false);

    /**
     * Возвращает review изображение
     *
     * @param $image
     * @param bool $onlyUrl
     * @return mixed
     */
    public function getImageReview($image, $onlyUrl = false);

    /**
     * Возвращает url изображения
     *
     * @param null $imageUrl
     * @return string
     */
    public function getImageUrl($imageUrl = null);

    /**
     * Возвращает путь к изображению
     *
     * @param $image
     * @return string
     */
    public function getUrlToImages($image);
}