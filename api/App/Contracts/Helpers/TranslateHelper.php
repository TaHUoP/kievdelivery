<?php

namespace Api\App\Contracts\Helpers;

/**
 * Translate Helper
 * @package Api\App\Contracts\Helpers
 */
interface TranslateHelper
{
    /**
     * Возвращает подходящий перевод из массива
     *
     * @param $translations
     * @param array $key
     * @return mixed
     */
    public function get($translations, $key = []);

    /**
     * Возвращает данные текущего языка
     *
     * @param null $key
     * @return array
     */
    public function getCurrentLang($key = null);

    /**
     * Возвращает язык исходя из браузера клиента
     *
     * @return string
     */
    public function getLocaleByBrowser();

    /**
     * Получить данные языка на основе локали
     *
     * @param $locale
     * @return bool
     */
    public function getLangByLocale($locale);

    /**
     * Возвращает язык по умолчанию из списка поддерживаемых
     *
     * @param null $key
     * @return null
     */
    public function getDefaultLang($key = null);

    /**
     * Идентифицирует и возращает язык пользователя
     *
     * @return string
     */
    public function getIdentifyLocale();

    /**
     * Возвращает данные языка исходя из сохраненной локали
     *
     * @param $key
     * @return bool
     */
    public function getLangBySavedLocale($key);

    /**
     * Возвращает локализированый переданый или текущий путь
     *
     * @param $locale, $path
     * @return string
     */
    public function localizePath($locale, $path = null);

    /**
     * Проверяет наличие локали в списке поддерживаемых языков
     *
     * @param $locale
     * @return bool
     */
    public function hasLocale($locale);

    /**
     * Установить локаль
     *
     * @param $locale
     */
    public function setLocale($locale);

    /**
     * Инициализировать язык по ключу
     *
     * @param string $key
     */
    public function init($key);

    /**
     * Сохранить локаль (например в куки или в сессию).
     *
     * @param $locale
     * @param $key
     * @return mixed
     */
    public function savedLocale($locale, $key);

}