<?php

namespace Api\App\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Content Repository
 * @package Api\App\Contracts\Entities
 */
interface ContentImageRepository extends FindInterface
{
    /**
     * @param $id
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function findById($id, ConditionInterface $condition = null);

    /**
     * @param ConditionInterface $condition
     * @return mixed
     */
    public function findOne(ConditionInterface $condition);

    /**
     * @param ConditionInterface $condition
     * @return mixed
     */
    public function findAll(ConditionInterface $condition);
}