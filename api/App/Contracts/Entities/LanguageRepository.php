<?php

namespace Api\App\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Language Repository
 * @package Api\App\Contracts\Entities
 */
interface LanguageRepository extends FindInterface
{
    /**
     * Возвращает список всех языков
     *
     * @param bool $active
     * @return mixed
     */
    public function lists($active = true);

}