<?php

namespace Api\App\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Content Repository
 * @package Api\App\Contracts\Entities
 */
interface ContentRepository extends FindInterface
{
    /**
     * Возвращает список типов
     *
     * @return mixed
     */
    public function getTypesArray();

    /**
     * Возвращает тип странцы
     *
     * @return string
     */
    public function getTypePage();

    /**
     * Возвращает тип странцы
     *
     * @return string
     */
    public function getTypeUkraine();

    /**
     * Возвращает тип новости
     *
     * @return string
     */
    public function getTypeNews();

    /**
     * Возвращает тип информация
     *
     * @return mixed
     */
    public function getTypeInformation();
}