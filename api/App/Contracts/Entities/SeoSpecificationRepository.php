<?php

namespace Api\App\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Seo Specification Repository
 * @package Api\App\Contracts\Entities
 */
interface SeoSpecificationRepository extends FindInterface
{

}