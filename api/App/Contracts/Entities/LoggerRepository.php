<?php

namespace Api\App\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Logger Repository
 * @package Api\App\Contracts\Entities
 */
interface LoggerRepository extends FindInterface
{

}