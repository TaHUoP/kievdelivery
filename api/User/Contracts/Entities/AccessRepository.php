<?php

namespace Api\User\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Access Repository
 * @package Api\User\Contracts\Entities
 */
interface AccessRepository extends FindInterface
{

}