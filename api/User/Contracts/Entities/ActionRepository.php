<?php

namespace Api\User\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Action Repository
 * @package Api\User\Contracts\Entities
 */
interface ActionRepository extends FindInterface
{

}