<?php

namespace Api\User\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * User Repository
 * @package Api\User\Contracts\Entities
 */
interface UserRepository extends FindInterface
{

}