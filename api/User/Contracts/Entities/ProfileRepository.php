<?php

namespace Api\User\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Profile Repository
 * @package Api\User\Contracts\Entities
 */
interface ProfileRepository extends FindInterface
{

}