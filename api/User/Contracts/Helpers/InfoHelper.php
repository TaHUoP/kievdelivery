<?php

namespace Api\User\Contracts\Helpers;

/**
 * Info Helper
 * @package Api\User\Contracts\Helpers
 */
interface InfoHelper
{
    /**
     * На основе данных пользователя возвращает его
     * имя и фамилию
     *
     * @param $user
     * @return string
     */
    public function getNameByUser($user);
}