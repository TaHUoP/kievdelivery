<?php

namespace Api\User\Contracts\Helpers;

/**
 * Image Helper
 * @package Api\User\Contracts\Helpers
 */
interface AvatarHelper
{
    /**
     * На основе данных пользователя, возвращает
     * Url адрес аватара
     *
     * @param $user
     * @return string
     */
    public function getAvatarUrlByUser($user);

    /**
     * Проверяет и возвращает Url адрес аватара.
     * В случае, если аватар не найден, возвращает
     * Url адрес к аватара по умолчанию.
     *
     * @param null $imageUrl
     * @return string
     */
    public function getAvatarUrl($imageUrl = null);

    /**
     * Возвращает Url адрес к папке с аватарами
     *
     * @return string
     */
    public function getUrlToAvatars();
}