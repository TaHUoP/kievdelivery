<?php

namespace Api\Media\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Media Repository
 * @package Api\App\Contracts\Entities
 */
interface MediaRepository extends FindInterface
{
    /**
     * Возвращает список типов
     *
     * @return mixed
     */
    public function getTypesArray();

    /**
     * Возвращает тип новости
     *
     * @return string
     */
    public function getTypeImage();

    /**
     * Возвращает тип видео
     *
     * @return string
     */
    public function getTypeVideo();
}