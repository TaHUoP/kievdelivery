<?php

namespace Api\Merchant\Contracts\Helpers;

/**
 * Currency Helper
 * @package Api\Merchant\Contracts\Helpers
 */
interface CurrencyHelper
{
    /**
     * Возвращает символ валюты
     *
     * @param $currencyCode
     * @return string
     */
    public function getCurrencySymbol($currencyCode);

    /**
     * Возвращает строку в денежном формате с округлением до целого
     *
     * @param $amount
     * @param string $currencyCode
     * @return string
     */
    public function getMoneyFormatRounded($amount, $currencyCode = null);

    /**
     * Возвращает строку в денежном формате
     *
     * @param $amount
     * @param string $currencyCode
     * @param int $round
     * @return string
     */
    public function getMoneyFormat($amount, $currencyCode = null, $round = 2);
}