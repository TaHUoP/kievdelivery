<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Category Repository
 * @package Api\Shop\Contracts\Entities
 */
interface CategoryRepository extends FindInterface
{

}