<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Cart Repository
 * @package Api\Shop\Contracts
 */
interface CartRepository extends FindInterface
{
    
}