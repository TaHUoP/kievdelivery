<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Coupon Repository
 * @package Api\Shop\Contracts\Entities
 */
interface CouponRepository extends FindInterface
{

}