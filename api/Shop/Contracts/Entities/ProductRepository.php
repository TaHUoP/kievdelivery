<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Product Repository
 * @package Api\Shop\Contracts\Entities
 */
interface ProductRepository extends FindInterface
{
    /**
     * Поиск рекомендованных товаров
     *
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function findRecommended(ConditionInterface $condition);

    /**
     * Поиск новых товаров
     *
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function findNew(ConditionInterface $condition);

    /**
     * Поиск товаров по фильтру свойств
     *
     * @param $properties
     * @param array $filter
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function findByPropertiesFilter($properties, array $filter, ConditionInterface $condition = null);

    /**
     * Возвращает количество продуктов
     *
     * @param ConditionInterface|null $condition
     * @return integer
     */
    public function getCount(ConditionInterface $condition = null);

    /**
     * Фильтр по get параметрам
     *
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function filterSearch(ConditionInterface $condition = null);
}