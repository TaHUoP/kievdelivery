<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Property Repository
 * @package Api\Shop\Contracts\Entities
 */
interface PropertyRepository extends FindInterface
{
    /**
     * Поиск всех записей по фильтру
     *
     * @param array $ids
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function findAllByFilter(array $ids, ConditionInterface $condition = null);
}