<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Value Repository
 * @package Api\Shop\Contracts\Entities
 */
interface ValueRepository extends FindInterface
{

}