<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Discount Repository
 * @package Api\Shop\Contracts\Entities
 */
interface DiscountRepository extends FindInterface
{

}