<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Query\FindInterface;

/**
 * Product Files Repository
 * @package Api\Shop\Contracts\Entities
 */
interface ProductFilesRepository extends FindInterface
{
}