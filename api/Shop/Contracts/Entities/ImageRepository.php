<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Image Repository
 * @package Api\Shop\Contracts\Entities
 */
interface ImageRepository extends FindInterface
{
    /**
     * Возвращает последний порядковый номер изображения
     *
     * @param $productId
     * @return mixed
     */
    public function getLastOrder($productId);
}