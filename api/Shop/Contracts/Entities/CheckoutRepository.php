<?php

namespace Api\Shop\Contracts\Entities;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Checkout Repository
 * @package Api\Shop\Contracts\Entities
 */
interface CheckoutRepository extends FindInterface
{
    /**
     * Возвращает колличество Checkout
     *
     * @param ConditionInterface $condition
     * @return mixed
     */
    public function getCount(ConditionInterface $condition = null);

    /**
     * Возвращает список типов
     *
     * @return mixed
     */
    public function getStatusArray();

    /**
     * Возвращает тип "Успешно"
     *
     * @return mixed
     */
    public function getStatusSuccess();

    /**
     * Возращает тип "Провал"
     *
     * @return mixed
     */
    public function getStatusFail();

    /**
     * Возращает тип "Ожидание"
     *
     * @return mixed
     */
    public function getStatusPending();

}