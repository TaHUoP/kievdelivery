<?php

namespace Api\Shop\Contracts\Helpers;

/**
 * Filter Helper
 * @package Api\Shop\Contracts\Helpers
 */
interface FilterHelper
{
    /**
     * Декодирует строку фильтра в массив.
     *
     * Пример $filterString: '/price_1000-5000/color_red-or-blue-or-white/weight_to-one-or-to-five'
     *
     * Ожидаемый результат:
     * Array
     * (
     *     [price] => Array
     *     (
     *         [0] => 1000-5000
     *     )
     *
     *     [color] => Array
     *     (
     *         [0] => red
     *         [1] => blue
     *         [2] => white
     *     )

     *     [weight] => Array
     *     (
     *         [0] => to-one
     *         [1] => to-five
     *     )
     * )
     *
     * @param $filterString
     * @return array
     */
    public function decodeFilter($filterString);

    /**
     * Преобразует массив фильтра в строку.
     *
     * Пример $filterArray:
     * Array
     * (
     *     [price] => Array
     *     (
     *         [0] => 1000-5000
     *     )
     *
     *     [color] => Array
     *     (
     *         [0] => red
     *         [1] => blue
     *         [2] => white
     *     )

     *     [weight] => Array
     *     (
     *         [0] => to-one
     *         [1] => to-five
     *     )
     * )
     *
     * Результат: '/price_1000-5000/color_red-or-blue-or-white/weight_to-one-or-to-five'
     *
     * @param array $filterArray
     * @return string
     */
    public function encodeFilter(array $filterArray);

    /**
     * Возвращает строку слияния текущего и указанного фильтра
     *
     * @param array $filter
     * @return string
     */
    public function mergeFilter(array $filter = []);

    /**
     * Возвращает url адрес со стракой фильтра
     *
     * @param $route
     * @param array $filter
     * @return string
     */
    public function routeFilter($route, $filter = []);

    /**
     * Возвращает false если не декодированый фильтр не проходит валидацию
     *
     * @param $raw_filter
     * @return bool
     */
    public function validateRawFilter($raw_filter);

    /**
     * Возвращает false если фильтр не проходит валидацию
     *
     * @param $filter
     * @return bool
     */
    public function validateFilter(array $filter);
}