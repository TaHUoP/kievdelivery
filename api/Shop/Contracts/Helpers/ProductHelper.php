<?php

namespace Api\Shop\Contracts\Helpers;
use Carbon\Carbon;

/**
 * Product Helper
 * @package Api\Shop\Contracts\Helpers
 */
interface ProductHelper
{
    /**
     * Возвращает стоимость товара в преобразованном формате
     *
     * @param $product
     * @param bool $format
     * @param Carbon $date
     * @return mixed
     */
    public function getPrice($product, $format = true, Carbon $date);

    /**
     * Возвращает инфомрацию о указанном свойстве товара
     *
     * @param $relations
     * @param $alias
     * @return mixed
     */
    public function getProperty($relations, $alias);

    /**
     * Возвращает значение атрибута из подробной его информации
     * В качестве параметка
     *
     * @param $attribute
     * @return mixed|null|string
     * @throws \ErrorException
     */
    public function getPropertyValue($attribute);

}