<?php


namespace App\Modules\I18nModule;

use App\Modules\I18nModule\Services\I18nService;

/**
 * Class Facade
 * @package App\Modules\I18nModule
 */
class Facade
{
    /**
     * @param string|null $url
     * @return string
     */
    public static function getRawUrl(string $url = null): string
    {
        /** @var I18nService $service */
        $service = app(I18nService::class);
        if (is_null($url)) {
            $url = \url()->current();
        }
        return $service->rawUrl($url);
    }

}