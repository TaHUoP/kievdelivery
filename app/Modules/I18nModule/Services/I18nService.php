<?php


namespace App\Modules\I18nModule\Services;

use Illuminate\Support\Facades\Request;
use Waavi\Translation\Models\Language;

/**
 * Class I18nService
 * @package App\Modules\I18nModule\Services
 */
class I18nService
{

    /**
     * @var array|null
     */
    private $langsAlias;

    /**
     * @return string
     */
    public function getRawPath(): string
    {
        return $this->rawPath(Request::path());
    }

    /**
     * @param string $path
     * @return string
     */
    public function rawPath(string $path): string
    {
        $path = trim($path, '/');
        $uriArr = explode('/', $path);
        if ($uriArr && in_array($uriArr[0], $this->getLangsAlias()) ) {
            array_shift($uriArr);
            $path = '';
            if ($uriArr) {
                $path = implode('/', $uriArr);
            }
        }
        return $path;
    }

    /**
     * @param string $url
     * @return string
     */
    public function rawUrl(string $url): string
    {
        $urlArr = parse_url($url);
        if (!$urlArr['host'] || !$urlArr['path']) {
            return $urlArr;
        }
        return $urlArr['scheme'] . "://" . $urlArr['host']
            ."/". $this->rawPath($urlArr['path']);
    }

    /**
     * @return array
     */
    public function getLangsAlias(): array
    {
        if (is_null($this->langsAlias)) {
            $this->langsAlias = Language::all()->map(function ($lang) {
                return $lang->locale;
            })->toArray();
        }
        return $this->langsAlias;
    }

}