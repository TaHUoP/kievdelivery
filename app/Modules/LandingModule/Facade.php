<?php


namespace App\Modules\LandingModule;

use App\Facades\Translate;
use App\Modules\I18nModule\Facade as FacadeI18n;
use App\Modules\LandingModule\Contracts\iLandingModel;
use App\Modules\LandingModule\Contracts\iLandingsService;
use App\Modules\LandingModule\Contracts\iLandingTranslate;
use App\Modules\LandingModule\Models\LandingModel;
use App\Modules\LandingModule\Services\LandingsService;

/**
 * Class Facade
 * @package App\Modules\LandingModule
 */
class Facade
{
    /**
     * @param string|null $url
     * @return bool
     */
    public static function isLanding(string $url = null): bool
    {
        return !!self::getLanding($url);
    }

    /**
     * @return bool
     */
    public static function isNoindex(): bool
    {
        $result = true;
        if (request()->has("page") === false) {
            if ($landing = self::getLanding()) {
                $result = $landing->getNoIndex() === LandingModel::ENUM_ON;
            }
        }
        return $result;
    }

    /**
     * @param string|null $url
     * @return iLandingModel|null
     */
    public static function getLanding(string $url = null): ?iLandingModel
    {
        /** @var LandingsService $service */
        $service = app(iLandingsService::class);
        if (is_null($url)) {
            $url = FacadeI18n::getRawUrl();
        }
        return $service->getLanding($url);
    }

    /**
     * @param string|null $url
     * @return iLandingTranslate|null
     */
    public static function getLandingTranslateCurrent(string $url = null): ?iLandingTranslate
    {
        if ($landing = self::getLanding($url)) {
            /** @var iLandingTranslate $translate */
            return Translate::get($landing->translations);
        }
        return null;
    }

    /**
     * @param string|null $url
     * @return bool
     */
    public static function isLandingPageDescription(string $url = null): bool
    {
        return self::isLanding($url) &&
            self::getLandingTranslateCurrent($url) &&
            self::getLandingTranslateCurrent($url)->getPageDescription();
    }
}