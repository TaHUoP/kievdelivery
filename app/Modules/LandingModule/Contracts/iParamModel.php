<?php


namespace App\Modules\LandingModule\Contracts;

use App\Modules\LandingModule\Models\ParamModel;

/**
 * Interface iParamModel
 * @package App\Modules\LandingModule\Contracts
 */
interface iParamModel
{
    /**
     * @return string
     */
    public function getParam(): string;

    /**
     * @param string $param
     * @return iParamModel
     */
    public function setParam(string $param): iParamModel;
}