<?php


namespace App\Modules\LandingModule\Contracts;

/**
 * Interface iLandingModel
 * @package App\Modules\LandingModule\Contracts
 */
interface iLandingModel
{

    /**
     * @return iLandingTranslate[]|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations();

    /**
     * @param int $id
     * @return iLandingModel
     */
    public function setId(int $id): iLandingModel;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getHash(): string;

    /**
     * @param string $hash
     * @return iLandingModel
     */
    public function setHash(string $hash): iLandingModel;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     * @return iLandingModel
     */
    public function setUrl(string $url): iLandingModel;

    /**
     * @return string
     */
    public function getNoIndex(): string;

    /**
     * @param string|null $param
     * @return iLandingModel
     */
    public function setNoIndex(string $param = null): iLandingModel;
}