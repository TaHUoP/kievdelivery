<?php


namespace App\Modules\LandingModule\Contracts;

use \Illuminate\Support\Collection;

/**
 * Interface iLandingTranslate
 * @package App\Modules\LandingModule\Contracts
 */
interface iLandingTranslate
{
    /**
     * @param int $id
     * @return iLandingTranslate
     */
    public function setId(int $id): iLandingTranslate;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     * @return iLandingTranslate
     */
    public function setLandingId(int $id): iLandingTranslate;

    /**
     * @return int
     */
    public function getLandingId(): int;

    /**
     * @param int $id
     * @return iLandingTranslate
     */
    public function setLangId(int $id): iLandingTranslate;

    /**
     * @return int
     */
    public function getLangId(): int;

    /**
     * @param iParamModel[]|Collection $collection
     * @return iLandingTranslate
     */
    public function setTitle(Collection $collection): iLandingTranslate;

    /**
     * @return iParamModel[]|Collection
     */
    public function getTitle(): Collection;

    /**
     * @param iParamModel[]|Collection $collection
     * @return iLandingTranslate
     */
    public function setDescription(Collection $collection): iLandingTranslate;

    /**
     * @return iParamModel[]|Collection
     */
    public function getDescription(): Collection;

    /**
     * @param string $param
     * @return iLandingTranslate
     */
    public function setKeywords(string $param): iLandingTranslate;

    /**
     * @return string
     */
    public function getKeywords(): string;

    /**
     * @param string $txt
     * @return iLandingTranslate
     */
    public function setPageDescription(string $txt): iLandingTranslate;

    /**
     * @return string
     */
    public function getPageDescription(): string;

    /**
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * @return string
     */
    public function getUpdatedAt(): string;
}
