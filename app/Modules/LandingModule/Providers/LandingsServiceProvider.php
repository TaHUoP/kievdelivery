<?php


namespace App\Modules\LandingModule\Providers;

use App\Modules\LandingModule\Contracts\iLandingsService;
use App\Modules\LandingModule\Services\LandingsService;
use Illuminate\Support\ServiceProvider;

/**
 * Class LandingsServiceProvider
 * @package App\Modules\LandingModule\Providers
 */
class LandingsServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(iLandingsService::class, function ($app) {
            return new LandingsService();
        });
    }
}