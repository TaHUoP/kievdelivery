<?php


namespace App\Modules\LandingModule\Services;

use App\Http\Requests\Shop\Backend\LandingRequest;
use App\Modules\I18nModule\Facade;
use App\Modules\LandingModule\Contracts\iLandingModel;
use App\Modules\LandingModule\Contracts\iLandingsService;
use App\Modules\LandingModule\Contracts\iLandingTranslate;
use App\Modules\LandingModule\Models\LandingModel;
use App\Modules\LandingModule\Models\LandingTranslateModel;
use App\Modules\LandingModule\Models\ParamModel;
use Illuminate\Database\Eloquent\Collection;
use Waavi\Translation\Models\Language;

/**
 * Class LandingsService
 * @package App\Modules\LandingModule\Services
 */
class LandingsService implements iLandingsService
{

    /**
     * @var iLandingModel[]|Collection
     */
    protected $landings;

    public function __construct()
    {
        $this->landings = new Collection();
    }

    /**
     * @param string $url
     * @return bool
     */
    public function isLanding(string $url): bool
    {
        return !!$this->getLanding($url);
    }

    /**
     * @param string $url
     * @return iLandingModel|null
     */
    public function getLanding(string $url): ?iLandingModel
    {
        $hash = $this->getHash($url);
        if (!$this->landings->has($hash)) {
             $this->landings->put($hash, $this->findOneByHash($hash));
        }
        return $this->landings->get($hash);
    }

    /**
     * @return iLandingModel[]|Collection
     */
    public function getAll(): Collection
    {
        return LandingModel::all();
    }

    public function findById(int $id)
    {
        return LandingModel::find($id);
    }

    /**
     * @param string $hash
     * @return mixed
     */
    public function findOneByHash(string $hash)
    {
        return LandingModel::where("hash", $hash)->get()->first();
    }

    /**
     * @param int $id
     * @param LandingRequest $request
     * @param iLandingModel $landing
     * @return iLandingModel
     */
    public function update(LandingRequest $request, iLandingModel $landing): iLandingModel
    {
        $this->addParamToLanding($request, $landing);
        $landing->save();

        if ($request->has('translations') && is_array($request->get('translations'))) {
            /** @var LandingTranslateModel[]|Collection $landingTranslations */
            $landingTranslations = $landing->translations;
            /** @var Language[] $langs */
            $langs = Language::all();
            foreach ($request->get('translations') as $keyLang => $translation) {
                /** @var LandingTranslateModel $landingTranslation */
                $landingTranslate = null;
                foreach ($langs as $lang) {
                    if ($lang->locale == $keyLang) {
                        foreach ($landingTranslations as $iTranslation) {
                            if ($iTranslation->getLangId() == $lang->id) {
                                $landingTranslate = $iTranslation;
                            }
                        }
                        break;
                    }
                }
                if ($landingTranslate) {
                    $this->addPartParamsToTranslate($translation, $landingTranslate);
                    $landingTranslate->save();
                }

            }
        }
        return $landing;
    }

    /**
     * @param LandingRequest $request
     * @return iLandingModel
     */
    public function create(LandingRequest $request): iLandingModel
    {
        $landing = new LandingModel();
        if ($oldLanding = LandingModel::where('hash', $this->getHash(trim($request->get('url'))))->get()->first()) {
            return $oldLanding;
        }
        $this->addParamToLanding($request, $landing);
        $landing->save();
        if ($landing->getId() && $request->has('translations') && is_array($request->get('translations'))) {
            /** @var Language[] $langs */
            $langs = Language::all();
            foreach ($request->get('translations') as $keyLang => $translation) {
                $landingTranslate = new LandingTranslateModel();
                foreach ($langs as $lang) {
                    if ($lang->locale == $keyLang) {
                        $landingTranslate->setLangId($lang->id);
                    }
                }
                $this->addPartParamsToTranslate($translation, $landingTranslate);
                $landing->translations()->save($landingTranslate);
            }
        }
        return $landing;
    }

    /**
     * @param iLandingModel $landingTranslate
     * @return bool
     */
    public function delete(iLandingModel $landingTranslate)
    {
        $ids = $landingTranslate->translations->map(function ($translate) {
            return $translate->id;
        });
        if ($ids) {
            LandingTranslateModel::destroy($ids->toArray());
        }
        $landingTranslate->delete();
    }

    /**
     * @param string $param
     * @return string
     */
    public function getHash(string $param): string
    {
        return md5($param);
    }

    /**
     * @param $translation
     * @param string $postKey
     * @param string $method
     * @param iLandingTranslate $landingTranslate
     */
    protected function addParamsToTranslate($translation, string $postKey, string $method, iLandingTranslate $landingTranslate): void
    {
        if ($translation[$postKey] && is_array($translation[$postKey])) {
            $params = new Collection();
            foreach ($translation[$postKey] as $part) {
                if (is_string($part)) {
                    $params->push(new ParamModel($part));
                }
            }
            $landingTranslate->{$method}($params);
        }
    }

    /**
     * @param LandingRequest $request
     * @param iLandingModel $landing
     */
    protected function addParamToLanding(LandingRequest $request, iLandingModel $landing): void
    {
        if ($request->has("url")) {
            $landing->setUrl(Facade::getRawUrl(trim($request->get('url'))));
            $landing->setHash($this->getHash($landing->getUrl()));
        }
        $landing->setNoIndex($request->has("noindex") == 1 ? LandingModel::ENUM_ON : LandingModel::ENUM_OFF);
    }

    /**
     * @param $translation
     * @param iLandingTranslate $landingTranslate
     */
    protected function addPartParamsToTranslate($translation, iLandingTranslate $landingTranslate): void
    {
        $this->addParamsToTranslate(
            $translation,
            'title_parts',
            'setTitle',
            $landingTranslate
        );
        $this->addParamsToTranslate(
            $translation,
            'description_parts',
            'setDescription',
            $landingTranslate
        );
        if ($translation['keywords_part']) {
            $landingTranslate->setKeywords($translation['keywords_part']);
        }
        if ($translation['description_page']) {
            $landingTranslate->setPageDescription($translation['description_page']);
        }
    }
}