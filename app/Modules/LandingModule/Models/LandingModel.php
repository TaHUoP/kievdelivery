<?php


namespace App\Modules\LandingModule\Models;

use App\Models\Model;
use App\Modules\LandingModule\Contracts\iLandingModel;

/**
 * Class LandingModel
 * @package App\Modules\LandingModule\Modeles
 */
class LandingModel extends Model implements iLandingModel
{
    const ENUM_ON = 'on';
    const ENUM_OFF = 'off';

    /**
     * @var string
     */
    protected $table = 'shop_seo_landings';

    /**
     * @return LandingTranslateModel[]|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(LandingTranslateModel::class, 'landing_id', 'id');
    }

    /**
     * @inheritDoc
     */
    public function setId(int $id): iLandingModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id ?? 0;
    }

    /**
     * @inheritDoc
     */
    public function getHash(): string
    {
        return $this->hash ?? '';
    }

    /**
     * @inheritDoc
     */
    public function setHash(string $hash): iLandingModel
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return $this->url ?? '';
    }

    /**
     * @inheritDoc
     */
    public function setUrl(string $url): iLandingModel
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getNoIndex(): string
    {
        return $this->no_index;
    }

    /**
     * @inheritDoc
     */
    public function setNoIndex(string $param = null): iLandingModel
    {
        $this->no_index = $param ?? self::ENUM_OFF;
        return $this;
    }
}