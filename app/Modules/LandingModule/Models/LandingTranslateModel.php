<?php


namespace App\Modules\LandingModule\Models;

use App\Models\Model;
use App\Modules\LandingModule\Contracts\iLandingTranslate;
use Illuminate\Support\Collection;
use Waavi\Translation\Models\Language;

/**
 * Class LandingTranslateModel
 * @package App\Modules\LandingModule\Models
 */
class LandingTranslateModel extends Model implements iLandingTranslate
{
    /**
     * @var string
     */
    protected $table = 'shop_seo_landings_translate';

    protected $fillable = [
        'id', 'landing_id', 'lang_id', 'title', 'keywords', 'app_name',
        'apple_title', 'h_1', 'description', 'created_at', 'updated_at', 'page_description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }

    /**
     * @inheritDoc
     */
    public function setId(int $id): iLandingTranslate
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id ?? 0;
    }

    /**
     * @inheritDoc
     */
    public function setLandingId(int $id): iLandingTranslate
    {
        $this->landing_id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLandingId(): int
    {
        return $this->landing_id ?? 0;
    }

    /**
     * @inheritDoc
     */
    public function setLangId(int $id): iLandingTranslate
    {
        $this->lang_id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLangId(): int
    {
        return $this->lang_id ?? 0;
    }

    /**
     * @inheritDoc
     */
    public function setTitle(Collection $collection): iLandingTranslate
    {
        $this->title = $collection->toJson();
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): Collection
    {
        return $this->getParams("title");
    }

    /**
     * @inheritDoc
     */
    public function setKeywords(string $param): iLandingTranslate
    {
        $this->keywords = $param;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getKeywords(): string
    {
        return $this->keywords ?? '';
    }

    /**
     * @inheritDoc
     */
    public function setDescription(Collection $collection): iLandingTranslate
    {
        $this->description = $collection->toJson();
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): Collection
    {
        return $this->getParams("description");
    }

    /**
     * @inheritDoc
     */
    public function setPageDescription(string $txt): iLandingTranslate
    {
        $this->page_description = $txt;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPageDescription(): string
    {
        return $this->page_description ?? '';
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }
    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    /**
     * @param string $prop
     * @return Collection
     */
    protected function getParams(string $prop): Collection
    {
        $results = new Collection();
        if ($this->{$prop}) {
            $arr = json_decode($this->{$prop});
            if (is_array($arr) && $arr) {
                foreach ($arr as $item) {
                    if (array_key_exists("param", $item)) {
                        $results->push(new ParamModel($item->param));
                    }
                }
            }
        }
        return $results;
    }
}