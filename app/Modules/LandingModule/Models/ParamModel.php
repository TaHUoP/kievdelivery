<?php


namespace App\Modules\LandingModule\Models;

/**
 * Class ParamModel
 * @package App\Modules\LandingModule\Models
 */
class ParamModel
{
    /**
     * @var string
     */
    public $param;

    public function __construct(string $param = '')
    {
        $this->param = $param;
    }

    /**
     * @return string
     */
    public function getParam(): string
    {
        return $this->param;
    }

    /**
     * @param string $param
     * @return ParamModel
     */
    public function setParam(string $param): self
    {
        $this->param = $param;
        return $this;
    }

}