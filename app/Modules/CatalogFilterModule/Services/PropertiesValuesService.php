<?php


namespace App\Modules\CatalogFilterModule\Services;

use App\Models\Shop\Belongs\CategoriesProperties;
use App\Models\Shop\Property;
use App\Models\Shop\Value;
use App\Modules\CatalogFilterModule\contracts\iPropertiesValuesService;
use App\Modules\CatalogFilterModule\Facade;
use App\Modules\CatalogFilterModule\Models\UrlsTree\FilterPropertyModel;
use App\Modules\CatalogFilterModule\Models\UrlsTree\FilterValueUrlModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Facades\Translate;

/**
 * Class FilterPropertiesValuesService
 * @package App\Modules\CatalogFilterModule\Services
 */
class PropertiesValuesService implements iPropertiesValuesService
{
    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var array|null
     */
    private $rawResults = null;

    /**
     * @var FilterPropertyModel[] | Collection
     */
    private $collection;

    /**
     * FilterPropertiesValuesService constructor.
     * @param int $categoryId
     */
    public function __construct(int $categoryId)
    {
        $this->categoryId = $categoryId;
        $this->collection = new Collection();
    }

    /**
     * @inheritDoc
     */
    public function getPropertiesValues(): Collection
    {
        if (is_null($this->rawResults)) {
            $this->getRawResults();
            if ($this->rawResults) {
                foreach ($this->rawResults as $rawResult) {
                    if (!$this->collection->has($rawResult->prop_id)) {
                        $this->collection->put($rawResult->prop_id, new FilterPropertyModel($rawResult->prop_id, $rawResult->prop_alias));
                    }
                    /** @var FilterPropertyModel $prop */
                    $prop = $this->collection->get($rawResult->prop_id);
                    $prop->getFilterValues()->push(new FilterValueUrlModel($rawResult->val_id, $rawResult->val_alias, null, $rawResult->val_sort));
                }
                $this->addNamesToModel();
                $this->sortValues();
            }
        }
        return $this->collection;
    }

    /**
     * @inheritDoc
     */
    public function addNamesToModel(): Collection
    {
        if ($this->collection->count() > 0) {
            $propIds = [];
            $valIds = [];
            foreach ($this->collection as $prop) {
                array_push($propIds, $prop->getId());
                if ($prop->getFilterValues()->count() > 0) {
                    foreach ($prop->getFilterValues() as $filterValue) {
                        array_push($valIds, $filterValue->getId());
                    }
                }
            }
            /** @var Property[] $props */
            $props = Property::whereIn("id", $propIds)->get();
            foreach ($this->collection as $propertyModel) {
                foreach ($props as $prop) {
                    if ($propertyModel->getId() == $prop->getId()) {
                         $propertyModel->setName(Translate::get($prop->translations, "name"));
                    }
                }
            }

            /** @var Value[] $values */
            $values = Value::whereIn("id", $valIds)->get();
            foreach ($this->collection as $propertyModel) {
                foreach ($propertyModel->getFilterValues() as $filterValue) {
                    foreach ($values as $value) {
                        if ($filterValue->getId() == $value->getId()) {
                            $filterValue->setName(Translate::get($value->translations, "name"));
                        }
                    }
                }
            }
        }
        return $this->collection;
    }

    /**
     * @return Collection
     */
    public function sortValues(): Collection
    {
        if ($this->collection->count() > 0) {
            foreach ($this->collection as $propertyModel) {
                if ($propertyModel->getFilterValues()->count() > 0) {
                    $propertyModel->setFilterValues(
                        $propertyModel->getFilterValues()->sortBy(function ($value, $key){
                            return (int)$value->getSort();
                        })
                    );
                }
            }
        }
        return $this->collection;
    }

    /**
     * @inheritDoc
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @inheritDoc
     */
    public function setCategoryId(int $categoryId): iPropertiesValuesService
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRawResults(): array
    {
        if (is_null($this->rawResults)) {
            /** @var CategoriesProperties|Collection $properties */
            $this->rawResults = [];
            $properties = CategoriesProperties::where("category_id", $this->getCategoryId())->get();
            if ($properties->count()) {
                $propertiesIds = $properties->map(function ($property) {
                    /**@var CategoriesProperties $property */
                    return $property->getPropertyId();
                });
                $andQuery = implode(' or ', array_fill(0, $propertiesIds->count(), 'sp.id=?'));
                $sql = "select sp.id as prop_id, sp.alias as prop_alias, sv.id as val_id, sv.alias as val_alias, sv.sort as val_sort";
                $sql .= " from shop_categories sc";
                $sql .= " left join shop_product_categories spc on sc.id = spc.category_id
                        left join shop_products shp on spc.product_id = shp.id
                        left join shop_products_properties_values sppv on shp.id = sppv.product_id
                        left join shop_properties sp on sppv.property_id = sp.id
                        left join shop_categories_properties scp on sp.id = scp.property_id
                        left join shop_values sv on sppv.value_id = sv.id";
                $sql .= " where ({$andQuery}) and sc.id=? and shp.visible=1";
                $propertiesIds->push($this->getCategoryId());
                if (Facade::isFilter() && Facade::getProductsIds()->count()) {
                    foreach (Facade::getProductsIds() as $productsId) {
                        $propertiesIds->push($productsId);
                    }
                    $andIn = implode(', ', array_fill(0, Facade::getProductsIds()->count(), '?'));
                    $sql .= " and shp.id in ({$andIn}) ";
                }
                $sql .= " GROUP BY sv.alias";
                $sql .= " ORDER BY scp.id ASC";
                try {
                    $this->rawResults = DB::select($sql, $propertiesIds->toArray());
                } catch (\Exception $ex) {
                    return abort(404);
                }
            }
        }
        return $this->rawResults;
    }

    /**
     * @inheritDoc
     */
    public function setRawResults(?array $rawResults): iPropertiesValuesService
    {
        $this->rawResults = $rawResults;
        return $this;
    }


}