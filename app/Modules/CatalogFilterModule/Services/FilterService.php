<?php


namespace App\Modules\CatalogFilterModule\Services;

use App\Models\City;
use App\Models\Shop\Translate\Category;
use App\Models\Shop\Value;
use App\Modules\CatalogFilterModule\contracts\iPropertiesValuesService;
use App\Modules\CatalogFilterModule\Models\FilterParamModel;
use Illuminate\Database\Eloquent\Collection as CollectionEloquent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class FilterService
 * @package App\Modules\CatalogFilterModule\Services
 */
class FilterService
{
    /**
     * @var int
     */
    protected $categoryId = null;

    /**
     * @var string|null
     */
    protected $filter = null;

    /**
     * @var Value[]|CollectionEloquent|null
     */
    protected $propsValuesAll = null;

    /**
     * @var FilterParamModel[]|Collection|null
     */
    protected $filterValues = null;

    /**
     * @var iPropertiesValuesService
     */
    private $propertiesValuesService;

    /**
     * @var Collection
     */
    private $productsIds = null;

    /**
     * @var Category
     */
    private $categoryTranslate;

    /**
     * @var bool
     */
    private $isFilter;

    /**
     * @var string
     */
    private $cityAliasParam;

    /**
     * @var City|null
     */
    private $city;

    /**
     * @param iPropertiesValuesService $propertiesValuesService
     * @param string $filter
     */
    public function init(iPropertiesValuesService $propertiesValuesService, string $filter)
    {
        $this->propertiesValuesService = $propertiesValuesService;
        $this->filter = $filter;
        $this->categoryId = $this->propertiesValuesService->getCategoryId();
    }

    /**
     * @return Collection
     */
    public function parseFilter(): Collection
    {
        if (is_null($this->filterValues)) {
            $this->filterValues = new Collection();
            $params = explode("/", $this->filter);
            if ($params) {
                foreach ($params as $alias) {
                    $this->filterValues->push(new FilterParamModel($alias));
                }
            }
        }
        return $this->filterValues;
    }

    /**
     * @return Collection
     */
    public function addIdToFilterParams(): Collection
    {
        if ($this->filterValues && $this->filterValues->isEmpty() === false) {
            $values = $this->getValuesAll();
            if ($values->isEmpty() === false) {
                foreach ($this->filterValues as $filterParam) {
                    foreach ($values as $value) {
                        if ($filterParam->getAlias() === $value->getAlias()) {
                            $filterParam->setId($value->getId());
                        }
                    }
                }
                $this->cityAliasParam = $this->filterValues->last()->getAlias();
                $this->filterValues = $this->filterValues->filter(function ($filterParam) {
                    /**@var FilterParamModel $filterParam */
                    if ($filterParam->getId()) {
                        return true;
                    }
                });
            }

        }
        return $this->filterValues;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        if (is_null($this->isFilter)) {
            $this->isFilter = false;
            if ($this->filterValues && $this->filterValues->count()) {
                foreach ($this->filterValues as $filterValue) {
                    if ($filterValue->getId()) {
                        $this->isFilter= true;
                        break;
                    }
                }
            }
        }
        return $this->isFilter;
    }

    /**
     * @return Collection
     */
    public function getProductsIds(): Collection
    {
        if (is_null($this->productsIds)) {
            $this->productsIds = new Collection();
            if ($this->filterValues && $this->filterValues->count()) {
                $valuesIds = $this->filterValues->map(function ($value) {
                    return $value->getId();
                });
                for ($i = 0; $i < $valuesIds->count(); $i++) {
                    if ($i === 0) {
                        $this->productsIds = $this->getProductsIdsCascade($valuesIds->get($i));
                        continue;
                    }
                    $this->productsIds = $this->getProductsIdsCascade($valuesIds->get($i), $this->productsIds->toArray());
                }
            }
        }
        return $this->productsIds;
    }

    /**
     * @return iPropertiesValuesService
     */
    public function addIsActiveToValues(): iPropertiesValuesService
    {
        if ($this->filterValues && $this->filterValues->count()) {
            $properties = $this->propertiesValuesService->getPropertiesValues();
            if ($properties->count()) {
                foreach ($properties as $property) {
                    if ($property->getFilterValues()->count()) {
                        foreach ($property->getFilterValues() as $filterValue) {
                            foreach ($this->filterValues as $value) {
                                if ($filterValue->getId() == $value->getId()) {
                                    $filterValue->setIsActive(true);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->propertiesValuesService;
    }

    /**
     * @param string|null $filter
     * @return string|null
     */
    public function cropCityFromFilter(string $filter = null): ?string
    {
        if (is_null($filter) || ! $filter) {
            return $filter;
        }
        $params = explode("/", $filter);
        if ($params && $params[0]) {
            $city = $this->getCityByAlias($params[0]);
            if ($city) {
                unset($params[0]);
            }
        }
        return implode('/', $params);
    }

    /**
     * @return string|null
     */
    public function cropQuery(): ?string
    {
        $allParams = explode("/", $this->getFilterString());
        $params = [];
        foreach ($this->getFilterValues() as $filterParam) {
            if ($filterParam->getId()) {
                array_push($params, $filterParam->getAlias());
            }
        }
        if ($this->isCity()) {
            array_push($params, $this->getCityByUrl()->getAlias());
        }
        $result = $this->getFilterString();
        if ($allParams && $params) {
            $diffArr = array_diff($allParams, $params);
            if ($diffArr && $params == $diffArr) {
                $result = null;
            } else {
                $result = implode("/", $diffArr);
            }
        }
        return $result;
    }

    /**
     * @return Value[] | CollectionEloquent
     */
    public function getValuesAll(): CollectionEloquent
    {
        if (is_null($this->propsValuesAll)) {
            $this->propsValuesAll = Value::all();
        }
        return $this->propsValuesAll;
    }

    /**
     * @return string|null
     */
    public function getFilterString(): ?string
    {
        return $this->filter;
    }

    /**
     * @return FilterParamModel[]|Collection|null
     */
    public function getFilterValues(): Collection
    {
        return $this->filterValues;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param Category $categoryTranslate
     * @return FilterService
     */
    public function setCategoryTranslate(Category $categoryTranslate): self
    {
        $this->categoryTranslate = $categoryTranslate;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategoryTranslate(): Category
    {
        return $this->categoryTranslate;
    }

    /**
     * @param int $valueId
     * @param array|null $productsIds
     * @return Collection
     */
    protected function getProductsIdsCascade(int $valueId, array $productsIds = null): Collection
    {
        $sql = "select shp.id as product_id";
        $sql .= " from shop_categories sc";
        $sql .= "   left join shop_product_categories spc on sc.id = spc.category_id
                            left join shop_products shp on spc.product_id = shp.id
                            left join shop_products_properties_values sppv on shp.id = sppv.product_id
                            left join shop_properties sp on sppv.property_id = sp.id
                            left join shop_values sv on sppv.value_id = sv.id";
        $sql .= " where sv.id=? and sc.id=? and shp.visible=1";
        $params = [$valueId, $this->propertiesValuesService->getCategoryId()];
        if (!is_null($productsIds)) {
            $inQuery = implode(', ', array_fill(0, count($productsIds), '?'));
            $sql .= " and shp.id in ({$inQuery})";
            $params = array_merge($params, $productsIds);
        }
        try {
            $results = DB::select($sql, $params);
        } catch (\Exception $ex) {
            return abort(404);
        }
        $resultsIds = new Collection();
        if ($results) {
            foreach ($results as $result) {
                $resultsIds->push($result->product_id);
            }
        }
        return $resultsIds;
    }

    /**
     * @param string|null $alias
     * @return bool
     */
    public function isCity(string $alias = null): bool
    {
        $result = false;
        $city = $this->getCityByUrl();
        if (is_null($alias)) {
            $result = $city->getAlias() !== "";
        } else{
            if ($this->getCityByAlias($alias)) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * @return City
     */
    public function getCityByUrl(): City
    {
        $this->city = new City();
        if ($this->cityAliasParam) {
            $city = $this->getCityByAlias($this->cityAliasParam);
            if ($city) {
                $this->city = $city;
            }
        }
        return $this->city;
    }

    /**
     * @return string
     */
    public function getFilterValuesStr(): string
    {
        $result = [];
        foreach ($this->getFilterValues() as $filterValue) {
            array_push($result, $filterValue->getAlias());
        }
        return $result ? implode("/", $result) : '';
    }

    /**
     * @param string $alias
     * @return City|null
     */
    protected function getCityByAlias(string $alias): ?City
    {
        return City::where("alias", $alias)->get()->first();
    }

    /**
     * @return City|null
     */
    public function getCity(): ?City
    {
        return $this->city;
    }


}