<?php


namespace App\Modules\CatalogFilterModule\Models\UrlsTree;

use App\Modules\CatalogFilterModule\Models\FilterParamModel;
use Illuminate\Support\Collection;

/**
 * Class FilterPropertyModel
 * @package App\Modules\CatalogFilterModule\Models\UrlsTree
 */
class FilterPropertyModel
{
    /**
     * @var FilterParamModel[]|Collection
     */
    private $filterValues;
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $alias;
    /**
     * @var string
     */
    private $name;

    public function __construct(int $id, string $alias)
    {
        $this->filterValues = new Collection();
        $this->id = $id;
        $this->alias = $alias;
    }

    /**
     * @return FilterValueUrlModel[]|Collection
     */
    public function getFilterValues(): Collection
    {
        return $this->filterValues;
    }

    /**
     * @param Collection $filterValues
     * @return FilterPropertyModel
     */
    public function setFilterValues(Collection $filterValues): self
    {
        $this->filterValues = $filterValues;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FilterPropertyModel
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return FilterPropertyModel
     */
    public function setAlias(string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FilterPropertyModel
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }



}