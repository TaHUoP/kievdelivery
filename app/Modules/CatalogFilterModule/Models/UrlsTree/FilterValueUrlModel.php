<?php


namespace App\Modules\CatalogFilterModule\Models\UrlsTree;

/**
 * Class FilterValueUrlModel
 * @package App\Modules\CatalogFilterModule\Models\UrlsTree
 */
class FilterValueUrlModel
{
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string|null
     */
    private $alias;
    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int
     */
    private $sort;

    private $isActive = false;

    /**
     * FilterValueUrlModel constructor.
     * @param int|null $id
     * @param string|null $alias
     * @param string|null $name
     * @param int|null $sort
     */
    public function __construct(int $id = null, string $alias = null, string $name = null, int $sort = null)
    {
        $this->id = $id;
        $this->alias = $alias;
        $this->name = $name;
        $this->sort = $sort ?? 0;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return FilterValueUrlModel
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     * @return FilterValueUrlModel
     */
    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return FilterValueUrlModel
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return FilterValueUrlModel
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return FilterValueUrlModel
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }


}