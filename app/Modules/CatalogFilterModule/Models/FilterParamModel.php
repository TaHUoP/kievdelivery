<?php


namespace App\Modules\CatalogFilterModule\Models;

/**
 * Class FilterParamModel
 * @package App\Modules\CatalogFilterModule\Models
 */
class FilterParamModel
{
    /**
     * @var string
     */
    private $alias;
    /**
     * @var int|null
     */
    private $id;

    public function __construct(string $alias, int $id = null)
    {
        $this->alias = $alias;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return FilterParamModel
     */
    public function setAlias(string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return FilterParamModel
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

}