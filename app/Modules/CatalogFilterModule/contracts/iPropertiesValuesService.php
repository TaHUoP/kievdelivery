<?php


namespace App\Modules\CatalogFilterModule\contracts;

use App\Modules\CatalogFilterModule\Models\UrlsTree\FilterPropertyModel;
use Illuminate\Support\Collection;

/**
 * Interface iPropertiesValuesService
 * @package App\Modules\CatalogFilterModule\contracts
 */
interface iPropertiesValuesService
{
    /**
     * @return FilterPropertyModel[]|Collection
     */
    public function getPropertiesValues(): Collection;


    /**
     * @return FilterPropertyModel[]|Collection
     */
    public function addNamesToModel(): Collection;

    /**
     * @return int
     */
    public function getCategoryId(): int;


    /**
     * @param int $categoryId
     * @return self
     */
    public function setCategoryId(int $categoryId): self;


    /**
     * @return \stdClass[]|null
     */
    public function getRawResults(): array;


    /**
     * @param \stdClass[]|null $rawResults
     * @return self
     */
    public function setRawResults(?array $rawResults): self;
}