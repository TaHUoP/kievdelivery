<?php


namespace App\Modules\CatalogFilterModule;

use App\Models\City;
use App\Models\Shop\Translate\Category;
use App\Modules\CatalogFilterModule\Models\UrlsTree\FilterPropertyModel;
use App\Modules\CatalogFilterModule\Services\PropertiesValuesService;
use App\Modules\CatalogFilterModule\Services\FilterService;
use Illuminate\Support\Collection;

/**
 * Class Facade
 * @package App\Modules\CatalogFilterModule
 */
class Facade
{

    /**
     * @param int $categoryId
     * @param string $filter
     */
    public static function init(int $categoryId, string $filter)
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        $service->init(new PropertiesValuesService($categoryId), $filter);
        $service->parseFilter();
        $service->addIdToFilterParams();
    }

    /**
     * @param Category $category
     */
    public static function setCategoryTranslate(Category $category): void
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        $service->setCategoryTranslate($category);
    }

    /**
     * @return Category
     */
    public static function getCategoryTranslate(): Category
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        return $service->getCategoryTranslate();
    }

    /**
     * @return bool
     */
    public static function isFilter(): bool
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        $result = false;
        if ($service->isFilter()) {
            $result = true;
        }
        return $result;
    }

    /**
     * @param string|null $alias
     * @return bool
     */
    public static function isCity(string $alias = null): bool
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        return $service->isCity($alias);
    }

    /**
     * @return City
     */
    public static function getCityByUrl(): City
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        return $service->getCityByUrl();
    }

    /**
     * @return Collection
     */
    public static function getFilterParams(): Collection
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        return $service->getFilterValues();
    }

    /**
     * @return Collection
     */
    public static function getProductsIds(): Collection
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        return $service->getProductsIds();
    }

    /**
     * @return string|null
     */
    public static function cropParamsFromFilter(): ?string
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        $filter = $service->cropQuery();
        return $service->cropCityFromFilter($filter);
    }

    /**
     * @return FilterPropertyModel[]|Collection
     */
    public static function buildFilterUrlsTree(): Collection
    {
        /** @var FilterService $service */
        $service = app(FilterService::class);
        return $service->addIsActiveToValues()->getPropertiesValues();
    }

    /**
     * @return bool
     */
    public static function isFilterUrlsTree(): bool
    {
        return !!self::buildFilterUrlsTree()->count();
    }

    public static function getCanonical($paramsStr)
    {
        if (! self::isFilter() || ! is_string($paramsStr)) {
            return $paramsStr;
        }
        /** @var FilterService $service */
        $service = app(FilterService::class);
        $valuesStr = $service->getFilterValuesStr();
        $paramsStr .= $valuesStr ? "/" . $valuesStr : '' ;
        if ($service->getCity()) {
            $paramsStr .= "/" . $service->getCity()->getAlias();
        }
        return $paramsStr;
    }
}