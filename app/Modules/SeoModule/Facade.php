<?php


namespace App\Modules\SeoModule;

use App\Facades\Translate;
use App\Modules\CatalogFilterModule\Facade as FacadeFilterCatalog;
use App\Modules\CatalogFilterModule\Models\UrlsTree\FilterValueUrlModel;
use App\Modules\LandingModule\Contracts\iLandingTranslate;
use App\Modules\LandingModule\Facade as FacadeLanding;
use App\Modules\SeoModule\Patterns\DescriptionPattern;
use App\Modules\SeoModule\Patterns\H1Pattern;
use App\Modules\SeoModule\Patterns\KeywordsPattern;
use App\Modules\SeoModule\Patterns\TitlePattern;
use App\Modules\SeoModule\Services\SeoBuilderParamsService;

/**
 * Class Facade
 * @package App\Modules\SeoModule
 */
class Facade
{

    /**
     * @return string
     */
    public static function getTitle(): string
    {
        /** @var SeoBuilderParamsService $service */
        $service = app(SeoBuilderParamsService::class);
        $translate = self::getTranslate();
        return $service->getPrepareText(new TitlePattern(), is_object($translate) ? $translate->getTitle(): '');
    }

    /**
     * @return string
     */
    public static function getDescription(): string
    {
        /** @var SeoBuilderParamsService $service */
        $service = app(SeoBuilderParamsService::class);
        $translate = self::getTranslate();
        return $service->getPrepareText(new DescriptionPattern(), is_object($translate) ? $translate->getDescription() : '');
    }

    /**
     * @return string
     */
    public static function getKeywords(): string
    {
        /** @var SeoBuilderParamsService $service */
        $service = app(SeoBuilderParamsService::class);
        $translate = self::getTranslate();
        return $service->getPrepareText(new KeywordsPattern(), is_object($translate) ? $translate->getKeywords() : '');
    }

    /**
     * @return string
     */
    public static function getCityAlias(): string
    {
        /** @var SeoBuilderParamsService $service */
        $service = app(SeoBuilderParamsService::class);
        return $service->getCityAlias();
    }

    /**
     * @param string $parentUrl
     * @return array
     */
    public static function getBreadcrumbs(string $parentUrl): array
    {
        $results = [];
        $props = \App\Modules\CatalogFilterModule\Facade::buildFilterUrlsTree();
        /** @var FilterValueUrlModel $lastValue */
        $lastValue = null;
        foreach ($props as $prop) {
            foreach ($prop->getFilterValues() as $filterValue) {
                if ($filterValue->isActive()) {
                    $lastValue = $filterValue;
                    $parentUrl .= "/" . $filterValue->getAlias();
                    if (self::getCityAlias()) {
                        $parentUrl .= "/" . self::getCityAlias();
                    }
                    array_push($results, "<a href=\"{$parentUrl}\">{$filterValue->getName()}</a>");
                }
            }
        }
        if ($results && $lastValue) {
            array_pop($results);
            array_push($results, $lastValue->getName());
        }
        return $results;
    }

    /**
     * @return string
     */
    public static function getH1(): string
    {
        /** @var SeoBuilderParamsService $service */
        $service = app(SeoBuilderParamsService::class);
        return $service->getPrepareText(new H1Pattern(), '');
    }

    /**
     * @return iLandingTranslate|null
     */
    public static function getTranslate(): ?iLandingTranslate
    {
        if (!FacadeFilterCatalog::isFilter() || !FacadeLanding::isLanding()) {
            return null;
        }
        $landing = FacadeLanding::getLanding();
        /** @var iLandingTranslate $translate */
        return Translate::get($landing->translations);
    }
}