<?php


namespace App\Modules\SeoModule\Patterns;

use App\Modules\SeoModule\AbstractClasses\AbstractPattern;

/**
 * Class KeywordsPattern
 * @package App\Modules\SeoModule\Patterns
 */
class KeywordsPattern extends AbstractPattern
{
    /**
     * @inheritDoc
     */
    public function getPattern(): string
    {
        return self::PARAMS .$this->getDelimiter()
            . self::CITY . $this->getDelimiter(). self::CUSTOM_1;
    }

    /**
     * @inheritDoc
     */
    public function getDelimiter(): string
    {
        return ", ";
    }

    /**
     * @inheritDoc
     */
    public function strToLower(string $str): string
    {
        return strtolower($str);
    }
}