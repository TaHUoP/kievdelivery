<?php


namespace App\Modules\SeoModule\Patterns;


use App\Modules\SeoModule\AbstractClasses\AbstractPattern;

/**
 * Class TitlePattern
 * @package App\Modules\SeoModule\Patterns
 */
class TitlePattern extends AbstractPattern
{
    /**
     * @return string
     */
    public function getPattern(): string
    {
        return self::CUSTOM_1 . $this->getDelimiter()
            . self::CATEGORY . $this->getDelimiter()
            . self::PARAMS . $this->getDelimiter()
            . self::CUSTOM_2 . $this->getDelimiter()
            . self::CITY . self::LINK_NAME;
    }

    /**
     * @inheritDoc
     */
    public function getDelimiter(): string
    {
        return " ";
    }


    /**
     * @inheritDoc
     */
    public function strToLower(string $str): string
    {
        return strtolower($str);
    }
}