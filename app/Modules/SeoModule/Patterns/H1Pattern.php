<?php


namespace App\Modules\SeoModule\Patterns;


use App\Modules\SeoModule\AbstractClasses\AbstractPattern;

/**
 * Class H1Pattern
 * @package App\Modules\SeoModule\Patterns
 */
class H1Pattern extends AbstractPattern
{
    /**
     * @return string
     */
    public function getPattern(): string
    {
        return self::CATEGORY . $this->getDelimiter()
            . self::PARAMS .$this->getDelimiter()
            . self::CITY;
    }

    /**
     * @inheritDoc
     */
    public function getDelimiter(): string
    {
        return " ";
    }


    /**
     * @inheritDoc
     */
    public function strToLower(string $str): string
    {
        return strtolower($str);
    }
}