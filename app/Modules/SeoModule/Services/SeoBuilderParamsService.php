<?php


namespace App\Modules\SeoModule\Services;

use App\Facades\Translate;
use App\Http\Composers\UserCityComposer;
use App\Models\City;
use App\Models\Translate\Cities;
use App\Modules\CatalogFilterModule\Facade;
use App\Modules\LandingModule\Models\ParamModel;
use App\Modules\SeoModule\AbstractClasses\AbstractPattern;
use Illuminate\Support\Collection;

/**
 * Class SeoBuilderParamsService
 * @package App\Modules\SeoModule\Services
 */
class SeoBuilderParamsService
{
    /**
     * @var string
     */
    protected $categoryName;

    /**
     * @var string
     */
    protected $cityName;

    /**
     * @var string
     */
    protected $cityAlias;

    /**
     * @var string
     */
    protected $params;

    /**
     * @param AbstractPattern $pattern
     * @param string|Collection|ParamModel[] $customs
     * @return string
     */
    public function getPrepareText(AbstractPattern $pattern, $customs = ''): string
    {
        $arr = [
            $this->getCategoryName() => AbstractPattern::CATEGORY,
            $this->getCityName() => AbstractPattern::CITY,
            $this->getParams($pattern) => AbstractPattern::PARAMS,
        ];
        if ($customs === '') {
            $arr[$customs] = AbstractPattern::CUSTOM_1;
            $arr["  "] = AbstractPattern::CUSTOM_2;
        } else {
            if (is_object($customs) && $customs instanceof Collection) {
                $key1 = $customs->first() ? $customs->first()->getParam() !== ''? $customs->first()->getParam() :'' : "";
                $key2 = $customs->last() ? $customs->last()->getParam() !== ''? $customs->last()->getParam() :' ' : " ";
                $arr[$key1] = AbstractPattern::CUSTOM_1;
                $arr[$key2] = AbstractPattern::CUSTOM_2;
            } else {
                $arr[$customs] = AbstractPattern::CUSTOM_1;
                $arr['  '] = AbstractPattern::CUSTOM_2;
            }

        }
        $txt = str_replace(array_values($arr), array_keys($arr), $pattern->getPattern());
        return trim(implode(" ", array_filter(explode(" ", $txt))), ",");
    }

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        if (is_null($this->categoryName)) {
            $this->categoryName = Facade::getCategoryTranslate()->getName() ? Facade::getCategoryTranslate()->getName() : "    " ;
        }
        return $this->categoryName;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        if (is_null($this->cityName)) {
            /** @var UserCityComposer $city */
            $city = app(UserCityComposer::class);
            /** @var Cities $translate */
            $translate = Translate::get($city->getData()->translations);
            $this->cityName = $translate->getNameDeclension();
        }
        return $this->cityName;
    }

    public function getCityAlias(): string
    {
        if (is_null($this->cityAlias)) {
            $this->cityAlias = '';
            /** @var UserCityComposer $city */
            $city = app(UserCityComposer::class);
            if ($city->getAlias() !== City::DEFAULT_CITY) {
                $this->cityAlias = $city->getAlias();
            }
        }
        return $this->cityAlias;
    }

    /**
     * @param AbstractPattern $pattern
     * @return string
     */
    public function getParams(AbstractPattern $pattern): string
    {
        if (is_null($this->params)) {
            $props = \App\Modules\CatalogFilterModule\Facade::buildFilterUrlsTree();
            $this->params = [];
            if ($props->count()) {
                foreach ($props as $prop) {
                    foreach ($prop->getFilterValues() as $filterValue) {
                        if ($filterValue->isActive()) {
                            array_push($this->params, $filterValue->getName());
                        }
                    }
                }
            }
        }
        return $pattern->strToLower(implode($pattern->getDelimiter(), $this->params));
    }

}