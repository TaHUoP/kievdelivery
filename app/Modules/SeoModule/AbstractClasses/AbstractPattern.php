<?php


namespace App\Modules\SeoModule\AbstractClasses;

/**
 * Class AbstractPattern
 * @package App\Modules\SeoModule\AbstractClasses
 */
abstract class AbstractPattern
{
    const LINK_NAME = " | Kievdelivery.com";

    const CATEGORY = "#category#";
    const CUSTOM_1 = "#custom1#";
    const CUSTOM_2 = "#custom2#";
    const CITY = "#city#";
    const PARAMS = "#params#";

    /**
     * @return string
     */
    public abstract function getPattern(): string;

    /**
     * @return string
     */
    public abstract function getDelimiter(): string;

    /**
     * @param string $str
     * @return string
     */
    public abstract function strToLower(string $str): string;

}