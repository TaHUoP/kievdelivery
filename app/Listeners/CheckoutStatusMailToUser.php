<?php

namespace App\Listeners;

use App\Services\SmsC;
use Illuminate\Contracts\Mail\Mailer;
use App\Events\CheckoutChangeStatus;
use App\Models\Shop\Checkout;
use Config;
use Theme;

/**
 * Checkout Status Mail To User
 * @package App\Listeners
 */
class CheckoutStatusMailToUser
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CheckoutChangeStatus  $event
     * @return void
     */
    public function handle(CheckoutChangeStatus $event)
    {
        $checkout = $event->checkout;
        $oldStatus = $event->oldStatus;

        $emailTo = $checkout->email;

        if ($checkout->user) {
            $emailTo = $checkout->user->email;
        }

        $supportEmails = explode(',', Config::get('app.support_email'));
        $supportEmails[] = $emailTo;

        if (!is_null($oldStatus) && (int)$oldStatus !== (int)$checkout->status) {

            switch ($checkout->status) {
                case Checkout::STATUS_PENDING:

                    break;

                case Checkout::STATUS_SUCCESS:
                    $this->mailer->queue('emails.shop.checkout_status_success', ['checkout' => $checkout], function ($m) use ($supportEmails) {
                        $m->from(Config::get('app.email'), Config::get('app.name'));
                        $m->to($supportEmails)->subject(trans('email.new_status_is_shipped'));
                    });

                    if ($event->checkout->send_sms)
                        SmsC::sendSmsOrderDelivered($event->checkout);
                    break;

                case Checkout::STATUS_FAIL:
                    $this->mailer->queue('emails.shop.checkout_status_fail', ['checkout' => $checkout], function ($m) use ($supportEmails) {
                        $m->from(Config::get('app.email'), Config::get('app.name'));
                        $m->to($supportEmails)->subject(trans('email.new_status_is_cancelled'));
                    });

                    if ($event->checkout->send_sms)
                        SmsC::sendSmsOrderCancelled($event->checkout);
                    break;
            }
        }
    }
}
