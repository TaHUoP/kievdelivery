<?php

namespace App\Listeners;

use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Auth\Events\Failed;
use Session;

/**
 * Handle Failed Login
 * @package App\Listeners
 */
class HandleFailedLogin
{
    /**
     * @var HasherContract
     */
    protected $hasher;

    /**
     * HandleFailedLogin constructor.
     * @param HasherContract $hasher
     */
    public function __construct(HasherContract $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        // Если логин введен неправильный
        if (!$event->user) {
            return;
        }

        // Если пароль введен неправильный
        if (!$this->hasher->check($event->credentials['password'], $event->user->getAuthPassword())) {
            return;
        }

        $errors = [];

        if ($event->user && $event->user->isBanned()) {
            $errors[] = trans('auth.you-have-been-banned');
        }

        if ($event->user && $event->user->isInactive()) {
            $errors[] = trans('auth.you-are-not-activated');
        }

        Session::flash('authentication_errors', $errors);
    }
}
