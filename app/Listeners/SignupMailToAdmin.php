<?php

namespace App\Listeners;

use Illuminate\Contracts\Mail\Mailer;
use App\Events\UserSignup;
use Config;

/**
 * Signup Mail To Admin
 * @package App\Listeners
 */
class SignupMailToAdmin
{
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserSignup  $event
     * @return void
     */
    public function handle(UserSignup $event)
    {
        $user = $event->user;
        $supportEmails = explode(',', Config::get('app.support_email'));

        $this->mailer->queue('emails.from_signup_to_support', ['user' => $user], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            $m->subject('Регистрация нового пользователя');
            foreach ($supportEmails as $supportEmail) {
                $m->to($supportEmail);
            }
        });
    }
}
