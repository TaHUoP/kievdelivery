<?php

namespace App\Listeners;

use App\Services\User\EmailConfirmationService;
use App\Events\UserNotConfirmedNotice;

/**
 * Handle Failed Login
 * @package App\Listeners
 *
 * Отлавливаем ивент срабатываемый, когда залогиненый пользователь является неактивным.
 */
class CheckUserForConfirm
{
    /**
     * Сервис конфирмации имейлов у пользователя
     * @var EmailConfirmationService
     */
    protected $confirmationService;

    /**
     * @param EmailConfirmationService $confirmationService
     */
    public function __construct(EmailConfirmationService $confirmationService)
    {
        $this->confirmationService = $confirmationService;
    }

    /**
     * Handle the event.
     *
     * @param UserNotConfirmedNotice $event
     */
    public function handle(UserNotConfirmedNotice $event)
    {
        // Если пользователь неактивирован и у него не назначен конфирмейшн код
        if ($event->user->isInactive() && empty($event->user->confirmation_code)) {
            $this->confirmationService->setUserToBeConfirmed($event->user);
        }
    }
}
