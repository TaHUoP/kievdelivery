<?php

namespace App\Listeners;

use Illuminate\Contracts\Mail\Mailer;
use App\Events\CheckoutPost;
use Config;

/**
 * Checkout Mail ToUser
 * @package App\Listeners
 */
class CheckoutMailToUser
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CheckoutPost  $event
     * @return void
     */
    public function handle(CheckoutPost $event)
    {
        $checkout = $event->checkout;
        $emailTo = $checkout->email;

        if ($checkout->user) {
            $emailTo = $checkout->user->email;
        }

        $supportEmails = explode(',', Config::get('app.support_email'));
        $supportEmails[] = $emailTo;

        $this->mailer->queue('emails.shop.checkout_new_order_to_user', ['checkout' => $checkout], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            $m->to($supportEmails)->subject(trans('email.new_order'));
        });
    }
}
