<?php

namespace App\Listeners;

use App\Events\ReviewCreated;
use Illuminate\Contracts\Mail\Mailer;
use Config;

class ReviewMailToAdmin
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  ReviewCreated  $event
     * @return void
     */
    public function handle(ReviewCreated $event)
    {
        $supportEmails = explode(',', Config::get('app.support_email'));

        $this->mailer->queue('emails.review_created', [], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            $m->subject(trans('email.new_order'));
            $m->to('contact@kievdelivery.com');
        });
    }
}
