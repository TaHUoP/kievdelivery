<?php

namespace App\Listeners;

use App\Services\SmsC;
use Illuminate\Contracts\Mail\Mailer;
use App\Events\CheckoutPayment;
use Config;
use Theme;

/**
 * Checkout Payment Mail To User
 * @package App\Listeners
 */
class CheckoutPaymentMailToUser
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CheckoutPayment  $event
     * @return void
     */
    public function handle(CheckoutPayment $event)
    {
        $checkout = $event->checkout;

        $emailTo = $checkout->email;

        if ($checkout->user) {
            $emailTo = $checkout->user->email;
        }

        $supportEmails = explode(',', Config::get('app.support_email'));
        $supportEmails[] = $emailTo;

        $this->mailer->queue('emails.shop.checkout_payment_to_user', [
            'checkout' => $checkout,
            'ip_address' => request()->getClientIp()
        ], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            $m->to($supportEmails)->subject(trans('email.checkout_payment_success'));
        });

        if ($event->checkout->send_sms)
            SmsC::sendSmsOrderPaid($event->checkout);
    }
}
