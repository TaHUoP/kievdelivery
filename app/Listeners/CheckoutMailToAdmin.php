<?php

namespace App\Listeners;

use Illuminate\Contracts\Mail\Mailer;
use App\Events\CheckoutPost;
use Config;

/**
 * Checkout Mail To Admin
 * @package App\Listeners
 */
class CheckoutMailToAdmin
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CheckoutPost  $event
     * @return void
     */
    public function handle(CheckoutPost $event)
    {
        $checkout = $event->checkout;

        $supportEmails = explode(',', Config::get('app.support_email'));

        $this->mailer->queue('emails.shop.checkout_new_order_to_admin', ['checkout' => $checkout], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            $m->subject(trans('email.new_order'));
            $m->to($supportEmails);
        });
    }
}
