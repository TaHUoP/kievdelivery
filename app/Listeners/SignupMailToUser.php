<?php

namespace App\Listeners;

use Illuminate\Contracts\Mail\Mailer;
use Api\App\Storage\Condition;
use App\Events\UserSignup;
use Config;

/**
 * Signup Mail To User
 * @package App\Listeners
 */
class SignupMailToUser
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param  UserSignup  $event
     * @return void
     */
    public function handle(UserSignup $event)
    {
        $user = $event->user;

        $this->mailer->queue('emails.from_signup_for_confirmation', ['user' => $user], function ($m) use ($user) {
            $m->from(config('app.email'), config('app.name'));
            $m->subject(trans('email.theme.signup'));
            $m->to($user->email);
        });
    }
}
