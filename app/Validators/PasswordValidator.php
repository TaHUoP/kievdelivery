<?php

namespace App\Validators;

use Illuminate\Validation\Validator;

/**
 * Password Validator
 * @package App\Validators
 */
class PasswordValidator extends Validator
{
    /**
     * Логика валидации
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validatePassword($attribute, $value, $parameters)
    {
        $lowercase = preg_match('/[a-zа-я]/', $value);
        if (!$lowercase) {
            return false;
        }

        $uppercase = preg_match('/[A-ZА-Я]/', $value);
        if (!$uppercase) {
            return false;
        }

        $digits = preg_match('/\d/', $value);
        if (!$digits) {
            return false;
        }

        return true;
    }

}