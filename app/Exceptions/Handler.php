<?php

namespace App\Exceptions;

use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Api\App\Storage\Condition;
use Exception;
use Request;
use Theme;
use App;

/**
 * Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $e)
    {
        // 404 page when a model is not found
        if ($e instanceof NotFoundHttpException) {
            if (Request::ajax() || Request::wantsJson()) {
                return $this->ajaxError($e, 404);
            }

            if(!Request::is('backend/*')){
                return redirect()->route('error.404');                
            }
            
            $this->setBackendTheme();

            $contentRepository = App::make(ContentInterface::class);

            $condition = new Condition();
            $condition->addCondition('url', 'url', '404')
                ->addCondition('type', 'type', $contentRepository->getTypePage())
                ->scopes(['visible' => 0]);

            $content = $contentRepository->findOne($condition);

            return response()->view('errors.404', [
                'content' => $content,
            ], 404);
        }

        if ($this->isHttpException($e)) {
            return $this->renderHttpException($e);
        }

        return parent::render($request, $e);
    }

    /**
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function convertExceptionToResponse(Exception $e)
    {
        if (config('app.debug')) {
            return parent::convertExceptionToResponse($e);
        }
        if (Request::ajax() || Request::wantsJson()) {
            return $this->ajaxError($e, 500);
        }

        $this->setBackendTheme();

        return response()->view('errors.others', [
            'e' => $e
        ], 500);
    }

    /**
     * Render for ajax response
     *
     * @param $e
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function ajaxError($e, $code)
    {
        return response()->json([
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
        ], $code);
    }

    /**
     * TODO: заменить на хелпер
     */
    protected function setBackendTheme()
    {
        if (Request::is('backend/*')) {
            Theme::set('backend');
        }
    }
}
