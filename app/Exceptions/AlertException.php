<?php

namespace App\Exceptions;

use ErrorException;

/**
 * Alert Exception
 * @package App\Exceptions
 */
class AlertException extends ErrorException
{
    private $info;

    /**
     * @param string $info
     */
    public function __construct($info)
    {
        $this->info = $info;
        parent::__construct($this->info);
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }
}