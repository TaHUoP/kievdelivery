<?php

namespace App\Exceptions;

use Api\App\Contracts\Exceptions\ForErrorBagException;
use ErrorException;

/**
 * Signup Exception
 * @package App\Exceptions
 */
class SignupException extends ErrorException implements ForErrorBagException
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * Array of message for error bag
     *
     * @return array
     */
    public function errorBagMessage()
    {
        return [
            $this->getAttribute() => $this->getMessage()
        ];
    }

    /**
     * Name of invalid attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->attribute ? : '0';
    }
}