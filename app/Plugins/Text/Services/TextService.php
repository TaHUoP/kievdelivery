<?php

namespace App\Plugins\Text\Services;

use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use App\Models\Plugin;
use DB;

/**
 * Text Service
 * @package App\Plugins\Text\Services
 */
class TextService
{
    /**
     * @var PluginInterface
     */
    protected $pluginRepository;

    /**
     * @param PluginInterface $pluginRepository
     */
    public function __construct(PluginInterface $pluginRepository)
    {
        $this->pluginRepository = $pluginRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Plugin|null $model
     * @return bool
     */
    public function save(array $data, Plugin $model = null)
    {
        $data['value'] = $this->encode($data['content']);

        if ($this->pluginRepository->save($data, $model)) {
            return true;
        }

        return false;
    }

    /**
     * @param Plugin $model
     * @return mixed
     */
    public function decode(Plugin $model)
    {
        $model->value = @json_decode($model->value, true);
        if (!is_array($model->value)) {
            $model->value = [];
        }
        return $model;
    }

    /**
     * @param  $data
     * @return mixed
     */
    public function encode($data)
    {
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        return $data;
    }

    /**
     * Удалить сущность
     *
     * @param Plugin $model
     * @return bool
     */
    public function delete(Plugin $model)
    {
        if ($this->pluginRepository->delete($model)) {
            return true;
        }

        return false;
    }
}