<?php

namespace App\Plugins\Text\Services;

use Translate;

/**
 * Handler Plugin
 * @package App\Plugins\Text\Services
 */
class HandlerPlugin
{
    public $trans;

    /**
     * Переработка json для отображения на front в зависимости от языка
     *
     * @param $data
     * @return mixed
     */
    public function getData($data)
    {
        $locale = Translate::getCurrentLang('locale');
        $data = @json_decode($data, true);

        if (isset($data[$locale])) {
            return $data[$locale];
        }

        return null;
    }

}