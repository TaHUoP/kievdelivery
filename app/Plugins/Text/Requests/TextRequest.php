<?php

namespace App\Plugins\Text\Requests;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Request;

/**
 * Text Request
 * @package App\Plugins\Text\Requests
 */
class TextRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;

        $dl = $translateHelper->getDefaultLang('locale');

        return [
            'content.*.content' => 'string|required_with:' . $this->with('title', 'translations.*'),
            'content.' . $dl . '.*.content' => 'required|string',
        ];
    }

    /**
     * Возвращает строку для валидации
     *
     * $without - к примеру валидируем name, то в метод передать name
     * $parentKey - к примеру валидируем переменный перевод, передаем translations.*
     * к примеру, ввели то что выше, тогда вернёт translations.*.short, translations.*.full ...
     *
     * @param $without
     * @param $parentKey
     * @return string
     */
    protected function with($without, $parentKey)
    {
        $all = $this->getTranslationInputs();

        if ($foundKey = array_search($without, $all)) {
            unset($all[$foundKey]);
        }

        foreach ($all as $key => &$item) {
            $item = $parentKey . '.' . $item;
        }

        return implode(',', $all);
    }

    /**
     * Все поля для переводов
     * @return array
     */
    protected function getTranslationInputs()
    {
        return [
            'content'
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->input();
        $input['content'] = filter_var(isset($input['content']) ? $input['content'] : null, FILTER_SANITIZE_STRING);
        $this->replace($input);
    }
}