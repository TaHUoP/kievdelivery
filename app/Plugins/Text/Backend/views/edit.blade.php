@extends('layouts.main')

@section('title', trans('plugins.create'))

@section('page-title')
    <h4>{{ trans('plugins.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.plugins.index').'">' . trans('plugins.plugins') . '</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')
    <button type="button" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </button>
@stop

@push('scripts')
    <script src="{{ Statics::asset('extends/plugins/slider/js/slider.js') }}"></script>
@endpush

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('plugins.text') }}</h5>
            <div class="heading-elements panel-nav">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'plugin',
                ]) }}
            </div>
        </div>
        {{ Form::open(['route' => ['backend.plugins.text.update', $text], 'class' => 'form-image', 'method' => 'PUT', 'data-id' => $text->id]) }}
        {{ Form::hidden('slider', $text->id) }}
        <div class="panel-body">
            <div class="tabbable">
                <div class="tab-content">
                    @foreach ($languageControl['all'] as $language)
                        <div class="tab-pane fade in{{ $language->default ? ' active' : '' }}"
                             id="lang-plugin-{{ $language->locale }}">
                            <div class="block-inner">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group required {{ $errors->has('content[' . $language->locale . ']') ? 'has-error' : '' }}">
                                            {{ Form::label('content', trans('plugins.text.attr.content')) }}
                                            {{ Form::textarea('content[' . $language->locale . ']', isset($text->value[$language->locale])? $text->value[$language->locale] : null, [
                                                'class' => 'form-control',
                                            ]) }}
                                            <div class="help-block">{{ $errors->has('content[' . $language->locale . ']') ? $errors->first('content[' . $language->locale . ']') : '' }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit"
                        class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@stop