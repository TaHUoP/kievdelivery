<?php

namespace App\Plugins\Text\Backend;

use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use App\Plugins\Text\Requests\TextRequest;
use App\Plugins\Text\Services\TextService;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Request;
use Session;
use DB;

/**
 * Text Controller
 * @package App\Plugins\Text\Backend
 */
class TextController extends \App\Http\Controllers\Controller
{
    /**
     * @var PluginInterface
     */
    protected $pluginRepository;

    /**
     * @var PluginInterface
     */
    protected $languageRepository;

    /**
     * @var TextService
     */
    protected $textService;

    /**
     * @param PluginInterface $pluginRepository
     * @param LanguageInterface $languageRepository
     * @param TextService $textService
     */
    public function __construct(
        PluginInterface $pluginRepository,
        LanguageInterface $languageRepository,
        TextService $textService
    )
    {
        $this->pluginRepository = $pluginRepository;
        $this->languageRepository = $languageRepository;
        $this->textService = $textService;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$text = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        if (!empty($text->value)) {
            $text->value = json_decode($text->value, true);
        }

        $condition = new Condition();
        $languages = $this->languageRepository->findAll($condition);

        return view('plugin::Text.Backend.views.edit', [
            'text' => $text,
            'language' => $languages
        ]);
    }

    /**
     * @param TextRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(TextRequest $request)
    {
        if (!$text = $this->pluginRepository->findById($request->slider)) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $result = $this->textService->save($request->all(), $text);
            if ($result) {
                DB::commit();
                Session::flash('success', trans('app.update_success'));
                return redirect(route('backend.plugins.index'));
            } else {
                DB::rollBack();
                Session::flash('danger', trans('app.critical_error'));
            }
            DB::commit();
        } catch (AlertException $e) {
            DB::rollBack();
            Session::flash('danger', $e->getInfo());
        }

        return view('plugins.text.create', ['text' => $text]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (!$content = $this->pluginRepository->findById($id)) {
            abort(404);
        }
        if ($this->textService->delete($content)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.plugins.index'));
    }

}
