<?php

namespace App\Plugins\Slider\Helpers;

use Storage;
use Theme;
use App;

/**
 * Slider Helper
 * @package App\Plugins\Slider\Helpers
 */
class SliderHelper
{
    /**
     * @param $slide
     * @return null|string
     */
    public static function slide($slide)
    {
        $url = self::getUrlToImage($slide);

        $pathToPublic = public_path();

        if (!empty($url) && file_exists($pathToPublic . $url)) {
            return $url;
        }

        return DIRECTORY_SEPARATOR . self::getUrlToTheme() . '/default/images/placeholder.jpg';
    }

    /**
     * @return mixed
     */
    public static function getUrlToTheme()
    {
        return Theme::config('asset-path');
    }

    /**
     * @param $slide
     * @return null|string
     */
    public static function getUrlToImage($slide)
    {
        if (isset($slide['img']) && !empty($slide['img'])) {
            //return '/upload/plugins/slider/' . $slide['img'];
            return $slide['img'];
        }
        return null;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public static function convertSlideToValidFormat(array $data)
    {
        $service = App::make(App\Plugins\Slider\Services\SliderService::class);
        return $service->transformSlider($data);
    }
}