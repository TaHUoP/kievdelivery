<?php

namespace App\Plugins\Slider\Requests;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Request;

/**
 * Slider Request
 * @package App\Plugins\Slider\Requests
 */
class SliderRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->sanitize();
        $this->translateHelper = $translateHelper;
        $dl = $translateHelper->getDefaultLang('locale');

        $rules = [];
        foreach ($this->get('slider')['img'] as $key => $img) {
            $rules = array_merge($rules, [
                'slider.translations.*.' . $key . '.url' => 'string|required_with:' . $this->with('url', 'translations.*'),
                'slider.translations.*.' . $key . '.title' => 'string|required_with:' . $this->with('title', 'translations.*'),
                'slider.translations.*.' . $key . '.description' => 'string|required_with:' . $this->with('description', 'translations.*'),

                'slider.translations.' . $dl . '.' . $key . '.url' => 'string',
                'slider.translations.' . $dl . '.' . $key . '.title' => 'string',
                'slider.translations.' . $dl . '.' . $key . '.description' => 'string',
            ]);
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $dl = $this->translateHelper->getDefaultLang('locale');
        $rules = [];
        foreach ($this->get('slider')['img'] as $key => $img) {
            $rules = array_merge($rules, [
                'slider.translations.' . $dl . '.' . $key . '.url' => trans('app.attr.url'),
                'slider.translations.' . $dl . '.' . $key . '.title' => trans('app.attr.title'),
                'slider.translations.' . $dl . '.' . $key . '.description' => trans('app.attr.description'),
            ]);
        }
        return $rules;
    }

    /**
     * Возвращает строку для валидации
     *
     * $without - к примеру валидируем name, то в метод передать name
     * $parentKey - к примеру валидируем переменный перевод, передаем translations.*
     * к примеру, ввели то что выше, тогда вернёт translations.*.short, translations.*.full ...
     *
     * @param $without
     * @param $parentKey
     * @return string
     */
    protected function with($without, $parentKey)
    {
        $all = $this->getTranslationInputs();

        if ($foundKey = array_search($without, $all)) {
            unset($all[$foundKey]);
        }

        foreach ($all as $key => &$item) {
            $item = $parentKey . '.' . $item;
        }

        return implode(',', $all);
    }

    /**
     * Все поля для переводов
     * @return array
     */
    protected function getTranslationInputs()
    {
        return [
            'url',
            'title',
            'description'
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->input();

        if (!isset($input['slider']) || !is_array($input['slider'])) {
            $input['slider'] = [];
        }

        if (!isset($input['slider']['img']) || !is_array($input['slider']['img'])) {
            $input['slider']['img'] = [];
        }

        $this->replace($input);
    }
}