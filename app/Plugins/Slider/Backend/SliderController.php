<?php

namespace App\Plugins\Slider\Backend;

use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use App\Plugins\Slider\Services\SliderService;
use App\Plugins\Slider\Requests\SliderRequest;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Request;
use Session;
use DB;

/**
 * Slider Controller
 * @package App\Plugins\Slider\Backend
 */
class SliderController extends \App\Http\Controllers\Controller
{
    /**
     * @var PluginInterface
     */
    protected $pluginRepository;

    /**
     * @var PluginInterface
     */
    protected $languageRepository;

    /**
     * @var $sliderService
     */
    protected $sliderService;

    /**
     * @param SliderService $sliderService
     * @param PluginInterface $pluginRepository
     * @param LanguageInterface $languageRepository
     */
    public function __construct(
        SliderService $sliderService,
        PluginInterface $pluginRepository,
        LanguageInterface $languageRepository
    )
    {
        $this->sliderService = $sliderService;
        $this->pluginRepository = $pluginRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$slider = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        $slider = $this->sliderService->decode($slider);

        $condition = new Condition();
        $languages = $this->languageRepository->findAll($condition);

        return view('plugin::Slider.Backend.views.edit', [
            'slider' => $slider,
            'language' => $languages
        ]);
    }

    /**
     * @param SliderRequest $request
     * @param $id
     * @return \Illuminate\View\View
     */
    public function update(SliderRequest $request, $id)
    {
        if (!$slider = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $result = $this->sliderService->save($request->input('slider'), $slider);
            if ($result) {
                DB::commit();
                Session::flash('success', trans('app.update_success'));
                return back();
            } else {
                DB::rollBack();
                Session::flash('danger', trans('app.critical_error'));
            }
            DB::commit();
        } catch (AlertException $e) {
            DB::rollBack();
            Session::flash('danger', $e->getInfo());
        }

        return view('plugins.slider.create', ['slider' => $slider]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (!$content = $this->pluginRepository->findById($id)) {
            abort(404);
        }
        if ($this->sliderService->delete($content)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.plugins.index'));
    }

}
