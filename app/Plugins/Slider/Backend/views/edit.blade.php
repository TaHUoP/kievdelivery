<?php use \App\Plugins\Slider\Helpers\SliderHelper; ?>

@extends('layouts.main')

@section('title', trans('plugins.create'))

@section('page-title')
    <h4>{{ trans('plugins.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.plugins.index').'">' . trans('plugins.all') . '</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')
    <button type="button" class="btn btn-link btn-float has-text" id="slide-add">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.add') }}</span>
    </button>
@stop

@push('scripts')
    <script src="{{ Statics::asset('extends/plugins/slider/js/slider.js') }}"></script>
@endpush

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('plugins.slider') }}</h5>
            <div class="heading-elements panel-nav">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'plugin',
                ]) }}
            </div>
        </div>
        {{ Form::open(['route' => ['backend.plugins.slider.update', $slider], 'class' => 'form-image', 'method' => 'POST', 'data-id' => $slider->id]) }}
        <div class="panel-body">
            <div class="tabbable">
                <div class="tab-content">
                    <div id="no-data"{{ empty($slider->value) ? '' : ' class=hide' }}>{{ trans('app.no-data') }}</div>
                    @foreach ($languageControl['all'] as $language)
                        <div class="tab-pane fade in{{ $language->default ? ' active' : '' }}" id="lang-plugin-{{ $language->locale }}">
                            <div class="block-inner">
                                <div class="col-sm-4 slide slide-template" style="display: none" data-locale="{{ $language->locale }}">
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <div class="thumb-image" style="background-image: url({{ SliderHelper::slide(null) }})"></div>
                                            <div class="caption-overflow">
                                                <span>
                                                    <button type="button" data-popup="lightbox" class="slide-upload btn border-white text-white btn-flat btn-icon btn-rounded legitRipple">
                                                        <i class="icon-plus3"></i>
                                                    </button> &nbsp;
                                                    <button type="button" data-confirm="{{ trans('app.confirm') }}" class="btn slide-remove border-danger text-danger btn-flat btn-icon btn-rounded legitRipple">
                                                        <i class="icon-cross"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        {{Form::hidden('slider[img][0]', null, ['disabled' => 'disabled']) }}
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('url', null, trans('app.attr.url')) }}
                                                {{ Form::text('slider[translations][' . $language->locale . '][0][url]', null, [
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled',
                                                ]) }}
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                {{ Form::label('title', trans('app.attr.title')) }}
                                                {{ Form::text('slider[translations][' . $language->locale . '][0][title]', null, [
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled',
                                                ]) }}
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                {{ Form::label('description', trans('app.attr.description')) }}
                                                {{ Form::text('slider[translations][' . $language->locale . '][0][description]', null, [
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled',
                                                ]) }}
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {{ Form::label('description', trans('app.attr.order')) }}
                                                {{ Form::text('slider[translations][' . $language->locale . '][0][order]', null, [
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled',
                                                ]) }}
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-list" data-locale="{{ $language->locale }}">
                                    @if(isset($slider->value) && is_array($slider->value))
                                        @if (old('slider'))
                                            <?php $slider->value = SliderHelper::convertSlideToValidFormat(old('slider')) ?>
                                        @endif
                                        @foreach($slider->value as $key => $slide)
                                            <div class="col-sm-4 slide" data-id="{{ $key }}">
                                                <div class="thumbnail">
                                                    <div class="thumb">
                                                        <div class="thumb-image" style="background-image: url({{ SliderHelper::slide($slide) }})"></div>
                                                        <div class="caption-overflow">
                                                            <span>
                                                                <button type="button" data-popup="lightbox" class="slide-upload btn border-white text-white btn-flat btn-icon btn-rounded legitRipple">
                                                                    <i class="icon-plus3"></i>
                                                                </button> &nbsp;
                                                                <button type="button" data-confirm="{{ trans('app.confirm') }}" class="btn slide-remove border-danger text-danger btn-flat btn-icon btn-rounded legitRipple">
                                                                    <i class="icon-cross"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    {{ Form::hidden('slider[img]['.$key.']', isset($slide['img']) ? $slide['img'] : null) }}

                                                    <div class="col-sm-6">
                                                        <div class="form-group {{ $errors->has('slider.translations.' . $language->locale . '.' . $key. '.url') ? 'has-error' : '' }}">
                                                            {{ Form::label('url', trans('app.attr.url')) }}
                                                            {{ Form::text('slider[translations][' . $language->locale . ']['.$key.'][url]', isset($slide['translations'][$language->locale]['url']) ? $slide['translations'][$language->locale]['url'] : null, [
                                                                'class' => 'form-control',
                                                            ]) }}
                                                            <div class="help-block">
                                                                {{ $errors->has('slider.translations.' . $language->locale .'.'. $key. '.url') ? $errors->first('slider.translations.' . $language->locale .'.'. $key. '.url') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group {{ $errors->has('slider.translations.' . $language->locale . '.' . $key. '.title') ? 'has-error' : '' }}">
                                                            {{ Form::label('title', trans('app.attr.title')) }}
                                                            {{ Form::text('slider[translations][' . $language->locale . ']['.$key.'][title]', isset($slide['translations'][$language->locale]['title']) ? $slide['translations'][$language->locale]['title'] : null, [
                                                                'class' => 'form-control',
                                                            ]) }}
                                                            <div class="help-block">
                                                                {{ $errors->has('slider.translations.' . $language->locale .'.'. $key. '.title') ? $errors->first('slider.translations.' . $language->locale .'.'. $key. '.title') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="form-group {{ $errors->has('slider.translations.' . $language->locale . '.' . $key. '.description') ? 'has-error' : '' }}">
                                                            {{ Form::label('description', trans('app.attr.description')) }}
                                                            {{ Form::text('slider[translations][' . $language->locale . ']['.$key.'][description]', isset($slide['translations'][$language->locale]['description']) ? $slide['translations'][$language->locale]['description'] : null, [
                                                                'class' => 'form-control',
                                                            ]) }}
                                                            <div class="help-block">
                                                                {{ $errors->has('slider.translations.' . $language->locale . '.' . $key. '.description') ? $errors->first('slider.translations.' . $language->locale . '.' . $key. '.description') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group {{ $errors->has('slider.translations.' . $language->locale . '.' . $key. '.order') ? 'has-error' : '' }}">
                                                            {{ Form::label('order', trans('app.attr.order')) }}
                                                            {{ Form::text('slider[translations][' . $language->locale . ']['.$key.'][order]', isset($slide['translations'][$language->locale]['order']) ? $slide['translations'][$language->locale]['order'] : null, [
                                                                'class' => 'form-control',
                                                            ]) }}
                                                            <div class="help-block">
                                                                {{ $errors->has('slider.translations.' . $language->locale . '.' . $key. '.order') ? $errors->first('slider.translations.' . $language->locale . '.' . $key. '.order') : '' }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-save btn-success heading-btn pull-left legitRipple{{ empty($slider->value) ? ' hide' : '' }}">
                    {{ trans('app.save') }}
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </div>


    <div id="modal-file-manager" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header custom-modal-bg">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h6 class="modal-title">{{ trans('app.file-manager') }}</h6>
                </div>
                <div class="modal-body no-padding">
                    <div id="slider-elfinder" data-url="{{ route('elfinder.connector') }}"></div>
                </div>
                <div class="modal-footer text-left"></div>
            </div>
        </div>
    </div>

@stop