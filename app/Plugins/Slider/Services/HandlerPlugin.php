<?php

namespace App\Plugins\Slider\Services;

use Translate;

/**
 * Handler Plugin
 * @package App\Plugins\Slider\Services
 */
class HandlerPlugin
{
    public $trans;

    /**
     * Переработка json для отображения на front в зависимости от языка
     *
     * @param $data
     * @return mixed
     */
    public function getData($data)
    {
        $locale = Translate::getCurrentLang('locale');

        $data = @json_decode($data, true);

        if (!is_array($data)) {
            return [];
        }

        foreach ($data as &$slides) {
            foreach ($slides as &$titles) {
                if (is_array($titles)) {
                    if (key_exists($locale, $titles)) {
                        $titles = $titles[$locale];
                    }
                }
            }
        }

        return $data;
    }

}