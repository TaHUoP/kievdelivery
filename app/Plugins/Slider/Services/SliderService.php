<?php

namespace App\Plugins\Slider\Services;

use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use App\Models\Plugin;
use DB;

/**
 * Slider Service
 * @package App\Plugins\Slider\Services
 */
class SliderService
{
    /**
     * @var PluginInterface
     */
    protected $pluginRepository;

    /**
     * @param PluginInterface $pluginRepository
     */
    public function __construct(PluginInterface $pluginRepository)
    {
        $this->pluginRepository = $pluginRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Plugin|null $model
     * @return bool
     */
    public function save(array $data, Plugin $model = null)
    {
        $value = $this->transformSlider($data);

        $slider['value'] = @json_encode($value);

        if ($this->pluginRepository->save($slider, $model)) {
            return true;
        }

        return false;
    }

    /**
     * Трансформирует слайдер для записи и отображения
     *
     * @param array $data
     * @return array
     */
    public function transformSlider(array $data)
    {
        $result = [];

        if (isset($data['img']) && is_array($data['img'])) {
            foreach ($data['img'] as $key => $img) {
                $result[$key]['img'] = $img;
            }
            if (isset($data['translations'])) {
                foreach ($result as $key => $slider) {
                    foreach ($data['translations'] as $locale => $messages) {
                        if (isset($data['translations'][$locale][$key])) {
                            $result[$key]['translations'][$locale] = $data['translations'][$locale][$key];
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param Plugin $model
     * @return mixed
     */
    public function decode(Plugin $model)
    {
        $model->value = @json_decode($model->value, true);

        if (!is_array($model->value)) {
            $model->value = [];
        }

        return $model;
    }

    /**
     * @param  $data
     * @return mixed
     */
    public function encode($data)
    {
        $data = @json_encode($data, true);
        return $data;
    }

    /**
     * Удалить сущность
     *
     * @param Plugin $model
     * @return bool
     */
    public function delete(Plugin $model)
    {
        if ($this->pluginRepository->delete($model)) {
            return true;
        }

        return false;
    }
}