<?php

namespace App\Services;

use Api\App\Contracts\Entities\SeoSpecificationRepository as SeoSpecificationInterface;
use App\Models\SeoSpecification;
use Auth;
use DB;

/**
 * Seo Service
 * @package App\Services
 */
class SeoService
{
    /**
     * @var SeoSpecificationInterface
     */
    protected $seoSpecificationRepository;

    /**
     * @param SeoSpecificationInterface $seoSpecificationRepository
     */
    public function __construct(SeoSpecificationInterface $seoSpecificationRepository)
    {
        $this->seoSpecificationRepository = $seoSpecificationRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param SeoSpecification|null $model
     * @return bool
     */
    public function save(array $data, SeoSpecification $model = null)
    {
        DB::beginTransaction();

        if ($this->seoSpecificationRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param SeoSpecification $model
     * @return bool
     */
    public function delete(SeoSpecification $model)
    {
        DB::beginTransaction();

        if ($this->seoSpecificationRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}