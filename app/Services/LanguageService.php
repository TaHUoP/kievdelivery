<?php

namespace App\Services;

use Api\App\Contracts\Entities\TranslationRepository as TranslationInterface;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Illuminate\Support\Collection;
use Session;
use Auth;
use DB;

/**
 * Language Service
 * @package App\Services
 */
class LanguageService
{
    /**
     * @var TranslationInterface
     */
    protected $translationRepository;

    /**
     * @var LanguageInterface
     */
    protected $languageRepository;

    /**
     * @var array
     */
    private $languages;

    /**
     * Language Service constructor
     * @param TranslationInterface $translationRepository
     * @param LanguageInterface $languageRepository
     */
    public function __construct(
        TranslationInterface $translationRepository,
        LanguageInterface $languageRepository
    )
    {
        $this->translationRepository = $translationRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * Создать новый язык
     *
     * @param array $data
     * @return bool
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        $condition = new Condition();

        if ($data['default']) {
            $data['active'] = 1;
            $this->languageRepository->updateAll($condition, ['default' => '0']);
        } else {
            if (!$this->languageRepository->findAll(new Condition())) {
                $data['default'] = 1;
                $data['active'] = 1;
            }
        }

        if ($this->languageRepository->save($data)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Редактирует язык
     *
     * @param array $data
     * @param $model
     * @return bool
     * @throws AlertException
     * @throws \ErrorException
     */
    public function update(array $data, $model)
    {
        DB::beginTransaction();

        if ((!$data['default'] || !$data['active']) && !$this->languageRepository->otherExists($model->id)) {
            DB::rollBack();
            throw new AlertException(trans('languages.msg.only-one'));
        }

        if ($data['default'] && $this->languageRepository->otherExists($model->id)) {
            $data['active'] = 1;
            $condition = new Condition();
            $condition->addCondition('id', 'id !=', $model->id);
            $this->languageRepository->updateAll($condition, ['default' => 0]);
        }

        if (!$data['default'] && $model->default) {
            throw new AlertException(trans('languages.msg.setting_default'));
        }

        if ($this->languageRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }


    /**
     * Удалить сущность
     *
     * @param $model
     * @return bool
     * @throws AlertException
     * @throws \ErrorException
     */
    public function delete($model)
    {
        DB::beginTransaction();

        if (!$this->languageRepository->otherExists($model->id)) {
            DB::rollBack();
            throw new AlertException(trans('languages.msg.delete-only-one'));
        }

        if ($model->default) {
            $condition = new Condition();
            $condition
                ->addCondition('id', 'id !=', $model->id)
                ->scopes(['active']);
            $langCandidate = $this->languageRepository->findOne($condition);
            if (!$langCandidate) {
                $condition = new Condition();
                $condition
                    ->addCondition('id', 'id !=', $model->id);
                $langCandidate = $this->languageRepository->findOne($condition);
            }
            $this->languageRepository->update($langCandidate, ['default' => 1, 'active' => 1]);
        }

        $this->languageRepository->delete($model);

        DB::commit();

        return true;
    }

    /**
     * Редактировать список переводов
     *
     * @param array $translates
     * @throws AlertException
     * @return bool
     */
    public function updateTranslations(array $translates)
    {
        $languages = $this->all();

        foreach ($translates as $locale => $groups) {

            // Проверка на существование такой локали
            if (!$languages->where('locale', $locale)->first()) {
                throw new AlertException(trans('app.wrong-post-data-format'));
            }

            foreach ($groups as $group => $items) {
                foreach ($items as $translate) {
                    if (!empty($translate['id'])) {
                        // Удалить если текст опустошили
                        if (empty(trim($translate['text']))) {
                            $model = $this->translationRepository->findById($translate['id']);
                            if (!$model) {
                                throw new AlertException(trans('app.critical_error'));
                            }
                            $this->translationRepository->delete($model);
                        } else {
                            $this->translationRepository->update($translate['id'], $translate['text']);
                        }
                    } else {
                        $condition = new Condition();
                        $condition->addCondition('locale', 'locale', $locale);
                        $condition->addCondition('group', 'group', $group);
                        $condition->addCondition('item', 'item', $translate['constant']);

                        // Проверка уникальности
                        if ($this->translationRepository->findOne($condition)) {
                            throw new AlertException(trans('app.critical_error'));
                        }

                        if (!empty($translate['text'])) {
                            $this->translationRepository->create([
                                'locale' => $locale,
                                'namespace' => '*',
                                'group' => $translate['group'],
                                'item' => $translate['constant'],
                                'text' => $translate['text'],
                            ]);
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Взять все языки
     *
     * @return Collection|mixed
     */
    public function all()
    {
        if (empty($this->languages)) {
            $condition = new Condition();
            $this->languages = $this->languageRepository->findAll($condition);
        }

        return $this->languages;
    }

    /**
     * Взять только один язык
     *
     * @param $id
     * @param bool $fromBag
     * @return mixed
     */
    public function only($id, $fromBag = true)
    {
        if (!$fromBag) {
            $this->languageRepository->findById($id);
        }

        return $this->all()->first(function ($key, $value) use ($id) {
            return $value->id == $id;
        });
    }

    /**
     * Взять дефолтный язык
     *
     * @param bool $fromBag
     * @return mixed
     */
    public function defaultLanguage($fromBag = true)
    {
        if (!$fromBag) {
            return $this->languageRepository->findByDefault();
        }

        return $this->all()->first(function ($key, $value) {
            return $value->default == 1;
        });
    }

    /**
     * Взять первый не дефолтный язык
     *
     * @param bool $fromBag
     * @return mixed
     */
    public function firstNotDefault($fromBag = true)
    {
        if (!$fromBag) {
            return $this->languageRepository->findByDefault(0);
        }

        return $this->all()->first(function ($key, $value) {
            return $value->default == 0;
        });
    }

    /**
     * Взять все языки кроме языка с id $id
     *
     * @param $id
     * @param bool $fromBag
     * @return Collection
     */
    public function allExcept($id, $fromBag = true)
    {
        if (!$fromBag) {
            return $this->languageRepository->other($id);
        }

        return $this->all()->filter(function ($value, $key) use ($id) {
            return $value->id != $id;
        });
    }

    /**
     * Взять группы определенного языка
     *
     * @param $locale
     * @return array
     */
    public function groupsOfLanguage($locale)
    {
        return $this->translationRepository->selectDistinctGroupsByLocale($locale)
            ->pluck('group', 'group')->toArray();
    }

    /**
     * Возвращает подходящий массив для формы скомбинированный из 2 переводов
     *
     * @param $mainLocale
     * @param $semiLocale
     * @return array
     */
    public function relativeFormTranslations($mainLocale, $semiLocale, $group = null)
    {
        $condition = new Condition();
        $condition->addCondition('locale', 'locale', $mainLocale);
        $condition->limit(null);

        if ($group !== null) {
            $condition->addCondition('group', 'group', $group);
        }

        $mainTranslations = $this->translationRepository->findAll($condition);

        if ($semiLocale !== null) {
            $condition = new Condition();
            $condition->limit(null);
            $condition->addCondition('locale', 'locale', $semiLocale);
            if ($group !== null) {
                $condition->addCondition('group', 'group', $group);
            }
            $condition->addCondition('items', 'item', $mainTranslations->pluck('item')->toArray());
            $semiTranslations = $this->translationRepository->findAll($condition);
        } else {
            $semiTranslations = Collection::make();
        }
        
        return $this->combineToForm($mainTranslations, $semiTranslations);
    }

    /**
     * Комбинирует 2 пачки переводов в подходящий массив для формы
     *
     * @param $mainTranslations
     * @param $semiTranslations
     * @return array
     */
    private function combineToForm(Collection $mainTranslations, Collection $semiTranslations)
    {
        if ($semiTranslations->isEmpty()) {
            return $this->makeSingleToForm($mainTranslations);
        }

        $trans1 = $mainTranslations->keyBy('id');
        $trans2 = $semiTranslations->keyBy('id');

        $combined = [];

        foreach ($trans1 as $key => $item) {

            $semi = $this->findInCollection($item['group'], $item['item'], $trans2);

            $combined[] = [
                'mainId' => $item['id'],
                'semiId' => $semi ? $semi['id'] : null,
                'mainLocale' => $item['locale'],
                'semiLocale' => $semi ? $semi['locale'] : null,
                'group' => $item['group'],
                'constant' => $item['item'],
                'mainTranslate' => $item['text'],
                'semiTranslate' => $semi ? $semi['text'] : null,
            ];
        }

        return $combined;
    }

    /**
     * Возвращает набор данных удобных для формы всего одного языка
     *
     * @param Collection $mainTranslations
     * @return array
     */
    private function makeSingleToForm(Collection $mainTranslations)
    {
        $trans1 = $mainTranslations->keyBy('id');

        $result = [];

        foreach ($trans1 as $key => $item) {

            $result[] = [
                'mainId' => $item['id'],
                'semiId' => null,
                'mainLocale' => $item['locale'],
                'semiLocale' => null,
                'group' => $item['group'],
                'constant' => $item['item'],
                'mainTranslate' => $item['text'],
                'semiTranslate' => null,
            ];
        }

        return $result;
    }

    /**
     * Находит в коллекции $translations запись у которой group = $group и item = $item
     *
     * @param $group
     * @param $item
     * @param Collection $translations
     * @return mixed
     */
    private function findInCollection($group, $item, Collection $translations)
    {
        return $translations->where('group', $group)->where('item', $item)->first();
    }
}