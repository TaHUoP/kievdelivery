<?php

namespace App\Services;

use Api\App\Contracts\Services\ContactsService as ContactsServiceInterface;
use Config;
use Mail;

/**
 * Contacts Service
 * @package App\Services
 */
class ContactsService implements ContactsServiceInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function sendContacts(array $data)
    {
        $supportEmails = explode(',', Config::get('app.support_email'));

        Mail::onQueue('from_contacts_to_support', 'emails.from_contacts_to_support', ['request' => $data], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            foreach ($supportEmails as $key => $supportEmail) {
                if ($key === 0) {
                    $m->to($supportEmail)->subject('Запрос "Обратная связь"');
                } else {
                    $m->to($supportEmail);
                }
            }
        });

        return true;
    }
}