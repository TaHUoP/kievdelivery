<?php

namespace App\Services;

use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Services\Files\ImageApi;
use App\Models\Content;
use Exception;
use Auth;
use DB;

/**
 * Content Service
 * @package App\Services
 */
class ContentService
{
    /**
     * @var ContentInterface
     */
    protected $contentRepository;

    /**
     * @param ContentInterface $ContentRepository
     */
    public function __construct(ContentInterface $ContentRepository)
    {
        $this->contentRepository = $ContentRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Content|null $model
     * @return bool
     */
    public function save(array $data, Content $model = null)
    {
        DB::beginTransaction();

        if ($this->contentRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Content $model
     * @return bool
     */
    public function delete(Content $model)
    {
        DB::beginTransaction();

        if ($this->contentRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Загрузка
     *
     * @param UploadedFile $file
     * @return bool
     */
    public function upload(UploadedFile $file)
    {
        $imageName = uniqid('', true) . '.' . $file->extension();
        $imagePathLogo = public_path() . '/upload/logo.png';
        $imageUrl =  '/upload/content/' . date('Y-m');
        $imagePath = public_path() . $imageUrl;

        try {

            (new ImageApi($file))
                ->resize(1200, null)
                //->insert($imagePathLogo, 'bottom-right')
                ->save($imagePath . DIRECTORY_SEPARATOR . $imageName, 100);

            return $imageUrl . DIRECTORY_SEPARATOR . $imageName;

        } catch (Exception $e) {
            // $e->getMessage();
            return false;
        }
    }

}