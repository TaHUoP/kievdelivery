<?php

namespace App\Services\Payments;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Facades\Shop;
use App\Models\Shop\Checkout;
use Illuminate\Contracts\Mail\Mailer;
use Exception;
use Config;
use DB;
use Translate;

/**
 * Way For Pay Service
 * @package App\Services\Payments
 */
class MulticardsService
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @var WayForPay
     */
    protected $wayForPay;

    /**
     * @var array
     *
     */
    protected $config = [];

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param CartInterface $cartRepository
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        CartInterface $cartRepository,
        Mailer $mailer
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->cartRepository = $cartRepository;
        $this->config =  config('laravel-omnipay.gateways.way_for_pay.options');
        $this->wayForPay = new WayForPay($this->config['merchant_account'], $this->config['merchant_key']);
        $this->mailer = $mailer;
    }

    /**
     * @param Checkout $checkout
     * @return array
     */
    public function getDataForFormProducts(Checkout $checkout){

        if($checkout->cart->discount > 0){
            $interest_discount = round($checkout->cart->discount/($checkout->cart->total_price/100), 2);

            foreach($checkout->cart->products as $product){
                $product['amount'] =  $product->amount;
                $product['price'] =  round((Shop::getPrice($product->product, false)) * ((100 - $interest_discount) / 100) , 2);
                $product['name_en'] =  $product->product->translations[1]->name;
                $products[] = $product;
            }

        }
        else{
            foreach($checkout->cart->products as $product){
                $product['amount'] =  $product->amount;
                $product['price'] =  round(Shop::getPrice($product->product, false),2);
                $product['name_en'] = Translate::get($product->product->translations, 'name');;
                $products[] = $product;
            }
        }

        $deliveryPrice = $checkout->delivery->delivery_price;

        if($deliveryPrice) {
            $delivery['amount'] =  1;
            $delivery['price'] =  round ($deliveryPrice,2);
            $delivery['name_en'] =  trans('app.Delivery');
            $products[] = $delivery;
        }

        return $products;
    }


    /**
     * @param Checkout $checkout
     * @return array
     */
    public function getDataForFormCustomer(Checkout $checkout){

        $customer['name'] = $checkout->first_name." ".$checkout->last_name;
        $customer['email'] = $checkout->email;
        $customer['address'] = $checkout->address;
        $customer['phone'] = $checkout->phone;
        $customer['city'] = $checkout->delivery->city;

        return $customer;
    }

    /**
     * @param Checkout $checkout
     * @return array
     */
    public function updateStatus($order, $checkout){

       DB::table('shop_checkout')
            ->where('id', $order)
            ->update([
                'paid' => 1
            ]);



        $emailTo = $checkout->email;

        if ($checkout->user) {
            $emailTo = $checkout->user->email;
        }

        $supportEmails = explode(',', Config::get('app.support_email'));
        $supportEmails[] = $emailTo;


        $this->mailer->queue('emails.shop.checkout_payment_to_user', ['checkout' => $checkout], function ($m) use ($supportEmails) {
            $m->from(Config::get('app.email'), Config::get('app.name'));
            $m->to($supportEmails)->subject(trans('email.new_order'));
        });

        return true;
    }



}