<?php

namespace App\Services\Payments;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use App\Models\Shop\Checkout;
use Illuminate\Support\Facades\Log;
use Omnipay\Omnipay;
use Exception;
use Config;
use DB;

/**
 * PayPal
 * @package App\Services\Payments
 */
class PayPalService
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var mixed
     */
    protected $payPal;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param CheckoutInterface $checkoutRepository
     */
    public function __construct(
        CheckoutInterface $checkoutRepository
    )
    {
        $this->config = config('laravel-omnipay.gateways.paypal');

        $this->payPal = Omnipay::create($this->config['driver']);
        //$this->payPal->setUsername($this->config['options']['username']);
        //$this->payPal->setPassword($this->config['options']['password']);
        //$this->payPal->setSignature($this->config['options']['signature']);
        //$this->payPal->setTestMode($this->config['options']['testMode']);

        $this->payPal->initialize([
            'clientId' => $this->config['options']['clientId'],
            'secret'   => $this->config['options']['secret'],
            'testMode' => $this->config['options']['testMode'], // Or false when you are ready for live transactions
        ]);

        $this->checkoutRepository = $checkoutRepository;
    }

    /**
     * Генерация Redirect Url
     *
     * @param Checkout $checkout
     * @return mixed
     * @throws Exception
     */
    public function redirect(Checkout $checkout)
    {
        $deliveryPrice = $checkout->delivery->delivery_price;

        if($checkout->cart->total_price > Config::get('static.deliveryPriceBorder')) {
            $deliveryPrice = \App\Helpers\StaticsHelper::getCityFee();
        }

        $response = $this->payPal->purchase([
            'amount' => round(($checkout->cart->total_price + $deliveryPrice) - $checkout->cart->discount, 2),
            'currency' => $this->config['options']['currency'],
            'description' => trans('app.paypal_message'),
            'returnUrl' => route('paypal.success', ['checkout_id' => $checkout->id]),
            'cancelUrl' => route('paypal.cancel', ['checkout_id' => $checkout->id]),
        ])->send();

        if (!$response->isRedirect()) {
            Log::error($response->getData());
            throw new Exception("Couldn't create a redirect url");
        }

        return $response->getRedirectUrl();
    }

    /**
     * Просмотр инфомрации о транзакции
     *
     * @param $paymentId
     * @param $payerID
     * @return mixed
     */
    public function showPaymentDetails($paymentId, $payerID)
    {
        $response = $this->payPal->fetchPurchase([
            'payer_id' => $payerID,
            'transactionReference' => $paymentId,
        ])->send();

        if ($response->isSuccessful()) {
            return $response->getData();
        }

        return false;
    }

    /**
     * Завершить транзакцию
     *
     * @param $paymentId
     * @param $payerID
     * @return bool
     */
    public function completePayment($paymentId, $payerID)
    {
        $transaction = $this->payPal->completePurchase([
            'payer_id' => $payerID,
            'transactionReference' => $paymentId,
        ]);
        $response = $transaction->send();

        return $response->isSuccessful();
    }
}