<?php

namespace App\Services\Payments;


use App\Helpers\CurrencyHelper;
use App\Models\Shop\Checkout;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;

class PlatonService
{
    const ACCEPTED_RESULT = 'ACCEPTED';
    const SUCCESS_RESULT = 'SUCCESS';

    const ALREADY_EXISTS_MESSAGE = 'Order already exists';
    const DUPLICATE_REQUEST_MESSAGE = 'Duplicate request';
    const INCORRECT_HASH = 'Incorrect hash';
    const RECURRING_NOT_SUPPORTED = 'Recurring not supported';

    const SALE_ACTION = 'SALE';
    const GET_TRANS_DETAILS_ACTION = 'GET_TRANS_DETAILS';
    const REFUND_ACTION = 'CREDITVOID';

    const PAYMENT_TYPE_CC = 'CC';
    const PAYMENT_TYPE_CCT = 'CCT';

    private $platon_form_url;
    private $platon_api_url;
    private $callback_url;
    private $recurring_email;
    private $password;
    private $secret;

    /**
     * PlatonService constructor.
     */
    public function __construct()
    {
        $this->password = config('payment.methods.platon.password');
        $this->secret = config('payment.methods.platon.secret');
        $this->platon_form_url = config('payment.methods.platon.form_url');
        $this->platon_api_url = config('payment.methods.platon.api_url');
        $this->callback_url = route('platon.callback');
    }

    /**
     * Returns title for payment
     *
     * @param Checkout $checkout
     * @return string
     */
    public function getTitle(Checkout $checkout)
    {
        $text = App::getLocale() == 'en'
            ? "Payment order №: %d in the shop %s" : "Оплата заказа №: %d в магазине %s";

        return sprintf(
            $text,
            $checkout->id,
            url('/')
        );
    }

    /**
     * Creates "data" field for platon payment form
     *
     * @param Checkout $checkout
     * @return string
     */
    public function createData(Checkout $checkout)
    {
        return base64_encode(json_encode([
            'amount' => $this->formatAmount($checkout->total),
            'name' => $this->getTitle($checkout),
            'currency' => 'UAH'
        ]));
    }

    /**
     * Creates "sign" field for platon payment form
     *
     * @param Checkout $checkout
     * @param string|null $card_token
     * @return string
     */
    public function createSign(Checkout $checkout, string $card_token = null)
    {
        $payment_type = $card_token ? self::PAYMENT_TYPE_CCT : self::PAYMENT_TYPE_CC;

        return md5(strtoupper(implode([
            strrev($this->secret),
            strrev($payment_type),
            strrev($this->createData($checkout)),
            strrev($this->callback_url),
            strrev($card_token),
            strrev($this->password),
        ])));
    }

    /**
     * Creates "hash" field for platon actions related to existing transactions
     *
     * @param string $trans_id
     * @param string $card
     * @return string
     */
    public function createTransactionHash(string $trans_id, string $card)
    {
        return md5(strtoupper(implode([
            strrev($this->recurring_email),
            $this->password,
            $trans_id,
            strrev(implode([
                substr($card, 0, 6),
                substr($card, -4)
            ]))
        ])));
    }

    /**
     * Creates platon payment form html
     *
     * @param Checkout $checkout
     * @param string|null $lang
     * @param string|null $card_token
     * @return string
     */
    public function getFormHTML(Checkout $checkout, $lang = null, string $card_token = null)
    {
        $payment_data = [
            'order' => $checkout->id,
            'key' => $this->secret,
            'url' => $this->callback_url,
            'data' => $this->createData($checkout),
            'sign' => $this->createSign($checkout, $card_token),
            'lang' => $lang,
        ];

        if ($card_token) {
            $payment_data['card_token'] = $card_token;
            $payment_type = self::PAYMENT_TYPE_CCT;
        } else {
            $payment_data['req_token'] = 'Y';
            $payment_type = self::PAYMENT_TYPE_CC;
        }

        $payment_data['payment'] = $payment_type;

        $platon_form_url = $this->platon_form_url;

        return View::make('payment._platon-form', compact('payment_data', 'platon_form_url'))->render();
    }

    /**
     * Performs request for transaction details
     *
     * @param string $trans_id
     * @param string $card
     * @return array|false
     */
    public function getTransactionDetails(string $trans_id, string $card)
    {
        $params = [
            'action' => self::GET_TRANS_DETAILS_ACTION,
            'client_key' => $this->secret,
            'trans_id' => $trans_id,
            'hash' => $this->createTransactionHash($trans_id, $card),
        ];

        return $this->sendRequest($this->platon_api_url, $params);
    }

    /**
     * Send request to platon
     *
     * @param string $url
     * @param array $params
     * @return array|false
     */
    private function sendRequest(string $url, array $params)
    {
        $crq = curl_init();

        curl_setopt($crq, CURLOPT_URL, $url);
        curl_setopt($crq, CURLOPT_HEADER, 0);
        curl_setopt($crq, CURLOPT_POST, 1);
        curl_setopt($crq, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crq, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($crq, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($crq, CURLOPT_POSTFIELDS, http_build_query($params));

        $result = curl_exec($crq);

        $result = !curl_errno($crq) ? json_decode($result, true) : false;

        curl_close($crq);

        return $result;
    }

    /**
     * @param $amount
     * @return string
     */
    private function formatAmount($amount)
    {
        $currencyHelper = app()->make(CurrencyHelper::class);

        $currency_code = $currencyHelper->getUserCurrency();

        // Platon get amount only in UAH
        if ($currency_code !== 'UAH') {
            $amount = $amount * $currencyHelper->find('UAH')['exchange_rate'];
        }

        return number_format($amount, 2, ".", "");
    }
}