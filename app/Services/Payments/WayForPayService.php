<?php

namespace App\Services\Payments;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Facades\Shop;
use App\Services\Payments\WayForPay;
use App\Models\Shop\Checkout;
use Exception;
use Config;

/**
 * Way For Pay Service
 * @package App\Services\Payments
 */
class WayForPayService
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @var WayForPay
     */
    protected $wayForPay;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param CartInterface $cartRepository
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        CartInterface $cartRepository
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->cartRepository = $cartRepository;
        $this->config =  config('laravel-omnipay.gateways.way_for_pay.options');
        $this->wayForPay = new WayForPay($this->config['merchant_account'], $this->config['merchant_key']);
    }

    /**
     * @param Checkout $checkout
     * @return bool|string
     */
    public function getDataForPurchase(Checkout $checkout)
    {
        try {
            // $cart = $this->cartRepository->findById($checkout->cart_id);
            $cart = $checkout->cart;

            // Добавление продуктов
            $arrayAliases = ['_delivery'];
            $arrayAmounted = [1];
            $arrayPrices = [round($checkout->delivery->delivery_price, 2)];

            // TODO: раскомментировать, если нужно убрать цену за доставку
            /*$arrayAliases = [];
            $arrayAmounted = [];
            $arrayPrices = [];*/

            foreach ($cart->products as $product) {
                $arrayAliases[] = $product->product->alias;
                $arrayAmounted[] = (int)$product->amount;
                $arrayPrices[] = round(Shop::getPrice($product->product, false), 2);
            }

            $deliveryPrice = $checkout->delivery->delivery_price;

            if($cart->total_price > Config::get('static.deliveryPriceBorder')) {
                $deliveryPrice = 0;
            }

            $totalPrice = round(($cart->total_price + $deliveryPrice) - $cart->discount, 2);
            //$totalPrice = round($cart->total_price, 2); // TODO: раскомментировать, если нужно убрать цену за доставку

            return $this->wayForPay->buildForm([
                'merchantDomainName' => $this->config['merchant_domain_name'],
                'orderReference' => md5(uniqid('', true)) . '_' . $checkout->id,
                'orderDate' => strtotime(date('Y-m-d H:i:s')),
                'currency' => $this->config['currency'],
                'amount' => round($totalPrice, 2),
                'productName' => $arrayAliases,
                'productCount' => $arrayAmounted,
                'productPrice' =>  $arrayPrices,
                'merchantTransactionSecureType' => 'AUTO',
                'returnUrl' => route('wayforpay.returnUrl'),
                'serviceUrl' => route('wayforpay.serviceUrl'),
                'clientEmail' => !empty($checkout->email) ? $checkout->email : null,
                'clientPhone' => !empty($checkout->phone) ? $checkout->phone : null,
            ]);

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $orderId
     * @return bool|mixed
     */
    public function checkStatus($orderId)
    {
        try {

            return $this->wayForPay->checkStatus([
                'merchantAccount' => $this->config['merchant_account'],
                'orderReference' => $orderId,
            ]);

        } catch (Exception $e) {
            return false;
        }
    }

    /*
     * @param array $result
     * @param string $status
     *
     * @return bool|string
     */
    public function getResponceHash(array $result, $status = 'accept')
    {
        try {

            return hash_hmac('md5', implode(';', [
                'orderReference' => $result['orderReference'],
                'status' => $status,
                'time' => $result['processingDate'],
                'signature' => $result['merchantSignature'],
            ]), $this->config['merchant_key']);

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Получить модель заказа в случае успешной верификации оплаты
     *
     * @param array $data
     * @return mixed|null
     */
    public function findModelByApprovedStatus(array $data)
    {
        if (isset($data['transactionStatus']) && $data['transactionStatus'] === 'Approved') {
            $orderHash = explode('_', $data['orderReference']);
            $orderId = isset($orderHash[1]) ? (int)$orderHash[1] : null;
            if ($model = $this->checkoutRepository->findById($orderId)) {
                return $model;
            }
        }
        return null;
    }

}