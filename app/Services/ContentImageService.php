<?php

namespace App\Services;

use Api\App\Contracts\Entities\ContentImageRepository as ContentImageInterface;
use App\Services\Files\ImageApi;
use App\Models\ContentImage;
use Exception;
use Auth;
use File;
use DB;

/**
 * Content Image Service
 * @package App\Services
 */
class ContentImageService
{
    private $contentId;
    private $path;
    private $imageThumbsPath;
    private $imagePathLogo;

    /**
     * @param ContentImageInterface $contentImageRepository
     */
    public function __construct(ContentImageInterface $contentImageRepository)
    {
        $this->contentImageRepository = $contentImageRepository;
        $this->path = '/upload/media_content/images/';
        $this->imageThumbsPath = '/thumbs';
        $this->imagePathLogo = '/upload/logo.png';
    }

    /**
     * @param $contentId
     * @return $this
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;
        return $this;
    }

    /**
     * Загрузка
     *
     * @param $file
     * @return bool
     */
    public function upload($file)
    {
        $imageName = uniqid('', true) . '.' . $file->extension();

        $imagePathLogo = public_path() . $this->imagePathLogo;
        $imagePath = public_path() . $this->path . date('Y-m');
        $imageThumbsPath = $imagePath . $this->imageThumbsPath;

        try {
            (new ImageApi($file))
                ->resize(800, null)
                //->insert($imagePathLogo, 'bottom-right')
                ->save($imagePath . DIRECTORY_SEPARATOR . $imageName, 100)
                ->resize(300, null)
                ->save($imageThumbsPath . DIRECTORY_SEPARATOR . $imageName, 80);

            $data = [
                'preview_url' => $imageName,
                'review_url' => $imageName,
                'content_id' => $this->contentId
            ];

            return $this->contentImageRepository->save($data);

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param ContentImage|null $model
     * @return bool
     */
    public function save(array $data, ContentImage $model = null)
    {
        DB::beginTransaction();

        if ($this->contentImageRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param ContentImage $model
     * @return bool
     */
    public function delete(ContentImage $model)
    {
        DB::beginTransaction();

        if ($this->contentImageRepository->delete($model)) {

            // TODO: удалить файлы

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }
}