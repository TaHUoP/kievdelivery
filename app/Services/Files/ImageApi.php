<?php

namespace App\Services\Files;

use Api\App\Contracts\Files\ImageApi as ImageApiInterface;
use Image;
use File;

/**
 * Image Api
 * @package App\Services\Files
 */
class ImageApi implements ImageApiInterface
{
    /**
     * UploadedFile
     */
    protected $file;

    /**
     * Image
     */
    protected $image;

    /**
     * Image Api
     *
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
        $this->image = Image::make($this->file);
    }

    /**
     * Resize
     *
     * @param $width
     * @param $height
     * @return $this
     */
    public function resize($width, $height)
    {
        if (!is_null($width) || !is_null($height)) {
            $this->image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        return $this;
    }

    /**
     * Insert
     *
     * @param $source
     * @param string $position
     * @param int $x
     * @param int $y
     * @return $this
     */
    public function insert($source, $position = 'top-left', $x = 0, $y = 0)
    {
        if (!is_null($source)) {
            $this->image->insert($source, $position, $x, $y);
        }
        return $this;
    }

    /**
     * Save
     *
     * @param $path
     * @param $quality
     * @return $this
     */
    public function save($path, $quality)
    {
        $pathArray = explode(DIRECTORY_SEPARATOR, $path);
        array_pop($pathArray);
        $pathToDir = implode(DIRECTORY_SEPARATOR, $pathArray);

        if (!File::isDirectory($pathToDir)) {
            File::makeDirectory($pathToDir, 0777, true, true);
            @chmod($pathToDir, 0777 & ~umask());
        }

        $this->image->save($path, $quality);

        return $this;
    }
}