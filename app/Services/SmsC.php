<?php

namespace App\Services;


use App\Models\Shop\Checkout;
use GuzzleHttp\Client;

class SmsC
{
    /**
     * https://smsc.ua/api/http/
     *
     * @param Checkout $checkout
     * @return bool
     */
    static public function sendSmsOrderPaid(Checkout $checkout)
    {
        return self::sendSms($checkout, trans('sms.order_paid',
            ['name' => $checkout->first_name, 'order_id' => $checkout->id]));
    }

    static public function sendSmsOrderDelivered(Checkout $checkout)
    {
        return self::sendSms($checkout, trans('sms.order_delivered',
            ['name' => $checkout->first_name, 'order_id' => $checkout->id]));
    }

    static public function sendSmsOrderCancelled(Checkout $checkout)
    {
        return self::sendSms($checkout, trans('sms.order_cancelled',
            ['name' => $checkout->first_name, 'order_id' => $checkout->id]));
    }

    static private function sendSms(Checkout $checkout, $text = '')
    {
        $client = new Client(['base_uri' => config('smsc.url')]);

        try {
            $response = $client->request('GET', 'sys/send.php', [
                'query' => [
                    'login' => config('smsc.login'),
                    'psw' => config('smsc.password'),
                    'phones' => str_replace('+', '', $checkout->phone),
                    'mes' => $text,
                    'sender' => 'Kievdeliver'
                ]
            ]);
        } catch (\Exception $e) {
            error_log('Sms error: ' . $e->getMessage());

            return false;
        }

        info('SMS response: ' . $response->getBody()->getContents());

        return true;
    }
}