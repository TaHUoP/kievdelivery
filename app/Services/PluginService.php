<?php

namespace App\Services;

use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use Illuminate\Support\Facades\Hash;
use App\Models\Plugin;
use Auth;
use DB;

/**
 * Plugin Service
 * @package App\Services
 */
class PluginService
{
    /**
     * @var PluginInterface
     */
    protected $pluginRepository;

    /**
     * @param PluginInterface $pluginRepository
     */
    public function __construct(PluginInterface $pluginRepository)
    {
        $this->pluginRepository = $pluginRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Plugin|null $model
     * @return bool
     */
    public function save(array $data, Plugin $model = null)
    {
        if ($this->pluginRepository->save($data, $model)) {
            return true;
        }

        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Plugin $model
     * @return bool
     */
    public function delete(Plugin $model)
    {
        if ($this->pluginRepository->delete($model)) {
            return true;
        }

        return false;
    }

}