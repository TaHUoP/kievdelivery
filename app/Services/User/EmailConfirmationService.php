<?php

namespace App\Services\User;

use Api\User\Contracts\Entities\UserRepository as UserRepositoryInterface;
use Illuminate\Contracts\Mail\Mailer;
use App\Models\User\User;

/**
 * Email Confirmation Service
 * @package App\Services\User
 */
class EmailConfirmationService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, Mailer $mailer)
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    /**
     * Sets the user by given User model to be confirmed and sends email with code
     *
     * @param User $user
     */
    public function setUserToBeConfirmed(User $user)
    {
        $data = [
            'status' => User::STATUS_INACTIVE,
            'confirmation_code' => str_random(30),
        ];

        $this->userRepository->save($data, $user);

        $this->sendNewConfirmationEmail($user);
    }

    /**
     * Отправляет пользователю новый код активации аккаунта
     *
     * @param $user
     */
    public function sendNewConfirmationEmail($user)
    {
        $this->mailer->queue('emails.new_confirmation_code', ['user' => $user], function ($m) use ($user) {
            $m->from(config('app.email'), config('app.name'));
            $m->to($user->email)->subject(trans('email.theme.new-confirmation', ['appname' => config('app.name')]));
        });
    }
}