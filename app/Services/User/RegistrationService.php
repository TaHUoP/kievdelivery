<?php

namespace App\Services\User;

use Api\User\Contracts\Entities\UserRepository as UserRepositoryInterface;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use App\Http\Requests\User\SignupRequest;
use Illuminate\Contracts\Mail\Mailer;
use App\Exceptions\AlertException;
use App\Events\UserSignup;
use Event;

/**
 * Registration Service
 * @package App\Services\User
 */
class RegistrationService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @var HasherContract
     */
    protected $hasher;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param Mailer $mailer
     * @param HasherContract $hasher
     */
    public function __construct(UserRepositoryInterface $userRepository, Mailer $mailer, HasherContract $hasher)
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
        $this->hasher = $hasher;
    }

    /**
     * @param SignupRequest $userRequest
     * @return bool
     * @throws AlertException
     */
    public function register(SignupRequest $userRequest)
    {
        $data = $userRequest->input();
        $data['password'] = $this->hasher->make($data['password']);
        $data['confirmation_code'] = str_random(30);

        if (!$this->userRepository->save($data)) {
            throw new AlertException(trans('app.critical_error'));
        }

        // TODO: рефакторинг
        $user = $this->userRepository->findByEmail($data['email']);

        if ($user) {
            Event::fire(new UserSignup($user));
        }

        return true;
    }

    /**
     * @param $confirmation_code
     * @return mixed
     */
    public function confirmEmail($confirmation_code)
    {
        if (!$user = $this->userRepository->findByConfirmationCode($confirmation_code)) {
            abort(404);
        }

        $data = [
            'status' => 1,
            'confirmation_code' => null
        ];

        return $this->userRepository->save($data, $user);
    }
}