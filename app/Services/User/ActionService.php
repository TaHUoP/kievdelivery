<?php

namespace App\Services\User;

use Api\User\Contracts\Entities\ActionRepository as ActionInterface;
use App\Models\User\Action;
use Auth;
use DB;

/**
 * Action Service
 * @package App\Services\User
 */
class ActionService
{
    /**
     * @var ActionInterface
     */
    protected $actionRepository;

    /**
     * @param ActionInterface $actionRepository
     */
    public function __construct(ActionInterface $actionRepository)
    {
        $this->actionRepository = $actionRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Action|null $model
     * @return bool
     */
    public function save(array $data, Action $model = null)
    {
        DB::beginTransaction();

        if ($this->actionRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}