<?php

namespace App\Services\User;

use Api\User\Contracts\Entities\UserRepository as UserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests\User\Backend\UserRequest;
use App\Services\Files\ImageApi;
use App\Models\User\User;
use Exception;
use Config;
use File;
use DB;

/**
 * User Service
 * @package App\Services\User
 */
class UserService
{
    /**
     * @var UserInterface
     */
    protected $userRepository;

    /**
     * @param UserInterface $userRepository
     */
    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param User|null $model
     * @return bool
     */
    public function save(array $data, User $model = null)
    {
        DB::beginTransaction();

        if ($this->userRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Создает пользователя
     *
     * @param UserRequest $request
     * @return bool
     */
    public function create(UserRequest $request)
    {
        DB::beginTransaction();

        try {

            $data = $request->input();

            if ($request->hasFile('avatar')) {
                $data['avatar_url'] = $this->uploadAvatar($request->file('avatar'));
            }

            $data['password'] = bcrypt($request->input('password'));

            if ($this->userRepository->save($data)) {
                DB::commit();
                return true;
            }

            new Exception();

        } catch (\Exception $e) {
            DB::rollBack();
        }

        return false;
    }

    /**
     * Изменяет сущность юзера заданными данными
     *
     * @param UserRequest $request
     * @param User $model
     * @return bool
     */
    public function update(UserRequest $request, User $model)
    {
        DB::beginTransaction();

        try {

            $data = $request->input();

            if ($request->hasFile('avatar')) {
                if (!empty($model->profile->avatar_url)) {
                    $this->deleteOldAvatar($model->profile->avatar_url);
                }
                $data['avatar_url'] = $this->uploadAvatar($request->file('avatar'));
            }

            if ($request->input('new_password')) {
                $data['password'] = bcrypt($request->input('new_password'));
            }

            if ($this->userRepository->save($data, $model)) {
                DB::commit();
                return true;
            }

            new Exception();

        } catch (\Exception $e) {
            DB::rollBack();
        }

        return false;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function uploadAvatar(UploadedFile $file)
    {
        $imageName = uniqid('', true) . '.' . $file->extension();
        $imagePath = public_path() . DIRECTORY_SEPARATOR .  Config::get('app.avatars_folder');
        $imageThumbsPath = $imagePath . '/thumbs';

        (new ImageApi($file))
            ->resize(800, null)
            ->save($imagePath . DIRECTORY_SEPARATOR . $imageName, 100)
            ->resize(300, null)
            ->save($imageThumbsPath . DIRECTORY_SEPARATOR . $imageName, 80);

        return $imageName;
    }

    /**
     * Delete Old Avatar
     *
     * @param $fileName
     */
    public function deleteOldAvatar($fileName)
    {
        $filePath = public_path() . DIRECTORY_SEPARATOR .  Config::get('app.avatars_folder');
        $bigName = $filePath . '/' . $fileName;

        if (File::exists($bigName)) {
            File::delete($bigName);
        }

        $thumbName = $filePath . '/thumbs/' . $fileName;

        if (File::exists($thumbName)) {
            File::delete($thumbName);
        }
    }
}