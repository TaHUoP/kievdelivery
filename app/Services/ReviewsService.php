<?php

namespace App\Services;

use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use App\Services\Shop\ProductReviewsService;
use Illuminate\Support\Facades\Auth;
use App\Models\Review;
use DB;

/**
 * Reviews Service
 * @package App\Services
 */
class ReviewsService
{
    /**
     * @var ProductReviewsService
     */
    protected $productReviewsService;

    /**
     * @var ReviewInterface
     */
    protected $reviewRepository;

    /**
     * @param ReviewInterface $reviewRepository
     * @param ProductReviewsService $productReviewsService
     */
    public function __construct(
        ReviewInterface $reviewRepository,
        ProductReviewsService $productReviewsService
    )
    {
        $this->reviewRepository = $reviewRepository;
        $this->productReviewsService = $productReviewsService;
    }

    /**
     * Сохранение отзыва для продукта
     *
     * @param array $data
     * @param Review|null $model
     * @return bool
     */
    public function save(array $data, Review $model = null)
    {
        DB::beginTransaction();

        if ($this->reviewRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Сохранение главного отзыва
     *
     * @param array $data
     * @param Review|null $model
     * @return bool
     */
    public function saveMain(array $data, Review $model = null)
    {
        DB::beginTransaction();

        $data['type'] = 'main';

        if ($this->reviewRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Сохранить отзыв от Администратора
     * @param array $data
     * @param Review $model
     * @return bool
     */
    public function saveFromBack(array $data, Review $model = null)
    {
        DB::beginTransaction();

        if ($this->reviewRepository->save($data, $model)) {

            if ($data['confirmed'] == 1){
                if ($data['product_id']){
                    $this->productReviewsService->productReviewUpdate($data['product_id']);
                }
            }

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Review $model
     * @return bool
     */
    public function delete(Review $model)
    {
        DB::beginTransaction();

        if ($this->reviewRepository->delete($model)) {

            if ($model->confirmed == 1){
                $this->productReviewsService->productReviewUpdate($model->product_id);
            }

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}