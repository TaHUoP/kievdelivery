<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\ProductFilesRepository as ProductFilesInterface;
use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\Shop\Contracts\Entities\ValueRepository as ValueInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Helpers\ShopHelper as ShopHelperInterface;
use Api\App\Storage\Condition;
use App\Http\Requests\Request;
use App\Models\Shop\Product;
use App\Models\Shop\ProductRelation;
use Cookie;
use Auth;
use DB;

/**
 * Product Service
 * @package App\Services\Shop
 */
class ProductService
{
    /**
     * @var CategoryInterface
     */
    protected $categoryRepository;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @var ValueInterface
     */
    protected $valueRepository;

    /**
     * @var ShopHelperInterface
     */
    protected $shopHelper;

    /**
     * @var ProductFilesInterface
     */
    protected $productFilesRepository;

    /**
     * @param ProductInterface $productRepository
     * @param ShopHelperInterface $shopHelper
     * @param TranslateHelperInterface $translateHelper
     * @param CategoryInterface $categoryRepository
     * @param PropertyInterface $propertyRepository
     * @param ValueInterface $valueRepository
     * @param ProductFilesInterface $productFilesRepository
     */
    public function __construct(
        ProductInterface $productRepository,
        ShopHelperInterface $shopHelper,
        TranslateHelperInterface $translateHelper,
        CategoryInterface $categoryRepository,
        PropertyInterface $propertyRepository,
        ValueInterface $valueRepository,
        ProductFilesInterface $productFilesRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->valueRepository = $valueRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->translateHelper = $translateHelper;
        $this->shopHelper = $shopHelper;
        $this->productFilesRepository = $productFilesRepository;
    }

    /**
     * Дополнительные условия фильтра
     *
     * Добавляет условие к обьекту $condition количество продуктов на странице и сортировку по цене
     *
     * @param $filter
     * @param ConditionInterface $condition
     * @return ConditionInterface
     */
    public function extraFilterForCondition($filter, ConditionInterface $condition)
    {
        // Лимит
        $accessLimit = [4, 16, 32, 52];
        $defaultLimit = 16;
        if (isset($filter['count'][0])) {
            $limit = (int)$filter['count'][0];
            if (!in_array($limit, $accessLimit)) {
                $limit = $defaultLimit;
            }
            Cookie::queue(Cookie::forever('products_count', $limit));
        } else {
            $limit = (int)Cookie::get('products_count');
            if (!in_array($limit, $accessLimit)) {
                $limit = $defaultLimit;
            }
        }
        $condition->limit($limit);

        // Сортировка
        if (isset($filter['sort'][0])) {
            if ($filter['sort'][0] == 'price_desc') {
                $condition->orderBy(['price' => SORT_DESC]);
            } else {
                $condition->orderBy(['price' => SORT_ASC]);
            }
        }

        return $condition;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Product $product
     * @return bool
     */
    public function save(array $data, Product $product = null)
    {
        DB::beginTransaction();

        if (isset($data['categories'])) {

            $uniqueCategoryIds = [];

            foreach ($data['categories'] as $cat_id) {

                $uniqueCategoryIds = array_merge(
                    $this->categoryRepository->findAncestorsByNodeId($cat_id)
                        ->pluck('id')
                        ->toArray(),
                    $uniqueCategoryIds);

            }

            $uniqueCategoryIds = array_merge($data['categories'], $uniqueCategoryIds);
            if ($uniqueCategoryIds)
                $uniqueCategoryIds = array_unique($uniqueCategoryIds);

            $condition = new Condition();
            $condition->addCondition('ids', 'id', $uniqueCategoryIds);
            $data['categories'] = $this->categoryRepository->findAll($condition);


        } else {
            $data['categories'] = null;
        }

        // --------------------------------------------------------

        if ($productId = $this->productRepository->save($data, $product)) {
            DB::commit();
            return $productId;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Product $product
     * @return bool
     */
    public function delete(Product $product)
    {
        DB::beginTransaction();

        if ($this->productRepository->delete($product)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Комбинирует фильтр категорий
     *
     * @param \Kalnoy\Nestedset\Collection $categories
     * @return array
     */
    public function combineFilterByCategories(\Kalnoy\Nestedset\Collection $categories)
    {
        $result = [
            'categoriesString' => null,
            'categories' => [],
            'properties' => [],
        ];

        foreach ($categories as $category) {

            if (!isset($result['categories'][$category->id])) {
                $result['categories'][$category->id] = $category;
                $result['categoriesString'][] = $this->translateHelper->get($category->translations, 'name');
            }

            if (!isset($category->properties) || empty($category->properties)) {
                continue;
            }

            foreach ($category->properties as $property) {
                if (!isset($result['properties'][$property->id])) {
                    $result['properties'][$property->id] = $property;
                }
            }
        }

        if (is_array($result['categoriesString'])) {
            $result['categoriesString'] = implode(', ', $result['categoriesString']);
        } else {
            $result['categoriesString'] = trans('app.no-info');
        }

        return $result;
    }

    /**
     * Сохранить данные фильтра
     *
     * @param Product $product
     * @param array $data
     * @return bool
     */
    public function saveFilter(Product $product, array $data)
    {
        DB::beginTransaction();

        $data['properties'] = [];

        $propertyId = key($data);
        $valueId = $data[$propertyId];

        if ($property = $this->propertyRepository->findById($propertyId)) {
            $value = $this->valueRepository->findById($valueId);
            $data['properties'][] = [
                'product_id' => $product->id,
                'property_id' => $property->id,
                'value_id' => empty($value) ? null : $value->id,
                'custom_value' => null,
                'custom_value_type' => null,
                'order' => 0,
            ];
            if ($this->productRepository->save($data, $product)) {
                DB::commit();
                return true;
            }
        }

        DB::rollBack();
        return false;
    }

    /**
     * @param Product $product
     * @param Request $request
     * @return bool
     */
    public function saveFilterParam(Product $product, Request $request): bool
    {
        DB::beginTransaction();

        $productPropertyValue = ProductRelation::where("product_id", $product->id)
                            ->where("property_id", $request->get("property_id"))
                            ->where("value_id", $request->get("value_id"))->get()->first();
        if ($productPropertyValue) {
            $productPropertyValue->delete();
            DB::commit();
            return true;
        } else {
            $productPropertyValue = new ProductRelation();
            $productPropertyValue->product_id = $product->id;
            $productPropertyValue->property_id = $request->get("property_id");
            $productPropertyValue->value_id = $request->get("value_id");
            $productPropertyValue->order = 0;
            $productPropertyValue->save();
            DB::commit();
            return true;
        }
        DB::rollBack();
        return false;
    }

    /**
     * Редактирует файлы к товару
     *
     * @param array $files
     * @param Product $product
     * @return bool
     * @throws \Exception
     */
    public function updateProductFiles(array $files = [], Product $product)
    {
        DB::beginTransaction();

        try {

            // Массив файлов с индексом Id
            $productFiles = $product->files->keyBy('id');

            // ID, файлов из БД
            $oldIds = $productFiles->keys()->all();

            // ID, файлов которые остались и пришли из Формы
            $ids = array_pluck($files, 'product_file_id');

            // Разница массивов
            $toDeleteIds = array_diff($oldIds, $ids);

            // Удалить ненужные
            $this->deleteDeletedFilesFromProduct($toDeleteIds, $product);

            // Создать новые или изменить существующие
            foreach ($files as $file) {
                if (empty($file['product_file_id']) && empty($file['name']) && empty($file['url'])) {
                    continue;
                }

                if (!empty($file['product_file_id'])) {
                    $this->productFilesRepository->save($file, $productFiles->get($file['product_file_id']));
                } else {
                    $file['product_id'] = $product->id;
                    $this->productFilesRepository->save($file);
                }
            }

            DB::commit();

        } catch(\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Удаляет Файлы с Продукта
     *
     * @param array $toDeleteIds
     * @param Product $product
     */
    protected function deleteDeletedFilesFromProduct(array $toDeleteIds = [], Product $product)
    {
        foreach ($toDeleteIds as $toDeleteId) {
            $key = $product->files->search(function ($file, $key) use ($toDeleteId) {
                return $file->id == $toDeleteId;
            });

            if ($key !== false) {
                $this->productFilesRepository->delete($product->files[$key]);
            }
        }
    }

    /**
     * Обработка фильтра для Condition
     *
     * @param Condition $condition
     * @param array $filters
     * @return Condition
     */
    public function createConditionForFilter(Condition $condition, array $filters = [])
    {
        foreach ($filters as $key => $filter) {
            if ($key == 'visible') {
                $condition->scopes(['visible' => 1]);
            } else {
                $condition->addCondition($key, $key, $filter);
            }
        }
        return $condition;
    }
}