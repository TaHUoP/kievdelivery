<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\ValueRepository as ValueInterface;
use App\Models\Shop\Value;
use Auth;
use DB;

/**
 * Value Service
 * @package App\Services\Shop
 */
class ValueService
{
    /**
     * @var ValueInterface
     */
    protected $valueRepository;

    /**
     * @param ValueInterface $valueRepository
     */
    public function __construct(ValueInterface $valueRepository)
    {
        $this->valueRepository = $valueRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Value|null $model
     * @return bool
     */
    public function save(array $data, Value $model = null)
    {
        DB::beginTransaction();

        if ($this->valueRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Value|null $model
     * @return bool
     */
    public function delete(Value $model = null)
    {
        DB::beginTransaction();

        if ($this->valueRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}