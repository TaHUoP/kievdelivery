<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use App\Models\Shop\Coupon;
use Auth;
use DB;

/**
 * Coupon Service
 * @package App\Services\Shop
 */
class CouponService
{
    /**
     * @var CouponInterface
     */
    protected $couponRepository;

    /**
     * @param CouponInterface $couponRepository
     */
    public function __construct(CouponInterface $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Coupon|null $model
     * @return bool
     */
    public function save(array $data, Coupon $model = null)
    {
        DB::beginTransaction();

        if ($this->couponRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Coupon $model
     * @return bool
     */
    public function delete(Coupon $model)
    {
        DB::beginTransaction();

        if ($this->couponRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}