<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\ImageRepository as ImageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Services\Files\ImageApi;
use App\Models\Shop\Image;
use Exception;
use Cookie;
use File;
use Auth;
use DB;

/**
 * TODO: Рефакторинг
 *
 * Image Service
 * @package App\Services\Shop
 */
class ImageService
{
    /**
     * @var ImageInterface
     */
    protected $imageRepository;

    /**
     * @param ImageInterface $imageRepository
     */
    public function __construct(ImageInterface $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * Загрузка
     * 
     * @param UploadedFile $file
     * @param $productId
     * @return bool
     */
    public function upload(UploadedFile $file, $productId)
    {
        $imageName = uniqid('', true) . '.' . $file->extension();
        $imagePathLogo = public_path() . '/upload/logo.png';
        $imagePath = public_path() . '/upload/products/' . $productId;
        $imageThumbsPath = $imagePath . '/thumbs';

        (new ImageApi($file))
            ->resize(800, null)
            //->insert($imagePathLogo, 'bottom-right')
            ->save($imagePath . DIRECTORY_SEPARATOR . $imageName, 100)
            ->resize(300, null)
            ->save($imageThumbsPath . DIRECTORY_SEPARATOR . $imageName, 80);

        $data = [
            'product_id' => $productId,
            'preview_url' => $imageName,
            'review_url' => $imageName,
            'default' => 0,
            'order' => ($this->imageRepository->getLastOrder($productId) + 1),
            'visible' => 1,
        ];

        return $this->imageRepository->save($data);
    }

    /**
     * Сортировка
     *
     * @param array $ids
     * @param $productId
     * @return bool
     */
    public function order(array $ids, $productId)
    {
        DB::beginTransaction();

        if ($this->imageRepository->order($ids, $productId)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Image $image
     * @return bool
     */
    public function save(array $data, Image $image = null)
    {
        DB::beginTransaction();

        if ($this->imageRepository->save($data, $image)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Image $image
     * @return bool
     */
    public function delete(Image $image)
    {
        if ($this->imageRepository->delete($image)) {

            //TODO удалить файлы

            return true;
        }

        return false;
    }
}