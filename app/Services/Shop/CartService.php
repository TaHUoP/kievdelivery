<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Helpers\ShopHelper as ShopHelperInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Api\App\Storage\Condition;
use App\Models\Shop\Cart;
use Exception;
use Request;
use Cookie;
use Auth;
use DB;

/**
 * Cart Service
 * @package App\Services\Shop
 */
class CartService
{
    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @var ProductInterface
     */
    protected $productRepository;
    /**
     * @var CartService
     */
    protected $couponRepository;

    /**
     * @var ShopHelperInterface
     */
    protected $shopHelper;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @param CartInterface $cartRepository
     * @param ProductInterface $productRepository
     * @param CouponInterface $couponRepository
     * @param ShopHelperInterface $shopHelper
     */
    public function __construct(
        CartInterface $cartRepository,
        CouponInterface $couponRepository,
        ProductInterface $productRepository,
        ShopHelperInterface $shopHelper
    )
    {
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->couponRepository = $couponRepository;
        $this->shopHelper = $shopHelper;

        $this->cart = $this->getActualInfo();
    }

    /**
     * Возвращает актуальную информацию из корзины
     *
     * @return mixed|null
     */
    public function getActualInfo()
    {
        $identifier = Auth::check() ? Auth::user()->id : null;

        if (empty($identifier))
            $identifier = Cookie::get('cart');

        if (empty($identifier) && session('action') == 'add')
            $identifier = session('cart');

        if (!$identifier)
            return null;

        return $this->getInfo($identifier);
    }

    /**
     * Возвращает информацию о корзине по user_id или hash
     *
     * @param $identifier
     * @return mixed|null
     * @throws \ErrorException
     */
    public function getInfo($identifier)
    {
        $result = null;

        if (is_numeric($identifier)) {
            $condition = new Condition();
            $condition->addCondition('user_id', 'user_id', $identifier);
            $condition->scopes(['processed' => 0]);
            $result = $this->cartRepository->findOne($condition);
        }

        if (empty($result)) {
            $condition = new Condition();
            $condition->addCondition('hash', 'hash', $identifier);
            $condition->scopes(['processed' => 0]);
            $result = $this->cartRepository->findOne($condition);
        }

        return $result;
    }

    /**
     * Принудительно устанавливает новую корзину
     *
     * @param Model $cart
     */
    public function setCart(Model $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Добавить товар
     *
     * @param Model $product
     * @param int $amount
     * @return bool
     * @throws Exception
     */
    public function add(Model $product, $amount = 1)
    {
        $cartControl = new CartControlService($this->cart, $this->cartRepository);

        if ($this->cart) {
            $this->unuseCoupon($this->cart);
        }

        if (!$hash = $cartControl->add($product, $amount)) {
            throw new Exception('Adding to cart error');
        }

        return $hash;
    }

    /**
     * Изменить товар
     *
     * @param Model $product
     * @param int $amount
     * @throws Exception
     */
    public function change(Model $product, $amount = 1)
    {
        $cartControl = new CartControlService($this->cart, $this->cartRepository);

        if ($this->cart) {
            $this->unuseCoupon($this->cart);
        }

        if (!$cartControl->change($product, $amount)) {
            throw new Exception('Changing cart error');
        }
    }

    /**
     * Удалить товар
     *
     * @param Model $product
     * @throws Exception
     */
    public function remove(Model $product)
    {
        $cartControl = new CartControlService($this->cart, $this->cartRepository);

        if ($this->cart) {
            $this->unuseCoupon($this->cart);
        }

        if (!$cartControl->remove($product)) {
            throw new Exception('Remove cart error');
        }
    }

    /**
     * Получить общее количество товаров
     *
     * @param Cart $cart
     * @return float|int|mixed|string
     */
    public function countTotalProducts(Cart $cart)
    {
        $result = 0;
        foreach ($cart->products as $relation) {
            $result += $relation->amount;
        }
        return $result;
    }

    /**
     * Использовать купон
     *
     * @param Model $cart
     * @param Model $coupon
     * @throws Exception
     */
    public function useCoupon(Model $cart, Model $coupon)
    {
        $discount = $cart->total_price * $coupon->percentage / 100;

        if ($discount < 0 || $discount > $cart->total_price) {
            $discount = 0;
        }

        $data = [
            'cart' => [
                'discount' => $discount,
                'discount_code' => $coupon->code,
            ]
        ];

        if (!$this->cartRepository->save($data, $cart)) {
            throw new Exception('Failed to save cart');
        }

        if (!$this->couponRepository->save(['ttl' => $coupon->ttl - 1], $coupon)) {
            throw new Exception('Failed to save coupon');
        }
    }

    /**
     * Прекратить использование купона
     *
     * @param Model $cart
     * @throws Exception
     */
    public function unuseCoupon(Model $cart)
    {
        $data = [
            'cart' => [
                'discount' => 0,
                'discount_code' => null,
            ]
        ];

        if (!$this->cartRepository->save($data, $cart)) {
            throw new Exception('Failed to save cart');
        }
    }

}