<?php
namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use Illuminate\Support\Facades\Auth;
use Api\App\Storage\Condition;
use App\Models\Shop\Product;
use App\Models\Review;
use DB;

class ProductReviewsService
{
    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var ProductInterface
     */
    protected $reviewRepository;

    /**
     * @param ProductInterface $productRepository
     * @param ReviewInterface $reviewRepository
     */
    public function __construct(
        ProductInterface $productRepository,
        ReviewInterface $reviewRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * Обновить рейтинг продукта
     *
     * @param $productId
     * @return bool
     * @internal param $rating
     */
    public function productReviewUpdate($productId)
    {
        DB::beginTransaction();

        $product = $this->productRepository->findById($productId);

        $condition = new Condition();
        $condition
            ->addCondition('product_id', 'product_id', $productId)
            ->scopes(['confirmed'=>1]);

        $reviews = $this->reviewRepository->getAverage($condition);

        $data = ['rating' => $reviews];

        if ($this->productRepository->save($data, $product)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }
}