<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Events\CheckoutChangeStatus;
use App\Models\Shop\Checkout;
use Auth;
use DB;

/**
 * Checkout Service
 * @package App\Services\Shop
 */
class CheckoutService
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CartInterface
     */
    protected $cartInterface;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param CartInterface $cartRepository
     */
    public function __construct(CheckoutInterface $checkoutRepository, CartInterface $cartRepository)
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->cartRepository = $cartRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Checkout|null $model
     * @return bool
     */
    public function save(array $data, Checkout $model = null)
    {
        DB::beginTransaction();


        $data['delivery']['date'] = date('Y-m-d', strtotime($data['delivery']['date']));

        $flag = false;

        if ($model) {
            $oldStatus = $model->status;
        }

        if (!$model || ($model && !$model->cart_id)) {
            $cartId = $this->cartRepository->save([
                'cart' => [
                    'hash' => uniqid()
                ]
            ]);
            $checkoutId = $this->checkoutRepository->save(array_merge([
                'cart_id' => $cartId
            ], $data), $model);
            if ($cartId && $checkoutId) {
                $flag = true;
            }
        } else {
            if ($this->checkoutRepository->save($data, $model)) {
                $flag = true;
            }
        }

        if ($flag) {

            // Событие Checkout Change Status
            if ($model && isset($data['checkout']['status'])) {
                event(new CheckoutChangeStatus($model, isset($oldStatus) ? $oldStatus : null));
            }

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Checkout $model
     * @return bool
     */
    public function delete(Checkout $model)
    {
        DB::beginTransaction();

        if ($this->checkoutRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}