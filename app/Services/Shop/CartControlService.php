<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\CartRepository;
use App\Facades\Shop;
use Illuminate\Support\Collection;
use Auth;

/**
 * Cart Control Service
 * @package App\Services\Shop
 */
class CartControlService
{
    private $cart = null;
    private $products = [];
    private $user = null;
    private $cartRepository;
    private $totalPrice = 0;

    /**
     * @param $cart
     * @param CartRepository $cartRepository
     */
    public function __construct($cart, CartRepository $cartRepository)
    {
        $this->cart = $cart;
        if ($this->cart) {
            foreach ($this->cart->products as $relation) {
                $this->products[$relation->product_id] = $relation->amount;
                $this->totalPrice += round(Shop::getPrice($relation->product, false)) * $relation->amount;
            }
        }
        $this->user = Auth::check() ? Auth::user() : null;
        $this->cartRepository = $cartRepository;
    }

    /**
     * Добавить товар
     *
     * @param $product
     * @param int $amount
     * @return bool
     */
    public function add($product, $amount = 1)
    {
        $this->changeProductsList($product, $amount);

        if ($this->cart) {

            $hash = $this->cart['hash'];
            $data = [
                'products' => $this->products,
            ];

            if ($this->cartRepository->save($data, $this->cart)) {
                return $hash;
            }

        } else {

            $hash = md5(uniqid(true));
            $data = [
                'cart' => [
                    'hash' => $hash,
                    'user_id' => $this->user ? $this->user->id : null,
                ],
                'products' => $this->products,
            ];

            if ($this->cartRepository->save($data)) {
                return $hash;
            }
        }

        return false;
    }

    /**
     * Изменить товар
     *
     * @param $product
     * @param int $amount
     * @return bool
     */
    public function change($product, $amount = 1)
    {
        $this->changeProductsList($product, $amount, true);

        $hash = $this->cart['hash'];
        $data = [
            'products' => $this->products,
        ];

        if ($this->cartRepository->save($data, $this->cart)) {
            return $hash;
        }

        return false;
    }

    /**
     * Удалить товар
     *
     * @param $product
     * @return bool
     */
    public function remove($product)
    {
        if (isset($this->products[$product->id])) {
            $this->products[$product->id] = 0;
            $data = [
                'products' => $this->products,
            ];
            return $this->cartRepository->save($data, $this->cart);
        }

        return true;
    }

    /**
     * @param $product
     * @param $amount
     * @param bool $replace
     */
    private function changeProductsList($product, $amount, $replace = false)
    {
        if (isset($this->products[$product->id])) {
            if ($replace) {
                $this->products[$product->id] = $amount;
            } else {
                $this->products[$product->id]+= $amount;
            }
        } else {
            $this->products[$product->id] = $amount;
        }
    }
}