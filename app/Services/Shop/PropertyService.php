<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use App\Models\Shop\Property;
use Auth;
use DB;

/**
 * Property Service
 * @package App\Services\Shop
 */
class PropertyService
{
    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @param PropertyInterface $propertyRepository
     */
    public function __construct(PropertyInterface $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Property|null $model
     * @return bool
     */
    public function save(array $data, Property $model = null)
    {
        DB::beginTransaction();

        if ($this->propertyRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Property|null $model
     * @return bool
     */
    public function delete(Property $model = null)
    {
        DB::beginTransaction();

        if ($this->propertyRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}