<?php

namespace App\Services\Shop;

use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use App\Exceptions\AlertException;
use App\Models\Shop\Category;
use Auth;
use DB;
use Illuminate\Database\Eloquent\Collection;

/**
 * Category Service
 * @package App\Services\Shop
 */
class CategoryService
{
    /**
     * @var CategoryInterface
     */
    protected $categoryRepository;

    /**
     * @param CategoryInterface $categoryRepository
     */
    public function __construct(CategoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Category|null $model
     * @return bool
     */
    public function save(array $data, Category $model = null)
    {
        DB::beginTransaction();

        if ($this->categoryRepository->save($data, $model)) {

            // Пересчитать ордеры, чтобы были по-порядку
            $this->reorderLevel($this->categoryRepository->findOrderedChildrenOf($data['parent_id']));

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Category|null $model
     * @return bool
     */
    public function update(array $data, Category $model = null)
    {
        DB::beginTransaction();

        if ($this->categoryRepository->save($data, $model)) {
            if (!empty($data['order'])) {
                $model = $this->place($model, $data['order']);
            }

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Положить модель в определенное место
     *
     * @param Category $model
     * @param $order
     * @return Category
     * @throws AlertException
     */
    public function place(Category $model, $order)
    {
        // Весь уровень категорий в который входит модель
        $wholeLevel = $this->categoryRepository->findOrderedChildrenOf($model->parent_id);

        // Колво записей на уровне включая эту модель.
        $levelCount = $wholeLevel->count();

        // Если количество 1, то это и есть эта запись, нам её некуда перемещать
        if ($levelCount == 1) {
            // Если порядок не 1, то назначить 1
            if ($model->order != 1) {
                $this->categoryRepository->save(['order' => 1], $model);
            }
            return $model;
        }

        // Если в уровне к примеру 3 категории, а пользователь ввел ордер 4  или больше
        if ($order > $levelCount) {
            // То назначаь на 1 больше( к тому же пример будет 4 )
            $order = $levelCount;
        }

        // Индекс массива
        $newIndex = $order - 1;
        // Категория которую нужно ссунуть взад или вперед
        $newPlace = array_get($wholeLevel, $newIndex);


        // Место осталось прежним (то-есть перемещение не происходит)
        if ($newPlace->id == $model->id) {
            return $model;
        }

        // Получаем текущий индекс
        for ($i = 0; $i < $levelCount; $i++) {
            if ($wholeLevel->get($i)->id == $model->id) {
                $currentIndex = $i;
                break;
            }
        }

        // Если текущий индекс больше чем индекс на который нужно перенести,
        // значит перемещение происходит снизу вверх
        if (isset($currentIndex) && $currentIndex > $newIndex) {
            // по-этому нужно поставить перед, категорией, которая стоит на нужной позиции
            $result = $this->categoryRepository->placeBefore($model, $newPlace);
        } else {
            // в противном случае, перемещение проихсодит сверху вниз, нужно после использовать
            $result = $this->categoryRepository->placeAfter($model, $newPlace);
        }

        // Пересчитать ордеры, чтобы были по-порядку
        $this->reorderLevel($this->categoryRepository->findOrderedChildrenOf($model->parent_id));

        if (!$result) {
            throw new AlertException(trans('app.critical_error'));
        }

        return $model;
    }

    /**
     * Пересчитывает ордеры определенного левела
     *
     * @param Collection $level
     */
    public function reorderLevel(Collection $level)
    {
        for ($i = 0; $i < $level->count(); $i++) {
            $this->categoryRepository->save(['order' => $i + 1], $level->get($i));
        }
    }

    /**
     * Удалить сущность
     *
     * @param Category $model
     * @return bool
     */
    public function delete(Category $model)
    {
        DB::beginTransaction();

        if ($this->categoryRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

}