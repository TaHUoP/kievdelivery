<?php

namespace App\Services\Media;

use Api\Media\Contracts\Entities\MediaRepository as MediaInterface;
use App\Services\Files\ImageApi;
use App\Models\Media\Media;
use Exception;
use Auth;
use File;
use DB;

/**
 * Image Service
 * @package App\Services\Media
 */
class ImageService
{
    private $path;
    private $imageThumbsPath;
    private $imagePathLogo;

    /**
     * @param MediaInterface $mediaRepository
     */
    public function __construct(MediaInterface $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
        $this->path = '/upload/media/images/';
        $this->imageThumbsPath = '/thumbs';
        $this->imagePathLogo = '/upload/logo.png';
    }

    /**
     * Загрузка
     *
     * @param $file
     * @return bool
     */
    public function upload($file)
    {
        $imageName = uniqid('', true) . '.' . $file->extension();

        $imagePathLogo = public_path() . $this->imagePathLogo;
        $imagePath = public_path() . $this->path . date('Y-m');
        $imageThumbsPath = $imagePath . $this->imageThumbsPath;

        try {
            (new ImageApi($file))
                ->resize(800, null)
                //->insert($imagePathLogo, 'bottom-right')
                ->save($imagePath . DIRECTORY_SEPARATOR . $imageName, 100)
                ->resize(300, null)
                ->save($imageThumbsPath . DIRECTORY_SEPARATOR . $imageName, 80);

            $data = [
                'preview_url' => $imageName,
                'review_url' => $imageName,
                'type' => $this->mediaRepository->getTypeImage(),
                'visible' => 1
                //'file_info' => ;
            ];

            return $this->mediaRepository->save($data);

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Media|null $model
     * @return bool
     */
    public function save(array $data, Media $model = null)
    {
        DB::beginTransaction();

        if ($this->mediaRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Media $model
     * @return bool
     */
    public function delete(Media $model)
    {
        DB::beginTransaction();

        if ($this->mediaRepository->delete($model)) {

            // TODO: удалить файлы

            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }
}