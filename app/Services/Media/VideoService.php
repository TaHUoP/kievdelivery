<?php

namespace App\Services\Media;

use Api\Media\Contracts\Entities\MediaRepository;
use App\Models\Media\Media;
use Exception;
use Auth;
use File;
use DB;

/**
 * Video Service
 * @package App\Services\Media
 */
class VideoService
{
    /**
     * @var MediaRepository
     */
    protected $mediaRepository;

    /**
     * @param MediaRepository $mediaRepository
     */
    public function __construct(MediaRepository $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }

    /**
     * Сохранить сущность
     *
     * @param array $data
     * @param Media|null $model
     * @return bool
     */
    public function save(array $data, Media $model = null)
    {
        DB::beginTransaction();

        $data['type'] = $this->mediaRepository->getTypeVideo();
        $data['review_url'] = $data['url'];
        $data['visible'] = 1;
        unset($data['url']);

        if ($this->mediaRepository->save($data, $model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }

    /**
     * Удалить сущность
     *
     * @param Media $model
     * @return bool
     */
    public function delete(Media $model)
    {
        DB::beginTransaction();

        if ($this->mediaRepository->delete($model)) {
            DB::commit();
            return true;
        }

        DB::rollBack();
        return false;
    }
}