<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use app\models\shop\Checkout;

/**
 * Checkout Payment
 * @package App\Events
 */
class CheckoutPayment extends Event
{
    use SerializesModels;

    /**
     * @var Checkout
     */
    public $checkout;

    /**
     * @param Checkout $checkout
     */
    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
