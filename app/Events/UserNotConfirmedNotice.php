<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Models\User\User;
use App\Events\Event;

/**
 * User Not Confirmed Notice
 * @package App\Events
 */
class UserNotConfirmedNotice extends Event
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
