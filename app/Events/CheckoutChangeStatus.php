<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use app\models\shop\Checkout;

/**
 * Checkout Change Status
 * @package App\Events
 */
class CheckoutChangeStatus extends Event
{
    use SerializesModels;

    /**
     * @var Checkout
     */
    public $checkout;

    /**
     * @var int
     */
    public $oldStatus;

    /**
     * @param Checkout $checkout
     * @param $oldStatus
     */
    public function __construct(Checkout $checkout, $oldStatus)
    {
        $this->checkout = $checkout;
        $this->oldStatus = $oldStatus;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
