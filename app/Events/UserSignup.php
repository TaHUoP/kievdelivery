<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Models\User\User;

/**
 * User Signup
 * @package App\Events
 */
class UserSignup extends Event
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
