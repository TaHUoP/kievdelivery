<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Models\User\User;
use App\Events\Event;

/**
 * User Login
 * @package App\Events
 */
class UserLogin extends Event
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
