<?php

namespace App\Helpers;

use App\Contracts\Helpers\MediaHelper as MediaHelperInterface;
use Storage;
use Theme;

/**
 * Media Helper
 * @package App\Helpers
 */
class MediaHelper implements MediaHelperInterface
{
    /**
     * @inheritdoc
     */
    public function getImagePreview($image, $onlyUrl = false)
    {
        $imageUrl = $this->getImageUrl();
        if ($image) {
            $imageUrl = $this->getImageUrl($this->getUrlToImages($image) . $image['preview_url']);
        }
        if ($onlyUrl) {
            return $imageUrl;
        }
        return $this->getImageHtml($imageUrl, $image);
    }

    /**
     * @inheritdoc
     */
    public function getImageReview($image, $onlyUrl = false)
    {
        $imageUrl = $this->getImageUrl();
        if ($image) {
            $imageUrl = $this->getImageUrl($this->getUrlToImages($image) . $image['review_url']);
        }
        if ($onlyUrl) {
            return $imageUrl;
        }
        return $this->getImageHtml($imageUrl, $image);
    }

    /**
     * @inheritdoc
     */
    public function getImageUrl($imageUrl = null)
    {
        $pathToPublic = public_path();
        if ($imageUrl && file_exists($pathToPublic . $imageUrl)) {
            return $imageUrl;
        }

        return '/' . StaticsHelper::getUrlToTheme() . '/default/img/no-image.svg';
    }

    /**
     * @inheritdoc
     */
    public function getUrlToImages($image)
    {
        if ($image['created_at']) {
            $date = date('Y-m', strtotime($image['created_at']));
            return '/upload/media/images/' . $date . '/';
        }
        return null;
    }

    /**
     * Возвращает html изображения
     *
     * @param $url
     * @param $image
     * @return string
     */
    private function getImageHtml($url, $image)
    {
        $title = isset($image['title']) ? $image['title'] : '';
        $alt = isset($image['alt']) ? $image['alt'] : '';

        return '<img src="' . $url . '" alt="' . $alt . '" title="' . $title . '" />';
    }
}