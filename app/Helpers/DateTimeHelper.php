<?php

namespace App\Helpers;

use Api\App\Contracts\Helpers\DateTimeHelper as DateTimeInterface;
use IntlDateFormatter;
use DateTime;
use App;

/**
 * Хелпер работает с датой и временем, преобразуя их в строки различных форматов
 *
 * DateTimeHelper::toFullDate($datetime) - Возвращает полную строку даты
 * DateTimeHelper::toFullDateTime($datetime, $separator = null) - Возвращает полную строку даты и времени
 *
 * TODO
 * DateTimeHelper::toTime($datetime) - Возвращает время
 * DateTimeHelper::toShortDate($datetime, $leadingZeros = true) - Возвращает сокращенную строку даты
 * DateTimeHelper::toShortDateTime($datetime, $separator = null, $leadingZeros = true) - Возвращает сокращенную строку даты и времени
 * DateTimeHelper::diffFullPeriod($datetime1, $datetime2 = null, $reduce = false, $showSeconds = false) - Считает разницу между датами и возвращает полный период между этими датами
 * DateTimeHelper::diffDaysPeriod($datetime1, $datetime2 = null, $showTimeUntilDay = true) - Считает разницу между датами и возвращает период состоящий из дней между этими датами
 * DateTimeHelper::diffAgoPeriod($datetime1, $datetime2 = null, $reduce = false, $showSeconds = false) Считает разницу между датами и возвращает полный период между этими датами в прошедшем времени
 * DateTimeHelper::diffAgoPeriodRound($datetime1, $datetime2 = null, $reduce = false) - Считает разницу между датами и возвращает округленный период между этими датами в прошедшем времени
 * DateTimeHelper::getDaysList() - Возвращает список дней недели
 * DateTimeHelper::getMonthsList($declension = false) - Возвращает список месяцев
 * DateTimeHelper::getTimeUnitsList() - Возвращает список единиц измерения времени
 * DateTimeHelper::getDiff($datetime1, $datetime2 = null) - Разница между датами
 *
 */
class DateTimeHelper implements DateTimeInterface
{
    /**
     * Например: 9 мая 2015
     *
     * @param $datetime
     * @return string
     */
    public function toFullDate($datetime)
    {
        return $this->renderDate($datetime, 'd MMMM Y');
    }

    /**
     * Например: 9 мая 2015, 15:00
     *
     * @param $datetime
     * @return string
     */
    public function toFullDateTime($datetime)
    {
        return $this->renderDate($datetime, 'd MMMM Y, k:mm');
    }

    /**
     * @param $datetime
     * @param $format
     * @return string
     */
    private function renderDate($datetime, $format)
    {
        if ($datetime) {
            $date = new DateTime($datetime);
            $formatter = new IntlDateFormatter(App::getLocale(), IntlDateFormatter::FULL, IntlDateFormatter::FULL);
            if ($formatter) {
                $formatter->setPattern($format);
                return $formatter->format($date);
            }
        }

        return null;
    }
}