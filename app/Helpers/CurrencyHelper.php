<?php

namespace App\Helpers;

use Api\Merchant\Contracts\Helpers\CurrencyHelper as CurrencyInterface;
use NumberFormatter;
use Config;
use App;
use \TorannCurrency;

/**
 * Currency Helper
 * @package App\Helpers
 */
class CurrencyHelper implements CurrencyInterface
{
    /**
     * @inheritdoc
     */
    public function getCurrencySymbol($currencyCode)
    {
        $formatter = new NumberFormatter(App::getLocale() . '@currency=' . $currencyCode, \NumberFormatter::CURRENCY);
        return $formatter->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
    }

    /**
     * @inheritdoc
     */
    public function getMoneyFormatRounded($amount, $currencyCode = null)
    {
        return $this->getMoneyFormat($amount, $currencyCode, 0);
    }

    /**
     * @inheritdoc
     */
    public function getMoneyFormat($amount, $currencyCode = null, $round = 0)
    {
        $currencyCode = !is_null($currencyCode) ? $currencyCode : TorannCurrency::getUserCurrency();
        
        $round = is_numeric($round) && $round >= 0 ? (int)$round : 0;
        
        $amount = TorannCurrency::convert($amount, 'USD', $currencyCode, false);
        $amountString = number_format($amount, $round, '.', ' ');

        $currencyCodeString = '';
        if (!is_null($currencyCode)) {
            $currencyCodeString = $this->getCurrencySymbol($currencyCode);
        }

        if ($currencyCode === 'USD') {
            return trim($currencyCodeString . ' ' . $amountString);
        }

        return trim($amountString . ' ' . $currencyCodeString);
    }

    /**
     * @inheritdoc
     */
    public function getCurrencies()
    {
        return TorannCurrency::getCurrencies();
    }

    /**
     * @inheritdoc
     */
    public function getActiveCurrencies()
    {
        return TorannCurrency::getActiveCurrencies();
    }

    /**
     * @inheritdoc
     */
    public function getCurrency($currency)
    {
        return TorannCurrency::getCurrency($currency);
    }

    /**
     * @inheritdoc
     */
    public function getUserCurrency()
    {
        return TorannCurrency::getUserCurrency();
    }

    /**
     * @inheritdoc
     */
    public function setUserCurrency($currency)
    {
        return TorannCurrency::setUserCurrency($currency);
    }

    /**
     * @inheritdoc
     */
    public function config($setting)
    {
        return TorannCurrency::config($setting);
    }

    /**
     * @inheritdoc
     */
    public function create($currency_array)
    {
        return TorannCurrency::create($currency_array);
    }

    /**
     * @inheritdoc
     */
    public function update($currency_code, $currency_array)
    {
        return TorannCurrency::update($currency_code, $currency_array);
    }

    /**
     * @inheritdoc
     */
    public function find($currency_code)
    {
        $currency = TorannCurrency::getCurrency($currency_code);
        $default = $this->config('default');
        
        if($currency['code'] == $default && $currency_code != $default)
            $currency = null;
            
        return $currency;
    }

    /**
     * @inheritdoc
     */
    public function delete($currency)
    {
        return TorannCurrency::delete($currency);
    }

    /**
     * @inheritdoc
     */
    public function isActive($currency)
    {
        return TorannCurrency::isActive($currency);
    }

}