<?php

namespace App\Helpers\Backend;

use Auth as AuthFacade;
use Config;

/**
 * Auth Helper
 * @package App\Helpers\Backend
 */
class AuthHelper
{
    /**
     * @return mixed
     */
    public static function isAccess()
    {
        return AuthFacade::guard(Config::get('auth.backendGuard'))->check();
    }

    /**
     * @return mixed
     */
    public static function getUser()
    {
        return AuthFacade::guard(Config::get('auth.backendGuard'))->user();
    }
}