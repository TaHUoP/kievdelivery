<?php

namespace App\Helpers;

use Api\App\Contracts\Helpers\StaticsHelper as StaticsHelperInterface;
use App\Models\City;
use Illuminate\Support\Facades\Cookie;
use Storage;
use Theme;

/**
 * Statics Helper
 * @package App\Helpers
 */
class StaticsHelper implements StaticsHelperInterface
{
    /**
     * @inheritdoc
     */
    public static function asset($file, $globally = false)
    {
        if ($globally) {
            return '/statics/' . $file;
        }

        $pathToTheme = Theme::config('asset-path');
        $fullPathToTheme = public_path() . DIRECTORY_SEPARATOR . $pathToTheme;

        $version = null;

        if (file_exists($fullPathToTheme . DIRECTORY_SEPARATOR . $file)) {
            $version = Storage::lastModified($pathToTheme . DIRECTORY_SEPARATOR . $file);
        }

        $themeUrl = Theme::url($file);

        if (empty($themeUrl)) {
            return $file;
        }

        return $themeUrl . (!is_null($version) ? '?' . $version : '');
    }

    /**
     * @inheritdoc
     */
    public static function img($file)
    {
        $urlToFile = Theme::url($file);

        $fileNameAndAnchor = explode('#', $file);
        if (isset($fileNameAndAnchor[0]) && count($fileNameAndAnchor) > 1) {
            $urlToFile = DIRECTORY_SEPARATOR . self::getUrlToTheme() . DIRECTORY_SEPARATOR . $file;
        }

        if (empty($urlToFile)) {
            return $file;
        }

        return $urlToFile;
    }

    /**
     * @inheritdoc
     */
    public static function getUrlToTheme()
    {
        return Theme::config('asset-path');
    }

    /**
     * @inheritdoc
     */
    public static function getPathToTheme()
    {
        return public_path() . DIRECTORY_SEPARATOR . self::getUrlToTheme();
    }

    public static function getCityFee()
    {
        $fee = 0;
        $city_alias = Cookie::get('city_alias');
        $user_city = City::where('alias', $city_alias)->first()
            ? City::where('alias', $city_alias)->first()
            : City::where('alias', City::DEFAULT_CITY)->first();

        if ($user_city->price_type)
            $fee = $user_city->price_type->fee;

        return $fee;
    }
}