<?php

namespace App\Helpers;

use App\Contracts\Helpers\ShopHelper as ShopHelperInterface;
use App\Models\Shop\Value;
use App\Support\DiscountContainer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Shop\ProductRelation;
use Translate;
use Route;
use Validator;

/**
 * Shop Helper
 * @package App\Helpers
 */
class ShopHelper implements ShopHelperInterface
{
    protected $discountContainer;

    public function __construct(DiscountContainer $discountContainer)
    {
        $this->discountContainer = $discountContainer;
    }

    // TODO: Вынести в хелпер Product
    # -------------------------------

    /**
     * Возвращает данные о изображении по умолчанию
     *
     * @param $product
     * @return string
     */
    public function getDefaultImage($product)
    {
        if (isset($product['images'])) {
            foreach ($product['images'] as $image) {
                if ($image['default']) {
                    return $image;
                }
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getImagePreview($image, $onlyUrl = false, $email = false)
    {
        $imageUrl = $this->getImageUrl();
        if ($image) {
            $imageUrl = $this->getImageUrl($this->getUrlToImages($image) . 'thumbs/' . $image['preview_url']);
        }

        if ($onlyUrl) {
            return $email ? $imageUrl : cdn($imageUrl, false, true);
        }
        return $this->getImageHtml($imageUrl, $image);
    }

    /**
     * @inheritdoc
     */
    public function getImageReview($image, $onlyUrl = false)
    {
        $imageUrl = $this->getImageUrl();

        if ($image && !empty($image['review_url'])) {
            $imageUrl = $this->getImageUrl($this->getUrlToImages($image) . $image['review_url']);
        }

        if ($onlyUrl) {
            return $imageUrl;
        }

        return $this->getImageHtml($imageUrl, $image);
    }

    /**
     * @inheritdoc
     */
    public function getImageUrl($imageUrl = null)
    {
        $pathToPublic = public_path();

        if ($imageUrl && file_exists($pathToPublic . $imageUrl)) {
            return $imageUrl;
        }

        return '/' . StaticsHelper::getUrlToTheme() . '/default/img/no-image.svg';
    }

    /**
     * @inheritdoc
     */
    public function getUrlToImages($image)
    {
        if (isset($image['product_id'])) {
            return '/upload/products/' . $image['product_id'] . '/';
        }
        return null;
    }

    /**
     * Возвращает html изображения
     *
     * @param $url
     * @param $image
     * @return string
     */
    private function getImageHtml($url, $image)
    {
        $title = isset($image['translations']) ? Translate::get($image['translations'], 'title') : '';
        $alt = isset($image['translations']) ? Translate::get($image['translations'], 'alt') : '';

        return '<img src="' . cdn($url, false, true) . '" alt="' . $alt . '" title="' . $title . '" />';
    }

    /**
     * @inheritdoc
     */
    public function getProperty($relations, $alias)
    {
        if (!$relations instanceof Collection) {
            throw new \ErrorException('Variable "$relations" must bee instanceof ' . Collection::class);
        }

        foreach ($relations as $relation) {
            if ($relation->property->alias === $alias) {
                return $relation;
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getPropertyValue($attribute)
    {
        if (!$attribute instanceof ProductRelation) {
            throw new \ErrorException('Variable "$attribute" must bee instanceof ' . ProductRelation::class);
        }

        $value = null;

        if (!empty($attribute->custom_value)) {
            $value = $attribute->custom_value;
        } else {
            if ($attribute->value) {
                $value = $attribute->value;
            }
        }
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function getPrice($product, $format = true, Carbon $date = null)
    {
        $date = $date ?: Carbon::now();

        $price = $product->price;

        if ($product->sale_category) {
            $discount = $date->diffInDays(Carbon::now()) != 0
                ? $this->discountContainer->byDate($date)
                : $this->discountContainer->current();

            $price *= $discount->getAbsoluteDiscount();
        }

        return $format ? \Currency::getMoneyFormat($price): (float)$price;
    }

    public function getSalePrice($product, $format = true, Carbon $date = null)
    {
        $price = $this->getPrice($product, false, $date);
        $original_price = $this->getOriginalPrice($product, false);

        if ($format) {
            if ($price !== $original_price) {
                return '<strike>' . \Currency::getMoneyFormat($original_price) . '</strike> ' . \Currency::getMoneyFormat($price);
            } else {
                return \Currency::getMoneyFormat($price);
            }
        } else {
            return (float)$price;
        }
    }

    /**
     * @inheritdoc
     */
    public function getOriginalPrice($product, $format = true)
    {
        $price = $product->getOriginal('price');

        return $format ? \Currency::getMoneyFormat($price): (float)$price;
    }

    # -------------------------------


    // TODO: Вынести в хелпер SearchFilter
    # -------------------------------
    /**
     * @inheritdoc
     */
    public function decodeFilter($filterString)
    {
        if (empty($filterString)) {
            return [];
        }
        $filters = explode('/', $filterString);
        $filterArray = [];

        foreach ($filters as $filter) {
            $filter = explode('_', $filter);

            $property = null;
            if (isset($filter[0])) {
                $property = $filter[0];
                array_shift($filter);
            }

            $values = null;
            if (is_array($filter)) {
                $values = implode('_', $filter);
            }

            if (!$property || !$values) {
                continue;
            }

            $values = explode('-or-', $values);

            $filterArray[$property] = $values;
        }
        return $filterArray;
    }

    /**
     * @inheritdoc
     */
    public function encodeFilter(array $filterArray)
    {
        $result = [];
        foreach ($filterArray as $property => $values) {
            $result[] = $property . '_' . implode('-or-', $values);
        }
        return mb_strtolower(implode('/', $result));
    }

    /**
     * @inheritdoc
     */
    public function mergeFilter(array $filter = [])
    {
        $currentFilter = [];

        if (Route::current()) {
            $currentFilter = Route::input('filter', []);
        }

        $filterArray = [];
        foreach ($filter as $property => $values) {
            $filterArray[$property] = is_array($values) ? $values : (array)$values;
        }

        $currentFilterArray = $this->decodeFilter($currentFilter);

        $filter = array_merge($currentFilterArray, $filterArray);
        return $this->encodeFilter($filter);
    }

    /**
     * @inheritdoc
     */
    public function routeFilter($route, $filter = [])
    {
        $routeData = $route;
        $filter = $this->mergeFilter($filter);
        $filter = ['filter' => $filter];

        if (is_array($routeData)) {
            $route = array_shift($routeData);
            $filter = array_merge($routeData, $filter);
        }

        return urldecode(route($route, $filter));
    }

    /**
     * @inheritdoc
     */
    public function validateRawFilter($raw_filter)
    {
        if(empty($raw_filter)){
            return true;
        }

        $filter = explode('/', $raw_filter);

        foreach ($filter as $field){
            $field = explode('_', $field);
            if(!in_array($field[0], ['sort','count','price']) || $field[1] === ''){
                return false;
            }
        }

        $filter = $this->decodeFilter($raw_filter);

        return $this->validateFilter($filter);
    }

    /**
     * @inheritdoc
     */
    public function validateFilter(array $filter)
    {
        $validator = Validator::make($filter, [
            'sort.0' => 'sometimes|in:price_asc,price_desc',
            'count.0' => 'sometimes|numeric',
            'price.0' => 'sometimes|regex:/^[0-9]+-?[0-9]*$/',
        ]);

        return $validator->passes();
    }
    # -------------------------------


    /**
     * @param $values
     * @return array
     */
    public function getValuesListByDropdown($values)
    {
        $result = [];
        foreach ($values as $value) {
            $result[$value->id] = Translate::get($value->translations, 'name');
        }
        return $result;
    }

    /**
     * @param Collection $productRelations
     * @param Value $value
     * @return bool
     */
    public static function isPropertyValueInProduct(Collection $productRelations, Value $value): bool
    {
        $result = false;
        if (! $productRelations->count()) {
            return $result;
        }
        foreach ($productRelations as $productRelation) {
            if ((int)$productRelation->value_id !==0 && (int)$value->getId() !==0 && (int)$productRelation->value_id === (int)$value->getId() ) {
                $result = true;
            }
        }
        return $result;
    }

}