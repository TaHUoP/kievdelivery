<?php

namespace App\Helpers;

use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Cookie;
use Config;
use App;
use Request;

/**
 * Translate Helper
 * @package App\Helpers
 */
class TranslateHelper implements TranslateHelperInterface
{
    private $languageRepository;
    private $languagesList;

    /**
     * @param LanguageInterface $languageRepository
     * @param ConditionInterface $condition
     */
    public function __construct(LanguageInterface $languageRepository, ConditionInterface $condition)
    {
        $languages = $languageRepository->findActiveAll();

        $this->languageRepository = $languageRepository;
        $this->languagesList = $languages ? $languages->toArray() : [];
    }

    /**
     * @inheritdoc
     */
    public function get($translations, $key = [], $lang = null)
    {

        if (!$translations && !is_array($translations)) {
            return trans('app.no-translate');
        }

        $currentTranslate = [];
        foreach ($translations as $translate) {
            if (!isset($translate['lang_id'])) {
                break;
            }
            if ($translate['lang_id'] == (!isset($lang) ? $this->getCurrentLang('id') : $this->getLangByLocale($lang)['id'])) {
                $currentTranslate = $translate;
                break;
            }
        }

        if (!$currentTranslate) {
            $currentTranslate = $translations->first();
        }

        if ($currentTranslate) {
            if (is_array($key)) {
                if (!$key) {
                    return $currentTranslate;
                }
                $keys = $key;
                $result = [];
                foreach ($keys as $_key) {
                    $result[$_key] = $currentTranslate[$_key];
                }
                return $result;
            }
            return $currentTranslate[$key];
        }

        return trans('app.no-translate');
    }

    /**
     * @inheritdoc
     */
    public function getCurrentLang($key = null)
    {
        foreach ($this->languagesList as $language) {
            if ($language['locale'] == Config::get('app.locale')) {
                if ($key) {
                    return $language[$key];
                }
                return $language;
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getLocaleByBrowser()
    {
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            return substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getLangByLocale($locale)
    {
        foreach ($this->languagesList as $language) {
            if ($language['locale'] == $locale) {
                return $language;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getDefaultLang($key = null)
    {
        foreach ($this->languagesList as $language) {
            if (isset($language['default']) && $language['default']) {
                if ($key) {
                    return $language[$key];
                }
                return $language;
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getIdentifyLocale()
    {
        return $this->getLocaleByBrowser();
    }

    /**
     * @inheritdoc
     */
    public function getLangBySavedLocale($key)
    {
        $savedLocale = Cookie::get($key);

        if ($savedLocale) {
            return $this->getLangByLocale($savedLocale);
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function localizePath($locale, $path = null)
    {
        if(is_null($path))
            $path = Request::path();

        $segments = explode('/', $path);
        if($this->getLangByLocale($segments[0]))
            array_shift($segments);

        $localizedPath = $this->getDefaultLang('locale') == $locale ? implode('/', $segments) : $locale . '/' . implode('/', $segments);

        return $localizedPath;
    }

    /**
     * @inheritdoc
     */
    public function hasLocale($locale)
    {
        foreach ($this->languagesList as $language) {
            if ($language['locale'] == $locale) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function setLocale($locale)
    {
        App::setLocale($locale);
    }

    /**
     * @inheritdoc
     */
    public function savedLocale($locale, $key)
    {
        $languages = $this->languagesList;
        $currentLanguage = Config::get('locale');
        $cookie = null;

        if ($languages) {
            $setLanguage = null;
            foreach ($languages as $language) {
                if ($language['locale'] == $locale) {
                    $setLanguage = $locale;
                    break;
                }
            }
            if ($setLanguage && $currentLanguage !== $setLanguage) {
                Cookie::forget($key);
                $cookie = Cookie::forever($key, $setLanguage);
            }
        }

        return $cookie;
        
    }

    /**
     * @inheritdoc
     */
    public function init($key)
    {
        // Проверяем есть ли данные о локали в куках
        $language = $this->getLangBySavedLocale($key);

        // Если языка нет, пытаемся определить его автоматически
        // например из данных браузера
        if (!$language) {
            $language = $this->getLangByLocale($this->getIdentifyLocale());
        }

        // Если язык не удалось определить или он не поддерживается
        // берем язык из системы по умолчанию
        if (!$language) {
            $language = $this->getDefaultLang();
        }

        // Установить локаль в системе
        if ($language) {
            $this->setLocale($language['locale']);
        }
    }

}