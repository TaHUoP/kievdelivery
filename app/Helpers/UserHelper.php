<?php

namespace App\Helpers;

use App\Contracts\Helpers\UserHelper as UserHelperInterface;
use Config;

/**
 * User Helper
 * @package App\Helpers
 */
class UserHelper implements UserHelperInterface
{
    /**
     * @inheritdoc
     */
    public function getNameByUser($user)
    {
        $name = trans('app.no-username');
        if (isset($user['profile'])) {
            $name = [];
            if (isset($user['profile']['name']) && !empty($user['profile']['name'])) {
                $name[] = $user['profile']['name'];
            }
            if (isset($user['profile']['surname']) && !empty($user['profile']['surname'])) {
                $name[] = $user['profile']['surname'];
            }
            if (!empty($name)) {
                return implode(' ', $name);
            }
        }
        return $name;
    }

    /**
     * @inheritdoc
     */
    public function getAvatarUrlByUser($user)
    {
        $imageUrl = null;
        if (isset($user['profile']) && isset($user['profile']['avatar_url']) && !empty($user['profile']['avatar_url'])) {
            $imageUrl = $this->getUrlToAvatars() . $user['profile']['avatar_url'];
        }
        return $this->getAvatarUrl($user['profile']['avatar_url']);
    }

    /**
     * @inheritdoc
     */
    public function getAvatarUrl($imageUrl = null)
    {
        $pathToPublic = public_path();

        if ($imageUrl && file_exists($pathToPublic . $imageUrl)) {
            return $imageUrl;
        }

        return cdn('/img/no-avatar.png');
    }

    /**
     * @inheritdoc
     */
    public function getUrlToAvatars()
    {
        return '/upload/avatars/';
    }
}