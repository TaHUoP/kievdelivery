<?php

namespace App\Helpers;

use App\Helpers\Backend;
use Auth as AuthFacade;

/**
 * Auth Helper
 * @package App\Helpers
 */
class AuthHelper
{
    /**
     * @return mixed
     */
    public static function isAccess()
    {
        return AuthFacade::check();
    }
}