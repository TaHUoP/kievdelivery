<?php

namespace App\Providers\Media;

use Api\Media\Contracts\Entities\MediaRepository as MediaInterface;
use App\Models\Media\Translate\Media as MediaTranslation;
use App\Repositories\Media\MediaRepository;
use Illuminate\Support\ServiceProvider;
use App\Models\Media\Media;

/**
 * Media Service Provider
 * @package App\Providers
 */
class MediaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // ContentInterface to ContentRepository
        $this->app->bind(MediaInterface::class, function($app) {
            return new MediaRepository(new Media(), new MediaTranslation());
        });

        // MediaHelperInterface to MediaHelper
        $this->app->singleton(\App\Contracts\Helpers\MediaHelper::class, function($app) {
            return new \App\Helpers\MediaHelper();
        });
    }
}