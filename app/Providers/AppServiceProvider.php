<?php

namespace App\Providers;

use Api\App\Contracts\Storage\Condition as ConditionStorageInterface;
use Api\App\Contracts\Entities\SeoSpecificationRepository as SeoSpecificationInterface;
use Waavi\Translation\Repositories\TranslationRepository as WaaviTranslationRepository;
use Waavi\Translation\Repositories\LanguageRepository as WaaviLanguageRepository;
use Api\App\Contracts\Entities\ContentImageRepository as ContentImageInterface;
use Api\App\Contracts\Entities\TranslationRepository as TranslationInterface;
use Api\App\Contracts\Services\ContactsService as ContactsServiceInterface;
use Api\App\Contracts\Entities\CountriesRepository as CountriesInterface;
use Api\App\Contracts\Entities\CityRepository as CityInterface;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use Api\App\Contracts\Entities\LoggerRepository as LoggerInterface;
use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use App\Models\Translate\Countries as CountriesTranslation;
use App\Models\Translate\Cities as CitiesTranslation;
use App\Models\Translate\Content as ContentTranslation;
use Api\App\Storage\Condition as StorageCondition;
use App\Repositories\SeoSpecificationRepository;
use App\Repositories\ContentImageRepository;
use App\Repositories\TranslationRepository;
use App\Repositories\CountriesRepository;
use App\Repositories\CityRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\ContentRepository;
use App\Repositories\PluginRepository;
use App\Repositories\LoggerRepository;
use App\Repositories\ReviewRepository;
use App\Models\SeoSpecification;
use App\Models\Countries;
use App\Models\Content;
use App\Models\ContentImage;
use App\Models\Review;
use App\Models\Logger;
use App\Models\City;
use Illuminate\Support\ServiceProvider;
use App\Validators\PasswordValidator;
use App\Http\Composers\SeoSpecificationsComposer;
use App\Services\ContactsService;
use Validator;
use View;
use App;

/**
 * App Service Provider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::addNamespace('plugin', app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR);


        // Seo Specifications data
        App::singleton(App\Http\Composers\SeoSpecificationsComposer::class);
        View::composer('layouts.app', \App\Http\Composers\SeoSpecificationsComposer::class);

        // Password Validator
        Validator::resolver(function($translator, $data, $rules, $messages, $customAttributes) {
            return new PasswordValidator($translator, $data, $rules, $messages, $customAttributes);
        });

        // Request Data
        App::singleton(\App\Http\Composers\RequestComposer::class);
        View::composer('*', \App\Http\Composers\RequestComposer::class);

        // Language Data
        App::singleton(\App\Http\Composers\LanguageComposer::class);
        View::composer('*', \App\Http\Composers\LanguageComposer::class);

        // Currency Data
        App::singleton(\App\Http\Composers\CurrencyComposer::class);
        View::composer(['layouts._top', 'shop.product.new_show'], \App\Http\Composers\CurrencyComposer::class);

        // Filter Data
        App::singleton(\App\Http\Composers\FilterComposer::class);
        View::composer(['shop.product._filter'], \App\Http\Composers\FilterComposer::class);

        // Cart Data
        App::singleton(\App\Http\Composers\CartComposer::class);
        View::composer(['shop.cart._cart-mini', 'layouts._top'], \App\Http\Composers\CartComposer::class);

        // Category Data
        App::singleton(\App\Http\Composers\CategoryComposer::class);
        View::composer('layouts._nav-category', \App\Http\Composers\CategoryComposer::class);

        // Category Data
        App::singleton(\App\Http\Composers\CountriesComposer::class);
        View::composer(['shop.checkout', 'shop.checkouts._form'], \App\Http\Composers\CountriesComposer::class);

        App::singleton(\App\Http\Composers\CitiesComposer::class);
        View::composer('*', \App\Http\Composers\CitiesComposer::class);

        App::singleton(\App\Http\Composers\UserCityComposer::class);
        View::composer('*', \App\Http\Composers\UserCityComposer::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // LanguageInterface to LanguageRepository
        $this->app->bind(LanguageInterface::class, function ($app) {
            return new LanguageRepository($app->make(WaaviLanguageRepository::class));
        });

        // TranslationInterface to TranslationRepository
        $this->app->bind(TranslationInterface::class, function ($app) {
            return new TranslationRepository($app->make(WaaviTranslationRepository::class));
        });

        // ContentInterface to ContentRepository
        $this->app->bind(ContentInterface::class, function ($app) {
            return new ContentRepository(new Content(), new ContentTranslation());
        });

        // ContentImageInterface to ContentImageRepository
        $this->app->bind(ContentImageInterface::class, function ($app) {
            return new ContentImageRepository(new ContentImage());
        });

        // CountriesInterface to CountriesRepository
        $this->app->bind(CountriesInterface::class, function ($app) {
            return new CountriesRepository(new Countries(), new CountriesTranslation());
        });

        // CitiesInterface to CitiesRepository
        $this->app->bind(CityInterface::class, function ($app) {
            return new CityRepository(new City(), new CitiesTranslation());
        });

        // PluginInterface to PluginRepository
        $this->app->bind(PluginInterface::class, function ($app) {
            return new PluginRepository();
        });

        // LogInterface to LogRepository
        $this->app->bind(LoggerInterface::class, function ($app) {
            return new LoggerRepository(new Logger());
        });

        // ReviewInterface to ReviewRepository
        $this->app->bind(ReviewInterface::class, function ($app) {
            return new ReviewRepository(new Review());
        });

        // SeoSpecificationInterface to SeoSpecificationRepository
        $this->app->bind(SeoSpecificationInterface::class, function ($app) {
            return new SeoSpecificationRepository(new SeoSpecification());
        });

        // ReviewInterface to ReviewRepository
        $this->app->bind(ContactsServiceInterface::class, function ($app) {
            return new ContactsService();
        });

        // --- HELPERS
        // TranslateInterface to TranslateHelper
        $this->app->singleton(\App\Contracts\Helpers\TranslateHelper::class, function ($app) {
            return new \App\Helpers\TranslateHelper (
                new LanguageRepository($app->make(WaaviLanguageRepository::class)),
                new StorageCondition
            );
        });

        // UserHelperInterface to UserHelper
        $this->app->singleton(\App\Contracts\Helpers\UserHelper::class, function ($app) {
            return new \App\Helpers\UserHelper();
        });

        // make DiscountContainer singleton
        $this->app->singleton(\App\Support\DiscountContainer::class);

        // ShopHelperInterface to ShopHelper
        $this->app->singleton(\App\Contracts\Helpers\ShopHelper::class, function ($app) {
            return new \App\Helpers\ShopHelper($app->make(\App\Support\DiscountContainer::class));
        });

        // StaticsHelperInterface to StaticsHelper
        $this->app->singleton(\App\Contracts\Helpers\StaticsHelper::class, function ($app) {
            return new \App\Helpers\StaticsHelper();
        });

        // CurrencyHelperInterface to CurrencyHelper
        $this->app->singleton(\App\Contracts\Helpers\CurrencyHelper::class, function ($app) {
            return new \App\Helpers\CurrencyHelper();
        });

        // --- STORAGE
        // ConditionStorageInterface to StorageCondition
        $this->app->singleton(ConditionStorageInterface::class, function ($app) {
            return new StorageCondition();
        });

        // ContentImageHelper
        $this->app->singleton(\App\Contracts\Helpers\ContentImageHelper::class, function ($app) {
            return new \App\Helpers\ContentImageHelper();
        });

        // DateTimeHelperInterface to DateTimeHelper
        $this->app->singleton(\Api\App\Contracts\Helpers\DateTimeHelper::class, function ($app) {
            return new \App\Helpers\DateTimeHelper();
        });

        $this->app->singleton(App\Modules\I18nModule\Services\I18nService::class, function ($app) {
            return new App\Modules\I18nModule\Services\I18nService();
        });

        $this->app->singleton(App\Modules\CatalogFilterModule\Services\FilterService::class, function ($app) {
            return new App\Modules\CatalogFilterModule\Services\FilterService();
        });

        $this->app->singleton(App\Modules\SeoModule\Services\SeoBuilderParamsService::class, function ($app) {
            return new App\Modules\SeoModule\Services\SeoBuilderParamsService();
        });
    }
}