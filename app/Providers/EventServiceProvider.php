<?php

namespace App\Providers;

use App\Events\ReviewCreated;
use App\Events\UserSocialSignup;
use App\Listeners\ReviewMailToAdmin;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use App\Events\UserNotConfirmedNotice;
use App\Events\CheckoutChangeStatus;
use Illuminate\Auth\Events\Failed;
use App\Events\CheckoutPayment;
use App\Events\CheckoutPost;
use App\Events\UserSignup;
use App\Events\UserLogin;

/**
 * Event Service Provider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserSignup::class => [
            \App\Listeners\SignupMailToAdmin::class,
            \App\Listeners\SignupMailToUser::class,
        ],
        UserSocialSignup::class => [
            \App\Listeners\SignupMailToAdmin::class,
        ],
        Failed::class => [
            \App\Listeners\HandleFailedLogin::class,
        ],
        UserNotConfirmedNotice::class => [
            \App\Listeners\CheckUserForConfirm::class,
        ],
        CheckoutPost::class => [
            \App\Listeners\CheckoutMailToUser::class,
            \App\Listeners\CheckoutMailToAdmin::class,
        ],
        CheckoutChangeStatus::class => [
            \App\Listeners\CheckoutStatusMailToUser::class,
        ],
        CheckoutPayment::class => [
            \App\Listeners\CheckoutPaymentMailToUser::class,
        ],
        ReviewCreated::class => [
            ReviewMailToAdmin::class
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
