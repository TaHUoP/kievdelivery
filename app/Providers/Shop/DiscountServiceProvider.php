<?php

namespace App\Providers\Shop;

use Api\Shop\Contracts\Entities\DiscountRepository as DiscountInterface;
use App\Repositories\Shop\DiscountRepository;
use Illuminate\Support\ServiceProvider;
use App\Models\Shop\Discount;

/**
 * Discount Service Provider
 * @package App\Providers\Shop
 */
class DiscountServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        // Discount Repository
        // Привязка модели Discount к DiscountInterface
        $this->app->bind(DiscountInterface::class, function ($app) {
            return new DiscountRepository(new Discount());
        });
    }
}
