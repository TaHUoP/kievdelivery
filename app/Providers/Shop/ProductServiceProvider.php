<?php

namespace App\Providers\Shop;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\Shop\Contracts\Entities\ValueRepository as ValueInterface;
use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use Api\Shop\Contracts\Entities\ImageRepository as ImageInterface;
use Api\Shop\Contracts\Entities\ProductFilesRepository as ProductFilesInterface;
use App\Repositories\Shop\PropertyRepository;
use App\Repositories\Shop\CategoryRepository;
use App\Repositories\Shop\ProductRepository;
use App\Repositories\Shop\ProductFilesRepository;
use App\Repositories\Shop\CheckoutRepository;
use App\Repositories\Shop\ValueRepository;
use App\Repositories\Shop\ImageRepository;
use App\Repositories\Shop\CartRepository;
use Illuminate\Support\ServiceProvider;
use App\Models\Shop\Property;
use App\Models\Shop\Checkout;
use App\Models\Shop\Category;
use App\Models\Shop\ProductFile;
use App\Models\Shop\Product;
use App\Models\Shop\Image;
use App\Models\Shop\Value;
use App\Models\Shop\Cart;

/**
 * Product Service Provider
 * @package App\Providers\Shop
 */
class ProductServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        // Product Files Repository
        // Привязка модели Category к CategoryInterface
        $this->app->bind(ProductFilesInterface::class, function($app) {
            return new ProductFilesRepository(new ProductFile());
        });

        // Category Repository
        // Привязка модели Category к CategoryInterface
        $this->app->bind(CategoryInterface::class, function($app) {
            return new CategoryRepository(new Category());
        });

        // Product Repository
        // Привязка модели Product к ProductInterface
        $this->app->bind(ProductInterface::class, function($app) {
            return new ProductRepository(new Product());
        });

        // Product Repository
        // Привязка модели Product к ProductInterface
        $this->app->bind(ImageInterface::class, function($app) {
            return new ImageRepository(new Image());
        });

        // Property Repository
        // Привязка модели Property к PropertyInterface
        $this->app->bind(PropertyInterface::class, function($app) {
            return new PropertyRepository(new Property());
        });

        // Value Repository
        // Привязка модели Value к ValueInterface
        $this->app->bind(ValueInterface::class, function($app) {
            return new ValueRepository(new Value());
        });

        // Cart Repository
        // Привязка модели Cart к CartInterface
        $this->app->bind(CartInterface::class, function($app) {
            return new CartRepository(new Cart());
        });

        // Checkout Repository
        // Привязка модели Checkout к CheckoutInterface
        $this->app->bind(CheckoutInterface::class, function($app) {
            return new CheckoutRepository(new Checkout());
        });
    }
}