<?php

namespace App\Providers\Shop;

use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use App\Repositories\Shop\CouponRepository;
use Illuminate\Support\ServiceProvider;
use App\Models\Shop\Coupon;

/**
 * Coupon Service Provider
 * @package App\Providers\Shop
 */
class CouponServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        // Coupon Repository
        // Привязка модели Coupon к CouponInterface
        $this->app->bind(CouponInterface::class, function ($app) {
            return new CouponRepository(new Coupon());
        });
    }
}
