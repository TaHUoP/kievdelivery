<?php

namespace App\Providers\User;

use Illuminate\Support\ServiceProvider;

use Api\User\Contracts\Entities\ProfileRepository as ProfileInterface;
use Api\User\Contracts\Entities\ActionRepository as ActionInterface;
use Api\User\Contracts\Entities\UserRepository as UserInterface;

use App\Repositories\User\ProfileRepository;
use App\Repositories\User\ActionRepository;
use App\Repositories\User\UserRepository;

use App\Models\User\Action;
use App\Models\User\Profile;
use App\Models\User\User;

use Validator;
use Config;
use View;
use App;

/**
 * User Service Provider
 * @package App\Providers\User
 */
class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // UserInterface to UserRepository
        $this->app->bind(UserInterface::class, function($app) {
            return new UserRepository(new User(), new Profile());
        });

        // ProfileInterface to ProfileRepository
        $this->app->bind(ProfileInterface::class, function($app) {
            return new ProfileRepository(new Profile());
        });

        // ActionInterface to ActionRepository
        $this->app->bind(ActionInterface::class, function($app) {
            return new ActionRepository(new Action());
        });

        // ActionInterface to ActionRepository
        $this->app->bind(ActionInterface::class, function($app) {
            return new ActionRepository(new Action());
        });
    }
}
