<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Media Helper
 * @package App\Facades
 */
class ContentImage extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\ContentImageHelper::class;
    }
}