<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Currency Helper
 * @package App\Facades
 */
class Currency extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\CurrencyHelper::class;
    }
}