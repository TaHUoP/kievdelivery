<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Translate Helper
 * @package App\Facades
 */
class Translate extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\TranslateHelper::class;
    }
}