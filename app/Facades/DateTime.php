<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Statics
 * @package App\Facades
 */
class DateTime extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \Api\App\Contracts\Helpers\DateTimeHelper::class;
    }
}