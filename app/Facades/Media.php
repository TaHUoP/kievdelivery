<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Media Helper
 * @package App\Facades
 */
class Media extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\MediaHelper::class;
    }
}