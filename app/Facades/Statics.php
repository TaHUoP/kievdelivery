<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Statics
 * @package App\Facades
 */
class Statics extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\StaticsHelper::class;
    }
}