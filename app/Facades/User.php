<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * User Helper
 * @package App\Facades
 */
class User extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\UserHelper::class;
    }
}