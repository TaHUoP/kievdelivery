<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Shop Helper
 * @package App\Facades
 */
class Shop extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Contracts\Helpers\ShopHelper::class;
    }
}