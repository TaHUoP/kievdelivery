<?php

namespace App\Contracts\Helpers;

use Api\Shop\Contracts\Helpers\ProductHelper;
use Api\Shop\Contracts\Helpers\FilterHelper;
use Api\Shop\Contracts\Helpers\ImageHelper;

/**
 * Shop Helper
 * @package App\Contracts\Helpers
 */
interface ShopHelper extends FilterHelper, ImageHelper, ProductHelper
{
    
}