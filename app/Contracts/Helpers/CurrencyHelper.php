<?php

namespace App\Contracts\Helpers;

use Api\Merchant\Contracts\Helpers\CurrencyHelper as CurrencyHelperInterface;

/**
 * Currency Helper
 * @package App\Contracts\Helpers
 */
interface CurrencyHelper extends CurrencyHelperInterface
{
    
}