<?php

namespace App\Contracts\Helpers;

use Api\App\Contracts\Helpers\ContentImageHelper as ContentImageHelperInterface;

/**
 * Content Image Helper
 * @package App\Contracts\Helpers
 */
interface ContentImageHelper extends ContentImageHelperInterface
{

}