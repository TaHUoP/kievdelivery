<?php

namespace App\Contracts\Helpers;

use Api\User\Contracts\Helpers\AvatarHelper;
use Api\User\Contracts\Helpers\InfoHelper;

/**
 * User Helper
 * @package App\Contracts\Helpers
 */
interface UserHelper extends AvatarHelper, InfoHelper
{

}