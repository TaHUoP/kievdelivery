<?php

namespace App\Contracts\Helpers;

use Api\App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;

/**
 * Translate Helper
 * @package App\Contracts\Helpers
 */
interface TranslateHelper extends TranslateHelperInterface
{
    
}