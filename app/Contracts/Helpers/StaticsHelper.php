<?php

namespace App\Contracts\Helpers;

use Api\App\Contracts\Helpers\StaticsHelper as StaticsHelperInterface;

/**
 * Statics Helper
 * @package App\Contracts\Helpers
 */
interface StaticsHelper extends StaticsHelperInterface
{
    
}