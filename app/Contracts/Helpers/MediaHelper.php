<?php

namespace App\Contracts\Helpers;

use Api\Media\Contracts\Helpers\ImageHelper;

/**
 * Media Helper
 * @package App\Contracts\Helpers
 */
interface MediaHelper extends ImageHelper
{

}