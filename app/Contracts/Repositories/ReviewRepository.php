<?php 

namespace App\Contracts\Repositories;

use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Api\App\Contracts\Query\FindInterface;

/**
 * Review Interface
 * @package App\Contracts\Repositories
 */
interface ReviewRepository extends FindInterface
{
    /**
     * Осуществляет поиск последних нескольких записей
     * 
     * @param ConditionInterface $condition
     * @return mixed
     */
    public function findLastAll(ConditionInterface $condition = null);

    /**
     * Возвращает тип продукта "Product"
     *
     * @return mixed
     */
    public function getTypeProduct();

    /**
     * Возвращает тип продуктов "Main" для стрвницы с отзывами
     *
     * @return mixed
     */
    public function getTypeMain();

    /**
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function getAverage(ConditionInterface $condition = null);
}