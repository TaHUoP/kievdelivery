<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

/**
 * Cart
 * @package App\Widgets
 */
class Cart extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        
        return view("widgets.cart", [

        ]);
    }
}