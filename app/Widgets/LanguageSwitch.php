<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

/**
 * LanguageSwitch
 * @package App\Widgets
 */
class LanguageSwitch extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        
        return view("widgets.language-switch", [

        ]);
    }
}