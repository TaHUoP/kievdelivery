<?php

namespace App\Widgets;

use Api\App\Contracts\Entities\WidgetRepository as WidgetInterface;
use App\Repositories\WidgetRepository;
use App\Models\Widget as WidgetModel;
use Arrilot\Widgets\AbstractWidget;
use Api\App\Storage\Condition;
use App;

/**
 * Widget
 * @package App\Widgets
 */
class Widget extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        if (!is_array($this->config)) {
            $this->config = [];
        }

        $key = isset($this->config['key']) ? $this->config['key'] : null;

        if (is_null($key)) {
            return null;
        }

        $condition = new Condition();
        $condition->addCondition('key', 'key', $key);

        $widget = new WidgetRepository(new WidgetModel());
        $widget = $widget->findOne($condition);

        if (!empty($widget)) {
            $widget = $widget->toArray();
        }

        return view("widgets.widget", [
            'widget' => $widget
        ]);
    }
}