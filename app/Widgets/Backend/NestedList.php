<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;
use App\Facades\Translate;

/**
 * Nested List
 * @package App\Widgets\Backend
 */
class NestedList extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $categories = [];

    /**
     * @var integer
     */
    protected $expandedId;

    /**
     * @var array
     */
    protected $expandedIds;

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $this->categories = $this->config['categories'];
        if ($this->config['expandedId'] != null) {
            $this->expandedId = $this->config['expandedId'];
        }

        // TODO: перенести в репозиторий и заинжектить
        $this->expandedIds = \App\Models\Shop\Category::ancestorsOf($this->expandedId)->pluck('id')->toArray();
        $this->ulList();
    }

    /**
     * Рендер дерева категорий
     */
    protected function ulList()
    {
        $traverse = function ($categories, $depth = 0) use (&$traverse) {

            echo '<ul>';

            foreach ($categories as $category) {

                $expanded = '';
                if ($this->expandedId === $category->id || in_array($category->id, $this->expandedIds)) {
                    $expanded = 'expanded';
                }

                echo "<li class='{$expanded}'>";

                echo '<input type="hidden" name="category_id" value="' . $category->id . '" data-url="'. route('backend.shop.categories.edit', $category) .'" />';

                // TODO: заинжектить TranslateHelper
                echo Translate::get($category->translations, 'name');

                $traverse($category->children, $depth + 1);

                echo '</li>';
            }

            echo '</ul>';

        };

        if (isset($this->categories) && $this->categories->first()) {
            $traverse($this->categories->first()->children, 1);
        } else {
            echo trans('app.no-data');
        }
    }
}