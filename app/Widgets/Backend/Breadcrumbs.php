<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * Breadcrumbs
 * @package App\Widgets
 */
class Breadcrumbs extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        if (!is_array($this->config)) {
            $this->config = [];
        }
        return view("widgets.backend.breadcrumbs", [
            'links' => $this->config,
        ]);
    }
}