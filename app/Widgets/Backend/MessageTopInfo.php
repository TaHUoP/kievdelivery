<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * Message Top Info
 * @package App\Widgets
 */
class MessageTopInfo extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        return '';
        /*
        return view("widgets.message-top-info", [

        ]);*/
    }
}