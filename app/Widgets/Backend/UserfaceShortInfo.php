<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * Userface Short Info
 * @package App\Widgets
 */
class UserfaceShortInfo extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $user = isset($this->config['user']) ? $this->config['user'] : null;
        
        return view("widgets.backend.userface-short-info", compact('user'));
    }
}