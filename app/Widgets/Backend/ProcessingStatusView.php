<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * Processing Status View
 * @package App\Widgets\Backend
 */
class ProcessingStatusView extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $value = 0;

        if (isset($this->config['value'])) {
            $value = (int)$this->config['value'];
        }

        if (!in_array($value, [0, 1, -1])) {
            $value = 0;
        }

        return view("widgets.backend.processing-status-view", compact('value'));
    }
}