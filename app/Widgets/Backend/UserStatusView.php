<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * User Status View
 * @package App\Widgets\Backend
 */
class UserStatusView extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $value = 0;

        if (isset($this->config['value'])) {
            $value = (int)$this->config['value'];
        }

        if (!in_array($value, [0, 1, -1])) {
            $value = 0;
        }

        return view("widgets.backend.user-status-view", compact('value'));
    }
}