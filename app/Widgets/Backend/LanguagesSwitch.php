<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * Languages List
 * @package App\Widgets\Backend
 */
class LanguagesSwitch extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $fullName =  isset($this->config['fullName']) ? (bool)$this->config['fullName'] : true;
        $section =  isset($this->config['section']) ? $this->config['section'] : 'default';

        return view("widgets.backend.languages-switch", [
            'fullName' => $fullName,
            'section' => $section
        ]);
    }
}