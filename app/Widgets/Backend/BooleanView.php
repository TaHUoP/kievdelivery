<?php

namespace App\Widgets\Backend;

use Arrilot\Widgets\AbstractWidget;

/**
 * Boolean View
 * @package App\Widgets
 */
class BooleanView extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $value = false;
        $reverse = false;

        if (isset($this->config['value'])) {
            $value = (bool)$this->config['value'];
        }

        if (isset($this->config['reverse'])) {
            $reverse = (bool)$this->config['reverse'];
        }

        return view("widgets.backend.boolean-view", compact('value', 'reverse'));
    }
}