<?php

namespace App\Widgets\Backend;

use App\Helpers\Backend\AuthHelper;
use Arrilot\Widgets\AbstractWidget;
use Auth;

/**
 * Userface Top Info Widget
 * @package App\Widgets
 */
class UserfaceTopInfo extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\View\View
     */
    public function run()
    {
        $user = AuthHelper::getUser();

        return view("widgets.backend.userface-top-info", [
            'user' => $user
        ]);
    }
}