<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

/**
 * Breadcrumbs
 * @package App\Widgets
 */
class Breadcrumbs extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return array
     */
    protected function getLinksMeta($links)
    {
        $linksMeta = [];

        foreach ($links as $link) {
            preg_match('/<a href="(.+)">/', $link, $href );
            preg_match('/<a href=".*">(.+)<\/a>/', $link, $text );

            if(!empty($href[1]) && !empty($text[1]))
                $linksMeta[] = ['href' => $href[1], 'text' => $text[1]];
            else {
                $linksMeta[] = ['href' => url()->current(), 'text' => $link];
            }
        }

        return $linksMeta;
    }

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        if (!is_array($this->config)) {
            $this->config = [];
        }
        return view("widgets.breadcrumbs", [
            'links' => $this->config,
            'linksMeta' => $this->getLinksMeta($this->config),
        ]);
    }
}