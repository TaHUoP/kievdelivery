<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

/**
 * Products Categories
 * @package App\Widgets
 */
class ProductsCategories extends AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        
        return view("widgets.products-categories", [

        ]);
    }
}