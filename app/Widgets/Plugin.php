<?php

namespace App\Widgets;

use Symfony\Component\Process\Exception\InvalidArgumentException;
use App\Repositories\PluginRepository;
use Arrilot\Widgets\AbstractWidget;
use Api\App\Storage\Condition;
use App;

/**
 * Products Categories
 * @package App\Widgets
 */
class Plugin extends AbstractWidget
{
    /**
     * @var PluginRepository
     */
    protected $pluginRepository;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return\Illuminate\View\View
     */
    public function run()
    {
        if (!isset($this->config['key'])) {
            throw new InvalidArgumentException('Argument "Key" must be set');
        }

        if (!isset($this->config['view'])) {
            throw new InvalidArgumentException('Argument "View" must be set');
        }

        $this->pluginRepository = App::make(PluginRepository::class);

        $condition = new Condition();
        $condition->scopes(['visible' => 1]);


        if (!$plugin = $this->pluginRepository->findByKey($this->config['key'], $condition)) {
            return null;
        }

        $classHandler = null;

        try {

            switch ($plugin->type) {

                case 'slider':
                    $classHandler = App\Plugins\Slider\Services\HandlerPlugin::class;
                    break;

                case 'text':
                    $classHandler = App\Plugins\Text\Services\HandlerPlugin::class;
                    break;
            }

            $service = App::make($classHandler);

            return view($this->config['view'], [
                'data' => $service->getData($plugin->value),
            ]);

        } catch (\Exception $e) {

            throw new \ErrorException('Class "' . $classHandler . '" not found. Check "class_handler" field in the database.');

        }
    }
}