<?php

namespace App\Support;


use App\Exceptions\PriceException;
use App\Helpers\DateHelper;
use App\Models\Price;
use App\Models\PriceType;
use App\Models\Shop\Discount;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DiscountContainer
{
    protected $discounts;
    protected $current_discount;

    public function __construct()
    {
        $this->discounts = Discount::orderBy('percents', 'acs')->get();

        $now = Carbon::now();

        $current = Discount::where('date_from', '<=', $now)
            ->where('date_to', '>=', $now)
            ->orderBy('percents', 'acs')
            ->first();

        $this->current_discount = $current ?: new Discount;
    }

    public function all()
    {
        return $this->discounts;
    }

    //TODO: fix byDate method
    public function byDate(Carbon $date)
    {
//        $discount = $this->discounts->where('date_from', '<=', $date)
//            ->where('date_to', '>=', $date)
//            ->first();

        $discount = Discount::where('date_from', '<=', $date)
            ->where('date_to', '>=', $date)
            ->orderBy('percents', 'acs')
            ->first();

        return $discount ?: new Discount;
    }

    //TODO: current method should use byDate when it fixed
    public function current()
    {
//        return $this->byDate(Carbon::now());
        return $this->current_discount;
    }
}