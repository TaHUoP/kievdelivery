<?php

namespace App\Http\Requests;

use HttpResponseException;
use Validator;
use Response;

/**
 * Contacts Request
 * @package App\Http\Requests
 */
class CityRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            'text' => 'required',
            'price_type_id' => 'required',
            'region_id' => 'required',
            'alias' => 'required|unique:cities,alias'
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }
}