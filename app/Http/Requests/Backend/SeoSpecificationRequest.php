<?php

namespace App\Http\Requests\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use HttpResponseException;
use Validator;
use Response;
use Route;

/**
 * Seo Specification Request
 * @package App\Http\Requests\Backend
 */
class SeoSpecificationRequest extends \App\Http\Requests\Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param TranslateHelperInterface $translateHelper
     * @return array
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;

        $this->sanitize();

        $input = $this->all();

        $dl = $translateHelper->getDefaultLang('locale');

        return [
            'url' => [
                'bail', 'required', 'max:255',
                'regex:/(^[a-zA-Z_0-9\-\/]+|[a-zA-Z_0-9\-\/]+\.+[a-zA-Z0-9]{1,})$/i',
                'unique:seo_specifications,url,' . $this->route()->parameter('seo', null),
            ],

            //'specify_head' => 'required|string|max:255',
            //'specify_footer' => 'required|string|max:255',

            'translations.*.title' => 'present|string|max:255',
            'translations.*.meta_title' => 'present|string|max:255',
            'translations.*.meta_description' => 'present|string|max:255',
            'translations.*.meta_keywords' => 'present|string|max:255',

            'translations.'.$dl.'.meta_title' => 'present|string|max:255',
            'translations.'.$dl.'.meta_description' => 'present|string|max:255',
            'translations.'.$dl.'.meta_keywords' => 'present|string|max:255',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'url' => trans('seo.attr.url'),
            'translations.*.title' => trans('seo.attr.title'),
            'translations.*.meta_title' => trans('seo.attr.meta_title'),
            'translations.*.meta_description' => trans('seo.attr.meta_description'),
            'translations.*.meta_keywords' => trans('seo.attr.meta_keywords'),
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['url'] = filter_var(isset($input['url']) ? $input['url'] : null, FILTER_SANITIZE_URL);
        $input['title'] = filter_var(isset($input['title']) ? $input['title'] : null, FILTER_SANITIZE_STRING);
        $input['meta_title'] = filter_var(isset($input['meta_title']) ? $input['meta_title'] : null, FILTER_SANITIZE_STRING);
        $input['meta_description'] = filter_var(isset($input['meta_description']) ? $input['meta_description'] : null, FILTER_SANITIZE_STRING);
        $input['meta_keywords'] = filter_var(isset($input['meta_keywords']) ? $input['meta_keywords'] : null, FILTER_SANITIZE_STRING);
        $input['translations'] = (isset($input['translations']) && is_array($input['translations'])) ? $input['translations'] : [];

        $input['url'] = '/' . trim($input['url'], '/');

        // Добавляем lang_id к каждому переводу
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }

        $this->replace($input);
    }
}