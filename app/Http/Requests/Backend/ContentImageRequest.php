<?php

namespace App\Http\Requests\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;

/**
 * ContentImage Request
 * @package App\Http\Requests\Media\Backend
 */
class ContentImageRequest extends \App\Http\Requests\Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param TranslateHelperInterface $translateHelper
     * @return array
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->sanitize();

        // Загрузка изображения
        if ($this->route()->getName() == "backend.content.images.upload") {
            return [
                'uploadfile' => 'required|image|max:2048|mimes:jpeg,bmp,png|dimensions:min_width=100,min_height=100',
            ];
        }

        // Редактировать изображение
        return [];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['default'] = (isset($input['default']) && $input['default'] == 'on') ? 1 : 0;

        $this->replace($input);
    }

}