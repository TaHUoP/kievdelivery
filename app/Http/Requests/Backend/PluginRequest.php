<?php

namespace App\Http\Requests\Backend;

use HttpResponseException;
use App\Models\Plugin;
use Validator;
use Response;
use Route;

/**
 * Plugin Request
 * @package App\Http\Requests\Backend
 */
class PluginRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        switch (strtoupper($this->getMethod())) {
            case 'POST':
                return [
                    'type' => 'required|in:slider,text',
                    'key' => 'required|string|min:3|max:255|unique:' . Plugin::getTableName() . ',key,id',
                    'description' => 'required|string|min:3|max:255',
                    'visible' => 'in:on',
                ];

            case 'PUT':
                $id = (int)$this->route()->parameter('plugins');
                return [
                    'key' => 'required|unique:' . Plugin::getTableName() . ',key,NULL,id,id,!' . $id,
                    'description' => 'required|min:3|max:255',
                    'visible' => 'in:on',
                ];

            default:
                throw new \InvalidArgumentException('Cannot process rules for HTTP method "' . $this->getMethod() . '"');
        }
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'key' => trans('app.attr.alias'),
            'description' => trans('app.attr.description'),
            'visible' => trans('app.attr.visible'),
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        if (strtoupper($this->getMethod()) === "POST") {
            $input['key'] = filter_var(isset($input['key']) ? $input['key'] : null, FILTER_SANITIZE_STRING);
        }
        $input['visible'] = isset($input['visible']) ? (bool)$input['visible'] : 0;
        $input['description'] = filter_var(isset($input['description']) ? $input['description'] : null, FILTER_SANITIZE_STRING);

        $this->replace($input);
    }
}