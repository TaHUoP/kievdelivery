<?php

namespace App\Http\Requests\Backend;

use HttpResponseException;
use Validator;
use Response;
use Route;

/**
 * Settings Request
 * @package App\Http\Requests\Backend
 */
class SettingsRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        $input = $this->all();

        return [

        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();


        $this->replace($input);
    }
}