<?php

namespace App\Http\Requests\Backend;

use HttpResponseException;
use Validator;
use Response;
use Route;

/**
 * Currency Request
 * @package App\Http\Requests\Backend
 */
class CurrencyRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        switch (strtoupper($this->getMethod())) {
            case 'POST':
                return [
                    'code' => 'required|alpha|min:2|max:3|unique:currencies',
                    'name' => 'required|max:255',
                    'exchange_rate' => 'required|numeric',
                ];

            case 'PUT':
                return [
                    'code' => 'required|alpha|min:2|max:3',
                    'name' => 'required|max:255',
                    'exchange_rate' => 'required|numeric',
                ];

            default:
                throw new \InvalidArgumentException('Cannot process rules for HTTP method "' . $this->getMethod() . '"');
        }
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'code' => trans('currencies.attr.code'),
            'name' => trans('currencies.attr.name'),
            'exchange_rate' => trans('currencies.attr.exchange_rate')
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['code'] = filter_var(isset($input['code']) ? strtoupper($input['code']) : null, FILTER_SANITIZE_STRING);
        $input['name'] = filter_var(isset($input['name']) ? $input['name'] : null, FILTER_SANITIZE_STRING);
        $input['exchange_rate'] = filter_var(isset($input['exchange_rate']) ? str_replace(',', '.', $input['exchange_rate']) : null, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $input['active'] = isset($input['active']) ? 1 : 0;

        $this->replace($input);

    }
}