<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

/**
 * Content Upload Request
 * @package App\Http\Requests\Backend
 */
class ContentUploadRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'file' => 'required|image|max:2048|mimes:jpeg,bmp,png|dimensions:min_width=13,min_height=13',
        ];
    }
}