<?php

namespace App\Http\Requests\Backend;

use HttpResponseException;
use Validator;
use Response;
use Route;

/**
 * Language Request
 * @package App\Http\Requests\Backend
 */
class LanguageRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        switch (strtoupper($this->getMethod())) {

            case 'POST':
                return [
                    'locale' => 'required|unique:translator_languages,locale|regex:/^[a-zA-Z]+$/|min:2|max:3',
                    'name' => 'required|max:16',
                ];

            case 'PUT':
                $id = (int)$this->route()->parameter('languages');
                return [
                    'locale' => 'required|unique:translator_languages,locale,NULL,id,id,!' . $id . '|regex:/^[a-zA-Z]+$/|min:2|max:3',
                    'name' => 'required|max:16',
                ];

            default:
                throw new \InvalidArgumentException('Cannot process rules for HTTP method "' . $this->getMethod() . '"');
        }
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'locale' => trans('languages.attr.locale'),
            'name' => trans('languages.attr.name')
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['locale'] = filter_var(isset($input['locale']) ? $input['locale'] : null, FILTER_SANITIZE_STRING);
        $input['name'] = filter_var(isset($input['name']) ? $input['name'] : null, FILTER_SANITIZE_STRING);
        $input['default'] = isset($input['default']) ? 1 : 0;
        $input['active'] = isset($input['active']) ? 1 : 0;

        $this->replace($input);

    }
}