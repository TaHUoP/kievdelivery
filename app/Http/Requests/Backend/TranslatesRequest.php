<?php

namespace App\Http\Requests\Backend;

use App\Exceptions\AlertException;
use App\Http\Requests\Request;

/**
 * Translates Request
 * @package App\Http\Requests\Backend
 */
class TranslatesRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'translates' => 'present|array',
            'translates.*.*' => 'present|array',
        ];
    }

    /**
     * Валидация правильности отправленних данних
     *
     * @throws AlertException
     */
    public function validatePostDataFormat()
    {
        foreach ($this->input('translates.*.*.*') as $translate) {
            // Массив
            if (!is_array($translate)) {
                throw new AlertException(trans('app.wrong-post-data-format'));
            }

            // Массив включает индекс text
            if (!array_has($translate, 'text')) {
                throw new AlertException(trans('app.wrong-post-data-format'));
            }
        }
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $this->replace($input);
    }
}