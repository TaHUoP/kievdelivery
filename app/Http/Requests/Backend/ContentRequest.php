<?php

namespace App\Http\Requests\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Models\Content;
use App\Http\Requests\Request;
use Doctrine\DBAL\Platforms\Keywords\MySQL57Keywords;

/**
 * Content Request
 * @package App\Http\Requests\Backend
 */
class ContentRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param TranslateHelperInterface $translateHelper
     * @return array
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;
        $this->sanitize();

        $dl = $translateHelper->getDefaultLang()['locale'];

        return [
            'translations.*.title' => 'string|max:255|required_with:' . $this->with('title', 'translations.*'),
            'translations.*.text' => 'string|required_with:' . $this->with('text', 'translations.*'),
            'translations.*.info' => 'present|string|max:255',
            'translations.*.meta_title' => 'present|string|max:255',
            'translations.*.meta_description' => 'present|string|max:255',
            'translations.*.meta_keywords' => 'present|string|max:255',

            "translations.{$dl}.title" => 'required|string|max:255',
            "translations.{$dl}.text" => 'required|string',
            "translations.{$dl}.info" => 'present|string',
            "translations.{$dl}.meta_title" => 'present|string|max:255',
            "translations.{$dl}.meta_description" => 'present|string|max:255',
            "translations.{$dl}.meta_keywords" => 'present|string|max:255',

            'url' => [
                'bail', 'required', 'max:255',
                'regex:/(^[a-zA-Z_0-9\-\/]+|[a-zA-Z_0-9\-\/]+\.+[a-zA-Z0-9]{1,})$/i',
                'unique:contents,url,' . $this->route()->parameter('contents', null),
            ],
            'type' => 'bail|required|in:' . $this->getTypes(),
            'visible' => 'required|in:0,1',
        ];
    }

    public function attributes()
    {
        return [
            'translations.*.title' => trans('contents.attr.title'),
            'translations.*.text' => trans('contents.attr.text'),
            'translations.*.info' => trans('contents.attr.info'),
            'translations.*.meta_title' => trans('contents.attr.meta_title'),
            'translations.*.meta_description' => trans('contents.attr.meta_description'),
            'translations.*.meta_keywords' => trans('contents.attr.meta_keywords'),
            'url' => trans('contents.attr.url'),
            'type' => trans('contents.attr.type'),
            'visible' => trans('contents.attr.visible'),
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'required_with' => trans('validation.language_required_with'),
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        if (!isset($input['translations'])) {
            return;
        }

        $input['type'] = isset($input['type']) ? $input['type'] : null;
        $input['url'] = isset($input['url']) ? $input['url'] : null;
        $input['visible'] = isset($input['visible']) ? (bool)$input['visible'] : null;

        // удаление пустого перевода
        foreach ($input['translations'] as $key => $lng) {
            if (!array_filter($lng)) {
                unset($input['translations'][$key]);
            }
        }

        // sanitize string
        foreach ($input['translations'] as $locale => $translation) {
            foreach ($this->getTranslationInputs() as $translationInput) {
                if (!isset($translation[$translationInput])) {
                    continue;
                }
                $translation[$translationInput] = filter_var($translation[$translationInput], FILTER_SANITIZE_STRING);
            }
        }

        // назначение language_id к каждому из трансляций
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }
        
        $this->replace($input);
    }

    /**
     * Фильтр данных переводов
     *
     * @param array $data
     * @return array
     */
    protected function sanitizeStringArray(array $data)
    {
        $result = [];
        foreach ($data as $lang => $value) {
            $result[$lang] = filter_var($value, FILTER_SANITIZE_STRING);
        }
        return $result;
    }

    /**
     * Возвращает все типы через запятую в строке
     * @return string
     */
    protected function getTypes()
    {
        return implode(',', array_keys(Content::getTypesArray()));
    }

    /**
     * Возвращает строку для валидации
     *
     * $without - к примеру валидируем name, то в метод передать name
     * $parentKey - к примеру валидируем переменный перевод, передаем translations.*
     * к примеру, ввели то что выше, тогда вернёт translations.*.short, translations.*.full ...
     *
     * @param $without
     * @param $parentKey
     * @return string
     */
    protected function with($without, $parentKey)
    {
        $all = $this->getTranslationInputs();

        if ($foundKey = array_search($without, $all)) {
            unset($all[$foundKey]);
        }

        foreach ($all as $key => &$item) {
            $item = $parentKey . '.' . $item;
        }

        return implode(',', $all);
    }

    /**
     * Все поля для переводов
     * @return array
     */
    protected function getTranslationInputs()
    {
        return [
            'title',
            'text',
            'info',
            'meta_title',
            'meta_description',
            'meta_keywords',
        ];
    }
}