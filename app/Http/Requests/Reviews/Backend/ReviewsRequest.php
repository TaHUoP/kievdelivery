<?php

namespace App\Http\Requests\Reviews\Backend;

use Validator;
use Response;
/**
 * ReviewsRequest
 * @package App\Http\Request
 */
class ReviewsRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'text' => 'required|string',
            'type' => 'present|in:product,main',
            'rating' => 'required|integer|min:0|max:5',
            'name' => 'required|string',
            'email' => 'required|string',
            'city_delivery' => 'required|string',
            'confirmed' => 'present',
            'product_id' => 'required_if:type,product|exists:shop_products,id'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'text' => trans('reviews.attr.text'),
            'type' => trans('reviews.attr.type'),
            'rating' => trans('reviews.attr.rating'),
            'confirmed' => trans('reviews.attr.confirmed'),
            'product_id' => trans('reviews.attr.product_id'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();
        $input['text'] = filter_var(isset($input['text']) ? $input['text'] : null, FILTER_SANITIZE_STRING);
        $input['type'] = filter_var(isset($input['type']) ? $input['type'] : null, FILTER_SANITIZE_STRING);
        $input['rating'] = filter_var(isset($input['rating']) ? $input['rating'] : null, FILTER_SANITIZE_NUMBER_INT);
        $input['user_id'] = filter_var(isset($input['user_id']) ? $input['user_id'] : null, FILTER_SANITIZE_NUMBER_INT);
        $input['confirmed'] = filter_var(isset($input['confirmed']) ? $input['confirmed'] : null, FILTER_SANITIZE_NUMBER_INT);
        if (!$input['product_id']){
            $input['product_id'] = null;
        } else {
            $input['product_id'] = filter_var(isset($input['product_id']) ? $input['product_id'] : null, FILTER_SANITIZE_NUMBER_INT);
        }
        $this->replace($input);
    }

}
