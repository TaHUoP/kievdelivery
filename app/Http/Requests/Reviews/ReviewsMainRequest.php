<?php

namespace App\Http\Requests\Reviews;

use Validator;
use Response;
/**
 * ReviewsRequest
 * @package App\Http\Request
 */
class ReviewsMainRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name' => 'required|string|min:2',
            'email' => 'required|string|min:5',
            'rating' => 'required|integer|max:5|min:0',
            'text' => 'required|string|min:5|max:1500',
            'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'text' => trans('reviews.attr.text'),
            'rating' => trans('reviews.attr.rating'),
            'g-recaptcha-response' => trans ('site.attr.g-recaptcha-response'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();
        $input['text'] = filter_var(isset($input['text']) ? $input['text'] : null, FILTER_SANITIZE_STRING);
        $this->replace($input);
    }

}
