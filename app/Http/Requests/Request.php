<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request
 * @package App\Http\Requests
 */
abstract class Request extends FormRequest
{

}
