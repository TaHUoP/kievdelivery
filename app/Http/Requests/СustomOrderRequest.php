<?php

namespace App\Http\Requests;

use HttpResponseException;
use Validator;
use Response;

/**
 * Сustom Order Request
 * @package App\Http\Requests
 */
class СustomOrderRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name' => 'required|max:255|min:2|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|max:255|min:5',
            'contact_info' => 'required|min:10',
            'address' => 'required|min:20',
            'instruction' => 'present',
            'comment' => 'required|min:10',
            'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'name' => trans('app.attr.name'),
            'email' => trans('app.attr.email'),
            'contact_info' => trans('site.you-want-being-ordered'),
            'address' => trans('site.to-be-delivered-to'),
            'instruction' => trans('site.add-insruct'),
            'comment' => trans('site.message-to-recipient'),
            'g-recaptcha-response' => trans ('site.attr.g-recaptcha-response'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['name'] = filter_var(isset($input['name']) ? $input['name'] : null, FILTER_SANITIZE_STRING);
        $input['email'] = filter_var(isset($input['email']) ? $input['email'] : null, FILTER_SANITIZE_EMAIL);
        $input['contact_info'] = filter_var(isset($input['contact_info']) ? $input['contact_info'] : null, FILTER_SANITIZE_STRING);
        $input['address'] = filter_var(isset($input['address']) ? $input['address'] : null, FILTER_SANITIZE_STRING);
        $input['instruction'] = filter_var(isset($input['instruction']) ? $input['instruction'] : null, FILTER_SANITIZE_STRING);
        $input['comment'] = filter_var(isset($input['comment']) ? $input['comment'] : null, FILTER_SANITIZE_STRING);

        $this->replace($input);
    }

}