<?php

namespace App\Http\Requests;

use HttpResponseException;
use Validator;
use Response;

/**
 * Search Request
 * @package App\Http\Requests
 */
class SearchRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();
        return [];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();
        $input['q'] = filter_var(isset($input['q']) ? $input['q'] : null, FILTER_SANITIZE_STRING);
        $this->replace($input);
    }

}