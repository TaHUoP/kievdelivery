<?php

namespace App\Http\Requests;

use HttpResponseException;
use Validator;
use Response;

/**
 * Contacts Request
 * @package App\Http\Requests
 */
class ContactsRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name' => 'required|max:255|min:2|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|max:255|min:5',
            'message' => 'required|min:10|max:1000',
            'g-recaptcha-response' => 'required|recaptcha',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'name' => trans('app.attr.name'),
            'email' => trans('app.attr.email'),
            'message' => trans('app.attr.message'),
            'g-recaptcha-response' => trans ('site.attr.g-recaptcha-response'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['name'] = filter_var(isset($input['name']) ? $input['name'] : null, FILTER_SANITIZE_STRING);
        $input['email'] = filter_var(isset($input['email']) ? $input['email'] : null, FILTER_SANITIZE_EMAIL);
        $input['message'] = filter_var(isset($input['message']) ? $input['message'] : null, FILTER_SANITIZE_STRING);

        $this->replace($input);
    }

}