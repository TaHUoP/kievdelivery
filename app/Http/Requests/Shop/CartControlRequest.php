<?php

namespace App\Http\Requests\Shop;

use App\Http\Requests\Request;
use HttpResponseException;
use Validator;
use Response;

/**
 * Cart Control Request
 * @package App\Http\Requests
 */
class CartControlRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'data.amount' => 'integer|min:1',
            'productId' => 'integer',
            'action' => 'string',
            'data.method' => 'string|in:minus,plus',
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['action'] = filter_var(isset($input['action']) ? $input['action'] : null, FILTER_SANITIZE_STRING);
        $input['productId'] = isset($input['productId']) ? (int)$input['productId'] : null;
        $input['amount'] = isset($input['data']['amount']) ? (int)$input['data']['amount'] : 1;
        $input['method'] = isset($input['data']['method']) && $input['data']['method'] == 'minus' ? 'minus' : 'plus';

        unset($input['data']);

        $this->replace($input);
    }

}