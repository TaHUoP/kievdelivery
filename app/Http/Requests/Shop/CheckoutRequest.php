<?php

namespace App\Http\Requests\Shop;

use App\Http\Requests\Request;
use Carbon\Carbon;

/**
 * Checkout Request
 * @package App\Http\Requests\Shop
 */
class CheckoutRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        //dd($this->all());
        return [
            // Checkout
            'checkout.first_name' => 'required|min:2|max:255|regex:/^[\pL\s\-]+$/u',
            'checkout.last_name' => 'required|min:2|max:255|regex:/^[\pL\s\-]+$/u',
            'checkout.phone' => 'required|max:255|min:7',

            'checkout.address' =>  'present|min:2|max:255',
            'checkout.email' => 'required|email|max:255|min:5',
            'checkout.country' => 'required|min:2|max:255',

            // Delivery
            'delivery.first_name' => 'required|min:2|max:255|regex:/^[\pL\s\-]+$/u',
            'delivery.last_name' => 'required|min:2|max:255|regex:/^[\pL\s\-]+$/u',
            'delivery.address' => 'present|min:2|max:255',
//            'delivery.city' => 'required|min:2|max:255|regex:/^[\pL\s\-]+$/u',
            'delivery.city' => 'required|exists:cities,id',
            'delivery.phone' => 'required|max:255|min:7',
            'delivery.date' => 'required|date_format:d.m.Y|after:' . Carbon::yesterday()->format('d.m.Y'),

            'agree' => 'required',
            'agree_nbu' => 'required_if:checkout.payment_system,platon|required_if:checkout.payment_system,paypal',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            // Checkout
            'checkout.first_name' => trans('app.attr.name'),
            'checkout.last_name' => trans('app.attr.surname'),
            'checkout.phone' => trans('app.attr.phone'),
            'checkout.email' => trans('app.attr.email'),
            'checkout.city' => trans('app.attr.city'),
            'checkout.country' => trans('app.attr.country'),
            'checkout.address' => trans('app.attr.address'),

            // Delivery
            'delivery.first_name' => trans('app.attr.name'),
            'delivery.last_name' => trans('app.attr.surname'),
            'delivery.phone' => trans('app.attr.phone'),
            'delivery.date' => trans('app.date'),
            'delivery.address' => trans('app.attr.address'),
            'delivery.type' => trans('shop.attr.type'),
            'delivery.post' => trans('shop.attr.post'),
            'delivery.city' => trans('app.attr.city'),
        ];
    }

    public function messages()
    {
        return [
            'delivery.post.required_if' => 'Поле Новая почта обязательно для заполнения, когда "Тип доставки" "Самовывоз".'
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $this->replace($input);
    }
}