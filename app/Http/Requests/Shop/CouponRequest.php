<?php

namespace App\Http\Requests\Shop;

use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use Api\App\Storage\Condition;
use App\Http\Requests\Request;
use App;

/**
 * CouponRequest
 * @package App\Http\Requests\Shop
 */
class CouponRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'coupon'=>'exists:shop_coupons,code|required'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $couponRepository = App::make(CouponInterface::class);

        $condition = new Condition();
        $condition->addCondition('code', 'code', $this->get('coupon'));

        $coupon = $couponRepository->findOne($condition);

        $validator->after(function() use ($validator, $coupon) {
            if (!$coupon || $coupon->ttl <= 0 || $coupon->date_expiry < date("Y-m-d")) {
                $validator->errors()->add('---', '-------');
            }
        });

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'coupon' => trans('shop.attr.discount_code'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['coupon'] = filter_var(isset($input['coupon']) ? $input['coupon'] : null, FILTER_SANITIZE_STRING);

        $this->replace($input);
    }
}