<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;
use Validator;
use Response;

/**
 * Discount Request
 * @package App\Http\Requests\Shop\Backend
 */
class DiscountRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'percents' => 'required|integer|min:0|max:99',
            'date_from' => 'required|date:d.m.Y|before:date_to',
            'date_to' => 'required|date:d.m.Y|after:date_from',
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'percents' => trans('shop.discounts.attr.percents'),
            'date_from' => trans('shop.discounts.attr.date_from'),
            'date_to' => trans('shop.discounts.attr.date_to'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['percentage'] = filter_var(isset($input['percentage']) ? $input['percentage'] : null, FILTER_SANITIZE_NUMBER_INT);

        settype($input['percentage'], 'int');

        $this->replace($input);
    }

}