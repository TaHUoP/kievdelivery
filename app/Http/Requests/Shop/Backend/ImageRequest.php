<?php

namespace App\Http\Requests\Shop\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;

/**
 * Image Request
 * @package App\Http\Requests\Shop\Backend
 */
class ImageRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param TranslateHelperInterface $translateHelper
     * @return array
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;

        // Загрузка изображения
        if ($this->route()->getName() == 'backend.shop.images.upload') {
            return [
                'uploadfile' => 'required|image|max:2048|mimes:jpeg,bmp,png|dimensions:min_width=100,min_height=100',
            ];
        }

        // Сортировка изображений
        if ($this->route()->getName() == 'backend.shop.images.order') {
            $this->sanitizeOrder();
            return [
                'ids' => 'array',
            ];
        }

        // Редактировать изображение
        $this->sanitize();

        $dl = $translateHelper->getDefaultLang('locale');

        return [
            /*'translations.*.title' => 'string|max:255|required_with:' . $this->with('title', 'translations.*'),
            'translations.*.alt' => 'string|max:255|required_with:' . $this->with('title', 'translations.*'),

            "translations.{$dl}.title" => 'required|string|max:255',
            "translations.{$dl}.alt" => 'required|string|max:255',*/

            'translations.*.title' => 'max:255|required_with:' . $this->with('title', 'translations.*'),
            'translations.*.alt' => 'max:255|required_with:' . $this->with('alt', 'translations.*'),

            "translations.{$dl}.title" => 'string|max:255',
            "translations.{$dl}.alt" => 'string|max:255',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'translations.*.title' => trans('app.image.attr.title'),
            'translations.*.alt' => trans('app.image.attr.alt'),
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            //'required_with' => trans('validation.language_required_with'),
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['default'] = (isset($input['default']) && $input['default'] == 'on') ? 1 : 0;
        $input['translations'] = (isset($input['translations'])) ? (array)$input['translations'] : [];

        // Удаление пустого перевода
        foreach ($input['translations'] as $locale => $translation) {
            if (!array_filter($translation)) {
                unset($input['translations'][$locale]);
            }
        }

        // Sanitize string
        foreach ($input['translations'] as $locale => $translation) {
            foreach ($this->getTranslationInputs() as $translationInput) {
                if (!isset($translation[$translationInput])) {
                    continue;
                }
                $translation[$translationInput] = filter_var($translation[$translationInput], FILTER_SANITIZE_STRING);
            }
        }

        // Добавляем lang_id к каждому переводу
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }

        $this->replace($input);
    }

    /**
     * @return array
     */
    public function sanitizeOrder()
    {
        $input = $this->all();

        $input['ids'] = array_map(function ($value) {
            settype($value, 'int');
            return $value;
        }, $input['ids']);

        $this->replace($input);
    }

    /**
     * Возвращает строку для валидации
     *
     * $without - к примеру валидируем name, то в метод передать name
     * $parentKey - к примеру валидируем переменный перевод, передаем translations.*
     * к примеру, ввели то что выше, тогда вернёт translations.*.short, translations.*.full ...
     *
     * @param $without
     * @param $parentKey
     * @return string
     */
    protected function with($without, $parentKey)
    {
        $all = $this->getTranslationInputs();

        if ($foundKey = array_search($without, $all)) {
            unset($all[$foundKey]);
        }

        foreach ($all as $key => &$item) {
            $item = $parentKey . '.' . $item;
        }

        return implode(',', $all);
    }

    /**
     * Все поля для переводов
     * @return array
     */
    protected function getTranslationInputs()
    {
        return [
            'title',
            'alt',
        ];
    }
}