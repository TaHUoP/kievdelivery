<?php

namespace App\Http\Requests\Shop\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Request;

/**
 * Value Request
 * @package App\Http\Requests\Shop\Backend
 */
class ValueRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;
        $this->sanitize();
        $dl = $translateHelper->getDefaultLang('locale');

        return [
            'translations.*.name' => 'string|max:255',
            "translations.{$dl}.name" => 'required|string|max:255',
            'alias' => 'required|regex:/^[a-zA-Z0-9-_.]+$/|unique:shop_values,alias,' . $this->route()->parameter('values', null),
            'property_id' => 'required|integer|exists:shop_properties,id'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'translations.*.name' => trans('app.attr.name'),
            'property_id' => trans('shop.attr.property'),
            'alias' => trans('app.attr.alias'),
            'sort' => trans('shop.attr.value.sort'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();
        $input['translations'] = (isset($input['translations'])) ? (array)$input['translations'] : [];

        foreach ($input['translations'] as $key => $lng) {
            if (!array_filter($lng)) {
                unset($input['translations'][$key]);
            }
        }

        foreach ($input['translations'] as $translation) {
            if (isset($translation['name'])) {
                $translation['name'] = filter_var($translation['name'], FILTER_SANITIZE_STRING);
            }
        }

        // Назначение language_id к каждому из трансляций
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }

        settype($input['property_id'], 'int');
        settype($input['sort'], 'int');

        $this->replace($input);
    }

}