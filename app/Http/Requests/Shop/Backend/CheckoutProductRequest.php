<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;
use HttpResponseException;
use Validator;
use Response;

/**
 * Checkout Product Request
 * @package App\Http\Requests\Shop\Backend
 */
class CheckoutProductRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();


        if (strtoupper($this->getMethod()) == 'POST') {

            return [
                'product_id' => [
                    'required',
                    'integer',
                    'exists:shop_products,id',
                ],
                'checkout_id' => [
                    'required',
                    'integer',
                    'exists:shop_checkout,id',
                ]
            ];

        }

        else if (strtoupper($this->getMethod()) == 'PUT') {

            return [
                'relation_id' => 'exists:shop_cart_products,id',
                'amount' => [
                    'integer',
                    'min:1',
                ]
            ];

        } else if (strtoupper($this->getMethod()) == 'DELETE') {

            return [
                'relation_id' => 'exists:shop_cart_products,id'
            ];

        }
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            // 'name' => trans('shop.coupons.attr.name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['product_id'] = isset($input['product_id']) ? (int)$input['product_id'] : null;
        $input['checkout_id'] = isset($input['checkout_id']) ? (int)$input['checkout_id'] : null;

        $this->replace($input);
    }

}