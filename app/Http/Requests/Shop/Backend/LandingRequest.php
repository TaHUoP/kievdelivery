<?php

namespace App\Http\Requests\Shop\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Request;

/**
 * Product Request
 * @package App\Http\Requests\Shop\Backend
 */
class LandingRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;
        return [
            'url' => 'string|max:255|required_with:'
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'url' => trans('shop.product.attr.name'),
        ];
    }

}