<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;
use App\Models\Shop\Checkout;
use Carbon\Carbon;

/**
 * Checkout Request
 * @package App\Http\Requests\Shop\Backend
 */
class CheckoutRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            // Checkout
            'checkout.first_name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'checkout.last_name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'checkout.phone' => 'required|max:255|min:7',
            'checkout.email' => 'required|email|max:255|min:5',
            'checkout.status' => 'required|in:' . implode(',', array_keys(Checkout::getStatusArray())),
            'checkout.paid' => 'required|boolean',
            // TODO: валидация 'checkout.address' => 'min:6|max:255',
            // TODO: валидация 'checkout.payment_system' => 'required|in:cash,card,paypal',

            // Delivery
            'delivery.first_name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'delivery.last_name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
            'delivery.phone' => 'required|max:255|min:7',
            'delivery.date' => 'required|date_format:d.m.Y',
            'delivery.city' => 'required|string|min:2|max:255',
            'delivery.address' => 'present|string|min:6|max:255',
            'delivery.comment_recipient' => 'present|string|min:1',
            'delivery.comment_note' => 'present|string|min:1',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            // Checkout
            'checkout.first_name' => trans('app.attr.name'),
            'checkout.last_name' => trans('app.attr.surname'),
            'checkout.email' => trans('app.attr.email'),
            'checkout.address' => trans('app.attr.address'),
            'checkout.phone' => trans('app.attr.phone'),
            'checkout.country' => trans('app.attr.country'),
            'checkout.discount_code' => trans('shop.attr.discount_code'),
            'checkout.payment_system' => trans('shop.attr.payment'),

            // Delivery
            'delivery.first_name' => trans('app.attr.name'),
            'delivery.last_name' => trans('app.attr.surname'),
            'delivery.address' => trans('app.attr.address'),
            'delivery.phone' => trans('app.attr.phone'),
            //'delivery.country' => trans('app.attr.country'),
            'delivery.city' => trans('app.attr.city'),
            'delivery.date' => trans('app.attr.date'),
            'delivery.comment_recipient' => trans('shop.attr.enter-message-to-recipient'),
            'delivery.comment_note' => trans('shop.attr.your-instroction-to-kievdelivery'),
            'delivery.shipment' => trans('shop.attr.shipment'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        // filster string
        $input['delivery']['date'] = filter_var(isset($input['delivery']['date']) ? $input['delivery']['date'] : null, FILTER_SANITIZE_STRING);
        $input['delivery']['city'] = filter_var(isset($input['delivery']['city']) ? $input['delivery']['city'] : null, FILTER_SANITIZE_STRING);
        $input['delivery']['address'] = filter_var(isset($input['delivery']['address']) ? $input['delivery']['address'] : null, FILTER_SANITIZE_STRING);
        $input['delivery']['comment_recipient'] = filter_var(isset($input['delivery']['comment_recipient']) ? $input['delivery']['comment_recipient'] : null, FILTER_SANITIZE_STRING);
        $input['delivery']['comment_note'] = filter_var(isset($input['delivery']['comment_note']) ? $input['delivery']['comment_note'] : null, FILTER_SANITIZE_STRING);
        $input['delivery']['shipment'] = isset($input['delivery']['shipment']) ? (bool)$input['delivery']['shipment'] : 0;

        $this->replace($input);
    }

}