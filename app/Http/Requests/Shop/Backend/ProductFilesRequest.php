<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;

/**
 * Product Files Request Request
 * @package App\Http\Requests\Shop\Backend
 */
class ProductFilesRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'files' => 'filled|array',
            'files.*.name' => 'required|string',
            'files.*.url' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'files.*.name' => trans('shop.product.attr.file_name'),
            'files.*.url' => trans('shop.product.attr.file_url')
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();


        $this->replace($input);
    }

}