<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;
use Validator;
use Response;

/**
 * Coupon Request
 * @package App\Http\Requests\Shop\Backend
 */
class CouponRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name' => 'required|max:255',
            'date_expiry' => 'required|date:d.m.Y',
            'code' => 'required|max:255|unique:shop_coupons,code,' . $this->route()->parameter('coupons', null),
            'ttl' => 'required|integer|min:0',
            'percentage' => 'required|integer|min:0|max:99',
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'name' => trans('shop.coupons.attr.name'),
            'date_expiry' => trans('shop.coupons.attr.date_expiry'),
            'code' => trans('shop.coupons.attr.code'),
            'ttl' => trans('shop.coupons.attr.ttl'),
            'percentage' => trans('shop.coupons.attr.percentage'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['name'] = filter_var(isset($input['name']) ? $input['name'] : null, FILTER_SANITIZE_STRING);
        $input['ttl'] = filter_var(isset($input['ttl']) ? $input['ttl'] : null, FILTER_SANITIZE_NUMBER_INT);
        $input['percentage'] = filter_var(isset($input['percentage']) ? $input['percentage'] : null, FILTER_SANITIZE_NUMBER_INT);

        settype($input['ttl'], 'int');
        settype($input['percentage'], 'int');

        $this->replace($input);
    }

}