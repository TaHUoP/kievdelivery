<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;

/**
 * Filter Request
 * @package App\Http\Requests\Shop\Backend
 */
class FilterRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['filter'] = (isset($input['filter'])) ? (array)$input['filter'] : [];

        $result = [];

        foreach ($input['filter'] as $propertyId => $valueId) {
            $result[(int)$propertyId] = (int)$valueId;
        }

        $this->replace($result);
    }

}