<?php

namespace App\Http\Requests\Shop\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use App\Http\Requests\Request;

/**
 * Category Request
 * @package App\Http\Requests\Shop\Backend
 */
class CategoryRequest extends Request
{
    /**
     * @var TranslateInterface
     */
    protected $translateHelper;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;

        $this->sanitize();
        $dl = $translateHelper->getDefaultLang('locale');

        return [
            'translations.*.name' => 'string|max:255',
            'translations.*.meta_title' => 'string|max:255',
            'translations.*.meta_description' => 'string|max:255',
            'translations.*.meta_keywords' => 'string|max:255',
            'translations.*.description_full' => 'string',

            "translations.{$dl}.name" => 'present|max:255',
            "translations.{$dl}.meta_title" => 'present|max:255',
            "translations.{$dl}.meta_description" => 'present|max:255',
            "translations.{$dl}.meta_keywords" => 'present|max:255',
            "translations.{$dl}.description_full" => 'present',

            'alias' => 'required|regex:/^[a-zA-Z0-9-_.]+$/|unique:shop_categories,alias,' . $this->route()->parameter('categories', null),
            'parent_id' => 'present|exists:shop_categories,id',
            'order' => 'integer|min:1',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'translations.*.name' => trans('app.attr.name'),
            'translations.*.meta_title' => trans('app.attr.meta_title'),
            'translations.*.meta_description' => trans('app.attr.meta_description'),
            'translations.*.meta_keywords' => trans('app.attr.meta_keywords'),
            'translations.*.description_full' => trans('shop.attr.description_full'),
            'order' => trans('app.attr.order'),
            'alias' => trans('app.attr.alias'),
            'parent_id' => trans('shop.category_parent'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['order'] = isset($input['order']) ? (int)$input['order'] : 0;
        $input['visible'] = isset($input['visible']) ? (bool)$input['visible'] : 0;
        $input['recommended'] = isset($input['recommended']) ? (bool)$input['recommended'] : 0;
        $input['translations'] = (isset($input['translations'])) ? (array)$input['translations'] : [];

        foreach ($input['translations'] as $key => $lng) {
            if (!array_filter($lng)) {
                unset($input['translations'][$key]);
            }
        }

        // Filter string
        foreach ($input['translations'] as $translation) {
            if (isset($translation['name'])) {
                $translation['name'] = filter_var($translation['name'], FILTER_SANITIZE_STRING);
                $translation['meta_title'] = filter_var($translation['meta_title'], FILTER_SANITIZE_STRING);
                $translation['meta_description'] = filter_var($translation['meta_description'], FILTER_SANITIZE_STRING);
                $translation['meta_keywords'] = filter_var($translation['meta_keywords'], FILTER_SANITIZE_STRING);
                $translation['description_full'] = filter_var($translation['description_full'], FILTER_SANITIZE_STRING);
            }
        }

        // Назначение language_id к каждому из трансляций
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }

        $input['alias'] = filter_var(isset($input['alias']) ? $input['alias'] : null, FILTER_SANITIZE_STRING);
        $input['parent_id'] = isset($input['parent_id']) ? (int)$input['parent_id'] : 0;
        $input['properties'] = isset($input['properties']) ? $input['properties'] : [];
        $input['properties'] = is_array($input['properties']) ? $input['properties'] : [];
        
        foreach ($input['properties'] as &$propertyId) {
            settype($propertyId, 'int');
        }

        $this->replace($input);
    }

}