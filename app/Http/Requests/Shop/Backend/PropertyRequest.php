<?php

namespace App\Http\Requests\Shop\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Request;

/**
 * Property Request
 * @package App\Http\Requests\Shop\Backend
 */
class PropertyRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;
    
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;
        $this->sanitize();
        $dl = $translateHelper->getDefaultLang('locale');

        return [
            'translations.*.name' => 'string|max:255',
            "translations.{$dl}.name" => 'required|string|max:255',
            'alias' => 'required|regex:/^[a-zA-Z0-9-_.]+$/|unique:shop_properties,alias,' . $this->route()->parameter('properties', null),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'translations.*.name' => trans('app.attr.name'),
            'alias' => trans('app.attr.alias'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['is_filter'] = isset($input['is_filter']) ? (bool)$input['is_filter'] : 0;
        $input['translations'] = (isset($input['translations'])) ? (array)$input['translations'] : [];

        foreach ($input['translations'] as $key => $lng) {
            if (!array_filter($lng)) {
                unset($input['translations'][$key]);
            }
        }

        foreach ($input['translations'] as $translation) {
            if (isset($translation['name'])) {
                $translation['name'] = filter_var($translation['name'], FILTER_SANITIZE_STRING);
            }
        }

        // назначение language_id к каждому из трансляций
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }

        $this->replace($input);
    }

}