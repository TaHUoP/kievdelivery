<?php

namespace App\Http\Requests\Shop\Backend;

use App\Http\Requests\Request;
use Nette\Application\BadRequestException;

/**
 * Filter Request
 * @package App\Http\Requests\Shop\Backend
 */
class FilterParamRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();

        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        if (! $this->has("property_id") || ! $this->has("value_id")) {
            throw new BadRequestException("Expected props");
        }
    }

}