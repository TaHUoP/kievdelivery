<?php

namespace App\Http\Requests\Shop\Backend;

use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Request;

/**
 * Product Request
 * @package App\Http\Requests\Shop\Backend
 */
class ProductRequest extends Request
{
    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules(TranslateHelperInterface $translateHelper)
    {
        $this->translateHelper = $translateHelper;
        $this->sanitize();

        $dl = $translateHelper->getDefaultLang('locale');

        return [
            // Translations
            'translations.*.name' => 'string|max:255|required_with:' . $this->with('name', 'translations.*'),
            'translations.*.description_short' => 'string|max:255|required_with:' . $this->with('description_short', 'translations.*'),
            'translations.*.description_full' => 'string|required_with:' . $this->with('description_full', 'translations.*'),
            'translations.*.meta_title' => 'present|string|max:255',
            'translations.*.meta_description' => 'present|string|max:255',
            'translations.*.meta_keywords' => 'present|string|max:255',

            // Default language translations
            "translations.{$dl}.name" => 'required|string|max:255',
            "translations.{$dl}.description_short" => 'string|max:255',
            "translations.{$dl}.description_full" => 'string',
            "translations.{$dl}.meta_title" => 'present|string|max:255',
            "translations.{$dl}.meta_description" => 'present|string|max:255',
            "translations.{$dl}.meta_keywords" => 'present|string|max:255',

            'price' => 'required|numeric|min:0',
            'amount' => 'required|integer|min:0',
            'alias' => 'required|regex:/^[a-zA-Z0-9-_.]+$/|unique:shop_products,alias,' . $this->route()->parameter('products', null),
            'vendor_code' => 'required|string',
            'visible' => 'in:on',
            'pre_order' => 'in:on',
            'main' => 'in:on',

            //'files' => 'пока пусто',
            //'categories' => 'пока пусто'
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
            'required_with' => trans('validation.language_required_with'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'translations.*.name' => trans('shop.product.attr.name'),
            'translations.*.description_short' => trans('shop.product.attr.description_short'),
            'translations.*.description_full' => trans('shop.product.attr.description_full'),
            'translations.*.meta_title' => trans('shop.product.attr.meta_title'),
            'translations.*.meta_description' => trans('shop.product.attr.meta_description'),
            'translations.*.meta_keywords' => trans('shop.product.attr.meta_keywords'),
            'price' => trans('shop.product.attr.price'),
            'amount' => trans('shop.product.attr.amount'),
            'alias' => trans('shop.product.attr.alias'),
            'vendor_code' => trans('shop.product.attr.vendor_code'),
            'mpn_code' => trans('shop.product.attr.mpn_code'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->input();

        // Пустые значения в null
        $input['main'] = isset($input['main']) ? (bool)$input['main'] : 0;
        $input['visible'] = isset($input['visible']) ? (bool)$input['visible'] : 0;
        $input['pre_order'] = isset($input['pre_order']) ? (bool)$input['pre_order'] : 0;
        $input['translations'] = (isset($input['translations'])) ? (array)$input['translations'] : [];


        foreach ($input['translations'] as $key => $lng) {
            if (!array_filter($lng)) {
                unset($input['translations'][$key]);
            }
        }

        // Set type
        settype($input['price'], 'float');
        settype($input['amount'], 'int');

        // Filter var
        foreach ($input['translations'] as $translation) {
            foreach ($this->getTranslationInputs() as $translationInput) {
                if (!isset($translation[$translationInput])) {
                    continue;
                }
                $translation[$translationInput] = filter_var($translation[$translationInput], FILTER_SANITIZE_STRING);
            }
        }

        // Добавляем lang_id к каждому переводу
        foreach ($input['translations'] as $locale => &$translation) {
            $translation['lang_id'] = $this->translateHelper->getLangByLocale($locale)['id'];
        }

        $this->replace($input);
    }

    /**
     * Возвращает строку для валидации
     *
     * $without - к примеру валидируем name, то в метод передать name
     * $parentKey - к примеру валидируем переменный перевод, передаем translations.*
     * к примеру, ввели то что выше, тогда вернёт translations.*.short, translations.*.full ...
     *
     * @param $without
     * @param $parentKey
     * @return string
     */
    protected function with($without, $parentKey)
    {
        $all = $this->getTranslationInputs();

        if ($foundKey = array_search($without, $all)) {
            unset($all[$foundKey]);
        }

        foreach ($all as $key => &$item) {
            $item = $parentKey . '.' . $item;
        }

        return implode(',', $all);
    }

    /**
     * Все поля для переводов
     * @return array
     */
    protected function getTranslationInputs()
    {
        return [
            'name',
            'description_short',
            'description_full',
            'meta_title',
            'meta_description',
            'meta_keywords',
        ];
    }
}