<?php

namespace App\Http\Requests\Shop;

use App\Http\Requests\Request;
use App\Models\Shop\Product;

/**
 * Buy Now Request
 * @package App\Http\Shop\Requests
 */
class BuyNowRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $this->sanitize();
        return [
            //'product_id' => 'present|exists:' . Product::getTableName() . ',id',
            'product_id' => 'present',
        ];
    }

    public function attributes()
    {
        return [
            'product_id' => trans('shop.product'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function sanitize()
    {
        $input = $this->all();
        $input['product_id'] = filter_var(isset($input['product_id']) ? $input['product_id'] : null, FILTER_SANITIZE_NUMBER_INT);
        $this->replace($input);
    }

}