<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use Auth;

/**
 * Change Email Request
 * @package App\Http\Requests\User
 */
class ChangeEmailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = Auth::user()->id;
        return [
            'cur_email' => 'required|email|max:255|exists:users,email,id,' . $user_id,
            'new_email' => 'required|email|max:255|unique:users,email',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'cur_email' => trans('users.attr.cur-email'),
            'new_email' => trans('users.attr.new-email'),
        ];
    }
}