<?php

namespace App\Http\Requests\User\Backend;

/**
 * User Request
 * @package App\Http\Requests\User\Backend
 */
class UserRequest extends \App\Http\Requests\Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function rules()
    {
        $this->sanitize();

        $id = (int)$this->route()->parameter('users');

        if (strtoupper($this->method()) == 'PUT') {
            return [
                'name' => 'required|alpha|max:255|min:2',
                'surname' => 'required|alpha|max:255|min:2',
                'avatar' => 'image|max:2048|dimensions:min_width=200,min_height=200',

                'email' => 'required|email|max:255|min:5|unique:users,email,' . $id,
                'phone' => 'required|max:255|min:7|unique:users,phone,' . $id,
                'new_password' => ['present', 'min:6', 'max:70', 'password', 'confirmed'],
                'new_password_confirmation' => 'present',
            ];
        } else if (strtoupper($this->method()) == 'POST') {
            return [
                'name' => 'required|alpha|max:255|min:2',
                'surname' => 'required|alpha|max:255|min:2',
                'avatar' => 'image|max:2048|dimensions:min_width=200,min_height=200',

                'phone' => 'required|max:255|min:7|unique:users,phone',
                'email' => 'required|email|max:255|min:5|unique:users,email',
                'password' => ['required', 'min:6', 'max:70', 'password', 'confirmed'],
            ];
        }
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'name' => trans('app.attr.name'),
            'surname' => trans('app.attr.surname'),
            'access_cpanel' => trans('app.attr.cpanel_access'),
            'status' => trans('app.attr.status'),
            'avatar' => trans('app.attr.image'),
            'email' => trans('app.attr.email'),
            'phone' => trans('app.attr.phone'),
            'password' => trans('users.attr.cur-password'),
            'new_password' => trans('users.attr.new-password'),
            'new_password_confirmation' => trans('users.attr.new-password-confirmation'),
        ];
    }

    /**
     * @return array
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['name'] = filter_var(isset($input['name']) ? $input['name'] : null, FILTER_SANITIZE_STRING);
        $input['status'] = filter_var(isset($input['status']) ? $input['status'] : null, FILTER_SANITIZE_STRING);
        $input['surname'] = filter_var(isset($input['surname']) ? $input['surname'] : null, FILTER_SANITIZE_STRING);
        $input['access_cpanel'] = isset($input['access_cpanel']) ? (bool)$input['access_cpanel'] : 0;
        $input['status'] = isset($input['status']) ? (bool)$input['status'] : 0;
        $input['avatar'] = filter_var(isset($input['avatar']) ? $input['avatar'] : null, FILTER_SANITIZE_STRING);
        $input['phone'] = filter_var(isset($input['phone']) ? $input['phone'] : null, FILTER_SANITIZE_STRING);
        $input['email'] = filter_var(isset($input['email']) ? $input['email'] : null, FILTER_SANITIZE_STRING);
        $input['password'] = filter_var(isset($input['password']) ? $input['password'] : null, FILTER_SANITIZE_STRING);
        $input['new_password'] = filter_var(isset($input['new_password']) ? $input['new_password'] : null, FILTER_SANITIZE_STRING);
        $input['new_password_confirmation'] = filter_var(isset($input['new_password_confirmation']) ? $input['new_password_confirmation'] : null, FILTER_SANITIZE_STRING);

        $this->replace($input);
    }
}