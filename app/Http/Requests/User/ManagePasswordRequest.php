<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Profile Request
 * @package App\Http\Requests\User
 */
class ManagePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cur_password' => 'required|max:70',
            'new_password' => ['required', 'min:6', 'max:70', 'password', 'confirmed'],
        ];
    }
}