<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Main Info Request
 * @package App\Http\Requests\User
 */
class MainInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return [
            'name' => 'required|max:255|alpha',
            'surname' => 'required|max:255|alpha',
        ];
    }

    /**
     * @return void
     */
    public function sanitize()
    {
        $input = $this->all();

        $input['name'] = trim($input['name']);
        $input['surname'] = trim($input['surname']);

        $this->replace($input);
    }
}