<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Signin Request
 * @package App\Http\Requests\User
 */
class SigninRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:5|max:255',
            'password' => 'required|max:70',
        ];
    }

    public function attributes()
    {
        return [
            'email' => trans('auth.attr.email'),
            'password' => trans('auth.attr.password')
        ];
    }
}
