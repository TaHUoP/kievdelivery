<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Signup Request
 * @package App\Http\Requests
 */
class OAuthSignupRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'user_profile_id' => 'required|exists:users_profile,id',
            'email' => 'required|email|max:255|min:5|unique:users',
            'phone' => 'required|max:255|min:7|unique:users',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'email' => trans('auth.attr.email'),
            'phone' => trans('auth.attr.phone'),
        ];
    }
}
