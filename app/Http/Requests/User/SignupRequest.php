<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Signup Request
 * @package App\Http\Requests
 */
class SignupRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255|min:5|unique:users',
            'password' => 'required|min:6|max:70|password|confirmed',
            'name' => 'required|max:255|alpha',
            'surname' => 'required|max:255|alpha',
            'phone' => 'required|max:255|min:7|unique:users',
            'terms-and-conditions' => 'accepted',
            'g-recaptcha-response' => 'required|captcha'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'name' => trans('auth.attr.name'),
            'surname' => trans('auth.attr.surname'),
            'password' => trans('auth.attr.password'),
            'email' => trans('auth.attr.email'),
            'phone' => trans('auth.attr.phone'),
            'terms-and-conditions' => trans('auth.attr.terms-and-conditions'),
            'g-recaptcha-response' => trans('auth.attr.g-recaptcha-response'),
        ];
    }
}
