<?php

use Illuminate\Support\Facades\Config;

function createPaginationUrl($i, $paginator, array $params) {
    $paramsString = '';
    if (is_array($params) && !empty($params)) {
        $params['page'] = (int)$i;
        $paramsString = http_build_query($params);
    }
    if (!empty($paramsString)) {
        return url()->current() . '?' . $paramsString;
    }
    return $paginator->url($i);
}

function sanitize_title_with_dashes( $title ) {
    $title = strip_tags($title);
    // Preserve escaped octets.
    $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
    // Remove percent signs that are not part of an octet.
    $title = str_replace('%', '', $title);
    // Restore octets.
    $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

    $title = strtolower($title);

    // Convert nbsp, ndash and mdash to hyphens
    $title = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $title );
    // Convert nbsp, ndash and mdash HTML entities to hyphens
    $title = str_replace( array( '&nbsp;', '&#160;', '&ndash;', '&#8211;', '&mdash;', '&#8212;' ), '-', $title );

    // Strip these characters entirely
    $title = str_replace( array(
        // iexcl and iquest
        '%c2%a1', '%c2%bf',
        // angle quotes
        '%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
        // curly quotes
        '%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
        '%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
        // copy, reg, deg, hellip and trade
        '%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
        // acute accents
        '%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
        // grave accent, macron, caron
        '%cc%80', '%cc%84', '%cc%8c',
    ), '', $title );

    // Convert times to x
    $title = str_replace( '%c3%97', 'x', $title );

    $title = preg_replace('/&.+?;/', '', $title); // kill entities
    $title = str_replace('.', '-', $title);

    $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
    $title = preg_replace('/\s+/', '-', $title);
    $title = preg_replace('|-+|', '-', $title);

    return $title;
}

function cdn($asset, $globally = false, $upload = false){

    // Verify if KeyCDN URLs are present in the config file
    if( !Config::get('app.cdn') )
        return Statics::asset( $asset, $globally );

    // Get file name incl extension and CDN URLs
    $cdn = Config::get('app.cdn');
    if (!$upload)
        $cdn = $globally ? $cdn . '/statics' : $cdn . '/statics/frontend/KievDelivery';

    $assetName = basename( $asset );

    // Remove query string
    $assetName = explode("?", $assetName);
    $assetName = $assetName[0];

    // Select the CDN URL based on the extension
//    foreach( $cdns as $cdn => $types ) {
//        if( preg_match('/^.*\.(' . $types . ')$/i', $assetName) )
//            return cdnPath($cdn, $asset);
//    }

    // In case of no match use the last in the array
//    end($cdns);
    return cdnPath( $cdn , $asset);

}

function cdnPath($cdn, $asset) {
    return  "//" . rtrim($cdn, "/") . "/" . ltrim( $asset, "/");
}