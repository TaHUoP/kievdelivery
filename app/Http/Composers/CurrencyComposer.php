<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Helpers\CurrencyHelper as CurrencyInterface;
/**
 * Currency Composer
 *
 * @package App\Http\Composers
 */
class CurrencyComposer
{
    /**
     * @var null
     */
    protected $currencyHelper;
    /**
     * @var null
     */
    private $data;

    /**
     * @param CurrencyInterface $currencyHelper
     */
    public function __construct(CurrencyInterface $currencyHelper)
    {
        $this->currencyHelper = $currencyHelper;
    }

    /**
     * Информация о валютах
     *
     * Формируем следующие данные:
     *  - Все активные валюты
     *  - Текущая (указанная пользователем) валюта
     *  - валюта по умолчанию
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }
        
        $currencies = $this->currencyHelper->getActiveCurrencies();

        $data = [
            'all' => $currencies ?: [],
            'current' => $this->currencyHelper->getUserCurrency(),
            'default' => $this->currencyHelper->config('default'),
        ];

        $this->data = $data;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('currencies', $this->getData());
    }

}

