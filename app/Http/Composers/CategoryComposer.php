<?php

namespace App\Http\Composers;

use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use Illuminate\Contracts\View\View;
use Api\App\Storage\Condition;
use Config;
use Route;

/**
 * Language Composer
 *
 * Внимание:
 * Данный composer объявляется для всех представлений, поэтому
 * является singleton.
 *
 * @package App\Http\Composers
 */
class CategoryComposer
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var CategoryInterface
     */
    private $categoryRepository;

    /**
     * @var TranslateInterface
     */
    private $translateHelper;

    /**
     * @param CategoryInterface $categoryRepository
     * @param TranslateInterface $translateHelper
     */
    public function __construct(CategoryInterface $categoryRepository, TranslateInterface $translateHelper)
    {
        $this->categoryRepository = $categoryRepository;
        $this->translateHelper = $translateHelper;
    }

    /**
     * Информация о категориях
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $condition = new Condition();
        $condition->addCondition('alias', 'alias', 'root')
            ->scopes(['visible' => 1]);

        $categories = $this->categoryRepository->findDescendantsByParent($condition);

        $current = $all = [];
        $currentAlias = Route::current() ? Route::input('alias') : null;
        $currentDescendant = null;

        foreach ($categories as $category) {
            $all[] = [
                'id' => $category->id,
                'name' => $this->translateHelper->get($category->translations, 'name'),
                'menu_name' => $this->translateHelper->get($category->translations, 'menu_name'),
                'alias' => $category->alias,
                // TODO: должно быть в репозитории
                'children' => $category->descendants()
                    ->defaultOrder()
                    ->where('visible', 1)
                    ->with('translations')
                    ->get()
            ];
            if (!$current) {
                if ($currentAlias === $category->alias) {
                    $current = $category;
                    continue;
                }
                foreach ($category->descendants as $descendant) {
                    if ($currentAlias === $descendant->alias) {
                        $current = $category;
                        $currentDescendant = $descendant;
                        break;
                    }
                }
            }
        }

        $data = [
            'all' => $all,
            'current' => $current,
            'currentDescendant' => $currentDescendant,
        ];

        $this->data = $data;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('categoryControl', $this->getData());
    }

}