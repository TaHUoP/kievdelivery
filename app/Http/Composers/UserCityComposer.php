<?php

namespace App\Http\Composers;

use App\Modules\CatalogFilterModule\Facade as FacadeFilter;
use Illuminate\Contracts\View\View;
use Config;
use Translate;
use App\Models\City;
use Illuminate\Support\Facades\Cookie;

/**
 * Countries Composer
 *
 * @package App\Http\Composers
 */
class UserCityComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * @var string|null
     */
    private $aliasUrl = null;

    /**
     * @param string|null $alias
     * @return City|array
     */
    public function getData(string $alias = null)
    {
        if ($this->data !== null && is_null($alias)) {
            return $this->data;
        }

        $city_alias = $alias;
        if (is_null($alias)) {
            $city_alias = Cookie::get('city_alias', 'kiev');
        }

        $user_city = City::where('alias', $city_alias)->translates()->first();

        if (! $user_city)
            $user_city = City::where('alias', City::DEFAULT_CITY)->translates()->first();

        if (! $user_city)
            $user_city = new City;

        $this->data = $user_city;

        return $this->data;
    }

    /**
     * @param string $cityAlias
     */
    public function setCookieCityByUrl(string $cityAlias): void
    {
        Cookie::queue('city_alias', $cityAlias, 43200);
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $alias = null;
        if (is_null($this->aliasUrl)) {
            $alias = $this->getAliasUrl();
        }
        $view->with('user_city', $this->getData($alias));
    }

    /**
     * @return string|null
     */
    public function getAliasUrl(): ?string
    {
        if (is_null($this->aliasUrl)) {
            $this->aliasUrl = '';
            if (FacadeFilter::isCity()) {
                $this->aliasUrl = FacadeFilter::getCityByUrl()->getAlias();
            }
        }
        return $this->aliasUrl === "" ? null : $this->aliasUrl;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        $alias = "";
        if ($this->getData() instanceof City) {
            $alias = $this->getData()->getAlias();
        }
        return  $alias;
    }

}

