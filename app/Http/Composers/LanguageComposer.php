<?php

namespace App\Http\Composers;

use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Contracts\View\View;
use Request;
use Config;

/**
 * Language Composer
 *
 * Внимание:
 * Данный composer объявляется для всех представлений, поэтому
 * является singleton.
 *
 * @package App\Http\Composers
 */
class LanguageComposer
{
    /**
     * @var null
     */
    private $languageRepository;

    /**
     * @var null
     */
    private $data;

    /**
     * @param LanguageInterface $languageRepository
     * @param ConditionInterface $condition
     */
    public function __construct(LanguageInterface $languageRepository, ConditionInterface $condition, TranslateInterface $translate)
    {
        $this->languageRepository = $languageRepository;
        $this->condition = $condition;
        $this->translate = $translate;
    }

    /**
     * Информация о языках
     *
     * Формируем следующие данные:
     *  - Все активные языки
     *  - Текущий (указанный пользователем) язык
     *  - Язык по умолчанию
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $currentLocale = Config::get('app.locale');
        $languages = $this->languageRepository->findActiveAll();

        $data = [
            'all' => $languages ?: [],
            'current' => null,
            'default' => null,
        ];

        foreach ($data['all'] as &$lang) {
            if (!$data['default'] && $lang['default']) {
                $data['default'] = $lang;
            }
            if (!$data['current'] && $currentLocale == $lang['locale']) {
                $data['current'] = $lang;
            }
            $lang['short_name'] = mb_substr($lang['name'], 0, 3);

            $lang['path'] = $this->translate->localizePath($lang['locale']);
            
            unset($lang['created_at']);
            unset($lang['updated_at']);
            unset($lang['deleted_at']);
        }

        if (!$data['current']) {
            $data['current'] = $data['default'];
        }

        $this->data = $data;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('languageControl', $this->getData());
    }

}

