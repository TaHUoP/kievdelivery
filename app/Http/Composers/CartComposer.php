<?php

namespace App\Http\Composers;

use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Helpers\CurrencyHelper as CurrencyInterface;
use Illuminate\Contracts\View\View;
use App\Services\Shop\CartService;
use Config;

/**
 * Cart Composer
 *
 * @package App\Http\Composers
 */
class CartComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * @var CartInterface
     */
    protected $cartService;

    /**
     * @var CurrencyInterface
     */
    protected $currencyHelper;

    /**
     * @param CartService $cartService
     * @param CurrencyInterface $currencyHelper
     */
    public function __construct(
        CartService $cartService,
        CurrencyInterface $currencyHelper
    )
    {
        $this->currencyHelper = $currencyHelper;
        $this->cartService = $cartService;
    }

    /**
     * Информация о корзине
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $cart = $this->cartService->getActualInfo();

        $countProducts = $totalPrice = 0;
        $totalPriceString = $this->currencyHelper->getMoneyFormat(0);

        if ($cart) {
            $totalPrice = $cart->total_price;
            $countProducts = $this->cartService->countTotalProducts($cart);
            $totalPriceString = $this->currencyHelper->getMoneyFormat($totalPrice);
        }

        $data = [
            'countProducts' => $countProducts,
            'totalPrice' => $totalPrice,
            'totalPriceString' => $totalPriceString,
        ];

        $this->data = $data;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('cartControl', $this->getData());
    }

}

