<?php

namespace App\Http\Composers;

use Api\App\Contracts\Entities\CountriesRepository as CountriesInterface;
use Illuminate\Contracts\View\View;
use Api\App\Storage\Condition;
use Config;
use Translate;

/**
 * Countries Composer
 *
 * @package App\Http\Composers
 */
class CountriesComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * @var CountriesInterface
     */
    protected $countriesRepository;

    /**
     * CountriesComposer constructor.
     * @param CountriesInterface $countriesRepository
     */
    public function __construct(
        CountriesInterface $countriesRepository
    )
    {
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Выбор стран
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $condition = new Condition();
        $condition
            ->limit(null)
            ->scopes(['visible' => 1]);

        $countries = $this->countriesRepository->findAll($condition);

        $newCountry = [];
        $priorityCountry = [];

        foreach ($countries as $country) {
            if($country->priority) {
                $priorityCountry[$country->alias] = Translate::get($country->translations, 'name');
            } else {
                $newCountry[$country->alias] = Translate::get($country->translations, 'name');
            }
        }

        array_multisort($newCountry);

        $this->data = array_merge($priorityCountry, $newCountry);

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('countries', $this->getData());
    }

}

