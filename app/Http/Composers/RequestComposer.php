<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Request;
use Config;
use Route;

/**
 * Request Composer
 *
 * @package App\Http\Composers
 */
class RequestComposer
{
    /**
     * @var null
     */
    private $data;

    /**
     * Информация о корзине
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }
        $path = Request::path();
        $route = Route::current() ? Route::current()->getName() : null;

        $section = null;

        switch ($route) {

            // Пользователи
            case 'backend.user.users.index':
            case 'backend.user.users.actions':
            case 'backend.user.users.create':
            case 'backend.user.users.edit':
            case 'backend.access.index':

                // Контент
            case 'backend.contents.index':
            case 'backend.contents.create':
            case 'backend.contents.edit':

                // Виджеты
            case 'backend.plugins.index':
            case 'backend.plugins.create':
            case 'backend.plugins.edit':

                // SEO
            case 'backend.seo.index':
            case 'backend.seo.create':
            case 'backend.seo.edit':

                // Языки
            case 'backend.languages.index':
            case 'backend.languages.create':
            case 'backend.languages.edit':
            case 'backend.languages.translates':

                // Другое
            case 'backend.site.settings':
            case 'backend.site.logs':
                $section = 'system';
                break;

            case 'backend.shop.categories.index':
            case 'backend.shop.filter.index':

                // Product
            case 'backend.shop.products.index':
            case 'backend.shop.products.create':
            case 'backend.shop.products.edit':

                // Checkout
            case 'backend.shop.checkouts.index':
            case 'backend.shop.checkouts.create':
            case 'backend.shop.checkouts.edit':

                // Coupon
            case 'backend.shop.coupons.index':
            case 'backend.shop.coupons.create':
            case 'backend.shop.coupons.edit':

                // Lending
            case 'backend.shop.landings.index':
            case 'backend.shop.landings.create':
            case 'backend.shop.landings.edit':
                $section = 'shop';
                break;

            // Другое
            case 'backend.media.images.index':
            case 'backend.media.images.create':
            case 'backend.media.images.edit':
            case 'backend.media.videos.index':
            case 'backend.media.videos.create':
            case 'backend.media.videos.edit':
                $section = 'media';
                break;
        }

        $this->data = [
            'path' => $path,
            'route' => $route,
            'section' => $section,
        ];

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('requestInfo', $this->getData());
    }

}