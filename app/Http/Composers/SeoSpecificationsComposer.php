<?php

namespace App\Http\Composers;

use Api\App\Contracts\Entities\SeoSpecificationRepository as SeoSpecificationInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use Api\App\Storage\Condition;
use Illuminate\View\View;

/**
 * Seo Specifications Composer
 * @package App\Http\Composers
 */
class SeoSpecificationsComposer
{
    /**
     * @var TranslateInterface
     */
    private $translateHelper;

    /**
     * @var null
     */
    private $data = null;

    /**
     * @var SeoSpecificationInterface
     */
    protected $seoRepository;

    /**
     * @param TranslateInterface $translateHelper
     * @param SeoSpecificationInterface $seoRepository
     */
    public function __construct(
        TranslateInterface $translateHelper,
        SeoSpecificationInterface $seoRepository
    )
    {
        $this->translateHelper = $translateHelper;
        $this->seoRepository = $seoRepository;
    }

    /**
     * Информация о сео спецификациях
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $url = \Request::path();
        $url = (mb_substr($url, 0, 1) !== '/') ? '/' . $url : $url;

        $condition = new Condition();
        $condition->addCondition('url', 'url', $url);
        $seo = $this->seoRepository->findOne($condition);

        $seoData = [];

        if ($seo) {
            $seoData = [
                'title' => $this->translateHelper->get($seo->translations, 'title'),
                'meta_title' => $this->translateHelper->get($seo->translations, 'meta_title'),
                'meta_description' => $this->translateHelper->get($seo->translations, 'meta_description'),
                'meta_keywords' => $this->translateHelper->get($seo->translations, 'meta_keywords'),
            ];
        }

        $this->data = $seoData;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('seoSpecifications', $this->getData());
    }
}

