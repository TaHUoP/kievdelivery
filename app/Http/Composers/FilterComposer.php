<?php

namespace App\Http\Composers;

use App\Contracts\Helpers\ShopHelper as ShopHelperInterface;
use Illuminate\Contracts\View\View;
use Cookie;
use Route;

/**
 * Filter Composer
 * @package App\Http\Composers
 */
class FilterComposer
{
    /**
     * @param ShopHelperInterface $shopHelper
     */
    public function __construct(ShopHelperInterface $shopHelper)
    {
        $this->shopHelper = $shopHelper;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $currentFilter = [];

        if (Route::current()) {
            $currentFilter = Route::input('filter', []);
        }

        $filter = $this->shopHelper->decodeFilter($currentFilter);

        // Проверяем, сохранен ли фильтр отображения кол-ва товаров на странице
        $limit = Cookie::get('products_count');
        if (!isset($filter['count'][0]) && !empty($limit)) {
            $filter['count'] = [];
            $filter['count'][] = $limit;
        }

        $view->with('filter', $filter);
    }

}
