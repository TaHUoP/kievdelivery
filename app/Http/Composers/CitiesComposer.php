<?php

namespace App\Http\Composers;

use Api\App\Contracts\Entities\CityRepository as CityInterface;
use App\Models\City;
use Illuminate\Contracts\View\View;
use Api\App\Storage\Condition;
use Config;
use Translate;

/**
 * Countries Composer
 *
 * @package App\Http\Composers
 */
class CitiesComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * @var CityInterface
     */
    protected $cityRepository;

    /**
     * CountriesComposer constructor.
     * @param CityInterface $cityRepository
     */
    public function __construct(
        CityInterface $cityRepository
    )
    {
        $this->cityRepository = $cityRepository;
    }

    /**
     *
     * @return array
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $cities = City::with('translations', 'region.translations', 'price_type')->orderBy('alias')->get();
        foreach ($cities as $city) {
            $city->with_region_and_fee = Translate::get($city->translations, 'name');
            if (!$city->is_region_center && $city->region) {
                $city->with_region_and_fee .= ', ' . Translate::get($city->region->translations, 'name');
            }
            if ($city->price_type && (int) $city->price_type->fee > 0) {
                $city->with_region_and_fee .= ' + $' . $city->price_type->fee . ' ' . trans('cities.delivery fee');
            }
        }


        $this->data = $cities;

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('cities', $this->getData());
    }

}

