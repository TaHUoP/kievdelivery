<?php

namespace App\Http\Controllers;

use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use Api\App\Storage\Condition;

/**
 * News Controller
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * @var ContentInterface
     */
    protected $contentRepository;

    /**
     * @var TranslateInterface
     */
    protected $translationsHelper;

    /**
     * @param ContentInterface $contentRepository
     * @param TranslateInterface $translationsHelper
     */
    public function __construct(
        ContentInterface $contentRepository,
        TranslateInterface $translationsHelper
    )
    {
        $this->contentRepository = $contentRepository;
        $this->translationsHelper = $translationsHelper;
    }

    /**
     * GET /news
     * Display all news
     *
     * @return mixed
     */
    public function index()
    {
        $condition = new Condition();
        $condition
            ->addCondition('type', 'type', $this->contentRepository->getTypeNews())
            ->limit(25)
            ->pagination();

        $contents = $this->contentRepository->findAll($condition);

        return view('news.index', ['news' => $contents]);
    }

    /**
     * GET /news/{slug}
     * Display one news
     *
     * @param $url
     * @return mixed
     */
    public function show($url)
    {
        $condition = new Condition();
        $condition
            ->addCondition('url', 'url', $url)
            ->addCondition('type', 'type', $this->contentRepository->getTypeNews());

        if (!$content = $this->contentRepository->findOne($condition)) {
            abort(404);
        }

        return view('news.show', ['news' => $content]);
    }
}
