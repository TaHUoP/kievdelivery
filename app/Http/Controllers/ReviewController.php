<?php

namespace App\Http\Controllers;

use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use App\Http\Requests\Reviews\ReviewsMainRequest;
use App\Http\Requests\Reviews\ReviewsRequest;
use App\Exceptions\AlertException;
use App\Services\ReviewsService;
use Api\App\Storage\Condition;
use App\Models\Review;
use Response;
use Request;
use Session;

/**
 * Review Controller
 * @package App\Http\Controllers
 */
class ReviewController extends Controller
{
    /**
     * @var ReviewInterface
     */
    protected $reviewRepository;

    /**
     * @var ReviewInterface
     */
    protected $productRepository;

    /**
     * @var ReviewsService
     */
    protected $reviewsService;

    /**
     * @param ReviewInterface $reviewRepository
     * @param ReviewsService $reviewsService
     * @param ProductInterface $productRepository
     */
    public function __construct(
        ProductInterface $productRepository,
        ReviewInterface $reviewRepository,
        ReviewsService $reviewsService
    )
    {
        $this->productRepository = $productRepository;
        $this->reviewRepository = $reviewRepository;
        $this->reviewsService = $reviewsService;

    }

    /**
     * GET /reviews
     * Display all reviews
     *
     * @return mixed
     */
    public function index()
    {
        $condition = new Condition();
        $condition
            ->scopes(['confirmed' => 1])
            ->addCondition('type', 'type', $this->reviewRepository->getTypeMain())
            ->limit(25);

        $reviews = $this->reviewRepository->findLastAll($condition);

        if (Request::ajax()) {
            return view('review._index', ['reviews' => $reviews]);
        }

        return view('review.index', ['reviews' => $reviews]);
    }

    /**
     * @param ReviewsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ReviewsRequest $request)
    {
        if (!$product = $this->productRepository->findById($request->product_id)){
            abort(404);
        }

        try {
            $result = $this->reviewsService->save($request->all());

            if ($result) {
                Session::flash('success', trans('app.create_success'));
                return redirect(route('shop.product', ['alias' => $product->alias]));
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }

        } catch (AlertException $e) {
            return Response::json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Сохранение отзыва для страницы отзывов
     *
     * @param ReviewsMainRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeMain(ReviewsMainRequest $request)
    {
        try {
            $result = $this->reviewsService->saveMain($request->all());

            if ($result) {
                Session::flash('success', trans('app.create_success'));
                return redirect(route('review.index'));
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }

        } catch (AlertException $e) {
            return Response::json(['message' => $e->getMessage()], 500);
        }
    }
}
