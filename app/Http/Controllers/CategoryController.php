<?php

namespace App\Http\Controllers;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Contracts\Helpers\ShopHelper as ShopInterface;
use App\Facades\Translate;
use App\Http\Composers\UserCityComposer;
use App\Modules\CatalogFilterModule\Facade as FacadeCatalogFilter;
use App\Services\Shop\ProductService;
use Api\App\Storage\Condition;
use Request;
use View;

/**
 * Category Controller
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @var CategoryInterface
     */
    protected $categoryRepository;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var ShopInterface
     */
    protected $shopHelper;

    /**
     * @param PropertyInterface $propertyRepository
     * @param CategoryInterface $categoryRepository
     * @param ProductInterface $productRepository
     * @param ShopInterface $shopHelper
     * @param ProductService $productService
     */
    public function __construct(
        PropertyInterface $propertyRepository,
        CategoryInterface $categoryRepository,
        ProductInterface $productRepository,
        ShopInterface $shopHelper,
        ProductService $productService)
    {
        $this->propertyRepository = $propertyRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->productService = $productService;
        $this->shopHelper = $shopHelper;
    }

    /**
     * GET /category/{alias}/{filter?}
     * Display category page
     *
     * @param $alias
     * @param null $filter
     * @return \Illuminate\View\View
     */
    public function show($alias, $filter = null)
    {
        if($alias != strtolower($alias)) {
            $route = '/category/' . strtolower($alias);
            $route = is_null($filter) ? $route : $route . '/' . $filter;
            return redirect($route);
        }

        $category = $this->categoryRepository->findByNested($alias);

        if (empty($category['root']) ) {
            abort(404);
        }

        $take = 4;

        // Если есть дочерние категории
        if ($category['children']->count()) {

            if (!empty($filter)) {
                abort(404);
            }

            // Запрашиваем списки товаров исходя из дочерних категорий
            // чтобы сформировать страницу выборка подкатегорий
            foreach ($category['children'] as $key => $cat) {
                $condition = new Condition();
                $condition->scopes(['visible' => 1])
                    ->addCondition('category_id', 'id', $cat->id)
                    ->orderBy(['price' => SORT_ASC])
                    ->limit($take);

                $category['children'][$key]['products'] = $this->productRepository->findAll($condition);
                unset($condition);
            }

        } else {

            $productsIds = [];
            FacadeCatalogFilter::init($category['root']->id, $filter ?? "");
            FacadeCatalogFilter::setCategoryTranslate(Translate::get($category['root']->translations));
            if (FacadeCatalogFilter::isFilter()) {
                $collectionProductsIds = FacadeCatalogFilter::getProductsIds();
                if ($collectionProductsIds->count() > 0) {
                    $productsIds = $collectionProductsIds->toArray();
                }
                $filter = FacadeCatalogFilter::cropParamsFromFilter();
                if (FacadeCatalogFilter::isCity()) {
                    /** @var UserCityComposer $city */
                    $city = app(UserCityComposer::class);
                    $city->setCookieCityByUrl(FacadeCatalogFilter::getCityByUrl()->getAlias());
                }
            }

            if(!$this->shopHelper->validateRawFilter($filter)) {
                abort(404);
            }

            // Декодируем фильтр
            $filter = $this->shopHelper->decodeFilter($filter);

            if (!isset($filter['sort'])) {
                $filter['sort'][] = 'price_asc';
            }

            // Если в категории нет подкатегорий, запрашиваем список товаров
            // чтобы сформировать страницу категории с товарами
            $condition = new Condition();
            $condition->addCondition('category_id', 'id', $category['root']->id)
                ->scopes(['visible' => 1])
                ->pagination();
            if ($productsIds) {
                $condition->addCondition('products_ids', 'products_ids', $productsIds);
            }

            // Дополнительные условия для фильтра
            $this->productService->extraFilterForCondition($filter, $condition);

            // Получаем список свойств для поиска
            $properties = $this->propertyRepository->findAllByFilter(array_keys($filter));

            if($properties){
                $category['root']['products'] = $this->productRepository->findByPropertiesFilter($properties ,$filter, $condition);
            }else{
                $category['root']['products'] = $this->productRepository->findAll($condition);
            }
            unset($condition);
        }

        $category['parents']->shift();

        $canonical = "category/".$alias;

        if(Request::ajax()){
            $page = View::make('shop.product._page', ['products' => $category['root']['products']])->render();
            $pagination = View::make('_pagination', ['paginator' => $category['root']['products']])->render();

            return json_encode(['page' => $page, 'pagination' => $pagination, 'last_page' => $category['root']['products']->lastPage()]);
        } else {
            return view('shop.category', ['category' => $category, 'canonical' => $canonical, 'filter' => $filter, 'alias' => $alias]);
        }
    }
}
