<?php

namespace App\Http\Controllers;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Helpers\ShopHelper as ShopHelperInterface;
use App\Http\Requests\Shop\CheckoutRequest;
use App\Http\Requests\Shop\CouponRequest;
use App\Models\City;
use App\Services\Shop\CouponService;
use App\Services\Shop\CartService;
use Api\App\Storage\Condition;
use App\Events\CheckoutPost;
use Illuminate\Support\Facades\Config;
use Response;
use Request;
use Session;
use Event;
use Auth;
use DB;
use Cookie;
use Translate;

/**
 * Checkout Controller
 * @package App\Http\Controllers
 */
class CheckoutController extends Controller
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var CartService
     */
    protected $couponRepository;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * @var CouponService
     */
    protected $couponService;

    /**
     * @var ShopHelperInterface
     */
    protected $shopHelper;

    /**
     * @var ShopHelperInterface
     */
    protected $contentRepository;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param CartInterface $cartRepository
     * @param ProductInterface $productRepository
     * @param CouponInterface $couponRepository
     * @param ShopHelperInterface $shopHelper
     * @param CartService $cartService
     * @param CouponService $couponService
     * @param ContentInterface $contentRepository
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        CartInterface $cartRepository,
        ProductInterface $productRepository,
        CouponInterface $couponRepository,
        ShopHelperInterface $shopHelper,
        CartService $cartService,
        CouponService $couponService,
        ContentInterface $contentRepository
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->couponRepository = $couponRepository;
        $this->shopHelper = $shopHelper;
        $this->cartService = $cartService;
        $this->couponService = $couponService;
        $this->contentRepository = $contentRepository;
    }

    /**
     * GET /checkout
     * Display checkout page
     *
     * @return mixed
     */
    public function index()
    {
        $user = Auth::user();
        $cart = $this->cartService->getActualInfo();

        if (!Session::has('success') && (!$cart || !$cart->count())) {
            return redirect(route('site.index'));
        }
        return view('shop.checkout', [
            'user' => $user,
            'cart' => $cart,
        ]);
    }

    /**
     * POST /checkout
     * Save checkout info
     *
     * @todo Рефакторинг
     *
     * @param CheckoutRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCheckout(CheckoutRequest $request)
    {
        $data = $request->input();
        $city = City::find($data['delivery']['city']);

        // TODO: перенести в сервис
        $dateTime = new \DateTime($data['delivery']['date']);
        $data['delivery']['date'] = $dateTime->format('Y-m-d');
        $data['delivery']['city'] = Translate::get($city->translations, 'name');
        $data['checkout']['phone'] = preg_replace('/\D/', '', $data['checkout']['phone']);

        if (!$cart = $this->cartService->getActualInfo()) {
            abort(404);
        }

        DB::beginTransaction();

        try {
            $data['cart'] = $cart;

            $checkout = $cart->checkout;
            if (!$checkout) {
                $checkoutId = $this->checkoutRepository->save(array_merge([
                    'cart_id' => $cart->id
                ], $data));


                if ($checkout = $this->checkoutRepository->findById($checkoutId)) {
                    Event::fire(new CheckoutPost($checkout));
                }
            }
            $cart = $cart->load('checkout');


            DB::commit();


            if (isset($cart['checkout']['payment_system']) && $cart['checkout']['payment_system'] !== 'cash') {
                // PayPal
                if ($cart['checkout']['payment_system'] === 'paypal') {
                    return redirect(route('paypal.index', $checkout->id));
                } elseif ($cart['checkout']['payment_system'] === 'platon'){
                    // Platon
                    return redirect(route('platon.process', $checkout->id));
                } else {
                    throw new \Exception();
                }
            } else {
                Session::flash('success', trans('app.create_success'));
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('danger', trans('app.critical_error'));
            return redirect(route('shop.checkout'))->withErrors($request);
        }

        return redirect(route('shop.checkout'))->withCookie('city_alias', $city->alias, 43200);
    }

    /**
     * GET /orders
     * Display all orders page
     *
     * @return mixed
     */
    public function orders()
    {
        $condition = new Condition();
        $condition->addCondition('user_id', 'user_id', Auth::user()->id)
            ->limit(25)
            ->pagination();

        $checkouts = $this->checkoutRepository->findAll($condition);

        return view('shop.orders', ['checkouts' => $checkouts]);
    }

    /**
     * Получает номер купона и устанавливает скидку или возвращает ошибку
     * @param CouponRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkCoupon(CouponRequest $request)
    {
        $cart = $this->cartService->getActualInfo();

        if (!Request::ajax() || !$cart) {
            abort(404);
        }

        DB::beginTransaction();

        try {

            $couponCondition = new Condition();
            $couponCondition->addCondition('code', 'code', $request->get('coupon'));

            $coupon = $this->couponRepository->findOne($couponCondition);

            $this->cartService->useCoupon($cart, $coupon);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json(['message' => $e->getMessage()], 500);
        }

        return view('shop._finance-part', [
            'cart' => $cart, 'currentLocale' => $request['lang']
        ]);
    }

    /**
     * DELETE /delete-coupon
     * Delete coupon
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCoupon()
    {
        $cart = $this->cartService->getActualInfo();

        if (!Request::ajax() || !$cart) {
            abort(404);
        }

        DB::beginTransaction();

        try {

            $this->cartService->unuseCoupon($cart);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json(['message' => $e->getMessage()], 500);
        }

        return view('shop._finance-part', [
            'cart' => $cart, 'currentLocale' => request('lang')
        ]);
    }

    /**
     * GET /order
     * Display order page
     *
     * @param $id
     * @return mixed
     */
    public function order($id)
    {
        $condition = new Condition();
        $condition->addCondition('id', 'id', $id);
        $condition->addCondition('user_id', 'user_id', Auth::user()->id);

        if (!$checkout = $this->checkoutRepository->findOne($condition)) {
            abort(404);
        }

        return view('shop.order', ['checkout' => $checkout]);
    }

    /**
     * GET /checkout/success/{id}
     * Display info about paid checkout
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function success($id)
    {
        if (!$checkout = $this->checkoutRepository->findById($id)) {
            abort(404);
        }

        $condition = new Condition();
        $condition->addCondition('url', 'url', 'success')
            ->addCondition('type', 'type', $this->contentRepository->getTypePage())
            ->scopes(['visible' => 0]);

        $content = $this->contentRepository->findOne($condition);

        return view('payment.success', [
            'checkout' => $checkout,
            'page' => $content,
        ]);
    }
}
