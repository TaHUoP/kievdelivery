<?php

namespace App\Http\Controllers;

use Api\User\Contracts\Entities\UserRepository as UserRepositoryInterface;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Services\User\RegistrationService;
use App\Http\Requests\User\SignupRequest;
use App\Http\Requests\User\SigninRequest;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use App\Events\UserSignup;
use App\Models\User\User;
use App\Events\UserLogin;
use ErrorException;
use Exception;
use Redirect;
use Session;
use Config;
use Event;
use Auth;
use DB;

/**
 * Guest Controller
 * @package App\Http\Controllers
 */
class GuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use ThrottlesLogins, ResetsPasswords;

    protected $guestMiddleware = 'guest';

    public $maxLoginAttempts = 7;

    public $lockoutTime = 60;

    protected $redirectTo = '/';

    protected $guard = 'web';

    protected $throttles = true;

    protected $redirectAfterLogout = '/login';

    /**
     * @var RegistrationService
     */
    protected $registrationService;

    /**
     * @param RegistrationService $registrationService
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(RegistrationService $registrationService, UserRepositoryInterface $userRepository)
    {
        $this->registrationService = $registrationService;
        $this->userRepository = $userRepository;
    }

    /**
     * @return string
     */
    public function loginUsername()
    {
        return 'email';
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSignup()
    {
        return view('auth.signup');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param SignupRequest $request
     * @return $this|Redirect
     */
    public function postSignup(SignupRequest $request)
    {
        DB::beginTransaction();

        try {
            $this->registrationService->register($request);
            DB::commit();
            Session::flash('success', trans('auth.registration_success'));
        } catch (ErrorException $e) {
            DB::rollBack();
            Session::flash('danger', trans('app.critical_error'));
            return redirect()->back()->withInput($request->all())->withErrors($e->getMessage());
        }

        return redirect(route('guest.signin'));
    }

    /**
     * Форма ввода регистрационного ключа для подтверждения email'а
     *
     * @return \Illuminate\Http\Response
     */
    public function getConfirmForm()
    {
        return view('auth.confirm_form');
    }

    /**
     * Confirm user's email
     *
     * @param Request $request
     * @return Redirect
     */
    public function getConfirm(Request $request)
    {
        if (!$request->get('code')) {
            return redirect(route('email_confirmation_form'));
        }

        try {
            if ($this->registrationService->confirmEmail($request->get('code'))) {
                Session::flash('success', trans('auth.confirmation_success'));
            }
        } catch (\Exception $e) {
            return redirect(route('email_confirmation_form'));
        }

        return redirect()->intended('/');
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSignin()
    {
        return view('auth.signin');
    }

    /**
     * Handle a login request to the application.
     *
     * @param SigninRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function postSignin(SigninRequest $request)
    {
        if ($this->throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard(Config::get('auth.frontendGuard'))->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $this->throttles, Auth::user());
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($this->throttles && !$lockedOut) {

            $times = $this->maxLoginAttempts - app(RateLimiter::class)->attempts(
                    $this->getThrottleKey($request)
                );

            $errors = ['email' => trans('auth.failed')];
            if ($times <= 5) {
                $errors = ['email' => trans('auth.login-attempts-amount', ['times' => $times])];
            }

            $this->incrementLoginAttempts($request);
        }

        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors($errors);
    }

    /**
     * Send the response after the user was authenticated.
     * @param Request $request
     * @param $throttles
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles, User $user)
    {
        Event::fire(new UserLogin($user));

        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

//        return redirect()->intended($this->redirectTo);
        return redirect()->back();
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param Request $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }
}