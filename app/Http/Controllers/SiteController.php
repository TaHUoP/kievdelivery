<?php

namespace App\Http\Controllers;

use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use Api\Media\Contracts\Entities\MediaRepository as MediaInterface;
use Api\App\Contracts\Entities\CityRepository as CityInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use App\Helpers\CurrencyHelper as CurrencyInterface;
use App\Http\Requests\СustomOrderRequest;
use App\Http\Requests\ContactsRequest;
use App\Contracts\Helpers\ShopHelper;
use Api\App\Storage\Condition;
use App\Services\ReviewsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Response;
use Request;
use Session;
use Config;
use Mail;
use Auth;
use App;
use Illuminate\Support\Facades\Cookie;
use App\Models\City;
use Waavi\Translation\Models\Language;
use function GuzzleHttp\Psr7\str;

/**
 * Site Controller
 * @package App\Http\Controllers
 */
class SiteController extends Controller
{
    /**
     * @var CategoryInterface
     */
    protected $categoryRepository;

    /**
     * @var ShopHelper
     */
    protected $shopHelper;

    /**
     * @var CurrencyInterface
     */
    protected $currencyHelper;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var ContentInterface
     */
    protected $contentRepository;

    /**
     * @var MediaInterface
     */
    protected $mediaRepository;

    /**
     * @var TranslateInterface
     */
    protected $translateHelper;

    /**
     * @var MediaInterface
     */
    protected $cityRepository;

    /**
     * @var ReviewInterface
     */
    protected $reviewRepository;

    /**
     * @param CategoryInterface $categoryRepository
     * @param ProductInterface $productRepository
     * @param ContentInterface $contentRepository
     * @param TranslateInterface $translateHelper
     * @param MediaInterface $mediaRepository
     * @param ShopHelper $shopHelper
     * @param CurrencyInterface $currencyHelper
     * @param CityInterface $cityRepository
     * @param ReviewInterface $reviewRepository
     */
    public function __construct(
        CategoryInterface $categoryRepository,
        ProductInterface $productRepository,
        ContentInterface $contentRepository,
        TranslateInterface $translateHelper,
        MediaInterface $mediaRepository,
        ShopHelper $shopHelper,
        CurrencyInterface $currencyHelper,
        CityInterface $cityRepository,
        ReviewInterface $reviewRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->contentRepository = $contentRepository;
        $this->mediaRepository = $mediaRepository;
        $this->translateHelper = $translateHelper;
        $this->currencyHelper = $currencyHelper;
        $this->cityRepository = $cityRepository;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * GET /
     * Display index page of the site
     *
     * @param $city_alias
     * @return \Illuminate\View\View
     * @throws \ErrorException
     */
    public function index($city_alias = null)
    {
        if (!is_null($city_alias) && City::where('alias', $city_alias)->get()->isEmpty())
            return redirect()->route('error.404');

        $categoriesRecommendedCondition = new Condition();
        $categoriesRecommendedCondition
            ->addCondition('recommended', 'recommended', 1)
            ->scopes(['visible' => 1])
            ->limit(8);

        $categories = $this->categoryRepository->findRecommended($categoriesRecommendedCondition);

        $key = ($categories->count() > 0) ? true : false;


        //Выбор рекомендуемых
        $productsRecommendedCondition = new Condition();
        $productsRecommendedCondition
            ->scopes(['visible' => 1])
            ->limit(8);
        $productsRecommended = $this->productRepository->findRecommended($productsRecommendedCondition, $key);

        // Новые
        $productsNewCondition = new Condition();
        $productsNewCondition
            ->scopes(['visible' => 1])
            ->limit(4);
        $productsNew = $this->productRepository->findNew($productsNewCondition);

        $has_city = Cookie::has('city_alias');
        if (!$has_city)
            Cookie::queue('city_alias', City::DEFAULT_CITY, 43200);


        $condition = new Condition();
        $condition
            ->scopes(['confirmed' => 1])
            ->addCondition('type', 'type', $this->reviewRepository->getTypeMain())
            ->limit(25);

        $reviews = $this->reviewRepository->findLastAll($condition);

        return view('index', [
            'productsRecommended' => $productsRecommended,
            'productsNew' => $productsNew,
            'categories' => $categories,
            'has_city' => $has_city,
            'reviews' => $reviews
        ]);
    }

    /**
     * GET /page/{url}
     * Display content page
     *
     * @param $url
     * @return \Illuminate\View\View
     */
    public function page($url)
    {
        $condition = new Condition();
        $condition
            ->addCondition('url', 'url', $url)
            ->addCondition('type', 'type', $this->contentRepository->getTypePage());
        if (!$content = $this->contentRepository->findOne($condition)) {
            abort(404);
        }
        return view('page', ['page' => $content]);
    }

    /**
     * GET /page/{url}
     * Display content page
     *
     * @param $url
     * @return \Illuminate\View\View
     * @throws \ErrorException
     */
    public function pageUkraine($url)
    {
        $condition = new Condition();
        $condition
            ->addCondition('url', 'url', $url)
            ->addCondition('type', 'type', $this->contentRepository->getTypeUkraine());
        if (!$content = $this->contentRepository->findOne($condition)) {
            abort(404);
        }

        $categoriesRecommendedCondition = new Condition();
        $categoriesRecommendedCondition
            ->addCondition('recommended', 'recommended', 1)
            ->scopes(['visible' => 1])
            ->limit(8);

        $categories = $this->categoryRepository->findRecommended($categoriesRecommendedCondition);

        $key = ($categories->count() > 0) ? true : false;

        //Выбор рекомендуемых
        $productsRecommendedCondition = new Condition();
        $productsRecommendedCondition
            ->scopes(['visible' => 1])
            ->limit(8);
        $productsRecommended = $this->productRepository->findRecommended($productsRecommendedCondition, $key);

        return view('ukraine-page', [
            'page' => $content,
            'categories' => $categories,
            'productsRecommended' => $productsRecommended
        ]);
    }

    /**
     * GET /contacts
     * Форма "связатся с нами"
     *
     * @return \Illuminate\View\View
     */
    public function getContacts()
    {
        return view('pages.contacts');
    }

    /**
     * POST /contacts
     * Сохраняет форму "связатся с нами"
     *
     * @param ContactsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postContacts(ContactsRequest $request)
    {
        $data = [
            'request' => $request->input(),
            'user' => Auth::check() ? Auth::user() : null,
        ];

        try {

            Mail::queue('emails.contacts_to_admin', $data, function ($m) {
                $m->from(Config::get('app.email'), Config::get('app.name'));
                $m->to(explode(',', Config::get('app.support_email')))->subject('Запрос "Связаться с нами"');
            });

            Session::flash('success', trans('app.send_success'));

        } catch (\Exception $e) {

            Session::flash('danger', trans('app.critical_error'));

        }

        return redirect()->back();
    }

    /**
     * GET /customorder
     * Форма "Особый заказ"
     *
     * @return \Illuminate\View\View
     */
    public function getСustomorder()
    {
        return view('pages.customorder');
    }

    /**
     * POST /customorder
     * Обработать данные "Особый заказ"
     *
     * @param СustomOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postСustomorder(СustomOrderRequest $request)
    {
        $data = [
            'request' => $request->input(),
            'user' => Auth::check() ? Auth::user() : null,
        ];

        try {

            Mail::queue('emails.custom_order_to_admin', $data, function ($m) {
                $m->from(Config::get('app.email'), Config::get('app.name'));
                $m->to(explode(',', Config::get('app.support_email')))->subject('Запрос "Особый заказ"');
            });

            Session::flash('success', trans('app.send_success'));

        } catch (\Exception $e) {

            Session::flash('danger', trans('app.critical_error'));

        }

        return redirect(route('site.index'));
    }

    /**
     * GET /set-curr
     * Change currency
     *
     * @param $currency
     * @return \Illuminate\View\View
     */
    public function setCurrency($currency)
    {
        $back = Request::server('HTTP_REFERER');

        if($this->currencyHelper->isActive($currency)){
            Session::put(['currency' => $currency]);
        }

        return redirect($back ? $back : '/');
    }

    /**
     * GET /photos
     * Display photos page
     *
     * @return \Illuminate\View\View
     */
    public function photos()
    {
        $condition = new Condition();
        $condition->pagination()
            ->addCondition('visible', 'visible', 1)
            ->addCondition('type', 'type', $this->mediaRepository->getTypeImage());

        $photos = $this->mediaRepository->findAll($condition);

        return view('pages.photos', [
            'photos' => $photos
        ]);
    }

    /**
     * GET /videos
     * Display videos page
     *
     * @return \Illuminate\View\View
     */
    public function videos()
    {
        $condition = new Condition();
        $condition->pagination()
            ->addCondition('visible', 'visible', 1)
            ->addCondition('type', 'type', $this->mediaRepository->getTypeVideo());

        $videos = $this->mediaRepository->findAll($condition);

        return view('pages.videos', [
            'videos' => $videos
        ]);
    }

    /**
     * GET /badbrowser
     * Dadbrowser
     *
     * @return \Illuminate\View\View
     */
    public function badbrowser()
    {
        return view('badbrowser');
    }

    /**
     * GET /404.html
     * 404 page
     *
     * @return \Illuminate\View\View
     */
    public function notFound()
    {
        $contentRepository = App::make(ContentInterface::class);

        $condition = new Condition();
        $condition->addCondition('url', 'url', '404')
            ->addCondition('type', 'type', $contentRepository->getTypePage())
            ->scopes(['visible' => 0]);

        $content = $contentRepository->findOne($condition);

        return response()->view('errors.404', [
            'content' => $content,
        ], 404);
    }

    /**
     * GET /statics/js-data
     * Render data for js
     */
    public function jsData()
    {
        print('var jsData = {
    messages: {
        cart_add_success: \''. trans('shop.cart-add-success') .'\',
        coupon_no_discounts: \''. trans('shop.coupons.no_discounts') .'\',
        critical_error: \''. trans('app.critical_error') .'\',
    }
};');
        exit();
    }

    /**
     * GET /sitemap
     * Display sitemap page
     */
    public function sitemap()
    {
        ob_start();
        print('<?xml version="1.0" encoding="UTF-8"?>
        <urlset
            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                    http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">');

        $parse = function ($locale, $url) {
            $domain = route('site.index');
            $split = explode($domain, $url);

            $locale = $locale !== '' ? '/' . $locale : $locale;
            return $domain . $locale . $split[1];
        };

        foreach (['', 'uk', 'ru'] as $locale) {
            print('
            <url>
                <loc>' . $parse($locale, route('site.index')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>1.0</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('pages.contacts')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('pages.photos')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('pages.videos')) . '</loc>
                <priority>0.8</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('pages.customorder')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('review.index')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('news.index')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
            <url>
                <loc>' . $parse($locale, route('shop.products')) . '</loc>
                <lastmod>' . date(DATE_ATOM) . '</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
        ');

            //Категории
            $condition = new Condition();
//            $condition->scopes(['visible' => 1])
//                ->limit(null);
            $category = $this->categoryRepository->findAll($condition);

            foreach ($category as $cat) {
                if ($cat->id != 1) {
                    print('
                    <url>
                        <loc>' . $parse($locale, route('shop.category', ['alias' => $cat->alias])) . '</loc>
                        <lastmod>' . $cat->updated_at->format(DATE_ATOM) . '</lastmod>
                        <priority>0.6</priority>
                        <changefreq>daily</changefreq>
                    </url>
                ');
                }
            }

            // Товары
            $condition = new Condition();
            $condition->scopes(['visible' => 1])
                ->limit(null);
            $products = $this->productRepository->findAll($condition);

            foreach ($products as $product) {
                print('
                <url>
                    <loc>' . $parse($locale, route('shop.product', ['alias' => $product->alias])) . '</loc>
                    <lastmod>' . $product->updated_at->format(DATE_ATOM) . '</lastmod>
                    <priority>0.6</priority>
                    <changefreq>daily</changefreq>
                </url>
            ');
            }

            // Контент
            $condition = new Condition();
            $condition->scopes(['visible' => 1])
                ->limit(null);
            $contents = $this->contentRepository->findAll($condition);
            foreach ($contents as $content) {
                print('
                <url>
                    <loc>' . $parse($locale, $content->getUrl()) . '</loc>
                    <priority>0.6</priority>
                    <changefreq>daily</changefreq>
                    <lastmod>' . $content->updated_at->format(DATE_ATOM) . '</lastmod>
                </url>
            ');
            }
        }


        echo '</urlset>';

        $output = ob_get_contents();
        ob_end_clean();

        return Response::make($output, '200')->header('Content-Type', 'text/xml');
    }

    public function cityUpdate(\Illuminate\Http\Request $request)
    {
        $city = City::find($request->input('city_id'));

        if (app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName() == 'site.index') {
            return redirect()->route('site.index', ['city_alias' => $city->alias])->withCookie('city_alias', $city->alias, 43200);
        }

        $router = app('router')->getRoutes()->match(app('request')->create(URL::previous()));
        if ($router->getName() === "shop.category" && count($router->parameters()) > 1) {
            $params = $router->parameters();
            if (isset($params['filter'])) {
                $filterParams = explode("/", $params['filter']);
                if (count($filterParams) > 1) {
                    $cityAlias = array_pop($filterParams);
                    if (App\Modules\CatalogFilterModule\Facade::isCity($cityAlias)) {
                        return redirect()->to(str_replace("/".$params['filter'], '', URL::previous()))->withCookie('city_alias', $city->alias, 43200);
                    }
                }
            }
        }

        if (Request::ajax() && $city->price_type) {
            return response()->json(['cityFee' => $city->price_type->fee])
                ->withCookie('city_alias', $city->alias, 43200);
        }

        return back()->withCookie('city_alias', $city->alias, 43200);
    }
}