<?php

namespace App\Http\Controllers;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use App\Contracts\Helpers\ShopHelper as ShopInterface;
use App\Models\Shop\Category;
use App\Models\Shop\Product;
use App\Services\Shop\ProductService;
use App\Http\Requests\SearchRequest;
use Api\App\Storage\Condition;
use Illuminate\Http\Request;
use View;

/**
 * Product Controller
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var ProductInterface
     */
    protected $reviewRepository;

    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @var ShopInterface
     */
    protected $shopHelper;

    /**
     * @param ProductInterface $productRepository
     * @param PropertyInterface $propertyRepository
     * @param ShopInterface $shopHelper
     * @param ProductService $productService
     * @param ReviewInterface $reviewRepository
     */
    public function __construct(
        ProductInterface $productRepository,
        PropertyInterface $propertyRepository,
        ShopInterface $shopHelper,
        ProductService $productService,
        ReviewInterface $reviewRepository
    )
    {
        $this->shopHelper = $shopHelper;
        $this->productRepository = $productRepository;
        $this->propertyRepository = $propertyRepository;
        $this->productService = $productService;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * GET /products
     * Display all products
     *
     * @param null $filter
     * @param Request $request
     * @return \Illuminate\View\View
     * @throws \ErrorException
     */
    public function index(Request $request, $filter = null)
    {
        //Если мы пришли с главной, то нужно отфильтровать цену оставив дефолтный url
        if ($request->isMethod('post')) {
            $filter = $request->input('filter', '');
        }

        //Валидируем фильтр
        if(!$this->shopHelper->validateRawFilter($filter)) {
            abort(404);
        }

        // Декодируем фильтр
        $filter = $this->shopHelper->decodeFilter($filter);

        if (!isset($filter['sort'])) {
            $filter['sort'][] = 'price_asc';
        }

        // Получаем список свойств для поиска
        $properties = $this->propertyRepository->findAllByFilter(array_keys($filter));

        $products = collect();
        $search = trim($request->input('search')) ?: null;

        if ($properties) {
            // Поиск товаров по фильтру
            $condition = new Condition();
            $condition->scopes(['visible' => 1]);
            $condition->pagination();

            if (!empty($search)) {
                $condition->addCondition('category_name', 'name', $search);
                $condition->addCondition('product_name', 'name', $search);
                $condition->addCondition('vendor_code', 'vendor_code', $search);
                $condition->addCondition('description_short', 'desc', $search);
                $condition->addCondition('description_full', 'desc', $search);
            }

            // Дополнительные условия для фильтра
            $this->productService->extraFilterForCondition($filter, $condition);

            $condition->pagination();
            $products = $this->productRepository->findByPropertiesFilter($properties, $filter, $condition);
        }

        if($request->ajax()){
            $page = View::make('shop.product._page', ['products' => $products])->render();
            $pagination = View::make('_pagination', ['paginator' => $products])->render();

            return json_encode(['page' => $page, 'pagination' => $pagination, 'last_page' => $products->lastPage()]);
        } else {
            return view('shop.product.index', ['products' => $products, 'filter' => $filter]);
        }
    }

    /**
     * GET /product/{alias}
     * Display product
     *
     * @param $alias
     * @return \Illuminate\View\View
     */
    public function show(Request $request, $alias)
    {
        //get previous page category
        $referer = explode('/', $request->header('referer'));
        $category_alias = end($referer);
        $category = Category::where('alias', 'like', $category_alias)->first();

        if($alias != strtolower($alias)) {
            $route = '/product/' . strtolower($alias);
            return redirect($route);
        }

        $productsCondition = new Condition();
        $productsCondition
            ->scopes(['visible' => 1])
            ->addCondition('alias', 'alias', $alias)
            ->relations(['reviews', 'images']);

        $product = $this->productRepository->findOne($productsCondition);

        if (!$product || $product->visible == 0) {
            abort(404);
        }

        $recently_viewed = $request->session()->get('recently_viewed', []);

        $productsRecentlyViewed = Product::whereIn('id', array_slice($recently_viewed, -4))
            ->where('id', '!=', $product->id)->get();

        $recently_viewed = array_merge($recently_viewed, [$product->id]);

        $request->session()->put(['recently_viewed' => $recently_viewed]);

        $productsRecommendedCondition = new Condition();
        $productsRecommendedCondition
            ->scopes(['visible' => 1, 'main' => 1])
            ->addCondition('except', 'id !=', $product->id);

        $productsRecommended = $this->productRepository->getMainProducts($productsRecommendedCondition, $product->id);

        $reviewsCondition = new Condition();
        $reviewsCondition
//            ->pagination()
//            ->limit(5)
            ->scopes(['confirmed' => 1])
            ->addCondition('product_id', 'product_id', $product->id)
            ->addCondition('type', 'type', $this->reviewRepository->getTypeProduct());

        $reviews = $this->reviewRepository->findLastAll($reviewsCondition);

        return view('shop.product.new_show', [
            'product' => $product,
            'productsRecommended' => $productsRecommended,
            'reviews' => $reviews,
            'alias' => $alias,
            'category' => $category,
            'productsRecentlyViewed' => $productsRecentlyViewed
        ]);
    }

    /**
     * Products search
     *
     * @param SearchRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getSearch(SearchRequest $request)
    {
        $condition = new Condition();
        $condition
            ->scopes(['visible' => 1])
            ->addCondition('vendor_code', 'vendor_code', $request->get('s'));

        if ($product = $this->productRepository->findOne($condition)) {
            return redirect(route('shop.product', ['alias' => $product->alias]));
        }

        return redirect(route('shop.products') . '?search=' . $request->get('s'));
    }
}
