<?php

namespace App\Http\Controllers;

use Api\User\Contracts\Entities\ProfileRepository;
use App\Events\UserSocialSignup;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\OAuthSignupRequest;
use App\Models\User\Profile;
use App\Models\User\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Api\User\Contracts\Entities\UserRepository as UserInterface;
use Event;

class OAuthController extends Controller
{
    protected $userRepository;
    protected $profileRepository;

    public function __construct(UserInterface $userRepository, ProfileRepository $profileRepository)
    {
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * Redirect the user to the authentication page.
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Socialite.
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $socialite_user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect()->route('guest.signin');
        }

        // get user profile by socialite user id
        $user_profile = Profile::whereHas('user', function ($user_query) use ($socialite_user) {
            $user_query->where('email', $socialite_user->email);
        })->first();
        $user_profile = is_null($user_profile) ? Profile::where('profile_id', $socialite_user->id)->first() : $user_profile;

        $name_array = explode(' ', $socialite_user->name);
        $name = array_shift($name_array);
        $surname = implode(' ', $name_array);

        $data = [
            'profile_id' => $socialite_user->id,
            'token' => $socialite_user->token,
            'email' => $socialite_user->email,
            'provider' => $provider,
            'name' => $name,
            'surname' => $surname,
            'avatar_url' => $socialite_user->avatar,
        ];

        if ($provider === Profile::GOOGLE_PROVIDER) {
            $data['profile_url'] = $socialite_user->user['url'];
        } elseif ($provider === Profile::FACEBOOK_PROVIDER) {
            $data['profile_url'] = $socialite_user->profileUrl;
        }

        // if user profile doesn`t exist, create new
        if (!$user_profile) {
            $user_profile = Profile::create(array_filter($data));
        } else {
            $user_profile->update(array_filter($data));
        }

        // if we do not get email, return view for pre-registration that list to the method below
        if (is_null($user_profile->email) || is_null($user_profile->user) || is_null($user_profile->user->phone)) {
            return view('auth.oauth.signup', compact('user_profile', 'provider'));
        }

        // return authorize user
        return $this->auth($provider, new OAuthSignupRequest([
            'email' => $user_profile->email,
            'phone' => null,
            'user_profile_id' => $user_profile->id,
        ]));
    }

    public function auth($provider, OAuthSignupRequest $request)
    {
        // get user by email
        $user = $this->userRepository->findByEmail($request->input('email'));
        $user_profile = Profile::find($request->input('user_profile_id'));

        // if user doesn`t exist - create new
        if (!$user) {
            $user = User::create([
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'password' => null,
                    'access_cpanel' => false,
                    'status' => 1,
            ]);

            // add email and user_id to user profile
            $user_profile->update([
                'email' => $request->input('email'),
                'user_id' => $user->id
            ]);

            Event::fire(new UserSocialSignup($user));
        }

        Auth::login($user);

        return redirect('/');
    }
}
