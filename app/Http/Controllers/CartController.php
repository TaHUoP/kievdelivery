<?php

namespace App\Http\Controllers;

use Api\App\Storage\Condition;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Facades\Shop;
use App\Helpers\ShopHelper as ShopHelperInterface;
use App\Http\Requests\Shop\CartControlRequest;
use App\Http\Requests\Shop\BuyNowRequest;
use App\Models\Shop\Cart;
use App\Services\Shop\CartService;
use Exception;
use Illuminate\Support\Facades\Config;
use Response;
use Session;
use Request;
use Cookie;
use DB;

/**
 * Cart Controller
 * @package App\Http\Controllers
 */
class CartController extends \App\Http\Controllers\Controller
{
    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * @var ShopHelperInterface
     */
    protected $shopHelper;

    /**
     * @param CartInterface $cartRepository
     * @param ProductInterface $productRepository
     * @param ShopHelperInterface $shopHelper
     * @param CartService $cartService
     */
    public function __construct(
        CartInterface $cartRepository,
        ProductInterface $productRepository,
        ShopHelperInterface $shopHelper,
        CartService $cartService
    )
    {
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->shopHelper = $shopHelper;
        $this->cartService = $cartService;
    }

    /**
     * GET /cart
     * Display checkout page
     *
     * @return mixed
     */
    public function index()
    {
        $actualInfo = $this->cartService->getActualInfo();

        $products = $actualInfo->products->pluck('product_id');

        $productsRecommendedCondition = new Condition();
        $productsRecommendedCondition
            ->scopes(['visible' => 1, 'main' => 1])
            ->addCondition('except', 'id !=', $products->toArray());

        $productsRecommended = $this->productRepository
            ->getMainProducts($productsRecommendedCondition, $products->first());

        return view('shop.cart.cart', ['cart' => $actualInfo, 'productsRecommended' => $productsRecommended]);
    }

    /**
     * POST /cart/control
     * Ajax control cart
     *
     * @param CartControlRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function control(CartControlRequest $request)
    {
        $post = $request->all();
        if (!$product = $this->productRepository->findById($post['productId'])) {
            abort(404);
        }
        DB::beginTransaction();

        try {

            // Добавить в корзину
            if ($post['action'] === 'add') {
                $hash = $this->cartService->add($product, $post['amount']);
                Cookie::queue(Cookie::make('cart', $hash, Cart::COOKIE_MINUTES_EXPIRES));
                $request->session()->put(['cart' => $hash, 'action' => 'add']);
            } else {
                // Изменение кол-ва
                if ($post['action'] === 'change' && !is_null($post['amount'])) {
                    $this->cartService->change($product, $post['amount']);
                } // Удаление
                else if ($post['action'] === 'remove') {
                    $this->cartService->remove($product);
                }
                if ($post['action'] === 'remove' || $post['action'] === 'change') {
                    DB::commit();
                    return view('shop.cart._cart', [
                        'cart' => $this->cartService->getActualInfo(),
                        'currentLocale' => $post['lang']
                    ]);
                }
            }
            DB::commit();

            return view('shop.cart._cart-mini', [
                'currentLocale' => $post['lang'],
                'cartControl' => [
                    'countProducts' => $post['amount'],
                    'totalPriceString' => Shop::getSalePrice($product)
                ]
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json(['message' => trans('app.critical_error')], 500);
        }
    }

    /**
     * POST /buy-now
     * "Купить сейчас"
     *
     * @param BuyNowRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postBuyNow(BuyNowRequest $request)
    {
        if (!$product = $this->productRepository->findById($request->input('product_id'))) {
            abort(404);
        }

        if ($result = $this->cartService->add($product, 1)) {
            Cookie::queue(Cookie::make('cart', $result, Cart::COOKIE_MINUTES_EXPIRES));
            return Response::json(['message' => trans('shop.cart-add-success'), 'url' => route('shop.checkout')]);
        }

        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}