<?php

namespace App\Http\Controllers\Payment;

use Api\App\Storage\Condition;
use App\Events\CheckoutPayment;
use App\Models\Shop\Checkout;
use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Services\Payments\PlatonService;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class PlatonController extends Controller
{
    private $platonService;
    private $checkoutRepository;
    private $cartRepository;

    public function __construct(PlatonService $platonService,
                                CheckoutInterface $checkoutRepository,
                                CartInterface $cartRepository)
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->platonService = $platonService;
        $this->cartRepository = $cartRepository;
    }

    public function process(Request $request, $checkout_id)
    {
        $condition = new Condition();

        $condition
            ->addCondition('id', 'id', $checkout_id)
            ->scopes(['paid' => 0, 'status' => $this->checkoutRepository->getStatusPending()]);

        if (!$checkout = $this->checkoutRepository->findOne($condition)) {
            abort(404);
        }

        $form_html = $this->platonService->getFormHTML($checkout, App::getLocale());

        return view('payment.platon', compact('form_html'));
    }

    /**
     * Handles response from platon payment system
     *
     * @param Request $request
     * @return mixed
     */
    public function callback(Request $request)
    {
        $checkout = Checkout::findOrFail($request->input('order'));

        if ($request->isMethod('post')) {
            $this->callbackPost($request, $checkout);
        } elseif ($request->isMethod('get')) {
            return $this->callbackGet($checkout);
        }
    }

    /**
     * @param Checkout $checkout
     * @return \Illuminate\Http\RedirectResponse
     */
    private function callbackGet(Checkout $checkout)
    {
        return $this->success($checkout);
    }

    /**
     * @param Request $request
     * @param Checkout $checkout
     * @return \Illuminate\Http\RedirectResponse
     */
    private function callbackPost(Request $request, Checkout $checkout)
    {
        $this->saveTransactionDetails($checkout, $request->input('id'), $request->input('card'));

        return $this->success($checkout);
    }

    /**
     * @param Checkout $checkout
     * @return \Illuminate\Http\RedirectResponse
     */
    private function success(Checkout $checkout)
    {
        $this->checkoutRepository->save([
            'checkout' => [
                'paid' => 1
            ],
        ], $checkout);
        $this->cartRepository->save(['cart' => ['processed' => 1]], $checkout->cart);

        event(new CheckoutPayment($checkout));

        return redirect(route('shop.checkout.success', $checkout));
    }

    private function saveTransactionDetails(Checkout $checkout, string $trans_id, string $card)
    {
        $response = $this->platonService->getTransactionDetails($trans_id, $card);

        if ($response['result'] === PlatonService::SUCCESS_RESULT) {
            $this->checkoutRepository->save([
                'checkout' => [
                    'payment_data' => json_encode($response)
                ],
            ], $checkout);
        }
    }
}
