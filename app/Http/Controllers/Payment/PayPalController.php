<?php

namespace App\Http\Controllers\Payment;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Services\Shop\CartService;
use App\Services\Payments\PayPalService;
use App\Http\Controllers\Controller;
use App\Events\CheckoutPayment;
use Api\App\Storage\Condition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Session;
use DB;

/**
 * PayPal Controller
 * @package App\Http\Controllers
 */
class PayPalController extends Controller
{
    /**
     * @var PayPalService
     */
    protected $payService;

    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param PayPalService $payService
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        PayPalService $payService,
        CartService $cartService,
        CartInterface $cartRepository
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->payService = $payService;
        $this->cartService = $cartService;
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function index($id)
    {
        $condition = new Condition();
        $condition
            ->addCondition('id', 'id', $id)
            ->scopes(['paid' => 0, 'status' => $this->checkoutRepository->getStatusPending()]);

        if (!$model = $this->checkoutRepository->findOne($condition)) {
            abort(404);
        }

        if ($redirect = $this->payService->redirect($model)) {
            return redirect($redirect);
        }

        Session::flash('danger', trans('app.critical_error'));
        return redirect(route('site.index'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function success(Request $request)
    {
        $checkoutId = $request->get('checkout_id');
        $paymentId = $request->get('paymentId');
        $payerID = $request->get('PayerID');
        $token = $request->get('token');

        if (!$model = $this->checkoutRepository->findById($checkoutId)) {
            abort(404);
        }
        $this->cartRepository->save(['cart' => ['processed' => 1]], $model->cart);

        if ($this->payService->completePayment($paymentId, $payerID)) {

            try {

                if (!$model->paid) {
                    $this->checkoutRepository->save([
                        'checkout' => [
                            'payment_data' => json_encode($this->payService->showPaymentDetails($paymentId, $payerID)),
                            'paid' => 1,
                        ],
                    ], $model);
                    event(new CheckoutPayment($model));
                }

                DB::commit();
                //Session::flash('success', trans('site.payment_data_received'));
                return redirect(route('shop.checkout.success', $model));

            } catch (\Exception $e) {
                DB::rollBack();
                Session::flash('danger', trans('site.payment_data_error'));
            }
        }

        return redirect(route('site.index'));
    }

    /**
     * @return mixed
     */
    public function cancel()
    {
        Session::flash('danger', trans('site.payment_data_error'));
        return redirect(route('site.index'));
    }

    /**
     * @return mixed
     */
    /*public function result()
    {
        $data = Request::input();

        $file = __DIR__ . '/../../../../_test/pay_pal.txt';

        $result = '';
        foreach($data as $key => $value ) {
            $result .= $key . ': - ' . $value . "\n";
        }

        if (empty($result)) {
            $result = 'Нет данных';
        }

        file_put_contents($file, $result);

    }*/
}
