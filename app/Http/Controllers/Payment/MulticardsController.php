<?php

namespace App\Http\Controllers\Payment;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Models\Shop\Cart;
use App\Services\Shop\CartService;
use App\Services\Payments\WayForPayService;
use App\Services\Payments\MulticardsService;
use App\Http\Controllers\Controller;
use App\Events\CheckoutPayment;
use Api\App\Storage\Condition;
use Illuminate\Http\Request;
use Session;
use DB;

/**
 * Way For Pay Controller
 * @package App\Http\Controllers\Payment
 */
class MulticardsController extends Controller{

    /**
     * @var WayForPayService
     */
    protected $wayForPayService;
    protected $multiCardsService;

    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;
    protected $productRepository;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param WayForPayService $wayForPayService
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        WayForPayService $wayForPayService,
        multiCardsService $multiCardsService,
        ProductInterface $productRepository,
        CartService $cartService,
        CartInterface $cartRepository
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->wayForPayService = $wayForPayService;
        $this->multiCardsService = $multiCardsService;
        $this->productRepository = $productRepository;
        $this->cartService = $cartService;
        $this->cartRepository = $cartRepository;
    }



    /**
     * @param $id
     * @return mixed
     */

    public function index($id){

        $condition = new Condition();
        //$condition_prod = new Condition();



        $condition
            ->addCondition('id', 'id', $id)
            ->scopes(['paid' => 0, 'status' => $this->checkoutRepository->getStatusPending()]);

        if (!$checkout = $this->checkoutRepository->findOne($condition)) {
            abort(404);
        }

        $products = $this->multiCardsService->getDataForFormProducts($checkout);
        $customer = $this->multiCardsService->getDataForFormCustomer($checkout);

        return view('payment.multicards', ['products' => $products, 'customer' => $customer, 'id' => $id]);
    }

    public function returnUrl(Request $request){

        if(isset($request->user1)){
            $order = $request->user1;
        }
        else{
            return view('payment.multicards_success');
        }

        $condition = new Condition();
        $condition
            ->addCondition('id', 'id', $order)
            ->scopes(['paid' => 0, 'status' => $this->checkoutRepository->getStatusPending()]);


        if (!$checkout = $this->checkoutRepository->findOne($condition)) {
            abort(404);
        }
        
        $this->cartRepository->save(['cart' => ['processed' => 1]], $checkout->cart);


        $this->multiCardsService->updateStatus($order, $checkout);


        return view('payment.multicards_success');
    }
}
