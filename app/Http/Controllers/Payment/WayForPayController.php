<?php

namespace App\Http\Controllers\Payment;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use App\Services\Payments\WayForPayService;
use App\Http\Controllers\Controller;
use App\Events\CheckoutPayment;
use Api\App\Storage\Condition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Session;
use DB;

/**
 * Way For Pay Controller
 * @package App\Http\Controllers\Payment
 */
class WayForPayController extends Controller
{
    /**
     * @var WayForPayService
     */
    protected $wayForPayService;

    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param WayForPayService $wayForPayService
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        WayForPayService $wayForPayService
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->wayForPayService = $wayForPayService;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function index($id)
    {
        $condition = new Condition();
        $condition
            ->addCondition('id', 'id', $id)
            ->scopes(['paid' => 0, 'status' => $this->checkoutRepository->getStatusPending()]);

        if (!$checkout = $this->checkoutRepository->findOne($condition)) {
            abort(404);
        }

        $form = $this->wayForPayService->getDataForPurchase($checkout);

        return view('payment.wayforpay', [
            'form' => $form,
        ]);
    }

    /**
     * Url адрес возврата
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function returnUrl(Request $request)
    {
        try {

            $result = $this->wayForPayService->checkStatus($request->get('orderReference'));
            if (!$model = $this->wayForPayService->findModelByApprovedStatus($result)) {
                throw new \Exception('Model "Checkout" not found.');
            }

            // Session::flash('success', trans('site.payment_data_received'));
            return redirect(route('shop.checkout.success', $model));

        } catch (\Exception $e) {
            Session::flash('danger', trans('site.payment_data_error'));
        }

        return redirect(route('site.index'));
    }

    /**
     * Url адрес обработки заказа
     */
    public function serviceUrl()
    {
        try {
            $requestData = $this->getRequestDataByWayForPay();
            if ($model = $this->wayForPayService->findModelByApprovedStatus($requestData)) {
                if (!$model->paid) {
                    $this->checkoutRepository->save([
                        'checkout' => [
                            'payment_data' => json_encode($requestData),
                            'paid' => 1,
                        ],
                    ], $model);
                    event(new CheckoutPayment($model));
                }
            }
            DB::commit();
            echo $this->wayForPayService->getResponceHash($requestData);
        } catch (\Exception $e) {
            DB::rollBack();
        }

        // $fileR = __DIR__ . '/../../../../_test/way_for_pay_result.txt';
        // file_put_contents($fileR, $_r);

        exit();
    }

    /**
     * http://stackoverflow.com/questions/68651/get-php-to-stop-replacing-characters-in-get-or-post-arrays
     *
     * @return array
     */
    private function getRequestDataByWayForPay() {
        $source = file_get_contents('php://input');
        $source = preg_replace_callback(
            '/(^|(?<=&))[^=[&]+/',
            function($key) { return bin2hex(urldecode($key[0])); },
            $source
        );

        parse_str($source, $post);
        $input = array_combine(array_map('hex2bin', array_keys($post)), $post);

        $inputKey = array_keys($input);
        $data = isset($inputKey[0]) ? $inputKey[0] : null;

        return json_decode($data, true);
    }
}