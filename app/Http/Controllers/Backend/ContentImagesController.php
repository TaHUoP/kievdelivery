<?php

namespace App\Http\Controllers\Backend;

use Api\App\Contracts\Entities\ContentImageRepository as ContentImageInterface;
use App\Http\Requests\Backend\ContentImageRequest;
use App\Services\ContentImageService;
use App\Exceptions\AlertException;
use Response;
use DB;

/**
 * Content Images Controller
 * @package App\Http\Controllers\Backend
 */
class ContentImagesController extends \App\Http\Controllers\Controller
{
    /**
     * @var ContentImageInterface
     */
    protected $contentImagesRepository;

    /**
     * @var ContentImageService
     */
    protected $contentImageService;

    /**
     * @param ContentImageInterface $contentImagesRepository
     * @param ContentImageService $contentImageService
     */
    public function __construct(
        ContentImageInterface $contentImagesRepository,
        ContentImageService $contentImageService
    )
    {
        $this->contentImagesRepository = $contentImagesRepository;
        $this->contentImageService = $contentImageService;
    }

    /**
     * POST /backend/media/1/images/upload
     * Display the form for editing a image.
     *
     * @param ContentImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(ContentImageRequest $request)
    {
        $file = $request->file('uploadfile');
        if (!$contentId = (int)$request->route()->getParameter('content')) {
            abort(404);
        };
        try {
            $result = $this->contentImageService
                ->setContentId($contentId)
                ->upload($file);
            if ($result) {
                DB::commit();
                return Response::json(['message' => trans('app.update_success'), 'id' => $result]);
            } else {
                DB::rollBack();
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            DB::rollBack();
            return Response::json(['message' => trans('app.critical_error')], 500);
        }
    }

    /**
     * GET /backend/contentImages/1/edit
     * Display the form for editing a image.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$image = $this->contentImagesRepository->findById($id)) {
            abort(404);
        }

        return view('contents.modal._edit', [
            'image' => $image,
        ]);
    }

    /**
     * PUT /backend/contentImages/1
     * Update a given image in storage.
     *
     * @param $id
     * @param ContentImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, ContentImageRequest $request)
    {
        if (!$image = $this->contentImagesRepository->findById($id)) {
            abort(404);
        }
        try {
            $result = $this->contentImageService->save($request->all(), $image);
            if ($result) {
                DB::commit();
                return Response::json(['message' => trans('app.update_success')]);
            } else {
                DB::rollBack();
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            DB::rollBack();
            return Response::json(['message' => trans('app.critical_error')], 500);
        }
    }

    /**
     * DELETE /backend/contentImages/1
     * Remove a image from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (!$image = $this->contentImagesRepository->findById($id)) {
            abort(404);
        }
        if ($this->contentImageService->delete($image)) {
            return Response::json(['message' => trans('app.delete_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}
