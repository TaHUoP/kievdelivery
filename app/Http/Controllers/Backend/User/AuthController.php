<?php

namespace App\Http\Controllers\Backend\User;

use Illuminate\Http\Request;
use App\Models\User\User;
use Validator;
use Redirect;
use Config;
use Auth;

/**
 * Auth Controller
 * @package App\Http\Controllers\Backend
 */
class AuthController extends \App\Http\Controllers\Controller
{
    /**
     * GET /backend/login
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function login(Request $request)
    {
        if (Auth::guard(Config::get('auth.backendGuard'))->check()) {
            return Redirect::route('backend.site.index');
        }

        if ($request->method() == 'POST') {

            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return Redirect::route('backend.auth.login')
                    ->withErrors($validator)->withInput();
            }

            $auth = Auth::guard(Config::get('auth.backendGuard'))->attempt([
                'email' => $request->get('username'),
                'password' => $request->get('password'),
                'access_cpanel' => User::AUTHORIZED_TO_CPANEL,
            ], false);

            if (!$auth) {
                return Redirect::route('backend.auth.login')->withErrors([
                    'username' => ' ',
                    'password' => trans('auth.failed'),
                ])->withInput();
            }

            return Redirect::route('backend.site.index');
        }

        return view('auth.login');
    }

    /**
     * GET /backend/logout
     *
     * @return \Illuminate\View\View
     */
    public function logout()
    {
        Auth::guard(Config::get('auth.frontendGuard'))->logout();
        Auth::guard(Config::get('auth.backendGuard'))->logout();
        return Redirect::route('backend.auth.login');
    }
}