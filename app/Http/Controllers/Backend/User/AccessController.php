<?php

namespace App\Http\Controllers\Backend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

/**
 * Access Controller
 * @package App\Http\Controllers\Backend
 */
class AccessController extends Controller
{

    public function __construct()
    {

    }

    /**
     * GET /backend/access
     * Display all of the access.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('access.index', []);
    }

    /**
     * GET /backend/access/create
     * Display the form for creating a new access record.
     *
     * @return \Illuminate\View\View
     */
    /*public function create()
    {
        return view('access.create', $data);
    }*/

    /**
     * POST /backend/access
     * Store a new access in storage.
     *
     * @param AccessRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function store(AccessRequest $request)
    {
        echo '<pre>';
        print_r($request->all());
    }*/

    /**
     * GET /backend/access/1
     * Show a access in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function show($id)
    {
        return redirect(route('access.edit', ['id' => $id]));
    }*/

    /**
     * GET /backend/access/1
     * Display the form for editing a access.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    /*public function edit($id)
    {
        return view('access.edit', $data);
    }*/

    /**
     * PUT /backend/access/1
     * Update a given access in storage.
     *
     * @param AccessRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function update(AccessRequest $request, $id)
    {
        echo '<pre>';
        print_r($request->all());
    }*/

    /**
     * DELETE /backend/access/1
     * Remove a access from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function destroy($id)
    {

    }*/
}
