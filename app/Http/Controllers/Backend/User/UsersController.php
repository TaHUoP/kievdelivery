<?php

namespace App\Http\Controllers\Backend\User;

use Api\User\Contracts\Entities\ProfileRepository as ProfileInterface;
use Api\User\Contracts\Entities\ActionRepository as ActionInterface;
use Api\User\Contracts\Entities\UserRepository as UserInterface;
use App\Http\Requests\User\Backend\UserRequest;
use App\Http\Controllers\Controller;
use App\Services\User\UserService;
use Api\App\Storage\Condition;
use App\Models\User\User;
use App\Http\Requests;
use Request;
use Session;

/**
 * Users Controller
 * @package App\Http\Controllers\Backend
 */
class UsersController extends Controller
{
    /**
     * @var UserInterface
     */
    protected $userRepository;

    /**
     * @var ProfileInterface
     */
    protected $profileRepository;

    /**
     * @var ActionInterface
     */
    protected $actionRepository;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserInterface $userRepository
     * @param ProfileInterface $profileRepository
     * @param ActionInterface $actionRepository
     * @param UserService $userService
     */
    public function __construct(
        UserInterface $userRepository,
        ProfileInterface $profileRepository,
        ActionInterface $actionRepository,
        UserService $userService
    )
    {
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
        $this->actionRepository = $actionRepository;
        $this->userService = $userService;
    }

    /**
     * GET /backend/users
     * Display all of the users.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->withTrashed();
        $condition->pagination()
            ->limit(100);

        $users = $this->userRepository->findAll($condition);

        if (Request::ajax()) {
            return view('users._ajax_index', ['users' => $users]);
        }

        return view('users.index', ['users' => $users]);
    }

    /**
     * GET /backend/users/create
     * Display the form for creating a new user.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('users.create', []);
    }

    /**
     * POST /backend/users
     * Store a new user in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        if ($this->userService->create($request)) {
            Session::flash('success', trans('app.update_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.user.users.index'));
    }

    /**
     * GET /backend/users/1
     * Display a specified user.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.user.users.edit', ['id' => $id]));
    }

    /**
     * GET /backend/users/1
     * Display the form for editing a user.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$user = $this->userRepository->findById($id)) {
            abort(404);
        }

        return view('users.edit', [
            'user' => $user,
        ]);
    }

    /**
     * PUT /backend/users/1
     * Update a given user in storage.
     *
     * @param UserRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, $id)
    {
        if (!$user = $this->userRepository->findById($id)) {
            abort(404);
        }

        if ($this->userService->update($request, $user)) {
            Session::flash('success', trans('app.update_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.user.users.edit', $user));
    }

    /**
     * DELETE /backend/users/1
     * Remove a content from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$user = $this->userRepository->findById($id)) {
            abort(404);
        }
        if ($user->delete()) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.user.users.index'));
    }

    /**
     * GET /backend/users/actions
     * Display actions of the users.
     *
     * @return \Illuminate\View\View
     */
    public function actions()
    {
        $condition = new Condition();
        $condition->pagination()
            ->limit(100);
        $data = [
            'actions' => $this->actionRepository->findAll($condition),
        ];
        return view('users.actions', $data);
    }

    /**
     * POST /backend/users/1/restore
     * Restores blocked user.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $condidtion = (new Condition())->withTrashed();
        if (!$user = $this->userRepository->findById($id, $condidtion)) {
            abort(404);
        }
        if ($user->restore()) {
            Session::flash('success', trans('app.restore_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.user.users.index'));
    }
}
