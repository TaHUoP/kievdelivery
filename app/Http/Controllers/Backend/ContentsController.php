<?php

namespace App\Http\Controllers\Backend;

use Api\App\Contracts\Entities\ContentImageRepository as ContentImageInterface;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use App\Http\Requests\Backend\ContentUploadRequest;
use App\Http\Requests\Backend\ContentRequest;
use App\Http\Controllers\Controller;
use App\Exceptions\AlertException;
use App\Services\ContentService;
use Api\App\Storage\Condition;
use App\Http\Requests;
use Storage;
use Request;
use Session;
use DB;

/**
 * Contents Controller
 * @package App\Http\Controllers\Backend
 */
class ContentsController extends Controller
{
    /**
     * @var ContentInterface
     */
    protected $contentRepository;

    /**
     * @var LanguageInterface
     */
    protected $languageRepository;

    /**
     * @var LanguageInterface
     */
    protected $contentImageRepository;

    /**
     * @var ContentService
     */
    protected $contentService;

    /**
     * ContentsController constructor.
     * @param ContentInterface $contentRepository
     * @param LanguageInterface $languageRepository
     * @param ContentImageInterface $contentImageRepository
     * @param ContentService $contentService
     */
    public function __construct(
        ContentInterface $contentRepository,
        LanguageInterface $languageRepository,
        ContentImageInterface $contentImageRepository,
        ContentService $contentService
    )
    {
        $this->contentImageRepository = $contentImageRepository;
        $this->languageRepository = $languageRepository;
        $this->contentRepository = $contentRepository;
        $this->contentService = $contentService;
    }

    /**
     * GET /backend/contents
     * Display all of the contents.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

        $contents = $this->contentRepository->findAll($condition);

        if (Request::ajax()) {
            return view('contents._ajax_index', ['contents' => $contents]);
        }

        return view('contents.index', ['contents' => $contents]);
    }

    /**
     * GET /backend/contents/create
     * Display the form for creating a new content.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $condition = new Condition();
        $condition->scopes(['active']);

        $typesArray = $this->contentRepository->getTypesArray();
        $languages = $this->languageRepository->findAll($condition);

        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

         $data = [
            'typesArray' => $typesArray,
            'languages' => $languages
        ];

        return view('contents.create', $data);
    }

    /**
     * POST /backend/contents
     * Store a new content in storage.
     *
     * @param ContentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContentRequest $request)
    {
        DB::beginTransaction();

        try {
            $result = $this->contentService->save($request->all());
            if ($result) {
                DB::commit();
                Session::flash('success', trans('app.update_success'));
            } else {
                DB::rollBack();
                Session::flash('danger', trans('app.critical_error'));
            }
            DB::commit();
        } catch (AlertException $e) {
            DB::rollBack();
            Session::flash('danger', $e->getInfo());
        }

        return redirect(route('backend.contents.index'));
    }

    /**
     * GET /backend/contents/1
     * Show a content in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.contents.edit', ['id' => $id]));
    }

    /**
     * GET /backend/contents/1
     * Display the form for editing a content.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $condition = new Condition();
        $condition->scopes(['active']);

        if (!$content = $this->contentRepository->findById($id)) {
            abort(404);
        }

        $typesArray = $this->contentRepository->getTypesArray();
        $languages = $this->languageRepository->findAll($condition);

        return view('contents.edit', [
            'content' => $content,
            'typesArray' => $typesArray,
            'languages' => $languages,
        ]);
    }

    /**
     * PUT /backend/contents/1
     * Update a given content in storage.
     *
     * @param ContentRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ContentRequest $request, $id)
    {
        DB::beginTransaction();

        if (!$content = $this->contentRepository->findById($id)) {
            abort(404);
        }

        try {
            $result = $this->contentService->save($request->all(), $content);
            if ($result) {
                DB::commit();
                Session::flash('success', trans('app.update_success'));
            } else {
                DB::rollBack();
                Session::flash('danger', trans('app.critical_error'));
            };
        } catch (AlertException $e) {
            DB::rollBack();
            Session::flash('danger', $e->getInfo());
        }

        return redirect(route('backend.contents.index'));
    }

    /**
     * DELETE /backend/contents/1
     * Remove a content from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$content = $this->contentRepository->findById($id)) {
            abort(404);
        }
        if ($this->contentService->delete($content)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.contents.index'));
    }

    /**
     * POST /backend/contents/upload
     * Upload a content image.
     *
     * @param ContentUploadRequest $request
     */
    public function upload(ContentUploadRequest $request)
    {
        echo $this->contentService->upload($request->file('file'));
    }
}
