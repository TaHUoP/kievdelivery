<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\CurrencyHelper as CurrencyInterface;
use App\Http\Requests\Backend\CurrencyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class CurrenciesController extends Controller
{
    /**
     * @var CurrencyInterface
     */
    protected $currencyHelper;

    /**
     * @param CurrencyInterface $currencyHelper
     */
    public function __construct(CurrencyInterface $currencyHelper)
    {
        $this->currencyHelper = $currencyHelper;
    }

    /**
     * GET /backend/currencies
     * Display all of the currencies.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = [
            'currencies' => $this->currencyHelper->getCurrencies(),
            'defaultCurrency' => $this->currencyHelper->config('default'),
        ];

        if ($request->ajax())
            return view('currencies._ajax_index', $data);

        return view('currencies.index', $data);
    }

    /**
     * GET /backend/currencies/create
     * Display the form for creating a new currency.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('currencies.create');
    }

    /**
     * POST /backend/currencies
     * Store a new currency in storage.
     *
     * @param CurrencyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CurrencyRequest $request)
    {
        $result = $this->currencyHelper->create([
            'name' => $request->input('name'),
            'code' => $request->input('code'),
            'exchange_rate' => $request->input('exchange_rate'),
            'active' => $request->input('active'),
        ]);

        if ($result === 'exists') {
            Session::flash('danger', trans('currencies.msg.exists'));
        } elseif ($result) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.currencies.index'));
    }

    /**
     * GET /backend/currencies/1
     * Show a currency in storage.
     *
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($currency)
    {
        return redirect(route('backend.currencies.edit', ['id' => $currency]));
    }

    /**
     * GET /backend/currencies/1/edit
     * Display the form for editing a currency.
     *
     * @param string $code
     * @return \Illuminate\View\View
     */
    public function edit($currency)
    {
        if (is_null($currency = $this->currencyHelper->find($currency))) {
            abort(404);
        }

        return view('currencies.edit', ['currency' => $currency]);
    }

    /**
     * PUT /backend/currencies/1
     * Update a given currency in storage.
     *
     * @param  string  $currency
     * @param  CurrencyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(CurrencyRequest $request, $currency)
    {
        if (is_null($this->currencyHelper->find($currency))) {
            abort(404);
        }

        $result = $this->currencyHelper->update($currency ,[
            'name' => $request->input('name'),
            'code' => $request->input('code'),
            'exchange_rate' => $request->input('exchange_rate'),
            'active' => $request->input('active'),
        ]);

        if ($result === 'exists') {
            Session::flash('danger', trans('currencies.msg.exists'));
        } elseif ($result) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.currencies.index'));
    }

    /**
     * DELETE /backend/currencies/1
     * Remove a currency from storage.
     *
     * @param string $currency
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($currency)
    {
        if (is_null($this->currencyHelper->find($currency))) {
            abort(404);
        }

        if ($this->currencyHelper->delete($currency)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.currencies.index'));
    }
}
