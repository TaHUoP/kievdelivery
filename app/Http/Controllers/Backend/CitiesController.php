<?php

namespace App\Http\Controllers\Backend;

use Api\App\Contracts\Entities\ContentImageRepository as ContentImageInterface;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use Api\App\Contracts\Entities\CityRepository as CityInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use App\Http\Requests\Backend\ContentUploadRequest;
use App\Http\Requests\Backend\ContentRequest;
use App\Http\Controllers\Controller;
use App\Exceptions\AlertException;
use App\Http\Requests\CityRequest;
use App\Models\City;
use App\Models\PriceType;
use Api\App\Storage\Condition;
use Illuminate\Http\Request as CommonRequest;
use App\Models\Region;
use Request;
use Storage;
use Session;
use DB;

/**
 * Cities Controller
 * @package App\Http\Controllers\Backend
 */
class CitiesController extends Controller
{
    /**
     * @var CityInterface
     */
    protected $cityRepository;

    /**
     * @var LanguageInterface
     */
    protected $languageRepository;

    /**
     * CitiesController constructor.
     * @param CityInterface $cityRepository
     * @param LanguageInterface $languageRepository
     */
    public function __construct(
        CityInterface $cityRepository,
        LanguageInterface $languageRepository
    )
    {
        $this->cityRepository = $cityRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * GET /backend/cities
     * Display all of the cities.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $cities = City::orderBy('alias')->get();

        if (Request::ajax()) {
            return view('cities._ajax_index', ['cities' => $cities]);
        }

        return view('cities.index', ['cities' => $cities]);
    }

    /**
     * GET /backend/cities/create
     * Display the form for creating a new city.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $condition = new Condition();

        $priceTypes = PriceType::all()->pluck('fee', 'id');
        $regions = Region::orderBy('alias')->pluck('alias', 'id');
        $languages = $this->languageRepository->findAll($condition);

        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

         $data = [
            'priceTypes' => $priceTypes,
            'languages' => $languages,
             'regions' => $regions
        ];

        return view('cities.create', $data);
    }

    /**
     * POST /backend/cities
     * Store a new city in storage.
     *
     * @param CommonRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CityRequest $request)
    {
        DB::beginTransaction();

        try {
            $city = new City();
            $result = $this->cityRepository->save($request->all(), $city);
            if ($result) {
                DB::commit();
                Session::flash('success', trans('app.update_success'));
            } else {
                DB::rollBack();
                Session::flash('danger', trans('app.critical_error'));
            }
            DB::commit();
        } catch (AlertException $e) {
            DB::rollBack();
            Session::flash('danger', $e->getInfo());
        }

        return redirect(route('backend.cities.index'));
    }

    /**
     * GET /backend/cities/1
     * Show a city in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.cities.edit', ['id' => $id]));
    }

    /**
     * GET /backend/cities/1
     * Display the form for editing a city.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $condition = new Condition();

        if (!$city = $this->cityRepository->findById($id)) {
            abort(404);
        }

        $priceTypes = PriceType::all()->pluck('fee', 'id');
        $languages = $this->languageRepository->findAll($condition);
        $regions = Region::orderBy('alias')->pluck('alias', 'id');

        return view('cities.edit', [
            'city' => $city,
            'priceTypes' => $priceTypes,
            'languages' => $languages,
            'regions' => $regions
        ]);
    }

    /**
     * PUT /backend/cities/1
     * Update a given content in storage.
     *
     * @param CommonRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CommonRequest $request, $id)
    {
        DB::beginTransaction();

        if (!$city = $this->cityRepository->findById($id)) {
            abort(404);
        }

        try {
            $result = $this->cityRepository->save($request->all(), $city);
            if ($result) {
                DB::commit();
                Session::flash('success', trans('app.update_success'));
            } else {
                DB::rollBack();
                Session::flash('danger', trans('app.critical_error'));
            };
        } catch (AlertException $e) {
            DB::rollBack();
            Session::flash('danger', $e->getInfo());
        }

        return redirect(route('backend.cities.index'));
    }

    /**
     * DELETE /backend/cities/1
     * Remove a content from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$city = $this->cityRepository->findById($id)) {
            abort(404);
        }
        if ($this->cityRepository->delete($city)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.cities.index'));
    }

}
