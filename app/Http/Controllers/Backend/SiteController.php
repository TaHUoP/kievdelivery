<?php

namespace App\Http\Controllers\Backend;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\App\Contracts\Entities\LoggerRepository as LoggerInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Http\Requests\Backend\SettingsRequest;
use Api\App\Storage\Condition;
use Request;

/**
 * Site Controller
 * @package App\Http\Controllers\Backend
 */
class SiteController extends \App\Http\Controllers\Controller
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var LoggerInterface
     */
    protected $logRepository;

    /**
     * @var TranslateInterface
     */
    protected $translateHelper;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param LoggerInterface $logRepository
     * @param ProductInterface $productRepository
     * @param TranslateInterface $translateHelper
     */
    public function __construct(
        ProductInterface $productRepository,
        CheckoutInterface $checkoutRepository,
        LoggerInterface $logRepository,
        TranslateInterface $translateHelper
    )
    {
        $this->productRepository = $productRepository;
        $this->checkoutRepository = $checkoutRepository;
        $this->logRepository = $logRepository;
        $this->translateHelper = $translateHelper;
    }

    /**
     * GET /backend
     * Display index page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Кол-во товаров
        $productsCount = $this->productRepository->getCount();

        // Количество заказов
        $checkoutsAll = $this->checkoutRepository->getCount();

        // Список всех новых заказов со статусом 1
        $condition = new Condition();
        $condition->addCondition('status', 'status', $this->checkoutRepository->getStatusPending())
            ->scopes(['paid' => 0]);
        $checkoutsCountPending = $this->checkoutRepository->getCount($condition);

        // Статистика заказов, общее кол-во и кол-во необработанных
        $condition = new Condition();
        $condition->addCondition('status', 'status', $this->checkoutRepository->getStatusPending())
            ->scopes(['paid' => 0])
            ->limit(35);
        $checkouts = $this->checkoutRepository->findAll($condition);

        if (Request::ajax()) {
            switch (Request::get('action')) {
                case 'checkouts':
                    return view('shop.checkouts._ajax_statistic', [
                        'checkouts' => $checkouts,
                    ]);
                case 'checkouts-count':
                    return view('shop.checkouts._ajax_statistic_count', [
                        'checkoutsCountPending' => $checkoutsCountPending,
                        'checkoutsAll' => $checkoutsAll,
                    ]);
                case 'products-count':
                    return view('shop.products._ajax_statistic_count', [
                        'productsCount' => $productsCount
                    ]);
            }
        }

        return view('site.index', [
            'checkouts' => $checkouts,
            'checkoutsCountPending' => $checkoutsCountPending,
            'checkoutsAll' => $checkoutsAll,
            'productsCount' => $productsCount
        ]);
    }

    /**
     * TODO: В разработке
     *
     * GET /backend/logs
     * Display all of the logs.
     *
     * @return \Illuminate\View\View
     */
    public function logs()
    {
        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

        $logs = $this->logRepository->findAll($condition);

        if (Request::ajax()) {
            return view('site._ajax_logs', ['logs' => $logs]);
        }

        return view('site.logs', ['logs' => $logs]);
    }

    /**
     * TODO: В разработке
     *
     * GET /backend/settings
     * Display all of the logs.
     *
     * @param SettingsRequest $request
     * @return \Illuminate\View\View
     */
    public function settings(SettingsRequest $request)
    {
        $data = [];

        if ($request->method() == 'PUT') {

            echo '<pre>';
            print_r($request->all());
            echo '</pre>';

            die();
        }

        return view('site.settings', $data);
    }

    /**
     * GET /backend/set-lang
     * Change language
     *
     * @param $locale
     * @return \Illuminate\View\View
     */
    public function setLang($locale)
    {
        $back = Request::server('HTTP_REFERER');

        $store = $this->translateHelper->savedLocale($locale, 'backend_language');

        if (empty($store)) {
            return redirect($back ? $back : '/');
        }

        return redirect($back ? $back : '/')->withCookie($store);
    }
}