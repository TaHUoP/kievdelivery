<?php

namespace App\Http\Controllers\Backend;

use Api\App\Contracts\Entities\TranslationRepository as TranslationInterface;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use App\Http\Requests\Backend\LanguageRequest;
use App\Http\Requests\Backend\TranslatesRequest;
use App\Http\Controllers\Controller;
use App\Exceptions\AlertException;
use App\Services\LanguageService;
use Api\App\Storage\Condition;
use Illuminate\Support\Facades\DB;
use Session;
use Request;
use Waavi\Translation\Models\Translation;

/**
 * Languages Controller
 * @package App\Http\Controllers\Backend
 */
class LanguagesController extends Controller
{
    /**
     * @var TranslationInterface
     */
    protected $translationRepository;

    /**
     * @var LanguageInterface
     */
    protected $languageRepository;

    /**
     * @var LanguageService
     */
    protected $languageService;

    /**
     * @param TranslationInterface $translationRepository
     * @param LanguageInterface $languageRepository
     * @param LanguageService $languageService
     */
    public function __construct(
        TranslationInterface $translationRepository,
        LanguageInterface $languageRepository,
        LanguageService $languageService
    )
    {
        $this->translationRepository = $translationRepository;
        $this->languageRepository = $languageRepository;
        $this->languageService = $languageService;
    }

    /**
     * GET /backend/languages
     * Display all of the languages.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $languages = $this->languageService->all();
        $defaultLanguage = $this->languageService->defaultLanguage();
        $notDefaultLanguage = $this->languageService->firstNotDefault();

        if (Request::ajax()) {
            return view('languages._ajax_index', [
                'languages' => $languages,
                'defaultLanguage' => $defaultLanguage,
                'notDefaultLanguage' => $notDefaultLanguage
            ]);
        }

        return view('languages.index', [
            'languages' => $languages,
            'defaultLanguage' => $defaultLanguage,
            'notDefaultLanguage' => $notDefaultLanguage
        ]);
    }

    /**
     * GET /backend/languages/create
     * Display the form for creating a new language.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('languages.create');
    }

    /**
     * POST /backend/languages
     * Store a new language in storage.
     *
     * @param LanguageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LanguageRequest $request)
    {
        $result = $this->languageService->create($request->all());
        if ($result) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.languages.index'));
    }

    /**
     * GET /backend/languages/1
     * Show a language in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.languages.edit', ['id' => $id]));
    }

    /**
     * GET /backend/languages/1/edit
     * Display the form for editing a language.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$language = $this->languageRepository->findById($id)) {
            abort(404);
        }

        $anotherLanguage = $this->languageRepository->other($id);

        return view('languages.edit', ['language' => $language, 'anotherLanguage' => $anotherLanguage]);
    }

    /**
     * PUT /backend/languages/1
     * Update a given post in storage.
     *
     * @param LanguageRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LanguageRequest $request, $id)
    {
        if (!$language = $this->languageRepository->findById($id)) {
            abort(404);
        }

        $old_locale = $language->locale;

        try {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');

            $result = $this->languageService->update($request->all(), $language);

            if ($result) {
                Translation::where('locale', $old_locale)->update([
                    'locale' => $request->input('locale')
                ]);

                Session::flash('success', trans('app.update_success'));
            } else {
                Session::flash('danger', trans('app.critical_error'));
            }

            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        } catch (AlertException $e) {
            Session::flash('danger', $e->getInfo());

            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
        return redirect(route('backend.languages.index'));
    }

    /**
     * DELETE /backend/languages/1
     * Remove a language from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$language = $this->languageRepository->findById($id)) {
            abort(404);
        }

        // delete all translation with this language locale
        Translation::where('locale', $language->locale)->delete();

        if ($this->languageService->delete($language)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.languages.index'));
    }

    /**
     * GET /admin/languages/translations/1
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function translations($id = null)
    {
        // Взять все языки
        $allLanguages = $this->languageService->all();

        // Взять редактируемые языки
        if (!$mainLanguage = $this->languageService->only($id)) {
            abort(404);
        }

        if (Request::has('with') && empty(Request::get('with'))) {
            $params = ['id' => $mainLanguage];
            if (!empty(Request::get('group'))) {
                $params = array_merge($params, ['group' => Request::get('group')]);
            }
            return redirect(route('backend.languages.translations', $params));
        }

        $semiLanguage = Request::get('with') ? $this->languageService->only(Request::get('with')) : null;

        // Взять все языки кроме $id языка
        $allExcept= $this->languageService->allExcept($id);

        // Взять все группы $id языка
        $groups = $this->languageService->groupsOfLanguage($mainLanguage->locale);

        // Взять переводы в удобном формате для формы
        $semiLanguageLocale = isset($semiLanguage) ? $semiLanguage->locale : null;
        $formTranslations = $this->languageService->relativeFormTranslations($mainLanguage->locale, $semiLanguageLocale, Request::get('group', 'app'));

        return view('languages.translations', [
            'all' => $allLanguages,
            'allExcept' => $allExcept,
            'groups' => $groups,
            'mainLanguage' => $mainLanguage,
            'semiLanguage' => $semiLanguage,
            'formTranslations' => $formTranslations,
        ]);
    }

    /**
     * PUT /admin/languages/translations/1
     *
     * @param TranslatesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function translationsUpdate(TranslatesRequest $request)
    {
        try {
            $request->validatePostDataFormat();
            $result = $this->languageService->updateTranslations($request->input('translates'));
            if ($result) {
                Session::flash('success', trans('app.update_success'));
            } else {
                Session::flash('danger', trans('app.critical_error'));
            }
        } catch (AlertException $e) {
            Session::flash('danger', $e->getInfo());
        }

        return back();
    }
}
