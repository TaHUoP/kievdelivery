<?php

namespace App\Http\Controllers\Backend;

use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use App\Http\Requests\Backend\PluginRequest;
use App\Services\PluginService;
use Api\App\Storage\Condition;
use Request;
use Session;
use Route;

/**
 * Plugins Controller
 * @package App\Http\Controllers\Backend
 */
class PluginsController extends \App\Http\Controllers\Controller
{
    /**
     * @var PluginInterface
     */
    protected $pluginRepository;

    /**
     * @var PluginService
     */
    protected $pluginService;

    /**
     * @param PluginInterface $pluginRepository
     * @param PluginService $pluginService
     */
    public function __construct(
        PluginInterface $pluginRepository,
        PluginService $pluginService
    )
    {
        $this->pluginRepository = $pluginRepository;
        $this->pluginService = $pluginService;
    }

    /**
     * GET /backend/plugins
     * Display all of the plugins.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

        $plugins = $this->pluginRepository->findAll($condition);

        if (Request::ajax()) {
            return view('plugins._ajax_index', [
                'plugins' => $plugins
            ]);
        }

        return view('plugins.index', [
            'plugins' => $plugins
        ]);
    }

    /**
     * GET /backend/plugins/create
     * Display the form for creating a new widget.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('plugins.create');
    }

    /**
     * POST /backend/plugins
     * Store a new widget in storage.
     *
     * @param PluginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PluginRequest $request)
    {
        $result = $this->pluginService->save($request->all());

        if ($result) {
            Session::flash('success', trans('app.create_success'));
            return redirect(route('backend.plugins.index'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.plugins.index'));
    }

    /**
     * POST /backend/plugins
     * Update a widget in storage.
     *
     * @param PluginRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PluginRequest $request, $id)
    {
        if (!$plugin = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        $result = $this->pluginService->save($request->all(), $plugin);

        if ($result) {
            Session::flash('success', trans('app.create_success'));
            return redirect(route('backend.plugins.index'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.plugins.index'));
    }

    /**
     * GET /backend/plugins/1
     * Show a widget in storage.
     *
     * @param $id
     * @return \Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        return redirect(route('backend.plugins.edit', ['id' => $id]));
    }

    /**
     * GET /backend/plugins/1/edit
     * Display the form for editing a widget.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$plugin = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        return view('plugins.edit', [
            'plugin' => $plugin
        ]);
    }

    /**
     * GET /backend/plugins/1/control
     * Display the form for editing a widget.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function control($id)
    {
        if (!$plugin = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        $route = 'backend.plugins.' . $plugin->type . '.edit';

        if (!Route::has($route)) {
            abort(404);
        }

        return redirect(route($route, $plugin));
    }

    /**
     * DELETE /backend/plugins/1
     * Remove a plugin from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (!$plugin = $this->pluginRepository->findById($id)) {
            abort(404);
        }

        if ($this->pluginService->delete($plugin)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.plugins.index'));
    }
}
