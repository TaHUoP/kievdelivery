<?php

namespace App\Http\Controllers\Backend;

/**
 * Help Controller
 * @package App\Http\Controllers\Backend
 */
class HelpController extends \App\Http\Controllers\Controller
{
    /**
     * GET /backend/help
     * Display help page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('help.index', []);
    }

}