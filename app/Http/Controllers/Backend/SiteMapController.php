<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SiteMapService;

class SiteMapController extends Controller
{

    protected $siteMapService;

    public function __construct(
        SiteMapService $siteMapService
    )
    {
        $this->siteMapService = $siteMapService;
    }


    /*Вызов класса для создания SiteMap*/
    public function createSiteMap(){

        $this->siteMapService->createSiteMap();
    }
}
