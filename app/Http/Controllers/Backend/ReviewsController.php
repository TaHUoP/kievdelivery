<?php

namespace App\Http\Controllers\Backend;

use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use App\Http\Requests\Reviews\Backend\ReviewsRequest;
use App\Exceptions\AlertException;
use App\Services\ReviewsService;
use Api\App\Storage\Condition;
use Response;
use Request;
use Session;

/**
 * Review Controller
 * @package App\Http\Controllers
 */
class ReviewsController extends \App\Http\Controllers\Controller
{
    /**
     * @var ReviewInterface
     */
    protected $reviewRepository;

    /**
     * @var ReviewInterface
     */
    protected $productRepository;

    /**
     * @var ReviewsService
     */
    protected $reviewsService;

    /**
     * @param ReviewInterface $reviewRepository
     * @param ReviewsService $reviewsService
     * @param ProductInterface $productRepository
     */
    public function __construct(
        ProductInterface $productRepository,
        ReviewInterface $reviewRepository,
        ReviewsService $reviewsService
    )
    {
        $this->productRepository = $productRepository;
        $this->reviewRepository = $reviewRepository;
        $this->reviewsService = $reviewsService;
    }

    /**
     * GET /reviews
     * Display all reviews
     *
     * @return mixed
     */
    public function index()
    {
        $condition = new Condition();
        $condition
            ->pagination();

        $reviews = $this->reviewRepository->findLastAll($condition);

        if (Request::ajax()) {
            return view('reviews._ajax_index', ['reviews' => $reviews]);
        }

        return view('reviews.index', ['reviews' => $reviews]);
    }

    /**
     * @param ReviewsRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $id
     */
    public function update(ReviewsRequest $request, $id)
    {
        if (!$review = $this->reviewRepository->findById($id)) {
            abort(404);
        }

        try {
            $result = $this->reviewsService->saveFromBack($request->all(), $review);

            if ($result) {
                Session::flash('success', trans('app.create_success'));
                return redirect(route('backend.reviews.index'));
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }

        } catch (AlertException $e) {
            return Response::json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$review = $this->reviewRepository->findById($id)) {
            abort(404);
        }

        $types = $this->reviewRepository->getTypesArray();

        return view('reviews.edit', [
            'review' => $review,
            'types' => $types
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $types = $this->reviewRepository->getTypesArray();

        return view('reviews.create', ['types' => $types]);
    }

    /**
     * @param ReviewsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ReviewsRequest $request)
    {
        if ((!$product = $this->productRepository->findById($request->product_id)) && ($request['type'] == 'product')) {
            abort(404);
        }

        try {
            $result = $this->reviewsService->saveFromBack($request->all());
            if ($result) {
                Session::flash('success', trans('app.create_success'));
                return redirect(route('backend.reviews.index'));
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            return Response::json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (!$review = $this->reviewRepository->findById($id)) {
            abort(404);
        }

        if ($this->reviewsService->delete($review)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.reviews.index'));
    }
}
