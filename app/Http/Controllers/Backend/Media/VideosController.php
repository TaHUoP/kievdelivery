<?php

namespace App\Http\Controllers\Backend\Media;

use Api\Media\Contracts\Entities\MediaRepository as MediaRepository;
use App\Http\Requests\Media\Backend\VideoRequest;
use App\Services\Media\VideoService;
use App\Http\Controllers\Controller;
use Api\App\Storage\Condition;
use Response;
use Request;
use Session;

/**
 * Videos Controller
 * @package App\Http\Controllers\Backend\Media
 */
class VideosController extends Controller
{
    /**
     * @var MediaRepository
     */
    protected $mediaRepository;

    /**
     * @var VideoService
     */
    protected $videoService;

    /**
     * @param MediaRepository $mediaRepository
     * @param VideoService $videoService
     */
    public function __construct(
        MediaRepository $mediaRepository,
        VideoService $videoService
    )
    {
        $this->mediaRepository = $mediaRepository;
        $this->videoService = $videoService;
    }

    /**
     * GET /backend/media/videos
     * Display all of the videos.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination()
            ->addCondition('type', 'type', $this->mediaRepository->getTypeVideo())
            ->limit(100);

        $videos = $this->mediaRepository->findAll($condition);

        if (Request::ajax()) {
            return view('media.videos._ajax_index', ['videos' => $videos]);
        }

        return view('media.videos.index', ['videos' => $videos]);
    }

    /**
     * GET /backend/media/videos/create
     * Display the form for creating a new video.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('media.videos.create');
    }

    /**
     * POST /backend/media/videos
     * Store a new video in storage.
     *
     * @param VideoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VideoRequest $request)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.media.videos.index'));
        }

        $result = $this->videoService->save($request->all());
        if ($result) {
            return Response::json(['message' => trans('app.create_success')]);
        }

        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * GET /backend/media/videos/1
     * Show a video in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.media.videos.edit', ['id' => $id]));
    }

    /**
     * GET /backend/media/videos/1
     * Display the form for editing a video.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$video = $this->mediaRepository->findById($id)) {
            abort(404);
        }
        return view('media.videos.edit', ['video' => $video]);
    }

    /**
     * PUT /backend/media/videos/1
     * Update a given video in storage.
     *
     * @param VideoRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VideoRequest $request, $id)
    {
        if (!$video = $this->mediaRepository->findById($id)) {
            abort(404);
        }
        $data = $request->all();
        if ($this->videoService->save($data, $video)) {
            Session::flash('success', trans('app.update_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.media.videos.index'));
    }

    /**
     * DELETE /backend/media/videos/1
     * Remove a video from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$video = $this->mediaRepository->findById($id)) {
            abort(404);
        }
        if ($this->videoService->delete($video)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.media.videos.index'));
    }
}
