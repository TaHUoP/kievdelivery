<?php

namespace App\Http\Controllers\Backend\Media;

use Api\Media\Contracts\Entities\MediaRepository as MediaRepository;
use App\Http\Requests\Media\Backend\ImageRequest;
use App\Services\Media\ImageService;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Response;
use Session;
use Request;
use File;
use DB;

/**
 * Images Controller
 * @package App\Http\Controllers\Backend\Media
 */
class ImagesController extends \App\Http\Controllers\Controller
{
    /**
     * @var MediaRepository
     */
    protected $mediaRepository;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @param MediaRepository $mediaRepository
     * @param ImageService $imageService
     */
    public function __construct(
        MediaRepository $mediaRepository,
        ImageService $imageService
    )
    {
        $this->mediaRepository = $mediaRepository;
        $this->imageService = $imageService;
    }

    /**
     * GET /backend/media/images
     * Display all of the images.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination()
            ->addCondition('type', 'type', $this->mediaRepository->getTypeImage())
            ->limit(100);

        $images = $this->mediaRepository->findAll($condition);

        if (Request::ajax()) {
            return view('media.images._ajax_index', ['images' => $images]);
        }

        return view('media.images.index', ['images' => $images]);
    }

    /**
     * POST /backend/media/1/images/upload
     * Display the form for editing a image.
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(ImageRequest $request)
    {
        $file = $request->file('uploadfile');
        try {
            $result = $this->imageService->upload($file);
            if ($result) {
                DB::commit();
                return Response::json(['message' => trans('app.upload_success'), 'id' => $result]);
            } else {
                DB::rollBack();
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            DB::rollBack();
            return Response::json(['message' => trans('app.critical_error')], 500);
        }
    }

    /**
     * GET /backend/media/images/1/edit
     * Display the form for editing a image.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$image = $this->mediaRepository->findById($id)) {
            abort(404);
        }
        return view('media.images.modal._edit', [
            'image' => $image,
        ]);
    }

    /**
     * PUT /backend/media/images/1
     * Update a given image in storage.
     *
     * @param ImageRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ImageRequest $request, $id)
    {
        if (!$image = $this->mediaRepository->findById($id)) {
            abort(404);
        }
        try {
            $result = $this->imageService->save($request->all(), $image);
            if ($result) {
                DB::commit();
                return Response::json(['message' => trans('app.update_success')]);
            } else {
                DB::rollBack();
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            DB::rollBack();
            return Response::json(['message' => trans('app.critical_error')], 500);
        }
    }

    /**
     * DELETE /backend/media/images/1
     * Remove a image from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (!$image = $this->mediaRepository->findById($id)) {
            abort(404);
        }
        if ($this->imageService->delete($image)) {
            return Response::json(['message' => trans('app.delete_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}
