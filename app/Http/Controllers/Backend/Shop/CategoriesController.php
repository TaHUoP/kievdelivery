<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Shop\Backend\CategoryRequest;
use App\Services\Shop\CategoryService;
use Api\App\Storage\Condition;
use Response;
use Request;
use Session;

/**
 * Categories Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class CategoriesController extends \App\Http\Controllers\Controller
{
    /**
     * @var CategoryInterface
     */
    protected $categoryRepository;

    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * @param CategoryInterface $categoryRepository
     * @param TranslateHelperInterface $translateHelper
     * @param CategoryService $categoryService
     */
    public function __construct(
        CategoryInterface $categoryRepository,
        TranslateHelperInterface $translateHelper,
        CategoryService $categoryService
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->translateHelper = $translateHelper;
        $this->categoryService = $categoryService;
    }

    /**
     * GET /backend/shop/categories
     * Display all of the categories.
     *
     * @return mixed
     */
    public function index()
    {
        $condition = new Condition();
        $condition->limit(null);

        if (Request::ajax() && !empty(Request::get('search'))) {
            $condition
                ->addCondition('search_name', 'name', Request::get('search'))
                ->limit(25);

            $categories = $this->categoryRepository->findAll($condition);
            $result = [];
            foreach ($categories as $category) {
                $result[] = [
                    'id' => $category->id,
                    'text' => $this->translateHelper->get($category->translations, 'name'),
                ];
            }
            return Response::json(['items' => $result]);
        }

        // TODO: не должны знать про toTree()
        $categories = $this->categoryRepository->findAll($condition)->toTree();

        if (Request::ajax()) {
            return view('shop.categories._ajax_index', ['categories' => $categories]);
        }

        return view('shop.categories.index', ['categories' => $categories]);
    }

    /**
     * GET /backend/shop/categories/create
     * Display the form for creating a new category.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $condition = new Condition();
        $condition->limit(null);

        // TODO: не должны знать про toTree()
        $categories = $this->categoryRepository->findAll($condition)->toTree();

        if (Request::ajax()) {
            return view('shop.categories._ajax_create', ['categories' => $categories]);
        }

        return view('shop.categories.create', ['categories' => $categories]);
    }

    /**
     * POST /backend/shop/categories
     * Store a new category in storage.
     *
     * @param CategoryRequest $request
     * @return mixed
     */
    public function store(CategoryRequest $request)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.categories.index'));
        }

        if ($result = $this->categoryService->save($request->all())) {
            // TODO: не должны знать про toTree()
            $condition = new Condition();
            $condition->limit(null);
            $categories = $this->categoryRepository->findAll($condition)->toTree();
            return view('shop.categories._ajax_index', ['categories' => $categories]);
        }

        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * GET /backend/shop/categories/1
     * Show a category in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.shop.categories.edit', ['id' => $id]));
    }

    /**
     * GET /backend/shop/categories/1/edit
     * Display the form for editing a category.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$category = $this->categoryRepository->findById($id)) {
            abort(404);
        }

        $condition = new Condition();
        $condition->limit(null);

        // TODO: не должны знать про toTree()
        $categories = $this->categoryRepository->findAll($condition)->toTree();

        if (Request::ajax()) {
            return view('shop.categories._ajax_edit', [
                'categories' => $categories,
                'category' => $category,
            ]);
        }

        return view('shop.categories.edit', [
            'categories' => $categories,
            'category' => $category
        ]);
    }

    /**
     * PUT /backend/shop/categories/1
     * Update a given category in storage.
     *
     * @param CategoryRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CategoryRequest $request, $id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.categories.index'));
        }

        if (!$category = $this->categoryRepository->findById($id)) {
            abort(404);
        }

        $result = $this->categoryService->update($request->all(), $category);

        if ($result) {
            $category = $this->categoryRepository->findById($category->id);

            $condition = new Condition();
            $condition->limit(null);

            // TODO: не должны знать про toTree()
            $categories = $this->categoryRepository->findAll($condition)->toTree();

            return view('shop.categories._ajax_edit', [
                'categories' => $categories,
                'category' => $category,
            ]);
        }

        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * DELETE /backend/shop/categories/1
     * Remove a category from storage.
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        if (!$category = $this->categoryRepository->findById($id)) {
            abort(404);
        }
        
        $result = $this->categoryService->delete($category);

        if (!Request::ajax()) {
            return redirect(route('backend.shop.categories.index'));
        }

        if ($result) {
            $condition = new Condition();
            $condition->limit(null);
            // TODO: не должны знать про toTree()
            $categories = $this->categoryRepository->findAll($condition)->toTree();
            return view('shop.categories._ajax_index', ['categories' => $categories]);
        }

        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}
