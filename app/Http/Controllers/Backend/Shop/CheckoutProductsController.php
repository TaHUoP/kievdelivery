<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use App\Http\Requests\Shop\Backend\CheckoutProductRequest;
use App\Http\Requests\Shop\Backend\CheckoutRequest;
use App\Services\Shop\CheckoutService;
use App\Services\Shop\CartService;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Response;
use Request;
use Session;
use DB;

/**
 * Checkout Products Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class CheckoutProductsController extends \App\Http\Controllers\Controller
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CartInterface
     */
    protected $cartRepository;

    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param CartInterface $cartRepository
     * @param ProductInterface $productRepository
     * @param CartService $cartService
     */
    public function __construct(
        CheckoutInterface $checkoutRepository,
        CartInterface $cartRepository,
        ProductInterface $productRepository,
        CartService $cartService
    )
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
        $this->cartService = $cartService;
    }

    /**
     * Добавить товар
     *
     * @param CheckoutProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CheckoutProductRequest $request)
    {
        if (!$checkout = $this->checkoutRepository->findById($request->input('checkout_id'))) {
            abort(404);
        }

        if (!Request::ajax() || !$checkout) {
            return Response::json(['message' => '404'], 404);
        }

        $product = $this->productRepository->findById($request->input('product_id'));

        $this->cartService->setCart($checkout->cart);
        $this->cartService->add($product, 1);

        return view('shop.checkouts._ajax_products', [
            'checkout' => $this->checkoutRepository->findById($request->input('checkout_id'))
        ]);
    }

    /**
     * Изменить количество товаров
     *
     * @param $id
     * @param CheckoutProductRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function update($id, CheckoutProductRequest $request)
    {
        $condition = new Condition();
        $condition->addCondition('cart_product_info', 'id', $id);

        if (!$cart = $this->cartRepository->findOne($condition)) {
            abort(404);
        }

        $product = null;
        foreach ($cart->products as $relation) {
            if ($relation->id == $id) {
                $product = $relation->product;
                break;
            }
        }

        if (!Request::ajax() || !$cart || !$product) {
            return Response::json(['message' => '404'], 404);
        }
        
        $this->cartService->setCart($cart);
        $this->cartService->change($product, $request->input('amount'));

        return view('shop.checkouts._ajax_products', [
            'checkout' => $this->checkoutRepository->findById($cart->checkout->id)
        ]);
    }

    /**
     * Удалить товар
     *
     * @param $id
     * @param CheckoutProductRequest $request
     * @return mixed
     */
    public function destroy($id, CheckoutProductRequest $request)
    {
        $condition = new Condition();
        $condition->addCondition('cart_product_info', 'id', $id);

        if (!$cart = $this->cartRepository->findOne($condition)) {
            abort(404);
        }

        $product = null;
        foreach ($cart->products as $relation) {
            if ($relation->id == $id) {
                $product = $relation->product;
                break;
            }
        }

        if (!Request::ajax() || !$cart || !$product) {
            return Response::json(['message' => '404'], 404);
        }

        $this->cartService->setCart($cart);
        $this->cartService->remove($product);

        return view('shop.checkouts._ajax_products', [
            'checkout' => $this->checkoutRepository->findById($cart->checkout->id)
        ]);
    }
}
