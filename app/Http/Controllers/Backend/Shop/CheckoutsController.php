<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use App\Http\Requests\Shop\Backend\CheckoutRequest;
use App\Services\Shop\CheckoutService;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Request;
use Session;
use DB;

/**
 * Checkouts Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class CheckoutsController extends \App\Http\Controllers\Controller
{
    /**
     * @var CheckoutInterface
     */
    protected $checkoutRepository;

    /**
     * @var CheckoutService
     */
    protected $checkoutService;

    /**
     * @param CheckoutInterface $checkoutRepository
     * @param CheckoutService $checkoutService
     */
    public function __construct(CheckoutInterface $checkoutRepository, CheckoutService $checkoutService)
    {
        $this->checkoutRepository = $checkoutRepository;
        $this->checkoutService = $checkoutService;
    }

    /**
     * GET /backend/shop/checkouts
     * Display all of the checkouts.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

        $checkouts = $this->checkoutRepository->findAll($condition);

        if (Request::ajax()) {
            return view('shop.checkouts._ajax_index', ['checkouts' => $checkouts]);
        }

        return view('shop.checkouts.index', ['checkouts' => $checkouts]);
    }

    /**
     * GET /backend/shop/checkouts/create
     * Display the form for creating a new checkout.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $statusArray = $this->checkoutRepository->getStatusArray();

        return view('shop.checkouts.create',[
            'statusArray' => $statusArray
        ]);
    }

    /**
     * POST /backend/shop/checkouts
     * Store a new checkout in storage.
     *
     * @param CheckoutRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CheckoutRequest $request)
    {
        try {
            $result = $this->checkoutService->save($request->all());
            if ($result) {
                Session::flash('success', trans('app.update_success'));
            } else {
                Session::flash('danger', trans('app.critical_error'));
            }
        } catch (AlertException $e) {
            Session::flash('danger', $e->getInfo());
        }
        return redirect(route('backend.shop.checkouts.index'));
    }

    /**
     * GET /backend/shop/checkouts/1
     * Show a checkout in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.shop.checkouts.edit', ['id' => $id]));
    }

    /**
     * GET /backend/shop/checkouts/1/edit
     * Display the form for editing a checkout.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$checkout = $this->checkoutRepository->findById($id)) {
            abort(404);
        }

        $statusArray = $this->checkoutRepository->getStatusArray();

        return view('shop.checkouts.edit', [
            'checkout' => $checkout,
            'statusArray' => $statusArray
        ]);
    }

    /**
     * PUT /backend/shop/checkouts/1
     * Update a given checkout in storage.
     *
     * @param CheckoutRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CheckoutRequest $request, $id)
    {
        if (!$checkout = $this->checkoutRepository->findById($id)) {
            abort(404);
        }
        try {
            $result = $this->checkoutService->save($request->all(), $checkout);
            if ($result) {
                Session::flash('success', trans('app.update_success'));
            } else {
                Session::flash('danger', trans('app.critical_error'));
            }
        } catch (AlertException $e) {
            Session::flash('danger', $e->getInfo());
        }
        return redirect(route('backend.shop.checkouts.index'));
    }

    /**
     * DELETE /backend/shop/checkouts/1
     * Remove a checkout from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$checkout = $this->checkoutRepository->findById($id)) {
            abort(404);
        }
        if ($this->checkoutService->delete($checkout)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.checkouts.index'));
    }
}
