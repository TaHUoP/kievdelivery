<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use App\Contracts\Helpers\TranslateHelper as TranslateHelperInterface;
use App\Http\Requests\Shop\Backend\PropertyRequest;
use App\Services\Shop\PropertyService;
use App\Http\Controllers\Controller;
use Api\App\Storage\Condition;
use App\Http\Requests;
use Response;
use Request;
use Session;

/**
 * Properties Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class PropertiesController extends Controller
{
    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @var TranslateHelperInterface
     */
    protected $translateHelper;

    /**
     * @var PropertyService
     */
    protected $propertyService;

    /**
     * @param PropertyInterface $propertyRepository
     * @param TranslateHelperInterface $translateHelper
     * @param PropertyService $propertyService
     */
    public function __construct(
        PropertyInterface $propertyRepository,
        TranslateHelperInterface $translateHelper,
        PropertyService $propertyService
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->translateHelper = $translateHelper;
        $this->propertyService = $propertyService;
    }

    /**
     * GET /backend/shop/properties
     * Display all of the properties.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        $condition = new Condition();
        $condition
            ->addCondition('search_name', 'name', Request::get('search'))
            ->relations([])
            ->limit(25);
        $properties = $this->propertyRepository->findAll($condition);
        $result = [];
        foreach ($properties as $property) {
            $result[] = [
                'id' => $property->id,
                'text' => $this->translateHelper->get($property->translations, 'name'),
            ];
        }
        return Response::json(['items' => $result]);
    }

    /**
     * POST /backend/shop/properties
     * Store a new property in storage.
     *
     * @param PropertyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PropertyRequest $request)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        $result = $this->propertyService->save($request->all());
        if ($result) {
            return Response::json(['message' => trans('app.create_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * GET /backend/shop/properties/1/edit
     * Display the form for editing a property.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        if (!$property = $this->propertyRepository->findById($id)) {
            abort(404);
        }
        return view('shop.properties.modal._edit', ['property' => $property]);
    }

    /**
     * PUT /backend/shop/properties/1
     * Update a given property in storage.
     *
     * @param PropertyRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PropertyRequest $request, $id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        if (!$property = $this->propertyRepository->findById($id)) {
            abort(404);
        }
        $result = $this->propertyService->save($request->all(), $property);
        if ($result) {
            return Response::json(['message' => trans('app.update_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * DELETE /backend/properties/1
     * Remove a property from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        if (!$property = $this->propertyRepository->findById($id)) {
            abort(404);
        }
        $result = $this->propertyService->delete($property);
        if ($result) {
            return Response::json(['message' => trans('app.delete_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}
