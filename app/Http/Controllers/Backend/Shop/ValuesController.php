<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\ValueRepository as ValueInterface;
use App\Http\Requests\Shop\Backend\ValueRequest;
use App\Http\Controllers\Controller;
use App\Services\Shop\ValueService;
use App\Http\Requests;
use Response;
use Request;
use Session;

/**
 * Values Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class ValuesController extends Controller
{
    /**
     * @var ValueService
     */
    protected $valueService;

    /**
     * @var ValueInterface
     */
    protected $valueRepository;

    /**
     * @param ValueService $valueService
     * @param ValueInterface $valueRepository
     */
    public function __construct(ValueService $valueService, ValueInterface $valueRepository)
    {
        $this->valueRepository = $valueRepository;
        $this->valueService = $valueService;
    }

    /**
     * POST /backend/shop/values
     * Store a new value in storage.
     *
     * @param ValueRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ValueRequest $request)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        $result = $this->valueService->save($request->all());
        if ($result) {
            return Response::json(['message' => trans('app.create_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * GET /backend/shop/values/1/edit
     * Display the form for editing a value.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        if (!$value = $this->valueRepository->findById($id)) {
            abort(404);
        }
        return view('shop.values.modal._edit', ['value' => $value]);
    }

    /**
     * PUT /backend/shop/values/1
     * Update a given value in storage.
     *
     * @param ValueRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ValueRequest $request, $id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        if (!$value = $this->valueRepository->findById($id)) {
            abort(404);
        }
        $result = $this->valueService->save($request->all(), $value);
        if ($result) {
            return Response::json(['message' => trans('app.update_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }

    /**
     * DELETE /backend/shop/values/1
     * Remove a value from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!Request::ajax()) {
            return redirect(route('backend.shop.filter.index'));
        }
        if (!$value = $this->valueRepository->findById($id)) {
            abort(404);
        }
        $result = $this->valueService->delete($value);
        if ($result) {
            return Response::json(['message' => trans('app.delete_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}
