<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use App\Contracts\Helpers\ShopHelper as ShopInterface;
use App\Http\Requests\Shop\Backend\FilterParamRequest;
use App\Http\Requests\Shop\Backend\ProductRequest;
use App\Http\Requests\Shop\Backend\FilterRequest;
use App\Http\Requests\Shop\Backend\ProductFilesRequest;
use App\Models\Shop\PaymentSystem;
use App\Models\Shop\Product;
use App\Services\Shop\ProductService;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Response;
use Session;
use Request;
use File;
use DB;

/**
 * Products Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class ProductsController extends \App\Http\Controllers\Controller
{
    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var CategoryInterface
     */
    protected $categoryRepository;

    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var ShopInterface
     */
    protected $shopHelper;

    /**
     * @param PropertyInterface $propertyRepository
     * @param CategoryInterface $categoryRepository
     * @param ProductInterface $productRepository
     * @param ProductService $productService
     * @param ShopInterface $shopHelper
     */
    public function __construct(
        PropertyInterface $propertyRepository,
        CategoryInterface $categoryRepository,
        ProductInterface $productRepository,
        ProductService $productService,
        ShopInterface $shopHelper

    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->productService = $productService;
        $this->shopHelper = $shopHelper;
    }

    /**
     * GET /backend/shop/products
     * Display all of the products.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $filters = Request::input('s');
        $orderBy = Request::input('orderBy');
        $condition = new Condition();

        if (isset($filters['product'])) {

            // Поиск по id и переход
            $condition->addCondition('id', 'id', $filters['product']);
            if ($product = $this->productRepository->findOne($condition)) {
                return redirect(route('backend.shop.products.update', $product));
            } else {
                //Поиск по вендору и переход
                $condition = new Condition();
                $condition->addCondition('vendor_code', 'vendor_code', $filters['product']);
                if ($product = $this->productRepository->findOne($condition)) {
                    return redirect(route('backend.shop.products.update', $product));
                }
            }
        }

        if (! Request::input('without_pagination')) {
            $condition->pagination()->limit(100);
        } else {
            $condition->limit(Product::count());
        }

        $condition
            ->relations(['categories', 'properties', 'images']);

        if ($filters) {
            $condition = $this->productService->createConditionForFilter($condition, $filters);
        }

        if ($orderBy) {
            $condition->orderBy($orderBy);
        }

        $products = $this->productRepository->filterSearch($condition);

        if (Request::ajax()) {
            return view('shop.products._ajax_index', [
                'products' => $products,
                'orderBy' => $orderBy
            ]);
        }

        return view('shop.products.index', [
            'products' => $products
        ]);
    }

    /**
     * GET /backend/shop/products/create
     * Display the form for creating a new product.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categoryRepository->findByNested('root', null, false);
        $paymentSystems = PaymentSystem::active()->get()->pluck('name', 'id');

        return view('shop.products.create', [
            'product' => null,
            'categories' => $categories,
            'paymentSystems' => $paymentSystems,
        ]);
    }

    /**
     * POST /backend/shop/products
     * Store a new product in storage.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        try {
            $result = $this->productService->save($request->all());
            if ($result) {
                Session::flash('success', trans('app.create_success'));
                return redirect(route('backend.shop.products.edit', ['id' => $result]));
            } else {
                Session::flash('danger', trans('app.critical_error'));
            }
        } catch (AlertException $e) {
            Session::flash('danger', $e->getInfo());
        }
        return redirect(route('backend.shop.products.index'));
    }

    /**
     * GET /backend/shop/products/1
     * Show a product in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.shop.products.edit', ['id' => $id]));
    }

    /**
     * GET /backend/shop/product/1/edit
     * Display the form for editing a product.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }

        $categories = $this->categoryRepository->findByNested('root', null, false);
        $filter = $this->productService->combineFilterByCategories($product->categories);
        $paymentSystems = PaymentSystem::active()->get()->pluck('name', 'id');

        return view('shop.products.edit', [
            'product' => $product,
            'categories' => $categories,
            'filter' =>  $filter,
            'paymentSystems' => $paymentSystems,
        ]);
    }

    /**
     * PUT /backend/shop/products/1
     * Update a given product in storage.
     *
     * @param ProductRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, $id)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }
        try {
            $result = $this->productService->save($request->all(), $product);
            if ($result) {
                Session::flash('success', trans('app.update_success'));
            } else {
                Session::flash('danger', trans('app.critical_error'));
            }
        } catch (AlertException $e) {
            Session::flash('danger', $e->getInfo());
        }
        return redirect(route('backend.shop.products.edit', $product));
    }

    /**
     * DELETE /backend/shop/products/1
     * Remove a product from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }
        if ($this->productService->delete($product)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.products.index'));
    }

    /**
     * POST /backend/shop/products/1/filter
     * Filter control by product
     *
     * @param FilterRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function filter(FilterRequest $request, $id)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }
        
        $result = $this->productService->saveFilter($product, $request->all());
        
        if (Request::ajax()) {
            if ($result) {
                return Response::json(['message' => trans('app.update_success')]);
            }
            return Response::json(['message' => trans('app.critical_error')], 500);
        }

        if ($result) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.shop.products.edit', $product));
    }

    /**
     * POST /backend/shop/products/1/filter-params
     * Filter control by product
     *
     * @param FilterParamRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function filterParam(FilterParamRequest $request, $id)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }
        $result = $this->productService->saveFilterParam($product, $request);

        if (Request::ajax()) {
            if ($result) {
                return Response::json(['message' => trans('app.update_success')]);
            }
            return Response::json(['message' => trans('app.critical_error')], 500);
        }

        if ($result) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.shop.products.edit', $product));
    }

    /**
     * POST /backend/shop/products/1/files
     * Files control by product
     *
     * @param ProductFilesRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function files(ProductFilesRequest $request, $id)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }

        $result = $this->productService->updateProductFiles($request->input('files', []), $product);

        if (Request::ajax()) {
            if ($result) {
                return Response::json(['message' => trans('app.update_success')]);
            }
            return Response::json(['message' => trans('app.critical_error')], 500);
        }

        if ($result) {
            Session::flash('success', trans('app.update_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect(route('backend.shop.products.edit', $product));
    }
}
