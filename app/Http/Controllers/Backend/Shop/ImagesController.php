<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\Shop\Contracts\Entities\ImageRepository as ImageInterface;
use App\Http\Requests\Shop\Backend\ImageRequest;
use App\Services\Shop\ImageService;
use App\Exceptions\AlertException;
use Api\App\Storage\Condition;
use Response;
use Session;
use Request;
use File;
use DB;

/**
 * Images Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class ImagesController extends \App\Http\Controllers\Controller
{
    /**
     * @var ProductInterface
     */
    protected $productRepository;

    /**
     * @var ImageInterface
     */
    protected $imageRepository;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @param ProductInterface $productRepository
     * @param ImageInterface $imageRepository
     * @param ImageService $imageService
     */
    public function __construct(
        ProductInterface $productRepository,
        ImageInterface $imageRepository,
        ImageService $imageService
    )
    {
        $this->productRepository = $productRepository;
        $this->imageRepository = $imageRepository;
        $this->imageService = $imageService;
    }

    /**
     * POST /backend/shop/1/images/upload
     * Display the form for editing a image.
     *
     * @param $id
     * @param ImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload($id, ImageRequest $request)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }
        $file = $request->file('uploadfile');
        try {
            $result = $this->imageService->upload($file, $product->id);
            if ($result) {
                return Response::json(['message' => trans('app.upload_success'), 'id' => $result]);
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            return Response::json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * POST /backend/shop/1/images/order
     * Display the form for editing a image.
     *
     * @param $id
     * @param ImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function order($id, ImageRequest $request)
    {
        if (!$product = $this->productRepository->findById($id)) {
            abort(404);
        }
        $ids = Request::input('ids');
        $ids = is_array($ids) ? $ids : [];
        try {
            $result = $this->imageService->order($ids, $product->id);
            if ($result) {
                return Response::json(['message' => trans('app.update_success')]);
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            return Response::json(['message' => trans('app.critical_error')], 500);
        }
    }

    /**
     * GET /backend/shop/images/1/edit
     * Display the form for editing a image.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$image = $this->imageRepository->findById($id)) {
            abort(404);
        }
        return view('shop.images._modal_form', [
            'image' => $image,
        ]);
    }

    /**
     * PUT /backend/shop/images/1
     * Update a given image in storage.
     *
     * @param ImageRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ImageRequest $request, $id)
    {
        if (!$image = $this->imageRepository->findById($id)) {
            abort(404);
        }
        try {
            $result = $this->imageService->save($request->all(), $image);
            if ($result) {
                return Response::json(['message' => trans('app.update_success')]);
            } else {
                return Response::json(['message' => trans('app.critical_error')], 500);
            }
        } catch (AlertException $e) {
            return Response::json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * DELETE /backend/shop/images/1
     * Remove a image from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$image = $this->imageRepository->findById($id)) {
            abort(404);
        }
        if ($this->imageService->delete($image)) {
            return Response::json(['message' => trans('app.delete_success')]);
        }
        return Response::json(['message' => trans('app.critical_error')], 500);
    }
}
