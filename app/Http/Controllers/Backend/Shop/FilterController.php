<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\Shop\Contracts\Entities\ValueRepository as ValueInterface;
use App\Http\Controllers\Controller;
use Api\App\Storage\Condition;
use App\Http\Requests;
use Session;
use Request;

/**
 * Filter Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class FilterController extends Controller
{
    /**
     * @var PropertyInterface
     */
    protected $propertyRepository;

    /**
     * @var ValueInterface
     */
    protected $valueRepository;

    /**
     * @param PropertyInterface $propertyRepository
     * @param ValueInterface $valueRepository
     */
    public function __construct(
        PropertyInterface $propertyRepository,
        ValueInterface $valueRepository
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->valueRepository = $valueRepository;
    }

    /**
     * GET /backend/filter
     * Display filter page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination();

        $properties = $this->propertyRepository->findAll($condition);

        if (Request::ajax()) {
            return view('shop.filter._ajax_index', ['properties' => $properties]);
        }

        return view('shop.filter.index', ['properties' => $properties]);
    }
}
