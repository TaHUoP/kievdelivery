<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\DiscountRepository as DiscountInterface;
use App\Http\Requests\Shop\Backend\DiscountRequest;
use App\Http\Controllers\Controller;
//use App\Services\Shop\DiscountService;
use Api\App\Storage\Condition;
use Request;
use Session;

/**
 * Discounts Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class DiscountsController extends Controller
{
    /**
     * @var DiscountInterface
     */
    protected $discountRepository;

    /**
     * @param DiscountInterface $discountRepository
     */
    public function __construct(DiscountInterface $discountRepository)
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * GET /backend/shop/discounts
     * Display all of the discounts.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination();
        $discounts = $this->discountRepository->findAll($condition);

        if (Request::ajax()) {
            return view('shop.discounts._ajax_index', ['discounts' => $discounts]);
        }

        return view('shop.discounts.index', ['discounts' => $discounts]);
    }

    /**
     * GET /backend/shop/discounts/create
     * Display the form for creating a new discount.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('shop.discounts.create');
    }

    /**
     * POST /backend/shop/discounts
     * Store a new discount in storage.
     *
     * @param DiscountRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DiscountRequest $request)
    {
        $data = $request->all();
        $data['date_from'] = date('Y-m-d', strtotime($data['date_from']));
        $data['date_to'] = date('Y-m-d', strtotime($data['date_to']));
        $result = $this->discountRepository->save($data);
        if ($result) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.discounts.index'));
    }

    /**
     * GET /backend/shop/discounts/1
     * Show a discount in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.shop.discounts.edit', ['id' => $id]));
    }

    /**
     * GET /backend/shop/discounts/1/edit
     * Display the form for editing a discount.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$discount = $this->discountRepository->findById($id)) {
            abort(404);
        }
        return view('shop.discounts.edit', ['discount' => $discount]);
    }

    /**
     * PUT /backend/shop/discounts/1
     * Update a given discount in storage.
     *
     * @param DiscountRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DiscountRequest $request, $id)
    {
        if (!$discount = $this->discountRepository->findById($id)) {
            abort(404);
        }
        $data = $request->all();
        $data['date_from'] = date('Y-m-d', strtotime($data['date_from']));
        $data['date_to'] = date('Y-m-d', strtotime($data['date_to']));
        if ($this->discountRepository->save($data, $discount)) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.discounts.index'));
    }

    /**
     * DELETE /backend/shop/discounts/1
     * Remove a discount from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$discount = $this->discountRepository->findById($id)) {
            abort(404);
        }
        if ($this->discountRepository->delete($discount)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.discounts.index'));
    }
}
