<?php

namespace App\Http\Controllers\Backend\Shop;

use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use App\Http\Requests\Shop\Backend\CouponRequest;
use App\Http\Controllers\Controller;
use App\Services\Shop\CouponService;
use Api\App\Storage\Condition;
use Request;
use Session;

/**
 * Coupons Controller
 * @package App\Http\Controllers\Backend\Shop
 */
class CouponsController extends Controller
{
    /**
     * @var CouponInterface
     */
    protected $couponRepository;

    /**
     * @var CouponService
     */
    protected $couponService;

    /**
     * @param CouponInterface $couponRepository
     * @param CouponService $couponService
     */
    public function __construct(CouponInterface $couponRepository, CouponService $couponService)
    {
        $this->couponRepository = $couponRepository;
        $this->couponService = $couponService;
    }

    /**
     * GET /backend/shop/coupons
     * Display all of the coupons.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination();
        $coupons = $this->couponRepository->findAll($condition);

        if (Request::ajax()) {
            return view('shop.coupons._ajax_index', ['coupons' => $coupons]);
        }

        return view('shop.coupons.index', ['coupons' => $coupons]);
    }

    /**
     * GET /backend/shop/coupons/create
     * Display the form for creating a new coupon.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('shop.coupons.create');
    }

    /**
     * POST /backend/shop/coupons
     * Store a new coupon in storage.
     *
     * @param CouponRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CouponRequest $request)
    {
        $data = $request->all();
        $data['date_expiry'] = date('Y-m-d H:i:s', strtotime($data['date_expiry']));
        $result = $this->couponService->save($data);
        if ($result) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.coupons.index'));
    }

    /**
     * GET /backend/shop/coupons/1
     * Show a coupon in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.shop.coupons.edit', ['id' => $id]));
    }

    /**
     * GET /backend/shop/coupons/1/edit
     * Display the form for editing a coupon.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$coupon = $this->couponRepository->findById($id)) {
            abort(404);
        }
        return view('shop.coupons.edit', ['coupon' => $coupon]);
    }

    /**
     * PUT /backend/shop/coupons/1
     * Update a given coupon in storage.
     *
     * @param CouponRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CouponRequest $request, $id)
    {
        if (!$coupon = $this->couponRepository->findById($id)) {
            abort(404);
        }
        $data = $request->all();
        $data['date_expiry'] = date('Y-m-d H:i:s', strtotime($data['date_expiry']));
        if ($this->couponService->save($data, $coupon)) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.coupons.index'));
    }

    /**
     * DELETE /backend/shop/coupons/1
     * Remove a coupon from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$coupon = $this->couponRepository->findById($id)) {
            abort(404);
        }
        if ($this->couponService->delete($coupon)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.coupons.index'));
    }
}
