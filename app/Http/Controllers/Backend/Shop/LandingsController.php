<?php

namespace App\Http\Controllers\Backend\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\Backend\LandingRequest;
use App\Modules\LandingModule\Services\LandingsService;
use Request;
use Sabre\DAV\Exception;
use Session;

/**
 * Class LandingsController
 * @package App\Http\Controllers\Backend\Shop
 */
class LandingsController extends Controller
{
    /**
     * @var LandingsService
     */
    private $landingsService;

    /**
     * @param LandingsService $landingsService
     */
    public function __construct(LandingsService $landingsService)
    {
        $this->landingsService = $landingsService;
    }

    /**
     * GET /backend/shop/landings
     * Display all of the coupons.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $landingModels = $this->landingsService->getAll();

        if (Request::ajax()) {
            return view('shop.landings._ajax_index', ['landings' => $landingModels]);
        }

        return view('shop.landings.index', ['landings' => $landingModels]);
    }

    /**
     * GET /backend/shop/landings/create
     * Display the form for creating a new coupon.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('shop.landings.create');
    }

    /**
     * POST /backend/shop/landings
     * Store a new landing in storage.
     *
     * @param LandingRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LandingRequest $request)
    {
        $landingModel = $this->landingsService->create($request);
        if ($landingModel->translations->count()) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.landings.index'));
    }

    /**
     * GET /backend/shop/landings/1
     * Show a landing in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.shop.landings.edit', ['id' => $id]));
    }

    /**
     * GET /backend/shop/landings/1/edit
     * Display the form for editing a coupon.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$landing = $this->landingsService->findById($id)) {
            abort(404);
        }
        return view('shop.landings.edit', ['landing' => $landing]);
    }

    /**
     * PUT /backend/shop/landings/1
     * Update a given landing in storage.
     *
     * @param LandingRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LandingRequest $request, $id)
    {
        if (!$landing = $this->landingsService->findById($id)) {
            abort(404);
        }
        if ($this->landingsService->update($request, $landing)) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.shop.landings.index'));
    }

    /**
     * DELETE /backend/shop/landings/1
     * Remove a landing from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$landing = $this->landingsService->findById($id)) {
            abort(404);
        }
        $this->landingsService->delete($landing);
        Session::flash('success', trans('app.delete_success'));
        return redirect(route('backend.shop.landings.index'));
    }
}
