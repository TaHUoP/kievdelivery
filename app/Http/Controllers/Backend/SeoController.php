<?php

namespace App\Http\Controllers\Backend;

use Api\App\Contracts\Entities\SeoSpecificationRepository as SeoSpecificationInterface;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use App\Http\Requests\Backend\SeoSpecificationRequest;
use Api\App\Storage\Condition;
use App\Services\SeoService;
use App\Http\Requests;
use Request;
use Session;

/**
 * Seo Controller
 * @package App\Http\Controllers\Backend
 */
class SeoController extends \App\Http\Controllers\Controller
{
    /**
     * @var SeoSpecificationInterface
     */
    protected $seoSpecificationRepository;

    /**
     * @var LanguageInterface
     */
    protected $languageRepository;

    /**
     * @var SeoService
     */
    protected $seoSpecificationService;

    /**
     * @param SeoSpecificationInterface $seoSpecificationRepository
     * @param SeoService $seoSpecificationService
     * @param LanguageInterface $languageRepository
     */
    public function __construct(
        SeoSpecificationInterface $seoSpecificationRepository,
        SeoService $seoSpecificationService,
        LanguageInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
        $this->seoSpecificationRepository = $seoSpecificationRepository;
        $this->seoSpecificationService = $seoSpecificationService;
    }

    /**
     * GET /backend/contents
     * Display all of the records.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $condition = new Condition();
        $condition->pagination()
            ->limit(100);

        $records = $this->seoSpecificationRepository->findAll($condition);

        if (Request::ajax()) {
            return view('seo._ajax_index', ['records' => $records]);
        }

        return view('seo.index', ['records' => $records]);
    }

    /**
     * GET /backend/contents/create
     * Display the form for creating a new record.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $condition = new Condition();
        $condition->scopes(['active']);

        $languages = $this->languageRepository->findAll($condition);

        return view('seo.create',[
            'languages' => $languages
        ]);
    }

    /**
     * POST /backend/contents
     * Store a new record in storage.
     *
     * @param SeoSpecificationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SeoSpecificationRequest $request)
    {
        $result = $this->seoSpecificationService->save($request->all());
        if ($result) {
            Session::flash('success', trans('app.create_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.seo.index'));
    }

    /**
     * GET /backend/seo/1
     * Show a record in storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(route('backend.seo.edit', ['id' => $id]));
    }

    /**
     * GET /backend/seo/1/edit
     * Display the form for editing a record.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        if (!$seo = $this->seoSpecificationRepository->findById($id)) {
            abort(404);
        }

        $condition = new Condition();
        $condition->scopes(['active']);

        $languages = $this->languageRepository->findAll($condition);

        return view('seo.edit', [
            'record' => $seo,
            'languages' => $languages
        ]);
    }

    /**
     * PUT /backend/seo/1
     * Update a given record in storage.
     *
     * @param SeoSpecificationRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SeoSpecificationRequest $request, $id)
    {
        if (!$widget = $this->seoSpecificationRepository->findById($id)) {
            abort(404);
        }
        if ($this->seoSpecificationService->save($request->all(), $widget)) {
            Session::flash('success', trans('app.update_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.seo.edit', ['id' => $id]));
    }

    /**
     * DELETE /backend/seo/1
     * Remove a record from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (!$content = $this->seoSpecificationRepository->findById($id)) {
            abort(404);
        }
        if ($this->seoSpecificationService->delete($content)) {
            Session::flash('success', trans('app.delete_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }
        return redirect(route('backend.seo.index'));
    }
}
