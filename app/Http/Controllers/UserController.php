<?php

namespace App\Http\Controllers;

use Api\User\Contracts\Entities\UserRepository as UserInterface;
use Api\User\Contracts\Entities\ProfileRepository as ProfileInterface;
use App\Http\Requests\User\ManagePasswordRequest;
use App\Http\Requests\User\ChangeEmailRequest;
use App\Http\Requests\User\MainInfoRequest;
use Redirect;
use Session;
use Config;
use Auth;
use Hash;

/**
 * User Controller
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    protected $userRepository;

    /**
     * @var ProfileInterface
     */
    protected $profileRepository;

    /**
     * @param UserInterface $userRepository
     * @param ProfileInterface $profileRepository
     */
    public function __construct(UserInterface $userRepository, ProfileInterface $profileRepository)
    {
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * GET /account
     * Display user profile
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $account = $this->userRepository->findById($user->id);

        return view('user.index', [
            'account' => $account
        ]);
    }

    /**
     * POST /main-info
     *
     * @param MainInfoRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postMainInfo(MainInfoRequest $request)
    {
        $user = Auth::user();
        $profileModel = $this->profileRepository->findById($user->id);
        $result = $this->profileRepository->save($request->only(['name', 'surname']), $profileModel);

        return $this->handleUpdateResult($result);
    }

    /**
     * POST /manage-password
     *
     * @param ManagePasswordRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postManagePassword(ManagePasswordRequest $request)
    {
        $user = Auth::user();
        if (!Hash::check($request->input('cur_password'), $user->password)) {
            return redirect()
                ->back()
                ->withErrors(['cur_password' => trans('validation.password-doesnt-match')]);
        }

        $model = $this->userRepository->findById($user->id);

        $result = $this->userRepository->save([
            'password' => bcrypt($request->input('new_password'))
        ], $model);

        return $this->handleUpdateResult($result);
    }

    /**
     * POST /change-email
     * TODO: подтверждение email необходимо через письмо
     *
     * @param ChangeEmailRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postChangeEmail(ChangeEmailRequest $request)
    {
        $model = $this->userRepository->findById(Auth::user()->id);

        $result = $this->userRepository->save([
            'email' => $request->input('new_email')
        ], $model);

        return $this->handleUpdateResult($result);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::guard(Config::get('auth.frontendGuard'))->logout();
        Auth::guard(Config::get('auth.backendGuard'))->logout();
        
        return Redirect::route('guest.signin');
    }

    /**
     * Обрабатывает результат выполнения апдейта
     *
     * @param $result
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function handleUpdateResult($result)
    {
        if ($result) {
            Session::flash('success', trans('app.update_success'));
        } else {
            Session::flash('danger', trans('app.critical_error'));
        }

        return redirect()->back();
    }
}
