<?php
/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/
Route::match(['GET', 'POST'], '/auth', [
    'prefix' => 'backend',
    'as' => 'backend.auth.login',
    'uses' => 'Backend\User\AuthController@login',
    'middleware' => ['web', 'language:backend', 'backend', 'setTheme:backend']
]);

Route::group(['prefix' => 'backend', 'namespace' => 'Backend', 'middleware' => ['web', 'language:backend', 'backend', 'auth:backend', 'setTheme:backend']], function () {

    // Site
    Route::get('/', ['as' => 'backend.site.index', 'uses' => 'SiteController@index']);
    Route::get('/page/logs', ['as' => 'backend.site.logs', 'uses' => 'SiteController@logs']);
    Route::match(['GET', 'POST'], '/page/settings', ['as' => 'backend.site.settings', 'uses' => 'SiteController@settings']);


    // Languages
    Route::get('/set-lang/{lang}', ['as' => 'backend.site.set-lang', 'uses' => 'SiteController@setLang']);
    Route::get('/languages/translations/{id}', ['as' => 'backend.languages.translations', 'uses' => 'LanguagesController@translations']);
    Route::put('/languages/translationUpdate', ['as' => 'backend.languages.translationsUpdate', 'uses' => 'LanguagesController@translationsUpdate']);

    // Modules
    Route::resource('contents', 'ContentsController');
    Route::resource('cities', 'CitiesController');
    // Route::resource('widgets', 'WidgetsController');
    Route::resource('seo', 'SeoController');
    Route::resource('languages', 'LanguagesController');
    Route::resource('currencies', 'CurrenciesController');
    //Route::resource('access', 'AccessController');
    Route::resource('reviews', 'ReviewsController');

    // Plugins
    Route::resource('plugins', 'PluginsController');
    Route::get('/plugins/control/{id}', [
        'as' => 'backend.plugins.control',
        'uses' => 'PluginsController@control'
    ]);

    // Content Extra
    Route::post('/contents/upload', ['as' => 'backend.contents.upload', 'uses' => 'ContentsController@upload']);
    Route::post('/contents/upload/images/{content}', ['as' => 'backend.content.images.upload', 'uses' => 'ContentImagesController@upload']);
    Route::resource('contentImages', 'ContentImagesController', ['only' => [
        'edit', 'update', 'destroy'
    ]]);

    // Help
    Route::get('/help', ['as' => 'backend.help.index', 'uses' => 'HelpController@index']);

    // Shop
    Route::group(['prefix' => 'shop', 'namespace' => 'Shop'], function ()
    {
        Route::get('/filter', ['as' => 'backend.shop.filter.index', 'uses' => 'FilterController@index']);

        Route::resource('checkoutProducts', 'CheckoutProductsController', ['only' => [
            'index', 'store', 'update', 'destroy'
        ]]);

        Route::resource('categories', 'CategoriesController');
        Route::resource('checkouts', 'CheckoutsController');
        Route::resource('coupons', 'CouponsController');
        Route::resource('landings', 'LandingsController');
        Route::resource('discounts', 'DiscountsController');
        Route::resource('products', 'ProductsController');
        Route::post('/products/{id}/files', ['as' => 'backend.products.files', 'uses' => 'ProductsController@files']);
        Route::post('/products/{id}/filter', ['as' => 'backend.products.filter', 'uses' => 'ProductsController@filter']);
        Route::post('/products/{id}/filter-param', ['as' => 'backend.products.filter.param', 'uses' => 'ProductsController@filterParam']);

        Route::resource('properties', 'PropertiesController', ['only' => [
            'index', 'create', 'store', 'edit', 'update', 'destroy'
        ]]);
        Route::resource('values', 'ValuesController', ['only' => [
            'store', 'update', 'destroy', 'create', 'edit'
        ]]);
        Route::resource('images', 'ImagesController', ['only' => [
            'edit', 'update', 'destroy'
        ]]);
        Route::post('/{id}/images/upload', ['as' => 'backend.shop.images.upload', 'uses' => 'ImagesController@upload']);
        Route::post('/{id}/images/order', ['as' => 'backend.shop.images.order', 'uses' => 'ImagesController@order']);
    });

    // Media
    Route::group(['prefix' => 'media', 'namespace' => 'Media'], function ()
    {
        Route::resource('videos', 'VideosController');
        Route::resource('images', 'ImagesController', ['only' => [
            'index', 'edit', 'update', 'destroy'
        ]]);
        Route::post('/images/upload', ['as' => 'backend.media.images.upload', 'uses' => 'ImagesController@upload']);
    });

    // User
    Route::group(['prefix' => 'user', 'namespace' => 'User'], function ()
    {
        // Auth
        Route::get('/logout', ['as' => 'backend.auth.logout', 'uses' => 'AuthController@logout']);

        Route::resource('users', 'UsersController');
        Route::post('users/{id}/restore', 'UsersController@restore')->name('backend.user.users.restore');
    });

    Route::group(['prefix' => 'plugins', 'namespace' => 'Plugins'], function () {

        // Slider
        Route::get('/slider/edit/{id}', [
            'as' => 'backend.plugins.slider.edit',
            'uses' => '\App\Plugins\Slider\Backend\SliderController@edit'
        ]);
        Route::post('/slider/update/{id}', [
            'as' => 'backend.plugins.slider.update',
            'uses' => '\App\Plugins\Slider\Backend\SliderController@update'
        ]);

        Route::get('/slider/destroy/{id}', [
            'as' => 'backend.plugins.slider.destroy',
            'uses' => '\App\Plugins\Slider\Backend\SliderController@destroy'
        ]);

        Route::resource('text', '\App\Plugins\Text\Backend\TextController');
    });

});

/*
|--------------------------------------------------------------------------
| App routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['guest', 'web']], function () {
    Route::group(['prefix' => 'signin', 'as' => 'guest.'], function () {
        Route::get('{profile_type}', 'OAuthController@redirectToProvider')->name('oauth.redirect');
        Route::get('{profile_type}/callback', 'OAuthController@handleProviderCallback')->name('oauth.callback');
        Route::post('{profile_type}/signup', 'OAuthController@auth')->name('oauth.signup');
    });
});

$frontend_attr = ['middleware' => ['web', 'frontend', 'language:frontend', 'setTheme:default']];
$locale = Request::segment(1);

if(Translate::getLangByLocale($locale) && empty(Translate::getLangByLocale($locale)['default'])) {
    $frontend_attr['prefix'] = $locale;
}


//TODO: fix this route (same route in Payment group doesn't work)

Route::match(['get', 'post'], '/platon/callback', ['as' => 'platon.callback', 'uses' => 'Payment\PlatonController@callback']);

Route::group($frontend_attr, function () {

    // Site
    Route::get('/badbrowser', ['as' => 'site.badbrowser', 'uses' => 'SiteController@badbrowser']);
    Route::get('/contacts', ['as' => 'pages.contacts', 'uses' => 'SiteController@getContacts']);
    //Route::get('/sitemap.xml', ['as' => 'site.sitemap', 'uses' => 'SiteController@sitemap']);
    Route::get('/statics/js-data', ['uses' => 'SiteController@jsData']);

    Route::post('/contacts', ['as' => 'pages.contacts', 'uses' => 'SiteController@postContacts']);

    Route::get('/photos', ['as' => 'pages.photos', 'uses' => 'SiteController@photos']);
    Route::get('/videos', ['as' => 'pages.videos', 'uses' => 'SiteController@videos']);
    Route::get('/customorder', ['as' => 'pages.customorder', 'uses' => 'SiteController@getСustomorder']);
    Route::post('/customorder', ['as' => 'pages.customorder', 'uses' => 'SiteController@postСustomorder']);

    Route::get('/set-curr/{curr}', ['as' => 'site.set-curr', 'uses' => 'SiteController@setCurrency']);

    Route::get('/search', ['as' => 'product.search', 'uses' => 'ProductController@getSearch']);

    Route::get('/404.html', ['as' => 'error.404', 'uses' => 'SiteController@notFound']);



    Route::group(['middleware' => ['guest']], function () {

        // Login
        Route::get('/signin', ['uses' => 'GuestController@getSignin', 'as' => 'guest.signin']);
        Route::post('/signin', ['uses' => 'GuestController@postSignin', 'as' => 'guest.signin']);

        // Registration
        Route::get('/signup', ['uses' => 'GuestController@getSignup', 'as' => 'guest.signup']);
        Route::post('/signup', ['uses' => 'GuestController@postSignup', 'as' => 'guest.signup']);

        // Password Reset
        Route::get('/password/reset/{token?}', ['uses' => 'GuestController@getReset', 'as' => 'guest.password-reset']);
        Route::post('/password/email', ['uses' => 'GuestController@postEmail', 'as' => 'guest.password-email']);
        Route::post('/password/reset', ['uses' => 'GuestController@postReset', 'as' => 'guest.password-reset']);
    });

    // Email Confirmation Form
    Route::get('/signup/email-confirmation', [
        'uses' => 'GuestController@getConfirmForm',
        'as' => 'email_confirmation_form'
    ]);

    // Email Confirmation
    Route::get('/signup/email-confirm', [
        'uses' => 'GuestController@getConfirm',
        'as' => 'email_confirm'
    ]);

    Route::post('/buy-now', ['as' => 'cart.buy-now', 'uses' => 'CartController@postBuyNow']);

    Route::group(['middleware' => ['auth:web']], function () {
        // Аккаунт
        Route::get('/account', ['as' => 'user.index', 'uses' => 'UserController@index']);
        Route::post('/main-info', ['as' => 'user.main-info', 'uses' => 'UserController@postMainInfo']);
        Route::post('/manage-password', ['as' => 'user.manage-password', 'uses' => 'UserController@postManagePassword']);
        Route::post('/change-email', ['as' => 'user.change-email', 'uses' => 'UserController@postChangeEmail']);

        // История заказов
        Route::get('/orders', ['as' => 'shop.checkout.orders', 'uses' => 'CheckoutController@orders']);
        Route::get('/order/{id}', ['as' => 'shop.checkout.order', 'uses' => 'CheckoutController@order']);
    });

    // Выход
    Route::get('/logout', ['uses' => 'UserController@getLogout', 'middleware' => ['auth:web']]);

    // Reviews
    Route::get('/testimonials', ['as' => 'review.index', 'uses' => 'ReviewController@index']);
    Route::post('/review', ['as' => 'review.store', 'uses' => 'ReviewController@store']);
    Route::post('/review-main', ['as' => 'review.store.main', 'uses' => 'ReviewController@storeMain']);

    // Content
    Route::get('/page/{url}', ['as' => 'site.page', 'uses' => 'SiteController@page']);
    Route::get('/ukraine/{url}', ['as' => 'site.ukraine-page', 'uses' => 'SiteController@pageUkraine']);
    Route::get('/news/{url}', ['as' => 'news.show', 'uses' => 'NewsController@show']);
    Route::get('/news', ['as' => 'news.index', 'uses' => 'NewsController@index']);

    // Shop
    Route::get('/checkout', ['as' => 'shop.checkout', 'uses' => 'CheckoutController@index']);
    Route::post('/checkout', ['as' => 'shop.checkout', 'uses' => 'CheckoutController@postCheckout']);
    Route::get('/checkout/success/{id}', ['as' => 'shop.checkout.success', 'uses' => 'CheckoutController@success']);
    Route::post('/check-coupon', ['as' => 'shop.check.coupon', 'uses' => 'CheckoutController@checkCoupon']);
    Route::delete('/delete-coupon', ['as' => 'shop.delete.coupon', 'uses' => 'CheckoutController@deleteCoupon']);
    Route::post('/cart/control', ['as' => 'shop.cart.control', 'uses' => 'CartController@control']);
    Route::get('/cart', ['as' => 'shop.cart', 'uses' => 'CartController@index']);

    Route::get('/category/{alias}/{filter?}', ['as' => 'shop.category', 'uses' => 'CategoryController@show'])->where('filter', '(.*)');


    // Products
    Route::get('/products/{filter?}', ['as' => 'shop.products', 'uses' => 'ProductController@index'])->where('filter', '(.*)');
    Route::post('/products/', ['as' => 'shop.products', 'uses' => 'ProductController@index']);
    Route::get('/product/{alias}', ['as' => 'shop.product', 'uses' => 'ProductController@show']);

    Route::group(['namespace' => 'Payment'], function () {
        // PayPal
        Route::get('/paypal/success', ['as' => 'paypal.success', 'uses' => 'PayPalController@success']);
        Route::get('/paypal/cancel', ['as' => 'paypal.cancel', 'uses' => 'PayPalController@cancel']);
        Route::match(['get', 'post'], '/paypal/result', ['as' => 'paypal.result', 'uses' => 'PayPalController@result']);
        Route::get('/paypal/{id}', ['as' => 'paypal.index', 'uses' => 'PayPalController@index']);

        // Way for pay
        Route::post('/wayforpay/return', ['as' => 'wayforpay.returnUrl', 'uses' => 'WayForPayController@returnUrl']);
        Route::post('/wayforpay/service', ['as' => 'wayforpay.serviceUrl', 'uses' => 'WayForPayController@serviceUrl']);
        Route::get('/wayforpay/{id}', ['as' => 'wayforpay.index', 'uses' => 'WayForPayController@index']);

        // Multicards
    //    Route::get('/bestsellers/cart/confirm/{id}', ['as' => 'multicards.index', 'uses' => 'MulticardsController@index']);
    //    Route::match(['GET', 'POST'], '/multicard/successful', ['as' => 'multicards.post', 'uses' => 'MulticardsController@returnUrl']);

        // Platon
        Route::get("platon/{checkout_id}", ['as' => 'platon.process', 'uses' => 'PlatonController@process']);
    });
    Route::post('/city', ['as' => 'city.update', 'uses' => 'SiteController@cityUpdate']);
    // this route should be the last in the routs list
    Route::get('/{city_alias?}', ['as' => 'site.index', 'uses' => 'SiteController@index']);
});
