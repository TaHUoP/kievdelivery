<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Frontend
 * @package App\Http\Middleware
 */
class Frontend
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
