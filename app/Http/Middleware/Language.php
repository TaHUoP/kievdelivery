<?php

namespace App\Http\Middleware;

use App\Contracts\Helpers\TranslateHelper as TranslateInterface;
use Closure;
use Config;
use Auth;
use Illuminate\Foundation\Application;

/**
 * Language
 * @package App\Http\Middleware
 */
class Language
{
    /**
     * @var TranslateInterface
     */
    private $translate;

    /**
     * @param TranslateInterface $translate
     */
    public function __construct(TranslateInterface $translate, Application $app)
    {
        $this->translate = $translate;
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @param null $keyPrefix
     * @return mixed
     */
    public function handle($request, Closure $next, $keyPrefix = null)
    {
        if($keyPrefix == 'frontend') {
            $locale = $request->segment(1);

            if(!$this->translate->getLangByLocale($locale)) {
                $language = $this->translate->getDefaultLang();
                $locale = $language['locale'];
            }

            $this->app->setLocale($locale);
        } else {
            $this->translate->init($keyPrefix . '_language');
        }

        return $next($request);
    }
}
