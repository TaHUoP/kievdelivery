<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

/**
 * Verify Csrf Token
 * @package App\Http\Middleware
 */
class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/paypal/success',
        '/paypal/cancel',
        '/paypal/result',

        '/wayforpay/return',
        '/wayforpay/service',
        '/multicard/successful',
    ];
}
