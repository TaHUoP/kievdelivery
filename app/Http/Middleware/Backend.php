<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Auth;

/**
 * Backend
 * @package App\Http\Middleware
 */
class Backend
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Namespace для виджетов
        Config::set('laravel-widgets.default_namespace', 'App\Widgets\Backend');

        // Редирект на страницу логина
        Config::set('auth.redirectToLogin', 'backend.auth.login');

        // Перерегистрация класса AuthHelper
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('AuthHelper', \App\Helpers\Backend\AuthHelper::class);

        // Дополнительная проверка на доступ к спанеле
        if (!Auth::guard(Config::get('auth.backendGuard'))->guest()) {
            if (!Auth::guard(Config::get('auth.backendGuard'))->user()->isAuthorizedForCpanel()) {
                abort(403, '403 Access is denied');
            }
        }

        return $next($request);
    }
}
