<?php

namespace App\Models;

class PriceType extends \Illuminate\Database\Eloquent\Model
{
    public function getPostfixAttribute()
    {
        $postfix = '';

        switch ($this->attributes['fee']) {
            case 15 : $postfix = trans('cities.close region delivery fee');
            break;
            case 25 : $postfix = trans('cities.far region delivery fee');
        }

        return $postfix;
    }
}
