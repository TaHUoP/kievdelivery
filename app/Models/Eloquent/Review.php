<?php

namespace App\Models;

use App\Events\ReviewCreated;
use App\Models\User\User;
use App\Models\Shop\Product;
use Illuminate\Support\Facades\Log;

/**
 * Review
 * @package App\Models
 */
class Review extends \App\Models\Model
{
    const TYPE_PRODUCT = 'product';
    const TYPE_MAIN = 'main';

    protected $table = 'reviews';

    protected $fillable = [
        'name',
        'email',
        'city_delivery',
        'admin_answer',
        'text',
        'confirmed',
        'type',
        'rating'
    ];

    protected $casts = [
        'created_at' => 'datetime'
    ];

    public static function created($callback, $priority = 0)
    {
        parent::created($callback, $priority);

        try {
            event(new ReviewCreated());
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    // TODO: удалить когда будет на проде
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * Пользователь
     *
     * @param $query
     * @return mixed
     */
    public function scopeAuthor($query)
    {
        return $query->with(['user' => function($query) {
            $query->with('profile');
        }]);
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Подтверждение
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeConfirmed($query, $state = 1)
    {
        return $query->where('confirmed', $state);
    }

    /**
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            self::TYPE_PRODUCT => trans('reviews.type.product'),
            self::TYPE_MAIN => trans('reviews.type.main'),
        ];
    }
}
