<?php

namespace App\Models\Shop;


/**
 * Cart
 * @package App\Models\Shop
 */
class PaymentSystem extends \App\Models\Model
{

    /**
     * @var string
     */
    protected $table = 'payment_systems';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_payment_system', 'payment_system_id', 'product_id');
    }

    public function scopeActive($q)
    {
        $q->where('is_active', true);
    }
}
