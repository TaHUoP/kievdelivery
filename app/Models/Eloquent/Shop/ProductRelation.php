<?php

namespace App\Models\Shop;

/**
 * Product Relation
 * @package App\Models\Shop
 */
class ProductRelation extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_products_properties_values';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function property()
    {
        return $this->hasOne(Property::class, 'id', 'property_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function value()
    {
        return $this->hasOne(Value::class, 'id', 'value_id');
    }
}
