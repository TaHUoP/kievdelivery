<?php

namespace App\Models\Shop;

use App\Models\City;
use App\Models\Shop\Translate\Product as ProductTranslate;
use App\Models\Shop\Belongs\ProductCategories;
use App\Models\Review;
use DB;
use Illuminate\Support\Facades\Cookie;

/**
 * Product
 * @package App\Models\Shop
 */
class Product extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_products';

    protected $with = ['categories', 'paymentSystems'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relations()
    {
        return $this->hasMany(ProductRelation::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, ProductCategories::getTableName(), 'product_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(ProductTranslate::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        //return $this->hasMany(Image::class, 'id', 'product_id')->orderAsc();
        return $this->hasMany(Image::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(ProductFile::class, 'product_id', 'id');
    }

    /**
     * @return \App\Models\Review
     */
    public function reviews()
    {
        return $this->hasMany(Review::class, 'product_id', 'id');
    }

    public function avgReviewRating()
    {
        return $this->reviews->where('confirmed', 1)->avg('rating');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function paymentSystems()
    {
        return $this->belongsToMany(PaymentSystem::class, 'product_payment_system', 'product_id', 'payment_system_id');
    }

    /**
     *  Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }

    /**
     * Сортировка desc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка asc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Scope visible
     *
     * @param $query
     * @param int $scope
     * @return mixed
     */
    public function scopeVisible($query, $scope = 1)
    {
        return $query->where('visible', $scope);
    }

    /**
     * Scope state
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeMain($query, $state = 2)
    {
        return $query->where('main', $state);
    }

    /**
     * Сортировка по свойству
     *
     * @param $query
     * @param $property
     * @param int $sort
     * @return mixed
     */
    public function scopeOrderByProperty($query, $property, $sort = SORT_DESC)
    {
        $sort = ($sort === SORT_DESC) ? 'desc' : 'asc';
        return $query->leftJoin(ProductRelation::getTableName() . ' as ppv', 'ppv.product_id', '=', self::getTableName() . '.id')
            ->leftJoin(Property::getTableName(), 'ppv.property_id', '=', Property::getTableName() . '.id')
            ->select([
                self::getTableName() . '.*',
                'ppv.custom_value as ' . $property,
            ])
            ->where(function ($query) use ($property) {
                $query->where(Property::getTableName() . '.alias', $property);
                $query->orWhereNull(Property::getTableName() . '.alias');
            })
            ->orderBy(DB::raw('convert(' . $property . ', unsigned)'), $sort);
    }

    /**
     * Все данные по товару
     *
     * @param $query
     * @param array $relations
     * @return mixed
     */
    public function scopeTotal($query, array $relations = ['images', 'categories', 'properties', 'files'])
    {
        $with = ['translations'];

        if (in_array('files', $relations)) {
            $with = array_merge($with, ['files']);
        }

        if (in_array('images', $relations)) {
            $with = array_merge($with, ['images' => function($query) {
                $query->orderAscAndFirstDefault();
                $query->translates();
            }]);
        }

        if (in_array('categories', $relations)) {
            $with = array_merge($with, ['categories' => function ($query) {
                $query->with('translations');
                $query->with(['properties' => function($query) {
                    $query->translates();
                    $query->with(['values' => function ($query) {
                        $query->translates();
                    }]);
                }]);
            }]);
        }

        if (in_array('properties', $relations)) {
            $with = array_merge($with, ['relations' => function ($query) {
                $query->orderBy('order', 'asc');
                $query->with([
                    'property' => function ($query) {
                        $query->translates();
                    },
                    'value' => function ($query) {
                        $query->translates();
                    }
                ]);
            }]);
        }

        return $query->with($with);
    }

    /**
     * Возвращает default image
     *
     * @return array
     */
    public function getDefaultImage()
    {
        foreach ($this->images as $image) {
            if ($image->default) {
                return $image;
            }
        }
        return [];
    }

    public function getSaleCategoryAttribute()
    {
        return $this->categories->where('alias', Category::SALE_ALIAS)->first();
    }

    public function getKyivOnlyCategoryAttribute()
    {
        return $this->categories->where('alias', Category::KYIV_ONLY_ALIAS)->first();
    }

    /**
     * Форматирует элиас продукта
     *
     * @param  string  $value
     * @return void
     */
    public function setAliasAttribute($value)
    {
        $this->attributes['alias'] = sanitize_title_with_dashes($value);
    }

    public function hasPaymentSystem($paymentSystem)
    {
        return $this->paymentSystems->pluck('name')->contains($paymentSystem);
    }
}
