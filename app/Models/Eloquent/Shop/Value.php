<?php

namespace App\Models\Shop;

use App\Models\Shop\Translate\Value as ValueTranslate;
use App\Models\Shop\Belongs\PropertiesValues;

/**
 * Value
 * @package App\Models\Shop
 */
class Value extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_values';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(ValueTranslate::class, 'value_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function properties()
    {
        return $this->belongsToMany(Property::class, PropertiesValues::getTableName(), 'value_id', 'property_id');
    }

    /**
     *  Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id ?? 0;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias ?? '';
    }
}
