<?php

namespace App\Models\Shop;

/**
 * Cart Products
 * @package App\Models\Shop
 */
class CartProducts extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_cart_products';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
