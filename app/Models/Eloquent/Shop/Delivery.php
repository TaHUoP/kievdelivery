<?php

namespace App\Models\Shop;

/**
 * Delivery
 * @package App\Models\Shop
 */
class Delivery extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_checkout_delivery';

    /**
     * @var array
     */
    protected $dates = ['date', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function checkout()
    {
        return $this->hasOne(Checkout::class, 'checkout_id', 'id');
    }
}