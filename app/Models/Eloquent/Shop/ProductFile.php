<?php

namespace App\Models\Shop;

use App\Models\Model;
use App\Models\Shop\Product;

/**
 * Product Files
 * @package App\Models\Shop
 */
class ProductFile extends Model
{
    /**
     * @var string
     */
    protected $table = 'shop_product_files';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
