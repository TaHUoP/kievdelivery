<?php

namespace App\Models\Shop;

use App\Models\Model;

/**
 * Coupon
 * @package App\Models\Shop
 */
class Coupon extends Model
{
    /**
     * @var string
     */
    protected $table = 'shop_coupons';

    /**
     * Сортировка desc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка asc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }
}
