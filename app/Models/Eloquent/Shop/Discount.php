<?php

namespace App\Models\Shop;

/**
 * Delivery
 * @package App\Models\Shop
 */
class Discount extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'discounts_by_date';

    protected $fillable = [
        'percents',
        'date_from',
        'date_to',
    ];

    public function getAbsoluteDiscount()
    {
        return 1 - $this->percents/100;
    }
}