<?php

namespace App\Models\Shop;

use App\Models\Shop\Translate\Property as PropertyTranslate;
use App\Models\Shop\Belongs\PropertiesValues;

/**
 * Property
 * @package App\Models\Shop
 */
class Property extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_properties';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function values()
    {
        return $this->belongsToMany(Value::class, PropertiesValues::getTableName(), 'property_id', 'value_id')
            ->withPivot('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(PropertyTranslate::class, 'property_id', 'id');
    }

    /**
     *  Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }

    /**
     * Scope is filter
     *
     * @param $query
     * @param int $scope
     * @return mixed
     */
    public function scopeIsFilter($query, $scope = 1)
    {
        return $query->where('is_filter', $scope);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id ?? 0;
    }
}
