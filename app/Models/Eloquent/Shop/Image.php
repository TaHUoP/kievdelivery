<?php

namespace App\Models\Shop;

use App\Models\Shop\Translate\Image as ImageTranslate;

/**
 * Product Image
 * @package App\Models\Shop
 */
class Image extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_product_images';

    /**
     * Сортировка desc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('order', 'desc');
    }

    /**
     * Сортировка asc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('order', 'asc');
    }

    /**
     * Правильная сортировка картинок
     * 
     * @param $query
     * @return mixed
     */
    public function scopeOrderAscAndFirstDefault($query)
    {
        return $query->orderByRaw('`default` DESC, `order` ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(ImageTranslate::class, 'image_id', 'id');
    }

    /**
     * Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations' => function($query) {
            $query->with('language');
        }]);
    }

    /**
     * Все данные по изображению
     *
     * @param $query
     * @param array $relations
     * @return mixed
     */
    public function scopeTotal($query, array $relations = ['product'])
    {
        $with = ['translations' => function($query) {
            $query->with('language');
        }];

        if (in_array('product', $relations)) {
            $with = array_merge($with, ['product']);
        }

        return $query->with($with);
    }
}
