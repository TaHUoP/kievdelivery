<?php

namespace App\Models\Shop\Translate;

use Waavi\Translation\Models\Language;

/**
 * Value Translate
 * @package App\Models\Shop\Translate
 */
class Value extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_values_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}
