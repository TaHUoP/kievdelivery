<?php

namespace App\Models\Shop\Translate;

use Waavi\Translation\Models\Language;

/**
 * Category Translate
 * @package App\Models\Shop\Translate
 */
class Category extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_categories_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? '';
    }
}
