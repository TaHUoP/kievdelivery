<?php

namespace App\Models\Shop\Translate;

use Waavi\Translation\Models\Language;

/**
 * Image Translate
 * @package App\Models\Shop\Translate
 */
class Image extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_product_images_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}
