<?php

namespace App\Models\Shop;

use App\Models\Shop\Translate\Category as CategoryTranslate;
use App\Models\Shop\Belongs\CategoriesProperties;
use Kalnoy\Nestedset\NodeTrait;
use App\Models\Shop\Property;

/**
 * Category
 * @package App\Models\Shop
 */
class Category extends \App\Models\Model
{
    use NodeTrait;

    const SALE_ALIAS = 'sale';
    const KYIV_ONLY_ALIAS = 'kyiv-only';

    /**
     * @var string
     */
    protected $table = 'shop_categories';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function properties()
    {
        return $this->belongsToMany(Property::class, CategoriesProperties::getTableName(), 'category_id', 'property_id')
            ->withPivot('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function filterProperties()
    {
        return $this->belongsToMany(Property::class, CategoriesProperties::getTableName(), 'category_id', 'property_id')
            ->withPivot('order')->where('is_filter', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(CategoryTranslate::class, 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id')->with(['translations']);
    }

    /**
     *  Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }

    /**
     * Scope visible
     *
     * @param $query
     * @param int $scope
     * @return mixed
     */
    public function scopeVisible($query, $scope = 1)
    {
        return $query->where('visible', $scope);
    }

    /**
     * Все данные по категории
     *
     * @param $query
     * @return mixed
     */
    public function scopeTotal($query)
    {
        return $query->with([
            'translations',
            'parent' => function ($query) {
                $query->with(['translations']);
            },
            'properties' => function ($query) {
                $query->orderBy('order', 'asc');
                $query->with('translations');
                $query->with(['values' => function($query) {
                    $query->orderBy('order', 'asc');
                    $query->with('translations');
                }]);
            }
        ]);
    }

}
