<?php

namespace App\Models\Shop\Belongs;

/**
 * Categories Properties
 * @package App\Models\Shop
 */
class CategoriesProperties extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_categories_properties';

    /**
     * @return int
     */
    public function getPropertyId(): int
    {
        return $this->property_id ?? 0;
    }
}
