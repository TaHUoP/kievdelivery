<?php

namespace App\Models\Shop\Belongs;

/**
 * Product Categories
 * @package App\Models\Shop
 */
class ProductCategories extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_product_categories';

    /**
     * @var bool
     */
    public $timestamps = false;
}
