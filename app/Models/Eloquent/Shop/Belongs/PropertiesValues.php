<?php

namespace App\Models\Shop\Belongs;

/**
 * Properties Values
 * @package App\Models\Shop\Belongs
 */
class PropertiesValues extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'shop_properties_values';

    /**
     * @var bool
     */
    public $timestamps = false;
}
