<?php

namespace App\Models\Shop;

/**
 * Checkout
 * @package App\Models\Shop
 */
class Checkout extends \App\Models\Model
{
    const STATUS_PENDING = -1;
    const STATUS_SUCCESS = 1;
    const STATUS_FAIL = 0;

    /**
     * @var string
     */
    protected $table = 'shop_checkout';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cart()
    {
        return $this->hasOne(Cart::class, 'id', 'cart_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function delivery()
    {
        return $this->hasOne(Delivery::class, 'checkout_id', 'id');
    }

    /**
     * Все данные по заказу
     *
     * @param $query
     * @return mixed
     */
    public function scopeTotal($query)
    {
        return $query->with(['cart' => function($query) {
            $query->with(['products' => function($query) {
                $query->with(['product' => function($query) {
                    $query->translates();
                }]);
            }]);
        }, 'delivery']);
    }

    /**
     * Сортировка desc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка asc
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Scope is paid
     *
     * @param $query
     * @param int $scope
     * @return mixed
     */
    public function scopePaid($query, $scope = 1)
    {
        return $query->where('paid', $scope);
    }

    /**
     * Scope is status
     *
     * @param $query
     * @param int $scope
     * @return mixed
     */
    public function scopeStatus($query, $scope = 1)
    {
        return $query->where('status', $scope);
    }

    /**
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_PENDING => trans('shop.checkouts.status.pending'),
            self::STATUS_SUCCESS => trans('shop.checkouts.status.success'),
            self::STATUS_FAIL => trans('shop.checkouts.status.fail'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusNameAttribute()
    {
        return self::getStatusArray()[$this->status];
    }

    /**
     * @return string
     */
    public function getTotalAttribute()
    {
        $cart = $this->cart ?: new Cart();
        $delivery = $this->delivery ?: new Delivery();

        return $cart->total_price + $delivery->delivery_price - $cart->discount;
    }

    protected function setPaidAttribute($value)
    {
        $this->attributes['paid'] = $value;
        $this->cart()->update(['processed' => $value]);
    }
}
