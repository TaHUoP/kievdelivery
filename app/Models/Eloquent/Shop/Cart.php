<?php

namespace App\Models\Shop;

use App\Models\User\User;

/**
 * Cart
 * @package App\Models\Shop
 */
class Cart extends \App\Models\Model
{
    const COOKIE_MINUTES_EXPIRES = 30;

    /**
     * @var string
     */
    protected $table = 'shop_cart';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function checkout()
    {
        return $this->hasOne(Checkout::class, 'cart_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(CartProducts::class, 'cart_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function productModels()
    {
        return $this->belongsToMany(Product::class, 'shop_cart_products', 'cart_id', 'product_id');
    }

    /**
     * Все данные карзины
     *
     * @param $query
     * @return mixed
     */
    public function scopeTotal($query)
    {
        $query->with(['products' => function ($query) {
            $query->with(['product'  => function ($query) {
                $query->translates();
            }]);
        }]);
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeProcessed($query, $state = 1)
    {
        return $query->where('processed', $state);
    }

    public function isPaymentSystemAllowed($paymentSystem)
    {
        foreach ($this->productModels as $product) {
            if (! $product->hasPaymentSystem($paymentSystem)) {
                return false;
            }
        }

        return true;
    }
}
