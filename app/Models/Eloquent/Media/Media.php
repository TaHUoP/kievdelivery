<?php

namespace App\Models\Media;

/**
 * Media
 * @package App\Models\Media
 */
class Media extends \App\Models\Model
{
    protected $table = 'media_files';

    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(\App\Models\Media\Translate\Media::class, 'media_file_id', 'id');
    }

    /**
     * Видимость
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeVisible($query, $state = 1)
    {
        return $query->orderBy('visible', $state);
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations' => function($query) {
            $query->with('language');
        }]);
    }

    /**
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            self::TYPE_IMAGE => trans('media.type.image'),
            self::TYPE_VIDEO => trans('media.type.video'),
        ];
    }
}
