<?php

namespace App\Models\Media\Translate;

use Waavi\Translation\Models\Language;

/**
 * Media
 * @package App\Models\Media\Translate
 */
class Media extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'media_files_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}