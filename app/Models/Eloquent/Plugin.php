<?php

namespace App\Models;

/**
 * Plugin
 * @package App\Models
 */
class Plugin extends \App\Models\Model
{
    protected $table = 'plugins';

    const TYPE_SLIDER = 'slider';
    const TYPE_TEXT = 'text';

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Подтверждение
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeVisible($query, $state = 1)
    {
        return $query->where('visible', $state);
    }

    /**
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            self::TYPE_SLIDER => trans('plugins.slider'),
            self::TYPE_TEXT => trans('plugins.text'),
        ];
    }
}
