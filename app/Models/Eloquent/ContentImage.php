<?php

namespace App\Models;

/**
 * Content Image
 * @package App\Models
 */
class ContentImage extends \App\Models\Model
{
    protected $table = 'contents_images';

    const IMAGE_PATH = '/upload/media_content/images/';

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function content()
    {
        return $this->belongsTo(Content::class, 'id', 'content_id');
    }
}
