<?php

namespace App\Models;

/**
 * Seo specification
 * @package App\Models
 */
class SeoSpecification extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'seo_specifications';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(\App\Models\Translate\SeoSpecification::class, 'seo_id', 'id');
    }
}
