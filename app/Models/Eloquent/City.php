<?php

namespace App\Models;

class City extends \Illuminate\Database\Eloquent\Model
{
    const DEFAULT_CITY = 'kyiv';

    protected $table = 'cities';

    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }

    public function translations()
    {
        return $this->hasMany(\App\Models\Translate\Cities::class, 'city_id', 'id');
    }

    public function scopeOrderDesc($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function scopeOrderAsc($query)
    {
        return $query->orderBy('id', 'asc');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function price_type()
    {
        return $this->belongsTo(PriceType::class);
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias ?? "";
    }
}
