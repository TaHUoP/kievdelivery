<?php

namespace App\Models;

use App;

/**
 * Countries
 * @package App\Models
 */
class Countries extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'countries';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(\App\Models\Translate\Countries::class, 'countries_id', 'id');
    }

    /**
     * Видимость
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeVisible($query, $state = 1)
    {
        return $query->where('visible', $state);
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('priority', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('priority', 'asc');
    }

    /**
     * Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }
}
