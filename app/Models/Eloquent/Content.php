<?php

namespace App\Models;

/**
 * Content
 * @package App\Models
 */
class Content extends \App\Models\Model
{
    protected $table = 'contents';

    const TYPE_PAGE = 'page';
    const TYPE_NEWS = 'news';
    const TYPE_UKRAINE = 'ukraine';
    const TYPE_INFORMATION = 'information';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(\App\Models\Translate\Content::class, 'content_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(\App\Models\ContentImage::class, 'content_id', 'id');
    }

    /**
     * Видимость
     *
     * @param $query
     * @param int $state
     * @return mixed
     */
    public function scopeVisible($query, $state = 1)
    {
        return $query->where('visible', $state);
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Переводы
     *
     * @param $query
     * @return mixed
     */
    public function scopeTranslates($query)
    {
        return $query->with(['translations' => function($query) {
            $query->with('language');
        }]);
    }

    /**
     * Все данные по контенту
     * TODO при передачи $query без use не скоупится translates
     *
     * @param $query
     * @param array $relations
     * @return mixed
     */
    public function scopeTotal($query, array $relations = ['images'])
    {
        $with = ['translations'];
        if (in_array('images', $relations)) {
            $with = array_merge($with, ['images' => function () use ($query) {
                $query->translates();
            }]);
        }

        return $query->with($with);
    }

    /**
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            self::TYPE_PAGE => trans('contents.type.page'),
            self::TYPE_NEWS => trans('contents.type.news'),
            self::TYPE_UKRAINE => trans('contents.type.ukraine'),
        ];
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        if ($this->type == self::TYPE_NEWS) {
            return route('news.show', ['url' => $this->url]);
        } elseif ($this->type == self::TYPE_UKRAINE) {
            return route('site.ukraine-page', ['url' => $this->url]);
        }

        return route('site.page', ['url' => $this->url]);
    }

    /**
     * Возвращает default image
     *
     * @return array
     */
    public function getDefaultImage()
    {
        foreach ($this->images as $image) {
            if ($image->default) {
                return $image;
            }
        }
        return [];
    }
}
