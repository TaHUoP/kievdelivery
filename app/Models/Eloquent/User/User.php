<?php

namespace App\Models\User;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * User
 * @package App\Models\User
 */
class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * User statuses
     * - active
     * - inactive
     * - delete
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE = -1;

    /**
     * Access to cpanel
     * - true
     * - false
     */
    const AUTHORIZED_TO_CPANEL = 1;
    const UNATHORIZED_TO_CPANEL = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'firstname', 'phone'
    ];

    protected $casts = [
        'status' => 'integer'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }

    /**
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_ACTIVE => trans('users.status_active'),
            self::STATUS_INACTIVE => trans('users.status_inactive'),
            self::STATUS_DELETE => trans('users.status_delete'),
        ];
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Общее
     *
     * @param $query
     * @return mixed
     */
    public function scopeTotal($query)
    {
        return $query->with('profile');
    }

    /**
     * Доступ в админ панель
     *
     * @return bool
     */
    public function isAuthorizedForCpanel()
    {
        return $this->access_cpanel == self::AUTHORIZED_TO_CPANEL;
    }

    public function delete()
    {
        $this->update(['status' => self::STATUS_INACTIVE]);

        return parent::delete();
    }

    public function getNameAttribute()
    {
        return $this->profile ? $this->profile->name : '';
    }

    public function getSurnameAttribute()
    {
        return $this->profile ? $this->profile->surname : '';
    }
}
