<?php

namespace App\Models\User;

use Statics;
use Config;

/**
 * Profile
 * @package App\Models\User
 */
class Profile extends \App\Models\Model
{
    const GOOGLE_PROVIDER = 'google';
    const FACEBOOK_PROVIDER = 'facebook';

    /**
     * @var string
     */
    protected $table = 'users_profile';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'avatar_url',
        'provider',
        'profile_id',
        'token',
        'email',
        'profile_url',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return string
     */
    public function avatarUrl()
    {
        if (!$this->avatar_url) {
            return cdn('default/images/placeholder.jpg');
        }

        return DIRECTORY_SEPARATOR . Config::get('app.avatars_folder') . DIRECTORY_SEPARATOR . $this->avatar_url;
    }

    /**
     * @return string
     */
    public function avatarThumbUrl()
    {
        if (!$this->avatar_url) {
            return cdn('default/images/placeholder.jpg');
        }

        return DIRECTORY_SEPARATOR . Config::get('app.avatars_folder') . DIRECTORY_SEPARATOR .  'thumbs' . DIRECTORY_SEPARATOR . $this->avatar_url;
    }
}
