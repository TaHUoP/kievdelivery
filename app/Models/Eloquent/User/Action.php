<?php

namespace App\Models\User;

/**
 * Action
 * @package App\Models\User
 */
class Action extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'users_actions';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Сортировка DESC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderDesc($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Сортировка ASC
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrderAsc($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * Профиль
     *
     * @param $query
     * @return mixed
     */
    public function scopeUser($query)
    {
        return $query->with(['profile' => function($query) {
            $query->with('profile');
        }]);
    }
}
