<?php

namespace App\Models\Translate;

use Waavi\Translation\Models\Language;

/**
 * Regions
 * @package App\Models
 */
class Regions extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'regions_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}
