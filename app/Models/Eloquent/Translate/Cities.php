<?php

namespace App\Models\Translate;

use Waavi\Translation\Models\Language;

/**
 * Cities
 * @package App\Models
 */
class Cities extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'cities_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }

    /**
     * @return string
     */
    public function getNameDeclension(): string
    {
        return $this->name_declension ?? '';
    }
}
