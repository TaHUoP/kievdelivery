<?php

namespace App\Models\Translate;

use Waavi\Translation\Models\Language;

/**
 * Content Translate
 * @package App\Models\Translate
 */
class Content extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'contents_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}
