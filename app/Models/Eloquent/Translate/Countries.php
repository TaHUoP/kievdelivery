<?php

namespace App\Models\Translate;

use Waavi\Translation\Models\Language;

/**
 * Countries
 * @package App\Models
 */
class Countries extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'countries_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}
