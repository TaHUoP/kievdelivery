<?php

namespace App\Models\Translate;

use Waavi\Translation\Models\Language;

/**
 * Content Translate
 * @package App\Models\Translate
 */
class SeoSpecification extends \App\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'seo_specifications_translate';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lang_id');
    }
}
