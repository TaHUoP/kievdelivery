<?php

namespace App\Models;

class Region extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'regions';

    public function scopeTranslates($query)
    {
        return $query->with(['translations']);
    }

    public function scopeOrderDesc($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function scopeOrderAsc($query)
    {
        return $query->orderBy('id', 'asc');
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function translations()
    {
        return $this->hasMany(\App\Models\Translate\Regions::class, 'region_id', 'id');
    }
}
