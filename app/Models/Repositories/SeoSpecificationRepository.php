<?php

namespace App\Repositories;

use Api\App\Contracts\Entities\SeoSpecificationRepository as SeoSpecificationInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Translate\SeoSpecification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Seo Specification Repository
 * @package App\Repositories
 */
class SeoSpecificationRepository implements SeoSpecificationInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $seoSpecification
     */
    public function __construct(Model $seoSpecification)
    {
        $this->model = $seoSpecification;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);
        $query->with('translations');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();
        $query->with('translations');

        if ($condition->getCondition('url')) {
            $this->queryCondition($query, $condition, 'url');
        }

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();
        $query->with('translations');

        return $this->queryAll($query, $condition);
    }

    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'url');
        $model->save();

        if (isset($data['translations'])) {

            $existingLanguages = [];

            foreach ($data['translations'] as $lang => $dataTranslation) {
                foreach ($model->translations as $translation) {
                    if ($translation->language->locale === $lang) {
                        if ($dataTranslation) {
                            $existingLanguages[$lang] = true;
                            $this->setAttribute($translation, $dataTranslation, 'title');
                            $this->setAttribute($translation, $dataTranslation, 'meta_title');
                            $this->setAttribute($translation, $dataTranslation, 'meta_description');
                            $this->setAttribute($translation, $dataTranslation, 'meta_keywords');
                            $translation->save();
                        } else {
                            // Если язык пустой, удалить его
                            $translation->delete();
                        }
                        break;
                    }
                }
            }

            // Создание новых переводов
            foreach ($data['translations'] as $lang => $dataTranslation) {
                if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                    continue;
                }
                $newTranslation = new SeoSpecification();
                $newTranslation->seo_id = $model->id;
                $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
                $this->setAttribute($newTranslation, $dataTranslation, 'title');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_title');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_description');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_keywords');
                $newTranslation->save();
            }
        }
        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->delete();
    }
}