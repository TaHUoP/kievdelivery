<?php

namespace App\Repositories;

use Api\App\Contracts\Entities\ContentRepository as ContentInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Content Repository
 * @package App\Repositories
 */
class ContentRepository implements ContentInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Model
     */
    protected $translation;

    /**
     * @param Model $content
     * @param Model $translation
     */
    public function __construct(Model $content, Model $translation)
    {
        $this->model = $content;
        $this->translation = $translation;
    }

    /**
     * @inheritdoc
     */
    public function getTypePage()
    {
        $model = $this->model;
        return $model::TYPE_PAGE;
    }

    /**
     * @inheritdoc
     */
    public function getTypeUkraine()
    {
        $model = $this->model;
        return $model::TYPE_UKRAINE;
    }

    /**
     * @inheritdoc
     */
    public function getTypeNews()
    {
        $model = $this->model;
        return $model::TYPE_NEWS;
    }

    /**
     * @inheritdoc
     */
    public function getTypeInformation()
    {
        $model = $this->model;
        return $model::TYPE_INFORMATION;
    }

    /**
     * @inheritdoc
     */
    public function getTypesArray()
    {
        return $this->model->getTypesArray();
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::translates();
        $query->where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'url');
        $this->queryCondition($query, $condition, 'type');

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total();

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        // Поиск
        if ($condition->getCondition('search')) {
            $query->whereHas('translations', function ($query) use ($condition) {
                $query->orWhere('title', 'like', '%' . $condition->getCondition('search')['text'] . '%');
                $query->orWhere('text', 'like', '%' . $condition->getCondition('search')['text'] . '%');
                $query->orWhere('info', 'like', '%' . $condition->getCondition('search')['text'] . '%');
            });
        }

        $this->queryCondition($query, $condition, 'type');

        $query->orderDesc();

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'type');
        $this->setAttribute($model, $data, 'url');
        $this->setAttribute($model, $data, 'visible');
        $model->save();

        $existingLanguages = [];
        foreach ($data['translations'] as $lang => $dataTranslation) {
            foreach ($model->translations as $translation) {
                if ($translation->language->locale === $lang) {
                    if ($dataTranslation) {
                        $existingLanguages[$lang] = true;
                        $this->setAttribute($translation, $dataTranslation, 'meta_title');
                        $this->setAttribute($translation, $dataTranslation, 'meta_description');
                        $this->setAttribute($translation, $dataTranslation, 'meta_keywords');
                        $this->setAttribute($translation, $dataTranslation, 'text');
                        $this->setAttribute($translation, $dataTranslation, 'info');
                        $this->setAttribute($translation, $dataTranslation, 'title');
                        $translation->save();
                    } else {
                        // Если язык пустой, удалить его
                        $translation->delete();
                    }
                    break;
                }
            }
        }

        // Создание новых переводов
        foreach ($data['translations'] as $lang => $dataTranslation) {
            if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                continue;
            }
            $newTranslation = clone $this->translation;
            $newTranslation->content_id = $model->id;
            $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
            $this->setAttribute($newTranslation, $dataTranslation, 'meta_title');
            $this->setAttribute($newTranslation, $dataTranslation, 'meta_description');
            $this->setAttribute($newTranslation, $dataTranslation, 'meta_keywords');
            $this->setAttribute($newTranslation, $dataTranslation, 'text');
            $this->setAttribute($newTranslation, $dataTranslation, 'info');
            $this->setAttribute($newTranslation, $dataTranslation, 'title');
            $newTranslation->save();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }

}