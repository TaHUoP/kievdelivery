<?php

namespace App\Repositories;

use Api\App\Contracts\Entities\ContentImageRepository as ContentImageInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Media Repository
 * @package App\Repositories\ContentImage
 */
class ContentImageRepository implements ContentImageInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $content
     */
    public function __construct(Model $content)
    {
        $this->model = $content;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total();

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'url');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::orderDesc();

        return $this->queryAll($query, $condition);
    }

    /**
     * Получаем путь сохранения для изображений
     *
     * @return string
     */
    public function getImagePath()
    {
        $model = $this->model;
        return $model::IMAGE_PATH;
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        } else {
            if (isset($data['default']) && $data['default'] && $data['default'] != $model->default) {
                $model::where(['content_id' => $model->content_id])->update(['default' => 0]);
            }
        }

        $this->setAttribute($model, $data, 'preview_url');
        $this->setAttribute($model, $data, 'review_url');
        $this->setAttribute($model, $data, 'default');
        $this->setAttribute($model, $data, 'order');
        $this->setAttribute($model, $data, 'content_id');
        $model->save();

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }

}