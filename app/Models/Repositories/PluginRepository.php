<?php 

namespace App\Repositories;

use Api\App\Contracts\Entities\PluginRepository as PluginInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;
use App\Models\Plugin;

/**
 * Plugin Repository
 * @package App\Repositories
 */
class PluginRepository implements PluginInterface
{
    use QueryTrait;

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = new Plugin();
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findByKey($key, ConditionInterface $condition = null)
    {
        $model = new Plugin();
        $query = $model::where(['key' => $key]);

        if (key_exists('visible', $condition->getScopes())) {
            $query->visible();
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = new Plugin();
        $query = $model->newQuery();

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'key');

        if (key_exists('visible', $condition->getScopes())) {
            $query->visible();
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = new Plugin();
        $query = $model::orderBy('id', SORT_DESC);

        if ($condition->getOrderBy('id')) {
            $query->orderBy('id', $condition->getOrderBy('id'));
        }

        return $this->queryAll($query, $condition);
    }

    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = new Plugin();
        }

        $this->setAttribute($model, $data, 'key');
        $this->setAttribute($model, $data, 'type');
        $this->setAttribute($model, $data, 'visible');
        $this->setAttribute($model, $data, 'description');
        $this->setAttribute($model, $data, 'value');

        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->delete();
    }
}