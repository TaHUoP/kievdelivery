<?php 

namespace App\Repositories\User;

use Api\User\Contracts\Entities\UserRepository as UserInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\User\Profile;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;
use App\Models\User\User;
use Api\App\Storage\Condition;

/**
 * User Repository
 * @package App\Repositories\User
 */
class UserRepository implements UserInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Model
     */
    protected $profileModel;

    /**
     * @param Model $user
     * @param Model $profile
     */
    public function __construct(Model $user, Model $profile)
    {
        $this->model = $user;
        $this->profileModel = $profile;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'email');
        $this->queryCondition($query, $condition, 'phone');
        $this->queryCondition($query, $condition, 'confirmation_code');

        if($condition->getWithTrashed())
            $query = $query->withTrashed();

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        if($condition->getWithTrashed())
            $query = $query->withTrashed();

        return $this->queryAll($query, $condition);
    }



    # -----------------------------------------------------------

    // TODO: вынести в интерфейс
    
    /**
     * Возвращает пользователя по имейлу
     *
     * @param $email
     * @return Model|mixed|null
     * @throws \ErrorException
     */
    public function findByEmail($email)
    {
        $condition = new Condition();

        $condition->addCondition('email', 'email', $email);

        return $this->findOne($condition);
    }

    /**
     * Возвращает пользователя по коду подтверждения
     *
     * @param $confirmation_code
     * @return Model|mixed|null
     * @throws \ErrorException
     */
    public function findByConfirmationCode($confirmation_code)
    {
        $condition = new Condition();

        $condition->addCondition('confirmation_code', 'confirmation_code', $confirmation_code);

        return $this->findOne($condition);
    }

    /**
     * Возвращает пользователя по телефону
     *
     * @param $phone
     * @return Model|mixed|null
     * @throws \ErrorException
     */
    public function findByPhone($phone)
    {
        $condition = new Condition();

        $condition->addCondition('phone', 'phone', $phone);

        return $this->findOne($condition);
    }

    /**
     * @param array $attributes
     * @param Model|NULL $model
     * @return bool
     */
    public function save(array $attributes, Model $model = null)
    {
        if ($model === null) {
            $model = $this->model;
        }

        $this->setAttribute($model, $attributes, 'email');
        $this->setAttribute($model, $attributes, 'password');
        $this->setAttribute($model, $attributes, 'phone');
        $this->setAttribute($model, $attributes, 'access_cpanel');
        $this->setAttribute($model, $attributes, 'status');

        if (array_key_exists('confirmation_code', $attributes)) {
            $model->confirmation_code = $attributes['confirmation_code'];
        }

        $model->save();

        if (!isset($model->profile)) {
            $profile = $this->profileModel;
            $isNewProfile = true;
        } else {
            $profile = $model->profile;
        }

        $this->setAttribute($profile, $attributes, 'name');
        $this->setAttribute($profile, $attributes, 'surname');
        $this->setAttribute($profile, $attributes, 'avatar_url');

        if (isset($isNewProfile)) {
            $model->profile()->save($profile);
        } else {
            $model->profile->save();
        }

        return true;
    }
}