<?php 

namespace App\Repositories\User;

use Api\User\Contracts\Entities\ProfileRepository as ProfileInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Profile Repository
 * @package App\Repositories\User
 */
class ProfileRepository implements ProfileInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $profile
     */
    public function __construct(Model $profile)
    {
        $this->model = $profile;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findByUserId($user_id)
    {
        $model = $this->model;
        $query = $model::where(['user_id' => $user_id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        return $this->queryAll($query, $condition);
    }



    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @param array $attributes
     * @param Model|NULL $model
     * @return bool
     */
    public function save(array $attributes, Model $model = NULL)
    {
        if ($model === NULL) {
            $model = $this->model;
        }

        $model->fill($attributes);
        return $model->save();
    }
}