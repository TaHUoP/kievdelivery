<?php 

namespace App\Repositories\User;

use Api\User\Contracts\Entities\ActionRepository as ActionInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Action Repository
 * @package App\Repositories\User
 */
class ActionRepository implements ActionInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $action
     */
    public function __construct(Model $action)
    {
        $this->model = $action;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        return $this->queryAll($query, $condition);
    }
}