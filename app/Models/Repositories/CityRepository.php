<?php

namespace App\Repositories;

use Api\App\Contracts\Entities\CityRepository as CityInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Translate\Cities;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;
use Waavi\Translation\Models\Language;

/**
 * Content Repository
 * @package App\Repositories
 */
class CityRepository implements CityInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Model
     */
    protected $translation;

    /**
     * @param Model $city
     * @param Model $translation
     */
    public function __construct(Model $city, Model $translation)
    {
        $this->model = $city;
        $this->translation = $translation;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::translates();
        $query->where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $query->orderDesc();

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'price_type_id');
        $this->setAttribute($model, $data, 'region_id');
        $this->setAttribute($model, $data, 'alias');
        $this->setAttribute($model, $data, 'is_region_center');
        $model->save();

        $existingLanguages = [];
        foreach ($data['translations'] as $lang => $dataTranslation) {
            foreach ($model->translations as $translation) {
                if ($translation->language->locale === $lang) {
                    if ($dataTranslation) {
                        $existingLanguages[$lang] = true;
                        $this->setAttribute($translation, $dataTranslation, 'name');
                        $this->setAttribute($translation, $dataTranslation, 'name_declension');
                        $this->setAttribute($translation, $dataTranslation, 'meta_title');
                        $this->setAttribute($translation, $dataTranslation, 'meta_description');
                        $this->setAttribute($translation, $dataTranslation, 'meta_keywords');
                        $this->setAttribute($translation, $dataTranslation, 'text');
                        $this->setAttribute($translation, $dataTranslation, 'key_words');
                        $translation->save();
                    } else {
                        // Если язык пустой, удалить его
                        $translation->delete();
                    }
                    break;
                }
            }
        }

        // Создание новых переводов
        foreach ($data['translations'] as $lang => $dataTranslation) {
            if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                continue;
            }
            $newTranslation = new Cities();
            $language = Language::where('locale', $lang)->first();
            if ($language->active) {
                $newTranslation->city_id = $model->id;
                $newTranslation->lang_id = $language->id;
                $this->setAttribute($newTranslation, $dataTranslation, 'name');
                $this->setAttribute($newTranslation, $dataTranslation, 'name_declension');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_title');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_description');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_keywords');
                $this->setAttribute($newTranslation, $dataTranslation, 'text');
                $this->setAttribute($newTranslation, $dataTranslation, 'key_words');
                $newTranslation->save();
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }

}