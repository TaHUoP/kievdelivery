<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\ProductRepository as ProductInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Shop\Translate\Product as ProductTranslate;
use App\Models\Shop\Belongs\ProductCategories;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shop\ProductRelation;
use App\Models\Traits\QueryTrait;
use DB;

/**
 * Product Repository
 * @package App\Repositories\Shop
 */
class ProductRepository implements ProductInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $product
     */
    public function __construct(Model $product)
    {
        $this->model = $product;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::total();
        $query->where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;

        $query = $model::total($condition->getRelations() ?: ['images', 'categories', 'properties']);
        $this->queryCondition($query, $condition, 'vendor_code');
        $this->queryCondition($query, $condition, 'alias');
        $this->queryCondition($query, $condition, 'id');

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total($condition->getRelations() ? $condition->getRelations() : []);

        // Поиск по ids
        if ($condition->getCondition('ids')) {
            $this->queryCondition($query, $condition, 'ids');
        }

        // Поиск по категории
        if ($condition->getCondition('category_id')) {
            $query->whereHas('categories', function ($query) use ($condition) {
                if (isset($condition->getCondition('category_id')['id'])) {
                    $query->where([ProductCategories::getTableName() . '.category_id' => $condition->getCondition('category_id')['id']]);
                }
                if (isset($condition->getCondition('category_id')['alias'])) {
                    $query->where(['alias' => $condition->getCondition('category_id')['alias']]);
                }
                if (isset($condition->getCondition('category_id')['name'])) {
                    $query->whereHas('translations', function ($query) use ($condition) {
                        $query->where('name', 'like', '%' . $condition->getCondition('category_id')['name'] . '%');
                    });
                }
            });
        }

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        // Сортировка
        if (key_exists('price', $condition->getOrderBy())) {
            $query->orderBy('price', $condition->getOrderBy()['price'] === SORT_DESC ? 'desc' : 'asc');
        } else {
            $query->orderBy('created_at', 'desc');
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function findRecommended(ConditionInterface $condition, $recommendedCats = false)
    {
        $model = $this->model;
        $query = $model::total($condition->getRelations() ?: ['images', 'categories', 'properties']);
        $query->inRandomOrder();

        //Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        // Получить рекомендованные
        $query->whereHas('categories', function ($query) use ($condition, $recommendedCats) {
            $query->where('visible', 1);
            // получить рекомендуемые если если они есть
            if ($recommendedCats){
                $query->where('recommended', 1);
            }
        });

        //какой-то выборка
        if ($condition->getCondition('except')) {
            $this->queryCondition($query, $condition, 'except');
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function findNew(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total($condition->getRelations() ?: ['images', 'categories', 'properties']);
        $query->orderDesc();

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function findByPropertiesFilter($properties, array $filter, ConditionInterface $condition = null)
    {
        if (!$properties instanceof Collection) {
            throw new \ErrorException('Argument "$properties" must be instanceof Collection class');
        }

        $model = $this->model;
        /** @var \Illuminate\Database\Eloquent\Builder $query */
        $query = $model::total();

        // Поиск по категории
        if ($condition->getCondition('category_id')) {
            $query->whereHas('categories', function ($query) use ($condition) {
                if (isset($condition->getCondition('category_id')['id'])) {
                    $query->where([ProductCategories::getTableName() . '.category_id' => $condition->getCondition('category_id')['id']]);
                }
                if (isset($condition->getCondition('products_ids')['products_ids'])) {
                    $query->whereIn(ProductCategories::getTableName() . '.product_id' , $condition->getCondition('products_ids')['products_ids']);
                }

                if (isset($condition->getCondition('category_id')['alias'])) {
                    $query->where(['alias' => $condition->getCondition('category_id')['alias']]);
                }
                if (isset($condition->getCondition('category_id')['name'])) {
                    $query->whereHas('translations', function ($query) use ($condition) {
                        $query->where('name', 'like', '%' . $condition->getCondition('category_id')['name'] . '%');
                    });
                }
            });
        }

        // Поиск по названию товара
        if ($condition->getCondition('product_name')) {
            $query->orWhereHas('translations', function ($query) use ($condition) {
                $name = $condition->getCondition('product_name')['name'];
                $query->orWhere('name', 'like', '%' . $name . '%');
            });
        }

        // Поиск по артикулу
        if ($condition->getCondition('vendor_code')) {
            $vendorCode = $condition->getCondition('vendor_code')['vendor_code'];
            $query->orWhere('vendor_code', $vendorCode);
        }

        // Поиск по описанию description_short
        if ($condition->getCondition('description_short')) {
            $query->orWhereHas('translations', function ($query) use ($condition) {
                $name = $condition->getCondition('description_short')['desc'];
                $query->orWhere('description_short', 'like', '%' . $name . '%');
            });
        }

        // Поиск по описанию description_full
        if ($condition->getCondition('description_full')) {
            $query->orWhereHas('translations', function ($query) use ($condition) {
                $name = $condition->getCondition('description_full')['desc'];
                $query->orWhere('description_full', 'like', '%' . $name . '%');
            });
        }

        // Построение запроса по фильтру
        foreach ($properties as $property) {

            $values = isset($filter[$property->alias]) ? $filter[$property->alias] : [];

            if (empty($values)) {
                continue;
            }

            if ($property->type == 'range') {

                // TODO: ранее использовалось для поиска в диапазоне кастомных значений у свойства.

                // Поиск по свойству с диапазоном
                /*$query->whereHas('relations', function ($query) use ($property, $values) {
                    $query->where('property_id', $property->id);
                    $value = isset($values[0]) ? $values[0] : null;
                    if ($value) {
                        $value = explode('-', $value);
                        $from = isset($value[0]) ? (float)$value[0] : null;
                        $to = isset($value[1]) ? (float)$value[1] : null;
                        $query->where(function ($query) use ($from, $to) {
                            if (!is_null($from)) {
                                $query->where(DB::raw('convert(custom_value, unsigned)'), '>=', $from);
                            }
                            if (!is_null($to)) {
                                $query->where(DB::raw('convert(custom_value, unsigned)'), '<=', $to);
                            }
                        });
                    }
                });*/

            } else {

                // Поиск по свойству с единым значением
                $query->whereHas('relations', function ($query) use ($property, $values) {
                    $query->where('property_id', $property->id);
                    $query->whereHas('value', function ($query) use ($values) {
                        $query->where(function ($query) use ($values) {
                            foreach ($values as $value) {
                                $query->orWhere('alias', $value);
                            }
                        });

                    });
                });

            }

        }

        // Диапазон цены
        if (key_exists('price', $filter)) {
            $value = isset($filter['price'][0]) ? $filter['price'][0] : null;
            if ($value) {
                $value = explode('-', $value);
                $from = isset($value[0]) ? (float)$value[0] : null;
                $to = isset($value[1]) ? (float)$value[1] : null;
                $query->where(function ($query) use ($from, $to) {
                    if (!is_null($from)) {
                        $query->where('price', '>=', $from);
                    }
                    if (!is_null($to)) {
                        $query->where('price', '<=', $to);
                    }
                });
            }
        }

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        // Сортировка
        if (key_exists('price', $condition->getOrderBy())) {
            $query->orderBy('price', $condition->getOrderBy()['price'] === SORT_DESC ? 'desc' : 'asc');
        } else {
            $query->orderBy('created_at', 'desc');
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function getCount(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $query = $model->newQuery();

        if ($condition) {
            // Scope Main
            if (key_exists('main', $condition->getScopes())) {
                $query->main($condition->getScopes()['main']);
            }

            // Scope Visible
            if (key_exists('visible', $condition->getScopes())) {
                $query->visible($condition->getScopes()['visible']);
            }
        }

        return $query->count();
    }

    /**
     * @inheritdoc
     */
    public function getMainProducts(ConditionInterface $condition = null, $id = null)
    {
        $model = $this->model;
        $query = $model->total()->inRandomOrder();

        // Фильтрует текущий продукт в рекомендациях
        if ($id) {
            $query->where('id', '!=', $id);
        }

        if (key_exists('main', $condition->getScopes())) {
            $query->main($condition->getScopes()['main']);
        }

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * Метод для основного поиска по продуктам
     *
     * @param $condition
     * @param $query
     * @return array
     */
    private function mainSearching($condition, $query)
    {
        // Поиск по категории
        if ($condition->getCondition('category_id') || $condition->getCondition('category_name') || $condition->getCondition('category')) {
            $query->orWhereHas('categories', function ($query) use ($condition) {
                if ($condition->getCondition('category_id')) {
                    $query->orWhere($condition->getCondition('category_id'));
                }
                if ($condition->getCondition('category')) {
                    $query->orWhereHas('translations', function ($query) use ($condition) {
                        $names = $condition->getCondition('category')['category'];
                        foreach ($names as $key => $name) {
                            $query->orWhere('name', 'like', $name . '%');
                        }
                    });
                }
            });
        }

        // Поиск по описанию товара
        if ($condition->getCondition('product')) {
            $query->orWhereHas('translations', function ($query) use ($condition) {
                $desc = $condition->getCondition('product')['product'];
                $query->orWhere('name', 'like', '%' . $desc . '%');
                $query->orWhere('description_short', 'like', '%' . $desc . '%');
                $query->orWhere('description_full', 'like', '%' . $desc . '%');
            });
        }

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return [
            'query' => $query,
            'condition' => $condition
        ];
    }

    /**
     * Пареметры поиска
     * desc - по описанию продукта короткому и полному
     * category - по наименованию категории
     * product - по наименованию продукта
     * visible - по доступности продукта
     *
     * id - переход на продукт с id
     * vendor - переход на продукт с переданым vendor
     *
     * @param ConditionInterface|null $condition
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Collection
     */
    public function filterSearch(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $query = $model::total();

        $result = $this->mainSearching($condition, $query);

        // Сортировка
        if (key_exists('price', $condition->getOrderBy())) {
            $query->orderBy('price', $condition->getOrderBy()['price'] == SORT_DESC ? 'desc' : 'asc');
        } elseif (key_exists('id', $condition->getOrderBy())) {
            $query->orderBy('id', $condition->getOrderBy()['id'] == SORT_DESC ? 'desc' : 'asc');
        } else {
            $query->orderBy('created_at', 'desc');
        }

        return $this->queryAll($result['query'], $result['condition']);
    }



    # -----------------------------------------------------------


    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        /*if (!$data['rating']){
            $data['rating'] = 0;
        }*/

        $this->setAttribute($model, $data, 'alias');
        $this->setAttribute($model, $data, 'price');
        $this->setAttribute($model, $data, 'amount');
        $this->setAttribute($model, $data, 'vendor_code');
        $this->setAttribute($model, $data, 'mpn_code');
        $this->setAttribute($model, $data, 'visible');
        $this->setAttribute($model, $data, 'rating');
        $this->setAttribute($model, $data, 'pre_order');
        $this->setAttribute($model, $data, 'main');
        $model->save();

        // Переводы
        if (isset($data['translations'])) {
            $existingLanguages = [];
            foreach ($data['translations'] as $lang => $dataTranslation) {
                foreach ($model->translations as $translation) {
                    if ($translation->language->locale === $lang) {
                        if ($dataTranslation) {
                            $existingLanguages[$lang] = true;
                            $this->setAttribute($translation, $dataTranslation, 'name');
                            $this->setAttribute($translation, $dataTranslation, 'meta_title');
                            $this->setAttribute($translation, $dataTranslation, 'meta_description');
                            $this->setAttribute($translation, $dataTranslation, 'meta_keywords');
                            $this->setAttribute($translation, $dataTranslation, 'description_short');
                            $this->setAttribute($translation, $dataTranslation, 'description_full');
                            $this->setAttribute($translation, $dataTranslation, 'delivery_policy');
                            $this->setAttribute($translation, $dataTranslation, 'guarantees');
                            $translation->save();
                        } else {
                            // Если язык пустой, удалить его
                            $translation->delete();
                        }
                        break;
                    }
                }
            }

            // Создание новых переводов
            foreach ($data['translations'] as $lang => $dataTranslation) {
                if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                    continue;
                }
                $newTranslation = new ProductTranslate;
                $newTranslation->product_id = $model->id;
                $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
                $this->setAttribute($newTranslation, $dataTranslation, 'name');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_title');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_description');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_keywords');
                $this->setAttribute($newTranslation, $dataTranslation, 'description_short');
                $this->setAttribute($newTranslation, $dataTranslation, 'description_full');
                $newTranslation->save();
            }
        }

        // Категории
        if (isset($data['categories'])) {
            ProductCategories::where(['product_id' => $model->id])->delete();
            foreach ($data['categories'] as $category) {
                $relProductCategories = new ProductCategories;
                $relProductCategories->category_id = $category->id;
                $relProductCategories->product_id = $model->id;
                $relProductCategories->save();
            }
        }

        if (isset($data['paymentSystems'])) {
            $model->paymentSystems()->sync($data['paymentSystems']);
        }

        // Фильтр
        if (isset($data['properties'])) {
            foreach ($data['properties'] as $property) {
                if (empty($property['value_id']) && empty($property['custom_value'])) {
                    ProductRelation::where([
                        'product_id' => $property['product_id'],
                        'property_id' => $property['property_id']
                    ])->delete();
                } else {
                    $relProductRelation = ProductRelation::where([
                        'product_id' => $property['product_id'],
                        'property_id' => $property['property_id']
                    ])->first();
                    if (empty($relProductRelation)) {
                        $relProductRelation = new ProductRelation();
                        $this->setAttribute($relProductRelation, $property, 'product_id');
                        $this->setAttribute($relProductRelation, $property, 'property_id');
                    }
                    $this->setAttribute($relProductRelation, $property, 'value_id');
                    $this->setAttribute($relProductRelation, $property, 'custom_value');
                    $this->setAttribute($relProductRelation, $property, 'custom_value_type');
                    $this->setAttribute($relProductRelation, $property, 'order');
                    $relProductRelation->save();
                }
            }
        }

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }
}