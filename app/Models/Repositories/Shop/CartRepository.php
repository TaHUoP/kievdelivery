<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\CartRepository as CartInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Facades\Shop;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shop\CartProducts;
use App\Models\Traits\QueryTrait;

/**
 * Cart Repository
 * @package App\Repositories\Shop
 */
class CartRepository implements CartInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $cart
     */
    public function __construct(Model $cart)
    {
        $this->model = $cart;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::total();
        $query->where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total();

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'user_id');
        $this->queryCondition($query, $condition, 'hash');

        if (in_array('processed', $condition->getScopes())) {
            $query->processed($condition->getScopes()['processed']);
        }

        if ($condition->getCondition('cart_product_info')) {
            $query->whereHas('products', function ($query) use ($condition) {
                if (isset($condition->getCondition('cart_product_info')['id'])) {
                    $query->where([CartProducts::getTableName() . '.id' => $condition->getCondition('cart_product_info')['id']]);
                }
            });
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total();

        if (in_array('processed', $condition->getScopes())) {
            $query->processed($condition->getScopes()['processed']);
        }

        return $this->queryAll($query, $condition);
    }




    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        $isNewRecord = false;
        if (is_null($model)) {
            $isNewRecord = true;
            $model = $this->model;
        }

        if (isset($data['cart']) && is_array($data['cart'])) {
            $this->setAttribute($model, $data['cart'], 'hash');
            $this->setAttribute($model, $data['cart'], 'user_id');
            $this->setAttribute($model, $data['cart'], 'processed');
            $this->setAttribute($model, $data['cart'], 'discount');
            $this->setAttribute($model, $data['cart'], 'total_price');
            $this->setAttribute($model, $data['cart'], 'discount_code');
            $model->save();
        }

        if (isset($data['products']) && is_array($data['products'])) {
            if ($isNewRecord) {
                foreach ($data['products'] as $productId => $amount) {
                    $product = new CartProducts();
                    $product->cart_id = $model->id;
                    $product->product_id = $productId;
                    $product->amount = $amount;
                    $product->save();
                }
            } else {
                foreach ($data['products'] as $productId => $amount) {
                    $relationProduct = null;
                    foreach ($model->products as $relation) {
                        if ($productId == $relation->product_id) {
                            $relationProduct = $relation;
                            break;
                        }
                    }
                    if (is_null($relationProduct)) {
                        $product = new CartProducts();
                        $product->cart_id = $model->id;
                        $product->product_id = $productId;
                        $product->amount = $amount;
                        $product->save();
                    } else {
                        if ($amount === 0) {
                            $relationProduct->delete();
                        } else {
                            $relationProduct->amount = $amount;
                            $relationProduct->save();
                        }
                    }
                }
            }
        }

        // Обновить общую цену товаров
        if ($cart = $this->findById($model->id)) {
            $totalPrice = 0;
            foreach ($cart->products as $relation) {
                $totalPrice += round(Shop::getPrice($relation->product, false)) * $relation->amount;
            }
            $cart->total_price = $totalPrice;
            $cart->save();
        }

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }
}