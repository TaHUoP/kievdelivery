<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\DiscountRepository as DiscountInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;

/**
 * Discount Repository
 * @package App\Repositories\Shop
 */
class DiscountRepository implements DiscountInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $discount
     */
    public function __construct(Model $discount)
    {
        $this->model = $discount;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();
        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();

        return $this->queryAll($query, $condition);
    }

    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'percents');
        $this->setAttribute($model, $data, 'date_from');
        $this->setAttribute($model, $data, 'date_to');

        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->forceDelete();
    }
}