<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\CategoryRepository as CategoryInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Shop\Translate\Category as CategoryTranslate;
use App\Models\Shop\Belongs\CategoriesProperties;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Models\Traits\QueryTrait;
use App\Models\Shop\Property;

/**
 * Category Repository
 * @package App\Repositories\Shop
 */
class CategoryRepository implements CategoryInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $category
     */
    public function __construct(Model $category)
    {
        $this->model = $category;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $query = $this->model->newQuery();

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'alias');

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();
        $query->defaultOrder()
            ->withDepth()
            ->translates();

        $this->queryCondition($query, $condition, 'ids');

        if ($condition->getCondition('search_name')) {
            $query->whereHas('translations', function ($query) use ($condition) {
                $query->where('name', 'like', '%' . $condition->getCondition('search_name')['name'] . '%');
            });
        }

        return $this->queryAll($query, $condition);
    }






    # -----------------------------------------------------------

    /**
     * TODO вынести в интерфейс
     * @param ConditionInterface $condition
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function findRecommended(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        //Recommended
        if ($condition->getCondition('recommended')) {
            $this->queryCondition($query, $condition, 'recommended');
        }

        return $this->queryAll($query, $condition);
    }

    // TODO: вынести в интерфейс

    /**
     * Возвращает коллекцию состоящую из категории и ее дочерних потомков
     *
     * root - Ключ категории
     * parents - Родительские категории
     * children - Дочерние категории
     *
     * @param string $id
     * @param null $depth
     * @param bool $childrenVisible
     * @return Collection|null
     */
    public function findByNested($id = 'root', $depth = null, $childrenVisible = true)
    {
        $where = is_string($id) ? ['alias' => $id] : ['id' => $id];

        $model = $this->model;

        $root = $model::withDepth()
            ->with(['translations'])
            ->where($where)
            ->first();

        if (!$root) {
            return null;
        }

        $query = $root->descendants()
            ->withDepth()
            ->with(['translations']);

        $parents = $root->ancestors()
            ->withDepth()
            ->with(['translations'])
            ->get();

        if (!is_null($depth)) {
            $query->having('depth', '=', $root->depth + (int)$depth);
        }

        if ($childrenVisible) {
            $children = $query->defaultOrder()->get()->toTree();
        } else {
            $children = $query->defaultOrder()->get()->toTree();
        }

        return collect([
            'root' => $root,
            'parents' => $parents,
            'children' => $children,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function findDescendantsByParent(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $rootQuery = $model->newQuery();
        $this->queryCondition($rootQuery, $condition, 'alias');
        $root = $rootQuery->first();

        if ($root) {
            $query = $root->descendants()->defaultOrder()->withDepth();
            $query->with(['translations', 'properties' => function($query) use ($condition) {
                if (in_array('property.isFilter', $condition->getScopes())) {
                    $query->isFilter();
                }
            }]);
            // Scope Visible
            if (key_exists('visible', $condition->getScopes())) {
                $query->visible($condition->getScopes()['visible']);
            }

            $query->having('depth', '=', 1);
            return $query->get();
        }
        return [];
    }

    /**
     * @inheritdoc
     */
    public function findAncestorsByNodeId($node_id)
    {
        $model = $this->model;
        return $model::ancestorsOf($node_id);
    }

    /**
     * Находит детей по родительскому id
     *
     * @param $parent_id
     * @return mixed
     */
    public function findOrderedChildrenOf($parent_id)
    {
        $query = $this->model->newQuery();

        return $query->where('parent_id', $parent_id)->defaultOrder()->get();
    }

    /**
     * Перемещает первую модель на место перед второй
     *
     * @param Model $model
     * @param Model $beforeNode
     * @return mixed
     */
    public function placeBefore(Model $model, Model $beforeNode)
    {
        return $model->beforeNode($beforeNode)->save();
    }

    /**
     * Перемещает первую модель на место после второй
     *
     * @param Model $model
     * @param Model $beforeNode
     * @return mixed
     */
    public function placeAfter(Model $model, Model $beforeNode)
    {
        return $model->afterNode($beforeNode)->save();
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'alias');
        $this->setAttribute($model, $data, 'recommended');
        $this->setAttribute($model, $data, 'visible');
        $this->setAttribute($model, $data, 'order');
        $this->setAttribute($model, $data, 'parent_id');
        $model->save();

        // Переводы
        if (isset($data['translations'])) {
            $existingLanguages = [];
            foreach ($data['translations'] as $lang => $dataTranslation) {
                foreach ($model->translations as $translation) {
                    if ($translation->language->locale === $lang) {
                        if ($dataTranslation) {
                            $existingLanguages[$lang] = true;
                            $this->setAttribute($translation, $dataTranslation, 'name');
                            $this->setAttribute($translation, $dataTranslation, 'menu_name');
                            $this->setAttribute($translation, $dataTranslation, 'meta_title');
                            $this->setAttribute($translation, $dataTranslation, 'meta_description');
                            $this->setAttribute($translation, $dataTranslation, 'meta_keywords');
                            $this->setAttribute($translation, $dataTranslation, 'description_full');
                            $translation->save();
                        } else {
                            // Если язык пустой, удалить его
                            $translation->delete();
                        }
                        break;
                    }
                }
            }

            // Создание новых переводов
            foreach ($data['translations'] as $lang => $dataTranslation) {
                if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                    continue;
                }
                $newTranslation = new CategoryTranslate();
                $newTranslation->category_id = $model->id;
                $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
                $this->setAttribute($newTranslation, $dataTranslation, 'name');
                $this->setAttribute($newTranslation, $dataTranslation, 'menu_name');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_title');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_description');
                $this->setAttribute($newTranslation, $dataTranslation, 'meta_keywords');
                $this->setAttribute($newTranslation, $dataTranslation, 'description_full');
                $newTranslation->save();
            }
        }


        // Свойства
        if (isset($data['properties'])) {

            // TODO: сделать редактирование свойств у дочерних категорий
            CategoriesProperties::where(['category_id' => $model->id])->delete();
            $properties = Property::whereIn('id', $data['properties'])->get();

            foreach ($properties as $property) {
                $relCategoriesProperties = new CategoriesProperties();
                $relCategoriesProperties->category_id = $model->id;
                $relCategoriesProperties->property_id = $property->id;
                $relCategoriesProperties->order = 0;
                $relCategoriesProperties->save();
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->delete();
    }
}