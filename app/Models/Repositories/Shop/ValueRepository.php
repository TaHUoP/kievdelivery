<?php 

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\ValueRepository as ValueInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Shop\Translate\Value as ValueTranslate;
use App\Models\Shop\Belongs\PropertiesValues;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Value Repository
 * @package App\Repositories\Shop
 */
class ValueRepository implements ValueInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $value
     */
    public function __construct(Model $value)
    {
        $this->model = $value;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::translates();
        $query->with(['properties' => function($query) {
            $query->translates();
        }]);
        $query->where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $query->with(['properties' => function($query) {
            $query->translates();
        }]);

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        return $this->queryAll($query, $condition);
    }





    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        $isNewRecord = false;
        if (is_null($model)) {
            $isNewRecord = true;
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'alias');
        $this->setAttribute($model, $data, 'sort');
        $model->save();

        // Связь Свойства - значения
        if ($isNewRecord) {
            $relPropertiesValues = new PropertiesValues;
            $relPropertiesValues->property_id = $data['property_id'];
            $relPropertiesValues->value_id = $model->id;
            $relPropertiesValues->save();
        } else {
            $relPropertiesValues = PropertiesValues::where(['value_id' => $model->id])->first();
            $relPropertiesValues->property_id = $data['property_id'];
            $relPropertiesValues->save();
        }

        $existingLanguages = [];
        foreach ($data['translations'] as $lang => $dataTranslation) {
            foreach ($model->translations as $translation) {
                if ($translation->language->locale === $lang) {
                    if ($dataTranslation) {
                        $existingLanguages[$lang] = true;
                        $this->setAttribute($translation, $dataTranslation, 'name');
                        $translation->save();
                    } else {
                        // Если язык пустой, удалить его
                        $translation->delete();
                    }
                    break;
                }
            }
        }

        // Создание новых переводов
        foreach ($data['translations'] as $lang => $dataTranslation) {
            if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                continue;
            }

            $newTranslation = new ValueTranslate();
            $newTranslation->value_id = $model->id;
            $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
            $this->setAttribute($newTranslation, $dataTranslation, 'name');
            $newTranslation->save();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        // Удалить связи
        PropertiesValues::where(['value_id' => $model->id])->delete();

        // Удалить значение
        return $model->delete();
    }

}