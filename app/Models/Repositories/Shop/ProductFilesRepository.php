<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\ProductFilesRepository as ProductFilesInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shop\ProductFile;
use App\Models\Traits\QueryTrait;

/**
 * Product Files Repository
 * @package Api\Shop\Contracts\Entities
 */
class ProductFilesRepository implements ProductFilesInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $productFile
     */
    public function __construct(Model $productFile)
    {
        $this->model = $productFile;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        return ;
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        return ;
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        return ;
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if ($model === null) {
            $model = new ProductFile();
        }

        $this->setAttribute($model, $data, 'product_id');
        $this->setAttribute($model, $data, 'url');
        $this->setAttribute($model, $data, 'name');

        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }
}