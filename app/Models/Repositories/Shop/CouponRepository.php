<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\CouponRepository as CouponInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;

/**
 * Coupon Repository
 * @package App\Repositories\Shop
 */
class CouponRepository implements CouponInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $coupon
     */
    public function __construct(Model $coupon)
    {
        $this->model = $coupon;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();
        $this->queryCondition($query, $condition, 'code');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model->newQuery();

        return $this->queryAll($query, $condition);
    }

    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'name');
        $this->setAttribute($model, $data, 'date_expiry');
        $this->setAttribute($model, $data, 'code');
        $this->setAttribute($model, $data, 'ttl');
        $this->setAttribute($model, $data, 'quantity');
        $this->setAttribute($model, $data, 'percentage');

        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->forceDelete();
    }
}