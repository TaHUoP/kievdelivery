<?php 

namespace App\Repositories\Shop;

use Api\App\Storage\Condition;
use Api\Shop\Contracts\Entities\PropertyRepository as PropertyInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Shop\Translate\Property as PropertyTranslate;
use App\Models\Shop\Belongs\PropertiesValues;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Product Repository
 * @package App\Repositories\Shop
 */
class PropertyRepository implements PropertyInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $property
     */
    public function __construct(Model $property)
    {
        $this->model = $property;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::translates();
        $query->where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        if (in_array('values', $condition->getRelations())) {
            $query->with(['values' => function($query) {
                $query->translates();
            }]);
        }

        if ($condition->getCondition('search_name')) {
            $query->whereHas('translations', function ($query) use ($condition) {
                $query->where('name', 'like', '%' . $condition->getCondition('search_name')['name'] . '%');
            });
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function findAllByFilter(array $ids, ConditionInterface $condition = null)
    {
        $query = $this->model->newQuery();
        $query->whereIn('alias', $ids);

        return $query->get();
    }


    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'is_filter');
        $this->setAttribute($model, $data, 'alias');
        $model->save();

        $existingLanguages = [];
        foreach ($data['translations'] as $lang => $dataTranslation) {
            foreach ($model->translations as $translation) {
                if ($translation->language->locale === $lang) {
                    if ($dataTranslation) {
                        $existingLanguages[$lang] = true;
                        $this->setAttribute($translation, $dataTranslation, 'name');
                        $translation->save();
                    } else {
                        // Если язык пустой, удалить его
                        $translation->delete();
                    }
                    break;
                }
            }
        }

        // Создание новых переводов
        foreach ($data['translations'] as $lang => $dataTranslation) {
            if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                continue;
            }

            $newTranslation = new PropertyTranslate;
            $newTranslation->property_id = $model->id;
            $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
            $this->setAttribute($newTranslation, $dataTranslation, 'name');
            $newTranslation->save();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        // Удалить связи
        foreach ($model->values as $value) {
            $value->delete();
        }

        // Удалить свойство
        return $model->delete();
    }

}