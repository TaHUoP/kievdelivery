<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\CheckoutRepository as CheckoutInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\City;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;
use App\Models\Shop\Delivery;
use Config;
use Illuminate\Support\Facades\Cookie;

/**
 * Checkout Repository
 * @package App\Repositories\Shop
 */
class CheckoutRepository implements CheckoutInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $checkout
     */
    public function __construct(Model $checkout)
    {
        $this->model = $checkout;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::total()->where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {


        $model = $this->model;
        $query = $model::total();

        $this->queryCondition($query, $condition, 'id');

        if ($condition->getCondition('user_id')) {
            $query->whereHas('cart', function ($query) use ($condition) {
                $query->where(function ($query) use ($condition) {
                    $query->where($condition->getCondition('user_id'));
                });
            });
        }

        // Scope Paid
        if (key_exists('paid', $condition->getScopes())) {
            $query->paid($condition->getScopes()['paid']);
        }

        // Scope Status
        if (key_exists('status', $condition->getScopes())) {
            $query->status($condition->getScopes()['status']);
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total();

        $this->queryCondition($query, $condition, 'status');

        if ($condition->getCondition('user_id')) {
            $query->whereHas('cart', function ($query) use ($condition) {
                $query->where($condition->getCondition('user_id'));
            });
        }

        // Scope Paid
        if (key_exists('paid', $condition->getScopes())) {
            $query->paid($condition->getScopes()['paid']);
        }

        // Scope Status
        if (key_exists('status', $condition->getScopes())) {
            $query->status($condition->getScopes()['status']);
        }

        $query->orderDesc();

        return $this->queryAll($query, $condition);
    }

    /**
     * @param ConditionInterface|null $condition
     * @return mixed
     */
    public function getCount(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $query = $model->newQuery();

        if (!is_null($condition)) {
            $this->queryCondition($query, $condition, 'status');

            // Scope Paid
            if (key_exists('paid', $condition->getScopes())) {
                $query->paid($condition->getScopes()['paid']);
            }
        }

        return $query->count();
    }

    /**
     * @return mixed
     */
    public function getStatusSuccess()
    {
        $model = $this->model;
        return $model::STATUS_SUCCESS;
    }

    /**
     * @return mixed
     */
    public function getStatusFail()
    {
        $model = $this->model;
        return $model::STATUS_FAIL;
    }

    /**
     * @return mixed
     */
    public function getStatusPending()
    {
        $model = $this->model;
        return $model::STATUS_PENDING;
    }

    /**
     * @inheritdoc
     */
    public function getStatusArray()
    {
        return $this->model->getStatusArray();
    }



    # -----------------------------------------------------------


    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        if (isset($data['cart_id'])) {
            $this->setAttribute($model, $data, 'cart_id');
        }

        if (isset($data['checkout'])) {

            if (!isset($data['checkout']['status'])) {
                $data['checkout']['status'] = $this->getStatusPending();
            }

            $this->setAttribute($model, $data['checkout'], 'first_name');
            $this->setAttribute($model, $data['checkout'], 'last_name');
            $this->setAttribute($model, $data['checkout'], 'email');
            $this->setAttribute($model, $data['checkout'], 'address');
            $this->setAttribute($model, $data['checkout'], 'country');
            $this->setAttribute($model, $data['checkout'], 'phone');
            $this->setAttribute($model, $data['checkout'], 'payment_system');
            $this->setAttribute($model, $data['checkout'], 'status');
            $this->setAttribute($model, $data['checkout'], 'paid');
            $this->setAttribute($model, $data['checkout'], 'payment_data');
            $this->setAttribute($model, $data['checkout'], 'send_sms');

            $model->save();
        }

        if (isset($data['delivery'])) {
            if ($model->delivery) {
                $delivery = $model->delivery;
            } else {
                $delivery = new Delivery;
                $delivery->checkout_id = $model->id;
            }

            // TODO перенести логику в сервис
//            if (isset($data['cart'])){
//                if ($data['cart']->total_price > Config::get('static.deliveryPriceBorder')){
                    $data['delivery']['delivery_price'] = \App\Helpers\StaticsHelper::getCityFee();
//                } else {
//                    $data['delivery']['delivery_price'] = Config::get('static.deliveryPrice') + \App\Helpers\StaticsHelper::getCityFee();
//                }
//            }

            $this->setAttribute($delivery, $data['delivery'], 'first_name');
            $this->setAttribute($delivery, $data['delivery'], 'last_name');
            $this->setAttribute($delivery, $data['delivery'], 'address');
            $this->setAttribute($delivery, $data['delivery'], 'phone');
            $this->setAttribute($delivery, $data['delivery'], 'country');
            $this->setAttribute($delivery, $data['delivery'], 'delivery_price');
            $this->setAttribute($delivery, $data['delivery'], 'city');
            $this->setAttribute($delivery, $data['delivery'], 'date');
            $this->setAttribute($delivery, $data['delivery'], 'comment_recipient');
            $this->setAttribute($delivery, $data['delivery'], 'comment_note');
            $this->setAttribute($delivery, $data['delivery'], 'shipment');
            $delivery->save();
        }

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }
}