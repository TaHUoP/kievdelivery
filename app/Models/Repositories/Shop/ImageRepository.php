<?php

namespace App\Repositories\Shop;

use Api\Shop\Contracts\Entities\ImageRepository as ImageInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Shop\Translate\Image as ImageTranslate;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;
use DB;

/**
 * Image Repository
 * @package Api\Shop\Contracts\Entities
 */
class ImageRepository implements ImageInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $product
     */
    public function __construct(Model $product)
    {
        $this->model = $product;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::total();
        $query = $model->where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total($condition->getRelations() ?: ['product']);

        $this->queryCondition($query, $condition, 'id');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::total($condition->getRelations() ? $condition->getRelations() : []);

        // Поиск по ids
        if ($condition->getCondition('ids')) {
            $this->queryCondition($query, $condition, 'ids');
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function getLastOrder($productId)
    {
        $model = $this->model;
        $order = $model::where('product_id', $productId)
            ->select('order')
            ->orderDesc()
            ->first();
        return ($order ? $order->order : 0);
    }


    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function order(array $ids, $productId)
    {
        $model = $this->model;
        $images = $model::where('product_id', $productId)->whereIn('id', $ids)->get();

        foreach ($ids as $key => $id) {
            $id = (int)$id;
            if ($id === 0) {
                continue;
            }
            foreach ($images as $image) {
                if ($id === $image->id) {
                    $image->order = $key;
                    $image->save();
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        } else {
            if (isset($data['default']) && $data['default'] && $data['default'] != $model->default) {
                $model::where(['product_id' => $model->product_id])->update(['default' => 0]);
            }
        }

        $this->setAttribute($model, $data, 'product_id');
        $this->setAttribute($model, $data, 'preview_url');
        $this->setAttribute($model, $data, 'review_url');
        $this->setAttribute($model, $data, 'default');
        $this->setAttribute($model, $data, 'order');
        $this->setAttribute($model, $data, 'visible');
        $model->save();

        // Переводы
        if (isset($data['translations'])) {
            $existingLanguages = [];
            foreach ($data['translations'] as $lang => $dataTranslation) {
                foreach ($model->translations as $translation) {
                    if ($translation->language->locale === $lang) {
                        if ($dataTranslation) {
                            $existingLanguages[$lang] = true;
                            $this->setAttribute($translation, $dataTranslation, 'title');
                            $this->setAttribute($translation, $dataTranslation, 'alt');
                            $translation->save();
                        } else {
                            // Если язык пустой, удалить его
                            $translation->delete();
                        }
                        break;
                    }
                }
            }

            // Создание новых переводов
            foreach ($data['translations'] as $lang => $dataTranslation) {
                if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                    continue;
                }
                $newTranslation = new ImageTranslate();
                $newTranslation->image_id = $model->id;
                $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
                $this->setAttribute($newTranslation, $dataTranslation, 'title');
                $this->setAttribute($newTranslation, $dataTranslation, 'alt');
                $newTranslation->save();
            }
        }

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }
}