<?php

namespace App\Repositories;

use Api\App\Contracts\Entities\CountriesRepository as CountriesInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Content Repository
 * @package App\Repositories
 */
class CountriesRepository implements CountriesInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Model
     */
    protected $translation;

    /**
     * @param Model $countries
     * @param Model $translation
     */
    public function __construct(Model $countries, Model $translation)
    {
        $this->model = $countries;
        $this->translation = $translation;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::translates();
        $query->where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $this->queryCondition($query, $condition, 'id');

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        // Scope Visible
        if (key_exists('visible', $condition->getScopes())) {
            $query->visible($condition->getScopes()['visible']);
        }

        $query->orderDesc();


        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'priority');
        $this->setAttribute($model, $data, 'visible');
        $model->save();

        $existingLanguages = [];
        foreach ($data['translations'] as $lang => $dataTranslation) {
            foreach ($model->translations as $translation) {
                if ($translation->language->locale === $lang) {
                    if ($dataTranslation) {
                        $existingLanguages[$lang] = true;
                        $this->setAttribute($translation, $dataTranslation, 'countries_id');
                        $this->setAttribute($translation, $dataTranslation, 'lang_id');
                        $this->setAttribute($translation, $dataTranslation, 'name');
                        $translation->save();
                    } else {
                        // Если язык пустой, удалить его
                        $translation->delete();
                    }
                    break;
                }
            }
        }

        // Создание новых переводов
        foreach ($data['translations'] as $lang => $dataTranslation) {
            if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                continue;
            }
            $newTranslation = clone $this->translation;
            $newTranslation->content_id = $model->id;
            $this->setAttribute($newTranslation, $dataTranslation, 'countries_id');
            $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
            $this->setAttribute($newTranslation, $dataTranslation, 'name');
            $newTranslation->save();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }

}