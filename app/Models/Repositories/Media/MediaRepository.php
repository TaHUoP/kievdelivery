<?php

namespace App\Repositories\Media;

use Api\Media\Contracts\Entities\MediaRepository as MediaInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use App\Models\Media\Translate\Media as MediaTranslate;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Media Repository
 * @package App\Repositories\Media
 */
class MediaRepository implements MediaInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Model
     */
    protected $translation;

    /**
     * @param Model $content
     * @param Model $translation
     */
    public function __construct(Model $content, Model $translation)
    {
        $this->model = $content;
        $this->translation = $translation;
    }

    /**
     * @inheritdoc
     */
    public function getTypeImage()
    {
        $model = $this->model;
        return $model::TYPE_IMAGE;
    }

    /**
     * @inheritdoc
     */
    public function getTypeVideo()
    {
        $model = $this->model;
        return $model::TYPE_VIDEO;
    }

    /**
     * @inheritdoc
     */
    public function getTypesArray()
    {
        return $this->model->getTypesArray();
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::translates();
        $query->where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'url');
        $this->queryCondition($query, $condition, 'type');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::translates();

        $this->queryCondition($query, $condition, 'type');
        $this->queryCondition($query, $condition, 'visible');

        $query->orderDesc();

        return $this->queryAll($query, $condition);
    }


    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'preview_url');
        $this->setAttribute($model, $data, 'review_url');
        $this->setAttribute($model, $data, 'type');
        $this->setAttribute($model, $data, 'file_info');
        $this->setAttribute($model, $data, 'visible');
        $model->save();

        // Переводы
        if (isset($data['translations'])) {
            $existingLanguages = [];
            foreach ($data['translations'] as $lang => $dataTranslation) {
                foreach ($model->translations as $translation) {
                    if ($translation->language->locale === $lang) {
                        if ($dataTranslation) {
                            $existingLanguages[$lang] = true;
                            $this->setAttribute($translation, $dataTranslation, 'title');
                            $this->setAttribute($translation, $dataTranslation, 'description');
                            $translation->save();
                        } else {
                            // Если язык пустой, удалить его
                            $translation->delete();
                        }
                        break;
                    }
                }
            }

            // Создание новых переводов
            foreach ($data['translations'] as $lang => $dataTranslation) {
                if (isset($existingLanguages[$lang]) || !$dataTranslation) {
                    continue;
                }
                $newTranslation = new MediaTranslate();
                $newTranslation->media_file_id = $model->id;
                $this->setAttribute($newTranslation, $dataTranslation, 'lang_id');
                $this->setAttribute($newTranslation, $dataTranslation, 'title');
                $this->setAttribute($newTranslation, $dataTranslation, 'description');
                $newTranslation->save();
            }
        }

        return $model->id;
    }

    /**
     * @inheritdoc
     */
    public function delete($model)
    {
        return $model->delete();
    }

}