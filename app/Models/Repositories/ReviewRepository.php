<?php

namespace App\Repositories;

use App\Contracts\Repositories\ReviewRepository as ReviewInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Review Repository
 * @package App\Repositories
 */
class ReviewRepository implements ReviewInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param Model $review
     */
    public function __construct(Model $review)
    {
        $this->model = $review;
    }

    /**
     * @return mixed
     */
    public function getTypeProduct()
    {
        $model = $this->model;
        return $model::TYPE_PRODUCT;
    }

    /**
     * @return mixed
     */
    public function getTypeMain()
    {
        $model = $this->model;
        return $model::TYPE_MAIN;
    }

    /**
     * @inheritdoc
     */
    public function getTypesArray()
    {
        return $this->model->getTypesArray();
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::author();
        $query->where('id', $id);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::author();

        $this->queryCondition($query, $condition, 'id');

        if (in_array('confirmed', $condition->getScopes())) {
            $query->confirmed();
        }

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::author();

        // Scope Visible
        if (key_exists('confirmed', $condition->getScopes())) {
            $query->confirmed($condition->getScopes()['confirmed']);
        }

        return $this->queryAll($query, $condition);
    }

    /**
     * Выводим отсортированные отзывы
     *
     * @inheritdoc
     */
    public function findLastAll(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $query = $model::author();

        if ($condition){

            // Scope Visible
            if (key_exists('confirmed', $condition->getScopes())) {
                $query->confirmed($condition->getScopes()['confirmed']);
            }

            $this->queryCondition($query, $condition, 'type');
            $this->queryCondition($query, $condition, 'product_id');
        }

        $query->orderDesc();

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function getCount(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $query = $model->newQuery();

        if ($condition){

        }

        return $query->count();
    }

    public function getAverage(ConditionInterface $condition = null)
    {
        $model = $this->model;
        $query = $model->newQuery();

        // Scope Visible
        if (key_exists('confirmed', $condition->getScopes())) {
            $query->confirmed($condition->getScopes()['confirmed']);
        }

        return $query->avg('rating');
    }

    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }

        $this->setAttribute($model, $data, 'name');
        $this->setAttribute($model, $data, 'email');
        $this->setAttribute($model, $data, 'city_delivery');
        $this->setAttribute($model, $data, 'confirmed');
        $this->setAttribute($model, $data, 'type');
        $this->setAttribute($model, $data, 'product_id');
        $this->setAttribute($model, $data, 'rating');
        $this->setAttribute($model, $data, 'text');
        $this->setAttribute($model, $data, 'admin_answer');

        if (array_key_exists('product_id',$data)){
            $model->product_id = $data['product_id'];
        }

        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->delete();
    }

}