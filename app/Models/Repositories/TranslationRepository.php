<?php

namespace App\Repositories;

use Waavi\Translation\Repositories\TranslationRepository as WaaviTranslationRepository;
use Api\App\Contracts\Entities\TranslationRepository as TranslateInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Traits\QueryTrait;

/**
 * Translation Repository
 * @package App\Repositories
 */
class TranslationRepository implements TranslateInterface
{
    use QueryTrait;

    /**
     * @var WaaviTranslationRepository
     */
    protected $translationRepository;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param WaaviTranslationRepository $translationRepository
     */
    public function __construct(WaaviTranslationRepository $translationRepository)
    {
        $this->model = $translationRepository->getModel();
        $this->translationRepository = $translationRepository;
    }

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'id');
        $this->queryCondition($query, $condition, 'locale');
        $this->queryCondition($query, $condition, 'group');
        $this->queryCondition($query, $condition, 'item');

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'locale');
        $this->queryCondition($query, $condition, 'group');
        $this->queryCondition($query, $condition, 'items');

        return $this->queryAll($query, $condition);
    }

    /**
     * @inheritdoc
     */
    public function getGroupList()
    {
        $model = $this->model;
        return $model::groupBy('group')->pluck('group', 'group');
    }

    /**
     * @inheritdoc
     */
    public function selectDistinctGroupsByLocale($locale)
    {
        $query = $this->model->newQuery();
        return $query->select('group')->groupBy('group')->distinct()->get();
    }

    /**
     * @inheritdoc
     */
    public function getItemsList($group)
    {
        $model = $this->model;
        $query = $model::where(['group' => $group]);
        $query ->groupBy('item');
        $query ->lists('item', 'item');
        return $this->queryAll($query);
    }

    /**
     * @inheritdoc
     */
    public function updateDeprecated(Model $model, array $data)
    {
        $this->setAttribute($model, $data, 'id');
        $this->setAttribute($model, $data, 'text');
        return $model->save();
    }

    /**
     * @see WaaviTranslationRepository::update()
     */
    public function update($id, $text)
    {
        return $this->translationRepository->update($id, $text);
    }

    /**
     * @see WaaviTranslationRepository::create()
     */
    public function create(array $attributes)
    {
        return $this->translationRepository->create($attributes);
    }

    /**
     * Удаляет сущность используя сущность
     *
     * @param Model $model
     * @return bool|null
     */
    public function delete(Model $model)
    {
        return $model->delete();
    }



    # -----------------------------------------------------------

    // TODO: вынести в интерфейс


    public function save(array $data)
    {
        $model = $this->model;
        $this->setAttribute($model, $data, 'item');
        $this->setAttribute($model, $data, 'group');
        $this->setAttribute($model, $data, 'locale');
        $this->setAttribute($model, $data, 'text');

        return $model->save();
    }
}