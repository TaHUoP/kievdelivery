<?php

namespace App\Repositories;

use Waavi\Translation\Repositories\LanguageRepository as WaaviLanguageRepository;
use Api\App\Contracts\Entities\LanguageRepository as LanguageInterface;
use Api\App\Contracts\Storage\Condition as ConditionInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\QueryTrait;

/**
 * Language Repository
 * @package App\Repositories
 */
class LanguageRepository implements LanguageInterface
{
    use QueryTrait;

    /**
     * @var Model
     */
    protected $languageRepository;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @param WaaviLanguageRepository $languageRepository
     */
    public function __construct(WaaviLanguageRepository $languageRepository)
    {
        $this->model = $languageRepository->getModel();
        $this->languageRepository = $languageRepository;
    }
    /**
     * @inheritdoc
     */
    protected $fillable = array(
        'created_at',
        'updated_at',
        // The rest of the column names that you want it to be mass-assignable.
    );

    /**
     * @inheritdoc
     */
    public function findById($id, ConditionInterface $condition = null)
    {
        if ($condition) {
            $condition->addCondition('id', 'id', $id);
            return $this->findOne($condition);
        }

        $model = $this->model;
        $query = $model::where(['id' => $id]);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findOne(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'id');

        $this->defaultScope($query, $condition);
        $this->activeScope($query, $condition);

        return $this->queryOne($query);
    }

    /**
     * @inheritdoc
     */
    public function findAll(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'locales');
        $this->queryCondition($query, $condition, 'id');

        $this->defaultScope($query, $condition);
        $this->activeScope($query, $condition);

        return $this->queryAll($query, $condition);
    }

    #---------------------------------------------------------------------------

    // TODO: Вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function findActiveAll()
    {
        $model = $this->model;
        $query = $model::where(['active' => 1]);

        return $this->queryAll($query);
    }

    /**
     * Находит язык по статусу default
     *
     * @return Model|null|static
     */
    public function findByDefault($value = 1)
    {
        $query = $this->model->newQuery();
        $query->where(['default' => $value]);

        return $query->first();
    }

    /**
     * @inheritdoc
     */
    public function lists($active = true)
    {
        $model = $this->model;

        if ($active) {
            $model->where(['active' => 1]);
        }

        return $model->lists('name', 'id')->all();
    }

    /**
     * @inheritdoc
     */
    public function exists(ConditionInterface $condition)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);

        $this->queryCondition($query, $condition, 'id');

        $this->defaultScope($query, $condition);
        $this->activeScope($query, $condition);

        return $query->exists();
    }

    /**
     * @inheritdoc
     */
    public function other($id)
    {
        $query = $this->model->newQuery();
        $query->where('id', '!=', $id);
        return $query->first();
    }

    /**
     * @inheritdoc
     */
    public function otherExists($id)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', $id);
        return $query->exists();
    }


    # -----------------------------------------------------------

    // TODO: вынести в интерфейс

    /**
     * @inheritdoc
     */
    public function save(array $data, Model $model = null)
    {
        if (is_null($model)) {
            $model = $this->model;
        }


        $this->setAttribute($model, $data, 'locale');
        $this->setAttribute($model, $data, 'name');
        $this->setAttribute($model, $data, 'active');
        $this->setAttribute($model, $data, 'default');

        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function updateAll(ConditionInterface $condition = null, array $change)
    {
        $model = $this->model;
        $query = $model::where('id', '!=', 0);
        $this->queryCondition($query, $condition, 'id');
        return $query->update($change);
    }

    /**
     * @inheritdoc
     */
    public function delete(Model $model)
    {
        return $model->forceDelete();
    }

    /**
     * @param Builder $query
     * @param ConditionInterface $condition
     */
    protected function defaultScope(Builder &$query, ConditionInterface $condition)
    {
        if (in_array('default', $condition->getScopes())) {
            $query->where(['default' => 1]);
        }
    }

    /**
     * @param Builder $query
     * @param ConditionInterface $condition
     */
    protected function activeScope(Builder &$query, ConditionInterface $condition)
    {
        if (in_array('active', $condition->getScopes())) {
            $query->where(['active' => 1]);
        }
    }
}