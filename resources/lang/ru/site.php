<?php

return [
    'home' => 'На главную',
    'contacts' => 'Контакты',
    'address' => 'Бул.Лихачева, 4, оф.42, Киев, 01042, Украина',
    'phone' => '8(063)043433',
    'phone-a' => '8063043433',
    'ph' => 'тел.',

    'nav-about' => 'О нас',
    'nav-contact' => 'Контакты',
    'nav-delivery-policy' => 'Политика доставки',
    'nav-nodelivery' => 'Куда не доставляем',
    'nav-review' => 'Отзывы',
    'nav-faq' => 'ЧАВо',
    'nav-return' => 'Возврат и обмен',
    'nav-news' => 'Новости',
    'nav-video-gallery' => 'Видео галерея',
    'nav-delivery-photos' => 'Фото доставок',
    'nav-discounts' => 'Скидки и купоны',
    'nav-agreement' => 'Условия использования',
    'nav-delivery-cities' => 'Города доставок',
    'nav-guide' => 'Гид по Цветам и Уходу',
    'nav-flowers-and-holidays' => 'Цветы и праздники',
    'nav-guarantees' => 'Наши Гарантии',
    'nav-affiliate' => 'Стать партнеров',
    'nav-service' => 'Сервис и качество',
    'nav-secure-payment' => 'Безопасная оплата',
    'nav-sitemap' => 'Карта сайта',

    'novelty' => 'Новинки',
    'we-in-social-networks' => 'Мы в соц. сетях',
    'your-comment' => 'Пожалуйста добавьте свой отзыв здесь',

    'recommended' => 'Рекомендуем',
    'recommended-products' => 'Рекомендуем добавить к заказу',
    'recommended-products-description' => 'один из товаров ниже, чтобы сделать Ваш заказ лучше и интересней.',

    'reviews-by-products' => 'Отзывы о товаре',

    'sub-total' => 'Итого',
    'prices-are-without-excluding-delivery' => 'Стоимость указана без <br> учета доставки.',

    'add_to_cart_success' => 'Товар успешно добавлен в корзину',

    'checkout' => 'Оформить заказ',
    'checkout-guest' => 'Оформить как гость',

    'open-category' => 'Открыть категорию',

    'your-discount' => 'Ваша скидка',
    'enter-your-coupon-code' => 'Введите код купона',
    'total-for-order' => 'Общий заказ',
    'terms-of-service-and-delivery' => 'Я прочитал и принимаю условия пользовательского соглашения услуг и доставки', // I have read and accept the Terms of Service and Delivery

    'footer-copyright :y' => 'Copyright &copy; :y Киевделивери – Доставка Цветов и Подарков в Украине.',

    'read_agree' => 'Я прочитал и принимаю',
    'read_agree_continue' => 'условия пользовательского соглашения услуг и доставки',
    
    'filter-price-range-1' => 'Дешевле :price',
    'filter-price-range-2' => 'От :price_from до :price_to',
    'filter-price-range-3' => 'От :price_from до :price_to',
    'filter-price-range-4' => 'От :price и дороже',

    'login-into-my-office' => 'Вход в личный кабинет',

    'ukraine' => 'Украина',
    
    'bill-to' => 'Плательщик',
    'enter-senders-info' => "введите информацию отправителя",
    'attr.your-instroction-to-kievdelivery' => 'Ваши инструкции для KievDelivery',
    'deliver-to' => 'Получатель',
    'enter-recipients-info' => "Введите информацию получателя",
    'attr.enter-message-to-recipient' => 'Ваше сообщение получателю',
    'will-be-on-the-card' => 'Будет напечатано на карте',
    'regional-courier-delivery' => 'Региональная Курьерская Доставка',
    'pay-by' => 'Оплата с помощью',
    'credit-card' => 'кредитной карты',

    'delivery-cash-or-cash' => 'Наложенный платеж или наличные',

    'learn_more' => 'Узнать подробнее',

    'online_help' => 'Онлайн Помощь',

    'read-full' => 'Читать полностью',

    'public_offer' => 'Публичная Оферта',
    'personal_information' => 'Персональные данные',

    'text_we_service' => 'Доставляем цветы по',
    'text_all_ukraine' => 'всей Украине',

    'best_regards' => 'С уважением',

    'photo_deliveries' => 'Фото доставок',
    'video_deliveries' => 'Видео доставок',
    'custom_order' => 'Особый заказ',

    'redirect_to_way_for_pay' => 'Сейчас Вы будете перемещены на страницу платежной системы :url. Если этого не произошло, нажмине на кнопку вперед.',

    'checkout_status_success' => 'Выполнен',
    'checkout_status_fail' => 'Отменен',

    'your-name' => 'Ваше имя',
    'your-email' => 'Ваш E-mail',
    'your-rating' => 'Ваша оценка',
    'you-want-being-ordered' => 'Вы хотите, чтобы мы доставили',
    'describe-to-order' => 'пожалуйста опишите, что Вы хотите заказать к доставке',
    'to-be-delivered-to' => 'Куда доставить ',
    'write-info' => 'пожалуйста напишите имя, телефон и город получателя',
    'add-insruct' => 'Дополнительные инструкции',
    'if-any' => 'если есть',
    'message-to-recipient' => 'Ваше сообщение получателю',

    'attr.g-recaptcha-response' => 'Защитный код',

    'payment_data_received' => 'Данные об оплате получены и переданы на обработку. Пожалуйста ожидайте!',
    'payment_data_error' => 'Платеж отменен. Пожалуйста уточните детали у Вашего банка и попробуйте еще раз.',

    'text_postcard' => 'Текст открытки',
    'instructions_for_kiev_delivery' => 'Инструкции для Киевделивери',
    'delivery_address' => 'Адрес Доставки',
    'information_about_the_payer' => 'Информация о Плательщике',
    'details_of_the_order' => 'Детали Заказа',

    'order_id' => 'Номер Заказа',
    'order_date' => 'Дата Заказа',
    'order_status' => 'Статус Заказа',
    'order_address_from' => 'Откуда',
    'order_address_to' => 'Куда',

    'our_company' => 'Наша компания',
    'customer_support' => 'Поддержка клиентов',
    'important_info' => 'Важная информация',
    'photo_video' => 'Фото и видео материалы',
    'see_more' => 'подробнее',

    'how-to-make-order' => 'Как сделать заказ?',
    'how-to-change-order' => 'Как изменить заказ?',

    'title.logo' => 'Киевделивери - цветы и подарки в Украине',
    'title.logo-dark' => 'Премиум доставка от Киевделивери',
    'alt.logo' => 'Киевделивери - цветы и подарки в Украине - логотип',
    'alt.logo-dark' => 'Премиум доставка от Киевделивери - логотип',
];