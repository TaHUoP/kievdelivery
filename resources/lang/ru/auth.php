<?php

return [
    'logout' => 'Выход',
    'signin' => 'Вход',
    'signup' => 'Регистрация',

    'login' => 'Логин',
    'password' => 'Пароль',

    'signin-you' => 'Войдите',
    'signup-you' => 'Зарегистрируйтесь',

    'forgot-your-password' => 'Забыли пароль?',
    'login-and-place-your-order' => 'Логин и оформить заказ',

    'login-to-your-account' => 'Вход в аккаунт',
    'enter-your-credentials-below' => 'Введите учетные данные ниже',

    'failed' => 'Пользователь с такими данными не найден в базе данных',
    'fast-login' => 'Уже зарегистрированы? Затем войдите здесь:',

    'email-confirmation' => 'Подтверждение Email адреса',
    'enter-email-code' => 'Чтобы завершить регистрацию, пожалуйста, введите код присланный на Ваш почтовый ящик указанный при регистрации.',

    'throttle' => 'Много попыток входа. Пожалуйста попробуйте снова через :seconds секунд.',
    'login-attempts-amount' => 'У вас есть :times попытки входа',

    'register-button' => 'Нажмите для регистрацию',

    'reset-password' => 'Восстановление пароля',
    'reset-password-button' => 'Изменить пароль',
    'send-reset-link-button' => 'Отправить ссылку на восстановление пароля',

    'confirmation_success' => 'Ваша учетная запись успешно активирована.',
    'registration_success' => 'Ваша учётная запись успешно создана. На ваш почтовый адрес было отправлено письмо для активации Вашего аккаунта.',

    'attr.name' => 'Имя',
    'attr.surname' => 'Фамилия',
    'attr.password' => 'Пароль',
    'attr.password-confirmation' => 'Подтверждение пароля',
    'attr.email' => 'E-Mail адрес',
    'attr.email-confirmation' => 'Повторение E-Mail',
    'attr.phone' => 'Телефон',
    'attr.terms-and-conditions' => 'Я соглашаюсь с Правилами и Условиями использования',
    'attr.g-recaptcha-response' => 'Капча',
    'you-have-been-banned' => 'Ваш аккаунт заблокирован.',
    'you-are-not-activated' => 'Ваша учетная запись не активирована.',
    'forgot-password' => 'Забыли пароль?',
    'btn.enter-cabinet' => 'Войти в кабинет',
];


