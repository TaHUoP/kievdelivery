<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Общее
    |--------------------------------------------------------------------------
    */
    'seo' => 'SEO',
    'specify' => 'Сео-спецификация',
    'specifications' => 'Сео-спецификации',
    'control' => 'Управление SEO спецификациями',
    'all' => 'Все SEO спецификациями',
    'create' => 'Создать SEO спецификацию',
    'edit' => 'Редактировать SEO спецификацию',
    'update' => 'Обновить SEO спецификацию',

    'attr.url' => 'URL',
    'attr.meta_title' => 'Meta Title',
    'attr.meta_description' => 'Meta Description',
    'attr.meta_keywords' => 'Meta Keywords',
];
