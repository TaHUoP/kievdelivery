<?php

return [

    'products_search' => 'Добавление продукта по ID',
    'shop' => 'Магазин',

    'product-code' => 'Код Товара',
    'buy-now' => 'Купить сразу',
    'showing-verb' => 'Показывать',
    'bill-to' => 'Плательщик',
    'deliver-to' => 'Получатель',

    'filter.control' => 'Управление фильтром',
    'filter.all' => 'Фильтр',
    'filter' => 'Фильтр',

    'payment' => 'Оплата',
    'order-history' => 'История заказов',
    'paid-orders' => 'Оплаченные заказы',
    'order' => 'Заказ',
    'order-number-from' => 'Заказ №:number от :date',
    'product-name' => 'Наименование товара',
    'pay-by-credit-card' => 'Оплатить кредитной картой',
    'your-discount' => 'Ваша скидка',
    'total-for-order' => 'Всего к оплате',
    'attr.city-of-delivery' => 'Город доставки',
    'shopping-cart' => 'Корзина покупок',
    'property' => 'Свойство',
    'value' => 'Значение',
    'properties' => 'Свойства',
    'values' => 'Значения',
    'additional-files' => 'Дополнительные файлы',
    'total_price' => 'Общая сумма',
    'cart_number' => 'Купон',
    'discount' => 'Скидка',
    'kyiv_only' => 'Только в Киеве',
    'price' => 'Сумма',
    'no-product' => 'нет данных',
    'coupon-in-use' => 'Используется купон:',
    'coupon-edit' => 'Изменить купон',
    'coupon-delete' => 'Удалить купон',
    'delivery-price' => 'Региональная Курьерская Доставка',
    'comment' => 'Комментарий',
    'comment_placeholder' => 'Напишите свой комментарий',
    'not_published' => 'Не для публикации' ,

    'category_parent' => 'Родительская категория',
    'category' => 'Категория',
    'categories' => 'Категории',
    'categories.list' => 'Список категорий',
    'categories.control' => 'Управление категориями',
    'categories.all' => 'Все категории',
    'categories.create' => 'Создать категорию',
    'categories.edit' => 'Редактировать категорию',
    'categories.update' => 'Обновить категорию',
    'categories.delete' => 'Удалить категорию',

    'product' => 'Товар',
    'products' => 'Товары',
    'products.control' => 'Управление товарами',
    'products.all' => 'Все товары',
    'products.create' => 'Создать товар',
    'products.add' => 'Добавить товар',
    'products.edit' => 'Редактировать товар',
    'products.update' => 'Обновить товар',
    'products.delete' => ' Удалить товар',
    'products.file_help' => 'Добавить файлы к товару',

    'images.attr.default' => 'Главное изображение',

    'properties.add' => 'Добавить свойство',
    'properties.attr.is_filter' => 'Для фильтра',

    'checkout' => 'Оформление заказа',
    'checkouts' => 'Заказы',
    'checkouts.new' => 'Новые заказы',
    'checkouts.control' => 'Управление заказами',
    'checkouts.all' => 'Все заказы',
    'checkouts.create' => 'Создать заказ',
    'checkouts.edit' => 'Редактировать заказ',
    'checkouts.update' => 'Обновить заказ',
    'checkouts.list' => 'Список товаров',
    'checkouts.status.pending' => 'Ожидание',
    'checkouts.status.success' => 'Успешно',
    'checkouts.status.fail' => 'Отменено',
    'checkouts.not-processed :amount' => ':amount необработанный заказ| :amount необработанных заказов | :amount необработанных заказа',


    'checkouts.new-order' => 'Оформление нового заказа',
    'checkouts.list-of-products' => 'Список товаров',
    'checkouts.sum-of-order' => 'Общая сумма заказа',
    'checkouts.your-order-done' => 'Ваш заказ успешно оформлен. Через некоторое время с Вами свяжется оператор для подтверждения.',


    'coupon' => 'Купон',
    'coupons' => 'Купоны',
    'coupons.control' => 'Управление купонами',
    'coupons.all' => 'Все купоны',
    'coupons.create' => 'Создать купон',
    'coupons.add' => 'Добавить купон',
    'coupons.edit' => 'Редактировать купон',
    'coupons.update' => 'Обновить купон',

    'coupons.attr.name' => 'Название купона',
    'coupons.attr.date_expiry' => 'Дата окончания',
    'coupons.attr.code' => 'Код',
    'coupons.attr.ttl' => 'Оставшееся количество',
    'coupons.attr.quantity' => 'Стартовое количество',
    'coupons.attr.percentage' => 'Процент скидки',
    'coupons.no_discounts' => 'Нет доступной скидки по указанному номеру купона',

    'show-more' => 'Показать ещё',

    /*
    |--------------------------------------------------------------------------
    | Корзина
    |--------------------------------------------------------------------------
    */
    'cart' => 'Корзина',
    'to-cart' => 'В корзину',
    'sorting-verb' => 'Сортировать',
    'cart-products' => 'Корзина товаров',
    'sort-by-price-ascending' => 'По цене (возрастанию)',
    'sort-by-price-descending' => 'По цене (убыванию)',
    'cart-total-price :price' => 'Сумма покупки :price',
    'cart-total-products-and-price :amount :price' => ':amount товар на :price|:amount товара на :price|:amount товаров на :price',
    'cart-is-empty' => 'Корзина пуста',
    'cart-add-success' => 'Товар успешно добавлен в корзину',

    /*
    |--------------------------------------------------------------------------
    | Атрибуты
    |--------------------------------------------------------------------------
    */
    'attr.name' => 'Наименование товара',
    'attr.product_name' => 'Наименование товара',
    'attr.desc' => 'Описание',
    'attr.price' => 'Цена',
    'attr.amount' => 'Количество',
    'attr.sum' => 'Сумма',
    'attr.quantity' => 'Количество',
    'attr.total' => 'Всего',
    'attr.code' => 'Код',
    'attr.products' => 'Товары',
    'attr.priced' => 'Стоимость',
    'attr.customer' => 'Заказчик',
    'attr.discount_code' => 'Купон для скидки',
    'attr.payment' => 'Оплата',
    'attr.shipment' => 'Отгрузка',
    'attr.property' => 'Свойство',
    'attr.value' => 'Значение',
    'attr.properties' => 'Свойства',
    'attr.values' => 'Значения',
    'attr.discount' => 'Скидка',
    'attr.post' => 'Новая почта',
    'attr.type' => 'Тип доставки',
    'attr.pay_type' => 'Способ оплаты',
    'attr.comment_recipient' => 'Комментарий покупателя',
    'attr.comment_note' => 'Ваш комментарий',
    'attr.description_full' => 'Подробное описание',

    'product.attr.name' => 'Название',
    'product.attr.text' => 'Текст',
    'product.attr.meta_title' => 'Meta Title',
    'product.attr.meta_description' => 'Meta Description',
    'product.attr.meta_keywords' => 'Meta Keywords',
    'product.attr.price' => 'Цена',
    'product.attr.amount' => 'Количество',
    'product.attr.alias' => 'Алиас',
    'product.attr.vendor_code' => 'Артикул',
    'product.attr.mpn_code' => 'MPN',
    'product.product_visible' => 'Активный',
    'product.pre_order' => 'Предзаказ',
    'product.main' => 'Главный', //TODO  тут точно говорилось про главность?
    'product.attr.description_short' => 'Краткое описание',
    'product.attr.description_full' => 'Полное описание',
    'product.attr.file_name' => 'Название файла',
    'product.attr.file_url' => 'URL файла',
    'product.attr.rating' => 'Рейтинг',


    /*
    |--------------------------------------------------------------------------
    | Секции товара
    |--------------------------------------------------------------------------
    */
    'product.section.main' => 'Основное',
    'product.section.images' => 'Изображения',
    'product.section.filter' => 'Фильтр',
    'product.section.files' => 'Файлы',


    'categories.attr.visible' => 'Отображается на сайте',
    'categories.attr.recommended' => 'Рекомендованные товары',

    'paymentSystems' => 'Системы оплаты',
    'payment_system_allowed' => 'Способ оплаты заказа - только Paypal '
        . '<a href=' . route('site.page', 'paypal-explanation') . ' target="_blank" style="color: hotpink; text-decoration: underline">(прочитать объяснение)</a>'
];