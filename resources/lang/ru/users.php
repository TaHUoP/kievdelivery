<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Общее
    |--------------------------------------------------------------------------
    */
    'control' => 'Упраление пользователями',
    'user' => 'Пользователь',
    'users' => 'Пользователи',
    'all' => 'Все пользователи',
    'create' => 'Создать пользователя',
    'update' => 'Обновить пользователя',
    'edit' => 'Редактировать пользователя',


    'my-account' => 'Мой профиль',
    'signin' => 'Вход',
    'signup' => 'Регистрация',
    'user_actions' => 'Действия пользователя',
    'users_actions' => 'Действия пользователей',
    'actions' => 'Действия',
    'access' => 'Права доступа',


    'orders-history' => 'История заказов',
    'main-information' => 'Основная информация',
    'manage-password' => 'Управление паролем',
    'change-email' => 'Изменить E-Mail адрес',
    'enter-new-password' => 'Введите новый пароль',
    'repeat-new-password' => 'Повторите новый пароль',


    'btn.change-information' => 'Изменить данные',
    'btn.update-password' => 'Изменить пароль',
    'btn.update-email' => 'Изменить E-Mail адрес',


    'attr.name' => 'Имя',
    'attr.surname' => 'Фамилия',
    'attr.username' => 'Username',
    'attr.password' => 'Password',
    'attr.email' => 'Email адрес',
    'attr.cur-email' => 'Текущий Email',
    'attr.new-email' => 'Новый Email',
    'attr.phone' => 'Телефон',
    'attr.cur-password' => 'Текущий пароль',
    'attr.new-password' => 'Новый пароль',
    'attr.new-password-confirmation' => 'Подтверждение нового пароля',

    'password.info' => 'Внимание: В целях вашей безопасности пароль должен быть не менее 6 символов, содержать минимум 1 большую букву и цифру.',

];
