<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Общее
    |--------------------------------------------------------------------------
    */
    'control' => 'Управление языками',
    'translations-control' => 'Управление переводами',
    'language' => 'Язык',
    'languages' => 'Языки',
    'all' => 'Все языки',
    'create' => 'Создать Язык',
    'edit' => 'Редактировать Язык',
    'translates' => 'Переводы',
    'group' => 'Группа',
    'secondary-language' => 'Второй язык',

    /*
    |--------------------------------------------------------------------------
    | Атрибуты
    |--------------------------------------------------------------------------
    */
    'attr.locale' => 'Код',
    'attr.name' => 'Язык',
    'attr.default' => 'Основной',
    'attr.active' => 'Активный',
    'attr.constant' => 'Константа',

    /*
    |--------------------------------------------------------------------------
    | Сообщения
    |--------------------------------------------------------------------------
    */
    'msg.only-one' => 'Нельзя сделать неосновным или неактивным единственный язык!',
    'msg.delete-only-one' => 'Нельзя удалить единственный язык!',
    'msg.setting_default' => 'Выберите другой основной язык, чтобы сделать текущий не основным',

];


