<?php

return [
    'comment' => 'Коментар',
    'comment_placeholder' => 'Напишіть свій коментар',
    'not_published' => 'Не для публікації' ,
    'kyiv_only' => 'Тільки в Києві',

    'paymentSystems' => 'Спосіб оплати',
    'payment_system_allowed' => 'Спосіб оплати замовлення - тільки Paypal '
        . '<a href=' . route('site.page', 'paypal-explanation') . ' target="_blank" style="color: hotpink; text-decoration: underline">(прочитати пояснення)</a>'
];