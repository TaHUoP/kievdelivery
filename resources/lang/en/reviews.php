<?php

return [

//    'reviews' => 'Отзывы',
//    'main' => 'Основной',
//    'product' => 'К продукту',
//    'actions' => 'Действия',
//    'control' => 'Упраление отзывами',
//    'create' => 'Создать отзыв',
//    'edit' => 'Редактировать отзыв',

    /*
    |--------------------------------------------------------------------------
    | Атрибуты
    |--------------------------------------------------------------------------
    */

//    'type.product' => 'Продукт',
//    'type.main' => 'Основные',

    /*
    |--------------------------------------------------------------------------
    | Атрибуты
    |--------------------------------------------------------------------------
    */

    'attr.type' => 'Тип',
    'attr.text' => 'Tекст',
    'attr.confirmed' => 'Подтвердить',
    'attr.rating' => 'Рейтинг',
    'attr.created_at' => 'Обновить',
    'attr.product_id' => 'Продукт',
    'attr.user_id' => 'Пользователь',
    'attr.user_email' => 'Почта пользователя',
    'attr.city_delivery' => 'Город доставки',
    'attr.admin_answer' => 'Ответ администратора',
    'im_admin' => 'Admin',
    'product_review' => 'Product Review',
    'avg_rating' => 'Average rating',
    'based_on' => 'Based on :count reviews',
    'write_the_review_text' => 'Write a review to help other buyers with a choice!',
    'write_review' => 'Write the review',
];


