<?php

return [


    // Редактирования профиля
    'my-account' => 'My account',
    'orders-history' => 'Orders history',

    'main-information' => 'Main information',
    'attr.name' => 'Name',
    'attr.surname' => 'Last Name',
    // 'attr.username' => 'Username', нинужна
    'attr.email' => 'Email Address',
    'attr.phone' => 'Phone / Mobile phone',
    'btn.change-information' => 'Change Information',

    'manage-password' => 'Manage password',
    'attr.cur-password' => 'Current Password',
    'attr.new-password' => 'New Password',
    'attr.new-password-confirmation' => 'Confirm new Password',
    'btn.update-password' => 'Update Password',

    'change-email' => 'Change email',
    'attr.cur-email' => 'Current e-mail',
    'attr.new-email' => 'New e-mail',
    'btn.update-email' => 'Update e-mail',

    'enter-new-password' => 'Enter new password',
    'repeat-new-password' => 'Repeat new password',

];