<?php

return [


    // Sign Up
    'signup_welcome' => 'Dear Visitor,  :name. Thank you for creating account at our website. Your account will also provide you with access to details of previous orders, delivery addresses, and other useful information. ',
    'signup_important' => 'Important! To confirm registration, please follow the link',
    'signup_enter_a_code' => 'Or please enter the activation code at the following address',
    'signup_security' => 'You can be sure that all information you enter on our website is absolutely secure and safe, we do not and will not share it with any third parties.',
    'signup_contact' => 'If you have any questions or concerns - please :contact_url anytime.',


    'checkout_status_success' => 'Dear :name, Greetings from Kievdelivery Customer Support. <br><br> We thought you would like to know that status of your order #:id has changed. New Status is :status - it means we have delivered to recipient all the items you have ordered. <br><br> Thank you for being our customer and giving us the chance to make the delivery of your feelings in Ukraine. <br><br> Do not forget our special 5% discount coupon :coupon next time you do shopping with us.',
    'checkout_status_fail' => 'Dear :name, Greetings from Kievdelivery Customer Support. <br><br> We thought you would like to know that status of your order #:id has changed. New Status is :status - it means that you order was cancelled. Information about returns or money refunds if any will be sent to you additionally. <br><br> Thank you for being our customer and giving us the chance to make the delivery of your feelings in Ukraine. <br><br> Do not forget our special 5% discount coupon :coupon next time you do shopping with us.',

];