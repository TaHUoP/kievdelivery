<?php

return [
    'not_in_list' => 'Not in the list? - ' . '<a href=' . route('pages.contacts') . '>contact us</a>',
    'please_enter' => 'Please enter recipient’s city of delivery',
    'search' => 'Please start typing the name of the city of delivery or choose from the list below:',

    'control' => 'city control',
    'content' => 'content',
    'all' => 'All cities',
    'create' => 'Create a record',
    'update' => 'Update record',
    'edit' => 'Edit record',
    'delivery-city' => 'City of Delivery',
    'choose' => 'Please, select delivery city',
    'where-is-it-going' => 'Where is it going? ',
    'close region delivery fee' => 'close region delivery fee',
    'far region delivery fee' => 'far region delivery fee',
    'delivery fee' => 'delivery fee',


    /*
    |--------------------------------------------------------------------------
    | Атрибуты
    |--------------------------------------------------------------------------
    */
    'attr.url' => 'URL',
    'attr.title' => 'Header',
    'attr.visible' => 'Visibility',
    'attr.text' => 'Text',
    'attr.info' => 'Information',
    'attr.meta_title' => 'Meta Title',
    'attr.meta_description' => 'Meta Description',
    'attr.meta_keywords' => 'Meta Keywords',
    'attr.region' => 'Region',
    'attr.name' => 'Name',
    'attr.price-type' => 'Delivery price',
    'attr.is_region_center' => 'Region center',
    'attr.key_words' => 'Key words for search',
];