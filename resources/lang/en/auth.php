<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    // Для Throttle
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login-attempts-amount' => 'You have :times login attempts.',

    'reset-password' => 'Reset Password',
    'reset-password-button' => 'Reset Password',

    // Регистрация
    'attr.name' => 'Name',
    'attr.surname' => 'Lastname',
    'attr.username' => 'Username',
    'attr.password' => 'Password',
    'attr.password-confirmation' => 'Confirm Password',
    'attr.email' => 'Email Address',
    'attr.email-confirmation' => 'Confirm Email Address',
    'attr.phone' => 'Phone / Mobile phone',
    'attr.terms-and-conditions' => 'I agree to Terms & Conditions',
    'attr.g-recaptcha-response' => 'Captcha',
    'register-button' => 'Click to registration',
    'send-reset-link-button' => 'Send Password Reset Link',
];
