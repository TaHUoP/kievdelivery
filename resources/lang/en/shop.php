<?php

return [

    /*
     * Заказ товаров
     */
    'bill-to' => 'Bill to',
    'enter-senders-info' => "enter sender's information",
    'deliver-to' => 'Deliver to',
    'enter-recipients-info' => "enter recipient's information",
    'attr.enter-message-to-recipient' => 'Enter your message to recipient',
    'will-be-on-the-card' => 'Will be printed on the card',
    'attr.your-instroction-to-kievdelivery' => 'Your instructions to KievDelivery',
    'attr.shipment' => 'Shipment',
    'regional-courier-delivery' => 'Regional Courier Delivery',

    'attr.payment' => 'Payment',
    'pay-by' => 'Pay by',
    'credit-card' => 'Credit Card',
    'shopping-cart' => 'Shopping cart',
    'comment' => 'Comment',
    'comment_placeholder' => 'Describe your experience of using the product',
    'not_published' => 'not published' ,

    'order-history' => 'Order history',
    'paid-oreders' => 'Paid orders',
    'order' => 'Order',
    'order-number-from' => 'Order №:number from :date',
    'product-name' => 'Product name',
    'pay-by-credit-card' => 'Pay by Credit Card',
    'your-discount' => 'Your discount',
    'coupon-in-use' => 'Use code:',
    'total-for-order' => 'Total for Order',
    'cart-add-success' => 'This product is successfully added',
    'attr.city-of-delivery' => 'City of delivery',
    'delivery-price' => 'Regional Express Delivery',
    'coupon-delete' => 'Delete coupon',

    'attr.type' => 'Delivery type',
    'attr.post' => 'New post',

    'show-more' => 'Show more',
    'kyiv_only' => 'Kyiv only',

    'paymentSystems' => 'Payment systems',
    'payment_system_allowed' => 'Order Payment Method - Paypal Only '
        . '<a href=' . route('site.page', 'paypal-explanation') . ' target="_blank" style="color: hotpink; text-decoration: underline">(click here for explanation)</a>'
];