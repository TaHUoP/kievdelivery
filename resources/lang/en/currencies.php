<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common
    |--------------------------------------------------------------------------
    */
    'control' => 'Сurrency control',
    'currency' => 'Currency',
    'currencies' => 'Currencies',
    'all' => 'All currencies',
    'create' => 'Create currency',
    'edit' => 'Edit currency',

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    'attr.code' => 'Code',
    'attr.name' => 'Name',
    'attr.exchange_rate' => 'Exchange rate',
    'attr.default' => 'Default',
    'attr.active' => 'Active',

    /*
    |--------------------------------------------------------------------------
    | Messages
    |--------------------------------------------------------------------------
    */
    'msg.exists' => 'Currency with such code already exists!',

];


