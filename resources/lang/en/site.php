<?php

return [
    'home' => 'Go Home',
    'address' => '4 Likhacheva Blv, Suite 42, Kiev, 01042, Ukraine',
    'contacts' => 'Contacts',
    'online_help' => 'Customer Support',

    'footer-copyright :y' => 'Copyright &copy; :y Kievdelivery Group - Flowers and Gifts Delivery in Ukraine.',

    'read_agree' => 'YES, I have',
    'read_agree_continue' => 'read and agree to the Kievdelivery Terms and Conditions of delivery',

    'nav-about' => 'About us',
    'nav-contact' => 'contact',
    'nav-delivery-policy' => 'delivery-policy',
    'nav-review' => 'reviews',
    'nav-faq' => 'faq',
    'nav-return' => 'return',
    'nav-news' => 'news',
    'nav-video-gallery' => 'video-gallery',
    'nav-delivery-photos' => 'delivery-photos',
    'nav-discounts' => 'discounts',
    'nav-agreement' => 'agreement',
    'nav-delivery-cities' => 'delivery-cities',
    'nav-guide' => 'guide',
    'nav-guarantees' => 'guarantees',
    'nav-affiliate' => 'affiliate',
    'nav-service' => 'service',
    'nav-secure-payment' => 'secure-payment',
    'your-comment' => 'Leave your testimonial here',
   
    'login-into-my-office' => 'Log in My Cabinet',
    'coupon-discount' => 'Coupon discount',

    'read-full' => 'Read Full Review',

    'learn_more' => 'Click here for details',

    'public_offer' => 'Public offer',
    'personal_information' => 'Personal information',

    'text_we_service' => 'We service',
    'text_all_ukraine' => 'all Ukraine',

    'best_regards' => 'Best regards',


    'checkout_status_success' => 'Shipped',
    'checkout_status_fail' => 'Cancelled',

    'your-name' => 'Your Full Name',
    'your-email' => 'Your E-mail',
    'your-rating' => 'Your rating',
    'you-want-being-ordered' => 'You want to order',
    'describe-to-order' => 'please describe what you want to send',
    'to-be-delivered-to' => 'To be delivered to',
    'write-info' => 'Please write the NAME, PHONE NUMBER, CITY and ADDRESS of recipient',
    'add-insruct' => 'Additional Instructions',
    'if-any' => 'if any',
    'message-to-recipient' => 'Your Message to Recipient',


    'attr.g-recaptcha-response' => 'Security code',

    'our_company' => 'Our company',
    'customer_support' => 'Customer Support',
    'important_info' => 'Important Info',
    'photo_video' => 'Photos and videos',
    'see_more' => 'see more',

    'title.logo' => 'Kievdelivery - Flowers and Gifts in Ukraine',
    'title.logo-dark' => 'Premium Delivery by Kievdelivery',
    'alt.logo' => 'Kievdelivery - Flowers and Gifts in Ukraine - logo',
    'alt.logo-dark' => 'Premium Delivery by Kievdelivery - logo',
];