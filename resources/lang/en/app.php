<?php

return [

    'Ukraine' => 'Ukraine',
    'date' => 'Date',
    'quantity' => 'Quantity',
    'total' => 'Total',
    'code' => 'Code',
    'contact-with-us' => 'Contact with us',
    'description' => 'Description',
    'price' => 'Price',
    'sum' => 'Sum',
    'profile-info' => 'Profile information',
    'attr.upload-profile-image' => 'Upload profile image',
    'help.file-accepted-formats' => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
    'account-settings' => 'Account settings',
    'apply_code' => 'Apply code',
    'to_order' => 'Checkout',
    'next' => 'Next',
    'previous' => 'Previous',
    'Yes' => 'Yes',
    'No' => 'No',
    'Delivery' => 'Delivery',
    'deleted' => 'Deleted',
    'restore' => 'Restore',
    'cancel' => 'Cancel',
    'restore_success' => 'Data Restoration Successful',

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    'attr.name' => 'Name',
    'attr.surname' => 'Surname',
    'attr.phone' => 'Phone',
    'attr.email' => 'EMAIL',
    'attr.city' => 'City',
    'attr.country' => 'Country',
    'attr.address' => 'Address',

];