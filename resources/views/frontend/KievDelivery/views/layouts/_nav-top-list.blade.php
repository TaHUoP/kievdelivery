<ul class="nav-top-list">
    <li><a href="{{ route('pages.photos') }}" {{ $requestInfo['path'] == 'photos' ? 'class=is-active' : '' }}>{{ trans('site.nav-delivery-photos') }}</a></li>
    <li><a href="{{ route('site.page', ['url' => 'discounts']) }}" {{ $requestInfo['path'] == 'page/discounts' ? 'class=is-active' : '' }}>{{ trans('site.nav-discounts') }}</a></li>
    <li><a href="{{ route('pages.contacts') }}" {{ $requestInfo['path'] == 'contacts' ? 'class=is-active' : '' }}>{{ trans('site.nav-contact') }}</a></li>
    <li><a href="{{ route('pages.videos') }}" {{ $requestInfo['path'] == 'videos' ? 'class=is-active' : '' }}>{{ trans('site.nav-video-gallery') }}</a></li>
    <li><a href="{{ route('review.index') }}" {{ $requestInfo['path'] == 'reviews' ? 'class=is-active' : '' }}>{{ trans('site.nav-review') }}</a></li>
    <li>
        <div class="l-header-search">
            {{ Form::open(['route' => 'product.search', 'method' => 'GET']) }}
            {{ Form::input('search', 's', Request::input('search') ?: null, ['placeholder' => trans('app.search') . '...']) }}
            {{ Form::button('', ['type' => 'submit']) }}
            {{ Form::close() }}
        </div>
    </li>
</ul>