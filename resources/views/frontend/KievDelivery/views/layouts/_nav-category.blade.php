
    @foreach ($categoryControl['all'] as $category)
        <?php
            $class = '';
            if (isset($categoryControl['current']['id'])
                && ($categoryControl['current']['id'] == $category['id'])) {
                $class = 'is-active';
            }
        ?>
        <li class="{{ $class }}">
            <a href="{{ route('shop.category', ['slug' => $category['alias']]) }}">{{ $category['menu_name'] }}</a>
            @if (!empty($category['children']) && $category['children']->count())
                @foreach (collect($category['children'])->chunk(20)->take(3) as $key => $chunked)
                    <ul class="dropdown" style="{{ $key != 0 ? 'left:' . ($key*200+15) . 'px' : '' }};">
                        @foreach ($chunked as $child)
                            <?php
                                $class = '';
                                if ($categoryControl['currentDescendant']['id'] == $child['id']) {
                                    $class = 'is-active';
                                }
                            ?>
                            <li class="category-list-item {{ $class }}">
                                <a href="{{ route('shop.category', ['slug' => $child['alias']]) }}" style="text-transform:none">
                                    {{ Translate::get($child->translations)->menu_name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endforeach
            @endif
        </li>
    @endforeach
    {{--<li{{ $requestInfo['path'] == 'customorder' ? ' class=is-active' : '' }}>--}}
        {{--<a href="{{ route('pages.customorder') }}">{{ trans('site.custom_order') }}</a>--}}
    {{--</li>--}}


