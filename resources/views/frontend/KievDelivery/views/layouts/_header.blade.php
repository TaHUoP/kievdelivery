<header class="l-header">

    <div class="l-header-logo">
        <a href="{{ route('site.index', ['city_alias' => $user_city->alias]) }}">
            <img src="{{ cdn('img/logo.svg') }}" loading="lazy" title="{{ trans('site.title.logo') }}" alt="{{ trans('site.alt.logo') }}"/>
        </a>
        <div>{{ trans('site.text_we_service') }} <span class="l-header-logo-link">{{ trans('site.text_all_ukraine') }}</span></div>
    </div>

    <div class="l-header__search l-mobile-menu-search">
        {{ Form::open(['route' => 'product.search', 'method' => 'GET']) }}
            {{ Form::input('search', 's', Request::input('search') ?: null, ['placeholder' => trans('app.search') . '...']) }}
            {{ Form::button('', ['type' => 'submit']) }}
        {{ Form::close() }}
    </div>

    <div class="l-header-city b-l-header-city">
        <p>{{ trans('cities.delivery-city') }}</p>

        <form id="city-form" method="POST" action="{{ route('city.update') }}">
            <img src="{{ cdn('img/location.svg') }}" loading="lazy" alt="location sign" title="location sign"/>
            {{ csrf_field() }}
            <select name='city_id' id="city-select" class="select-city ">
                @foreach($cities as $city)
                    <option data-keywords="{{ Translate::get($city->translations, 'key_words') }}" value="{{ $city->id }}" {{ $user_city->alias == $city->alias ? 'selected' : '' }}>{{ $city->with_region_and_fee }}</option>
                @endforeach
            </select>
        </form>
        <p>{!! trans('cities.not_in_list') !!}</p>
    </div>

    <ul class="l-header-nav">

        @if (AuthHelper::isAccess())
            <li>
                <a href="{{ route('user.index') }}">
                    <span class="icon-user"></span>
                    <span>{{ trans('users.my-account') }}</span>
                </a>
            </li>
        @else
            <li>
                <a href="{{ route('guest.signin') }}">
                    <span class="icon-user"></span>
                    <span>{{ trans('users.signin') }}</span>
                </a>
            </li>
        @endif

        <li>
            <a href="{{ route('pages.contacts') }}">
                <span class="icon-support"></span>
                <span>{{ trans('site.online_help') }}</span>
            </a>
        </li>
    </ul>

    <div class="cart-mini">
        @include('shop.cart._cart-mini')
    </div>

</header>