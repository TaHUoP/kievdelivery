{{-- ----------------------------------------------------------- --}}
<nav id="menu" class="my-mmenu">
    <ul>
        @include('layouts._nav-category')
        <li class="mmenu-top-list mmenu-driver">
            <a href="{{ route('pages.photos') }}" {{ $requestInfo['path'] == 'photos' ? 'class=is-active' : '' }}>{{ trans('site.nav-delivery-photos') }}</a>
            <a href="{{ route('site.page', ['url' => 'discounts']) }}" {{ $requestInfo['path'] == 'page/discounts' ? 'class=is-active' : '' }}>{{ trans('site.nav-discounts') }}</a>
            <a href="{{ route('pages.contacts') }}" {{ $requestInfo['path'] == 'contacts' ? 'class=is-active' : '' }}>{{ trans('site.nav-contact') }}</a>
            <a href="{{ route('pages.videos') }}" {{ $requestInfo['path'] == 'videos' ? 'class=is-active' : '' }}>{{ trans('site.nav-video-gallery') }}</a>
            <a href="{{ route('review.index') }}" {{ $requestInfo['path'] == 'reviews' ? 'class=is-active' : '' }}>{{ trans('site.nav-review') }}</a>
        </li>
        <li class="mmenu-buttons mmenu-driver">
            @if (AuthHelper::isAccess())
                <a href="{{ route('user.index') }}" class="list-link--with-icon"><span class="icon-user"></span><span>{{ trans('users.my-account') }}</span></a>
            @else
                <a href="{{ route('guest.signin') }}" class="list-link--with-icon"><span class="icon-user"></span><span>{{ trans('users.signin') }}</span></a>
            @endif
        </li>
    </ul>
</nav>

{{-- -------------  scripts  ---------------- --}}
@push('scripts')
<script src="{{ cdn('js/jquery.mmenu.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('nav#menu').mmenu()
    });
</script>
@endpush
{{-- ----------------------------------------------------------- --}}

{{-- <div class="l-mobile-menu" style="display: none">

    <div class="l-mobile-menu-wrapper">

        <ul class="l-mobile-menu-nav">
            <li><a href="/"><span class="icon-home"></span><span>{{ trans('app.home') }}</span></a></li>
        </ul>

        <div class="l-mobile-menu-search">
            {{ Form::open(['route' => 'product.search', 'method' => 'GET']) }}
                {{ Form::input('search', 's', Request::input('search') ?: null, ['placeholder' => trans('app.search') . '...']) }}
                {{ Form::button('', ['type' => 'submit']) }}
            {{ Form::close() }}
        </div>

        @include('shop.cart._cart-mini')

        <div class="l-mobile-menu-title">{{ trans('shop.category') }}</div>

        <ul id="menuCategory" class="nav-horizontal nav-horizontal-warning">
            @include('layouts._nav-category')
        </ul>

        <div class="l-mobile-menu-title">{{ trans('shop.filter.all') }}</div>
        @include('layouts._nav-filter')

        <ul class="l-mobile-menu-nav">
            @if (AuthHelper::isAccess())
                <li><a href="{{ route('user.index') }}"><span class="icon-user"></span><span>{{ trans('users.my-account') }}</span></a></li>
            @else
                <li><a href="{{ route('guest.signin') }}"><span class="icon-user"></span><span>{{ trans('users.signin') }}</span></a></li>
            @endif
        </ul>

        <div class="cart-mini">
            @include('layouts._nav-top-list')
        </div>

    </div>

</div> --}}
