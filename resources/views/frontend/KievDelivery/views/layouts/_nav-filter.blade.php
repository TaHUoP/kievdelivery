<ul class="nav-horizontal price-filter">
    <li{{ $requestInfo['path'] == 'products/price_0-40' || Request::input('filter', '') == 'price_0-40' ? ' class=is-active' : '' }}>
        <a href="{{ route('site.index') }}" data-price_range="0-40">
            {{ trans('site.filter-price-range-1', ['price' => Currency::getMoneyFormat(40)]) }}
        </a>
    </li>
    <li{{ $requestInfo['path'] == 'products/price_40-80' || Request::input('filter', '') == 'price_40-80' ? ' class=is-active' : '' }}>
        <a href="{{ route('site.index') }}" data-price_range="40-80">
            {{ trans('site.filter-price-range-2', ['price_from' => Currency::getMoneyFormat(40), 'price_to' => Currency::getMoneyFormat(80)]) }}
        </a>
    </li>
    <li{{ $requestInfo['path'] == 'products/price_80-120' || Request::input('filter', '') == 'price_80-120' ? ' class=is-active' : '' }}>
        <a href="{{ route('site.index') }}" data-price_range="80-120">
            {{ trans('site.filter-price-range-3', ['price_from' => Currency::getMoneyFormat(80), 'price_to' => Currency::getMoneyFormat(120)]) }}
        </a>
    </li>
    <li{{ $requestInfo['path'] == 'products/price_120' || Request::input('filter', '') == 'price_120' ? ' class=is-active' : '' }}>
        <a href="{{ route('site.index') }}" data-price_range="120">
            {{ trans('site.filter-price-range-4', ['price' => Currency::getMoneyFormat(120)]) }}
        </a>
    </li>
</ul>