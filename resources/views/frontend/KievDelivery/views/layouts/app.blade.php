<?php
/*
$LastModified_unix = 1294844676; // время последнего изменения страницы
$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
<?php
/*
$LastModified_unix = 1294844676; // время последнего изменения страницы
$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
$IfModifiedSince = false;
if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
    exit;
}
header('Last-Modified: '. $LastModified);


*/?>
<!DOCTYPE html >
<html lang="{{ $languageControl['current']['locale'] }}">
<head>
    @if(preg_match('#/signin|/signup|/password/reset|/products\?search=|/customorder|/cart|/checkout|/page/public_offer|/page/nodelivery#', $_SERVER['REQUEST_URI']))
        <meta name="robots" content="noindex, nofollow"/>
    @endif
    <meta charset="utf-8">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N2FRT6');</script>
    <!-- End Google Tag Manager -->

    @if(!empty($has_city))
        <title>{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : Translate::get($user_city->translations, 'meta_title') }}</title>
        <meta name="description" content="{{ Translate::get($user_city->translations, 'meta_description') }}">
        <meta name="keywords" content="{{ Translate::get($user_city->translations, 'meta_keywords') }}">
        <meta name="application-name" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : Translate::get($user_city->translations, 'meta_title') }}">
        <meta name="apple-mobile-web-app-title" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getDescription() : Translate::get($user_city->translations, 'meta_title') }}">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@kievdelivery">
        <meta name="twitter:creator" content="@kievdelivery" />
        <meta property="og:title" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : Translate::get($user_city->translations, 'meta_title') }}" />
        <meta property="og:description"  content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : Translate::get($user_city->translations, 'meta_description') }}" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://kievdelivery.com/" />
        <meta property="og:image" content="https://kievdelivery.com/upload/products/289/thumbs/5883eb3761a6a2.63041165.jpeg" />
    @elseif (!empty($seoSpecifications))
        <title>{{  \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() :  $seoSpecifications['meta_title'] }}</title>
        <meta name="description" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getDescription() : $seoSpecifications['meta_description'] }}">
        <meta name="keywords" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getKeywords() : $seoSpecifications['meta_keywords'] }}">
        <meta name="application-name" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : $seoSpecifications['meta_title'] }}">
        <meta name="apple-mobile-web-app-title" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getDescription() : $seoSpecifications['meta_title'] }}">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@kievdelivery">
        <meta name="twitter:creator" content="@kievdelivery" />
        <meta property="og:title" content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : Translate::get($user_city->translations, 'meta_title') }}" />
        <meta property="og:description"  content="{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getDescription() : Translate::get($user_city->translations, 'meta_description') }}" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://kievdelivery.com/" />
        <meta property="og:image" content="https://kievdelivery.com/upload/products/289/thumbs/5883eb3761a6a2.63041165.jpeg" />
        {{--<link rel="canonical" href="https://kievdelivery.com/{{$canonical}}" />--}}
    @else
        @yield('seo-info')
        <meta name="application-name" content="{{\App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getTitle() : "Kiev Delivery"}}">
        <meta name="apple-mobile-web-app-title" content="{{\App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getDescription() : "Kiev Delivery"}}">
    @endif
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="twitter:image" content="https://kievdelivery.com/upload/products/289/thumbs/5883eb3761a6a2.63041165.jpeg" />
        <meta name="twitter:image:alt" content="Kievdelivery - Flowers in Ukraine" />
        <meta name="twitter:title" content=”Kievdelivery - Flowers and Gifts Delivery in Ukraine” />
        <meta name="twitter:description" content="Send flowers in Ukraine. Buy fresh flowers, gifts, food baskets, roses bouquets for delivery in Ukraine from local florists. Verified hand delivery, fast secure order." />
        <meta property="og:site_name" content="Kievdelivery - Flowers and Gifts in Ukraine" />

    @yield('canonical_links', "<!--No canonical links-->")

    @yield('rel_publisher', "")


    <meta name="yandex-verification" content="df6600e0a2ffd42a"/>

    <meta name="google-site-verification" content="8JamxnwqLidMfQmpjQvuOw3sslE-VNVhw8_1HvhbyR0"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="{{ cdn('img/apple-touch-icon-precomposed.png') }}">

    <meta name="msapplication-TileImage" content="/favicon.ico">
    <meta name="msapplication-TileColor" content="#fff">
    <meta name="theme-color" content="#43C900">

    <link rel="icon" sizes="192x192" href="{{ cdn('img/chrome-touch-icon-192x192.png') }}">
    <link rel="icon" type="image/png" href="{{ cdn('img/favicon.png') }}">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">
    <meta name="_token" content="{{ csrf_token() }}">
    <link href='https://fonts.googleapis.com/css?family=Material+Icons%7CRoboto:400,700italic,500,700,400italic,300italic,300&amp;subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ cdn('css/styles.min.css') }}?v=234">
    <link rel="stylesheet" href="{{ cdn('css/jquery.mmenu.css') }}">
        @stack('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    @if (getenv('APP_ENV') === 'production')
        <style>
            #zcwMiniButton #zcwMiniButtonMain {
                box-shadow: 0 0 4px 4px rgba(0, 0, 0, 0.2), 0 0 3px 2px rgba(0, 0, 0, 0), 0 0 0 0 rgba(0, 0, 0, 1) !important;
            }

            .sprite {
                background-image: url({{ cdn('img/socials-sprite.png') }});
                background-repeat: no-repeat;
                display: block;
            }

            .sprite-facebook {
                width: 30px;
                height: 30px;
                background-position: -5px -5px;
            }

            .sprite-instagram {
                width: 30px;
                height: 30px;
                background-position: -45px -5px;
            }

            .sprite-twitter {
                width: 30px;
                height: 30px;
                background-position: -85px -5px;
            }

            .sprite-youtube {
                width: 30px;
                height: 30px;
                background-position: -125px -5px;
            }

            .security-sprite {
                background-image: url({{ cdn('img/security-sprite.png') }});
                background-repeat: no-repeat;
                display: block;
            }

            .security-sprite-googlecheck {
                width: 128px;
                height: 25px;
                background-position: -5px -5px;
            }

            .security-sprite-mcaffe {
                width: 67px;
                height: 25px;
                background-position: -143px -5px;
            }

            .security-sprite-pci-dss {
                width: 64px;
                height: 25px;
                background-position: -220px -5px;
            }

            /* ======================================================================= */
            /* andrew styles */
            /* ======================================================================= */
            .footer-accordion__checkbox + span,
            .footer-accordion__checkbox,
            .my-mmenu,
            .nav-top__icon-links,
            .l-header__search,
            .add-to-cart__icon {
                display: none;
            }

            /* sign in */
            .sign-in__form-wrapp .form-horizontal .control-label {
                width: 100%;
            }

            .zopim {
                right: 10px !important;
                bottom: 10px !important;
            }

            .page__container {
                overflow-x: hidden;
            }

            /* ======================================================================= */
            /* max-width: 544px */
            /* ======================================================================= */
            @media (max-width: 544px) {
                .add-to-cart__text {
                    display: none;
                }

                .add-to-cart__icon {
                    display: block;
                }

                .add-to-cart__icon::after {
                    content: "add_shopping_cart";
                    text-transform: none;
                    -webkit-font-feature-settings: liga;
                    -moz-font-feature-settings: liga;
                    font-family: 'Material Icons';
                    line-height: 1;
                    letter-spacing: normal;
                    white-space: nowrap;
                    font-style: normal;
                    word-wrap: normal;
                    direction: ltr;
                    -webkit-font-smoothing: antialiased;
                    text-rendering: optimizeLegibility;
                    color: #fff;
                    font-size: 24px;
                }

                .grid-products .product-with-note {
                    padding: 0;
                    width: 50%;
                }
                
                .product-short-preview {
                    height: 200px;
                }

                .product-short-preview img {
                    object-fit: contain;
                }

                .product-short-name {
                    font-size: 1rem;
                }
                .product-short-price strike,
                .product-short-price {
                    font-size: 1.6rem;
                }
                
                .add-to-cart__btn {
                    padding-top: 4px;
                    padding-bottom: 4px;
                    margin-left: 0 !important;
                }

                .add-to-cart__btn:hover .add-to-cart__icon::after {
                    color: #d80047;
                }

                html[lang="uk"] .product-short__more-btn,
                html[lang="ru"] .product-short__more-btn {
                    padding: 13px 7px;
                    font-size: 13px;
                }

                html[lang="uk"] .add-to-cart__btn,
                html[lang="ru"] .add-to-cart__btn {
                    padding: 4px 10px;
                }

                .l-seo__block {
                    margin-left: -15px;
                    margin-right: -15px;
                }

                .l-seo__block .l-seo-text {
                    margin-left: 0;
                    margin-right: 0;
                }

                .l-header {
                    min-height: 0;
                }

                /* search */                
                .l-header__search {
                    display: block;
                    margin-top: 15px;
                    padding-bottom: 0;
                }
                .l-header__search form {
                    display: flex;
                    align-items: center;
                    height: 34px;
                    position: relative;
                    background: #f7f7f7;
                    border: 1px solid #7e7e7e;
                }

                .l-header__search input {
                    height: 34px;
                    font-size: 16px;
                    border: none !important;
                    background: none !important;
                }

                .l-header__search button {
                    top: 50%;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 34px !important;
                    width: 34px !important;
                    position: initial !important;
                }
                
                .l-header__search button[type=submit]:after {
                    font-size: 21px;
                }

                .b-l-header-city {
                    display: none !important;
                }

                .l-product-present-view .gallery-top .swiper-slide img {
                    width: 100%;
                    object-fit: contain;
                }

                .l-product-present-price {
                    font-size: 34px;
                }

                .product-card__cod-and-rating {
                    flex-direction: row !important;
                    justify-content: space-between;
                }
            }

            /* ======================================================================= */
            /* max-width: 991.98px */
            /* ======================================================================= */
            @media (max-width: 991.98px) {
                body {
                    padding-top: 0;  
                }
                .nav-top {
                    position: sticky;
                    font-size: 17px;
                }

                /* hamburger */
                .hamburger {
                    background-color: transparent;
                    border: 0;
                    color: inherit;
                    cursor: pointer;
                    display: inline-block;
                    font: inherit;
                    margin: 0;
                    overflow: visible;
                    padding: 10px;
                    text-transform: none;
                    transition-delay: .3s;
                    transition-duration: .15s;
                    transition-property: opacity, filter, -webkit-filter;
                    transition-property: opacity, filter;
                    transition-property: opacity, -webkit-filter;
                    transition-timing-function: linear;
                    /*  */
                    padding: 0px 10px;
                    margin: 0;
                    border: none;
                    height: 40px;
                    display: flex;
                    align-items: center;
                    margin-right: 5px;
                    margin-left: 5px;
                }

                /* .hamburger:hover { */
                    /* opacity: .8 */
                /* } */

                .hamburger-box {
                    display: inline-block;
                    height: 24px;
                    position: relative;
                    width: 30px
                }

                .hamburger-inner {
                    display: block;
                    margin-top: -2px;
                    top: 50%
                }

                .hamburger-inner,
                .hamburger-inner::after,
                .hamburger-inner::before {
                    background-color: #ffffff;
                    border-radius: 2px;
                    height: 5px;
                    position: absolute;
                    transition-duration: .15s;
                    transition-property: transform;
                    transition-timing-function: ease;
                    width: 30px
                }

                .hamburger-inner::after,
                .hamburger-inner::before {
                    content: "";
                    display: block
                }

                .hamburger-inner::before {
                    top: -10px
                }

                .hamburger-inner::after {
                    bottom: -10px
                }

                .hamburger--collapse .hamburger-inner {
                    bottom: 0;
                    top: auto;
                    transition-delay: .15s;
                    transition-duration: .15s;
                    transition-timing-function: cubic-bezier(.55, .055, .675, .19)
                }

                .hamburger--collapse .hamburger-inner::after {
                    top: -20px;
                    transition: top .3s .3s cubic-bezier(.33333, .66667, .66667, 1), opacity .1s linear
                }

                .hamburger--collapse .hamburger-inner::before {
                    transition: top .12s .3s cubic-bezier(.33333, .66667, .66667, 1), transform .15s cubic-bezier(.55, .055, .675, .19)
                }

                .mm-wrapper_opening .hamburger--collapse .hamburger-inner {
                    transform: translate3d(0, -10px, 0) rotate(-45deg);
                    transition-delay: .32s;
                    transition-timing-function: cubic-bezier(.215, .61, .355, 1)
                }

                .mm-wrapper_opening .hamburger--collapse .hamburger-inner::after {
                    opacity: 0;
                    top: 0;
                    transition: top .3s cubic-bezier(.33333, 0, .66667, .33333), opacity .1s .27s linear
                }

                .mm-wrapper_opening .hamburger--collapse .hamburger-inner::before {
                    ms-transform: rotate(-90deg);
                    top: 0;
                    transform: rotate(-90deg);
                    transition: top .12s .18s cubic-bezier(.33333, 0, .66667, .33333), transform .15s .42s cubic-bezier(.215, .61, .355, 1)
                }

                .mm-wrapper_opening .hamburger--arrow .hamburger-inner::before {
                    transform: translate3d(-8px, 0, 0) rotate(-45deg) scale(.7, 1)
                }

                .mm-wrapper_opening .hamburger--arrow .hamburger-inner::after {
                    transform: translate3d(-8px, 0, 0) rotate(45deg) scale(.7, 1)
                }

                .mm-wrapper_opening .hamburger--arrow-r .hamburger-inner::before {
                    transform: translate3d(8px, 0, 0) rotate(45deg) scale(.7, 1)
                }

                .mm-wrapper_opening .hamburger--arrow-r .hamburger-inner::after {
                    transform: translate3d(8px, 0, 0) rotate(-45deg) scale(.7, 1)
                }
                
                /* ======================================================================= */
                /* mmenu
                /* ======================================================================= */
                .mm-menu {
                    background: url({{ cdn('img/footer-bg.jpg') }}) center center no-repeat;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    color: #000;
                }

                .mmenu-top-list {
                    margin-top: 20px;
                    font-size: 14px;
                    padding-bottom: 20px;
                }

                .mmenu-buttons {
                    margin-top: 20px;
                }

                .mm-listitem {
                    color: #182a38;
                }

                .mm-listitem a {
                    color: #182a38;
                    font-weight: bold;
                }

                .mmenu-top-list a, 
                .mmenu-buttons a {
                    padding: 8px 10px 8px 20px;
                    font-weight: normal
                }

                .mmenu-buttons::after {
                    display: none;
                }
                
                .list-link--with-icon {
                    display: flex !important;
                    align-items: center;
                }

                .list-link--with-icon [class^="icon-"] {
                    color: #3f9913;
                    font-size: 21px;
                    display: flex;
                    margin-right: 5px;
                }

                .nav-top .nav-mobile {
                    display: flex;
                    align-items: center;
                }

                .nav-dropdown {
                    position: relative;
                    padding-right: 20px;
                }

                .nav-dropdown::after {
                    content: '';
                    position: absolute;
                    right: 4px;
                    top: 50%;
                    width: 0;
                    height: 0;
                    border-style: solid;
                    border-width: 5px 5px 0 5px;
                    border-color: #ffffff transparent transparent transparent;
                    transform: translateY(-50%);
                }

                .nav-dropdown:hover::after {
                    border-color: #43c900 transparent transparent transparent;
                    transform: translateY(-50%) rotateX(180deg);
                }

                .sidebar-left {
                    display: none;
                }

                .sidebar-container.opened .sidebar-left {
                    display: block;  
                }

                .mm-panel {
                    overflow: auto;
                }

                .mm-panels>.mm-panel>.mm-listview {
                    margin-bottom: 0;
                }
                
                .nav-top__icon-links {
                    display: flex;
                    align-items: center;
                    margin-left: 15px; 
                }

                .nav-top__icon-links [class^="icon-"] {
                    display: flex;
                    align-items: center;
                    color: #ffffff;
                    font-size: 25px;
                    margin-right: 15px; 
                }

                .nav-top__icon-links .icon-support {
                    transform: rotateY(180deg);   
                }

                .cart-mob {
                    display: flex;
                    align-items: center;
                }
                .cart-mob__icon {
                    margin-right: 15px;
                    color: #ffffff;
                    font-size: 25px;
                    -webkit-transition: color .3s;
                    -o-transition: color .3s;
                    -moz-transition: color .3s;
                    transition: color .3s;
                    display: flex;
                    align-items: center;
                    margin-left: 10px;
                }

                .cart-mob__icon::after {
                    font-weight: 400;
                    display: inline-block;
                    width: 1em;
                    height: 1em;
                    content: "shopping_cart";
                    text-transform: none;
                    -webkit-font-feature-settings: liga;
                    -moz-font-feature-settings: liga;
                    font-family: 'Material Icons';
                    line-height: 1;
                    letter-spacing: normal;
                    white-space: nowrap;
                    font-style: normal;
                    word-wrap: normal;
                    direction: ltr;
                    -webkit-font-smoothing: antialiased;
                    text-rendering: optimizeLegibility;
                }
                .cart-mob__price {
                    font-size: 16px;
                }

                .cart-mob__icon {
                    position: relative;
                }
                .cart-mob__has-product::before {
                    content: '';
                    position: absolute;
                    right: -2px;
                    top: -2px;
                    display: block;
                    width: 10px;
                    height: 10px;
                    background-color: #43c900;
                    border-radius: 50%;
                }

                /* ======================================================================= */
                /* footer
                /* ======================================================================= */
                .footer-accordion__body {
                    max-height: 0;
                    overflow: hidden;
                    transition: all 0.25s ease-in-out;
                }

                .checkbox.selected ~ .footer-accordion__body {
                    max-height: 250px; 
                    padding-bottom: 20px;
                }

                .footer-accordion__label {
                    position:  relative;
                    width: 100%;
                    transition: all 0.25s ease-in-out;
                    padding: 10px 0;
                }

                .l-footer-nav:not(:first-of-type) {
                    border-top: solid 1px rgb(85, 85, 85, 0.2);
                }

                .l-footer-container .l-footer-nav:last-child {
                    margin-bottom: 30px;
                }
                
                .footer-accordion__curve {
                    position: absolute;
                    transform: translate(-6px, -50%);
                    width: 10px;
                    height: 10px;
                    right: 0;
                    top: 50%;
                    transition: all 0.25s ease-in-out;
                }

                .footer-accordion__curve::before, 
                .footer-accordion__curve::after {
                    content: "";
                    position: absolute;
                    background-color: #ff6873;
                    width: 3px;
                    height: 9px;
                    transition: all 0.25s ease-in-out;
                }

                .footer-accordion__curve::before {
                    transform: translate(2px, 0) rotate(45deg);
                    
                }
                .footer-accordion__curve::after {
                    transform: translate(-2px, 0) rotate(-45deg);
                }

                .checkbox.selected + .footer-accordion__label .footer-accordion__curve::before {
                    transform: translate(-2px, 0) rotate(45deg);
                }
                .checkbox.selected + .footer-accordion__label .footer-accordion__curve::after {
                    transform: translate(2px, 0) rotate(-45deg);
                }

                .l-footer-nav {
                    width: 100%;
                    max-width: 260px;
                    margin: 0 auto;
                }

                .footer-accordion____container {
                    display: flex;
                    flex-direction: column;
                }

                /* ======================================================================= */
                /* .breadcrumbs
                /* ======================================================================= */
                .breadcrumbs {
                    display: flex;
                    flex-direction: row;
                }

                .breadcrumbs li {
                    padding: 0;
                    padding-right: 7px;
                }

                .breadcrumbs li:not(:last-of-type)::after {
                    content: ' / ';
                }

                .breadcrumbs li.active {
                    color: #182a38;
                }

                /* .sign-in */
                .sign-in__form-wrapp .fast-login-settings {
                    flex-direction: row-reverse;
                    justify-content: space-between;
                }

                .sign-in__form-wrapp .fast-login-settings-list {
                    padding: 0;
                }

                .sign-in__form-wrapp .fast-login {
                    max-width: 500px;
                    width: 100%;
                }

                .larger-image-block {
                    z-index: 9999;
                }

                .nav-top-lang-mobile {
                    margin-left: 0;
                }

                .nav-top-curr-mobile {
                    margin-right: auto;
                }

            }
            
            /* ======================================================================= */
            /* min-width: 992px
            /* ======================================================================= */
            @media (min-width: 992px) {
                .nav-horizontal li .dropdown li {
                    height:35px;
                    border-bottom: 1px solid white;
                    border-right: 1px solid white
                }

                .nav-horizontal li .dropdown {
                    width: 200px; 
                    text-align: left;
                    bottom: 0;

                    display: flex;
                    flex-direction: column;
                    flex-wrap: wrap;
                    height: 210px;
                    background: transparent;
                }

                .l-footer-nav-title {
                    text-align: center;
                    margin-left: auto;
                    margin-right: auto;
                    color: #1e2e3c;
                    letter-spacing: 0.5px;
                }

                .footer-accordion__body {
                    margin-left: auto;
                    margin-right: auto;
                }
            }
            /* ======================================================================= */
            /* END andrew styles */
            /* ======================================================================= */

            @if (request()->route()->getName() == 'shop.category')
                #back-top {
                    position: fixed;
                    bottom: 15%;
                    right: 3%;
                }

                #back-top a {
                    background-image: url({{ cdn('img/top.png') }});
                    background-size: cover;
                    width: 60px;
                    height: 60px;
                    display: block;
                    margin-bottom: 7px;
                }
            @endif

            @if (request()->route()->getName() == 'shop.product')
                .larger-image-block {
                    display: none;
                    background-color: rgba(0, 0, 0, 0.7);
                    height: 100%;
                    position: fixed;
                    width: 100%;
                    z-index: 3;
                }

                .larger-image {
                    position: absolute;
                    background-color: white;
                    margin: 20px auto;
                    left: 26%;
                    /*top: 5%;*/
                    width: 45%;
                }

                .larger-image img {
                    width: 100%;
                }

                @media screen and (max-width: 768px) {
                    .larger-image {
                        width: 95%;
                        left: 2%;
                    }
                }
            @endif
        </style>
    <script type='application/ld+json'>
                {
                    "@context":"http://www.schema.org",
                    "@type":"LocalBusiness",
                    "name":"Kievdelivery",
                    "legalName":"Kievdelivery",
                    "image":"{{ url('/') }}/statics/frontend/KievDelivery/img/logo.svg",
                    "logo":"{{ url('/') }}/statics/frontend/KievDelivery/img/logo.svg","url":"{{ url('/') }}",
                    "email":"contact@kievdelivery.com",
                    "sameAs": ["http://www.twitter.com/kievdelivery"],
                    "address": {
                        "@type":"PostalAddress",
                        "addressLocality":"Suite 37, Verkhovnoi Rady Blvd, 21b",
                        "addressRegion":"Kyiv",
                        "postalCode":"02192",
                        "addressCountry":"Ukraine"
                    },
                    "geo": {
                        "@type":"GeoCoordinates",
                        "latitude":"50.450292",
                        "longitude":"30.622838"
                    },
                    "contactPoint":[
                    {
                        "@type":"ContactPoint",
                        "telephone":"+1(980) 7295075",
                        "contactType":"customer service",
                        "areaServed":"US"
                    },{
                        "@type":"ContactPoint",
                        "telephone":"+38 (067) 7088480",
                        "contactType":"customer service",
                        "areaServed":"UA"
                    }],
                    "openingHours":"Mo, Tu, We, Th, Fr, Sa, Su 09:30-21:30",
                    "priceRange": "$$",
                    "telephone": "+1(980) 7295075"
                }
            </script>

    @endif
    @foreach($languageControl['all'] as $lang)
        {{--@if($languageControl['current'] != $lang)--}}
        <link rel="alternate" hreflang="{{ $lang['locale'] }}" href="{{ url($lang['path']) }}"/>
        {{--@endif--}}
    @endforeach

</head>
<body>
    <div id="page">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2FRT6"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        @if (request()->route()->getName() == 'shop.product')
            <div class="larger-image-block"><div class="larger-image"></div></div>
        @endif
        @if (!empty($seoSpecifications))
            <h1 style="display: none">{{ $seoSpecifications['title'] }}</h1>
        @endif
        <div style="display: none;"><?PHP
            $value = Cookie::get('frontend_language');
            print_r($value)
            ?></div>
        @include('layouts._top')

        @include('layouts._nav-mobile')

        <div class="container page__container" id="container">

            @if(Session::has('danger'))
                <div class="alert alert-danger">
                    {{ Session::get('danger') }}
                </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            @include('layouts._header')

            <ul id="menuCategory" class="nav-horizontal nav-horizontal-warning">
                @include('layouts._nav-category')
            </ul>

            @if ($requestInfo['route'] == 'shop.products' || $requestInfo['route'] == 'shop.category')
        {{--        @yield('nav-filter')--}}
            @endif

            @yield('breadcrumbs')

            @yield('extra-container')

            <main class="content">
                @yield('content')
            </main>

            @include('layouts._footer')

        </div>
    </div>

<script src="{{ cdn('js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ cdn('js_dev/city-select.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
<script src="{{ cdn('js/bootstrap-notify.min.js') }}"></script>


<script>
    var jsData = {
        messages: {
            cart_add_success: '{{ trans('shop.cart-add-success') }}',
            coupon_no_discounts: '{{ trans('shop.coupons.no_discounts') }}',
            critical_error: '{{ trans('app.critical_error') }}',
        }
    };
</script>
<script>
    var currentLang = '{{ $languageControl['current']->locale }}';
</script>
@stack('scripts')

@if (getenv('APP_ENV') === 'production')
    {{--<script src="{{ cdn('js/app.min.js') }}"></script>--}}
    {{--<script src="{{ cdn('js/cart.min.js') }}"></script>--}}
    <script src="{{ cdn('js_dev/app.js') }}"></script>
    <script src="{{ cdn('js_dev/cart.js?v=1') }}"></script>

{{--    <script>--}}
{{--        (function (i, s, o, g, r, a, m) {--}}
{{--            i['GoogleAnalyticsObject'] = r;--}}
{{--            i[r] = i[r] || function () {--}}
{{--                    (i[r].q = i[r].q || []).push(arguments)--}}
{{--                }, i[r].l = 1 * new Date();--}}
{{--            a = s.createElement(o),--}}
{{--                m = s.getElementsByTagName(o)[0];--}}
{{--            a.async = 1;--}}
{{--            a.src = g;--}}
{{--            m.parentNode.insertBefore(a, m)--}}
{{--        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');--}}

{{--        ga('create', 'UA-35584596-1', 'auto');--}}
{{--        ga('send', 'pageview');--}}
{{--    </script>--}}
@else

    <script src="{{ cdn('js_dev/app.js') }}"></script>
    <script src="{{ cdn('js_dev/cart.js?v=1') }}"></script>

@endif

@if (request()->route()->getName() !== 'site.index')
    <!--Start of Zendesk Chat Script-->
    <script>
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?WJuuEynI5dgh1CLJcUCWx22FYHzYNEKH";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <!--End of Zendesk Chat Script-->
@endif

@if (request()->route()->getName() == 'shop.category')
    <p id="back-top"><a href="#top"><span></span></a></p>
    <script>
        $(document).ready(function(){
            $("#back-top").hide();
            $(function () {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 600) {
                        $('#back-top').fadeIn();
                    } else {
                        $('#back-top').fadeOut();
                    }
                });
                $('#back-top a').click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 400);
                    return false;
                });
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            // open
            $('.sidebar-container .sidebar-toggle').click(function(){
                container = $(this).closest('.sidebar-container');
                container.toggleClass('opened');
                $('body').toggleClass('hide-scroll');
            });
            // close
            $('.sidebar-container .sidebar-toggle-hide, .sidebar-container .sidebar-overlay').click(function(){
                container = $(this).closest('.sidebar-container');
                container.removeClass('opened');
                $('body').removeClass('hide-scroll');
            });
            $(".catalogpage__podbor__speciflabel-cover").on("click", function () {
                let boxes = $("input:checkbox:not(:checked)", this)
                let boxesChecked = $("input:checkbox:checked", this)
                boxes.each(function(){
                    if (boxesChecked.length === 1) {
                        $(this).attr('disabled', 'disabled');
                    } else {
                        $(this).removeAttr('disabled');
                    }
                });
            })
            $(".catalogpage__podbor__speciflabel-cover input").on("click", function () {
                let parentUrl = $("#kd-btn-filter-search").data('parent-url');
                let cityAlias = $("#kd-btn-filter-search").data('city-alias');
                if( parentUrl.slice(-1) === '/' ){
                    parentUrl = parentUrl.substring(0, str.length - 1);
                }
                let isFilter = false;
                $("#podbor-catalogview-cover input:checked").each(function(){
                    parentUrl += "/" + $(this).val();
                    isFilter = true;
                });
                if (isFilter === true && cityAlias) {
                    parentUrl += "/" + cityAlias
                }
                location.href = parentUrl;
            });
        });
    </script>
@endif
</body>
</html>
