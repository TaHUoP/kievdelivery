<div class="nav-top open">
    <div class="container base">

        <div class="nav-mobile">
            <span id="hamburger">
                <a href="#menu" class="hamburger hamburger--collapse">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </a>
            </span>
{{--            <a href="{{ route('shop.cart') }}" class="nav-mobile-info ">{{ trans('shop.cart-total-price :price', ['price' => $cartControl['totalPriceString']]) }}</a>--}}
        </div>

        @include('layouts._nav-top-list')

        <ul class="nav-top-lang">
            @foreach($languageControl['all'] as $lang)
                <li>
                    @if ($languageControl['current']['id'] == $lang['id'])
                        <a rel="alternate" href="{{ url($lang['path']) }}" class="is-active">{{ $lang['short_name'] }}</a>
                    @else
                        <a rel="alternate" href="{{ url($lang['path']) }}">{{ $lang['short_name'] }}</a>
                    @endif
                </li>
                @if ($lang != $languageControl['all']->last())
                    <li>|</li>
                @endif
            @endforeach
        </ul>

        <ul class="nav-top-curr">
            @foreach($currencies['all'] as $currency)
                <li>
                    @if ($currency['code'] == $currencies['current'])
                        <a href="{{ route('site.set-curr', ['currency' => $currency['code']]) }}" class="is-active">{{ $currency['code'] }}</a>
                    @else
                        <a href="{{ route('site.set-curr', ['currency' => $currency['code']]) }}">{{ $currency['code'] }}</a>
                    @endif
                </li>
                @if ($currency != end($currencies['all']))
                    <li>|</li>
                @endif
            @endforeach
        </ul>

        <div class="nav-top-lang-mobile nav-dropdown">
            @foreach($languageControl['all'] as $lang)
                @if($languageControl['current']['id'] == $lang['id'])
                    <span>{{ $lang['short_name'] }}</span>
                @endif
            @endforeach
            <ul>
                @foreach($languageControl['all'] as $lang)
                    @if($languageControl['current']['id'] != $lang['id'])
                        <li><a rel="alternate" href="{{ url($lang['path']) }}">{{ $lang['short_name'] }}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>

        <div class="nav-top-curr-mobile nav-dropdown">
            @foreach($currencies['all'] as $currency)
                @if($currency['code'] == $currencies['current'])
                    <span>{{ $currency['code'] }}</span>
                @endif
            @endforeach
            <ul>
                @foreach($currencies['all'] as $currency)
                    @if($currency['code'] != $currencies['current'])
                        <li><a href="{{ route('site.set-curr', ['currency' => $currency['code']]) }}">{{ $currency['code'] }}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>

        {{-- @if (request()->dump == 'qwerty') --}}
            <div class="nav-top__icon-links">
                {{-- <div class="cart-mob">
                    <span class="cart-mob__price">$ 35</span>
                    <span class="cart-icon cart-mob__icon"></span>
                </div>  --}}
                <a href="{{ route('shop.cart') }}" class="cart-mob">
                    <span class="cart-mob__price js-cart-mob__price">{{ $cartControl['totalPriceString'] }}</span>
                    <span class="cart-icon cart-mob__icon js-cart-mob__has-product {{ $cartControl['countProducts'] > 0 ? 'cart-mob__has-product' : '' }}"></span>
                </a>

                <a href="{{ route('pages.contacts') }}">
                    <span class="icon-support"></span>
                </a>
            </div>
        {{-- @endif --}}

    </div>
</div>