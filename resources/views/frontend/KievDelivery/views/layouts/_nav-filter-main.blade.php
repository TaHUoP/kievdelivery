<ul class="nav-horizontal price-filter">
    <li>
        <form method="post" action="{{ route('shop.products') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ Shop::encodeFilter(['price' => ['0-40']]) }}" name="filter">
            <button>
                {{ trans('site.filter-price-range-1', ['price' => Currency::getMoneyFormat(40)]) }}
            </button>
        </form>
    </li>
    <li>
        <form method="post" action="{{ route('shop.products') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ Shop::encodeFilter(['price' => ['40-80']]) }}" name="filter">
            <button>
                {{ trans('site.filter-price-range-2', ['price_from' => Currency::getMoneyFormat(40), 'price_to' => Currency::getMoneyFormat(80)]) }}
            </button>
        </form>
    </li>
    <li>
        <form method="post" action="{{ route('shop.products') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ Shop::encodeFilter(['price' => ['80-120']]) }}" name="filter">
            <button>
                {{ trans('site.filter-price-range-3', ['price_from' => Currency::getMoneyFormat(80), 'price_to' => Currency::getMoneyFormat(120)]) }}
            </button>
        </form>
    </li>
    <li>
        <form method="post" action="{{ route('shop.products') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ Shop::encodeFilter(['price' => ['120']]) }}" name="filter">
            <button>
                {{ trans('site.filter-price-range-4', ['price' => Currency::getMoneyFormat(120)]) }}
            </button>
        </form>
    </li>
</ul>