<footer class="l-footer">
    <div class="l-footer-container footer-accordion____container">
        <nav class="l-footer-nav">
            <input id="footer-checkbox-1" type="checkbox" class="footer-accordion__checkbox">
            <label for="footer-checkbox-1" class="l-footer-nav-title footer-accordion__label">
                {{ trans('site.customer_support') }}
                <i class="footer-accordion__curve"></i>
            </label>
            <div class="footer-accordion__body">
                <a href="{{ route('pages.contacts') }}">{{ trans('site.nav-contact') }}</a>
                <a href="{{ route('site.page', ['url' => 'return']) }}">{{ trans('site.nav-return') }}</a>
                <a href="{{ route('site.page', ['url' => 'instructions']) }}">{{ trans('site.how-to-make-order') }}</a>
                <a href="{{ route('site.page', ['url' => 'changeorcancel']) }}">{{ trans('site.how-to-change-order') }}</a>
                <a href="{{ route('site.page', ['url' => 'nodelivery']) }}">{{ trans('site.nav-nodelivery') }}</a>
                <a href="{{ route('site.page', ['url' => 'faq']) }}">{{ trans('site.nav-faq') }}</a>
                <a href="{{ route('site.page', ['url' => 'sitemap']) }}">{{ trans('site.nav-sitemap') }}</a>
                <a href="{{ route('pages.customorder') }}">{{ trans('site.custom_order') }}</a>
            </div>
        </nav>
        <nav class="l-footer-nav">
            <input id="footer-checkbox-2" type="checkbox" class="footer-accordion__checkbox">
            <label for="footer-checkbox-2" class="l-footer-nav-title footer-accordion__label">
                {{ trans('site.important_info') }}
                <i class="footer-accordion__curve"></i>
            </label>
            <div class="footer-accordion__body">
                <a href="{{ route('site.page', ['url' => 'delivery-policy']) }}">{{ trans('site.nav-delivery-policy') }}</a>
                <a href="{{ route('site.page', ['url' => 'agreement']) }}">{{ trans('site.nav-agreement') }}</a>
                <a href="{{ route('site.page', ['url' => 'service']) }}">{{ trans('site.nav-service') }}</a>
                <a href="{{ route('site.page', ['url' => 'guarantees']) }}">{{ trans('site.nav-guarantees') }}</a>
                <a href="{{ route('site.page', ['url' => 'secure-payment']) }}">{{ trans('site.nav-secure-payment') }}</a>
                <a href="{{ route('site.page', ['url' => 'privacy-policy']) }}">{{ trans('site.nav-privacy-policy') }}</a>
            </div>
        </nav>
        <nav class="l-footer-nav">
            <input id="footer-checkbox-3" type="checkbox" class="footer-accordion__checkbox">
            <label for="footer-checkbox-3" class="l-footer-nav-title footer-accordion__label">
                {{ trans('site.our_company') }}
                <i class="footer-accordion__curve"></i>
            </label>
            <div class="footer-accordion__body">
                <a href="{{ route('site.page', ['url' => 'about']) }}">{{ trans('site.nav-about') }}</a>
                <a href="{{ route('site.page', ['url' => 'delivery-cities']) }}">{{ trans('site.nav-delivery-cities') }}</a>
    {{--            <a href="{{ route('site.page', ['url' => 'affiliate']) }}">{{ trans('site.nav-affiliate') }}</a>--}}
                <a href="{{ route('review.index') }}">{{ trans('site.nav-review') }}</a>
                <a href="{{ route('news.index') }}">{{ trans('site.nav-news') }}</a>
                <a href="{{ route('site.page', ['url' => 'guide']) }}">{{ trans('site.nav-flowers-and-holidays') }}</a>
                <a href="{{ route('site.page', ['url' => 'public-offer']) }}" target="_blank">{{ trans('site.public_offer') }}</a>
                <a href="{{ route('site.page', ['url' => 'personal-information']) }}" target="_blank">{{ trans('site.personal_information') }}</a>
            </div>
        </nav>
        <nav class="l-footer-nav">
            <input id="footer-checkbox-4" type="checkbox" class="footer-accordion__checkbox">
            <label for="footer-checkbox-4" class="l-footer-nav-title footer-accordion__label">
                {{ trans('site.florist_shops_ukraine') }}
                <i class="footer-accordion__curve"></i>
            </label>
            <div class="footer-accordion__body">
                @php
                    $aliases = [
                        'kyiv',
                        'lviv',
                        'odesa',
                        'dnipro',
                        'cherkasy',
                        'chernigiv',
                        'kriviyrig',
                        'mariupil'
                    ];
                @endphp
                @foreach($aliases as $alias)
                    @if ($c = $cities->where('alias', $alias)->first())
                        <a href="{{ route('site.ukraine-page', ['url' => ($alias == 'kriviyrig' ? 'kriviy-rig' : $alias)]) }}">
                            {{ Translate::get($c->translations, 'name') }}</a>
                    @endif
                @endforeach
                ...
                <a href="{{ route('site.page', ['url' => 'delivery-cities']) }}" target="_blank">{{ trans('site.another_cities') }}</a>
            </div>
        </nav>
    </div>

    <div class="l-footer-container">
        <div class="l-footer-block">
            <a href="{{ route('site.index') }}">
                <img src="{{ cdn('img/logo-dark.svg') }}" loading="lazy" title="{{ trans('site.title.logo-dark') }}" alt="{{ trans('site.alt.logo-dark') }}" />
            </a>
        </div>

        @if (App::getLocale() == 'ru' or App::getLocale() == 'uk')
            <div class="l-footer-block">
                <div class="mb-5">{{ trans('site.address') }}</div>
                <div class="l-footer-tels">
                    <div>&#9742; Украина : +380677088480</div>
                    <div>&#9742; USA: +19807295075</div>
                </div>
            </div>
        @else

            <div class="l-footer-block">
                <div class="mb-5">4100 Carmel Road, Suite B-155, Charlotte, NC 28226, USA</div>
                <div>&#9742; Украина : +380677088480</div>
                <div>&#9742; USA: +19807295075</div>
            </div>
        @endif

        <div class="l-footer-block" style="display: flex; justify-content: space-around;">
            <a href="https://twitter.com/kievdelivery" rel="nofollow noopener" target="blank"
               title="twitter" style="margin-left: 10px; width: 30px;">
                <i class="sprite sprite-twitter"></i>
            </a>
            <a href="https://www.facebook.com/FlowersAndGiftsDeliveryInUkraine/"
               rel="nofollow noopener" target="blank" title="facebook" style="margin-left: 10px; width: 30px;">
                <i class="sprite sprite-facebook"></i>
            </a>
            <a href="https://www.instagram.com/kievdelivery/"
               rel="nofollow noopener" target="blank" title="instagram" style="margin-left: 10px; width: 30px;">
                <i class="sprite sprite-instagram"></i>
            </a>
            <a href="https://www.youtube.com/user/KievdeliveryUkraine"
               rel="nofollow noopener" target="blank" title="youtube" style="margin-left: 10px; width: 30px;">
                <i class="sprite sprite-youtube"></i>
            </a>
        </div>

        <div class="l-footer-block">
            {{ trans('site.footer-copyright :y', ['y' => date('Y')]) }}
        </div>

        <div class="payments-methods"> &nbsp; </div>
        <div class="l-footer-block" style="display: flex;">
            <i class="security-sprite security-sprite-pci-dss"></i>
            <a href="https://www.mcafeesecure.com/verify?host=kievdelivery.com" rel="nofollow noopener" target="blank"
               {{--title="Certification by McAfee"--}} style="margin-left: 10px">
                <i class="security-sprite security-sprite-mcaffe"></i>
            </a>
            <a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=https://kievdelivery.com/"
               rel="nofollow noopener" target="blank" {{--title="Secure Check by Google"--}} style="margin-left: 10px">
                <i class="security-sprite security-sprite-googlecheck"></i>
            </a>
        </div>
    </div>
</footer>