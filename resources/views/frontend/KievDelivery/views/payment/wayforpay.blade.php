<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Way For Pay</title>
    <style type="text/css">
        * {margin: 0; padding: 0}
        header, section, footer, aside, nav, article,
        figure, audio, video, canvas {display: block;}
        p {margin-bottom: 15px;}
        html, body {height: 100%;}
        input[type=submit] {cursor: pointer;}
        a {color: #333; text-decoration: none }
        a:hover {color: #333; text-decoration: underline }
        body {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
            font-family: sans-serif;
            font-size: 14px;
            color: #666;
        }
        #wrapper {
            width: 100%;
            max-width: 650px;
            min-height: 100px;
            text-align: center;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <p>
        <svg width='50px' height='50px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-facebook">
            <rect x="0" y="0" width="100" height="100" fill="#ffffff" class="bk"></rect>
            <g transform="translate(20 50)">
                <rect x="-10" y="-30" width="20" height="60" fill="#cec9c9" opacity="0.6">
                    <animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform>
                </rect>
            </g>
            <g transform="translate(50 50)">
                <rect x="-10" y="-30" width="20" height="60" fill="#cec9c9" opacity="0.8">
                    <animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0.1s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform>
                </rect>
            </g>
            <g transform="translate(80 50)">
                <rect x="-10" y="-30" width="20" height="60" fill="#cec9c9" opacity="0.9">
                    <animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0.2s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform>
                </rect>
            </g>
        </svg>
    </p>
    <img src="{{ cdn('img/logo.svg') }}" alt="" >
    <p>{!! trans('site.redirect_to_way_for_pay', ['url' => '<a href="http://wayforpay.com/">http://wayforpay.com</a>']) !!}</p>
    {!! $form !!}
</div>
<script>
    //document.getElementById("form-way-for-pay").submit();
</script>
</body>
</html>