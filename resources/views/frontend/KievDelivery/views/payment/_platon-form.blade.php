<form action="{{ $platon_form_url }}" method="post" id="platon-form">
    @foreach($payment_data as $name => $value)
        <input type="hidden" name="{{ $name }}" value="{{ $value }}"/>
    @endforeach
    <input type="submit" class="button alt" value="Оплатить"/>
</form>