@extends('layouts.app')

@section('seo-info')

    @if(!empty($page))
    <title>{{ Translate::get($page->translations, 'meta_title') }}</title>
    <meta name="description" content="{{ Translate::get($page->translations, 'meta_description') }}">
   {{-- <meta name="keywords" content="{{ Translate::get($page->translations, 'meta_keywords') }}">--}}
    @else
    <title>{{ trans('site.payment_data_received') }}</title>
    <meta name="description" content="{{ trans('site.payment_data_received') }}">
    {{--<meta name="keywords" content="{{ trans('site.payment_data_received') }}">--}}
    @endif
@endsection

@section('content')

    @if(!empty($page))
    {!! Translate::get($page->translations, 'text') !!}
    @else
    {{ trans('site.payment_data_received') }}
    @endif

@endsection