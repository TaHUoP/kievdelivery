<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Multicards Pay</title>
    <style>
        * {margin: 0; padding: 0}
        header, section, footer, aside, nav, article,
        figure, audio, video, canvas {display: block;}
        p {margin-bottom: 15px;}
        html, body {height: 100%;}
        input[type=submit] {cursor: pointer;}
        a {color: #333; text-decoration: none }
        a:hover {color: #333; text-decoration: underline }
        body {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
            font-family: sans-serif;
            font-size: 14px;
            color: #666;
        }
        #wrapper {
            width: 100%;
            max-width: 650px;
            min-height: 100px;
            text-align: center;
        }
    </style>
</head>
<body>
<div id="wrapper">

    <p>
        <svg width="50px" height="50px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-facebook">
            <rect x="0" y="0" width="100" height="100" fill="#ffffff" class="bk"></rect>
            <g transform="translate(20 50)">
                <rect x="-10" y="-30" width="20" height="60" fill="#cec9c9" opacity="0.6" transform="scale(1.07723 1.07723)">
                    <animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform>
                </rect>
            </g>
            <g transform="translate(50 50)">
                <rect x="-10" y="-30" width="20" height="60" fill="#cec9c9" opacity="0.8" transform="scale(1.13294 1.13294)">
                    <animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0.1s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform>
                </rect>
            </g>
            <g transform="translate(80 50)">
                <rect x="-10" y="-30" width="20" height="60" fill="#cec9c9" opacity="0.9" transform="scale(1.22354 1.22354)">
                    <animateTransform attributeName="transform" type="scale" from="2" to="1" begin="0.2s" repeatCount="indefinite" dur="1s" calcMode="spline" keySplines="0.1 0.9 0.4 1" keyTimes="0;1" values="2;1"></animateTransform>
                </rect>
            </g>
        </svg>
    </p>
    <img src="{{ cdn('img/logo.svg') }}" alt="" >
    <p>{!! trans('site.redirect_to_way_for_pay', ['url' => '<a href="https://www.multicards.com/en/">https://www.multicards.com/</a>']) !!}</p>

    <form action="https://secure.multicards.com/cgi-bin/order2/processorder1.pl" method="post"  id="multicards_submit">

        <input name="mer_id" value="181420" type="hidden"> <!--your merchant number -->
        <input name="mer_url_idx" value="01" type="hidden"> <!-- the page id number -->

        <input type="hidden" name="next_phase" value="paydata">
        <input type="hidden" name="agree2terms" value="0">
        {{-- Customer  info --}}
        {{--<input type="hidden" value="Visa" name="pay_type" />--}}
        <input type="hidden" name="cust_name" value="{{$customer['name']}}">  <!-- Customer name -->
        <input type="hidden" name="cust_phone" value="{{$customer['phone']}}">  <!-- Customer phone -->
        <input type="hidden" name="cust_email" value="{{$customer['email']}}">  <!-- Customer email -->

        <input type="hidden" name="cust_zip" value="99999">  <!-- Customer zip -->
        <input type="hidden" name="cust_city" value="{{$customer['city']}}">  <!-- Customer city -->
        <input type="hidden" name="cust_address1" value="{{$customer['address']}}">  <!-- Customer address -->

        <input type="hidden" name="cust_country" value="Ukraine">  <!-- Customer Country -->

        <input type="hidden" name="user1" value="{{$id}}">  <!-- Customer order id -->

        <input type="hidden" name="langcode" value="{{ App::getLocale()}}">  <!-- Customer lang -->
        {{-- Customer  info --}}

        @foreach($products as $key => $product)
            <input name="item{{$key+1}}_desc" value="{{$product['name_en']}}" type="hidden">
            <input name="item{{$key+1}}_price" value="{{$product['price']}}" type="hidden">
            <input name="item{{$key+1}}_qty" value="{{$product['amount']}}" type="hidden">
        @endforeach

        <input type="submit" value="{!! trans('app.next')  !!}">

    </form>
</div>
<script>
    document.getElementById("multicards_submit").submit();
</script>
</body>
</html>