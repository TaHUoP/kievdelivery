@extends('layouts.app')

@section('seo-info')
<title>500</title>
<meta name="description" content="">
<meta name="keywords" content="">
@endsection

@section('content')

    <div class="page-404">
        <h1>500</h1>
        <p>{{ trans('app.critical_error') }}</p>
    </div>

@endsection







