<?php

if ($_SERVER['REQUEST_URI'] != '/404.html') {
    /*header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".'https://kievdelivery.com/404.html');
	exit();*/
}

?>
<?php


/*print_R(Translate::getCurrentLang());*/
/*$value = Cookie::get('frontend_language');
 print_r($value);*/
?>
@extends('layouts.app')

@section('seo-info')
    @if (empty($content))
        <title>404</title>
        <meta name="description" content="">
        {{-- <meta name="keywords" content="">--}}
    @else
        <title> {{ Translate::get($content->translations, 'meta_title') }}</title>
        <meta name="description" content="{{ Translate::get($content->translations, 'meta_description') }}">
        {{-- <meta name="keywords" content="{{ Translate::get($content->translations, 'meta_keywords') }}">--}}
    @endif
@endsection

@section('content')
    @if (empty($content))
        <div class="page-404">
            <h1>404</h1>
            <p>Запрашиваемая страница не найдена</p>
        </div>
    @else
        <div style="display: none">
            @if(isset($var))
                {{print_r($var)}}
            @endif
        </div>
        {!! Translate::get($content->translations, 'text') !!}
    @endif

@endsection







