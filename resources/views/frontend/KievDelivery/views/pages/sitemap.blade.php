@extends('layouts.app')

@section('seo-info')
    <title>SiteMap</title>
    <meta name="description" content="SiteMap">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="/sitemap.html">SiteMap</a>',
    ]) }}
@stop

@push('styles')
<link rel="stylesheet" href="{{ cdn('css/magnific-popup.min.css') }}">
@endpush

@push('scripts')
<script src="{{ cdn('js/jquery.magnific-popup.min.js') }}"></script>
@endpush

@section('content')
    <h3 class="title-separator site_map"><span>{{ trans('shop.categories') }}</span></h3>
    <div class="sitemap-list">
        @foreach($category as $cat)
            @if($cat->id != 1 && $cat->parent_id == 1 && $cat->visible == 1)
                <div class="row-cat">
                    <a href="{{route('shop.category', ['alias' => $cat->alias])}}">{{ Translate::get($cat->translations, 'name') }}</a>
                    <span><a href="/sitemap/{{$cat->alias}}">{{ trans('site.see_more') }}</a></span>
                </div>
            @endif
        @endforeach
    </div>

    <h3 class="title-separator site_map"><span>{{ trans('site.our_company') }}</span></h3>
    <div class="sitemap-list">
        <a href="{{ route('site.page', ['url' => 'about']) }}">{{ trans('site.nav-about') }}</a>
        <a href="{{ route('site.page', ['url' => 'delivery-cities']) }}">{{ trans('site.nav-delivery-cities') }}</a>
        <a href="{{ route('site.page', ['url' => 'affiliate']) }}">{{ trans('site.nav-affiliate') }}</a>
        <a href="{{ route('review.index') }}">{{ trans('site.nav-review') }}</a>
        <a href="{{ route('news.index') }}">{{ trans('site.nav-news') }}</a>
        <a href="{{ route('site.page', ['url' => 'guide']) }}">{{ trans('site.nav-flowers-and-holidays') }}</a>
        <a href="{{ route('site.page', ['url' => 'public-offer']) }}" target="_blank">{{ trans('site.public_offer') }}</a>
        <a href="{{ route('site.page', ['url' => 'personal_information']) }}" target="_blank">{{ trans('site.personal_information') }}</a>
    </div>

    <h3 class="title-separator site_map"><span>{{ trans('site.customer_support') }}</span></h3>
    <div class="sitemap-list">
        <a href="{{ route('pages.contacts') }}">{{ trans('site.nav-contact') }}</a>
        <a href="{{ route('site.page', ['url' => 'return']) }}">{{ trans('site.nav-return') }}</a>
        <a href="{{ route('site.page', ['url' => 'instructions']) }}">{{ trans('site.how-to-make-order') }}</a>
        <a href="{{ route('site.page', ['url' => 'changeorcancell']) }}">{{ trans('site.how-to-change-order') }}</a>
        <a href="{{ route('site.page', ['url' => 'nodelivery']) }}">{{ trans('site.nav-nodelivery') }}</a>
        <a href="{{ route('site.page', ['url' => 'faq']) }}">{{ trans('site.nav-faq') }}</a>
        <a href="{{ route('site.sitemap') }}">{{ trans('site.nav-sitemap') }}</a>
    </div>


    <h3 class="title-separator site_map"><span>{{ trans('site.important_info') }}</span></h3>
    <div class="sitemap-list">
        <a href="{{ route('site.page', ['url' => 'delivery-policy']) }}">{{ trans('site.nav-delivery-policy') }}</a>
        <a href="{{ route('site.page', ['url' => 'agreement']) }}">{{ trans('site.nav-agreement') }}</a>
        <a href="{{ route('site.page', ['url' => 'service']) }}">{{ trans('site.nav-service') }}</a>
        <a href="{{ route('site.page', ['url' => 'guarantees']) }}">{{ trans('site.nav-guarantees') }}</a>
        <a href="{{ route('site.page', ['url' => 'secure-payment']) }}">{{ trans('site.nav-secure-payment') }}</a>
    </div>

    <h3 class="title-separator site_map"><span>{{ trans('site.photo_video') }}</span></h3>
    <div class="sitemap-list">
        <a href="{{ route('pages.photos') }}">{{ trans('site.nav-delivery-photos') }}</a>
        <a href="{{ route('pages.videos') }}">{{ trans('site.nav-video-gallery') }}</a>
    </div>

    <h3 class="title-separator site_map"><span>{{ trans('site.nav-discounts') }}</span></h3>
    <div class="sitemap-list">
        <a href="{{ route('site.page', ['url' => 'discounts']) }}">{{ trans('site.nav-discounts') }}</a>
    </div>
@endsection