@extends('layouts.app')

@section('seo-info')
    @if(isset($categories) && !empty($categories))
        <title>SiteMap - {{ Translate::get($categories['root']->translations, 'name') }}</title>
        <meta name="description" content="SiteMap {{ trans('shop.category') }} {{ Translate::get($categories['root']->translations, 'name') }}. ">
    @elseif(isset($products) && !empty($products))
        <title>SiteMap - {{ Translate::get($products['root']->translations, 'name') }}</title>
        <meta name="description" content="SiteMap {{ trans('shop.category') }} {{ Translate::get($products['root']->translations, 'name') }}. ">
    @endif
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="/sitemap.html">SiteMap</a>',
    ]) }}
@stop

@push('styles')
<link rel="stylesheet" href="{{ cdn('css/magnific-popup.min.css') }}">
@endpush

@push('scripts')
<script src="{{ cdn('js/jquery.magnific-popup.min.js') }}"></script>
@endpush

@section('content')



        @if(isset($categories) && !empty($categories))
            <h3 class="title-separator site_map"><span>{{ Translate::get($categories['root']->translations, 'name') }}</span></h3>
            <div class="sitemap-list">
                @foreach($categories['children'] as $cat)
                    <div class="row-cat">
                        <a href="{{route('shop.category', ['alias' => $cat->alias])}}">{{ Translate::get($cat->translations, 'name') }}</a>
                        <span><a href="/sitemap/{{$cat->alias}}">{{ trans('site.see_more') }}</a></span>
                    </div>
                @endforeach
            </div>

        @elseif(isset($products) && !empty($products))
            <h3 class="title-separator site_map"><span>{{ Translate::get($products['root']->translations, 'name') }}</span></h3>
            <div class="sitemap-list">
                @foreach($products['root']['products'] as $product)
                    <a href="{{ route('shop.product', ['alias' => $product->alias]) }}">{{ Translate::get($product->translations, 'name') }}</a>
                @endforeach
            </div>
        @endif


@endsection