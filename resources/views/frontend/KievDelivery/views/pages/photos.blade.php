@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('site.photo_deliveries') }}</title>
    <meta name="description" content="{{ trans('site.photo_deliveries') }}">
    <meta name="keywords" content="{{ trans('site.photo_deliveries') }}">
@endsection

@section('canonical_links')
    <link rel="canonical" href="{{ route('pages.photos') }}"/>
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('site.photo_deliveries'),
    ]) }}
@stop

@push('styles')
    <link rel="stylesheet" href="{{ cdn('css/magnific-popup.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ cdn('js/jquery.magnific-popup.min.js') }}"></script>
@endpush

@section('content')

    <h3 class="title">{{ trans('site.photo_deliveries') }}</h3>

    <div class="l-photoDeliveries">
        @forelse($photos as $photo)
            <a href="{{ Media::getImagePreview($photo, true) }}">
                <div class="item" style="background-image: url('{{ Media::getImageReview($photo, true) }}');"></div>
            </a>
        @empty
            <div class="empty">{{ trans('app.no-data') }}</div>
        @endforelse
    </div>

    @if ($photos->count())
        @include('_pagination', ['paginator' => $photos])
    @endif

@endsection