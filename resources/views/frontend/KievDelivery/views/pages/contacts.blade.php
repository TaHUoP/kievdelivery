@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('site.contacts') }}</title>
    <meta name="description" content="{{ trans('site.contacts') }}">
    <meta name="keywords" content="{{ trans('site.contacts') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('site.contacts'),
    ]) }}
@stop

@section ('canonical_links')
    <link rel="canonical" href="{{ request()->url() }}"/>
@endsection

@section('content')

    <h3 class="title">{{ trans('app.contact-with-us') }}</h3>

    <div class="l-feedback-block text-justify">
        {{ Widget::run('Plugin', ['key' => 'contact_info', 'view' => 'plugins.text']) }}
    </div>

    <p><br></p>

    {{ Form::open([
        'route' => 'pages.contacts',
        'method' => 'POST'
    ]) }}
        <div class="l-feedback">

            <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', trans('app.attr.name'), ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::text('name', isset($user) ? $user->profile->name : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ Form::label('email', trans('app.attr.email'), ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::email('email', isset($user) ? $user->email : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('message') ? 'has-error' : '' }}">
                {{ Form::label('message', trans('app.attr.message'), ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::textarea('message', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('message') ? $errors->first('message') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                <div class="input-group">
                    {!! Recaptcha::render([ 'lang' => App::getLocale() ]) !!}
                    <div class="help-block"> {{ $errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '' }}</div>
                </div>
            </div>

            <div class="form-group">
                <div class="control-label"></div>
                <div class="input-group">
                    <button class="btn btn-warning">{{ trans('app.send') }}</button>
                </div>
            </div>

        </div>
    {{ Form::close() }}

@endsection