@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('site.custom_order') }}</title>
    <meta name="description" content="{{ trans('site.custom_order') }}">
    <meta name="keywords" content="{{ trans('site.custom_order') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('site.custom_order'),
    ]) }}
@stop

@section('content')

    <div class="l-registration-form">

        <h3 class="title">{{ trans('site.custom_order') }}</h3>

        {{ Form::open([
            'route' => 'pages.customorder',
            'class' => 'form-horizontal',
            'method' => 'POST'
        ]) }}

        <div class="section">

            <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', trans('site.your-name'), ['class' => 'control-label']) }}
                {{ Form::text('name', null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ Form::label('email', trans('site.your-email'), ['class' => 'control-label']) }}
                {{ Form::email('email', null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('contact_info') ? 'has-error' : '' }}">
                <span class="control-wrap">
                    {{ Form::label('contact_info', trans('site.you-want-being-ordered'), ['class' => 'control-label']) }}
                    <span class="control-label-info">({{ trans('site.describe-to-order') }})</span>
                </span>
                {{ Form::textarea('contact_info', null, ['class' => 'form-control', 'id' => 'contact_info']) }}
                <div class="help-block">{{ $errors->has('contact_info') ? $errors->first('contact_info') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('address') ? 'has-error' : '' }}">
                <span class="control-wrap">
                    {{ Form::label('address', trans('site.to-be-delivered-to'), ['class' => 'control-label']) }}
                    <span class="control-label-info">({{ trans('site.write-info') }})</span>
                </span>
                {{ Form::textarea('address', null, ['class' => 'form-control', 'id' => 'address']) }}
                <div class="help-block">{{ $errors->has('address') ? $errors->first('address') : '' }}</div>
            </div>

            <div class="form-group {{ $errors->has('instruction') ? 'has-error' : '' }}">
                <span class="control-wrap">
                    {{ Form::label('instruction', trans('site.add-insruct'), ['class' => 'control-label']) }}
                    <span class="control-label-info">({{ trans('site.if-any') }})</span>
                </span>
                {{ Form::textarea('instruction', null, ['class' => 'form-control', 'id' => 'instruction']) }}
                <div class="help-block">{{ $errors->has('instruction') ? $errors->first('instruction') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('comment') ? 'has-error' : '' }}">
                {{ Form::label('comment', trans('site.message-to-recipient'), ['class' => 'control-label']) }}
                {{ Form::textarea('comment', null, ['class' => 'form-control', 'id' => 'comment']) }}
                <div class="help-block">{{ $errors->has('comment') ? $errors->first('comment') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                {!! Recaptcha::render([ 'lang' => App::getLocale() ]) !!}
                <div class="help-block">
                    {{ $errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '' }}
                </div>
            </div>

        </div>

        <div>
            <button class="btn btn-warning">{{ trans('app.send') }}</button>
        </div>

        {{ Form::close() }}

    </div>

@endsection