@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('site.video_deliveries') }}</title>
    <meta name="description" content="{{ trans('site.video_deliveries') }}">
   <meta name="keywords" content="{{ trans('site.video_deliveries') }}">
@endsection

@section('canonical_links')
    <link rel="canonical" href="{{ route('pages.videos') }}"/>
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('site.video_deliveries'),
    ]) }}
@stop

@push('styles')
    <link rel="stylesheet" href="{{ cdn('css/magnific-popup.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ cdn('js/jquery.magnific-popup.min.js') }}"></script>
@endpush

@section('content')

    <h3 class="title">{{ trans('site.video_deliveries') }}</h3>

    <div class="l-videoGallery">
        @forelse($videos as $video)
            <iframe width="420" height="315" src="{{ $video->review_url }}" frameborder="0" allowfullscreen></iframe>
        @empty
            <div class="empty">{{ trans('app.no-data') }}</div>
        @endforelse
    </div>

    @if ($videos->count())
        @include('_pagination', ['paginator' => $videos])
    @endif

@endsection