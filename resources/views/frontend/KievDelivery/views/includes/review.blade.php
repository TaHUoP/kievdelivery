<div class="container">
    <div class="reviews">
        <div class="tabs_controls" style="">
            <div class="tab-caption">{{ trans('reviews.product_review') }} ({{ $reviews->count() }})</div>
        </div>
        <div class="reviews-stats">
            <div class="reviews-stats-item">
                <b>{{ trans('reviews.avg_rating') }}</b>
                <span class="star-rating">
                    <span class="star-rating__item star-rating__item_default"></span>
                    <span class="star-rating__positive" style="width: {{ $reviews->avg('rating') * 100 / 5 }}%">
                        <span class="star-rating__item star-rating__item_positive"></span>
                    </span>
                </span>
                <span class="rating-value">
                    <span class="item-value">{{ !$reviews->isEmpty() ? $reviews->avg('rating') : 0 }}</span> /
                    <span>5</span>
                    </span>
                <div class="text-muted">{{ trans('reviews.based_on', ['count' => $reviews->count()]) }}</div>
            </div>
            <div class="reviews-stats-item">
                <ul class="rating-list review__rating-list">
                    @for ($i = 5; $i > 0; $i--)
                        <li>
                            <div class="item-count">{{ $i }} star{{ $i == 1 ? '' : 's' }}</div>
                            <div class="item-scale-box">
                                <div class="item-scale">
                                    <div class="item-value" style="width: {{ $reviews->count() > 0
                                    ? $reviews->where('rating', (string) $i)->count()/$reviews->count() * 100 : 0 }}%;"></div>
                                </div>
                            </div>
                            <div class="item-count-value">({{ $reviews->where('rating', (string) $i)->count() }})</div>
                        </li>
                    @endfor
                </ul>
            </div>
            <div class="reviews-stats-item">
                <p class="review-text">
                    {{ trans('reviews.write_the_review_text') }}
                </p>
                <p><a href="#" class="btn btn-success write-review-btn">{{ trans('reviews.write_review') }}</a></p>
            </div>
        </div>

        @if ($reviews->count() > 0)
        <div class="reviews-list">
            @foreach($reviews as $review)
                <div class="review-item">
                    <div class="review-info">
                        <div class="review-info__group">
                            <span class="star-rating">
                                <span class="star-rating__item star-rating__item_default"></span>
                                <span class="star-rating__positive" style="width: {{ $review->rating * 100 }}%">
                                    <span class="star-rating__item star-rating__item_positive"></span>
                                </span>
                            </span>
                            <span class="review-info__badge">{{ $review->rating }}.0</span>
                            <span class="review-info__author"><b>{{ $review->name }}</b> ({{ $review->city_delivery }})</span>
                        </div>
                        <div class="review-info__group">
                            <div class="review-info__date">{{ $review->created_at->format('d-m-Y H:i') }}</div>
                        </div>
                    </div>
                    <div class="review-text">
                        <p>{{ $review->text }}</p>
                    </div>
                    @if ($review->admin_answer)
                        <div class="review-answer">
                            <span class="review-item-author"><b>{{ trans('reviews.im_admin') }}</b></span> {{ $review->admin_answer }}
                            {{--<div class="review-item-date">17 Aug 2017, 19:50</div>--}}
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
        @endif

        @include('review._form', ['main' => false, 'product' => $product])
    </div>
</div>