@extends('layouts.app')

@section('seo-info')
    <title>{{ Translate::get($news->translations, 'title') }}</title>
    <meta name="description" content="{{ Translate::get($news->translations, 'description') }}">
    <meta name="keywords" content="{{ Translate::get($news->translations, 'keywords') }}">
@endsection

@section ('canonical_links')
    <link rel="canonical" href="{{ request()->url() }}"/>
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('news.index').'">'.trans('news.news').'</a>',
        Translate::get($news->translations, 'title')
    ]) }}
@stop

@section('content')

    <article class="l-news">
        <h4 class="l-news-title">{{ Translate::get($news->translations, 'title') }}</h4>
        <span class="l-news-date">{{ trans('news.date_added') }}: <time datetime="{{ $news->created_at }}">{{ $news->created_at }}</time></span>
        <div class="l-news-text clearfix">
            {!! Translate::get($news->translations, 'text') !!}
        </div>
    </article>

@endsection
