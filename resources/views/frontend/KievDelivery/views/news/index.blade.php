@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('news.news') }}</title>
    <meta name="description" content="{{ trans('news.news') }}">
   <meta name="keywords" content="{{ trans('news.news') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('news.news'),
    ]) }}
@stop

@section ('canonical_links')
    <link rel="canonical" href="{{ request()->url() }}"/>
@endsection

@section('content')

    <h3 class="title">{{ trans('news.last_all') }}</h3>

    <div class="grid-news">
        @if ($news->count())
            @foreach($news as $article)
                <article class="l-news">
                    <h4 class="l-news-title"><a href="{{ route('news.show', ['url' => $article->url]) }}">{{ Translate::get($article->translations, 'title') }}</a></h4>
                    <span class="l-news-date">{{ trans('news.date_added') }}: <time datetime="{{ $article->created_at }}">{{ $article->created_at }}</time></span>
                    <div class="l-news-text clearfix shadow">
                        {{--<img src="{{ cdn('img/products/item-1.jpg') }}" alt="" style="float: right; margin: 0 55px 0 15px;width: 250px;"/>
                        <p>Потрясающий для любого случая, по любому поводу , этот захватывающий букет из 19
                            красных роз премиум качества доставит все Ваши чувства для Вашей девушки!</p>
                        <p>Реальные фотографии цветов букетов можно посмотреть на нашей странице в фейсбуке
                            www.facebook.com/kievdelivery или на странице Фото Доставок. Советуем посетить разделы
                            с продуктами и шампанским в Украине - товары из этих разделов могут стать
                            прекрасным дополнением к Вашим прекрасным розам.
                        </p> --}}
                        {!! Translate::get($article->translations, 'text') !!}
                    </div>
                     <a href="{{ route('news.show', ['url' => $article->url]) }}" class="btn btn-warning">{{ trans('app.more') }}</a>
                </article>
            @endforeach

            @include('_pagination', ['paginator' => $news])
        @else
            <p>{{ trans('app.no-data') }}</p>
        @endif
    </div>

@endsection

@push('styles')
    <style>
        .shadow {
            height: 48px;
            overflow: hidden;
            position: relative;
        }

        .shadow:before {
            content: "";
            display: block;
            width: 100%;
            height: 17px;
            position: absolute;
            z-index: 1;
            bottom: 0;
            background-image: -webkit-gradient(linear,left top,left bottom,from(hsla(0,0%,100%,0)),color-stop(70%,#fff));
            background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff 53%);
        }
    </style>
@endpush