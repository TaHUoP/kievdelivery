@extends('layouts.app')

@section('seo-info')
    <title>Главная страница</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section ('rel_publisher')
    <link href="https://www.facebook.com/FlowersAndGiftsDeliveryInUkraine/?ref=bookmarks" rel="publisher" />
@endsection

@section('canonical_links')
    <link rel="canonical" href="{{ route('site.index') }}"/>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="{{ cdn('css/review-block.min.css') }}">
{{--    <link rel="stylesheet" href="{{ cdn('css/fontello.css') }}">--}}
{{--    <link rel="stylesheet" href="{{ cdn('css/rating.min.css') }}">--}}

    <style>
        .shadow {
            height: 475px;
            overflow: hidden;
            position: relative;
        }

        .shadow:before {
            content: "";
            display: block;
            width: 100%;
            height: 17px;
            position: absolute;
            z-index: 1;
            bottom: 0;
            /*background-image: -webkit-gradient(linear,left top,left bottom,from(hsla(0,0%,100%,0)),color-stop(70%,#fff));*/
            /*background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff 53%);*/
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js"></script>
    <script src="{{ cdn('js/rating.min.js') }}"></script>
    <script>
        $(document).ready(function () {
             $('.show').on('click', function () {
                 $(this).prev().removeClass('shadow').css('height', 'auto');
                 $(this).hide();
                 $(this).next().show();
             });
             $('.hide').on('click', function () {
                 $(this).prev().prev().addClass('shadow').css('height', '600px');
                 $(this).hide();
                 $(this).prev().show();
             });
        });
    </script>
@endpush

@section('extra-container')
    @include('layouts._slider')

{{--    @include('layouts._nav-filter-main')--}}
@endsection

@section('content')
    <section>
        @if ($categories->count() == 1)
            <h3 class="title-separator"><span>{{ Translate::get($categories[0]->translations)->name }}</span></h3>
        @else
            <h3 class="title-separator"><span>{{ trans('site.recommended') }}</span></h3>
        @endif

        <div class="grid-products">
            @if ($productsRecommended->count())
                @each('shop.product._item', $productsRecommended, 'product')
            @else
                <div class="empty">{{ trans('app.no-data') }}</div>
            @endif
        </div>
    </section>

    <section>
        {{ Widget::run('Plugin', ['key' => 'contacts_social', 'view' => 'plugins.text']) }}
        @if($user_city)
            <div class="shadow l-seo__block" style="height: 600px">
                {!! Translate::get($user_city->translations, 'text') !!}
            </div>
            <div style="cursor: pointer; text-align: center; font-size: 20px" class="show">{{ trans('site.read_more') }}</div>
            <div style="cursor: pointer; text-align: center; font-size: 20px; display: none"
                 class="hide">{{ trans('site.hide') }}</div>
        @endif
    </section>

    <section>
        <h3 class="title-separator"><span>{{ trans('site.novelty') }}</span></h3>
        <div class="grid-products">
            @if ($productsNew->count())
                @each('shop.product._item', $productsNew, 'product')
            @else
                <div class="empty">{{ trans('app.no-data') }}</div>
            @endif
        </div>
    </section>

    {{--<section>--}}
        {{--<h3 class="title-separator"><span>{{ trans('reviews.reviews') }}</span></h3>--}}
        {{--<div class="l-review">--}}
            {{--@include('review._index')--}}
        {{--</div>--}}
    {{--</section>--}}

    {{--City modal--}}

    {{--<div class="overlay" @if($has_city) style="display: none" @endif>--}}
    {{--</div>--}}
    {{--<div class="modal-window" @if($has_city) style="display: none" @endif>--}}
        {{--<div class="modal-header">--}}
            {{--<div class="l-header-logo">--}}
                {{--<a href="{{ route('site.index', ['city_alias' => $user_city->alias]) }}">--}}
                    {{--<img src="{{ cdn('img/logo.svg') }}" title="{{ trans('site.title.logo') }}" alt="{{ trans('site.alt.logo') }}"/>--}}
                {{--</a>--}}
                {{--<div>{{ trans('site.text_we_service') }} <span--}}
                            {{--class="l-header-logo-link">{{ trans('site.text_all_ukraine') }}</span></div>--}}
            {{--</div>--}}
            {{--<div class="close">X</div>--}}
        {{--</div>--}}

        {{--<div class="modal-body">--}}
            {{--<form id="city-form-modal" method="POST" action="{{ route('city.update') }}">--}}
                {{--{{ csrf_field() }}--}}
                {{--<div class="select-location">--}}
                    {{--<p>{{ trans('cities.where-is-it-going') }}</p>--}}
                    {{--<select id="city-select-modal" name='city_id' class="select-city js-states form-control">--}}
                        {{--@foreach($cities as $city)--}}
                            {{--<option data-keywords="{{ Translate::get($city->translations, 'key_words') }}" value="{{ $city->id }}" {{ $user_city->alias == $city->alias ? 'selected' : '' }}>{{ $city->with_region_and_fee }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</form>--}}

            {{--<div class="modal-btn">--}}
                {{--<a href="#" class="decline">skip</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection

@push('styles')
    <style>
        .shadow {
            height: 48px;
            overflow: hidden;
            position: relative;
        }

        .shadow:before {
            content: "";
            display: block;
            width: 100%;
            height: 17px;
            position: absolute;
            z-index: 1;
            bottom: 0;
            background-image: -webkit-gradient(linear,left top,left bottom,from(hsla(0,0%,100%,0)),color-stop(70%,#fff));
            background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff 53%);
        }
    </style>
@endpush