@extends('layouts.app')

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('users.my-account')
    ]) }}
@stop

@section('content')

    <div class="l-profile">

        <div class="l-profile-steps">
            <a href="{{ route('user.index') }}" class="is-active">{{ trans('users.my-account') }}</a>
            <a href="{{ route('shop.checkout.orders') }}">{{ trans('users.orders-history') }}</a>
        </div>

        <div class="l-profile-myprofile">
            <h5 class="title">{{ trans('users.main-information') }}</h5>

            {{ Form::open(['route' => 'user.main-info', 'method' => 'POST']) }}

                <div class="form-group required {{ $errors->has('phone') ? 'has-error' : '' }}">
                    {{ Form::label(trans('users.attr.phone'), null, ['class' => 'control-label']) }}
                    {{ Form::text('phone', $account['phone'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                    <div class="help-block">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    {{ Form::label(trans('users.attr.email'), null, ['class' => 'control-label']) }}
                    {{ Form::email('email', $account['email'], ['class' => 'form-control', 'disabled' => 'disabled']) }}
                    <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    {{ Form::label(trans('users.attr.name'), null, ['class' => 'control-label']) }}
                    {{ Form::text('name', $account['profile']['name'], ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('surname') ? 'has-error' : '' }}">
                    {{ Form::label(trans('users.attr.surname'), null, ['class' => 'control-label']) }}
                    {{ Form::text('surname', $account['profile']['surname'], ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('surname') ? $errors->first('surname') : '' }}</div>
                </div>

                {{ Form::button(trans('users.btn.change-information'), ['class' => 'btn btn-light', 'type' => 'submit']) }}

            {{ Form::close() }}

            <div class="l-profile-myprofile-password-manage">
                <h5 class="title">{{ trans('users.manage-password') }}</h5>

                {{ Form::open(['route' => 'user.manage-password', 'method' => 'POST']) }}

                    <div class="form-group required {{ $errors->has('cur_password') ? 'has-error' : '' }}">
                        {{ Form::label(trans('users.attr.cur-password'), null, ['class' => 'control-label']) }}
                        {{ Form::password('cur_password', ['class' => 'form-control']) }}
                        <div class="help-block">{{ $errors->has('cur_password') ? $errors->first('cur_password') : '' }}</div>
                    </div>

                    <div class="form-group required {{ $errors->has('new_password') ? 'has-error' : '' }}">
                        {{ Form::label(trans('users.attr.new-password'), null, ['class' => 'control-label']) }}
                        {{ Form::password('new_password', ['class' => 'form-control']) }}
                        <div class="help-block">{{ $errors->has('new_password') ? $errors->first('new_password') : '' }}</div>
                    </div>

                    <div class="form-group required {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}">
                        {{ Form::label(trans('users.attr.new-password-confirmation'), null, ['class' => 'control-label']) }}
                        {{ Form::password('new_password_confirmation', ['class' => 'form-control']) }}
                        <div class="help-block">{{ $errors->has('new_password_confirmation') ? $errors->first('new_password_confirmation') : '' }}</div>
                    </div>

                    {{ Form::button(trans('users.btn.update-password'), ['class' => 'btn btn-light', 'type' => 'submit']) }}

                {{ Form::close() }}

            </div>

            <div class="l-profile-myprofile-password-manage">
                <h5 class="title">{{ trans('users.change-email') }}</h5>

                {{ Form::open(['route' => 'user.change-email', 'method' => 'POST']) }}
                    <div class="form-group required {{ $errors->has('cur_email') ? 'has-error' : '' }}">
                        {{ Form::label(trans('users.attr.cur-email'), null, ['class' => 'control-label']) }}
                        {{ Form::email('cur_email', $account['email'], ['class' => 'form-control']) }}
                        <div class="help-block">{{ $errors->has('cur_email') ? $errors->first('cur_email') : '' }}</div>
                    </div>

                    <div class="form-group required {{ $errors->has('new_email') ? 'has-error' : '' }}">
                        {{ Form::label(trans('users.attr.new-email'), null, ['class' => 'control-label']) }}
                        {{ Form::email('new_email', null, ['class' => 'form-control']) }}
                        <div class="help-block">{{ $errors->has('new_email') ? $errors->first('new_email') : '' }}</div>
                    </div>

                    {{ Form::button(trans('users.btn.update-email'), ['class' => 'btn btn-light', 'type' => 'submit']) }}
                {{ Form::close() }}

            </div>

        </div>

    </div>

@endsection