<div class="ajax-pagination" {!! $paginator->lastPage() > 1 ? '' : 'style="display: none !important;"' !!}>
    <img src="{{ cdn('img/pixel.gif') }}" class="preloader">
    <a href="{{ url()->current() }}" data-page="{{ $paginator->currentPage() + 1 }}">{{ trans('shop.show-more') }}</a>
</div>