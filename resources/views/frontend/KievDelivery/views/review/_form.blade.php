<div class="review-form" style="{{ $main ? 'display: block' : '' }}">
    <div class="review-form__caption">
        {{ trans('site.your-comment') }}
    </div>
    <form action="{{ $main ? route('review.store.main') : route('review.store') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="rating" id="review-rating" value="3">
        @if (!$main)
            <input type="hidden" name="product_id" value="{{ @$product->id }}">
        @endif
        <div class="review-form__group">
            <p class="review-form__label">{{ trans('site.your-rating') }}</p>
            <span class="reviews-block-form-rating" style="float: inherit;"></span>
        </div>
        <div class="review-form__group">
            <label class="review-form__label" for="comment">{{ trans('shop.comment') }} (<a href="{{ route('site.page', ['url' => 'reviews-rules']) }}">Rules for posting comments</a>)</label>
            <textarea placeholder="{{ trans('shop.comment_placeholder') }}" name="text" cols="50"
                      maxlength="1500" required
                      rows="10" id="comment"></textarea>
            <div class="help-block"> {{ $errors->has('text') ? $errors->first('text') : '' }}</div>
        </div>
        <div style="display: flex; flex-direction: row">
            <div class="review-form__group" style="margin-right: 40px;">
                <label class="review-form__label" for="name">{{ trans('site.your-name') }}</label>
                <input type="text" name="name" id="name" required
                       value="{{ Auth::check() ? Auth::user()->name . Auth::user()->surname : '' }}">
                <div class="help-block"> {{ $errors->has('name') ? $errors->first('name') : '' }}</div>
            </div>
            @if (!$main)
                <div class="review-form__group">
                    <label class="review-form__label" for="city">{{ trans('shop.attr.city-of-delivery') }}</label>
                    <input name="city_delivery" type="text" id="city" required
                           value="{{ \Illuminate\Support\Facades\Cookie::get('city_alias') }}">
                    <div class="help-block"> {{ $errors->has('city_delivery') ? $errors->first('city_delivery') : '' }}</div>
                </div>
            @endif
        </div>
        <div class="review-form__group">
            <label class="review-form__label" for="email">{{ trans('site.your-email') }} ({{ trans('shop.not_published') }})</label>
            <input name="email" type="text" id="email"
                   value="{{ Auth::check() ? Auth::user()->email : '' }}" required>
            <div class="help-block"> {{ $errors->has('email') ? $errors->first('email') : '' }}</div>
        </div>
        <div class="review-form__group">
            {!! Recaptcha::render([ 'lang' => App::getLocale() ]) !!}
            <div class="help-block">
                {{ $errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '' }}
            </div>
        </div>

        <div class="review-form-btn">
            <input type="submit" class="btn btn-success" value="{{ trans('reviews.create') }}">
            <a class="review-form__cancel" href="#">{{ trans('app.cancel') }}</a>
        </div>
    </form>
</div>