@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('reviews.reviews') }}</title>
    <meta name="description" content="{{ trans('reviews.reviews') }}">
    <meta name="keywords" content="{{ trans('reviews.reviews') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('reviews.reviews'),
    ]) }}
@stop

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
<link rel="stylesheet" href="{{ cdn('css/review-block.min.css') }}">
{{--<link rel="stylesheet" href="{{ cdn('css/fontello.css') }}">--}}
{{--<link rel="stylesheet" href="{{ cdn('css/rating.min.css') }}">--}}
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js"></script>
<script src="{{ cdn('js/rating.min.js') }}"></script>
@endpush

@section('content')

    <h3 class="title">{{ trans('reviews.reviews') }}</h3>

    <div class="l-review">
        @include('review._index')
    </div>

@endsection