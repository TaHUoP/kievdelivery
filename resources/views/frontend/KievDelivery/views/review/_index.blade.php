@if ($reviews->count())

    <div class="swiper-container">
        <div class="swiper-wrapper">
            @foreach ($reviews as $review)
                <div class="swiper-slide">
                    <div class="l-review-image" style="background-image: url({{ User::getAvatarUrlByUser($review->user) }})"></div>
                    <div class="l-review-name color-warning">{{ $review->name }}</div>
                    <div class="l-review-text">
                        {{ $review->text ? $review->text : trans('app.no-data') }}
                    </div>
                </div>
            @endforeach
        </div>

        <!-- <div class="swiper-pagination"></div> -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>

@else

    <p class="empty">{{ trans('app.no-data') }}</p>

@endif

@if (request()->route()->getName() != 'site.index')
    @include('review._form', ['main' => true, 'product' => null])
@endif