@if(!empty($data) && is_array($data))
    <div class="l-slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                @foreach($data as $key => $slide)
                    @if(!empty($slide['img']))
                        <div class="swiper-slide l-slider-slide-{{ $key }}" style="background-image: url('{{ $slide['img'] }}')">
                            @if (!empty($slide['translations']['title']))
                                <h3 class="l-slider-title">{{ $slide['translations']['title'] }}</h3>
                            @endif
                            @if (!empty($slide['translations']['description']))
                                <div class="l-slider-description">
                                    {{ $slide['translations']['description'] }}
                                </div>
                            @endif
                            {{-- <div class="l-slider-price">За $45</div> --}}
                            @if (!empty($slide['translations']['url']))
                                <a href="{{ $slide['translations']['url'] }}" class="btn btn-info btn-present">
                                    {{ trans('site.learn_more') }}
                                </a>
                            @endif
                        </div>
                    @endif
                @endforeach
            </div>
            <!-- <div class="swiper-pagination"></div> -->
            <!-- <div class="swiper-button-next"></div> -->
            <!-- <div class="swiper-button-prev"></div> -->
        </div>
    </div>
@endif