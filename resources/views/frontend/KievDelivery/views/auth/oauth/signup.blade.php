@extends ('layouts.app')

@section('seo-info')
    <title>{{ trans('auth.signup') }}</title>
    <meta name="description" content="{{ trans('auth.signup') }}">
    <meta name="keywords" content="{{ trans('auth.signup') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('guest.signup').'">'.trans('auth.signup').'</a>',
        trans('auth.oauth.signup'),
    ]) }}
@stop

{{--@section('main-page-class', 'header-over')--}}
@section('content')

    <div class="l-login-form">
        <div class="fast-login">
            <h4 class="fast-login-title">{{ trans('auth.oauth.not_enough_data') }}:</h4>

            {{ Form::open([
                'route' => ['guest.oauth.signup', $provider],
                'class' => 'form-horizontal',
                'method' => 'POST'
            ]) }}

            <input type="hidden" value="{{ $user_profile->id }}" name="user_profile_id">

            <div class="form-group required">
                {{ Form::label(trans('auth.attr.email'), null, ['class' => 'control-label']) }}
                <div class="form-input">
                    {{ Form::email('email', $user_profile->email, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group required">
                {{ Form::label(trans('auth.attr.phone'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::text('phone', $user_profile->phone, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="fast-login-settings">
                <button type="submit" class="btn btn-light">{{ trans('auth.signup') }}</button>
            </div>

            {{ Form::close() }}

        </div>
    </div>


@endsection
