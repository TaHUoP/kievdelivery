@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('auth.signin') }}</title>
    <meta name="description" content="{{ trans('auth.signin') }}">
    <meta name="keywords" content="{{ trans('auth.signin') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('guest.signin').'">'.trans('auth.signin').'</a>',
    ]) }}
@stop

@section('content')

    <div class="l-login-form sign-in__form-wrapp">
        <div class="fast-login">
            <h4 class="fast-login-title">{{ trans('site.login-into-my-office') }}:</h4>
        
            {{ Form::open([
                'route' => 'guest.signin',
                'class' => 'form-horizontal',
                'method' => 'POST'
            ]) }}

            <div class="form-group required {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.email'), null, ['class' => 'control-label']) }}
                <div class="form-input">
                    {{ Form::email('email', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('password') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.password'), null, ['class' => 'control-label']) }}
                <div class="form-input">
                    {{ Form::password('password', ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('password') ? $errors->first('password') : '' }}</div>
                </div>
            </div>

            <div class="fast-login-settings">
                <div class="fast-login-settings-list">
                    <a href="{{ route('guest.signup') }}">{{ trans('auth.signup') }}</a>
                    <a href="{{ route('guest.password-reset') }}">{{ trans('auth.forgot-password') }}</a>
                </div>
                <button type="submit" class="btn btn-light">{{ trans('auth.btn.enter-cabinet') }}</button>
            </div>

            @if (Session::has('authentication_errors'))
                @foreach(Session::get('authentication_errors') as $auth_error)
                    <p>{{ $auth_error }}</p>
                @endforeach
            @endif

            {{ Form::close() }}

        </div>
    </div>

@endsection