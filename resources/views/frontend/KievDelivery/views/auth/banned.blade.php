@extends('layouts.app')
@section('content')
<h3>{{ $message }}</h3>
    <a href="{{ url()->previous() }}">{{ trans('app.get-back') }}</a>
@endsection
