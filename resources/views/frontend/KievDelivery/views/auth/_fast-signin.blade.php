<div class="fast-login">
    <h4 class="fast-login-title">{{ trans('auth.fast-login', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</h4>
    {{ Form::open(['route' => ['guest.signin'], 'method' => 'POST', 'class' => 'form-horizontal']) }}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::label('email', trans('auth.login', [], 'messages', isset($currentLocale) ? $currentLocale : null), ['class' => 'control-label'] ) }}
            {{ Form::email('email', null, ['class' => 'form-control']) }}
            <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::label('password', trans('auth.password', [], 'messages', isset($currentLocale) ? $currentLocale : null), ['class' => 'control-label'] ) }}
            {{ Form::password('password', ['class' => 'form-control']) }}
            <div class="help-block">{{ $errors->has('password') ? $errors->first('password') : '' }}</div>
        </div>
        <div class="fast-login-settings">
            <div class="fast-login-settings-list">
                <a href="{{ route('guest.signup') }}">{{ trans('auth.signup', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</a>
                <a href="{{ route('guest.password-reset') }}">{{ trans('auth.forgot-your-password', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</a>
            </div>
            {{ Form::submit(trans('auth.login-and-place-your-order', [], 'messages', isset($currentLocale) ? $currentLocale : null), ['class' => 'btn btn-light']) }}
        </div>
    {{ Form::close() }}
</div>