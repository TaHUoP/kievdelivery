@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('auth.signin') }}</title>
    <meta name="description" content="{{ trans('auth.signin') }}">
    <meta name="keywords" content="{{ trans('auth.signin') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('email_confirmation_form').'">'.trans('auth.email-confirmation').'</a>',
    ]) }}
@stop

@section('content')

    <div class="l-login-form">
        <div class="fast-login">
            <h4 class="fast-login-title">{{ trans('auth.enter-email-code') }}</h4>

            {{ Form::open([
                'route' => 'email_confirm',
                'class' => 'form-horizontal',
                'method' => 'GET'
            ]) }}

            <div class="form-group required {{ $errors->has('code') ? 'has-error' : '' }}">
                <div class="form-input">
                    {{ Form::text('code', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('code') ? $errors->first('code') : '' }}</div>
                </div>
            </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-light">{{ trans('app.send') }}</button>
                    </div>
                </div>
            {{ Form::close() }}



        </div>
    </div>

@endsection