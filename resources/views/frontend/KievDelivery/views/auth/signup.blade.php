@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('auth.signup') }}</title>
    <meta name="description" content="{{ trans('auth.signup') }}">
    <meta name="keywords" content="{{ trans('auth.signup') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('guest.signup').'">'.trans('auth.signup').'</a>',
    ]) }}
@stop

@section('content')
    <div class="l-registration-form">

        <h3 class="title">{{ trans('auth.signup') }}</h3>

        <a class="login-social login-google" href="{{ route('guest.oauth.redirect', ['provider' => App\Models\User\Profile::GOOGLE_PROVIDER]) }}">{{ trans('auth.signup_via_google') }}</a>
        <a class="login-social login-facebook" href="{{ route('guest.oauth.redirect', ['provider' => App\Models\User\Profile::FACEBOOK_PROVIDER]) }}">{{ trans('auth.signup_via_facebook') }}</a>

        {{ Form::open([
            'route' => 'guest.signup',
            'class' => 'form-horizontal',
            'method' => 'POST'
        ]) }}

        <div class="section">

            <div class="form-group required {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.email'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::email('email', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('phone') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.phone'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::text('phone', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('password') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.password'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::password('password', ['class' => 'form-control']) }}
                    <p style="margin: 0">{{ trans('users.password.info') }}</p>
                    <div class="help-block">{{ $errors->has('password') ? $errors->first('password') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.password-confirmation'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('password_confirmation') ? $errors->first('password_confirmation') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.name'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
                </div>
            </div>

            <div class="form-group required {{ $errors->has('surname') ? 'has-error' : '' }}">
                {{ Form::label(trans('auth.attr.surname'), null, ['class' => 'control-label']) }}
                <div class="input-group">
                    {{ Form::text('surname', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('surname') ? $errors->first('surname') : '' }}</div>
                </div>
            </div>


            <div class="g-recaptcha" data-sitekey="6LfyWUgUAAAAAOKNuzvXH9k3VYoyzD26UkmNeeQF"></div>
            <div class="form-group required {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                <div class="input-group">
                    <div class="help-block">{{ $errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '' }}</div>
                </div>
            </div>

        </div>

        <div class="registration-form-subbmit">
            <label class="control-label {{ $errors->has('terms-and-conditions') ? 'color-danger' : '' }}">
                {{ Form::checkbox('terms-and-conditions', null, null, ['class' => 'form-control checkbox']) }}
                <a href="{{ route('site.page', ['url' => 'agreement']) }}"
                   target="_blank">{{ trans('auth.attr.terms-and-conditions') }}</a>
            </label>
            <button class="btn btn-warning">{{ trans('auth.register-button') }}</button>
        </div>

        {{ Form::close() }}


    </div>

@endsection