@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('shop.products') }}</title>
    <meta name="description" content="{{ trans('shop.products') }}">
<meta name="keywords" content="{{ trans('shop.products') }}">
    @if ($filter)
        <meta name="robots" content="noindex, nofollow" />
    @endif
@endsection

@section('canonical_links')
    {{--@if($filter === ['sort' => ['price_asc']])--}}
        {{--<link rel="canonical" href="{{ url('/') }}" />--}}
    {{--@endif--}}
    @if ($products->currentPage() > 1)
        <link rel="canonical" href="{{ url('products') }}" />
    @endif
@endsection

@section('nav-filter')
    @include('layouts._nav-filter', ['filter'])
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.products'),
    ]) }}
@stop

{{-- TODO: think on how following section can be written more beautiful--}}
@push('scripts')
    @if (getenv('APP_ENV') === 'production')
        <script src="{{ cdn('js/ajax-pagination.min.js') }}"></script>
    @else
        <script src="{{ cdn('js_dev/ajax-pagination.js') }}"></script>
    @endif
@endpush

@section('content')

    @if($products->count())
        @include('shop.product._filter', ['section' => 'products'])
        @include('_pagination', ['paginator' => $products])
    @endif

    <div class="grid-products">
        @include('shop.product._page', ['products' => $products])

        @if ($products->count())
            @include('_ajax_pagination', ['paginator' => $products])
        @endif
    </div>

@endsection