@extends('layouts.app')

@section('seo-info')
    <title>{{ Translate::get($product->translations, 'meta_title') }}</title>
    <meta property="og:site_name" content="Kievdelivery">
    <meta property="og:url" content="{{ route('shop.product', ['alias' => $alias]) }}">
    <meta property="og:title" content="{{ Translate::get($product->translations, 'name') }}">
    <meta property="og:type" content="product">
    <meta property="og:description" content="{{ Translate::get($product->translations, 'meta_description') }}">
    <meta property="og:price:amount" content="{{ Shop::getSalePrice($product, false) }}">
    <meta property="og:price:currency" content="{{ $currencies['current'] }}">
    <meta property="og:image:secure_url" content="{{ 'https:' . cdn(Shop::getImageReview(Shop::getDefaultImage($product), true), false, true) }}">
    <meta property="og:image:secure_url" content="{{ Shop::getImagePreview(Shop::getDefaultImage($product), true) }}">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="{{ Translate::get($product->translations, 'name') }}">
    <meta name="twitter:description" content="{{ Translate::get($product->translations, 'meta_description') }}">

    <meta name="description" content="{{ Translate::get($product->translations, 'meta_description') }}">
    <meta name="keywords" content="{{ Translate::get($product->translations, 'meta_keywords') }}">
    @php
        $reviews_arr = [];
        foreach ($reviews as $review) {
            $reviews_arr[] = [
                '@type' => "Review",
                "author" => $review->name,
                "datePublished" => $review->created_at->format('Y-m-d'),
                "description" => $review->text,
                "reviewRating" => [
                    "@type" => "Rating",
                    "bestRating" => "5",
                    "ratingValue" => $review->rating,
                    "worstRating" => "1"
                ]
            ];
        }
        $reviews_json = json_encode($reviews_arr, JSON_UNESCAPED_UNICODE);
        $seo_category = is_null($category) ? $product->categories->first() : $category;
        $seo_color = $product->relations->where('property_id', '3')->first();
    @endphp

    {{--<script type="application/ld+json">--}}
    {{--{--}}
      {{--"@context": "http://schema.org",--}}
      {{--"@type": "Product",--}}
      {{--"aggregateRating": {--}}
        {{--"@type": "AggregateRating",--}}
        {{--"ratingValue": "{{ $product->avgReviewRating() }}",--}}
        {{--"reviewCount": "{{ $product->reviews->count() }}"--}}
      {{--},--}}
      {{--"description": "{{ Translate::get($product->translations, 'description_short') ?: trans('app.no-description') }}",--}}
      {{--"name": "{{ Translate::get($product->translations, 'name') }}",--}}
      {{--"review": {!! $reviews_json !!}--}}
        {{--}--}}
    {{--</script>--}}

    <script type="application/ld+json">
        {
          "@context": "http://schema.org/",
          "@type": "Product",
          "name": "{{ Translate::get($product->translations, 'name') }}",
          "category": "{{ Translate::get($seo_category->translations, 'name') }}",
          "color": "{{ $seo_color ? Translate::get($seo_color->value->translations, 'name') : 'none' }}",
          "image": "{{ Shop::getImagePreview($product->getDefaultImage(), true) }}",
          "description": "{{ Translate::get($product->translations, 'description_short') ?: trans('app.no-description') }}",
          "sku": "{{ $product->vendor_code }}",
          "mpn": "{{ $product->mpn_code }}",
          "brand": {
            "@type": "Thing",
            "name": "Kievdelivery"
            },
          "review": {!! $reviews_json !!},
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "{{ !$reviews->isEmpty() ? $reviews->avg('rating') : 0 }}",
            "ratingCount": "{{ $product->reviews->count() }}"
          },
          "offers": {
            "@type": "Offer",
            "url": "{{ route('shop.product', ['alias' => $alias]) }}",
            "category": "{{ Translate::get($seo_category->translations, 'name') }}",
            "price": "{{ TorannCurrency::convert($product->price, 'USD', Currency::getUserCurrency(), false) }}",
            "priceCurrency": "{{ Currency::getUserCurrency() }}",
            "priceValidUntil": "2025-11-05",
            "itemCondition": "NewCondition",
            "availability": "InStock",
            "eligibleRegion": "UKR",
            "seller": {
              "@type": "Organization",
              "name": "Kievdelivery"
            }
          }
        }

    </script>
@endsection

@section ('canonical_links')
    <link rel="canonical" href="{{ route('shop.product', ['alias' => $alias]) }}"/>
@endsection

@push('styles')
    {{--<link rel="stylesheet" href="{{ cdn('css/rating.min.css') }}">--}}
    <link rel="stylesheet" href="{{ cdn('css/review-block.min.css') }}">
    {{--<link rel="stylesheet" href="{{ cdn('css/fontello.css') }}">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <style>
        .tabs {
            border-bottom: 3px solid #43c900;
            border-right: 3px solid #43c900;
            border-left: 3px solid #43c900;
            margin-bottom: 50px;
        }

        .tabs-container div.active {
            background-color: #43c900;
            color: white;
        }

        .tabs-container {
            display: flex;
            justify-content: space-between;
            border-bottom: 3px solid #43c900;
        }

        .tabs-container div {
            color: black;
            background-color: white;
            padding: 10px 30px;
            border-top: 3px solid #43c900;
            border-right: 3px solid #43c900;
            border-left: 3px solid #43c900;
            width: 33%;
            text-align: center;
            cursor: pointer;
        }

        .tabs-container div:last-of-type {
            border-right: none;
        }

        .tabs-container div:first-of-type {
            border-left: none;
        }

        .recommended-slider, .gallery-top, .gallery-thumbs {
            overflow: hidden;
        }

        .l-product-present-view .gallery-top  {
            width: 100%;
            height: 100%;
        }

        .l-product-present-view .gallery-top .swiper-slide img {
            border: 2px solid #43c900;
            padding: 5px;
            margin-top: 10px;
            cursor: pointer;
            width: 300px;
            height: 300px;
        }

        .close-image {
            width: max-content;
            height: auto;
            font-weight: bold;
            float: right;
            text-align: center;
            font-size: 30px;
            line-height: 30px;
            padding-right: 5px;
            cursor: pointer;
        }

        .l-product-present-info {
            width: 50%;
        }

        .l-product-present-view .gallery-thumbs {
            height: 100%;
            width: 20%;
            margin-right: 4%;
        }

        .icons-block {
            display: flex;
            margin-top: 50px
        }

        @media screen and (max-width: 615px) {
            .tabs-container {
                flex-direction: column;
            }
            .tabs-container div {
                width: 100%;
            }
        }

        @media screen and (max-width: 569px) {
            .l-product-present-view .gallery-top {
                width: 100%;
                height: 340px;
            }
        }

        @media screen and (max-width: 768px) {
            .gallery-thumbs {
                display: none;
            }

            .icons-block {
                flex-wrap: wrap;
            }

            .l-product-present-info {
                width: 100%;
            }
        }

        @media screen and (min-width: 570px) and (max-width: 767px){
            .l-product-present-view .gallery-top  {
                width: 100%;
            }
        }

        @media (min-width: 570px) {
            .l-product-present-view {
                max-width: 100%;
            }
        }
    </style>
@endpush

@push('scripts')
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script src="{{ cdn('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ cdn('js/rating.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.write-review-btn').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('write-review-btn_disabled');
                $('.review-form').slideDown();
                $('.reviews-list').slideUp();
            });

            $('.review-form__cancel').on('click', function (e) {
                e.preventDefault();

                $('.write-review-btn').removeClass('write-review-btn_disabled');
                $('.reviews-list').slideDown();
                $('.review-form').slideUp();
            });

            $('body').on('click', '.gallery-top .swiper-slide-active', function () {
                var img_url = $(this).data('url');

                 $('.larger-image').html('<div class="close-image">&times;</div>'
                     + '<img src="' + img_url + '">');
                 $('.larger-image-block').show();
            });

            $('body').on('click touchstart', '.close-image', function () {
                 $('.larger-image-block').hide();
            });

            $('.tabs-container div').on('click', function () {
                var block_id = $(this).data('id');

                $('.tabs-info-container > div').hide();
                $('.tabs-info-container div' + block_id).show();

                $('.tabs-container div.active').removeClass('active');
                $(this).addClass('active');
            });

            var slidesPerView = 1;

            if ($(window).width() >= 900) {
                slidesPerView = 4;

                var galleryThumbs = new Swiper('.gallery-thumbs', {
                    direction: 'vertical',
                    spaceBetween: 10,
                    // width: '100px',
                    // loop: true, bug too
                    // centeredSlides: true,
                    slidesPerView: 4,
                    touchRatio: 0.2,
                    slideToClickedSlide: true
                });

                var galleryTop = new Swiper('.gallery-top', {
                    // direction: 'vertical',
                    spaceBetween: 10,
                    // width: '400px',
                    thumbs: {
                        swiper: galleryThumbs
                    }
                });
            } else if ($(window).width() < 900 && $(window).width() > 700) {
                slidesPerView = 2;
            }

            if ($(window).width() < 900) {
                var galleryTop = new Swiper('.gallery-top', {
                    slidesPerView: 1,
                    autoplay: true,
                    speed: 3000,
                    spaceBetween: 20
                });
            }

            var swiper = new Swiper('.recommended-slider', {
                speed: 3000,
                slidesPerView: slidesPerView,
                spaceBetween: 10,
                autoplay: true
            });
        });
    </script>
@endpush

@section('breadcrumbs')
    <?php
    $categories = [];
    if (!is_null($category))
        $categories[] = '<a href="' . route('shop.category', ['alias' => $category->alias]) . '">' . Translate::get($category->translations, 'name') . '</a>';

    array_push($categories, Translate::get($product->translations, 'name'))
    ?>
    {{ Widget::run('Breadcrumbs', $categories) }}
@stop

@section('content')

    <div class="l-product" data-id="{{ $product->id }}">
        <div class="l-product-present" style="justify-content: space-between;">
            <div class="l-product-present-view" style="display: flex; max-width: 50%; margin-right: 15px">
                <div class="gallery-thumbs">
                    <div class="swiper-wrapper">
                        @foreach ($product->images as $image)
                            <div class="swiper-slide"
                                 style="background-image: url('{{ Shop::getImagePreview($image, true) }}');
                                         background-size: contain;
                                         background-repeat: no-repeat;
                                         background-position: center center">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="gallery-top">
                    <div class="swiper-wrapper">
                        @foreach ($product->images as $image)
                            <div class="swiper-slide"
                                 data-url="{{ Shop::getImageReview($image, true) }}"
                                 style="text-align: center;">
                                {!! Shop::getImagePreview($image) !!}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="l-product-present-info">
                <h1 class="l-product-present-title">{{ Translate::get($product->translations, 'name') }}</h1>

                {{--<div class="l-product-present-rating" style="width: 200px">--}}
                    {{--<span class="l-product-present-code" style="font-weight: bold">{{ trans('shop.product-code') }}--}}
                        {{--: {{ $product->vendor_code }}</span>--}}
                    {{--<span class="rating-stars rs-{{ $product->avgReviewRating() ? $product->avgReviewRating() : 0 }}"></span>--}}
                {{--</div>--}}
                <div class="l-product-present-description product-card__cod-and-rating" style="display: flex; flex-direction: column;">
                    <span class="l-product-present-code" style="font-weight: bold">{{ trans('shop.product-code') }}
                        : {{ $product->vendor_code }}</span>
                    <span class="rating-stars rs-{{ $product->avgReviewRating() ? $product->avgReviewRating() : 0 }}"></span>
                </div>
                <div class="cart-add" style="justify-content: space-around">
                    <div class="l-product-present-price">
                        {!! Shop::getSalePrice($product) ?: trans('app.no-data') !!}
                    </div>
                    @if ($product->amount > 0)
                        {{--<div class="cart-add-amount" style="margin-left: 0">--}}
                            {{--<button class="cart-add-amount-button" data-amount="plus">+</button>--}}
                            <input class="input-number" type="hidden" min="1" id="product-amount" value="1">
                            {{--<button class="cart-add-amount-button" data-amount="minus">-</button>--}}
                        {{--</div>--}}
                        <button class="btn btn-cart add-to-cart">{{ trans('shop.to-cart') }}</button>
                    @else
                        <span style="padding: 10px 5px; margin-left: 8px; text-transform: uppercase; color: #f33875;">{{ trans('shop.not-available') }}</span>
                    @endif
                    {{--<button class="popup-with-form btn btn-warning" data-url="{{ route('cart.buy-now') }}">{{ trans('shop.buy-now') }}</button>--}}
                </div>
                <div class="icons-block">
                    <div style="display: flex; flex-direction: column; margin-right: 50px; margin-bottom: 15px">
                        <div style="display:flex; align-items: center; margin-bottom: 15px">
                            <img src="{{ cdn('img/gift.svg') }}" alt=""
                                 style="margin-right: 15px; width: 40px">
                            <span>{{ trans('shop.courier_hand_delivery') }}</span>
                        </div>
                        <div style="display:flex; align-items: center">
                            <img src="{{ cdn('img/appointment.svg') }}" alt=""
                                 style="margin-right: 15px; width: 40px">
                            <span>{{ trans('shop.in-time_delivery') }}</span>
                        </div>
                    </div>
                    <div style="display: flex; flex-direction: column">
                        <div style="display:flex; align-items: center; margin-bottom: 15px">
                            <img src="{{ cdn('img/shop.svg') }}" alt=""
                                 style="margin-right: 15px; width: 40px">
                            <span>{{ trans('shop.local_florist') }}</span>
                        </div>
                        <div style="display:flex; align-items: center">
                            <img src="{{ cdn('img/bouquet.svg') }}" alt=""
                                 style="margin-right: 15px; width: 40px">
                            <span>{{ trans('shop.fresh_flowers') }}</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="tabs">
            <div class="tabs-container">
                <div data-id="#tab1" class="active">{{ trans('shop.product_details') }}</div>
                <div data-id="#tab2">{{ trans('shop.guarantees') }}</div>
                <div data-id="#tab3">{{ trans('shop.delivery_policy') }}</div>
            </div>
            <div style="padding: 10px 25px;" class="tabs-info-container">
                <div id="tab1" class="l-product-description {{ Translate::get($product->translations, 'description_full') ? '' : 'empty'}}">
                    {!! Translate::get($product->translations, 'description_full') ?: trans('app.no-description')!!}
                </div>
                <div id="tab2" class="l-product-description {{ Translate::get($product->translations, 'guarantees') ? '' : 'empty'}}"
                    style="display: none">
                    {!! Translate::get($product->translations, 'guarantees') ?: trans('app.no-description')!!}
                </div>
                <div id="tab3" class="l-product-description {{ Translate::get($product->translations, 'delivery_policy') ? '' : 'empty'}}"
                    style="display: none">
                    {!! Translate::get($product->translations, 'delivery_policy') ?: trans('app.no-description')!!}
                </div>
            </div>
        </div>

        @if ($productsRecommended->count())
            <div class="l-product-recommended" style="text-align: center; margin-bottom: 50px">
                <h3 class="l-product-recommended-title">{{ trans('site.recommended-products') }}</h3>
{{--                <div class="l-product-recommended-description">{{ trans('site.recommended-products-description') }}</div>--}}
                {{--<div class="">--}}
                    <div class="recommended-slider">
                        <div class="swiper-wrapper">
                            @foreach ($productsRecommended as $recommendedProduct)
                                <div class="swiper-slide">
                                    {!! view('shop.product._item', ['product' => $recommendedProduct]) !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                {{--</div>--}}
            </div>
        @endif

        @include('includes.review', ['reviews' => $reviews, 'product' => $product])

        @if (!$productsRecentlyViewed->isEmpty())
            <div class="l-product-recommended" style="text-align: center; margin: 50px 0">
                <h3 class="l-product-recommended-title">{{ trans('site.recently-viewed') }}</h3>
                <div class="recommended-slider">
                    <div class="swiper-wrapper">
                        @foreach ($productsRecentlyViewed as $productRecentlyViewed)
                            <div class="swiper-slide">
                                {!! view('shop.product._item', ['product' => $productRecentlyViewed]) !!}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection


