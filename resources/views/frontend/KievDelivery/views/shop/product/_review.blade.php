<div class="reviews-block">
    <h4 class="title">{{ trans('site.reviews-by-products') }}</h4>

    <ul>
        @forelse($reviews as $review)
            <li>
                <div class="reviews-block-section">
                    <div class="reviews-block-section-face"
                         style="background-image: url({{User::getAvatarUrlByUser($review->user)}})"></div>
                    <div class="reviews-block-section-info">
                        <div class="reviews-block-section-name">{{User::getNameByUser($review->user)}}</div>
                        <div class="reviews-block-section-rating">
                            <span class="rating-stars rs-{{$review->rating}}"></span>
                        </div>
                        <div class="reviews-block-section-text deploy-text" data-btn-text="{{ trans('site.read-full') }}">
                            {{$review->text}}
                        </div>
                    </div>
                </div>
            </li>
        @empty

        @endforelse

        @if ($reviews->count())
            @include('_pagination', ['paginator' => $reviews])
        @endif

    </ul>
</div>
