<div class="product-short product-with-note l-product" data-id="<?=$product->id?>">
    @if ($product->sale_category && !is_null(app()->make(\App\Support\DiscountContainer::class)->current()))
        <div class="product-short-product-note">
            <p>{{ trans('shop.discount') }} {{ app()->make(\App\Support\DiscountContainer::class)->current()->percents }}%</p>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="118" height="39" viewBox="0 0 118 39">
                <image id="glavnaya_red.svg" width="118" height="39" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHYAAAAnCAMAAADtq0xxAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAxlBMVEX6PHr////6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHr6PHoAAABb5nCDAAAAQHRSTlMAACZ2vOb5P8IImP4SxgrIm8Mvgb6/gkCgmUWSExzSZfHFBZ/8+ud3KCvQqFnw0e5Vlv0M4BGPZA1g4gPbcCJSIVwvLwAAAAFiS0dEQYnebE4AAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiCQ0KDC9i0V0GAAABHElEQVRYw+3XSXOCQBAF4CcKOqADLolZTDQh+x6y7/3/f1WYERM95MjrS96BKuTwFUXb3QNUCZqtMJIaE4WtZoDVtDt1ir/ptJdQEyccVSSJzULt9lioS687V23KVEVS69mMq4pkvppo33WRxNVVn62K9IEBXxUZYKjBDjHSYNcQarAhau3DfyWChiryzzJYpZJS+gMptQuV5riuNQqUBp/GmB+79WKDzW76XWqL/LrVCodtqptOqj15Z5eHJrH9ORVMZyx1b7x0GNnP5z8e1CkeHo2OT1ZPXqdn/sm5BSuNhrteXDr2KnD3jFSs8a35+obMwtyWg7fI2SzuskLk3rBZPDyWpfxEZ/H8MiteLZ3F2yR//+CzZT6/OOw3IbBZNda91+sAAAAASUVORK5CYII="/>
            </svg>

        </div>
    @endif
    @if ($product->kyiv_only_category)
        <div class="product-short-product-note" style="left: auto; right: 0">
            <p>{{ trans('shop.kyiv_only') }}</p>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="118" height="39" viewBox="0 0 118 39">
                <image id="glavnaya_red.svg" width="118" height="39" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHYAAAAnAQMAAADguy4AAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAZQTFRFAAAASDbbQ/P/ngAAAAJ0Uk5TAP9bkSK1AAAAXklEQVR4nN3RMRHAMAwDQPUCoBACJdBSaIFiCBkz5Op2tOS7Aqi23ywL8JANlGgHTvJAJRsaeSZ38komvgf8zZ2c+zdy/l8lm+xhstcA1DF+ifHtm3ls8WKXKTYx8AAXHLhlTQiEJAAAAABJRU5ErkJggg=="/>
            </svg>
        </div>
    @endif
    <div class="product-short-preview">
        <a href="{{ route('shop.product', ['alias' => $product->alias]) }}">
            {!! Shop::getImagePreview($product->getDefaultImage()) !!}
            <span class="product-preview-cover" style="background-image: url({!! Shop::getImagePreview($product->getDefaultImage(), true) !!})"></span>
        </a>
    </div>

    <div class="product-short-name">{{ Translate::get($product->translations, 'name') }}</div>

    <div class="product-short-price">
        {!! Shop::getSalePrice($product) ?: trans('app.no-data') !!}
    </div>

    <div class="product-short-extra">
        <a href="{{ route('shop.product', ['alias' => $product->alias]) }}" class="btn product-short__more-btn">{{ trans('app.more') }}</a>
        @if ($product->amount > 0)
            <button class="btn btn-warning add-to-cart add-to-cart__btn">
                <span class="add-to-cart__icon d-none"></span>
                <span class="add-to-cart__text">{{ trans('shop.to-cart') }}</span>
            </button>
        @else
            <span style="padding: 10px 5px; margin-left: 8px; text-transform: uppercase; color: #f33875;">{{ trans('shop.not-available') }}</span>
        @endif
    </div>

</div>
