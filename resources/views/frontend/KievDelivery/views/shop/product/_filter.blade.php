<?php
/** @var $section string  */
/** @var $alias string  */
?>

<?php
    $route = 'shop.products';
    if ($section === 'category') {
        if (isset($alias)) {
            $route = ['shop.category', 'alias' => $alias];
        }
    }
?>

<form method="get" action="#" id="filter-form">
    <div class="filter">
        <div class="filter-order">
            <div class="filter-group">
                {{ trans('shop.sorting-verb') }}:
                <select name="filter[order]">
                    <option {{ in_array('price_asc', isset($filter['sort']) ? $filter['sort'] : []) ? 'selected="selected"' : '' }}
                            value="{{ Shop::routeFilter($route, ['sort' => 'price_asc']) }}" data-order="asc">
                        {{ trans('shop.sort-by-price-ascending') }}
                    </option>
                    <option {{ in_array('price_desc', isset($filter['sort']) ? $filter['sort'] : []) ? 'selected="selected"' : '' }}
                            value="{{ Shop::routeFilter($route, ['sort' => 'price_desc']) }}" data-order="desc">
                        {{ trans('shop.sort-by-price-descending') }}
                    </option>
                </select>
            </div>
            <div class="filter-group">
                {{ trans('shop.showing-verb') }}:
                <select name="filter[count]">
                    <option {{ in_array(16, isset($filter['count']) ? $filter['count'] : []) ? 'selected="selected"' : '' }} value="{{ Shop::routeFilter($route, ['count' => 16]) }}">16</option>
                    <option {{ in_array(32, isset($filter['count']) ? $filter['count'] : []) ? 'selected="selected"' : '' }} value="{{ Shop::routeFilter($route, ['count' => 32]) }}">32</option>
                    <option {{ in_array(64, isset($filter['count']) ? $filter['count'] : []) ? 'selected="selected"' : '' }} value="{{ Shop::routeFilter($route, ['count' => 64]) }}">64</option>
                </select>
            </div>
        </div>
    </div>
</form>