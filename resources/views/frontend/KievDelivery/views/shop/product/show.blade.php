@extends('layouts.app')

@section('seo-info')
    <title>{{ Translate::get($product->translations, 'meta_title') }}</title>
    <meta name="description" content="{{ Translate::get($product->translations, 'meta_description') }}">
  <meta name="keywords" content="{{ Translate::get($product->translations, 'meta_keywords') }}">
    @php
        $reviews_arr = [];
        foreach ($reviews as $review) {
            $reviews_arr[] = [
                '@type' => "Review",
                "author" => $review->name,
                "datePublished" => $review->created_at->format('Y-m-d'),
                "description" => $review->text,
                "reviewRating" => [
                    "@type" => "Rating",
                    "bestRating" => "5",
                    "ratingValue" => $review->rating,
                    "worstRating" => "1"
                ]
            ];
        }
        $reviews_json = json_encode($reviews_arr, JSON_UNESCAPED_UNICODE);
    @endphp
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Product",
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "{{ $product->avgReviewRating() }}",
        "reviewCount": "{{ $product->reviews->count() }}"
      },
      "description": "{{ Translate::get($product->translations, 'description_short') ?: trans('app.no-description') }}",
      "name": "{{ Translate::get($product->translations, 'name') }}",
      "review": {!! $reviews_json !!}
    }
    </script>
@endsection

@section ('canonical_links')
    <link rel="canonical" href="{{ route('shop.product', ['alias' => $alias]) }}"/>
@endsection

@push('styles')
{{--<link rel="stylesheet" href="{{ cdn('css/rating.min.css') }}">--}}
<link rel="stylesheet" href="{{ cdn('css/review-block.min.css') }}">
{{--<link rel="stylesheet" href="{{ cdn('css/fontello.css') }}">--}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js"></script>
<script src="{{ cdn('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ cdn('js/rating.min.js') }}"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>--}}
<script>
    $(document).ready(function () {
        $('.write-review-btn').on('click', function (e) {
            e.preventDefault();

            $(this).addClass('write-review-btn_disabled');
            $('.review-form').slideDown();
            $('.reviews-list').slideUp();
        });

        $('.review-form__cancel').on('click', function (e) {
            e.preventDefault();

            $('.write-review-btn').removeClass('write-review-btn_disabled');
            $('.reviews-list').slideDown();
            $('.review-form').slideUp();
        });
    });
</script>
@endpush

@section('breadcrumbs')
    <?php
        $categories = [];
        if (!is_null($category))
            $categories[] = '<a href="'.route('shop.category', ['alias' => $category->alias]).'">'.Translate::get($category->translations, 'name').'</a>';

        array_push($categories, Translate::get($product->translations, 'name'))
    ?>
    {{ Widget::run('Breadcrumbs', $categories) }}
@stop

@section('content')

    <div class="l-product" data-id="{{ $product->id }}">
        <div class="l-product-present">
            <div class="l-product-present-view">
                <div class="l-product-present-image"
                     data-url="{{ Shop::getImageReview($product->getDefaultImage(), true) }}"
                     style="background-image: url({{ Shop::getImagePreview($product->getDefaultImage(), true) }})"></div>
                <div class="swiper-container" id="product-preview">
                    <div class="swiper-wrapper">
                        @foreach ($product->images as $image)
                            <div class="swiper-slide {{ $product->images->first() == $image ? ' is-active' : '' }}"
                                 data-url="{{ Shop::getImageReview($image, true) }}"
                                 style="background-image: url({{ Shop::getImagePreview($image, true) }})">
                            </div>
                        @endforeach
                    </div>
                    <!-- <div class="swiper-pagination"></div> -->
                    <!-- <div class="swiper-button-next"></div> -->
                    <!-- <div class="swiper-button-prev"></div> -->
                </div>
            </div>
            <div class="l-product-present-info">
                <div class="l-product-present-rating" style="width: 200px">
                    <span class="l-product-present-code" style="font-weight: bold">{{ trans('shop.product-code') }}: {{ $product->vendor_code }}</span>
                    <span class="rating-stars rs-{{ $product->avgReviewRating() ? $product->avgReviewRating() : 0 }}"></span>
                </div>
                <h1 class="l-product-present-title">{{ Translate::get($product->translations, 'name') }}</h1>

                {{--<div class="l-product-present-description">--}}
                    {{--{{ trans('shop.product-code') }}: {{ $product->vendor_code }}--}}
                {{--</div>--}}
                <div class="l-product-present-price">
                    {!! Shop::getSalePrice($product) ?: trans('app.no-data') !!}
                </div>
                <div class="cart-add">
                    @if ($product->amount > 0)
                        <div class="cart-add-amount" style="margin-left: 0">
                            <button class="cart-add-amount-button" data-amount="plus">+</button>
                            <input class="input-number" type="text" min="1" id="product-amount" value="1">
                            <button class="cart-add-amount-button" data-amount="minus">-</button>
                        </div>
                        <button class="btn btn-cart add-to-cart">{{ trans('shop.to-cart') }}</button>
                    @else
                        <span style="padding: 10px 5px; margin-left: 8px; text-transform: uppercase; color: #f33875;">{{ trans('shop.not-available') }}</span>
                    @endif
                    {{--<button class="popup-with-form btn btn-warning" data-url="{{ route('cart.buy-now') }}">{{ trans('shop.buy-now') }}</button>--}}
                </div>
                {{--<ul class="cart-social">--}}
                    {{--<li>--}}
                        {{--<button class="cart-share fb" style="background-image: url('{{ cdn('img/socials/facebook.svg') }}')"></button>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<button class="cart-share tw" style="background-image: url('{{ cdn('img/socials/twitter.svg') }}')" data-title="{{ Translate::get($product->translations, 'name') }}"></button>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<button class="cart-share pt" style="background-image: url('{{ cdn('img/socials/pinterest-box.svg') }}')" data-title="{{ Translate::get($product->translations, 'name') }}" data-image="{{ Shop::getImageReview($product->getDefaultImage(), true) }}"></button>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<button class="cart-share gp" style="background-image: url('{{ cdn('img/socials/google.svg') }}')"></button>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            </div>

            <script type="application/ld+json">
            {
              "@context": "http://schema.org/",
              "@type": "Product",
              "name": "{{ Translate::get($product->translations, 'name') }}",
              "image": "{{ Shop::getImagePreview($product->getDefaultImage(), true) }}",
              "description": "{{ Translate::get($product->translations, 'description_short') }}",
              "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "{{ $product->rating ? $product->rating : 5 }}",
                "ratingCount": "{{ count($product->reviews) ? count($product->reviews) : 1 }}"
              },
              "offers": {
                "@type": "Offer",
                "price": "{{ TorannCurrency::convert($product->price, 'USD', Currency::getUserCurrency(), false) }}",
                "priceCurrency": "{{ Currency::getUserCurrency() }}"
              }
            }
            </script>
        </div>

        <div class="l-product-description {{ Translate::get($product->translations, 'description_full') ? '' : 'empty'}}">
            {{-- <h2 class="l-product-description-title">Заголовок</h2> --}}
            {!! Translate::get($product->translations, 'description_full') ?: trans('app.no-description')!!}
        </div>

        @if ($productsRecommended->count())
            <div class="l-product-recommended">
                <h3 class="l-product-recommended-title">{{ trans('site.recommended-products') }}:</h3>
                <div class="l-product-recommended-description">{{ trans('site.recommended-products-description') }}</div>
                <div class="grid-products">
                    @foreach ($productsRecommended as $recommendedProduct)
                        {!! view('shop.product._item', ['product' => $recommendedProduct]) !!}
                    @endforeach
                </div>
            </div>
        @endif
        {{--@if ($reviews->count())--}}
            {{--{!! view('shop.product._review', ['reviews' => $reviews, 'product' => $product]) !!}--}}
        {{--@endif--}}

        @include('includes.review', ['reviews' => $reviews, 'product' => $product])
        {{--<div class="reviews-block-form">--}}
            {{--<div class="tabs_controls" style="border-bottom: 1px solid #ddd">--}}
                {{--<div class="tab-caption"--}}
                     {{--style="border: 1px solid #ddd; border-radius: 3px 3px 0 0; padding: 5px 10px; display: inline-block; border-bottom: none; margin-bottom: -1px; background: #fff; font-size: 16px;">{{ trans('site.reviews-by-products') }}--}}
                    {{--({{ $reviews->count() }})--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--@if ($reviews->count() > 0)--}}
                {{--<div class="reviews-stats">--}}
                {{--<div class="cell">--}}
                    {{--<b>Average rating</b>--}}
                    {{--<div>--}}
                        {{--<span class="rating-static rating{{ $product->avgReviewRating() }}"></span>--}}
                    {{--</div>--}}
                    {{--<span class="rating-value" itemscope="" itemtype="http://data-vocabulary.org/Rating">--}}
                    {{--<span class="item-value" itemprop="average">{{ $product->avgReviewRating() }}</span> /--}}
                    {{--<span itemprop="best">{{ $product->avgReviewRating() }}</span>--}}
                    {{--</span>--}}
                    {{--<div class="text-muted">Based on {{ $reviews->count() }} reviews</div>--}}
                {{--</div>--}}
                {{--<div class="cell">--}}
                    {{--<ul class="rating-list">--}}
                        {{--@for ($i = 5; $i > 1; $i--)--}}
                            {{--<li>--}}
                                {{--<div class="item-count">{{ $i }} star{{ $i == 1 ? '' : 's' }}</div>--}}
                                {{--<div class="item-scale-box">--}}
                                    {{--<div class="item-scale">--}}
                                        {{--<div class="item-value"--}}
                                             {{--style="width: {{ $reviews->where('rating', $i)->count()/$reviews->count() * 100 }}%;"></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="item-count-value">({{ $reviews->where('rating', $i)->count() }})</div>--}}
                            {{--</li>--}}
                        {{--@endfor--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="cell">--}}
                    {{--Write a review to help other buyers with a choice!--}}
                    {{--<p><a href="#" class="btn btn-success disabled" onclick="writeReview(); event.preventDefault()">Write--}}
                            {{--the review</a></p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--@endif--}}
            {{--<span class="reviews-block-form-rating"></span>--}}
            {{--{{ Form::open(['route' => ['review.store']]) }}--}}
            {{--<div class="form-group">--}}
                {{--{{ Form::hidden('product_id', $product->id) }}--}}
                {{--{{ Form::hidden('rating', 3, ['id' => 'review-rating']) }}--}}
                {{--<label for="">Comment</label>--}}
                {{--{{ Form::textarea('text', null, ['class' => 'form-control']) }}--}}
                {{--<div class="help-block"> {{ $errors->has('text') ? $errors->first('text') : '' }}</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="">Your name</label>--}}
                {{--{{ Form::text('name', null, ['class' => 'form-control']) }}--}}
                {{--<div class="help-block"> {{ $errors->has('name') ? $errors->first('name') : '' }}</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="">Your Email (not published)</label>--}}
                {{--{{ Form::text('email', null, ['class' => 'form-control']) }}--}}
                {{--<div class="help-block"> {{ $errors->has('email') ? $errors->first('email') : '' }}</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="">City of delivery</label>--}}
                {{--{{ Form::text('city_delivery', \Illuminate\Support\Facades\Cookie::get('city_alias'), ['class' => 'form-control']) }}--}}
                {{--<div class="help-block"> {{ $errors->has('city_delivery') ? $errors->first('city_delivery') : '' }}</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--{!! Recaptcha::render([ 'lang' => App::getLocale() ]) !!}--}}
                {{--<div class="help-block">--}}
                    {{--{{ $errors->has('g-recaptcha-response') ? $errors->first('g-recaptcha-response') : '' }}--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<button type="submit" class="btn btn-warning">{{ trans('app.send') }}</button>--}}
            {{--{{ Form::close() }}--}}
        {{--</div>--}}
    </div>

@endsection


