@if($products->count())
    @each('shop.product._item', $products, 'product')
@else
    {{ Request::ajax() ? '' : trans('app.no-data') }}
@endif