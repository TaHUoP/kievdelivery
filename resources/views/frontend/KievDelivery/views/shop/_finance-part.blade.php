<div id="finance-part">

    <div class="section section-padding">
        <div class="section-title">{{ trans('shop.attr.discount_code', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</div>
    @if(!empty($cart['discount_code']))
         <div class="form-group">
            <span>{{ trans('shop.coupon-in-use', [], 'messages', isset($currentLocale) ? $currentLocale : null) }} <b>{{ $cart['discount_code'] }}</b></span>
                &nbsp; &nbsp;
            <button type="button" class="btn btn-light" id="delete-coupon">{{ trans('shop.coupon-delete', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</button>
         </div>
    @else
        <div class="form-group">
            {{ Form::text('checkout[discount_code]', null, [
                'class' => 'form-control',
                'placeholder' => trans('site.enter-your-coupon-code', [], 'messages', isset($currentLocale) ? $currentLocale : null),
                'id'=>'coupon-code'
            ]) }}
            <button type="button" class="btn btn-light" id="add-coupon">{{ trans('app.apply_code', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</button>
            <div class="help-block no-padding"></div>
        </div>
    @endif
    </div>

    <div class="checkout-cart">

        <div class="section-title">{{ trans('shop.cart', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</div>

        <div class="checkout-cart-list">

            <div class="checkout-cart-list-header" style="margin-bottom: 0">
                <span class="checkout-cart-list-item-code"><span>{{ trans('shop.attr.code', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span></span>
                <span class="checkout-cart-list-item-code"><span>{{ trans('shop.attr.photo', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span></span>
                <span class="checkout-cart-list-item-name">{{ trans('shop.attr.product_name', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="checkout-cart-list-item-quantity">{{ trans('shop.attr.quantity', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="checkout-cart-list-item-price">{{ trans('shop.attr.total', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
            </div>

            <div class="checkout-cart-list-body">

                @foreach($cart['products'] as $relation)
                    <div class="checkout-cart-list-body-item" style="align-items: center;">
                        <span class="checkout-cart-list-item-code">{{ $relation['product']['vendor_code'] }}</span>
                        <span class="checkout-cart-list-item-code">{!! Shop::getImagePreview($relation->product->getDefaultImage()) !!}</span>
                        <span class="checkout-cart-list-item-name">
                        <a href="{{ route('shop.product', ['alias' => $relation['product']['alias']]) }}">
                            {{ Translate::get($relation['product']['translations'], 'name') }}
                        </a>
                    </span>
                        <span class="checkout-cart-list-item-quantity">{{ $relation['amount'] }}</span>
                        <span class="checkout-cart-list-item-price">
                        {{ Currency::getMoneyFormat(Shop::getPrice($relation['product'], false) * $relation['amount']) }}
                    </span>
                    </div>
                @endforeach

                <div class="checkout-cart-list-body-item-info">
                    <div class="checkout-cart-list-body-item">
                        <span class="checkout-cart-list-item-name">{{ trans('site.your-discount') }}</span>
                        <span class="checkout-cart-list-item-price checkout-cart-list-item-price-discount">
                            {{ Currency::getMoneyFormat($cart['discount'], Currency::getUserCurrency(), 2) }}
                    </span>
                    </div>
                    <div class="checkout-cart-list-body-item">
                        <span class="checkout-cart-list-item-name">{{ trans('shop.delivery-price', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                        <span id="delivery-price" class="checkout-cart-list-item-price checkout-cart-list-item-price-discount">
{{--                            @if($cart['total_price'] > Config::get('static.deliveryPriceBorder') && !\App\Helpers\StaticsHelper::getCityFee())--}}
{{--                                {{ trans('app.free', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}--}}
{{--                            @elseif($cart['total_price'] > Config::get('static.deliveryPriceBorder') && \App\Helpers\StaticsHelper::getCityFee())--}}
                                {{ Currency::getMoneyFormat(\App\Helpers\StaticsHelper::getCityFee(), Currency::getUserCurrency(), 2) }}
                            {{--@else--}}
{{--                                {{ Currency::getMoneyFormat(Config::get('static.deliveryPrice') + \App\Helpers\StaticsHelper::getCityFee(), Currency::getUserCurrency(), 2) }}--}}
                            {{--@endif--}}
                        </span>
                    </div>
                    <div class="checkout-cart-list-body-item" style="border-top: 1px solid green; background-color: #85cc2963; border-bottom: 1px solid green; align-items: center;">
                        <span class="checkout-cart-list-item-name">{{ trans('site.total-for-order', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                        <span id="total-price" class="checkout-cart-list-item-price checkout-cart-list-item-price-total">
{{--                            @if($cart['total_price'] > Config::get('static.deliveryPriceBorder') && !\App\Helpers\StaticsHelper::getCityFee())--}}
                                {{ Currency::getMoneyFormat($cart['total_price'] + \App\Helpers\StaticsHelper::getCityFee() - $cart['discount'], Currency::getUserCurrency(), 2) }}
{{--                            @elseif($cart['total_price'] > Config::get('static.deliveryPriceBorder') && \App\Helpers\StaticsHelper::getCityFee())--}}
{{--                                {{ Currency::getMoneyFormat($cart['total_price'] + \App\Helpers\StaticsHelper::getCityFee() - $cart['discount'], Currency::getUserCurrency(), 2) }}--}}
                            {{--@else--}}
{{--                                {{ Currency::getMoneyFormat($cart['total_price'] + Config::get('static.deliveryPrice') + \App\Helpers\StaticsHelper::getCityFee() - $cart['discount'], Currency::getUserCurrency(), 2) }}--}}
                            {{--@endif--}}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="checkout-cart-total-pay">
            <div style="display: flex; flex-direction: column">
                <div class="form-group {{ $errors->has('agree') ? 'has-error' : '' }}">
                    <div class="help-block">
                        <label class="control-label" style="width: auto; font-size: 120%">
                            {{ Form::checkbox('agree', null) }}
                            {{ trans('site.read_agree', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}
                            &nbsp;
                            <a href="{{ route('site.page', ['url' => 'delivery-policy']) }}" target="_blank">
                                {{ trans('site.read_agree_continue', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}
                            </a>
                        </label>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('agree_nbu') ? 'has-error' : '' }}">
                    <div class="help-block">
                        <label class="control-label platon-chk" style="width: auto; font-size: 120%">
                            {{ Form::checkbox('agree_nbu', null) }}
                            <span>
                            {{ trans('site.agree_nbu', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}
                            &nbsp;
                            <a href="https://bank.gov.ua/control/en/allinfo" target="_blank">
                                {{ trans('site.learn_more', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}
                            </a>
                                </span>
                        </label>
                        <label class="control-label paypal-chk" style="width: auto; font-size: 120%; display: none">
                            {{ Form::checkbox('agree_nbu', null) }}
                            {{ trans('site.agree_paypal', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}
                        </label>
                    </div>
                </div>
            </div>
            <button class="btn btn-warning">{{ trans('app.to_order', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</button>
        </div>
    </div>
</div>