@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('users.orders-history') }}</title>
    <meta name="description" content="{{ trans('users.orders-history') }}">
    <meta name="keywords" content="{{ trans('users.orders-history') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('shop.checkout.orders').'">' . trans('users.orders-history') . '</a>',
    ]) }}
@stop

@section('content')

    <div class="l-profile">

        <div class="l-profile-steps">
            <a href="{{ route('user.index') }}">{{ trans('users.my-account') }}</a>
            <a href="{{ route('shop.checkout.orders') }}" class="is-active">{{ trans('users.orders-history') }}</a>
        </div>

        <div class="order-history">
            <div class="section-title">{{ trans('shop.order-history') }}</div>

            <div class="order-history-list">

                <div class="order-history-list-header">
                    <span class="order-history-list-item-code"><span>{{ trans('app.code') }}</span></span>
                    <span class="order-history-list-item-date">{{ trans('app.date') }}</span>
                    <span class="order-history-list-item-quantity">{{ trans('app.quantity') }}</span>
                    <span class="order-history-list-item-status">{{ trans('app.attr.status') }}</span>
                    <span class="order-history-list-item-status">{{ trans('app.attr.paid') }}</span>
                    <span class="order-history-list-item-price">{{ trans('app.total') }}</span>
                </div>
                <div class="order-history-list-body">

                    @forelse ($checkouts as $checkout)

                        <a href="{{ route('shop.checkout.order', ['id' => $checkout->id]) }}">
                            <div class="order-history-list-body-item">
                                <span class="order-history-list-item-code"># {{ $checkout->id }}</span>
                                <span class="order-history-list-item-date">
                                    {!! DateTimeHelper::toFullDateTime($checkout->created_at) ?: trans('app.no-data') !!}
                                </span>
                                <span class="order-history-list-item-quantity">{{ $checkout->cart->products->count() }}</span>
                                <span class="order-history-list-item-status">{{ $checkout->status_name}}</span>
                                <span class="order-history-list-item-status">{{ trans($checkout->paid ? 'app.yes' : 'app.no' )}}</span>
                                <span class="order-history-list-item-price">{{ Currency::getMoneyFormat($checkout->total) }}</span>
                            </div>
                        </a>

                    @empty

                        <p>{{ trans('app.no-data') }}</p>

                    @endforelse

                </div>
            </div>

            @if ($checkouts->count())
                @include('_pagination', ['paginator' => $checkouts])
            @endif

        </div>

    </div>

@endsection