@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('shop.cart') }}</title>
    <meta name="description" content="{{ trans('shop.cart') }}">
    <meta name="keywords" content="{{ trans('shop.cart') }}">
    <meta name="robots" content="noindex, nofollow"/>
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.cart')
    ]) }}
@stop

@section('content')

    <div id="cart-section">
        @include('shop.cart._cart')
    </div>

    @if ($productsRecommended->count())
        <div class="l-product-recommended" style="text-align: center; margin-bottom: 50px">
            <h3 class="l-product-recommended-title">{{ trans('site.recommended-products') }}</h3>
            <div class="recommended-slider" style="overflow: hidden">
                <div class="swiper-wrapper">
                    @foreach ($productsRecommended as $recommendedProduct)
                        <div class="swiper-slide">
                            {!! view('shop.product._item', ['product' => $recommendedProduct]) !!}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script>
        var slidesPerView = 1;

        if ($(window).width() >= 900) {
            slidesPerView = 4;
        } else if ($(window).width() < 900 && $(window).width() > 700) {
            slidesPerView = 2;
        }

        var swiper = new Swiper('.recommended-slider', {
            speed: 3000,
            slidesPerView: slidesPerView,
            spaceBetween: 10,
            autoplay: true
        });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
@endpush