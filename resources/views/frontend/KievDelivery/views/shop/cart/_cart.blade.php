@if ($cart && $cart->products->count())

    <h3 class="title title-indent-min">{{ trans('shop.checkout', [], 'messages', isset($currentLocale) ? $currentLocale : null) }} </h3>

    <div class="l-cart">

        <div class="l-cart-steps">
            <span class="is-active">{{ trans('shop.cart-products', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
            <span>{{ trans('shop.payment', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
        </div>
        <div class="l-cart-list">
            <div class="l-cart-list-header">
                <span class="l-cart-list-item-name">{{ trans('shop.attr.name', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="l-cart-list-item-desc">{{ trans('shop.attr.desc', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="l-cart-list-item-price">{{ trans('shop.attr.price', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="l-cart-list-item-amount">{{ trans('shop.attr.amount', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="l-cart-list-item-total">{{ trans('shop.attr.sum', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</span>
                <span class="l-cart-list-item-remove"></span>
            </div>
            <div class="l-cart-list-body">
                @foreach($cart->products as $relation)
                    <div class="l-cart-list-item" data-id="{{ $relation->product->id }}">
                        <span class="l-cart-list-item-name">{!! Shop::getImagePreview(Shop::getDefaultImage($relation->product)) !!}</span>
                        <div class="l-cart-list-item-desc">
                            <a href="{{ route('shop.product', ['alias' => $relation->product->alias]) }}">
                                {{ Translate::get($relation->product->translations, 'name', (isset($currentLocale) ? $currentLocale : null)) }}</a>
                            <div class="l-cart-list-item-desc-text">
                                {{ Translate::get($relation->product->translations, 'description_short', (isset($currentLocale) ? $currentLocale : null))
                                    ?: trans('app.no-description', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}
                            </div>
                        </div>
                        <span class="l-cart-list-item-price">
                           {!! Shop::getSalePrice($relation->product) ?: trans('app.no-data', [], 'messages', isset($currentLocale) ? $currentLocale : null) !!}
                        </span>
                        <span class="l-cart-list-item-amount">
                            <input type="number" min="1" value="<?=$relation->amount?>" step="1" class="form-control cart-products-amount"/>
                        </span>
                        <span class="l-cart-list-item-total">{{ Currency::getMoneyFormat(Shop::getSalePrice($relation->product, false) * $relation->amount) }}</span>
                        <span class="l-cart-list-item-remove"><button></button></span>
                    </div>
                @endforeach
            </div>
        </div>

        @if(! $cart->isPaymentSystemAllowed('platon'))
            <div style="margin: 30px 0; padding-right: 44px; text-align: right; font-weight: 600; font-size: 18px">
                {!! trans('shop.payment_system_allowed', [], 'messages', isset($currentLocale) ? $currentLocale : null) !!}</div>
        @endif
        <div class="l-cart-total">
            <span class="l-cart-total-word">{{ trans('site.sub-total', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}:</span>
            <div class="l-cart-total-price">
                <span class="l-cart-total-price-money">{{ Currency::getMoneyFormat($cart->total_price) }}</span>
                <span class="l-cart-total-price-desc">{!! trans('site.prices-are-without-excluding-delivery', [], 'messages', isset($currentLocale) ? $currentLocale : null) !!}</span>
            </div>
            @if (Auth::check())
                <a href="{{ route('shop.checkout') }}" class="btn btn-warning">{{ trans('site.checkout', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</a>
            @else
                <a href="{{ route('shop.checkout') }}" class="btn btn-warning">{{ trans('site.checkout-guest', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</a>
            @endif
        </div>
        @if (!Auth::check())
            <div class="l-cart-login">
                {!! view('auth._fast-signin', ['currentLocale' => isset($currentLocale) ? $currentLocale : null]) !!}
            </div>
        @endif
    </div>

@else

    <p class="empty">{{ trans('shop.cart-is-empty', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</p>

@endif