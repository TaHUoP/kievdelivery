<a href="{{ route('shop.cart') }}">
    <div class="cart-mini-info">
        <b>{{ trans('shop.cart', [], 'messages', isset($currentLocale) ? $currentLocale : null) }}</b>
        <div class="cart-mini-info-count-products">
            {!! trans_choice('shop.cart-total-products-and-price :amount :price', $cartControl['countProducts'], [
                'amount' => $cartControl['countProducts'],
                'price' => '<span class="cart-mini-info-price">' . $cartControl['totalPriceString'] . '</span>'
            ], 'messages', isset($currentLocale) ? $currentLocale : null) !!}
        </div>
    </div>
    <span class="cart-icon"></span>
</a>