@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('shop.checkout') }}</title>
    <meta name="description" content="{{ trans('shop.checkout') }}">
    <meta name="keywords" content="{{ trans('shop.checkout') }}">
@endsection

@push('styles')
<link rel="stylesheet" href="{{ cdn('css/pikaday.min.css') }}">
<style>
    .l-checkout .form-group textarea {
        width: 434px;
        min-width: 230px;
    }

    .bottom-shadow {
        box-shadow: inset 0 -10px 15px -13px green;
    }

    .bottom-shadow-out {
        box-shadow: 0 8px 11px -10px green;
    }

    .form-group.required label.default:after {
        content: none;
    }

    @media (max-width:767px) {
        .l-checkout .form-group textarea {
            width: 100%;
        }
    }

    @media (min-width: 544px) {
        .checkout-cart-list-body-item-info .checkout-cart-list-item-name,
        .checkout-cart-list-body-item-info-info .checkout-cart-list-item-name {
            width: 20% !important;
        }
    }

    @media (min-width: 768px) {
        input[type=email].form-control,
        input[type=number].form-control,
        input[type=password].form-control,
        input[type=search].form-control,
        input[type=tel].form-control,
        input[type=text].form-control,
        select.form-control,
        textarea.form-control {
            width: 400px
        }
    }
</style>
@endpush

@push('scripts')
<script src="{{ cdn('js/moment.min.js') }}"></script>
<script src="{{ cdn('js/pikaday.min.js') }}"></script>
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script src="{{ cdn('js_dev/jquery.inputmask-multi.js') }}"></script>
<script>
    var totalItemsPrice = '{{ $cart['total_price'] }}';
    $(document).ready(function () {
        $('.change-agree').on('click', function () {
            if ($(this).val() == 'platon') {
                $('.paypal-chk').hide();
                $('.platon-chk').show();
            } else if ($(this).val() == 'paypal') {
                $('.paypal-chk').show();
                $('.platon-chk').hide();
            }
        })
    });
</script>
<script>
    var maskList = $.masksSort($.masksLoad("/public/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            if (completed) {
                var hint = maskObj.name_ru;
                if (maskObj.desc_ru && maskObj.desc_ru != "") {
                    hint += " (" + maskObj.desc_ru + ")";
                }
                $("#descr").html(hint);
            } else {
                $("#descr").html("Маска ввода");
            }
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    // $('#phone_mask').change(function() {
    //     if ($('#phone_mask').is(':checked')) {
            $('#customer_phone').inputmasks(maskOpts);
    //     } else {
    //         $('#customer_phone').inputmask("+[####################]", maskOpts.inputmask)
    //             .attr("placeholder", $('#customer_phone').inputmask("getemptymask"));
            // $("#descr").html("Маска ввода");
    //     }
    // });
    //
    // $('#phone_mask').change();
</script>
@endpush

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.checkout')
    ]) }}
@stop

@section('content')

    <div class="l-checkout">

        <h3 class="title">{{ trans('shop.checkout') }}</h3>

        @if(Session::has('success'))

            <p>{{ trans('app.connect') }} "<a href="{{ route('pages.contacts') }}">{{ trans('app.contacts') }}</a>".</p>

        @else

            {{ Form::open(['route' => ['shop.checkout'], 'class' => 'form-horizontal', 'id' => 'checkout-form']) }}

            <div class="section">
                <div class="section-title">
                    {{ trans('site.bill-to') }}
                    <span>({{ trans('site.enter-senders-info') }})</span>
                </div>

                <div class="form-group required {{ $errors->has('checkout.first_name') ? 'has-error' : '' }}">
                    {{ Form::label('checkout[first_name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                    {{ Form::text('checkout[first_name]', isset($user) ? $user->profile->name : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('checkout.first_name') ? $errors->first('checkout.first_name') : '' }}</div>
                </div>

                <div class="form-group required {{ $errors->has('checkout.last_name') ? 'has-error' : '' }}">
                    {{ Form::label('checkout[last_name]', trans('app.attr.surname'), ['class' => 'control-label']) }}
                    {{ Form::text('checkout[last_name]', isset($user) ? $user->profile->surname : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('checkout.last_name') ? $errors->first('checkout.last_name') : '' }}</div>
                </div>

                <div class="form-group required {{ $errors->has('checkout.email') ? 'has-error' : '' }}">
                    {{ Form::label('checkout[email]', trans('app.attr.email'), ['class' => 'control-label']) }}
                    {{ Form::text('checkout[email]', isset($user) ? $user->email : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('checkout.email') ? $errors->first('checkout.email') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('checkout.address') ? 'has-error' : '' }}">
                    {{ Form::label('checkout[address]', trans('app.attr.address'), ['class' => 'control-label']) }}
                    {{ Form::text('checkout[address]', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('checkout.address') ? $errors->first('checkout.address') : '' }}</div>
                </div>

                <div class="form-group required {{ $errors->has('checkout.phone') ? 'has-error' : '' }}">
                    <div style="display: flex">
                        {{ Form::label('checkout[phone]', trans('app.attr.phone'), ['class' => 'control-label',
                            'style' => 'margin-right: 4px']) }}
                        <div style="display: flex; flex-direction: column;">
                            {{ Form::text('checkout[phone]', isset($user) ? $user->phone : null,
                            ['class' => 'form-control', 'id' => 'customer_phone', 'autocomplete' => 'disabled']) }}
{{--                            {{ Form::checkbox(null, 0, true, ['id' => 'phone_mask']) }}--}}

                            <label class="control-label default" style="display: flex; align-items: center; width: max-content">
                                <input type="hidden" name="checkout[send_sms]" value="1">
                                {{ Form::checkbox('checkout[send_sms]', 0, false, ['class' => 'form-control default']) }}
                                {{ trans('app.attr.dont_send_sms') }}
                            </label>
                        </div>
                    </div>
                    <div class="help-block">{{ $errors->has('checkout.phone') ? $errors->first('checkout.phone') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('checkout.country') ? 'has-error' : '' }}">
                    {{ Form::label('checkout[country]', trans('app.attr.country'), ['class' => 'control-label']) }}
                    {{ Form::select('checkout[country]', $countries, null, ['class' => 'form-control min']) }}
                    <div class="help-block">{{ $errors->has('checkout.country') ? $errors->first('checkout.country') : '' }}</div>
                </div>

            </div>

            <div class="section bottom-shadow-out">
                <div class="section-title">{{ trans('site.deliver-to') }}
                    <span>({{ trans('site.enter-recipients-info') }})</span></div>

                <div class="form-group required {{ $errors->has('delivery.first_name') ? 'has-error' : '' }}">
                    {{ Form::label('delivery[first_name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                    {{ Form::text('delivery[first_name]', isset($user) ? $user->profile->name : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('delivery.first_name') ? $errors->first('delivery.first_name') : '' }}</div>
                </div>

                <div class="form-group required {{ $errors->has('delivery.last_name') ? 'has-error' : '' }}">
                    {{ Form::label('delivery[last_name]', trans('app.attr.surname'), ['class' => 'control-label']) }}
                    {{ Form::text('delivery[last_name]', isset($user) ? $user->profile->surname : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('delivery.last_name') ? $errors->first('delivery.last_name') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('delivery.address') ? 'has-error' : '' }}">
                    {{ Form::label('delivery[address]', trans('app.attr.address'), ['class' => 'control-label']) }}
                    {{ Form::text('delivery[address]', null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('delivery.address') ? $errors->first('delivery.address') : '' }}</div>
                </div>

                <div class="form-group required {{ $errors->has('delivery.phone') ? 'has-error' : '' }}">
                    {{ Form::label('delivery[phone]', trans('app.attr.phone'), ['class' => 'control-label']) }}
                    {{ Form::text('delivery[phone]', isset($user) ? $user->phone : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('delivery.phone') ? $errors->first('delivery.phone') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('delivery.country') ? 'has-error' : '' }}">
                    {{ Form::label('delivery[country]', trans('app.attr.country'), ['class' => 'control-label']) }}
                    {{ Form::select('delivery[country]', [
                           trans('site.ukraine') => trans('site.ukraine')
                       ], null, ['class' => 'form-control min', 'disabled' => 'disabled']) }}
                    <div class="help-block">{{ $errors->has('delivery.country') ? $errors->first('delivery.country') : '' }}</div>
                </div>

                <div class="form-group required {{ $errors->has('delivery.city') ? 'has-error' : '' }}">
                    {{ Form::label('delivery[city]', trans('app.attr.city'), ['class' => 'control-label']) }}
                    <select name='delivery[city]' class="min select-city form-control" style="width: 400px" id="checkout-city-select">
                        @foreach($cities as $city)
                            <option data-keywords="{{ Translate::get($city->translations, 'meta_keywords') }}" value="{{ $city->id }}" {{ $user_city->alias == $city->alias ? 'selected' : '' }}>{{ $city->with_region_and_fee }}</option>
                        @endforeach
                    </select>
                    <div class="help-block">{{ $errors->has('delivery.city') ? $errors->first('delivery.city') : '' }}</div>
                </div>

                <p><br></p>

                <div class="form-group required {{ $errors->has('delivery.date') ? 'has-error' : '' }}">
                    {{ Form::label('datepicker', trans('app.attr.date'), ['class' => 'control-label']) }}
                    {{ Form::text('delivery[date]', date('d.m.Y'), [
                        'class' => 'form-control min datapicker',
                        'placeholder' => date('d.m.Y'),
                        'id' => 'datepicker',
                        'readonly' => 'readonly',
                    ]) }}
                    <div class="help-block">{{ $errors->has('delivery.date') ? $errors->first('delivery.date') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('delivery.comment_recipient') ? 'has-error' : '' }}"
                     style="display: flex; flex-direction: column; align-items: baseline;">
                    <span class="control-wrap">
                        {{ Form::label('delivery[comment_recipient]', trans('site.attr.enter-message-to-recipient'), ['class' => 'control-label']) }}
                        <span class="control-label-info">({{ trans('site.will-be-on-the-card') }})</span>
                    </span>
                    {{ Form::textarea('delivery[comment_recipient]', null, ['class' => 'form-control', 'rows' => 3]) }}
                    <div class="help-block">{{ $errors->has('delivery.comment_recipient') ? $errors->first('delivery.comment_recipient') : '' }}</div>
                </div>

                <div class="form-group {{ $errors->has('delivery.comment_note') ? 'has-error' : '' }}"
                     style="display: flex; flex-direction: column; align-items: baseline;">
                    {{ Form::label('delivery[comment_note]', trans('site.attr.your-instroction-to-kievdelivery'), ['class' => 'control-label']) }}
                    {{ Form::textarea('delivery[comment_note]', null, ['class' => 'form-control', 'rows' => 3]) }}
                    <div class="help-block">{{ $errors->has('delivery.comment_note') ? $errors->first('delivery.comment_note') : '' }}</div>
                </div>
            </div>

            {{--<div class="section section-padding {{ $errors->has('delivery.shipment') ? 'has-error' : '' }}">--}}
                {{--<div class="section-title">{{ trans('shop.attr.shipment') }}</div>--}}
                {{--<label class="control-label">--}}
                    {{--{{ Form::checkbox('delivery[shipment]', true,true, ['class' => 'form-control', 'disabled' => 'disabled']) }} {{ trans('site.regional-courier-delivery') }}--}}
                    {{--{{ Currency::getMoneyFormat(Config::get('static.deliveryPrice')) }}--}}
                {{--</label>--}}
                {{--<div class="help-block">{{ $errors->has('delivery.shipment') ? $errors->first('delivery.shipment') : '' }}</div>--}}
            {{--</div>--}}

            <div class="section section-padding {{ $errors->has('checkout.payment_system') ? 'has-error' : '' }} bottom-shadow">
                <div class="section-title">{{ trans('shop.attr.payment') }}</div>
                <div class="form-group l-checkout-payment_system">
                    <label class="control-label l-checkout-payment-payments" @if(! $cart->isPaymentSystemAllowed('platon')) style="display: none;" @endif>
                        {{ Form::radio('checkout[payment_system]', "platon", false, ['class' => 'form-control change-agree', 'checked' => 'checked'/*, 'disabled' => 'disabled'*/]) }}
                        {{ trans('shop.pay-by-credit-card') }} (VISA, MasterCard, AmEx)
                    </label>
                    <label class="control-label l-checkout-payment-paypal">
                        {{ Form::radio('checkout[payment_system]', "paypal", false, ['class' => 'form-control change-agree', 'checked' => ! $cart->isPaymentSystemAllowed('platon') ? 'checked' : false]) }}
                        {{ trans('site.pay-by') }} PayPal
                    </label>

                    <div class="help-block">{{ $errors->has('checkout.payment_system') ? $errors->first('checkout.payment_system') : '' }}</div>
                </div>
            </div>
            <div id="cart_part">
                @include('shop._finance-part')
            </div>

            {{ Form::close() }}
        @endif

    </div>

@endsection