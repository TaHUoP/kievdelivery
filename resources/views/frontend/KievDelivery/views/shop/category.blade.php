@extends('layouts.app')

@section('seo-info')
    <?php
        use App\Modules\CatalogFilterModule\Facade as FacadeFilter;
        use App\Modules\SeoModule\Facade as FacadeSeo;
        $metaTitle =  FacadeFilter::isFilter() ? FacadeSeo::getTitle() : Translate::get($category['root']->translations, 'meta_title');
        $metaDescription = FacadeFilter::isFilter() ? FacadeSeo::getDescription() : Translate::get($category['root']->translations, 'meta_description');
        $metaKeywords = FacadeFilter::isFilter() ? FacadeSeo::getKeywords() : Translate::get($category['root']->translations, 'meta_keywords');
    ?>
    <title>{{ !empty($metaTitle) ? $metaTitle : Translate::get($category['root']->translations, 'name') }}</title>
    <meta name="description" content="{{ !empty($metaDescription) ? $metaDescription : Translate::get($category['root']->translations, 'name') }}">

	<meta name="keywords" content="{{ !empty($metaKeywords) ? $metaKeywords : Translate::get($category['root']->translations, 'name') }}">

    @if (\App\Modules\LandingModule\Facade::isNoindex())
        <meta name="robots" content="noindex, nofollow" />
    @endif
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.show').on('click', function () {
                $(this).prev().removeClass('shadow').css('height', 'auto');
                $(this).hide();
                $(this).next().show();
            });
            $('.hide').on('click', function () {
                $(this).prev().prev().addClass('shadow').css('height', '300px');
                $(this).hide();
                $(this).prev().show();
            });
        });
    </script>
@endpush

@section('canonical_links')

    <link rel="canonical" href="{{ route('shop.category', ['alias' => \App\Modules\CatalogFilterModule\Facade::getCanonical($alias)]) }}"/>
            {{--<link rel="canonical" href="https://kievdelivery.com/{{$canonical}}"/>--}}
    {{--@if($category['root']['products'])--}}
        {{--@if($category['root']['products']->lastPage() > 1)--}}
            {{--@if($category['root']['products']->currentPage() == 1 || $category['root']['products']->currentPage() < $category['root']['products']->lastPage())--}}
                {{--<link rel="next" href="{{url()->current()}}?page={{$category['root']['products']->currentPage() + 1}}"/>--}}
            {{--@endif--}}
            {{--@if($category['root']['products']->currentPage() <= $category['root']['products']->lastPage() && $category['root']['products']->currentPage() > 1)--}}
                {{--<link rel="prev" href="{{url()->current()}}?page={{$category['root']['products']->currentPage() - 1}}"/>--}}
            {{--@endif--}}
        {{--@endif--}}
    {{--@endif--}}

@endsection

@section('nav-filter')
    @if (!$category['children']->count())
        @include('layouts._nav-filter')
    @endif
@endsection

@section('breadcrumbs')
    <?php $links = [] ?>
    @foreach ($category['parents'] as $pCat)
        <?php $links[] = '<a href="' . route('shop.category', ['alias' => $pCat->alias]) . '">' . Translate::get($pCat->translations, 'name') . '</a>' ?>
    @endforeach
    @if(\App\Modules\CatalogFilterModule\Facade::isFilter())
        <?php $links[] = '<a href="' . route('shop.category', ['alias' => $alias]) . '">' . Translate::get($category['root']->translations, 'name') . '</a>' ?>
        <?php $links = array_merge($links, \App\Modules\SeoModule\Facade::getBreadcrumbs(route('shop.category', ['alias' => $alias]))) ?>
    @else
        <?php $links[] = Translate::get($category['root']->translations, 'name'); ?>
    @endif
    {{ Widget::run('Breadcrumbs', $links) }}
@stop

@section('content')

    @if ($category['children']->count())

        @foreach ($category['children'] as $cat)
            <div class="l-category">
                <div class="title">
                    <div class="title-group">
                        <h2 class="color-warning">
                            <a href="{{ route('shop.category', ['alias' => $cat->alias]) }}">
                                {{\App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getH1() :  Translate::get($cat->translations, 'name') }}
                            </a>
                        </h2>
                        <span class="title-description">
                            {{ Translate::get($cat->translations, 'description_short') ?: '' }}
                        </span>
                    </div>
                    <div class="title-btn">
                        <a href="{{ route('shop.category', ['alias' => $cat->alias]) }}" class="btn">{{ trans('site.open-category') }}</a>
                    </div>
                </div>
                <div class="grid-products">
                    @if ($cat['products']->count())
                        @each('shop.product._item', $cat['products'], 'product')
                    @else
                        {{ trans('app.no-data') }}
                        <p><br/></p>
                    @endif
                </div>
            </div>
        @endforeach

    @else

        {{-- TODO: think on how following section can be written more beautiful--}}
        @push('scripts')
        @if (getenv('APP_ENV') === 'production')
            <script src="{{ cdn('js/ajax-pagination.min.js') }}"></script>
        @else
            <script src="{{ cdn('js_dev/ajax-pagination.js') }}"></script>
        @endif
        @endpush

        <div class="l-category">
            <div class="title title-indent-min">
                {{-- <div class="title-group">
                    <h2 class="color-warning">{{ \App\Modules\CatalogFilterModule\Facade::isFilter() ? "" :  Translate::get($category['root']->translations, 'name') }}</h2>
                   <span class="title-description">
                       {{\App\Modules\CatalogFilterModule\Facade::isFilter() ? \App\Modules\SeoModule\Facade::getH1() :  Translate::get($category['root']->translations, 'description_short') ?: '' }}
                   </span>
                </div> --}}

                @if ($category['root']['products']->count())
                    @include('shop.product._filter', ['section' => 'category', 'alias' => $category['root']->alias])
                @endif
            </div>
            @php
                /**@var \App\Modules\CatalogFilterModule\Facade $filterFacade */
            @endphp
{{--                    <link rel="stylesheet" href="{{ cdn('css/filter.grid.css') }}">--}}
                <link rel="stylesheet" href="/statics/frontend/KievDelivery/css/filter.grid.css?123457">
                <div class="row">
                    <div class="col-md-2">
                        <div class="sidebar-container sidebar__inner">
                            <div class="sidebar-inline sidebar-toggle sidebar-toggle-inline sidebar-toggle-left hidden-lg hidden-md">
                                <a class="btn kd-color-green w100">
                                    {{ trans('app.filter') }}
                                </a>
                                <div>&nbsp;</div>
                            </div>
                            <div class="sidebar sidebar-left">
                                <div class="sidebar-toggle-hide hidden-lg">
                                    <span>&times;</span>
                                </div>
                                <div id="podbor-catalogview-cover">
                                    @if(\App\Modules\CatalogFilterModule\Facade::isFilterUrlsTree())
                                        @foreach(\App\Modules\CatalogFilterModule\Facade::buildFilterUrlsTree() as $propertyModel)
                                            <div class="catalogpage__podbor__filtercover">
                                                <div class="catalog-tovars__podbor__specif__header">
                                                    {{$propertyModel->getName()}}
                                                </div>
                                                <div class="catalog-tovars__podbor__specif__content">
                                                    <div class="catalogpage__podbor__speciflabel-cover">
                                                        @if($propertyModel->getFilterValues()->count())
                                                            @foreach($propertyModel->getFilterValues() as $filterValueUrlModel)
                                                                <label class="catalogpage__podbor__speciflabel"
                                                                        @if($filterValueUrlModel->isActive())
                                                                            checked
                                                                         @endif>
                                                                    <div class="row">
                                                                        <div class="col-xs-3" style="margin-right: -32px">
                                                                            <input
                                                                                    type="checkbox"
                                                                                    value="{{$filterValueUrlModel->getAlias()}}"
                                                                                    data-id="{{$filterValueUrlModel->getId()}}"
                                                                                    @if($filterValueUrlModel->isActive())
                                                                                    checked
                                                                                    @endif
                                                                                    class="hidden inputtourl"
                                                                            >
                                                                        </div>
                                                                        <div class="col-xs-9">
                                                                            {{$filterValueUrlModel->getName()}}
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div id="kd-btn-filter-search"
                                           data-parent-url="{{ route('shop.category', ['alias' => $alias]) }}"
                                           data-city-alias="{{\App\Modules\SeoModule\Facade::getCityAlias()}}"
{{--                                           class="btn kd-color-green w100"--}}
                                        >
{{--                                            {{ trans('app.search') }}--}}
                                        </div>
                                        <div>⠀</div>
                                    @endif

                                </div>
                            </div>
                            <div class="sidebar-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="grid-products">
                        @include('shop.product._page', ['products' => $category['root']['products'], 'azaza' => '1234'])
                    </div>
                </div>
                @if ($category['root']['products']->count())
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center text-center">
                            @include('_pagination', ['paginator' => $category['root']['products']])
                        </div>
                    </div>
                @endif
                @if ($category['root']['products']->count())
                    @include('_ajax_pagination', ['paginator' => $category['root']['products']])
                @endif
            </div>
        </div>

    @endif

    @if(\App\Modules\LandingModule\Facade::isLandingPageDescription())
        <div style="font-size: 80%; color: #999999;" class="">
            {!! \App\Modules\LandingModule\Facade::getLandingTranslateCurrent()->getPageDescription() !!}
        </div>
    @endif
    @if(\App\Modules\CatalogFilterModule\Facade::isFilter() === false)
        <div style="font-size: 80%; color: #999999; height: 300px" class="category-info shadow">
            {!! Translate::get($category['root']->translations, 'description_full') !!}
        </div>
        <div style="cursor: pointer; text-align: center; font-size: 20px" class="show">{{ trans('site.read_more') }}</div>
        <div style="cursor: pointer; text-align: center; font-size: 20px; display: none"
             class="hide">{{ trans('site.hide') }}</div>
    @endif

@endsection

@push('styles')
    <style>
        .shadow {
            height: 48px;
            overflow: hidden;
            position: relative;
        }

        .shadow:before {
            content: "";
            display: block;
            width: 100%;
            height: 17px;
            position: absolute;
            z-index: 1;
            bottom: 0;
            background-image: -webkit-gradient(linear,left top,left bottom,from(hsla(0,0%,100%,0)),color-stop(70%,#fff));
            background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff 53%);
        }

        @media (min-width: 1200px) {
            .product-short-extra .btn {
                font-size: 86%;
                padding: 11px 5px;
            }

            .product-short-preview {
                height: 193px;
            }
        }

        @media (min-width: 992px) {
            .product-short-extra .btn {
                font-size: 79%;
                padding: 10px 3px;
            }
        }


        .product-short-name {
            font-size: 119%;
            font-weight: 300
        }

        .product-short-price {
            margin: 9px 0;
            font-size: 166%;
            color: #fa3c7a
        }
    </style>
@endpush
