@extends('layouts.app')

@section('seo-info')
    <title>{{ trans('shop.paid-orders') }}</title>
    <meta name="description" content="{{ trans('shop.paid-orders') }}">
    <meta name="keywords" content="{{ trans('shop.paid-orders') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('shop.checkout.orders').'">' . trans('shop.order-history') . '</a>',
        '<a href="'.route('shop.checkout.order', ['id' => $checkout->id]).'">#' . $checkout->id . '</a>',
    ]) }}
@stop

@section('content')
    <div class="order-completed">

        <h3>{{ trans('shop.order-number-from', ['number' => $checkout->id, 'date' => '...' . $checkout->created_at]) }}</h3>

        <div class="l-cart-list">
            <div class="l-cart-list-header">
                <span class="l-cart-list-item-name">{{ trans('shop.product-name') }}</span>
                <span class="l-cart-list-item-desc">{{ trans('app.description') }}</span>
                <span class="l-cart-list-item-price">{{ trans('app.price') }}</span>
                <span class="l-cart-list-item-amount">{{ trans('app.quantity') }}</span>
                <span class="l-cart-list-item-total">{{ trans('app.sum') }}</span>
            </div>
            <div class="l-cart-list-body">
                @foreach ($checkout->cart->products as $relation)
                    <div class="l-cart-list-item">
                        <span class="l-cart-list-item-name">{!! Shop::getImagePreview($relation->product->getDefaultImage()) !!}</span>
                        <div class="l-cart-list-item-desc">
                            <a href="{{ route('shop.product', ['alias' => $relation->product->alias]) }}">{{ Translate::get($relation->product->translations, 'name') }}</a>
                            <div class="l-cart-list-item-desc-text">
                                {{ Translate::get($relation->product->translations, 'description_short') ?: trans('app.no-description') }}
                            </div>
                        </div>
                        <span class="l-cart-list-item-price">{!! Shop::getSalePrice($relation->product, true, $checkout->created_at ) ?: trans('app.no-data') !!}</span>
                        <span class="l-cart-list-item-amount">{{ $relation->amount }}</span>
                        <span class="l-cart-list-item-total">{{ Currency::getMoneyFormat(Shop::getPrice($relation->product, false) * $relation->amount) }}</span>
                    </div>
                @endforeach
            </div>
        </div>


        <?php // TODO: add styles ?>
        <div class="checkout-cart-list-body-item-info order-cart-list-payment-container">
            <div class="checkout-cart-list-body-item">
                <span class="checkout-cart-list-item-name">{{ trans('shop.pay-by-credit-card') }}</span>
                <span class="checkout-cart-list-item-price">{{ Currency::getMoneyFormat($checkout->cart->total_price) }}</span>
            </div>
            <div class="checkout-cart-list-body-item">
                <span class="checkout-cart-list-item-name">{{ trans('shop.your-discount') }}</span>
                <span class="checkout-cart-list-item-price checkout-cart-list-item-price-discount">{{ Currency::getMoneyFormat($checkout->cart->discount) }}</span>
            </div>
            <div class="checkout-cart-list-body-item">
                <span class="checkout-cart-list-item-name">{{ trans('shop.delivery-price') }}</span>
                <span class="checkout-cart-list-item-price">
                    {{ Currency::getMoneyFormat($checkout->delivery->delivery_price) }}
                </span>
            </div>
            <div class="checkout-cart-list-body-item">
                <span class="checkout-cart-list-item-name">{{ trans('shop.total-for-order') }}</span>
                <span class="checkout-cart-list-item-price checkout-cart-list-item-price-total">
                    {{ Currency::getMoneyFormat($checkout->total) }}
                </span>
            </div>
        </div>

        <?php // TODO: add styles ?>
        <div class="checkout-cart-list-body-item-info order-cart-list-status-container">
            <div class="checkout-cart-list-body-item">
                <span class="checkout-cart-list-item-name">{{ trans('app.attr.status') }}</span>
                <span class="checkout-cart-list-item-name">{{ $checkout->status_name }}</span>
            </div>
            <div class="checkout-cart-list-body-item">
                <span class="checkout-cart-list-item-name">{{ trans('app.attr.paid') }}</span>
                <span class="checkout-cart-list-item-name">{{ trans($checkout->paid ? 'app.yes' : 'app.no' ) }}</span>
            </div>
        </div>

        <div class="section">
            <div class="section-title">{{ trans('shop.deliver-to') }} <span>({{ trans('shop.enter-recipients-info') }})</span></div>

            <div class="form-group required {{ $errors->has('delivery.first_name') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.name'), null, ['class' => 'control-label']) }}
                {{ Form::text('delivery[first_name]', $checkout->delivery->first_name, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.first_name') ? $errors->first('delivery.first_name') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('delivery.last_name') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.surname'), null, ['class' => 'control-label']) }}
                {{ Form::text('delivery[last_name]', $checkout->delivery->last_name, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.last_name') ? $errors->first('delivery.last_name') : '' }}</div>
            </div>

            <div class="form-group {{ $errors->has('delivery.address') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.address'), null, ['class' => 'control-label']) }}
                {{ Form::text('delivery[address]', $checkout->delivery->address, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.address') ? $errors->first('delivery.address') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('delivery.phone') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.phone'), null, ['class' => 'control-label']) }}
                {{ Form::text('delivery[phone]', $checkout->delivery->last_name, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.phone') ? $errors->first('delivery.phone') : '' }}</div>
            </div>


            <div class="form-group required {{ $errors->has('delivery.country') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.country'), null, ['class' => 'control-label']) }}
                {{ Form::select('delivery[country]', [
                       trans('site.ukraine') => trans('site.ukraine')
                   ], null, ['class' => 'form-control min', 'disabled' => 'disabled']) }}
            </div>

            <div class="form-group required {{ $errors->has('delivery.city') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.city'), null, ['class' => 'control-label']) }}
                {{ Form::text('delivery[city]', $checkout->delivery->city, ['class' => 'form-control min', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.city') ? $errors->first('delivery.city') : '' }}</div>
            </div>

            <p><br></p>

            <div class="form-group required {{ $errors->has('delivery.date') ? 'has-error' : '' }}">
                {{ Form::label(trans('app.attr.date'), null, ['class' => 'control-label']) }}
                {{ Form::text('delivery[date]', $checkout->delivery->date, [
                    'class' => 'form-control min',
                    'placeholder' => date('Y.m.d'),
                    'id' => 'datepicker',
                    'readonly' => 'readonly',
                    'disabled' => 'disabled',
                ]) }}
                <div class="help-block">{{ $errors->has('delivery.date') ? $errors->first('delivery.date') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('delivery.comment_recipient') ? 'has-error' : '' }}">
                <label class="control-label">
                    {{ trans('shop.attr.enter-message-to-recipient') }}
                    <span>({{ trans('shop.will-be-on-the-card') }})</span>
                </label>
                {{ Form::textarea('delivery[comment_recipient]', $checkout->delivery->comment_recipient, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.comment_recipient') ? $errors->first('delivery.comment_recipient') : '' }}</div>
            </div>

            <div class="form-group required {{ $errors->has('delivery.comment_note') ? 'has-error' : '' }}">
                <label class="control-label">
                    {{ trans('shop.attr.your-instroction-to-kievdelivery') }}
                </label>
                {{ Form::textarea('delivery[comment_note]', $checkout->delivery->comment_note, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                <div class="help-block">{{ $errors->has('delivery.comment_note') ? $errors->first('delivery.comment_note') : '' }}</div>
            </div>
        </div>

    </div>

@endsection