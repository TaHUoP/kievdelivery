@extends('layouts.app')

@section('seo-info')
    <title>{{ Translate::get($page->translations, 'meta_title') }}</title>
    <meta name="description" content="{{ Translate::get($page->translations, 'meta_description') }}">
    <meta name="keywords" content="{{ Translate::get($page->translations, 'meta_keywords') }}">
@endsection

@section ('canonical_links')
    <link rel="canonical" href="{{ request()->url() }}"/>
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        Translate::get($page->translations, 'title'),
    ]) }}
@stop

@section('content')

    {!! Translate::get($page->translations, 'text') !!}

@endsection