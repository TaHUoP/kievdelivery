@extends('layouts.app')

@section('seo-info')
    <title>{{ Translate::get($page->translations, 'meta_title') }}</title>
    <meta name="description" content="{{ Translate::get($page->translations, 'meta_description') }}">
    <meta name="keywords" content="{{ Translate::get($page->translations, 'meta_keywords') }}">
@endsection

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        Translate::get($page->translations, 'title'),
    ]) }}
@stop

@section ('canonical_links')
    <link rel="canonical" href="{{ request()->url() }}"/>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.show').on('click', function () {
                $(this).prev().removeClass('shadow').css('height', 'auto');
                $(this).hide();
                $(this).next().show();
            });
            $('.hide').on('click', function () {
                $(this).prev().prev().addClass('shadow').css('height', '300px');
                $(this).hide();
                $(this).prev().show();
            });
        });
    </script>
@endpush

@section('content')
    <section>
        @if ($categories->count() == 1)
            <h3 class="title-separator"><span>{{ Translate::get($categories[0]->translations)->name }}</span></h3>
        @else
            <h3 class="title-separator"><span>{{ trans('site.recommended') }}</span></h3>
        @endif

        <div class="grid-products">
            @if ($productsRecommended->count())
                @each('shop.product._item', $productsRecommended, 'product')
            @else
                <div class="empty">{{ trans('app.no-data') }}</div>
            @endif
        </div>
    </section>

    <div>
        <div class="shadow" style="height: 300px">
            {!! Translate::get($page->translations, 'text') !!}
        </div>
        <div style="cursor: pointer; text-align: center; font-size: 20px" class="show">{{ trans('site.read_more') }}</div>
        <div style="cursor: pointer; text-align: center; font-size: 20px; display: none"
             class="hide">{{ trans('site.hide') }}</div>
    </div>

@endsection

@push('styles')
    <style>
        .shadow {
            height: 48px;
            overflow: hidden;
            position: relative;
        }

        .shadow:before {
            content: "";
            display: block;
            width: 100%;
            height: 17px;
            position: absolute;
            z-index: 1;
            bottom: 0;
            background-image: -webkit-gradient(linear,left top,left bottom,from(hsla(0,0%,100%,0)),color-stop(70%,#fff));
            background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff 53%);
        }
    </style>
@endpush