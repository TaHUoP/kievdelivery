@if (!empty($languageControl['all']) &&  $languageControl['all']->count() > 1)
    <ul class="nav nav-tabs nav-tabs-bottom languages-switch">
        <li class="dropdown language-switch">
            <a class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                <span class="language-view">
                    <img src="{{ Statics::img('default/images/flags/' . $languageControl['current']->locale . '.svg') }}" class="position-left" alt="">
                    {{ $languageControl['current']->name }}
                </span>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                @foreach ($languageControl['all'] as $language)
                    <li{{ $language->default ? ' active' : '' }}>
                        <a href="#lang-{{ $section }}-{{ $language->locale }}" data-toggle="tab">
                            <img src="{{ Statics::img('default/images/flags/' . $language->locale . '.svg') }}" class="position-left" alt="">
                            <span>{{ $fullName ? $language->name : '' }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    </ul>
@elseif(!empty($languageControl['all']) &&  $languageControl['all']->count() == 1)
    <div class="languages-switch">
        <div class="dropdown-toggle legitRipple">
            <span class="language-view">
                <img src="{{ Statics::img('default/images/flags/' . $languageControl['current']->locale . '.svg') }}" class="position-left" alt="">
                <span>{{ $fullName ? $languageControl['current']->name : '' }}</span>
            </span>
        </div>
    </div>
@endif