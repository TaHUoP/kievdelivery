<ul class="breadcrumb">
    <li><a href="{{ route('backend.site.index')  }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }}</a></li>
    @foreach($links as $link)
        @if ($link == end($links))
            <li class="active">{!! $link !!}</li>
        @else
            <li>{!! $link !!}</li>
        @endif
    @endforeach
</ul>