@if ($value === -1)
    <span class="label label-info">{{ trans('app.status.pending') }}</span>
@elseif($value === 1)
    <span class="label label-success">{{ trans('app.status.success') }}</span>
@else
    <span class="label label-danger">{{ trans('app.status.fail') }}</span>
@endif