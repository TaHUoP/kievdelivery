@if ($value === -1)
    <span class="label label-danger">Заблокирован</span>
@elseif($value === 0)
    <span class="label label-info">Неактивный</span>
@else
    <span class="label label-success">Активный</span>
@endif