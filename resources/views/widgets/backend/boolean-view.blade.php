@if($reverse)

    @if($value)
        <span class="label label-danger">Да</span>
    @else
        <span class="label label-success">Нет</span>
    @endif

@else

    @if($value)
        <span class="label label-success">Да</span>
    @else
        <span class="label label-danger">Нет</span>
    @endif

@endif




