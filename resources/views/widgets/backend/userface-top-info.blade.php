<li class="dropdown dropdown-user">
    <a class="dropdown-toggle" data-toggle="dropdown">
        <img src="{{ $user->profile->avatarUrl() }}" alt="">
        <span>{{ $user->email }}</span>
        <i class="caret"></i>
    </a>
    <ul class="dropdown-menu dropdown-menu-right">
        <li><a href="{{ route('backend.user.users.edit', $user) }}"><i class="icon-user-plus"></i> {{ trans('users.my-account') }}</a></li>
        <li class="divider"></li>
        <li><a href="{{ route('backend.auth.logout') }}"><i class="icon-switch2"></i> {{ trans('auth.logout') }}</a></li>
    </ul>
</li>