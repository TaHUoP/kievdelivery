@if (!empty($user))

    <a href="{{ route('backend.user.users.edit', $user) }}" class="media">
        <div class="media-left">
            <img src="{{ $user->profile->avatarUrl() }}" class="img-circle img-xs" alt="">
        </div>
        <div class="media-left">
            ID: {{ $user['id'] }} {{ $user['profile']['name'] }} {{ $user['profile']['surname'] }}
            <span class="display-block text-muted">
            {{ $user['phone'] ? $user['phone'] . ', ' : '' }} {{ $user['email'] ? $user['email'] : '' }}
        </span>
        </div>
    </a>

@else

    <div class="media">
        <div class="media-left">
            <img src="/statics/backend/Limitless/images/placeholder.jpg" class="img-circle img-xs" alt="">
        </div>
        <div class="media-left">
            {{ $anonym['username'] }}
            <span class="display-block text-muted">
                {{ $anonym['phone'] ? $anonym['phone'] . ', ' : '' }} {{ $anonym['email'] }}
            </span>
        </div>
    </div>

@endif