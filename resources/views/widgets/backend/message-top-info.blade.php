<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="icon-bubbles4"></i>
        <span class="visible-xs-inline-block position-right">Messages</span>
        <span class="badge bg-warning-400">1</span>
    </a>
    <div class="dropdown-menu dropdown-content width-350">
        <div class="dropdown-content-heading">Уведомления</div>
        <ul class="media-list dropdown-content-body">
            <li class="media">
                <div class="media-left">
                    <img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                    <span class="badge bg-danger-400 media-badge">5</span>
                </div>
                <div class="media-body">
                    <a href="#" class="media-heading">
                        <span class="text-semibold">Система</span>
                        <span class="media-annotation pull-right">20.10.2016 10:00</span>
                    </a>
                    <span class="text-muted">Текст уведомления...</span>
                </div>
            </li>
        </ul>
        <div class="dropdown-content-footer">
            <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
        </div>
    </div>
</li>