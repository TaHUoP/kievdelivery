@if (!empty($files) || !empty($presentVideoFrame))
    <div class="product-descriptions-advanced">
        @if (!empty($presentVideoFrame))
            <div class="product-descriptions-advanced-video">
                {!! $presentVideoFrame !!}
            </div>
        @endif
        @if (!empty($files) && $files->count())
            <ul>
                <li>Скачать:</li>
                @foreach($files as $file)
                    <li><a href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a></li>
                @endforeach
            </ul>
        @endif
    </div>
@endif