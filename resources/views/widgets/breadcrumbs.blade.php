@php
    $name_declension = '';

    if (request()->route()->getName() == 'shop.category'
        && request()->route()->getParameter('alias') != \App\Models\Shop\Category::KYIV_ONLY_ALIAS) {
        $name_declension = Translate::get($user_city->translations)->name_declension;
    }
@endphp
<ul class="breadcrumbs">
    <li><a href="{{ route('site.index')  }}">{{ trans('site.home') }}</a></li>
    @foreach($links as $link)
        @if ($link == end($links))
            <li class="active">{!! $link !!} {{ $name_declension }}</li>
        @else
            <li>{!! $link !!}</li>
        @endif
    @endforeach
    <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [
              {
                "@type": "ListItem",
                "position": 1,
                "item": {
                  "@id": "{{ route('site.index')  }}",
                  "name": "{{ trans('site.home') }}",
                  "image": ""
                }
              }
              {{ !empty($linksMeta) ? ',' : '' }}
              @foreach($linksMeta as $i => $meta)
              {
                "@type": "ListItem",
                "position": {{ $i+2 }},
                "item": {
                  "@id": "{{ $meta['href'] }}",
                  "name": "{{ $i+1 != count($linksMeta) ? $meta['text'] : "{$meta['text']} {$name_declension}" }}",
                  "image": ""
                }
              } {{ $i+1 != count($linksMeta) ? ',' : '' }}
              @endforeach
          ]
        }
    </script>
</ul>