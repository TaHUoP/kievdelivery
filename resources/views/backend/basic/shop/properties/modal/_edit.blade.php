<div id="modal-property-form-edit" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(['route' => ['backend.shop.properties.update', $property], 'class' => 'form-property', 'method' => 'PUT']) }}
        <div class="modal-content">
            <div class="modal-header custom-modal-bg">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'property-' . $property->id,
                ]) }}
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">
                    {{ trans('shop.attr.property') }}
                </h6>
            </div>
            <div class="modal-body">
                @include('shop.properties._form')
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="btn bg-success legitRipple">{{ trans('app.save') }}</button>
                <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">{{ trans('app.close') }}</button>
                <button type="button" class="btn bg-danger pull-right legitRipple btn-filter-destroy" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.shop.properties.destroy', $property) }}">
                    {{ trans('app.delete') }}
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>