<div id="modal-property-form-create" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(['route' => ['backend.shop.properties.store'], 'class' => 'form-property', 'method' => 'POST']) }}
        <div class="modal-content">
            <div class="modal-header custom-modal-bg">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'property',
                ]) }}
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">
                    {{ trans('shop.attr.property') }}
                </h6>
            </div>
            <div class="modal-body">
                @include('shop.properties._form')
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="btn bg-success legitRipple">{{ trans('app.save') }}</button>
                <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">{{ trans('app.close') }}</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>