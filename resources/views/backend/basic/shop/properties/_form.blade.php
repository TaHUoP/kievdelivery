<div class="tabbable">
    <div class="row">
        <div class="col-sm-6">
            <div class="tab-content">
                @foreach ($languageControl['all'] as $language)
                    <?php
                    if (!empty($property)) {
                        $_translate = null;
                        foreach ($property->translations as $translate) {
                            if ($language->id == $translate->lang_id) {
                                $_translate = $translate;
                            }
                        }
                    }
                    ?>
                    <div class="tab-pane{{ $language->default ? ' active' : '' }}" id="lang-property-{{ isset($property->id) ? $property->id . '-' : '' }}{{ $language->locale }}">
                        <div class="form-group">
                            {{ Form::label($language->locale . '[name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                            {{ Form::text('translations['.$language->locale . '][name]', isset($_translate['name']) ? $_translate['name'] : null, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('alias', trans('app.attr.alias'), ['class' => 'control-label']) }}
                {{ Form::text('alias', isset($property->alias) ? $property->alias : null, ['class' => 'form-control']) }}
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="checkbox checkbox-switchery">
                <label>
                    {{ Form::checkbox('is_filter', 'on', !empty($property->is_filter) ? $property->is_filter : null, [
                        'class' => 'switchery',
                    ]) }}
                    {{ trans('shop.properties.attr.is_filter') }}
                </label>
            </div>
        </div>
    </div>
</div>