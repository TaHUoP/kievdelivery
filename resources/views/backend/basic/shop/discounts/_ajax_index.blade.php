<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.discounts.all') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-discounts"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-togglable table-hover">
            <thead>
            <tr>
                <th class="td-number">{{ trans('shop.attr.discount') }}</th>
                <th class="td-date">{{ trans('app.date_from') }}</th>
                <th class="td-date">{{ trans('app.date_to') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($discounts as $discount)
                    <tr>
                        <td>{{$discount->percents}} %</td>
                        <td>{!! DateTimeHelper::toFullDate($discount->date_from) !!}</td>
                        <td>{!! DateTimeHelper::toFullDate($discount->date_to) !!}</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="{{ route('backend.shop.discounts.edit', $discount) }}">
                                                <i class="icon-pencil"></i> {{ trans('app.edit') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.shop.discounts.destroy', $discount) }}"  data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-close2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($discounts->count())
                {{ trans('app.all records :total', ['total' => $discounts->total()])  }}
            @endif
        </div>
        @if ($discounts->count())
            @include('_pagination', ['paginator' => $discounts])
        @endif
    </div>
</div>