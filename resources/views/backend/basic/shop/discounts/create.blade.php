@extends('layouts.main')

@section('title', trans('shop.discounts.create'))

@section('page-title')
    <h4>{{ trans('shop.discounts.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.discounts.index').'">'.trans('shop.discounts.all').'</a>',
        trans('app.create'),
    ]) }}
@stop

@section('heading-elements')
@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.discounts.store']]) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6>{{ trans('shop.discounts.create') }}</h6>
            </div>
            <div class="panel-body">
                @include('shop.discounts._form')
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    {{Form::submit(trans('app.save'), ['class' => 'btn btn-success heading-btn pull-left legitRipple'])}}
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop