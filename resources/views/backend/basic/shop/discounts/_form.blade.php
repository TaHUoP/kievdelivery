<div class="row">
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('percents') ? 'has-error' : '' }}">
            {{ Form::label('percents', trans('shop.discounts.attr.percents'), ['class' => 'control-label']) }}
            {{ Form::number('percents', isset($discount->percents) ? $discount->percents : null, ['class' => 'form-control', 'min'=> 0, 'max' => 100]) }}
            <div class="help-block">{{ $errors->has('percents') ? $errors->first('percents') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('date_from') ? 'has-error' : '' }}">
            {{ Form::label('date_from', trans('shop.discounts.attr.date_from'), ['class' => 'control-label']) }}
            {{ Form::text('date_from', isset($discount->date_from) ? date('d.m.Y', strtotime($discount->date_from)) : null, ['class' => 'form-control daterange-single']) }}
            <div class="help-block">{{ $errors->has('date_from') ? $errors->first('date_from') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('date_to') ? 'has-error' : '' }}">
            {{ Form::label('date_to', trans('shop.discounts.attr.date_to'), ['class' => 'control-label']) }}
            {{ Form::text('date_to', isset($discount->date_to) ? date('d.m.Y', strtotime($discount->date_to)) : null, ['class' => 'form-control daterange-single']) }}
            <div class="help-block">{{ $errors->has('date_to') ? $errors->first('date_to') : '' }}</div>
        </div>
    </div>
</div>