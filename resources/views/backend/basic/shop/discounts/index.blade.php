@extends('layouts.main')

@section('title', trans('shop.discounts.control'))

@section('page-title')
    <h4>{{ trans('shop.discounts.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.discounts.index').'">'.trans('shop.discounts.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.discounts.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div id="grid-discounts">
        @include('shop.discounts._ajax_index')
    </div>

@stop