@extends('layouts.main')

@section('title', trans('shop.checkouts.edit'))

@section('page-title')
    <h4>{{ trans('shop.checkouts.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.checkouts.index').'">'.trans('shop.checkouts.all').'</a>',
        trans('app.edit') . ' ID' . $checkout->id,
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.checkouts.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span></a>
@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.checkouts.update', $checkout], 'method' => 'PUT']) }}

        @include('shop.checkouts._form')

    {{ Form::close() }}

@stop