<div class="form-group content-group">
    <div class="has-feedback has-feedback-left">
        <input type="text"
               class="form-control input-xlg"
               id="checkout-product-add"
               data-checkout-id="{{ $checkout->id }}"
               data-url="{{ route('backend.shop.checkoutProducts.store') }}"
               placeholder="{{ trans('shop.products_search') }}">
        <div class="form-control-feedback">
            <i class="icon-search4 text-muted text-size-base"></i>
        </div>
    </div>
</div>

@if(isset($checkout->cart->products) && $checkout->cart->products)

    @forelse($checkout->cart->products as $relation)

        <div class="form-group">
            <div class="media">
                <a href="{{ route('backend.shop.products.edit', $relation->product) }}" class="media-left" target="_blank">
                    <img src="{!! Shop::getImagePreview(Shop::getDefaultImage($relation->product), true) !!}" alt="" class="img-rounded img-preview">
                </a>
                <div class="media-body">
                    <h6 class="media-heading">
                        <a href="{{ route('backend.shop.products.edit', $relation->product) }}" target="_blank">
                            {{ Translate::get($relation->product->translations, 'name') }}
                        </a>
                    </h6>
                    <div class="form-group col-sm-9 no-padding">
                        <input type="text"
                               data-url="{{ route('backend.shop.checkoutProducts.update', $relation) }}"
                               value="{{ $relation->amount }}"
                               class="touchspin-vertical product_amount_change input-lg">
                    </div>
                    <div class="form-group col-sm-3">
                        <button type="button" data-url="{{ route('backend.shop.checkoutProducts.destroy', $relation) }}" data-confirm="{{ trans('app.confirm') }}" class="btn delete-product">
                            <i class="icon-close2 text-danger"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    @empty

        {{ trans('app.no-data') }}

    @endforelse

    <hr>
    <h5>{{ trans('shop.price') }}: {{ Currency::getMoneyFormat($checkout->cart->total_price) }}</h5>
    <h5>{{ trans('shop.delivery-price') }}: {{ Currency::getMoneyFormat($checkout->delivery->delivery_price) }}</h5>
    @if (!empty($checkout->cart->discount_code))
        <h5>{{ trans('shop.cart_number') }}: {{ $checkout->cart->discount_code }}</h5>
    @endif
    <h5>{{ trans('shop.discount') }}: {{ Currency::getMoneyFormat($checkout->cart->discount) }}</h5>
    <h5>{{ trans('shop.total_price') }}: {{ Currency::getMoneyFormat($checkout->total) }}</h5>

@else

    {{ trans('app.no-data') }}

@endif