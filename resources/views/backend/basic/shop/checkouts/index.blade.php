@extends('layouts.main')

@section('title', trans('shop.checkouts.control'))

@section('page-title')
    <h4>{{ trans('shop.checkouts.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.checkouts.index').'">'.trans('shop.checkouts.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.checkouts.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span></a>
@stop

@section('content')

    <div id="grid-checkouts">
        @include('shop.checkouts._ajax_index')
    </div>

@stop