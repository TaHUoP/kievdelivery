<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">{{ trans('shop.bill-to') }}</h5>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.first_name') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[first_name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                            {{ Form::text('checkout[first_name]', isset($checkout->first_name) ? $checkout->first_name : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('checkout.first_name') ? $errors->first('checkout.first_name') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.last_name') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[last_name]', trans('app.attr.surname'), ['class' => 'control-label']) }}
                            {{ Form::text('checkout[last_name]', isset($checkout->last_name) ? $checkout->last_name : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('checkout.last_name') ? $errors->first('checkout.last_name') : '' }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.phone') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[phone]', trans('app.attr.phone'), ['class' => 'control-label']) }}
                            {{ Form::text('checkout[phone]', isset($checkout->phone) ? $checkout->phone : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('checkout.phone') ? $errors->first('checkout.phone') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.email') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[email]', trans('app.attr.email'), ['class' => 'control-label']) }}
                            {{ Form::text('checkout[email]', isset($checkout->email) ? $checkout->email : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('checkout.email') ? $errors->first('checkout.email') : '' }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.status') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[status]', trans('app.attr.status'), ['class' => 'control-label']) }}
                            {{ Form::select('checkout[status]', $statusArray, isset($checkout->status) ? $checkout->status : null, ['class' => 'form-control select']) }}
                            <div class="help-block">{{ $errors->has('checkout.status') ? $errors->first('checkout.status') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.country') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[country]', trans('app.attr.country'), ['class' => 'control-label']) }}
                            {{ Form::select('checkout[country]', $countries, isset($checkout->country) ? $checkout->country : null, ['class' => 'form-control select']) }}
                            <div class="help-block">{{ $errors->has('checkout.country') ? $errors->first('checkout.country') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('checkout.paid') ? 'has-error' : '' }}">
                            {{ Form::label('checkout[paid]', trans('app.attr.paid'), ['class' => 'control-label']) }}
                            {{ Form::select('checkout[paid]', [
                                0 => trans('app.no'),
                                1 => trans('app.yes'),
                            ], isset($checkout->paid) ? $checkout->paid : null, ['class' => 'form-control select']) }}
                            <div class="help-block">{{ $errors->has('checkout.paid') ? $errors->first('checkout.paid') : '' }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5 class="panel-title">{{ trans('shop.deliver-to') }}</h5>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('delivery.first_name') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[first_name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                            {{ Form::text('delivery[first_name]', isset($checkout->delivery->first_name) ? $checkout->delivery->first_name : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('delivery.first_name') ? $errors->first('delivery.first_name') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('delivery.last_name') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[last_name]', trans('app.attr.surname'), ['class' => 'control-label']) }}
                            {{ Form::text('delivery[last_name]', isset($checkout->delivery->last_name) ? $checkout->delivery->last_name : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('delivery.last_name') ? $errors->first('delivery.last_name') : '' }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <div class="form-group required {{ $errors->has('delivery.phone') ? 'has-error' : '' }}">
                                {{ Form::label('delivery[phone]', trans('app.attr.phone'), ['class' => 'control-label']) }}
                                {{ Form::text('delivery[phone]', isset($checkout->delivery->phone) ? $checkout->delivery->phone : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('delivery.phone') ? $errors->first('delivery.phone') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group required {{ $errors->has('delivery.date') ? 'has-error' : '' }}">
                                {{ Form::label('delivery[date]', trans('app.attr.date'), ['class' => 'control-label']) }}
                                {{ Form::text('delivery[date]', isset($checkout->delivery->date) ? $checkout->delivery->date->format('d.m.Y') : null, ['class' => 'form-control daterange-single']) }}
                                <div class="help-block">{{ $errors->has('delivery.date') ? $errors->first('delivery.date') : '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group required {{ $errors->has('delivery.country') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[country]', trans('app.attr.country'), ['class' => 'control-label']) }}
                            {{ Form::text('delivery[country]', isset($checkout->delivery->country) ? $checkout->delivery->country : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('delivery.country') ? $errors->first('delivery.country') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group required {{ $errors->has('delivery.city') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[city]', trans('app.attr.city'), ['class' => 'control-label']) }}
                            {{ Form::text('delivery[city]', isset($checkout->delivery->city) ? $checkout->delivery->city : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('delivery.city') ? $errors->first('delivery.city') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group required {{ $errors->has('delivery.address') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[address]', trans('app.attr.address'), ['class' => 'control-label']) }}
                            {{ Form::text('delivery[address]', isset($checkout->delivery->address) ? $checkout->delivery->address : null, ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('delivery.address') ? $errors->first('delivery.address') : '' }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('delivery.comment_recipient') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[comment_recipient]', trans('shop.attr.comment_recipient'), ['class' => 'control-label']) }}
                            {{ Form::textarea('delivery[comment_recipient]', isset($checkout->delivery->comment_recipient) ? $checkout->delivery->comment_recipient : null, ['class' => 'form-control', 'rows' => 4]) }}
                            <div class="help-block">{{ $errors->has('delivery.comment_recipient') ? $errors->first('delivery.comment_recipient') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required {{ $errors->has('delivery.comment_note') ? 'has-error' : '' }}">
                            {{ Form::label('delivery[comment_note]', trans('shop.attr.comment_note'), ['class' => 'control-label']) }}
                            {{ Form::textarea('delivery[comment_note]', isset($checkout->delivery->comment_note) ? $checkout->delivery->comment_note : null, ['class' => 'form-control', 'rows' => 4]) }}
                            <div class="help-block">{{ $errors->has('delivery.comment_note') ? $errors->first('delivery.comment_note') : '' }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="section-title">{{ trans('shop.attr.shipment') }}</div>
                        <div class="form-group required {{ $errors->has('delivery.shipment') ? 'has-error' : '' }}">
                            <div class="control-label">
                                {{ Form::checkbox('delivery[shipment]', true, isset($checkout->delivery->shipment) ? $checkout->delivery->shipment : null, ['class' => 'form-control switchery']) }}
                            </div>
                            <div class="help-block">{{ $errors->has('delivery.shipment') ? $errors->first('delivery.shipment') : '' }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
                </div>
            </div>
        </div>
    </div>

    @if(isset($checkout))
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">{{ trans('shop.checkouts.list') }}</h5>
                </div>
                <div class="panel-body">
                    <div id="grid-products-list">
                        @include('backend.basic.shop.checkouts._ajax_products')
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>