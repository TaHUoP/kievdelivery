<div class="col-md-4">
    <div class="panel panel-dashboard-info bg-indigo-400">
        <div class="panel-heading">
            <h6 class="panel-title">{{ trans('shop.checkouts') }}</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload" data-url="{{ route('backend.site.index', ['action' => 'checkouts-count']) }}" data-selector="#grid-checkouts-count"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="heading-elements">
                <a href="{{ route('backend.shop.checkouts.index') }}"><span class="heading-text badge bg-indigo-800">{{ $checkoutsAll }}</span></a>
            </div>
            <h3 class="no-margin"><i class=" icon-cart position-left"></i>
                {!! trans_choice('shop.checkouts.not-processed :amount', $checkoutsCountPending, ['amount' => $checkoutsCountPending])!!}
            </h3>
        </div>
    </div>
</div>