@extends('layouts.main')

@section('title', trans('shop.checkouts.create'))

@section('page-title')
    <h4>{{ trans('shop.checkouts.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.checkouts.index').'">'.trans('shop.checkouts.all').'</a>',
        trans('app.create'),
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.checkouts.store']]) }}

        @include('shop.checkouts._form')

    {{ Form::close() }}

@stop