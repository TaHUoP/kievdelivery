<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.checkouts.new') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ route('backend.site.index', ['action' => 'checkouts']) }}" data-selector="#grid-checkouts"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-togglable table-hover">
            <thead>
            <tr>
                <th data-hide="phone">{{ trans('app.attr.id') }}</th>
                <th data-hide="phone">{{ trans('shop.attr.products') }}</th>
                <th data-hide="phone" class="td-number">{{ trans('shop.attr.priced') }}</th>
                <th data-hide="phone">{{ trans('shop.attr.customer') }}</th>
                <th data-hide="phone" class="td-date">{{ trans('app.attr.date') }}</th>
                <th data-hide="phone" class="td-status">{{ trans('app.attr.paid') }}</th>
                <th data-hide="phone" class="td-status">{{ trans('app.attr.status') }}</th>
            </tr>
            </thead>
            <tbody>
            @forelse($checkouts as $checkout)
                <tr>
                    <td class="td-number"><a href="{{ route('backend.shop.checkouts.edit', $checkout) }}">#&nbsp;{{ $checkout->id }}</a></td>
                    <td>
                        @forelse($checkout->cart->products as $relation)
                            <div>
                                <a href="{{ route('backend.shop.products.edit', $relation->product) }}">
                                    {{ Translate::get($relation->product->translations, 'name') }}
                                </a> x {{ $relation->amount }}
                            </div>
                        @empty
                            {{ trans('app.no-data') }}
                        @endforelse
                    </td>
                    <td class="td-price">{{ Currency::getMoneyFormat($checkout->cart->total_price) }}</td>
                    <td class="td-name">
                        @if ($checkout->cart->user)
                            <a href="{{ route('backend.user.users.edit', $checkout->cart->user) }}">
                                {{ User::getNameByUser($checkout->cart->user) }}
                            </a>
                        @else
                            <span>{{ $checkout->first_name }} {{ $checkout->last_name }}</span><br>
                            <span style="color: #666; font-size: 85%">Тел.: {{ $checkout->phone }}</span>
                        @endif
                    </td>
                    <td class="td-date">
                        {!! DateTimeHelper::toFullDateTime($checkout->created_at)?: trans('app.no-data') !!}
                    </td>
                    <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $checkout->paid]) }}</td>
                    <td class="td-status">{{ Widget::run('ProcessingStatusView', ['value' => $checkout->status]) }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">{{ trans('app.no-data') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            <a href="{{ route('backend.shop.checkouts.index') }}">{{ trans('shop.checkouts.all') }}</a>
        </div>
    </div>
</div>