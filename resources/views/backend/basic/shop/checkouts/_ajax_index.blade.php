<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.checkouts.all') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-checkouts"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-togglable table-hover">
            <thead>
            <tr>
                <th data-hide="phone">{{ trans('app.attr.id') }}</th>
                <th data-hide="phone">{{ trans('shop.attr.products') }}</th>
                <th data-hide="phone" class="td-number">{{ trans('shop.attr.priced') }}</th>
                <th data-hide="phone">{{ trans('shop.attr.customer') }}</th>
                <th data-hide="phone" class="td-date">{{ trans('app.attr.date') }}</th>
                <th data-hide="phone" class="td-status">{{ trans('app.attr.paid') }}</th>
                <th data-hide="phone" class="td-status">{{ trans('app.attr.status') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($checkouts as $checkout)
                    <tr>
                        <td class="td-number"><a href="{{ route('backend.shop.checkouts.edit', $checkout) }}">#&nbsp;{{ $checkout->id }}</a></td>
                        <td>
                            @forelse($checkout->cart->products as $relation)
                                <div><a href="{{ route('backend.shop.products.edit', $relation->product) }}">{{ Translate::get($relation->product->translations, 'name') }}</a> x {{ $relation->amount }}</div>
                            @empty
                                {{ trans('app.no-data') }}
                            @endforelse
                        </td>
                        <td class="td-price">
                            <span {!! $checkout->cart->discount > 0 ? 'style="color: green" title="Sale: ' . Currency::getMoneyFormat($checkout->cart->discount) . '"' : '' !!}>
                                {{ Currency::getMoneyFormat($checkout->total) }}
                            </span>
                        </td>
                        <td class="td-name">
                            @if ($checkout->cart->user)
                                <a href="{{ route('backend.user.users.edit', $checkout->cart->user) }}">
                                    {{ User::getNameByUser($checkout->cart->user) }}
                                </a>
                            @else
                                <span>{{ $checkout->first_name }} {{ $checkout->last_name }}</span><br>
                                <span style="color: #666; font-size: 85%">{{ trans('app.attr.phone') }}: {{ $checkout->phone }}</span>
                            @endif
                        </td>
                        <td class="td-date">
                            {!! DateTimeHelper::toFullDateTime($checkout->created_at)?: trans('app.no-data') !!}
                        </td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $checkout->paid]) }}</td>
                        <td class="td-status">{{ Widget::run('ProcessingStatusView', ['value' => $checkout->status]) }}</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="{{ route('backend.shop.checkouts.edit', $checkout) }}">
                                                <i class="icon-pencil"></i> {{ trans('app.edit') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.shop.checkouts.destroy', $checkout) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-close2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($checkouts->count())
                {{ trans('app.all records :total', ['total' => $checkouts->total()])  }}
            @endif
        </div>
        @if ($checkouts->count())
            @include('_pagination', ['paginator' => $checkouts])
        @endif
    </div>
</div>