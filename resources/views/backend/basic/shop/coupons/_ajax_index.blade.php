<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.coupons.all') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-coupons"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-togglable table-hover">
            <thead>
            <tr>
                <th data-hide="phone" class="td-name">{{ trans('app.code') }}</th>
                <th data-toggle="true">{{ trans('app.attr.name_title') }}</th>
                <th data-hide="phone" class="td-number">{{ trans('shop.attr.discount') }}</th>
                <th data-hide="phone" class="td-number">{{ trans('app.quantity') }}</th>
                <th data-hide="phone" class="td-date">{{ trans('app.attr.date_expiry') }}</th>
                <th data-hide="phone" class="td-date">{{ trans('app.attr.date') }}</th>
                <th data-hide="phone" class="td-status">{{ trans('app.attr.status') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($coupons as $coupon)
                    <tr>
                        <td>{{$coupon->code}}</td>
                        <td>{{$coupon->name}}</td>
                        <td>{{$coupon->percentage}}%</td>
                        <td>{{$coupon->ttl}}&nbsp;/&nbsp;<b>{{$coupon->quantity}}</b></td>
                        <td>{!! DateTimeHelper::toFullDateTime($coupon->date_expiry)?: trans('app.no-data') !!}</td>
                        <td>
                            <p>{!! DateTimeHelper::toFullDateTime($coupon->created_at)?: trans('app.no-data') !!}</p>
                            <p>{!! DateTimeHelper::toFullDateTime($coupon->updated_at)?: trans('app.no-data') !!}</p>
                        </td>
                        <td><span class="label label-{{$coupon->ttl>0?'success':'warning'}}">---</span></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="{{ route('backend.shop.coupons.edit', $coupon) }}">
                                                <i class="icon-pencil"></i> {{ trans('app.edit') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.shop.coupons.destroy', $coupon) }}"  data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-close2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($coupons->count())
                {{ trans('app.all records :total', ['total' => $coupons->total()])  }}
            @endif
        </div>
        @if ($coupons->count())
            @include('_pagination', ['paginator' => $coupons])
        @endif
    </div>
</div>