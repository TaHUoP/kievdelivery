@extends('layouts.main')

@section('title', trans('shop.coupons.control'))

@section('page-title')
    <h4>{{ trans('shop.coupons.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.coupons.index').'">'.trans('shop.coupons.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.coupons.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div id="grid-coupons">
        @include('shop.coupons._ajax_index')
    </div>

@stop