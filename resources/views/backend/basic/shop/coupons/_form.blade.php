<div class="row">
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
            {{ Form::label('name', trans('shop.coupons.attr.name'), ['class' => 'control-label']) }}
            {{ Form::text('name', isset($coupon->name) ? $coupon->name : null, ['class' => 'form-control']) }}
            <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('date_expiry') ? 'has-error' : '' }}">
            {{ Form::label('date_expiry', trans('shop.coupons.attr.date_expiry'), ['class' => 'control-label']) }}
            {{ Form::text('date_expiry', isset($coupon->date_expiry) ? date('d.m.Y', strtotime($coupon->date_expiry)) : null, ['class' => 'form-control daterange-single']) }}
            <div class="help-block">{{ $errors->has('date_expiry') ? $errors->first('date_expiry') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('code') ? 'has-error' : '' }}">
            {{ Form::label('code', trans('shop.coupons.attr.code'), ['class' => 'control-label']) }}
            {{ Form::text('code', isset($coupon->code) ? $coupon->code : null, ['class' => 'form-control']) }}
            <div class="help-block">{{ $errors->has('code') ? $errors->first('code') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-quantity required {{ $errors->has('quantity') ? 'has-error' : '' }}">
            {{ Form::label('quantity', trans('shop.coupons.attr.quantity'), ['class' => 'control-label']) }}
            {{ Form::text('quantity', isset($coupon->quantity) ? $coupon->quantity : null, ['class' => 'form-control']) }}
            <div class="help-block">{{ $errors->has('quantity') ? $errors->first('quantity') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('ttl') ? 'has-error' : '' }}">
            {{ Form::label('ttl', trans('shop.coupons.attr.ttl'), ['class' => 'control-label']) }}
            {{ Form::text('ttl', isset($coupon->ttl) ? $coupon->ttl : null, ['class' => 'form-control']) }}
            <div class="help-block">{{ $errors->has('ttl') ? $errors->first('ttl') : '' }}</div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group required {{ $errors->has('percentage') ? 'has-error' : '' }}">
            {{ Form::label('percentage', trans('shop.coupons.attr.percentage'), ['class' => 'control-label']) }}
            {{ Form::number('percentage', isset($coupon->percentage) ? $coupon->percentage : null, ['class' => 'form-control', 'min'=> 0, 'max' => 100]) }}
            <div class="help-block">{{ $errors->has('percentage') ? $errors->first('percentage') : '' }}</div>
        </div>
    </div>
</div>