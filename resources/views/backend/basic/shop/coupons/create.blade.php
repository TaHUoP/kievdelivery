@extends('layouts.main')

@section('title', trans('shop.coupons.create'))

@section('page-title')
    <h4>{{ trans('shop.coupons.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.coupons.index').'">'.trans('shop.coupons.all').'</a>',
        trans('app.create'),
    ]) }}
@stop

@section('heading-elements')
@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.coupons.store']]) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6>{{ trans('shop.coupons.create') }}</h6>
            </div>
            <div class="panel-body">
                @include('shop.coupons._form')
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    {{Form::submit(trans('app.save'), ['class' => 'btn btn-success heading-btn pull-left legitRipple'])}}
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop