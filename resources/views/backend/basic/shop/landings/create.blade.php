@extends('layouts.main')

@section('title', trans('shop.landings.create'))

@section('page-title')
    <h4>{{ trans('shop.landings.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.landings.index').'">'.trans('shop.landings.all').'</a>',
        trans('app.create'),
    ]) }}
@stop

@section('heading-elements')
@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.landings.store']]) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6>{{ trans('shop.landings.create') }}</h6>
            </div>
            <div class="panel-body">
                @include('shop.landings._form')
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    {{Form::submit(trans('app.save'), ['class' => 'btn btn-success heading-btn pull-left legitRipple'])}}
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop