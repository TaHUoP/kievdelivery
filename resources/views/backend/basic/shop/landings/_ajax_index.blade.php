@php
/**@var App\Modules\LandingModule\Models\LandingModel[] $landings */
@endphp
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.landings.all') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-landings"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <div style="margin: 10px;">
            <div class="row" style="margin-bottom: 10px; font-weight: bold;  border-bottom: 1px solid #a9a9a9; padding-bottom: 10px">
                <div class="col-md-3">{{ trans('shop.landings.link.title') }}</div>
                <div class="col-md-3">{{ trans('landings.status.noindex') }}</div>
                <div class="col-md-2">{{ trans('landings.date.created') }}</div>
                <div class="col-md-2">{{ trans('landings.date.updated') }}</div>
                <div class="col-md-2"></div>
            </div>
            @forelse($landings as $landing)
                <div class="row" style="padding: 10px">
                    <div class="col-md-3"><a href="{{ route('backend.shop.landings.destroy', $landing) }}">{{$landing->getUrl()}}</a></div>
                    <div class="col-md-3">{{$landing->getNoIndex()}}</div>
                    <div class="col-md-2">{!! DateTimeHelper::toFullDateTime($landing->created_at)?: trans('app.no-data') !!}</div>
                    <div class="col-md-2">{!! DateTimeHelper::toFullDateTime($landing->updated_at)?: trans('app.no-data') !!}</div>
                    <div class="col-md-2">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-cog"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{ route('backend.shop.landings.edit', $landing) }}">
                                            <i class="icon-pencil"></i> {{ trans('app.edit') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('backend.shop.landings.destroy', $landing) }}"  data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                            <i class="icon-close2"></i> {{ trans('app.delete') }}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            @empty
                <div class="col-md-12">{{ trans('app.no-data') }}</div>
            @endforelse
        </div>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($landings->count())
                {{ trans('app.all records :total', ['total' => $landings->count()])  }}
            @endif
        </div>
{{--        @if ($landings->count())--}}
{{--            @include('_pagination', ['paginator' => $landings])--}}
{{--        @endif--}}
    </div>
</div>