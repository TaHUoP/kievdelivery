@extends('layouts.main')

@section('title', trans('shop.landings.control'))

@section('page-title')
    <h4>{{ trans('shop.landings.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.landings.index').'">'.trans('shop.landings.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.landings.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div id="grid-landings">
        @include('shop.landings._ajax_index')
    </div>

@stop