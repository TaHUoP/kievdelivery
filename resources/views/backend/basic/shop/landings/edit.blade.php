@extends('layouts.main')

@section('title', trans('shop.landings.edit'))

@section('page-title')
    <h4>{{ trans('shop.landings.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.landings.index').'">'.trans('shop.landings.all').'</a>',
        trans('app.edit'),
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.landings.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.landings.update', $landing], 'method' => 'PUT']) }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6>{{ trans('shop.landings.edit') }}</h6>
            </div>
            <div class="panel-body">
                @include('shop.landings._form')
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    {{Form::submit(trans('app.save'), ['class' => 'btn btn-success heading-btn pull-left legitRipple'])}}
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop