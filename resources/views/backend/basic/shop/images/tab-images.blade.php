<div class="panel panel-default">
    <div class="panel-body">
        <form action="{{ route('backend.shop.images.upload', $product) }}" data-url="{{ route('backend.shop.images.order', $product) }}" class="dropzone sortable" id="images-uploads" data-drop="{{ trans('app.drag-and-drop-image-upload') }}">
        @foreach ($product->images as $image)
            <div id="{{ $image->id }}" class="dz-complete dz-image-preview image-preview <?=(($image->default) ? 'is-main-photo' : '')?>" data-url="{{ route('backend.shop.images.edit', $image) }}">
                <div class="dz-preview dz-file-preview">
                    <div style="background-image: url('{!! Shop::getImagePreview($image, true) !!}');" class="dz-img" data-dz-thumbnail></div>
                    {{-- <div style="background-image: url('{!! Shop::getImagePreview($image, true) !!}');" class="dz-amby" data-dz-thumbnail></div> --}}
                    <div class="dz-progress"><span style="width: 100%;" class="dz-upload" data-dz-uploadprogress=""></span></div>
                    <span class="dz-error"><i class="icon-cross2"></i></span>
                    <span class="dz-done"><i class="icon-checkmark3"></i></span>
                    <button class="dz-remove" data-url="{{ route('backend.shop.images.destroy', $image) }}"><i class="icon-close2"></i></button>
                </div>
            </div>
        @endforeach
        </form>
        <div class="preview" style="display:none;">
            <div class="image-preview new" data-url="{{ route('backend.shop.images.edit', ['images' => '_id']) }}">
                <div class="dz-preview dz-file-preview">
                    <i class="icon-spinner10 spinner position-left"></i>
                    <img class="dz-img" data-dz-thumbnail />
                    <img class="dz-amby" data-dz-thumbnail />
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <span class="dz-error"><i class="icon-cross2"></i></span>
                    <span class="dz-done"><i class="icon-checkmark3"></i></span>
                    <button data-url="{{ route('backend.shop.images.destroy', ['images' => '_id']) }}" class="dz-remove"><i class="icon-close2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
