<div class="tabbable">
    <div class="tab-content">
        @foreach ($languageControl['all'] as $language)
            <?php
            if (!empty($image)) {
                $_translate = null;
                foreach ($image->translations as $translate) {
                    if ($language->id == $translate->lang_id) {
                        $_translate = $translate;
                    }
                }
            }
            ?>
            <div class="tab-pane{{ $language->default ? ' active' : '' }}" id="lang-image-{{ isset($image->id) ? $image->id . '-' : '' }}{{ $language->locale }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[title]', 'Title', ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][title]', isset($_translate['title']) ? $_translate['title'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[alt]', 'Alt', ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][alt]', isset($_translate['alt']) ? $_translate['alt'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="checkbox checkbox-switchery">
                        <label>
                            {{ trans('shop.images.attr.default') }}
                            {{ Form::checkbox('default', 'on', !empty($image->default) ? $image->default : null, [
                                'class' => 'switchery',
                            ]) }}
                        </label>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>