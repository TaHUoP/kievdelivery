@extends('layouts.main')

@section('title', trans('shop.products.edit'))

@section('page-title')
    <h4>{{ trans('shop.products.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.products.index').'">'.trans('shop.products.all').'</a>',
        trans('app.edit')
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.products.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span></a>
@stop

@section('content')

    <div class="panel panel-default">
        <ul class="nav nav-tabs nav-tabs-bottom bottom-divided nav-justified" style="margin: 0">
            <li class="active"><a href="#tab-main" data-toggle="tab">{{ trans('shop.product.section.main') }}</a></li>
            <li><a href="#tab-images" data-toggle="tab">{{ trans('shop.product.section.images') }}</a></li>
            <li><a href="#tab-filter" data-toggle="tab">{{ trans('shop.product.section.filter') }}</a></li>
            <li><a href="#tab-files" data-toggle="tab">{{ trans('shop.product.section.files') }}</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="tab-main">
            {{ Form::open(['route' => ['backend.shop.products.update', $product], 'method' => 'PUT']) }}
                <div class="row">
                    <div class="col-sm-7 col-md-8">
                        @include('shop.products.form.main')
                    </div>
                    <div class="col-sm-5 col-md-4">
                        @include('shop.products.form.extra')
                        @include('shop.products.form.categories')
                        @include('shop.products.form.paymentSystems')
                    </div>
                </div>
            {{ Form::close() }}
        </div>

        <div class="tab-pane" id="tab-images">
            @include('shop.images._tab-images')
        </div>

        <div class="tab-pane" id="tab-filter">
            @include('shop.products.form.filter')
        </div>

        <div class="tab-pane" id="tab-files">
            @include('shop.products.form.files')
        </div>
    </div>

@stop