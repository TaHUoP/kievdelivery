<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.products') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-products"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th data-toggle="true">{{ trans('app.attr.image') }}</th>
                <th data-hide="phone">{{ trans('shop.attr.product_name') }}
                    @if (request()->has('orderBy')
                        && array_key_exists('id', request()->input('orderBy'))
                        && request()->input('orderBy')['id'] == SORT_ASC)
                        <a href="{{ route('backend.shop.products.index', ['orderBy[id]' => SORT_DESC]) }}">&#8593;</a>
                    @else
                        <a href="{{ route('backend.shop.products.index', ['orderBy[id]' => SORT_ASC]) }}">&#8595;</a>
                    @endif
                </th>
                <th data-hide="phone">{{ trans('shop.attr.price') }}
                    @if (request()->has('orderBy')
                        && array_key_exists('price', request()->input('orderBy'))
                        && request()->input('orderBy')['price'] == SORT_ASC)
                        <a href="{{ route('backend.shop.products.index', ['orderBy[price]' => SORT_DESC]) }}">&#8593;</a>
                    @else
                        <a href="{{ route('backend.shop.products.index', ['orderBy[price]' => SORT_ASC]) }}">&#8595;</a>
                    @endif
                </th>
                <th data-hide="phone">{{ trans('app.attr.categories') }}</th>
                <th data-hide="phone">{{ trans('shop.attr.amount') }}</th>
                <th data-hide="phone">{{ trans('app.attr.visible') }}</th>
                <th data-toggle="true" class="text-center">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
            @forelse($products as $product)
                <tr>
                    <td class="td-image">
                        <img src="{!! Shop::getImagePreview($product->getDefaultImage(), true) !!}" alt="" class="img-rounded img-preview">
                    </td>
                    <td>
                        <p>
                            <a href="{{ route('backend.shop.products.edit', $product) }}">
                                {{ Translate::get($product->translations, 'name') ?: trans('app.no-description') }}
                            </a>
                            <br>
                            <span style="color: #666; font-size: 85%">
                                ID: {{ $product->id }} &nbsp; {{ trans('shop.product.attr.vendor_code') }}: {{ $product->vendor_code }}
                            </span>
                        </p>
                        {{ Translate::get($product->translations, 'description_short') ?: trans('app.no-description') }}
                    </td>
                    <td class="td-name">{!! Shop::getSalePrice($product) ?: trans('app.no-data') !!}</td>
                    <td>
                        @forelse($product->categories as $category)
                            <a href="{{ route('backend.shop.categories.edit', $category) }}">{{ Translate::get($category->translations, 'name') ?: trans('app.no-description') }}</a>
                            @if  ($product->categories->last() !== $category), @endif
                        @empty
                            {{ trans('app.no-data') }}
                        @endforelse
                    </td>
                    <td class="td-number text-center">{{ $product->amount }}</td>
                    <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $product->visible]) }}</td>
                    <td class="text-center td-control">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-cog"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('shop.product', ['alias' => $product->alias]) }}" target="_blank"><i class="icon-eye"></i> {{ trans('app.view') }}</a></li>
                                    <li><a href="{{ route('backend.shop.products.edit', $product) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                    <li>
                                        <a href="{{ route('backend.shop.products.destroy', $product) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                            <i class="icon-close2"></i> {{ trans('app.delete') }}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">{{ trans('app.no-data') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if (method_exists($products, 'total'))
                <a href="{{ route('backend.shop.products.index', array_merge(request()->except('page'), [
                        'without_pagination' => true,
                    ])) }}">{{ trans('app.all records :total', ['total' => $products->total()])  }}</a>
            @else
                <a href="{{ route('backend.shop.products.index', array_merge(request()->except('page'), [
                        'without_pagination' => false,
                    ])) }}">Постраничный просмотр</a>
            @endif
        </div>

        @if (method_exists($products, 'links'))
            {{ $products->appends(request()->except('page'))->links() }}
        @endif
{{--        @if ($products->count())--}}
{{--            @include('_pagination', ['paginator' => $products])--}}
{{--        @endif--}}
    </div>
</div>