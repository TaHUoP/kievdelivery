@extends('layouts.main')

@section('title', trans('shop.products.create'))

@section('page-title')
    <h4>{{ trans('shop.products.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.products.index').'">'.trans('shop.products.all').'</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    {{ Form::open(['route' => ['backend.shop.products.store']]) }}

        <div class="row">
            <div class="col-sm-9">
                @include('shop.products.form.main')
            </div>

            <div class="col-md-3">
                @include('shop.products.form.extra')

                @include('shop.products.form.categories')

                @include('shop.products.form.paymentSystems')
            </div>
        </div>

    {{ Form::close() }}

@stop