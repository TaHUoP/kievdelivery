<div class="col-md-4">
    <div class="panel panel-dashboard-info bg-teal-400">
        <div class="panel-heading">
            <h6 class="panel-title">{{ trans('shop.products') }}</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload" data-url="{{ route('backend.site.index', ['action' => 'products-count']) }}" data-selector="#grid-products-count"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <h3 class="no-margin"><i class="icon-database position-left"></i> {!! $productsCount !!}</h3>
        </div>
    </div>
</div>