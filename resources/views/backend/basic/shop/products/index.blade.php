@extends('layouts.main')

@section('title', trans('shop.products.control'))

@section('page-title')
    <h4>{{ trans('shop.products.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.products.index').'">'.trans('shop.products.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.shop.products.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span></a>
@stop

@section('content')

    <div id="grid-products">
        @include('shop.products._ajax_index')
    </div>

@stop