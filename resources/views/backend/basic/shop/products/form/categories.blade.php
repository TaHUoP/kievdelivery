<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.categories') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="{{ route('backend.shop.categories.index') }}" target="_blank"><i class="icon-pencil"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-group">
            @if (isset($categories))
                <select name="categories[]" multiple="multiple" class="form-control">
                    <?php
                    $nodes = $categories['children'];
                    $traverse = function ($categories, $currentCategories, $prefix = '') use (&$traverse) {
                        foreach ($categories as $category) {
                            $style = '';
                            $selected = '';
                            if (in_array($category->id, old('categories', []))) {
                                $selected = 'selected="selected"';
                            }

                            $name = Translate::get($category->translations, 'name');
                            if (empty($category->parent_id) || $category->parent_id === 1) {
                                $style = 'style="font-weight: bold; color: #2196f3;"';
                            }
                            if (!empty($currentCategories) && !$selected) {
                                foreach ($currentCategories as $currentCategory) {
                                    if ($currentCategory->id === $category->id) {
                                        $selected = 'selected="selected"';
                                        break;
                                    }
                                }
                            }
                            echo '<option ' . $style . ' ' . $selected . ' value="'.$category->id.'">' . $prefix . ' ' . $name . '</option>';
                            $traverse($category->children, $currentCategories, $prefix.'&emsp;');
                        }
                    };
                    $traverse($nodes, isset($product->categories) ? $product->categories : null);
                    ?>
                </select>
            @else
                {{ trans('app.no-data') }}
            @endif
        </div>
    </div>
</div>