<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('app.extra') }}</h5>
        @if(isset($product))
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a href="{{ route('shop.product', ['alias' => $product->alias]) }}" target="_blank"><i
                                    class="icon-eye"></i></a></li>
                </ul>
            </div>
        @endif
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('price') ? 'has-error' : '' }}">
                    {{ Form::label('price', trans('shop.product.attr.price'), ['class' => 'control-label']) }}
                    {{ Form::text('price', isset($product->price) ? $product->price : 0, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('price') ? $errors->first('price') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('amount') ? 'has-error' : '' }}">
                    {{ Form::label('amount', trans('shop.product.attr.amount'), ['class' => 'control-label']) }}
                    {{ Form::text('amount', isset($product->amount) ? $product->amount : 0, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('amount') ? $errors->first('amount') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('rating') ? 'has-error' : '' }}">
                    {{ Form::label('rating', trans('shop.product.attr.rating'), ['class' => 'control-label']) }}
                    {{ Form::text('rating', isset($product->rating) ? $product->rating : 0, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('rating') ? $errors->first('rating') : '' }}</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group required {{ $errors->has('alias') ? 'has-error' : '' }}">
                    {{ Form::label('alias', trans('shop.product.attr.alias'), ['class' => 'control-label']) }}
                    {{ Form::text('alias', isset($product->alias) ? $product->alias : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('alias') ? $errors->first('alias') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('vendor_code') ? 'has-error' : '' }}">
                    {{ Form::label('vendor_code', trans('shop.product.attr.vendor_code'), ['class' => 'control-label']) }}
                    {{ Form::text('vendor_code', isset($product->vendor_code) ? $product->vendor_code : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('vendor_code') ? $errors->first('vendor_code') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('mpn_code') ? 'has-error' : '' }}">
                    {{ Form::label('mpn_code', trans('shop.product.attr.mpn_code'), ['class' => 'control-label']) }}
                    {{ Form::text('mpn_code', isset($product->mpn_code) ? $product->mpn_code : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('mpn_code') ? $errors->first('mpn_code') : '' }}</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="checkbox checkbox-switchery">
                    <label>
                        {{ Form::checkbox('visible', 'on', !empty($product->visible) ? $product->visible : null, [
                            'class' => 'switchery',
                        ]) }}
                        {{ trans('shop.product.product_visible') }}
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="checkbox checkbox-switchery">
                    <label>
                        {{ Form::checkbox('pre_order', 'on', !empty($product->pre_order) ? $product->pre_order : null, [
                            'class' => 'switchery',
                        ]) }}
                        {{ trans('shop.product.pre_order') }}
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="checkbox checkbox-switchery">
                    <label>
                        {{ Form::checkbox('main', 'on', !empty($product->main) ? $product->main : null, [
                            'class' => 'switchery',
                        ]) }}
                        {{ trans('shop.product.main') }}
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>