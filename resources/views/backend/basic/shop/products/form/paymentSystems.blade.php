<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.paymentSystems') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
{{--                <li><a href="{{ route('backend.shop.categories.index') }}" target="_blank"><i class="icon-pencil"></i></a></li>--}}
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-group">
            @if (isset($paymentSystems))
                <select name="paymentSystems[]" multiple="multiple" class="form-control">
                    @foreach($paymentSystems as $key => $value)
                        <option
                                @if(isset($product) && $product->hasPaymentSystem($value))
                                        selected="selected"
                                @endif
                                value="{{ $key }}">{{ ucfirst($value) }}</option>
                    @endforeach
                </select>
            @else
                {{ trans('app.no-data') }}
            @endif
        </div>
    </div>
</div>