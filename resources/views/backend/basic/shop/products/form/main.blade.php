<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('app.main') }}</h5>
        <div class="heading-elements panel-nav">
            {{ Widget::run('LanguagesSwitch', [
                'section' => 'product',
            ]) }}
        </div>
    </div>
    <div class="panel-body">
        <div class="tabbable">
            <div class="tab-content">
                @foreach ($languageControl['all'] as $language)
                    <?php
                    if (!empty($product)) {
                        $_translate = null;
                        foreach ($product->translations as $translate) {
                            if ($language->id == $translate->lang_id) {
                                $_translate = $translate;
                            }
                        }
                    }
                    ?>
                    <div class="tab-pane fade in{{ $language->default ? ' active' : '' }}" id="lang-product-{{ $language->locale }}">
                        <div class="block-inner">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group required {{ $errors->has('translations.'.$language->locale.'.name') ? 'has-error' : '' }}">
                                        {{ Form::label(trans('shop.product.attr.name'), null, ['class' => 'control-label']) }}
                                        {{ Form::text('translations['.$language->locale.'][name]', isset($_translate['name']) ? $_translate['name'] : null, ['class' => 'form-control']) }}
                                        <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.name') ? $errors->first('translations.'.$language->locale.'.name') : '' }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.description_short') ? 'has-error' : '' }}">
                                {{ Form::label(trans('shop.product.attr.description_short'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][description_short]', isset($_translate['description_short']) ? $_translate['description_short'] : null, [
                                    'class' => 'form-control',
                                    'rows' => '2'
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.description_short') ? $errors->first('translations.'.$language->locale.'.description_short') : '' }}</div>
                            </div>
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.description_full') ? 'has-error' : '' }}">
                                {{ Form::label(trans('shop.product.attr.description_full'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][description_full]', isset($_translate['description_full']) ? $_translate['description_full'] : null, [
                                    'class' => 'form-control summernote',
                                    'data-upload' => route('backend.contents.upload'),
                                    'data-locale' => $language->locale,
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.description_full') ? $errors->first('translations.'.$language->locale.'.description_full') : '' }}</div>
                            </div>
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.guarantees') ? 'has-error' : '' }}">
                                {{ Form::label(trans('shop.product.attr.guarantees'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][guarantees]', isset($_translate['guarantees']) ? $_translate['guarantees'] : null, [
                                    'class' => 'form-control summernote',
                                    'data-upload' => route('backend.contents.upload'),
                                    'data-locale' => $language->locale,
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.guarantees') ? $errors->first('translations.'.$language->locale.'.guarantees') : '' }}</div>
                            </div>
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.delivery_policy') ? 'has-error' : '' }}">
                                {{ Form::label(trans('shop.product.attr.delivery_policy'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][delivery_policy]', isset($_translate['delivery_policy']) ? $_translate['delivery_policy'] : null, [
                                    'class' => 'form-control summernote',
                                    'data-upload' => route('backend.contents.upload'),
                                    'data-locale' => $language->locale,
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.delivery_policy') ? $errors->first('translations.'.$language->locale.'.delivery_policy') : '' }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_title') ? 'has-error' : '' }}">
                                        {{ Form::label(trans('shop.product.attr.meta_title'), null, ['class' => 'control-label']) }}
                                        {{ Form::text('translations['.$language->locale.'][meta_title]', isset($_translate['meta_title']) ? $_translate['meta_title'] : null, ['class' => 'form-control']) }}
                                        <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_title') ? $errors->first('translations.'.$language->locale.'.meta_title') : '' }}</div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_description') ? 'has-error' : '' }}">
                                        {{ Form::label(trans('shop.product.attr.meta_description'), null, ['class' => 'control-label']) }}
                                        {{ Form::text('translations['.$language->locale.'][meta_description]', isset($_translate['meta_description']) ? $_translate['meta_description'] : null, ['class' => 'form-control']) }}
                                        <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_description') ? $errors->first('translations.'.$language->locale.'.meta_description') : '' }}</div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? 'has-error' : '' }}">
                                        {{ Form::label(trans('shop.product.attr.meta_keywords'), null, ['class' => 'control-label']) }}
                                        {{ Form::text('translations['.$language->locale.'][meta_keywords]', isset($_translate['meta_keywords']) ? $_translate['meta_keywords'] : null, ['class' => 'form-control']) }}
                                        <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? $errors->first('translations.'.$language->locale.'.meta_keywords') : '' }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
            @if (isset($product))
                <a href="{{ route('backend.shop.products.destroy', $product) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete" class="btn btn-danger heading-btn pull-right legitRipple">
                    <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                </a>
            @endif
        </div>
    </div>
</div>