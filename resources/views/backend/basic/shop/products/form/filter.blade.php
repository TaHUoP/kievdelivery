<div id="product-filter" data-url="{{ route('backend.products.filter', $product) }}">
    <div class="panel panel-default">
        <div class="panel-body" id="product-filter-params" data-url="{{ route('backend.products.filter.param', $product) }}">
            <fieldset class="content-group">
                <legend class="text-bold">
                   {{ $filter['categoriesString'] }}
                     <a href="{{ route('backend.shop.filter.index') }}" target="_blank" class="pull-right"><i class="icon-eye"></i></a>
                </legend>
                @forelse($filter['properties'] as $property)
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label($property->alias, Translate::get($property->translations, 'name'), ['class' => 'control-label']) }}
                                @foreach($property->values as $value)
                                    <div>
                                        <label>
                                            {{ Form::checkbox("filter[{$value->alias}]", 0, \App\Helpers\ShopHelper::isPropertyValueInProduct($product->relations, $value), [
                                                'data-property-id' => $property->id,
                                                'data-value-id' => $value->id
                                            ]) }}
                                            {{Translate::get($value->translations, 'name')}}
                                        </label>
                                    </div>
                                @endforeach
                            <div class="help-block">
                                {{ $errors->has('filter.'.$property->id) ? $errors->first('filter.'.$property->id) : '' }}
                            </div>
                        </div>
                    </div>
                @empty
                    <p>{{ trans('app.no-data') }}</p>
                @endforelse
            </fieldset>
        </div>
    </div>
</div>

