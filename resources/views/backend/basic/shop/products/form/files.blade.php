{{ Form::open([
    'method' => 'POST',
    'route' => ['backend.products.files', $product]
]) }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('shop.additional-files') }}</h5>
        </div>
        <div class="panel-body">


            <div class="file-manager-help text-center {{ ((empty($product->files) || !$product->files->count()) && !count(old('files'))) ?: 'hide' }}">{{ trans('shop.products.file_help') }}</div>

            <div class="file-manager-cell clearfix hide file-manager-cell-maket" data-counter="0">
                <div class="col-sm-5">
                    <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                        {{ Form::label('name', trans('shop.product.attr.file_name'), ['class' => 'control-label']) }}
                        {{ Form::text('files[js_count_me][name]', null, ['class' => 'form-control input_disabled', 'disabled' => 'disabled']) }}
                        <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group required {{ $errors->has('url') ? 'has-error' : '' }}">
                        {{ Form::label('url', trans('shop.product.attr.file_url'), ['class' => 'control-label']) }}
                        {{ Form::text('files[js_count_me][url]', null, ['class' => 'form-control input_disabled', 'disabled' => 'disabled']) }}
                        <div class="help-block">{{ $errors->has('url') ? $errors->first('url') : '' }}</div>
                    </div>
                </div>
                <button type="button" class="btn btn-success legitRipple margin-top-20 file-manager-cell-choice" data-counter="0"><i class="icon-plus2"></i></button>
                <button type="button" class="btn btn-danger legitRipple margin-top-20 file-manager-cell-remove"><i class="icon-close2"></i></button>
            </div>

            <?php
                $foreach = old('files') ? old('files') : $product->files;
            ?>

{{--            {{ dd(old('files')) }}--}}

            @foreach ($foreach as $key => $file)
                <div class="file-manager-cell file-manager-cell-real clearfix" data-counter="{{ $key }}">
                    <div class="col-sm-5">
                        <div class="form-group required {{ $errors->has('files.' . $key . '.name') ? 'has-error' : '' }}">
                            {{ Form::label('name', trans('shop.product.attr.file_name'), ['class' => 'control-label']) }}
                            {{ Form::text('files[' . $key . '][name]', $file['name'], ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('files.' . $key . '.name') ? $errors->first('files.' . $key . '.name') : '' }}</div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group required {{ $errors->has('files.' . $key . '.url') ? 'has-error' : '' }}">
                            {{ Form::label('url', trans('shop.product.attr.file_url'), ['class' => 'control-label']) }}
                            {{ Form::text('files[' . $key . '][url]', $file['url'], ['class' => 'form-control']) }}
                            <div class="help-block">{{ $errors->has('files.' . $key . '.url') ? $errors->first('files.' . $key . '.url') : '' }}</div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success legitRipple margin-top-20 file-manager-cell-choice">
                        <i class="icon-plus2"></i>
                    </button>
                    <button type="button" class="btn btn-danger legitRipple margin-top-20 file-manager-cell-remove">
                        <i class="icon-close2"></i>
                    </button>
                </div>
            @endforeach

        </div>
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
                <button type="button" id="file-manager-cell-add" class="btn btn-primary heading-btn pull-right legitRipple">{{ trans('app.add') }}</button>
            </div>
        </div>
    </div>

{{ Form::close() }}


<div id="modal-file-manager" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header custom-modal-bg">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">{{ trans('app.file-manager') }}</h6>
            </div>
            <div class="modal-body no-padding">
                <div id="elfinder" data-url="{{ route('elfinder.connector') }}"></div>
            </div>
            <div class="modal-footer text-left"></div>
        </div>
    </div>
</div>