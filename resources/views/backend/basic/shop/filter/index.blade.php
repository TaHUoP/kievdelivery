@extends('layouts.main')

@section('title', trans('shop.filter.control'))

@section('page-title')
    <h4>{{ trans('shop.filter.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.filter.index').'">'.trans('shop.filter.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <button class="btn btn-link btn-float has-text text-size-small" data-toggle="modal" data-target="#modal-property-form-create">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>{{ trans('shop.property') }}</span>
    </button>
    <button class="btn btn-link btn-float has-text text-size-small" data-toggle="modal" data-target="#modal-value-form-create">
        <i class="icon-plus-circle2 text-primary"></i>
        <span>{{ trans('shop.value') }}</span>
    </button>
@stop

@section('content')

    @include('shop.properties.modal._create')
    @include('shop.values.modal._create')

    <div id="grid-filter">
        @include('shop.filter._ajax_index')
    </div>

@stop