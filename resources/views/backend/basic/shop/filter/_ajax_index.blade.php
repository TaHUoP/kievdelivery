<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.filter.all') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-filter"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-togglable table-hover">
            <thead>
                <tr>
                    <th data-toggle="true">{{ trans('shop.attr.properties') }}</th>
                    <th data-toggle="true">{{ trans('shop.attr.values') }}</th>
                    <th data-toggle="true" class="td-boolean">{{ trans('shop.properties.attr.is_filter') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse($properties as $property)
                    <tr>
                        <td>
                            <button class="property-edit label label-primary" data-url="{{ route('backend.shop.properties.edit', $property) }}">
                                {{ Translate::get($property->translations, 'name') }}
                            </button>
                        </td>
                        <td>
                            <?php if ($property->values->count() > 0) {
                                $property->values = $property->values->sortBy(function ($value, $key){
                                    return (int)$value->sort;
                                });
                            } ?>
                        @forelse($property->values as $value)
                                <button class="value-edit label bg-grey-300" data-url="{{ route('backend.shop.values.edit', $value) }}">
                                    {{ Translate::get($value->translations, 'name') }}
                                </button>
                            @empty
                                {{ trans('app.no-data') }}
                            @endforelse
                        </td>
                        <td class="td-boolean">
                            {{ Widget::run('BooleanView', ['value' => $property->is_filter]) }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($properties->count())
                {{ trans('app.all records :total', ['total' => $properties->total()])  }}
            @endif
        </div>
        @if ($properties->count())
            @include('_pagination', ['paginator' => $properties])
        @endif
    </div>
</div>