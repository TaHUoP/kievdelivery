<div id="modal-value-form-create" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(['route' => ['backend.shop.values.store'], 'class' => 'form-value', 'method' => 'POST']) }}
        <div class="modal-content">
            <div class="modal-header custom-modal-bg">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'value',
                ]) }}
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">
                    {{ trans('shop.attr.value') }}
                </h6>
            </div>
            <div class="modal-body">
                @include('shop.values._form')
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="btn bg-success legitRipple">{{ trans('app.save') }}</button>
                <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">{{ trans('app.close') }}</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>