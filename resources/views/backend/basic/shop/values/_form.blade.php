<div class="tabbable">
    <div class="tab-content">
        @foreach ($languageControl['all'] as $language)
            <?php
            if (!empty($value)) {
                $_translate = null;
                foreach ($value->translations as $translate) {
                    if ($language->id == $translate->lang_id) {
                        $_translate = $translate;
                    }
                }
            }
            ?>
            <div class="tab-pane{{ $language->default ? ' active' : '' }}" id="lang-value-{{ isset($value->id) ? $value->id . '-' : '' }}{{ $language->locale }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][name]', isset($_translate['name']) ? $_translate['name'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    {{ Form::label('alias', trans('app.attr.alias'), ['class' => 'control-label']) }}
                    {{ Form::text('alias', isset($value->alias) ? $value->alias : null, ['class' => 'form-control']) }}
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {{ Form::label('property_id', trans('shop.attr.property'), ['class' => 'control-label']) }}
                    @if (isset($value->properties[0]))
                        {{ Form::select('property_id', [
                            $value->properties[0]->id => Translate::get($value->properties[0]->translations, 'name')],
                            $value->properties[0]->id,
                            ['class' => 'select-ajax', 'data-url' => route('backend.shop.properties.index')
                        ]) }}
                    @else
                        {{ Form::select('property_id', [], null, ['class' => 'select-ajax', 'data-url' => route('backend.shop.properties.index')]) }}
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {{ Form::label('sort', trans('shop.attr.value.sort'), ['class' => 'control-label']) }}
                    {{ Form::text('sort', isset($value->sort) ? $value->sort : 0, ['class' => 'form-control']) }}
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>
</div>