{{ Form::open(['route' => ['backend.shop.categories.store'], 'method' => 'POST', 'id' => 'form-category']) }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('shop.categories.create') }}</h5>
            <div class="heading-elements panel-nav">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'category',
                ]) }}
            </div>
        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <p><strong><a href="{{ route('backend.shop.categories.create') }}" id="btn-categories-create" ><i class="icon-add"></i>  {{ trans('app.create') }}</a></strong></p>
                <br>
                <div class="content-group">
                    <h6 class="text-semibold no-margin-top">{{ trans('shop.categories.list') }}</h6>
                    <div class="tree-default" id="categories-tree">
                        {{ Widget::run('NestedList', [
                            'categories' => $categories,
                            'expandedId' => null,
                        ]) }}
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    @include('shop.categories._form')
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
            </div>
        </div>
    </div>
{{ Form::close() }}