{{ Form::open(['route' => ['backend.shop.categories.update', $category], 'method' => 'PUT', 'id' => 'form-category']) }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('shop.categories.edit') }}</h5>
            <div class="heading-elements panel-nav">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'category',
                ]) }}
            </div>
        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <p><strong><a href="{{ route('backend.shop.categories.create') }}" id="btn-categories-create" ><i class="icon-add"></i>  {{ trans('app.create') }}</a></strong></p>
                <br>
                <div class="content-group">
                    <h6 class="text-semibold no-margin-top">{{ trans('shop.categories.list') }}</h6>
                    <div id="categories-tree">
                        {{ Widget::run('NestedList', [
                            'categories' => $categories,
                            'expandedId' => $category->id,
                        ]) }}
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    @include('shop.categories._form', ['view_update' => true])
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
                <button type="button" data-url="{{ route('backend.shop.categories.destroy', $category) }}" data-confirm="{{ trans('app.confirm') }}" id="btn-categories-delete" data-c class="btn btn-danger heading-btn pull-right legitRipple">
                    <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                </button>
            </div>
        </div>
    </div>
{{ Form::close() }}