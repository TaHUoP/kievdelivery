<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('shop.categories') }}</h5>
    </div>
    <div class="panel-body">
        <div class="col-sm-3">
            <p><strong><a href="{{ route('backend.shop.categories.create') }}" id="btn-categories-create"><i class="icon-add"></i> {{ trans('app.create') }}</a></strong></p>
            <br>
            <div class="content-group">
                <h6 class="text-semibold no-margin-top">{{ trans('shop.categories.list') }}</h6>
                <div id="categories-tree">
                    {{ Widget::run('NestedList', [
                        'categories' => $categories,
                        'expandedId' => null,
                    ]) }}
                </div>
            </div>
        </div>
        <div class="col-sm-9"></div>
    </div>
    <div class="panel-footer"></div>
</div>