@extends('layouts.main')

@section('title', trans('shop.categories.control'))

@section('page-title')
    <h4>{{ trans('shop.categories.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('shop.shop'),
        '<a href="'.route('backend.shop.categories.index').'">'.trans('shop.categories.all').'</a>'
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    <div id="grid-categories">
        @include('shop.categories._ajax_index')
    </div>

@stop