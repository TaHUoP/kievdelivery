<div class="row">
    <div class="col-sm-6">
        <h6 class="text-semibold no-margin-top">{{ trans('shop.category') }}</h6>
        <div class="tabbable">
            <div class="tab-content">
                @foreach ($languageControl['all'] as $language)
                    <?php
                    if (isset($category)) {
                        $_translate = null;
                        foreach ($category->translations as $translate) {
                            if ($language->id == $translate->lang_id) {
                                $_translate = $translate;
                            }
                        }
                    }
                    ?>
                    <div class="tab-pane{{ $language->default ? ' active' : '' }}" id="lang-category-{{ $language->locale }}">
                        <div class="form-group">
                            {{ Form::label($language->locale . '[name]', trans('app.attr.name'), ['class' => 'control-label']) }}
                            {{ Form::text('translations['.$language->locale . '][name]', isset($_translate['name']) ? $_translate['name'] : null, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            {{ Form::label($language->locale . '[menu_name]', trans('app.attr.menu_name'), ['class' => 'control-label']) }}
                            {{ Form::text('translations['.$language->locale . '][menu_name]', isset($_translate['menu_name']) ? $_translate['menu_name'] : null, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            {{ Form::label($language->locale . '[meta_title]', trans('app.attr.meta_title'), ['class' => 'control-label']) }}
                            {{ Form::text('translations['.$language->locale . '][meta_title]', isset($_translate['meta_title']) ? $_translate['meta_title'] : null, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            {{ Form::label($language->locale . '[meta_description]', trans('app.attr.meta_description'), ['class' => 'control-label']) }}
                            {{ Form::text('translations['.$language->locale . '][meta_description]', isset($_translate['meta_description']) ? $_translate['meta_description'] : null, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            {{ Form::label($language->locale . '[meta_keywords]', trans('app.attr.meta_keywords'), ['class' => 'control-label']) }}
                            {{ Form::text('translations['.$language->locale . '][meta_keywords]', isset($_translate['meta_keywords']) ? $_translate['meta_keywords'] : null, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            {{ Form::label(trans('shop.attr.description_full'), null, ['class' => 'control-label']) }}
                            {{ Form::textarea('translations['.$language->locale.'][description_full]', isset($_translate['description_full']) ? $_translate['description_full'] : null, [
                                'class' => 'form-control summernote',
                                'data-locale' => $language->locale,
                            ]) }}
                            <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.description_full') ? $errors->first('translations.'.$language->locale.'.description_full') : '' }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <h6 class="text-semibold no-margin-top"><br></h6>
        <div class="form-group">
            {{ Form::label('alias', trans('app.attr.alias'), ['class' => 'control-label']) }}
            {{ Form::text('alias', isset($category->alias) ? $category->alias : null, ['class' => 'form-control']) }}
            <span class="help-block"></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="content-group">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {{ Form::label('parent_id', trans('shop.category_parent'), ['class' => 'control-label']) }}
                        @if (isset($category) && isset($category->parent))
                            {{ Form::select('parent_id', [
                                $category->parent->id => Translate::get($category->parent->translations, 'name')],
                                $category->id,
                                ['class' => 'select-ajax',
                                'data-url' => route('backend.shop.categories.index')
                            ]) }}
                        @else
                            {{ Form::select('parent_id',
                                [],
                                null,
                                ['class' => 'select-ajax',
                                'data-url' => route('backend.shop.categories.index')
                            ]) }}
                        @endif
                        <div class="help-block"></div>
                    </div>
                </div>
                @if (isset($view_update))
                    <div class="col-sm-2">
                        <div class="form-group">
                            {{ Form::label('order', trans('app.attr.order'), ['class' => 'control-label']) }}
                            {{ Form::text('order', isset($category) ? $category->order : 1, ['class' => 'form-control']) }}
                            <span class="help-block"></span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="checkbox checkbox-switchery">
                        <label>
                            {{ Form::checkbox('visible', 'on', !empty($category->visible) ? $category->visible : null, [
                                'class' => 'switchery',
                            ]) }}
                            {{ trans('shop.categories.attr.visible') }}
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="checkbox checkbox-switchery">
                        <label>
                            {{ Form::checkbox('recommended', 'on', !empty($category->recommended) ? $category->recommended : null, [
                                'class' => 'switchery',
                            ]) }}
                            {{ trans('shop.categories.attr.recommended') }}
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<p><br></p>

<div class="row">
    <div class="col-sm-12">
        <h6 class="text-semibold no-margin-top">{{ trans('shop.properties.add') }}</h6>
        <div class="form-group">
            {{ Form::label('properties', 'Выберите свойство', ['class' => 'control-label']) }}
            <select name="properties[]" multiple="multiple" class="select-ajax" data-url="{{ route('backend.shop.properties.index') }}">
                <?php
                if (isset($category)) {
                    foreach ($category->properties as $property) {
                        echo '<option selected="selected" value="' . $property->id . '">' . Translate::get($property->translations, 'name') . '</option>';
                    }
                }
                ?>
            </select>
            <div class="help-block"></div>
        </div>
    </div>
</div>