@extends('layouts.min')

@section('title', 'Вход')

@section('content')

    {{ Form::open(['route' => ['backend.auth.login']]) }}

        <div class="panel panel-body login-form">
            <div class="text-center">
                <div class="icon-object border-slate-800 text-slate-800"><i class="icon-windows2"></i></div>
                <h5 class="content-group">{{ trans('auth.login-to-your-account') }} <small class="display-block">{{ trans('auth.enter-your-credentials-below') }}</small></h5>
            </div>

            <div class="form-group has-feedback has-feedback-left required {{ $errors->has('username') ? 'has-error' : '' }}">
                {{ Form::text('username', old('username'), ['class' => 'form-control', 'placeholder' => trans('users.attr.username')]) }}
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
                <div class="help-block">{{ $errors->has('username') ? $errors->first('username') : '' }}</div>
            </div>

            <div class="form-group has-feedback has-feedback-left required {{ $errors->has('password') ? 'has-error' : '' }}">
                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('users.attr.password')]) }}
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
                <div class="help-block">{{ $errors->has('password') ? $errors->first('password') : '' }}</div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">{{ trans('auth.signin') }} <i class="icon-circle-right2 position-right"></i></button>
            </div>
        </div>

    {{ Form::close() }}

@stop