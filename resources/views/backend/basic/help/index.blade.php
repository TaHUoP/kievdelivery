@extends('layouts.main')

@section('title', 'Главная страница')

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="panel">
                <div class="panel-body text-center">
                    <div class="icon-object border-success-400 text-success"><i class="icon-book"></i></div>
                    <h5 class="text-semibold">Knowledge Base</h5>
                    <p class="mb-15">
                        Ouch found swore much dear conductively hid submissively hatchet vexed
                        far inanimately alongside candidly much and jeez
                    </p>
                    <a href="#" class="btn bg-success-400">Browse articles</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel">
                <div class="panel-body text-center">
                    <div class="icon-object border-warning-400 text-warning"><i class="icon-lifebuoy"></i></div>
                    <h5 class="text-semibold">Support center</h5>
                    <p class="mb-15">
                        Dear spryly growled much far jeepers vigilantly less and far
                        hideous and some mannishly less jeepers less and and crud
                    </p>
                    <a href="#" class="btn bg-warning-400">Open a ticket</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel">
                <div class="panel-body text-center">
                    <div class="icon-object border-blue text-blue"><i class="icon-reading"></i></div>
                    <h5 class="text-semibold">Articles and news</h5>
                    <p class="mb-15">
                        Diabolically somberly astride crass one endearingly blatant depending peculiar
                        antelope piquantly popularly adept much
                    </p>
                    <a href="#" class="btn bg-blue">Browse articles</a>
                </div>
            </div>
        </div>
    </div>


    <h4 class="text-center content-group">
        All questions are answered
        <small class="display-block">
            Am if number no up period regard sudden better.
            Decisively surrounded all admiration
        </small>
    </h4>

    <div class="row">
        <div class="col-lg-9">

            <div class="panel-group panel-group-control panel-group-control-right">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" href="#question1">
                                <i class="icon-help position-left text-slate"></i> ---?
                            </a>
                        </h6>
                    </div>
                    <div id="question1" class="panel-collapse collapse">
                        <div class="panel-body">
                            ---
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-size-small text-uppercase text-semibold text-muted mb-10">Selling and buying</div>
            <div class="panel-group panel-group-control panel-group-control-right">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" href="#question6">
                                <i class="icon-help position-left text-slate"></i> ---?
                            </a>
                        </h6>
                    </div>

                    <div id="question6" class="panel-collapse collapse">
                        <div class="panel-body">
                            ---
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-size-small text-uppercase text-semibold text-muted mb-10">Intellectual property</div>
            <div class="panel-group panel-group-control panel-group-control-right">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" href="#question11">
                                <i class="icon-help position-left text-slate"></i> ---?
                            </a>
                        </h6>
                    </div>

                    <div id="question11" class="panel-collapse collapse">
                        <div class="panel-body">
                            ---
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-size-small text-uppercase text-semibold text-muted mb-10">User accounts</div>
            <div class="panel-group panel-group-control panel-group-control-right">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" href="#question16">
                                <i class="icon-help position-left text-slate"></i>---?
                            </a>
                        </h6>
                    </div>

                    <div id="question16" class="panel-collapse collapse">
                        <div class="panel-body">
                            ---
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-3">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="panel-title">Search answers</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-stats-bars"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#">
                        <div class="input-group content-group">
                            <input type="text" class="form-control input-lg" placeholder="Search our help center">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-lg btn-icon"><i class="icon-search4"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@stop