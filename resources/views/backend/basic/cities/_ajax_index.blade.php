<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('cities.content') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-contents"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>{{ trans('app.translations') }}</th>
                <th>{{ trans('cities.attr.url') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($cities as $city)
                    <tr>
                        <td>
                            @forelse($city->translations as $translate)
                                <p>{{ $translate->name }} [{{ $translate->language->name }}]</p>
                            @empty
                                <p>{{ trans('app.no-translations') }}</p>
                            @endforelse
                        </td>
                        <td><a href="{{ route('site.index', ['city_alias' => $city->alias]) }}" target="_blank">{{ route('site.index', ['city_alias' => $city->alias]) }}</a></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('backend.cities.edit', $city) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                        <li>
                                            <a href="{{ route('backend.cities.destroy', $city) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>