@extends('layouts.main')

@section('title', trans('cities.create'))

@section('page-title')
    <h4>{{ trans('cities.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.cities.index').'">'.trans('cities.all').'</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    {{ Form::open(['route' => ['backend.cities.store']]) }}
        @include('cities._form')
    {{ Form::close() }}

@stop