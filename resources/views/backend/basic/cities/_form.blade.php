<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ isset($city) ? trans('cities.edit') : trans('cities.create') }}</h6>
        <div class="heading-elements panel-nav">
            {{ Widget::run('LanguagesSwitch', [
                'section' => 'content',
            ]) }}
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group required {{ $errors->has('url') ? 'has-error' : '' }}">
                    {{ Form::label('Alias', null, ['class' => 'control-label']) }}
                    {{ Form::text('alias', isset($city->alias) ? $city->alias : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('alias') ? $errors->first('alias') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required">
                    {{ Form::label(trans('cities.attr.price-type'), null, ['class' => 'control-label']) }}
                    {{ Form::select('price_type_id', $priceTypes, isset($city->price_type_id) ? $city->price_type_id : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('price_type_id') ? $errors->first('price_type_id') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('region_id') ? 'has-error' : '' }}">
                    {{ Form::label(trans('cities.attr.region'), null, ['class' => 'control-label']) }}
                    {{ Form::select('region_id', $regions, isset($city->region_id) ? $city->region_id : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('region_id') ? $errors->first('region_id') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('is_region_center') ? 'has-error' : '' }}">
                    {{ Form::label(trans('cities.attr.is_region_center'), null, ['class' => 'control-label']) }}
                    {{ Form::select('is_region_center', [ 0 => trans('app.No'), 1 => trans('app.Yes') ], isset($city->is_region_center) ? $city->is_region_center : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('is_region_center') ? $errors->first('is_region_center') : '' }}</div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            @foreach ($languages as $language)
                <?php
                if (isset($city)) {
                    $_translate = null;
                    foreach ($city->translations as $translate) {
                        if ($language->id == $translate->lang_id) {
                            $_translate = $translate;
                        }
                    }
                }
                ?>
                <div class="tab-pane fade in{{ $language->default ? ' active' : '' }}"
                     id="lang-content-{{ $language->locale }}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.text') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.text'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][text]', isset($_translate['text']) ? $_translate['text'] : null, [
                                    'class' => 'form-control summernote',
                                    'data-locale' => $language->locale,
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.text') ? $errors->first('translations.'.$language->locale.'.text') : '' }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.name') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.name'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][name]', isset($_translate['name']) ? $_translate['name'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.name') ? $errors->first('translations.'.$language->locale.'.name') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.name_declension') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.name_declension'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][name_declension]', isset($_translate['name_declension']) ? $_translate['name_declension'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.name_declension') ? $errors->first('translations.'.$language->locale.'.name_declension') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.key_words') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.key_words'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][key_words]', isset($_translate['key_words']) ? $_translate['key_words'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.key_words') ? $errors->first('translations.'.$language->locale.'.key_words') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_title') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.meta_title'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_title]', isset($_translate['meta_title']) ? $_translate['meta_title'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_title') ? $errors->first('translations.'.$language->locale.'.meta_title') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_description') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.meta_description'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_description]', isset($_translate['meta_description']) ? $_translate['meta_description'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_description') ? $errors->first('translations.'.$language->locale.'.meta_description') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? 'has-error' : '' }}">
                                {{ Form::label(trans('cities.attr.meta_keywords'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_keywords]', isset($_translate['meta_keywords']) ? $_translate['meta_keywords'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? $errors->first('translations.'.$language->locale.'.meta_keywords') : '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save')  }}</button>
            @if (isset($city))
                <a href="{{ route('backend.cities.destroy', $city) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete" class="btn btn-danger heading-btn pull-right legitRipple">
                    <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                </a>
            @endif
        </div>
    </div>
</div>