@extends('layouts.main')

@section('title', trans('cities.edit'))

@section('page-title')
    <h4>{{ trans('cities.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.cities.index').'">'.trans('cities.all').'</a>',
        trans('app.edit')
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.cities.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div class="tab-content">
        <div class="tab-pane active" id="tab-main">
            {{ Form::open(['route' => ['backend.cities.update', $city], 'method' => 'PUT']) }}
                @include('cities._form')
            {{ Form::close() }}
        </div>
    </div>

@stop