@extends('layouts.main')

@section('title', trans('cities.control'))

@section('page-title')
    <h4>{{ trans('cities.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.cities.index').'">'.trans('cities.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.cities.create') }}" class="btn btn-link btn-float has-text">
    	<i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')
    <div id="grid-contents">
        @include('cities._ajax_index')
    </div>
@stop