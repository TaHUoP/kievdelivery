<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-md-6 col-sm-6 col-xs-6 col-lg-6">
                <div class="form-group">
                    <label class="control-label">rbac_name</label>
                    <input type="text" class="form-control" name="RbacForm[name]" value="user" disabled="disabled">
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-md-8 col-sm-8 col-xs-8 col-lg-8">
                <div class="form-group">
                    <label class="control-label">rbac_description</label>
                    <textarea class="form-control" name="RbacForm[description]">User</textarea>

                    <div class="help-block"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#system" data-toggle="tab">system</a></li>
            <li class=""><a href="#user" data-toggle="tab">users</a></li>
            <li class=""><a href="#content" data-toggle="tab">contents</a></li>
            <li class=""><a href="#widget" data-toggle="tab">widgets</a></li>
            <li class=""><a href="#language" data-toggle="tab">languages</a></li>
            <li class=""><a href="#rbac" data-toggle="tab">rbac</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="system" style="padding: 15px">
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][system-backend-login]" value="1"> &nbsp;Логин в админпанель</label>
                </div>
            </div>

            <div class="tab-pane fade in" id="user" style="padding: 15px">
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][user-actions]" value="1"> &nbsp;Действия пользователей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][user-create]" value="1"> &nbsp;Создание пользователей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][user-delete]" value="1"> &nbsp;Удаление пользователей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][user-update]" value="1"> &nbsp;Редактирование пользователей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][user-view]" value="1"> &nbsp;Просмотр пользователей</label>
                </div>
            </div>

            <div class="tab-pane fade in" id="content" style="padding: 15px">
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][content-create]" value="1"> &nbsp;Создание контента</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][content-delete]" value="1"> &nbsp;Удаление контента</label>
                </div>
                <div class="form-group">
                    <input type="hidden" name="RbacForm[permissions][content-update]" value="0">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][content-update]" value="1"> &nbsp;Редактирование контента</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][content-view]" value="1"> &nbsp;Просмотр контента</label>
                </div>
            </div>

            <div class="tab-pane fade in" id="widget" style="padding: 15px">
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][widget-create]" value="1"> &nbsp;Создание виджетов</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][widget-delete]" value="1"> &nbsp;Удаление виджетов</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][widget-update]" value="1"> &nbsp;Редактирование виджетов</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][widget-view]" value="1"> &nbsp;Просмотр виджетов</label>
                </div>
            </div>

            <div class="tab-pane fade in" id="language" style="padding: 15px">
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][language-create]" value="1"> &nbsp;Создание языков</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][language-delete]" value="1"> &nbsp;Удаление языков</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][language-translations]" value="1"> &nbsp;Управениепереводами</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][language-update]" value="1"> &nbsp;Редактирование языков</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][language-view]" value="1"> &nbsp;Просмотр языков</label>
                </div>
            </div>

            <div class="tab-pane fade in" id="rbac" style="padding: 15px">
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][rbac-create]" value="1"> &nbsp;Создание ролей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][rbac-delete]" value="1"> &nbsp;Удаление ролей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][rbac-update]" value="1"> &nbsp;Редактирование ролей</label>
                </div>
                <div class="form-group">
                    <label><input type="checkbox" class="pull-left" name="RbacForm[permissions][rbac-view]" value="1"> &nbsp;Просмотр ролей</label>
                </div>
            </div>
        </div>
    </div>
</div>