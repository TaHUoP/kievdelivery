@extends('layouts.main')

@section('title', 'Права доступа')

@section('page-title')
    <h4>Управление ролями</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.access.index').'">Все роли</a>'
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.access.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span></a>
@stop

@section('content')

    <div class="alert alert-warning">
        <i class="icon-alert"></i>
        В стадии разработки
    </div>


    {{--
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">Права доступа</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-access"></a></li>
                </ul>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table datatable-basic table-hover">
                <thead>
                <tr>
                    <th>Управление</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Доступ в админ панель</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td class="text-left">
                        <a href="#"><i class="icon-cancel-circle2"></i></a> &nbsp;
                        <a href="#"><i class="icon-pencil"></i></a>
                    </td>
                    <td>Администратор</td>
                    <td>Описание</td>
                    <td>
                        <span class="label label-success">Да</span>
                    </td>
                </tr>

                <tr>
                    <td class="text-left">
                        <a href="#"><i class="icon-cancel-circle2"></i></a> &nbsp;
                        <a href="#"><i class="icon-pencil"></i></a>
                    </td>
                    <td>Пользователь</td>
                    <td>Описание</td>
                    <td>
                        <span class="label label-danger">Нет</span>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>

        <div class="datatable-footer">
            <div class="dataTables_info">
                Всего записей 10 000
            </div>
            <div class="dataTables_paginate paging_simple_numbers">
                <a class="paginate_button previous disabled">←</a>
                <span>
                    <a class="paginate_button current">1</a>
                    <a class="paginate_button ">2</a>
                </span>
                <a class="paginate_button next">→</a>
            </div>
        </div>
    </div>
    --}}
@stop