@extends('layouts.main')

@section('title', 'Редактировать роль')

@section('page-title')
    <h4>Редактировать</span> роль</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.access.index').'">Все роли</a>',
        'Редактировать'
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.access.create') }}" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Создать</span></a>
@stop

@section('content')

    @include('_form')

@stop