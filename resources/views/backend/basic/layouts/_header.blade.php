<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            @yield('page-title', '<h4>' . trans('app.control-panel') . '</h4>')
            @yield('breadcrumbs')
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                @yield('heading-elements')
            </div>
        </div>
    </div>
</div>