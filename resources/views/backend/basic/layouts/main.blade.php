<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('default/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('default/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('default/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('default/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('default/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('extends/css/extends.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('plugins/elfinder/css/elfinder.full.css', true) }}" rel="stylesheet" type="text/css">
    <link href="{{ Statics::asset('plugins/elfinder/css/theme.css', true) }}" rel="stylesheet" type="text/css">
</head>
<body>

@include('layouts._top')
@include('layouts._header')

<div class="page-container">
    <div class="page-content">
        <div class="content-wrapper">

            @if(Session::has('danger'))
                <div class="alert alert-danger alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {!! Session::get('danger') !!}
                </div>
            @endif

            @if(Session::has('warning'))
                <div class="alert alert-warning alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {!! Session::get('warning') !!}
                </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {!! Session::get('success') !!}
                </div>
            @endif

            @yield('content')
        </div>
    </div>
</div>

@include('layouts._footer')


<script src="{{ Statics::asset('default/js/plugins/loaders/pace.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/core/libraries/jquery.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/core/libraries/jquery_ui/full.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/core/libraries/bootstrap.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/ui/moment/moment_locales.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/ui/nicescroll.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/ui/drilldown.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/forms/tags/tokenfield.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/tables/footable/footable.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/uploaders/dropzone.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/editors/summernote/lang/summernote-ru-RU.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/trees/fancytree_all.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/ui/prism.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/plugins/ui/ripple.min.js') }}"></script>
<script src="{{ Statics::asset('default/js/core/app.js') }}"></script>

<script src="{{ Statics::asset('plugins/elfinder/js/elfinder.min.js', true) }}"></script>
<script src="{{ Statics::asset('plugins/elfinder/js/i18n/elfinder.ru.js', true) }}"></script>

<script src="{{ Statics::asset('extends/js/cPanelApi.js') }}"></script>
<script src="{{ Statics::asset('extends/js/initPlugins.js') }}"></script>
<script src="{{ Statics::asset('extends/js/main.js') }}"></script>
<script src="{{ Statics::asset('extends/js/elfinder.js') }}"></script>

@stack('scripts')

<script src="{{ Statics::asset('extends/modules/base/fileSaver.js') }}"></script>
<script src="{{ Statics::asset('extends/modules/base/imageUploader.js') }}"></script>
<script src="{{ Statics::asset('extends/modules/shop/categories.js') }}"></script>
<script src="{{ Statics::asset('extends/modules/shop/checkout.js') }}"></script>
<script src="{{ Statics::asset('extends/modules/shop/filter.js') }}"></script>
<script src="{{ Statics::asset('extends/modules/shop/products.js') }}"></script>

<script src="{{ Statics::asset('extends/modules/media/media.js') }}"></script>

</body>
</html>
