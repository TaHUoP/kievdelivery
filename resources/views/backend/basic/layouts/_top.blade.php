<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('backend.site.index') }}">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 65">
                <path d="M14.27 33.12L15.55 3a3.16 3.16 0 0 1 3.3-3l2.25.08a3.15 3.15 0 0 1 3 3.26l-1.27 30.14a3.16 3.16 0 0 1-3.3 3l-2.23-.1a3.15 3.15 0 0 1-3.03-3.26zm23.4 19.4l1.26-9a3.45 3.45 0 0 0 .15-.94v-.06a3.58 3.58 0 0 0-3.6-3.52H25.1a3.55 3.55 0 1 0 0 7.1H31l-1.1 6.55a3.5 3.5 0 0 0 .9 2.9l7 7.6a3.63 3.63 0 0 0 5.07.22 3.54 3.54 0 0 0 .23-5zM43.22 9.8h-.06l-2-.92A3.2 3.2 0 0 0 37 10.45L28.38 31a3.13 3.13 0 0 0 1.52 4.16h.1l2 .9a3.2 3.2 0 0 0 4.18-1.57l8.53-20.6a3.13 3.13 0 0 0-1.5-4.1zM32.5 60.48a1.9 1.9 0 0 1 .34 1.72 15.26 15.26 0 0 1-1.54 2.2 2 2 0 0 1-1.3.6H12.4a7.65 7.65 0 0 0-2.06-4.73C8.3 58.1 4 57.4 1.4 53.47s-1.14-7.47.54-9A21.4 21.4 0 0 1 8.34 40a14.8 14.8 0 0 1 8.5-.8c2.44.5 2.1 2.2 2.1 2.2a8.1 8.1 0 0 0 1.22 4.4 4.22 4.22 0 0 0 3.46 2h5.24s-1.2 6.34-1 7c.5 1.63 1.5 2.28 2.37 3.28 1.1 1.2 1.47 1.62 2.27 2.4zm-23.77-15a1.34 1.34 0 1 0-1.36 1.35 1.35 1.35 0 0 0 1.36-1.4z"/>
            </svg>
            {{-- <span class="navbar-brand-name">Company Name</span> --}}
        </a>
        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    @if (AuthHelper::isAccess())
        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav navbar-left navbar-nav-material">
                <li>
                    <a href="{{ route('backend.site.index') }}"><i class="icon-display4 position-left"></i> {{ trans('app.home') }}</a>
                </li>
                <li class="dropdown mega-menu mega-menu-wide <?=(($requestInfo['section'] === 'system') ? 'active' : '')?>">
                    <a href="{{ route('backend.user.users.index') }}" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-puzzle4 position-left"></i> {{ trans('system.system') }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-content col-md-9">
                        <div class="dropdown-content-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('users.users') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.user.users.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.user.users.index') }}"><i class="icon-users"></i> {{ trans('users.all') }}</a>
                                        </li>
                                        {{--<li>
                                            <a href="{{ route('backend.user.users.actions')  }}"><i class="icon-footprint"></i> {{ trans('users.actions') }}</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.access.index') }}"><i class="icon-lock2"></i> {{ trans('users.access') }}</a>
                                        </li>--}}
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('contents.content') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.contents.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.contents.index') }}"><i class="icon-files-empty2"></i> {{ trans('contents.all') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.cities.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.cities.index') }}"><i class="icon-files-empty2"></i> {{ trans('cities.all') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.plugins.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.plugins.index') }}"><i class="icon-file-spreadsheet2"></i> {{ trans('plugins.all') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.seo.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.seo.index') }}"><i class="icon-code"></i> {{ trans('seo.specifications') }}</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('system.system') }}</span>
                                    <ul class="menu-list">
                                        {{--<li>
                                            <a href="{{ route('backend.site.settings')  }}"><i class="icon-gear"></i> {{ trans('system.settings') }}</a>
                                        </li>--}}
                                        <li <?=(($requestInfo['route'] == 'backend.site.logs') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.site.logs')  }}"><i class="icon-archive"></i> {{ trans('system.logs') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.languages.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.languages.index') }}"><i class="icon-flag3"></i> {{ trans('system.languages') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.currencies.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.currencies.index') }}"><i class="icon-coins"></i> {{ trans('system.currencies') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="dropdown mega-menu mega-menu-wide <?=(($requestInfo['section'] === 'shop') ? 'active' : '')?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-cart2 position-left"></i> {{ trans('shop.shop') }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-content col-md-9">
                        <div class="dropdown-content-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('app.common') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.shop.categories.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.categories.index') }}"><i class="icon-menu8"></i> {{ trans('shop.categories') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.shop.filter.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.filter.index')  }}"><i class="icon-pushpin"></i> {{ trans('shop.filter') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.shop.landings.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.landings.index')  }}"><i class="icon-pagebreak"></i> {{ trans('shop.landings') }}</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('shop.products') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.shop.products.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.products.index') }}"><i class="icon-basket"></i> {{ trans('shop.products.all') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.shop.products.create') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.products.create')  }}"><i class="icon-cart-add"></i> {{ trans('shop.products.add') }}</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('app.extra') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.shop.checkouts.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.checkouts.index') }}"><i class="icon-phone-outgoing"></i> {{ trans('shop.checkouts') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.shop.coupons.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.coupons.index') }}"><i class="icon-price-tag2"></i> {{ trans('shop.coupons') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.shop.discounts.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.shop.discounts.index') }}"><i class="icon-price-tag2"></i> {{ trans('shop.discounts') }}</a>
                                        </li>
                                        {{--<li>
                                            <a href=""><i class="icon-gear"></i> {{ trans('system.settings') }}</a>
                                        </li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="dropdown mega-menu mega-menu-wide <?=(($requestInfo['section'] === 'media') ? 'active' : '')?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-images2 position-left"></i> {{ trans('media.media') }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-content col-md-9">
                        <div class="dropdown-content-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('media.photo_and_video') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.media.images.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.media.images.index') }}"><i class="icon-image2"></i> {{ trans('media.photo_gallery') }}</a>
                                        </li>
                                        <li <?=(($requestInfo['route'] == 'backend.media.videos.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.media.videos.index')  }}"><i class="icon-clapboard-play"></i> {{ trans('media.video_gallery') }}</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <span class="menu-heading underlined">{{ trans('reviews.reviews') }}</span>
                                    <ul class="menu-list">
                                        <li <?=(($requestInfo['route'] == 'backend.reviews.index') ? 'class="active"' : '')?>>
                                            <a href="{{ route('backend.reviews.index') }}"><i class="icon-image2"></i> {{ trans('reviews.reviews') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-nav-material visible-lg-block">
                <li>
                    <a href="/" target="_blank">
                        <i class="icon-home"></i> <span class="visible-xs-inline-block position-right"> {{ trans('to-website') }}</span>
                    </a>
                </li>
                <li class="dropdown language-switch">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Statics::img('default/images/flags/' . $languageControl['current']['locale'].'.png') }}" class="position-left" alt=""> <?=$languageControl['current']['name']?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach ($languageControl['all'] as $language)
                            <li><a href="{{ route('backend.site.set-lang', ['lang' => $language['locale']]) }}" class="lang-{{ $language['locale'] }}"><img src="{{ Statics::img('default/images/flags/' . $language['locale'].'.png') }}" alt=""> {{ $language['name'] }}</a></li>
                        @endforeach
                    </ul>
                </li>
                {{ Widget::run('message-top-info') }}
                {{ Widget::run('userface-top-info') }}
            </ul>
        </div>
    @else
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="/" target="_blank">
                    <i class="icon-home"></i> <span class="visible-xs-inline-block position-right"> {{ trans('to-website') }}</span>
                </a>
            </li>
        </ul>
    @endif
</div>