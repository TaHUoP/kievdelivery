<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google" content="notranslate">
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="{{ Statics::asset('default/img/favicon.png') }}">
        <link href="{{ Statics::asset('default/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Statics::asset('default/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Statics::asset('default/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Statics::asset('default/css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Statics::asset('default/css/colors.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Statics::asset('extends/css/extends.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body class="login-container">
        @include('layouts._top')
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        @include('layouts._footer')

        <script type="text/javascript" src="{{ Statics::asset('default/js/plugins/loaders/pace.min.js') }}"></script>
        <script type="text/javascript" src="{{ Statics::asset('default/js/core/libraries/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ Statics::asset('default/js/core/libraries/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ Statics::asset('default/js/plugins/loaders/blockui.min.js') }}"></script>
        <script type="text/javascript" src="{{ Statics::asset('default/js/core/app.js') }}"></script>

    </body>
</html>
