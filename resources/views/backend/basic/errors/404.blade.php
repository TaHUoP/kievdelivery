@extends('layouts.main')

@section('title', '404')

@section('content')

    <div class="text-center content-group">
        <h1 class="error-title">404</h1>
        <h5>Oops, an error has occurred. Page not found!</h5>
    </div>

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            <div class="text-center">
                <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="btn bg-pink-400">
                    <i class="icon-circle-left2 position-left"></i> Go Back
                </a>
            </div>
        </div>
    </div>

@endsection


