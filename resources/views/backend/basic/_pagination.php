<div class="dataTables_paginate paging_simple_numbers">
    <?php

    $linkCount = 7;
    $pageCount = $paginator->lastPage();

    if ($pageCount > 1) {
        $currentPage = $paginator->currentPage();
        $pagesEitherWay = floor($linkCount / 2);
        $paginationHtml = '';

        // Previous item
        $previousDisabled = $currentPage == 1 ? 'disabled' : '';

        if ($previousDisabled != 'disabled') {
            $paginationHtml .= '<a href="' . $paginator->url($currentPage - 1) . '" class="paginate_button previous ' . $previousDisabled . '">←</a>';
        }

        // Set the first and last pages
        $startPage = ($currentPage - $pagesEitherWay) < 1 ? 1 : $currentPage - $pagesEitherWay;
        $endPage = ($currentPage + $pagesEitherWay) > $pageCount ? $pageCount : ($currentPage + $pagesEitherWay);

        // Alter if the start is too close to the end of the list
        if ($startPage > $pageCount - $linkCount) {
            $startPage = ($pageCount - $linkCount) + 1;
            $endPage = $pageCount;
        }

        // Alter if the end is too close to the start of the list
        if ($endPage <= $linkCount) {
            $startPage = 1;
            $endPage = $linkCount < $pageCount ? $linkCount : $pageCount;
        }

        $paginationHtml .= '<span>';

        // Loop through and collect
        for ($i = $startPage; $i <= $endPage; $i++) {
            $disabledClass = $i == $currentPage ? 'disabled' : '';
            $paginationHtml .= '<a href="' . $paginator->url($i) . '" class="paginate_button ' . (($currentPage == $i) ? 'current' : '') . '">' . $i . '</a>';
        }

        $paginationHtml .= '</span>';

        // Next item
        $nextDisabled = $currentPage == $pageCount ? 'disabled' : '';

        if ($nextDisabled != 'disabled') {
            $paginationHtml .= '<a href="' . $paginator->url($currentPage + 1) . '" class="paginate_button next ' . $nextDisabled . '">→</a>';
        }

        echo $paginationHtml;
    }

    ?>

</div>