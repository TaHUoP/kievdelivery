<div id="tab-images">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('media.photo_gallery') }}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-media-images"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('backend.media.images.upload') }}" class="dropzone" id="images-uploads" data-drop="{{ trans('app.drag-and-drop-image-upload') }}">
                @foreach ($images as $image)
                    <div data-modal="false" id="{{ $image->id }}" class="dz-complete dz-image-preview image-preview <?=(($image->default) ? 'is-main-photo' : '')?>" data-url="{{ route('backend.media.images.edit', $image) }}">
                        <div class="dz-preview dz-file-preview">
                            <div style="background-image: url('{!! Media::getImagePreview($image, true) !!}');" class="dz-img" data-dz-thumbnail></div>
                            <div class="dz-progress"><span style="width: 100%;" class="dz-upload" data-dz-uploadprogress=""></span></div>
                            <span class="dz-error"><i class="icon-cross2"></i></span>
                            <span class="dz-done"><i class="icon-checkmark3"></i></span>
                            <button type="button" class="dz-remove" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.media.images.destroy', $image) }}">
                                <i class="icon-close2"></i>
                            </button>
                        </div>
                    </div>
                @endforeach
            </form>
            <div class="preview" style="display:none;">
                <div data-modal="false" class="image-preview new" data-url="{{ route('backend.media.images.edit', ['images' => '_id']) }}">
                    <div class="dz-preview dz-file-preview">
                        <i class="icon-spinner10 spinner position-left"></i>
                        <img class="dz-img" data-dz-thumbnail />
                        <img class="dz-amby" data-dz-thumbnail />
                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                        <span class="dz-error"><i class="icon-cross2"></i></span>
                        <span class="dz-done"><i class="icon-checkmark3"></i></span>
                        <button type="button" class="dz-remove" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.media.images.destroy', ['images' => '_id']) }}">
                            <i class="icon-close2"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="datatable-footer">
            <div class="dataTables_info">
                @if ($images->count())
                    {{ trans('app.all records :total', ['total' => $images->total()])  }}
                @endif
            </div>
            @if ($images->count())
                @include('_pagination', ['paginator' => $images])
            @endif
        </div>
    </div>
</div>