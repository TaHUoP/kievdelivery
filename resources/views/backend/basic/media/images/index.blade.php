@extends('layouts.main')

@section('title', trans('media.images.control'))

@section('page-title')
    <h4>{{ trans('media.images.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('media.media'),
        '<a href="'.route('backend.media.images.index').'">'.trans('media.images.all').'</a>',
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    <div id="grid-media-images">
        @include('media.images._ajax_index')
    </div>

@stop