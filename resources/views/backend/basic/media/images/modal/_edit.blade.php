<div id="modal-image-form" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(['route' => ['backend.media.images.update', $image], 'class' => 'form-media-image', 'method' => 'PUT']) }}
        <div class="modal-content">
            <div class="modal-header custom-modal-bg">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'image-' . $image->id,
                ]) }}
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">{{ trans('app.attr.image') }}</h6>
            </div>
            <div class="modal-body">
                @include('media.images._form')
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="btn bg-success legitRipple">{{ trans('app.save') }}</button>
                <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">{{ trans('app.close') }}</button>
                <button type="button" class="btn bg-danger pull-right legitRipple btn-filter-destroy" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.media.images.destroy', $image) }}">
                    {{ trans('app.delete') }}
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>