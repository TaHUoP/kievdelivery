<div class="tabbable">
    <div class="tab-content">
        @foreach ($languageControl['all'] as $language)
            <?php
            if (!empty($image)) {
                $_translate = null;
                foreach ($image->translations as $translate) {
                    if ($language->id == $translate->lang_id) {
                        $_translate = $translate;
                    }
                }
            }
            ?>
            <div class="tab-pane{{ $language->default ? ' active' : '' }}" id="lang-video-{{ isset($image->id) ? $image->id . '-' : '' }}{{ $language->locale }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[title]', trans('app.attr.title'), ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][title]', isset($_translate['title']) ? $_translate['title'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[description]', trans('app.attr.description'), ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][description]', isset($_translate['description']) ? $_translate['description'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>