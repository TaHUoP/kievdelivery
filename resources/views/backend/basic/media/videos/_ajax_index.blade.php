<div id="tab-videos">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('media.video_gallery') }}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-media-videos"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                @forelse($videos as $video)
                    <div class="col-sm-3">
                        <div class="thumbnail video-preview">
                            <div class="video-container">
                                <iframe width="560" height="315" src="{{ empty($video->preview_url) ? $video->review_url : $video->preview_url }}" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="caption">
                                <h6 class="no-margin">
                                    <span class="text-default">{{ Translate::get($video->translations, 'title') }}</span>
                                </h6>
                            </div>
                        </div>
                    </div>
                @empty
                    {{ trans('app.no-data') }}
                @endforelse
            </div>
        </div>
        <div class="datatable-footer">
            <div class="dataTables_info">
                @if ($videos->count())
                    {{ trans('app.all records :total', ['total' => $videos->total()])  }}
                @endif
            </div>
            @if ($videos->count())
                @include('_pagination', ['paginator' => $videos])
            @endif
        </div>
    </div>
</div>
