<div id="modal-video-form-edit" class="modal fade">
    <div class="modal-dialog">
        {{ Form::open(['route' => ['backend.media.videos.update', $video], 'class' => 'form-media-video', 'method' => 'PUT']) }}
        <div class="modal-content">
            <div class="modal-header custom-modal-bg">
                {{ Widget::run('LanguagesSwitch', [
                    'section' => 'video-' . $video->id,
                ]) }}
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">
                    {{ trans('app.attr.video') }}
                </h6>
            </div>
            <div class="modal-body">
                @include('media.videos._form')
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="btn bg-success legitRipple">{{ trans('app.save') }}</button>
                <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">{{ trans('app.close') }}</button>
                <button type="button" class="btn bg-danger pull-right legitRipple btn-filter-destroy" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.media.video.destroy', $video) }}">
                    {{ trans('app.delete') }}
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>









