@extends('layouts.main')

@section('title', trans('media.videos.control'))

@section('page-title')
    <h4>{{ trans('media.videos.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        trans('media.media'),
        '<a href="'.route('backend.media.videos.index').'">'.trans('media.videos.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <button type="submit" class="btn btn-link btn-float has-text" data-toggle="modal" data-target="#modal-video-form-create">
        <i class="icon-plus-circle2 text-primary"></i> <span>{{ trans('app.create') }}</span>
    </button>
@stop

@section('content')

    @include('media.videos.modal._create')

    <div id="grid-media-videos">
        @include('media.videos._ajax_index')
    </div>

@stop