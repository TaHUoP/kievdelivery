<div class="tabbable">
    <div class="tab-content">
        @foreach ($languageControl['all'] as $language)
            <?php
            if (!empty($video)) {
                $_translate = null;
                foreach ($video->translations as $translate) {
                    if ($language->id == $translate->lang_id) {
                        $_translate = $translate;
                    }
                }
            }
            ?>
            <div class="tab-pane{{ $language->default ? ' active' : '' }}" id="lang-video-{{ isset($video->id) ? $video->id . '-' : '' }}{{ $language->locale }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[title]', trans('app.attr.title'), ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][title]', isset($_translate['title']) ? $_translate['title'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label($language->locale . '[description]', trans('app.attr.description'), ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale . '][description]', isset($_translate['description']) ? $_translate['description'] : null, ['class' => 'form-control']) }}
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {{ Form::label('url', trans('app.attr.url'), ['class' => 'control-label']) }}
                    {{ Form::text('url', isset($video->url) ? $video->url : null, ['class' => 'form-control']) }}
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>
</div>