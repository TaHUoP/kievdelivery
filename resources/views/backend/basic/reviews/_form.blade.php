<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ isset($review) ? trans('reviews.edit') : trans('reviews.create') }}</h6>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.type'), null, ['class' => 'control-label']) }}
                    {{ Form::select('type', $types, isset($review->type) ? $review->type : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('type') ? $errors->first('type') : '' }}</div>
                </div>
            </div>
            {{--<div class="col-sm-3">--}}
                {{--<div class="form-group required">--}}
                    {{--{{ Form::label(trans('reviews.attr.user_id'), null, ['reviews' => 'control-label']) }}--}}
                    {{--{{ Form::text('user_id', isset($review->user_id) ? $review->user_id : null, ['class' => 'form-control']) }}--}}
                    {{--<div class="help-block">{{ $errors->has('user_id') ? $errors->first('user_id') : '' }}</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-sm-3">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.product_id'), null, ['reviews' => 'control-label']) }}
                    {{ Form::text('product_id', isset($review->product_id) ? $review->product_id : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('product_id') ? $errors->first('product_id') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.rating'), null, ['reviews' => 'control-label']) }}
                    {{ Form::text('rating', isset($review->rating) ? $review->rating : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('rating') ? $errors->first('rating') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.confirmed'), null, ['class' => 'control-label']) }}
                    {{ Form::select('confirmed', [
                        0 => trans('app.no'),
                        1 => trans('app.yes'),
                    ], isset($review->confirmed) ? $review->confirmed : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('confirmed') ? $errors->first('confirmed') : '' }}</div>
                </div>
            </div>
            {{--<div class="col-sm-2">
                <div class="form-group required">
                    <div class="checkbox checkbox-switchery">
                        <label>
                            {{ Form::checkbox('confirmed', 'on', !empty($review->confirmed) ? $review->confirmed : null, [
                            'class' => 'switchery',
                            ]) }}
                            {{ trans('reviews.attr.confirmed') }}
                        </label>
                    </div>
                </div>
            </div>--}}
            <div class="col-sm-2">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.user_id'), null, ['reviews' => 'control-label']) }}
                    {{ Form::text('name', isset($review->name) ? $review->name : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.user_email'), null, ['reviews' => 'control-label']) }}
                    {{ Form::text('email', isset($review->email) ? $review->email : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group required">
                    {{ Form::label(trans('reviews.attr.city_delivery'), null, ['reviews' => 'control-label']) }}
                    {{ Form::text('city_delivery', isset($review->city_delivery) ? $review->city_delivery : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('city_delivery') ? $errors->first('city_delivery') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group required {{ $errors->has('text') ? 'has-error' : '' }}">
                    {{ Form::label(trans('reviews.attr.text'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('text', isset($review->text) ? $review->text : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('text') ? $errors->first('text') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group {{ $errors->has('admin_answer') ? 'has-error' : '' }}">
                    {{ Form::label(trans('reviews.attr.admin_answer'), null, ['class' => 'control-label']) }}
                    {{ Form::textarea('admin_answer', isset($review->admin_answer) ? $review->admin_answer : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('admin_answer') ? $errors->first('admin_answer') : '' }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit"
                    class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save')  }}</button>
            @if (isset($review))
                <a href="{{ route('backend.reviews.destroy', ['id' =>$review->id]) }}"
                   data-confirm="{{ trans('app.confirm') }}"
                   data-method="delete" class="btn btn-danger heading-btn pull-right legitRipple">
                    <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                </a>
            @endif
        </div>
    </div>
</div>