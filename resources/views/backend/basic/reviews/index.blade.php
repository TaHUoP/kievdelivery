@extends('layouts.main')

@section('title', trans('reviews.control'))

@section('page-title')
    <h4>{{ trans('reviews.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.reviews.index').'">'.trans('reviews.reviews').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.reviews.create') }}" class="btn btn-link btn-float has-text">
    	<i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')
    <div id="grid-contents">
        @include('reviews._ajax_index')
    </div>
@stop