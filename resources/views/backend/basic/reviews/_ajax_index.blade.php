<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('reviews.reviews') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-contents"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>{{ trans('app.attr.visible') }}</th>
                <th>{{ trans('reviews.attr.type') }}</th>
                <th>{{ trans('reviews.attr.text') }}</th>
                <th>{{ trans('reviews.attr.rating') }}</th>
                <th>{{ trans('reviews.attr.created_at') }}</th>
                <th>{{ trans('reviews.attr.product_id') }}</th>
                <th>{{ trans('reviews.attr.user_id') }}</th>
                <th>{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>

            @forelse($reviews as $review)
                <tr>
                    <td class="td-status">{{(Widget::run('BooleanView', ['value' => $review->confirmed]))}}</td>
                    <td>{{ array_get($review->getTypesArray(), $review->type) }}</td>
                    <td>{{$review->text}}</td>
                    <td>{{$review->rating}}</td>
                    <td>
                        <p>{!! DateTimeHelper::toFullDateTime($review->created_at)?: trans('app.no-data') !!}</p>
                        <p>{!! DateTimeHelper::toFullDateTime($review->updated_at)?: trans('app.no-data') !!}</p>
                    </td>
                    @if (isset($review->product_id))
                        <td>
                            <a href="{{ route('backend.shop.products.edit', $review->product) }}">
                                {{ Translate::get($review->product->translations, 'name') ?: trans('app.no-description') }}
                            </a>
                        </td>
                    @else
                        <td>{{trans('shop.no-product')}}</td>
                    @endif
                    <td>{{ $review->name }}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-cog"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('backend.reviews.edit', ['id' =>$review->id]) }}"><i
                                                    class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                    <li>
                                        <a href="{{ route('backend.reviews.destroy', ['id' =>$review->id]) }}"
                                           data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                            <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">{{ trans('app.no-data') }}</td>
                </tr>
            @endforelse

            </tbody>
        </table>
        {{ $reviews->links() }}
    </div>
</div>