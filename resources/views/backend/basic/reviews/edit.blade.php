@extends('layouts.main')

@section('title', trans('reviews.edit'))

@section('page-title')
    <h4>{{ trans('reviews.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.reviews.index').'">'.trans('reviews.reviews').'</a>',
        trans('app.edit')
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.reviews.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div class="tab-reviews">
        <div class="tab-pane active" id="tab-main">
            {{ Form::open(['route' => ['backend.reviews.update', $review], 'method' => 'PUT']) }}
                @include('reviews._form')
            {{ Form::close() }}
        </div>
    </div>


@stop