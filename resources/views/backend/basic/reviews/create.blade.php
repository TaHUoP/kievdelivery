@extends('layouts.main')

@section('title', trans('reviews.create'))

@section('page-title')
    <h4>{{ trans('reviews.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.reviews.index').'">'.trans('reviews.reviews').'</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')
    {{ Form::open(['route' => ['backend.reviews.store']]) }}
        @include('reviews._form')
    {{ Form::close() }}
@stop