@extends('layouts.main')

@section('title', trans('plugins.create'))

@section('page-title')
    <h4>{{ trans('plugins.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.plugins.index').'">'.trans('plugins.plugins').'</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')
    
@stop

@section('content')

    {{ Form::open(['route' => ['backend.plugins.store']]) }}
    @include('plugins._form')
    {{ Form::close() }}

@stop