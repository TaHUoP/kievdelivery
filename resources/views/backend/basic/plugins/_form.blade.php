<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ isset($plugin) ? trans('plugins.edit') : trans('plugins.create') }}</h6>
    </div>
    <div class="panel-body">
        @if (!isset($plugin))
            <div class="col-sm-3">
                <div class="form-group required">
                    {{ Form::label('key', trans('app.attr.type'), ['class' => 'control-label']) }}
                    {{ Form::select('type', [
                        'slider' => 'Слайдер',
                        'text' => 'Текст',
                    ], null, ['class' => 'select form-control']) }}
                    <div class="help-block">{{ $errors->has('type') ? $errors->first('type') : '' }}</div>
                </div>
            </div>
        @endif
        <div class="col-sm-3">
            <div class="form-group required {{ $errors->has('key') ? 'has-error' : '' }}">
                {{ Form::label('key', trans('app.attr.alias'), ['class' => 'control-label']) }}
                {{ Form::text('key', isset($plugin->key) ? $plugin->key : null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('key') ? $errors->first('key') : '' }}</div>
            </div>
        </div>
        <div class="col-sm-3">
            <br>
            <div class="checkbox checkbox-switchery">
                <label>
                    {{ Form::checkbox('visible', 'on', !empty($plugin->visible) ? $plugin->visible : null, [
                        'class' => 'switchery',
                    ]) }}
                    <div class="help-block">{{ $errors->has('visible') ? $errors->first('visible') : '' }}</div>
                    {{ trans('shop.product.product_visible') }}
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group required {{ $errors->has('description') ? 'has-error' : '' }}">
                {{ Form::label('description', trans('app.attr.description'), ['class' => 'control-label']) }}
                {{ Form::text('description', isset($plugin->description) ? $plugin->description : null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('description') ? $errors->first('description') : '' }}</div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save')  }}</button>
        </div>
    </div>
</div>