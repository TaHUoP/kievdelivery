<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('plugins.plugins') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-plugins"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th class="td-name">{{ trans('app.attr.type') }}</th>
                <th class="td-name">{{ trans('app.attr.alias') }}</th>
                <th>{{ trans('app.attr.description') }}</th>
                <th class="td-boolean">{{ trans('app.attr.visible') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
            @if ($plugins->count())
                @foreach ($plugins as $plugin)
                    <tr>
                        <td class="td-name"><strong>{{ $plugin->type }}</strong></td>
                        <td class="td-name"><strong>{{ $plugin->key }}</strong></td>
                        <td>{{ trim($plugin->description) ? $plugin->description : trans('app.no-data')}}</td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $plugin->visible]) }}</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="{{ route('backend.plugins.control', $plugin) }}">
                                                <i class="icon-cog"></i> {{ trans('app.control') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.plugins.edit', $plugin) }}">
                                                <i class="icon-pencil"></i> {{ trans('app.edit') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.plugins.destroy', $plugin) }}"
                                               data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">{{ trans('app.no-data') }}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($plugins->count())
                {{ trans('app.all records :total', ['total' => $plugins->total()])  }}
            @endif
        </div>
        @if ($plugins->count())
            @include('_pagination', ['paginator' => $plugins])
        @endif
    </div>
</div>