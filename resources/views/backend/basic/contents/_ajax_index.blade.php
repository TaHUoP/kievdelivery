<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('contents.content') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-contents"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>{{ trans('app.translations') }}</th>
                <th>{{ trans('contents.attr.url') }}</th>
                <th class="td-status">{{ trans('contents.attr.type') }}</th>
                <th class="td-boolean">{{ trans('contents.attr.visible') }}</th>
                <th class="td-date">{{ trans('app.update') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($contents as $content)
                    <tr>
                        <td>
                            @forelse($content->translations as $translate)
                                <p>{{ $translate->title }} [{{ $translate->language->name }}]</p>
                            @empty
                                <p>{{ trans('app.no-translations') }}</p>
                            @endforelse
                        </td>
                        <td><a href="{{ $content->getUrl() }}" target="_blank">{{ $content->getUrl() }}</a></td>
                        <td>{{ array_get($content->getTypesArray(), $content->type) }}</td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $content->visible]) }}</td>
                        <td class="td-date">
                            <p>{!! DateTimeHelper::toFullDateTime($content->created_at)?: trans('app.no-data') !!}</p>
                            <p>{!! DateTimeHelper::toFullDateTime($content->updated_at)?: trans('app.no-data') !!}</p>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('backend.contents.edit', $content) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                        <li>
                                            <a href="{{ route('backend.contents.destroy', $content) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($contents->count())
                {{ trans('app.all records :total', ['total' => $contents->total()])  }}
            @endif
        </div>
        @if ($contents->count())
            @include('_pagination', ['paginator' => $contents])
        @endif
    </div>
</div>