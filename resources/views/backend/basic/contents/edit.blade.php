@extends('layouts.main')

@section('title', trans('contents.edit'))

@section('page-title')
    <h4>{{ trans('contents.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.contents.index').'">'.trans('contents.all').'</a>',
        trans('app.edit')
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.contents.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div class="panel panel-default">
        <ul class="nav nav-tabs nav-tabs-bottom bottom-divided nav-justified" style="margin: 0">
            <li class="active"><a href="#tab-main" data-toggle="tab">{{ trans('app.main') }}</a></li>
            <li><a href="#tab-images" data-toggle="tab">{{ trans('app.images') }}</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="tab-main">
            {{ Form::open(['route' => ['backend.contents.update', $content], 'method' => 'PUT']) }}
                @include('contents._form')
            {{ Form::close() }}
        </div>

        <div class="tab-pane" id="tab-images">
            @include('contents._tab-images')
        </div>
    </div>

@stop