<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('media.photo_gallery') }}</h5>
    </div>
    <div class="panel-body">
        <form action="{{ route('backend.content.images.upload', ['content' => $content->id]) }}" class="dropzone" id="images-uploads" data-drop="{{ trans('app.drag-and-drop-image-upload') }}">
            @foreach ($content->images as $image)
                <div id="{{ $image->id }}" class="dz-complete dz-image-preview image-preview <?=(($image->default) ? 'is-main-photo' : '')?>" data-url="{{ route('backend.contentImages.edit', $image) }}">
                    <div class="dz-preview dz-file-preview">
                        <div style="background-image: url('{!! ContentImage::getImagePreview($image, true) !!}');" class="dz-img" data-dz-thumbnail></div>
                        <div class="dz-progress"><span style="width: 100%;" class="dz-upload" data-dz-uploadprogress=""></span></div>
                        <span class="dz-error"><i class="icon-cross2"></i></span>
                        <span class="dz-done"><i class="icon-checkmark3"></i></span>
                        <button type="button" class="dz-remove" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.contentImages.destroy', $image) }}">
                            <i class="icon-close2"></i>
                        </button>
                    </div>
                </div>
            @endforeach
        </form>
        <div class="preview" style="display:none;">
            <div class="image-preview new" data-url="{{ route('backend.contentImages.edit', ['images' => '_id']) }}">
                <div class="dz-preview dz-file-preview">
                    <i class="icon-spinner10 spinner position-left"></i>
                    <img class="dz-img" data-dz-thumbnail/>
                    <img class="dz-amby" data-dz-thumbnail/>
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <span class="dz-error"><i class="icon-cross2"></i></span>
                    <span class="dz-done"><i class="icon-checkmark3"></i></span>
                    <button class="dz-remove" data-confirm="{{ trans('app.confirm') }}" data-url="{{ route('backend.contentImages.destroy', ['images' => '_id']) }}">
                        <i class="icon-close2"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>