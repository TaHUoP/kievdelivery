<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ isset($content) ? trans('contents.edit') : trans('contents.create') }}</h6>
        <div class="heading-elements panel-nav">
            {{ Widget::run('LanguagesSwitch', [
                'section' => 'content',
            ]) }}
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group required">
                    {{ Form::label(trans('contents.attr.type'), null, ['class' => 'control-label']) }}
                    {{ Form::select('type', $typesArray, isset($content->type) ? $content->type : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('type') ? $errors->first('type') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('url') ? 'has-error' : '' }}">
                    {{ Form::label(trans('contents.attr.url'), null, ['class' => 'control-label']) }}
                    {{ Form::text('url', isset($content->url) ? $content->url : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->has('url') ? $errors->first('url') : '' }}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group required">
                    {{ Form::label(trans('contents.attr.visible'), null, ['class' => 'control-label']) }}
                    {{ Form::select('visible', [
                        0 => trans('app.no'),
                        1 => trans('app.yes'),
                    ], isset($content->visible) ? $content->visible : null, ['class' => 'form-control select']) }}
                    <div class="help-block">{{ $errors->has('visible') ? $errors->first('visible') : '' }}</div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            @foreach ($languages as $language)
                <?php
                if (isset($content)) {
                    $_translate = null;
                    foreach ($content->translations as $translate) {
                        if ($language->id == $translate->lang_id) {
                            $_translate = $translate;
                        }
                    }
                }
                ?>
                <div class="tab-pane fade in{{ $language->default ? ' active' : '' }}"
                     id="lang-content-{{ $language->locale }}">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group required {{ $errors->has('translations.'.$language->locale.'.title') ? 'has-error' : '' }}">
                                {{ Form::label(trans('contents.attr.title'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][title]', isset($_translate['title']) ? $_translate['title'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.title') ? $errors->first('translations.'.$language->locale.'.title') : '' }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.text') ? 'has-error' : '' }}">
                                {{ Form::label(trans('contents.attr.text'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][text]', isset($_translate['text']) ? $_translate['text'] : null, [
                                    'class' => 'form-control summernote',
                                    'data-upload' => route('backend.contents.upload'),
                                    'data-locale' => $language->locale,
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.text') ? $errors->first('translations.'.$language->locale.'.text') : '' }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.info') ? 'has-error' : '' }}">
                                {{ Form::label(trans('contents.attr.info'), null, ['class' => 'control-label']) }}
                                {{ Form::textarea('translations['.$language->locale.'][info]', isset($_translate['info']) ? $_translate['info'] : null, [
                                    'class' => 'form-control summernote',
                                    'data-upload' => route('backend.contents.upload'),
                                    'data-locale' => $language->locale,
                                ]) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.info') ? $errors->first('translations.'.$language->locale.'.info') : '' }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_title') ? 'has-error' : '' }}">
                                {{ Form::label(trans('contents.attr.meta_title'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_title]', isset($_translate['meta_title']) ? $_translate['meta_title'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_title') ? $errors->first('translations.'.$language->locale.'.meta_title') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_description') ? 'has-error' : '' }}">
                                {{ Form::label(trans('contents.attr.meta_description'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_description]', isset($_translate['meta_description']) ? $_translate['meta_description'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_description') ? $errors->first('translations.'.$language->locale.'.meta_description') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? 'has-error' : '' }}">
                                {{ Form::label(trans('contents.attr.meta_keywords'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_keywords]', isset($_translate['meta_keywords']) ? $_translate['meta_keywords'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? $errors->first('translations.'.$language->locale.'.meta_keywords') : '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save')  }}</button>
            @if (isset($content))
                <a href="{{ route('backend.contents.destroy', $content) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete" class="btn btn-danger heading-btn pull-right legitRipple">
                    <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                </a>
            @endif
        </div>
    </div>
</div>