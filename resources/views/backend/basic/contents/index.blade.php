@extends('layouts.main')

@section('title', trans('contents.control'))

@section('page-title')
    <h4>{{ trans('contents.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.contents.index').'">'.trans('contents.all').'</a>',
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.contents.create') }}" class="btn btn-link btn-float has-text">
    	<i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')
    <div id="grid-contents">
        @include('contents._ajax_index')
    </div>
@stop