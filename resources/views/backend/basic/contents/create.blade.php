@extends('layouts.main')

@section('title', trans('contents.create'))

@section('page-title')
    <h4>{{ trans('contents.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.contents.index').'">'.trans('contents.all').'</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    {{ Form::open(['route' => ['backend.contents.store']]) }}
        @include('contents._form')
    {{ Form::close() }}

@stop