@extends('layouts.main')

@section('title', trans('seo.create'))

@section('page-title')
    <h4>{{ trans('seo.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.seo.index').'">'.trans('seo.all').'</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')

    {{ Form::open(['route' => ['backend.seo.store']]) }}

        @include('seo._form')

    {{ Form::close() }}

@stop