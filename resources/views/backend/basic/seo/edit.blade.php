@extends('layouts.main')

@section('title', trans('seo.edit'))

@section('page-title')
    <h4>{{ trans('seo.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.seo.index').'">'.trans('seo.all').'</a>',
        trans('app.edit')
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.seo.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-pencil text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    {{ Form::open(['route' => ['backend.seo.update', $record], 'method' => 'PUT']) }}

        @include('seo._form')

    {{ Form::close() }}

@stop