<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ isset($record) ? trans('seo.edit') : trans('seo.create') }}</h6>
        <div class="heading-elements panel-nav">
            {{ Widget::run('LanguagesSwitch', [
                'section' => 'seo-specification',
            ]) }}
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group required {{ $errors->has('url') ? 'has-error' : '' }}">
                    {{ Form::label('url', trans('seo.attr.url'), ['class' => 'control-label']) }}
                    {{ Form::text('url', isset($record->url) ? $record->url : null, ['class' => 'form-control']) }}
                    <div class="help-block">{{ $errors->first('url') }}</div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            @foreach ($languages as $language)
                <?php
                if (isset($record)) {
                    $_translate = null;
                    foreach ($record->translations as $translate) {
                        if ($language->id == $translate->lang_id) {
                            $_translate = $translate;
                        }
                    }
                }
                ?>
                <div class="tab-pane fade in{{ $language->default ? ' active' : '' }}" id="lang-seo-specification-{{ $language->locale }}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.title') ? 'has-error' : '' }}">
                                {{ Form::label(trans('seo.attr.title'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][title]', isset($_translate['title']) ? $_translate['title'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.title') ? $errors->first('translations.'.$language->locale.'.title') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_title') ? 'has-error' : '' }}">
                                {{ Form::label(trans('seo.attr.meta_title'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_title]', isset($_translate['meta_title']) ? $_translate['meta_title'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_title') ? $errors->first('translations.'.$language->locale.'.meta_title') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_description') ? 'has-error' : '' }}">
                                {{ Form::label(trans('seo.attr.meta_description'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_description]', isset($_translate['meta_description']) ? $_translate['meta_description'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_description') ? $errors->first('translations.'.$language->locale.'.meta_description') : '' }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group {{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? 'has-error' : '' }}">
                                {{ Form::label(trans('seo.attr.meta_keywords'), null, ['class' => 'control-label']) }}
                                {{ Form::text('translations['.$language->locale.'][meta_keywords]', isset($_translate['meta_keywords']) ? $_translate['meta_keywords'] : null, ['class' => 'form-control']) }}
                                <div class="help-block">{{ $errors->has('translations.'.$language->locale.'.meta_keywords') ? $errors->first('translations.'.$language->locale.'.meta_keywords') : '' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save')  }}</button>
            @if (isset($record))
                <a href="{{ route('backend.seo.destroy', $record) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete" class="btn btn-danger heading-btn pull-right legitRipple">
                    <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                </a>
            @endif
        </div>
    </div>
</div>