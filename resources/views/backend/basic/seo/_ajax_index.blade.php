<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('seo.specifications') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-seo"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>{{ trans('seo.attr.url') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($records as $record)
                    <tr>
                        <td><a href="{{ url($record->url) }}">{{ url($record->url) }}</a></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('backend.seo.edit', $record) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                        <li>
                                            <a href="{{ route('backend.seo.destroy', $record) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($records->count())
                {{ trans('app.all records :total', ['total' => $records->total()])  }}
            @endif
        </div>
        @if ($records->count())
            @include('_pagination', ['paginator' => $records])
        @endif
    </div>
</div>