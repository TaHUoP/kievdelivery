@extends('layouts.main')

@section('title', trans('languages.control'))

@section('page-title')
    <h4>{{ trans('languages.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.languages.index').'">'.trans('languages.all').'</a>'
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.languages.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>

    <a href="{{ route('backend.languages.translations', ['id' => $defaultLanguage->id, 'with' => $notDefaultLanguage ? $notDefaultLanguage->id : null]) }}" class="btn btn-link btn-float has-text">
        <i class="icon-graduation2 text-primary"></i><span>{{ trans('languages.translates') }}</span>
    </a>
@stop

@section('content')

    <div id="grid-languages">
        @include('languages._ajax_index')
    </div>

@stop