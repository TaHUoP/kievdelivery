@extends('layouts.main')

@section('title', trans('languages.edit'))

@section('page-title')
    <h4>{{ trans('languages.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.languages.index').'">'.trans('languages.all').'</a>',
        $language->name
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.languages.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>

    <a href="{{ route('backend.languages.translations', ['id' => $language]) }}" class="btn btn-link btn-float has-text">
        <i class="icon-graduation2 text-primary"></i><span>{{ trans('languages.translates') }}</span>
    </a>
@stop

@section('content')

    {{ Form::open(['route' => ['backend.languages.update', $language], 'method' => 'PUT']) }}

        @include('languages._form')

    {{ Form::close() }}

@stop