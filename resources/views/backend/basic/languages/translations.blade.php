@extends('layouts.main')

@section('title', trans('languages.translations-control'))

@section('page-title')
    <h4>{{ trans('languages.translations-control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.languages.index').'">'.trans('languages.all').'</a>'
    ]) }}
@stop

@section('content')
    <div class="row">
        <div class="col-sm-5">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <h5 class="panel-title">{{ trans('app.filter') }}</h5>
                </div>
                {{ Form::open(['method' => 'GET']) }}
                    <div class="panel-body">
                        <div class="form-group col-sm-6 no-margin-bottom">
                            <label for="language_to_edit">{{ trans('languages.group') }}</label>
                            {{ Form::select('group', $groups, Request::query('group'), [
                                'id' => 'group',
                                'name' => 'group',
                                'class' => 'select form-control'
                            ]) }}
                        </div>
                        <div class="form-group col-sm-6 no-margin-bottom">
                            <label for="with">{{ trans('languages.secondary-language') }}</label>
                            {{ Form::select('with',
                                $allExcept->pluck('name', 'id')->prepend(trans('app.no'), 0)->toArray(), Request::query('with'), [
                                'class' => 'select form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="heading-elements">
                            <button type="submit" class="btn btn-primary heading-btn pull-left legitRipple">
                                {{ trans('app.btn.filter') }}
                            </button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        {{ Form::open(['route' => ['backend.languages.translationsUpdate'], 'method' => 'PUT']) }}
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('app.translations') }}</h5>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ trans('languages.attr.constant') }}</th>
                            <th>{{ $mainLanguage->name }}</th>
                            @if (isset($semiLanguage))
                                <th>{{ $semiLanguage->name }}</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($formTranslations as $key => $translation)
                            <tr>
                                <td class="text-left" style="max-width: 350px; width: 350px;">{{ $translation['constant'] }}</td>
                                <td class="text-left">
                                    <div class="form-group">
                                        <?php
                                            $inputNameId = "translates[{$mainLanguage->locale}][{$translation['group']}][{$translation['constant']}][id]";
                                            $inputNameGroup = "translates[{$mainLanguage->locale}][{$translation['group']}][{$translation['constant']}][group]";
                                            $inputNameConst = "translates[{$mainLanguage->locale}][{$translation['group']}][{$translation['constant']}][constant]";
                                            $inputName = "translates[{$mainLanguage->locale}][{$translation['group']}][{$translation['constant']}][text]";
                                        ?>
                                        {{ Form::hidden($inputNameId, $translation['mainId']) }}
                                        {{ Form::hidden($inputNameGroup, $translation['group']) }}
                                        {{ Form::hidden($inputNameConst, $translation['constant']) }}
                                        {{ Form::textarea($inputName, $translation['mainTranslate'], ['class' => 'form-control', 'rows' => '2']) }}
                                    </div>
                                </td>
                                @if (isset($semiLanguage))
                                    <td class="text-left">
                                        <div class="form-group">
                                            @if ($semiLanguage)
                                                <?php
                                                    $inputNameId = "translates[{$semiLanguage->locale}][{$translation['group']}][{$translation['constant']}][id]";
                                                    $inputNameGroup = "translates[{$semiLanguage->locale}][{$translation['group']}][{$translation['constant']}][group]";
                                                    $inputNameConst = "translates[{$semiLanguage->locale}][{$translation['group']}][{$translation['constant']}][constant]";
                                                    $inputName = "translates[{$semiLanguage->locale}][{$translation['group']}][{$translation['constant']}][text]";
                                                ?>
                                                {{ Form::hidden($inputNameId, $translation['semiId']) }}
                                                {{ Form::hidden($inputNameGroup, $translation['group']) }}
                                                {{ Form::hidden($inputNameConst, $translation['constant']) }}
                                                {{ Form::textarea($inputName, $translation['semiTranslate'], ['class' => 'form-control', 'rows' => '2']) }}
                                            @endif
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">{{ trans('app.no-data') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="table-footer"></div>
                    <div class="panel-footer">
                        <div class="heading-elements">
                            @if (!empty($formTranslations))
                                <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">
                                    {{ trans('app.save') }}
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}
    </div>
@stop