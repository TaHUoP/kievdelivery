<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('languages.language') }}</h5>
    </div>
    <div class="panel-body">
        <div class="col-sm-2">
            <div class="form-group required {{ $errors->has('locale') ? 'has-error' : '' }}">
                {{ Form::label('locale', trans('languages.attr.locale'), ['class' => 'control-label']) }}
                {{ Form::text('locale', isset($language->locale) ? $language->locale : null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('locale') ? $errors->first('locale') : '' }}</div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', trans('languages.attr.name'), ['class' => 'control-label']) }}
                {{ Form::text('name', isset($language->name) ? $language->name : null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group checkbox checkbox-switchery {{ $errors->has('active') ? 'has-error' : '' }}">
                        <label>
                            {{ Form::checkbox('active', 'on', !empty($language->active) ? $language->active : null, [
                                'class' => 'switchery',
                            ]) }}
                            {{ trans('languages.attr.active') }}
                            <div class="help-block">{{ $errors->has('active') ? $errors->first('active') : '' }}</div>
                        </label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group checkbox checkbox-switchery {{ $errors->has('default') ? 'has-error' : '' }}">
                        <label>
                            {{ Form::checkbox('default', 'on', !empty($language->default) ? $language->default : null, [
                                'class' => 'switchery',
                            ]) }}
                            {{ trans('languages.attr.default') }}
                            <div class="help-block">{{ $errors->has('default') ? $errors->first('default') : '' }}</div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
        </div>
    </div>
</div>