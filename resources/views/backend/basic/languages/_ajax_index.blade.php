<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('languages.languages') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-languages"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>{{ trans('languages.attr.locale') }}</th>
                <th>{{ trans('languages.attr.name') }}</th>
                <th>{{ trans('languages.attr.default') }}</th>
                <th>{{ trans('languages.attr.active') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($languages as $language)
                    <tr>
                        <td class="td-name">{{ $language->locale }}</td>
                        <td>{{ $language->name }}</td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $language->default]) }}</td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $language->active]) }}</td>
                        <td class="td-control text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('backend.languages.edit', $language) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                        <li><a href="{{ route('backend.languages.destroy', $language) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                            <i class="icon-close2"></i> {{ trans('app.delete') }}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($languages->count())
                {{ trans('app.all records :total', ['total' => $languages->count()])  }}
            @endif
        </div>
    </div>
</div>