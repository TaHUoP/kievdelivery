<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ trans('app.profile-info') }}</h6>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                        {{ Form::label('name', trans('app.attr.name')) }}
                        {{ Form::text('name', isset($user) ? $user->profile->name : null, [
                            'class' => 'form-control'
                        ]) }}
                        <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group required {{ $errors->has('surname') ? 'has-error' : '' }}">
                        {{ Form::label('surname', trans('app.attr.surname')) }}
                        {{ Form::text('surname', isset($user) ? $user->profile->surname : null, [
                            'class' => 'form-control'
                        ]) }}
                        <div class="help-block">{{ $errors->has('surname') ? $errors->first('surname') : '' }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="control-label checkbox checkbox-switch">
                        <label>
                            {{ Form::checkbox('access_cpanel', null, isset($user) ? $user->access_cpanel : false, ['class' => 'form-control switchery']) }}
                            &nbsp; {{ trans('app.attr.cpanel_access') }}
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="control-label checkbox checkbox-switch">
                        <label>
                            {{ Form::checkbox('status', null, isset($user) ? $user->status : false, ['class' => 'form-control switchery']) }}
                            &nbsp; {{ trans('app.status.active') }}
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group no-margin-bottom">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                        {{ Form::label('avatar', trans('app.attr.upload-profile-image'), ['class' => 'display-block']) }}
                        <div class="media no-margin-top">
                            <div class="media-left">
                                <img src="{{ isset($user) ? $user->profile->avatarUrl() : Statics::img('default/images/placeholder.jpg') }}" style="width: 100px; height: auto; border-radius: 2px;" alt="">
                            </div>
                            <div class="media-body">
                                {{ Form::file('avatar', ['class' => 'file-styled']) }}
                                <span class="help-block">{{ trans('app.help.file-accepted-formats') }}</span>
                                <span class="help-block">{{ $errors->has('avatar') ? $errors->first('avatar') : '' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn heading-btn btn-success">{{ trans('app.save') }}</button>
        </div>
    </div>
</div>