@extends('layouts.main')

@section('title', trans('users.create'))

@section('page-title')
    <h4>{{ trans('users.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.user.users.index').'">' .trans('users.all'). '</a>',
        trans('app.create')
    ]) }}
@stop

@section('heading-elements')

@stop

@section('content')
    {{ Form::open([
        'route' => ['backend.user.users.store'],
        'method' => 'POST',
        'enctype' => 'multipart/form-data',
    ]) }}
        <div class="row">
            <div id="settings">
                <div class="col-sm-6">
                    @include('users._form-profile')
                </div>
                <div class="col-sm-6">
                    @include('users._form-settings')
                </div>
            </div>
        </div>
    {{ Form::close() }}
@stop