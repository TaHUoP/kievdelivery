@extends('layouts.main')

@section('title', trans('users.edit'))

@section('page-title')
    <h4>{{ trans('users.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.user.users.index').'">' .trans('users.all'). '</a>',
        trans('users.edit')
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.user.users.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-user-plus text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')
    {{ Form::open([
        'route' => ['backend.user.users.update', $user],
        'method' => 'PUT',
        'enctype' => 'multipart/form-data'
    ]) }}
        <div class="row">
            <div id="settings">
                <div class="col-sm-6">
                    @include('users._form-profile')
                </div>
                <div class="col-sm-6">
                    @include('users._form-settings')
                </div>
            </div>
        </div>
    {{ Form::close() }}
@stop