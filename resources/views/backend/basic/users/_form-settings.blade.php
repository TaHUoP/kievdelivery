<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">{{ trans('app.account-settings') }}</h6>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group required {{ $errors->has('phone') ? 'has-error' : '' }}">
                    {{ Form::label('phone', trans('app.attr.phone')) }}
                    {{ Form::text('phone', isset($user) ? $user->phone : null, [
                        'class' => 'form-control',
                    ]) }}
                    <div class="help-block">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group required {{ $errors->has('email') ? 'has-error' : '' }}">
                    {{ Form::label('email', trans('app.attr.email')) }}
                    {{ Form::email('email', isset($user) ? $user->email : null, [
                        'class' => 'form-control',
                    ]) }}
                    <div class="help-block">{{ $errors->has('email') ? $errors->first('email') : '' }}</div>
                </div>
            </div>
            @if (isset($user))
                <div class="col-sm-6">
                    <div class="form-group required {{ $errors->has('password') ? 'has-error' : '' }}">
                        {{ Form::label('password', trans('users.attr.cur-password')) }}
                        {{ Form::input('password', 'password', 'password', [
                            'class' => 'form-control',
                            'disabled' => 'disabled',
                        ]) }}
                        <div class="help-block">{{ $errors->has('password') ? $errors->first('password') : '' }}</div>
                    </div>
                </div>
            @endif
        </div>

        <div class="row">
            @if(!isset($user))
                <div class="col-sm-6">
                    <div class="form-group required {{ $errors->has('password') ? 'has-error' : '' }}">
                        {{ Form::label('password', trans('users.attr.cur-password')) }}
                        {{ Form::input('password', 'password', null, [
                            'class' => 'form-control',
                        ]) }}
                        <div class="help-block">{{ $errors->has('password') ? $errors->first('password') : '' }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group required {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        {{ Form::label('password_confirmation', trans('users.attr.new-password-confirmation')) }}
                        {{ Form::password('password_confirmation', [
                            'class' => 'form-control',
                            'placeholder' => trans('users.repeat-new-password')
                        ]) }}
                        <div class="help-block">{{ $errors->has('password_confirmation') ? $errors->first('password_confirmation') : '' }}</div>
                    </div>
                </div>
            @else
                <div class="col-sm-6">
                    <div class="form-group required {{ $errors->has('new_password') ? 'has-error' : '' }}">
                        {{ Form::label('new_password', trans('users.attr.new-password')) }}
                        {{ Form::password('new_password', [
                            'class' => 'form-control',
                            'placeholder' => trans('users.enter-new-password'),
                        ]) }}
                        <div class="help-block">{{ $errors->has('new_password') ? $errors->first('new_password') : '' }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group required {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}">
                        {{ Form::label('new_password_confirmation', trans('users.attr.new-password-confirmation')) }}
                        {{ Form::password('new_password_confirmation', [
                            'class' => 'form-control',
                            'placeholder' => trans('users.repeat-new-password')
                        ]) }}
                        <div class="help-block">{{ $errors->has('new_password_confirmation') ? $errors->first('new_password_confirmation') : '' }}</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>