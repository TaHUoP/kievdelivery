@extends('layouts.main')

@section('title', trans('users.control'))

@section('page-title')
    <h4>{{ trans('users.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.user.users.index').'">' .trans('users.all'). '</a>'
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.user.users.create') }}" class="btn btn-link btn-float has-text"><i class="icon-user-plus text-primary"></i><span>{{ trans('app.create') }}</span></a>
@stop

@section('content')

    <div id="grid-users">
        @include('users._ajax_index')
    </div>

@stop