<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('users.users') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-users"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>Пользователь</th>
                {{--<th>Роль</th>--}}
                <th class="td-date">Регистрация</th>
                <th class="td-date">Последний вход</th>
                <th class="td-status">Управление</th>
                <th class="td-status">{{ trans('app.attr.status') }}</th>
                <th class="td-status">{{ trans('app.deleted') }}</th>
                <th data-toggle="true" class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td>
                        {{ Widget::run('UserfaceShortInfo', ['user' => $user]) }}
                    </td>
                    {{--<td>
                        <p>{{ $user->role ?: 'Нет роли' }}</p>
                    </td>--}}
                    <td>{!! DateTimeHelper::toFullDateTime($user->created_at)?: trans('app.no-data') !!}</td>
                    <td>{!! DateTimeHelper::toFullDateTime($user->visit_at)?: trans('app.no-data') !!}</td>
                    <td class="text-center">
                        {{ Widget::run('BooleanView', ['value' => $user->access_cpanel]) }}
                    </td>
                    <td class="text-center">
                        {{ Widget::run('UserStatusView', ['value' => $user->status]) }}
                    </td>
                    <td class="text-center">
                        {{ Widget::run('BooleanView', ['value' => $user->trashed()]) }}
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    <i class="icon-cog"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('backend.user.users.edit', $user) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                    @if (!$user->trashed())
                                        <li>
                                            <a href="{{ route('backend.user.users.destroy', $user) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                                <i class="icon-cancel-circle2"></i> {{ trans('app.delete') }}
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ route('backend.user.users.restore', $user) }}" data-confirm="{{ trans('app.confirm') }}" data-method="post">
                                                <i class="icon-cancel-circle2"></i> {{ trans('app.restore') }}
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">{{ trans('app.no-data') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($users->count())
                {{ trans('app.all records :total', ['total' => $users->total()])  }}
            @endif
        </div>
        @if ($users->count())
            @include('_pagination', ['paginator' => $users])
        @endif
    </div>
</div>