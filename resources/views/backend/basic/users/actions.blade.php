@extends('layouts.main')

@section('title', trans('users.users_actions') )

@section('page-title')
    <h4>{{ trans('users.users_actions') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.user.users.index').'">'.trans('users.all').'</a>',
        trans('users.users_actions')
    ]) }}
@stop

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h5 class="panel-title">{{ trans('users.users_actions') }}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table datatable-basic table-hover">
                <thead>
                <tr>
                    <th>{{ trans('app.attr.user') }}</th>
                    <th>{{ trans('app.attr.date') }}</th>
                    <th>{{ trans('app.attr.section') }}</th>
                    <th>{{ trans('app.attr.action') }}</th>
                    <th>{{ trans('app.attr.info') }}</th>
                </tr>
                </thead>
                <tbody>

                    @if ($actions->count())
                        @foreach ($actions as $action)
                            <tr>
                                <td>----
                                    {{ Widget::run('UserfaceShortInfo', ['user' => $action->user]) }}
                                </td>
                                <td>20.01.2016, 15:00</td>
                                <td>---Админ панель</td>
                                <td>---Создал товар</td>
                                <td>---User Agent Hash Ip</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">{{ trans('app.no-data') }}</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
        <div class="datatable-footer">
            <div class="dataTables_info">
                @if ($actions->count())
                    {{ trans('app.all records :total', ['total' => $actions->total()])  }}
                @endif
            </div>
            @if ($actions->count())
                @include('_pagination', ['paginator' => $actions])
            @endif
        </div>
    </div>
@stop