<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">Навигация</h6>
    </div>
    <div class="list-group no-border no-padding-top">
        <a href="#" class="list-group-item"><i class="icon-user"></i> Мои купоны</a>
        <a href="#" class="list-group-item"><i class="icon-cash3"></i> Мои заказы
            <span class="badge bg-success pull-right">29</span>
        </a>
        <a href="#" class="list-group-item"><i class="icon-users"></i> Мои адреса</a>
        <div class="list-group-divider"></div>
        <a href="#" class="list-group-item"><i class="icon-calendar3"></i> Платежная система</a>
        <a href="#" class="list-group-item"><i class="icon-cog3"></i> Выход</a>
    </div>
</div>