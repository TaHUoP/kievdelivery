@extends('layouts.main')

@section('title', trans('currencies.control'))

@section('page-title')
    <h4>{{ trans('currencies.control') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.currencies.index').'">'.trans('currencies.all').'</a>'
    ]) }}
@stop

@section('heading-elements')
    <a href="{{ route('backend.currencies.create') }}" class="btn btn-link btn-float has-text">
        <i class="icon-plus-circle2 text-primary"></i><span>{{ trans('app.create') }}</span>
    </a>
@stop

@section('content')

    <div id="grid-currencies">
        @include('currencies._ajax_index')
    </div>

@stop