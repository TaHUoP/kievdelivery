<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('currencies.currencies') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-currencies"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>{{ trans('currencies.attr.code') }}</th>
                <th>{{ trans('currencies.attr.name') }}</th>
                <th>{{ trans('currencies.attr.default') }}</th>
                <th>{{ trans('currencies.attr.active') }}</th>
                <th class="text-center td-control">{{ trans('app.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($currencies as $currency)
                    <tr>
                        <td class="td-name">{{ $currency['code'] }}</td>
                        <td>{{ $currency['name'] }}</td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => ($currency['code'] == $defaultCurrency)]) }}</td>
                        <td class="td-boolean">{{ Widget::run('BooleanView', ['value' => $currency['active']]) }}</td>
                        <td class="td-control text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cog"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{ route('backend.currencies.edit', $currency['code']) }}"><i class="icon-pencil"></i> {{ trans('app.edit') }}</a></li>
                                        <li><a href="{{ route('backend.currencies.destroy', $currency['code']) }}" data-confirm="{{ trans('app.confirm') }}" data-method="delete">
                                            <i class="icon-close2"></i> {{ trans('app.delete') }}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">{{ trans('app.no-data') }}</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if (count($currencies))
                {{ trans('app.all records :total', ['total' => count($currencies)])  }}
            @endif
        </div>
    </div>
</div>