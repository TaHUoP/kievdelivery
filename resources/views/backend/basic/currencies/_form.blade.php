<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('currencies.currency') }}</h5>
    </div>
    <div class="panel-body">
        <div class="col-sm-2">
            <div class="form-group required {{ $errors->has('code') ? 'has-error' : '' }}">
                {{ Form::label('code', trans('currencies.attr.code'), ['class' => 'control-label']) }}
                {{ Form::text('code', isset($currency['code']) ? $currency['code'] : null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('code') ? $errors->first('code') : '' }}</div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', trans('currencies.attr.name'), ['class' => 'control-label']) }}
                {{ Form::text('name', isset($currency['name']) ? $currency['name'] : null, ['class' => 'form-control']) }}
                <div class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group required {{ $errors->has('exchange_rate') ? 'has-error' : '' }}">
                {{ Form::label('exchange_rate', trans('currencies.attr.exchange_rate'), ['class' => 'control-label']) }}
                {{ Form::number('exchange_rate', isset($currency['exchange_rate']) ? $currency['exchange_rate'] : null, ['class' => 'form-control', 'step'=>'any']) }}
                <div class="help-block">{{ $errors->has('exchange_rate') ? $errors->first('exchange_rate') : '' }}</div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group checkbox checkbox-switchery {{ $errors->has('active') ? 'has-error' : '' }}">
                <label>
                    {{ Form::checkbox('active', '1', !empty($currency['active']) ? $currency['active'] : null, [
                        'class' => 'switchery',
                    ]) }}
                    {{ trans('currencies.attr.active') }}
                    <div class="help-block">{{ $errors->has('active') ? $errors->first('active') : '' }}</div>
                </label>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="heading-elements">
            <button type="submit" class="btn btn-success heading-btn pull-left legitRipple">{{ trans('app.save') }}</button>
        </div>
    </div>
</div>