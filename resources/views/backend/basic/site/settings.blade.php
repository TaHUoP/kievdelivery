@extends('layouts.main')

@section('title', trans('app.settings'))

@section('page-title')
    <h4>{{ trans('app.settings') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.site.settings').'">'.trans('app.settings').'</a>'
    ]) }}
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">


            {{-- ========================== --}}
            <ul class="nav nav-tabs">
                <li class="active"><a href="#param-section-0" data-toggle="tab">system</a></li>
                <li><a href="#param-section-1" data-toggle="tab">users</a></li>
            </ul>
            {{-- ========================== --}}



            {{ Form::open(['route' => ['backend.site.settings'], 'method' => 'put']) }}
                <div class="tab-content">


                    {{-- ========================== --}}
                    <div class="tab-pane fade in  active" id="param-section-0">
                        <div class="form-group">
                            <label class="display-block text-bold">Права доступа для входа в панель управления</label>
                            <span>Список прав доступа, которые позволяют авторизоваться в панель управления.</span>
                            <input type="text" class="form-control" name="params[accessRolesToControlpanel]" value="system-backend-login" disabled="disabled">
                        </div>
                    </div>
                    {{-- ========================== --}}


                    {{-- ========================== --}}
                    <div class="tab-pane fade in" id="param-section-1">
                        <div class="form-group">
                            <label class="display-block text-bold">Назвиние для изображения "Нет аватара"</label>
                            <span>Будет отображаться, если у пользователя нет аватара</span>
                            <input type="text" class="form-control" name="params[userAvatarDefaultImage]" value="noavatar.png">
                        </div>
                        <div class="form-group">
                            <label class="display-block text-bold">Url адрес к папке, где хранятся аватары по умолчанию</label>
                            <span>Например: /statics/images/</span>
                            <input type="text" class="form-control" name="params[userAvatarDefaultPath]" value="/statics/images/">
                        </div>
                        <div class="form-group">
                            <label class="display-block text-bold">Путь к папке с аватарами на сервере</label>
                            <span>Например: @statics/uploads/avatars/</span>
                            <input type="text" class="form-control" name="params[userAvatarPath]" value="@statics/uploads/avatars/">
                        </div>
                        <div class="form-group">
                            <label class="display-block text-bold">Url адрес к папке с аватарами на сервере</label>
                            <span>Например: /statics/uploads/avatars/</span>
                            <input type="text" class="form-control" name="params[userAvatarUrl]" value="/statics/uploads/avatars/">
                        </div>
                    </div>
                    {{-- ========================== --}}



                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">{{ trans('app.save') }}</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop