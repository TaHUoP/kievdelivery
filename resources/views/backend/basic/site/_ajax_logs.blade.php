<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title">{{ trans('app.logs') }}</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="reload" data-url="{{ URL::full() }}" data-selector="#grid-logs"></a></li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="td-number">{{ trans('app.attr.level') }}</th>
                <th class="td-name">{{ trans('app.attr.type') }}</th>
                <th class="td-date">{{ trans('app.attr.date') }}</th>
                <th>{{ trans('app.attr.message') }}</th>
            </tr>
            </thead>
            <tbody>
            @if ($logs->count())
                @foreach ($logs as $log)
                    <tr>
                        <td class="td-number">{{ $log->level }}</td>
                        <td class="td-name">{{ $log->level_name }}</td>
                        <td class="td-date">{{ $log->created_at }}</td>
                        <td>
                            <pre>{{ $log->message }}</pre>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4">{{ trans('app.no-data') }}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="datatable-footer">
        <div class="dataTables_info">
            @if ($logs->count())
                {{ trans('app.all records :total', ['total' => $logs->total()])  }}
            @endif
        </div>
        @if ($logs->count())
            @include('_pagination', ['paginator' => $logs])
        @endif
    </div>
</div>