@extends('layouts.main')

@section('title', trans('app.control-panel'))

@section('content')

    <div class="row">
        <div id="grid-checkouts-count">
            @include('shop.checkouts._ajax_statistic_count')
        </div>
        <div id="grid-products-count">
            @include('shop.products._ajax_statistic_count')
        </div>
    </div>
    <div id="grid-checkouts">
        @include('shop.checkouts._ajax_statistic')
    </div>

@stop