@extends('layouts.main')

@section('title', trans('app.logs'))

@section('page-title')
    <h4>{{ trans('app.logs') }}</h4>
@stop

@section('breadcrumbs')
    {{ Widget::run('Breadcrumbs', [
        '<a href="'.route('backend.site.logs').'">'.trans('app.logs').'</a>'
    ]) }}
@stop

@section('content')

    <div id="grid-logs">
        @include('site._ajax_logs')
    </div>

@stop