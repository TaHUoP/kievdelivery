@extends('emails._layout')

@section('content')


    <p style="margin: 0 0 30px;text-align: center;font-size: 20px;color: #455a64;">
        {{ trans('email.buy.now') }}
    </p>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">
                <p><strong>{{ trans('app.attr.name') }}</strong>: {{ $request['name'] }}</p>
                <p><strong>{{ trans('app.attr.email') }}</strong>: {{ $request['email'] }}</p>
                <p><strong>{{ trans('shop.product') }}:</strong>: {{ Translate::get($product->translations, 'name') }}
                </p>

                <p><strong>{{ trans('email.from.user') }}</strong>: </p>
                @if ($user)
                    <p>{{ $user->profile->name }} {{ $user->profile->surname }}</p>
                    <p>ID: {{ $user->id }}, Email: {{ $user->email }}</p>
                @else
                    <p>Неавторизированный пользователь</p>
                @endif
            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>
@endsection