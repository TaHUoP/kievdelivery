@extends('emails._layout')

@section('content')
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">
                <p>{{ trans('email.user.registered') }}</p>
                <p>
                    <strong>{{ trans('app.attr.name') }}</strong>: {{ $user->profile->name }} {{ $user->profile->surname }} <br>
                    <strong>{{ trans('app.attr.email') }}</strong>: {{ $user->email }} <br>
                    <strong>{{ trans('app.attr.phone') }}</strong>: {{ $user->phone }}
                </p>

                <br>

                <p style="text-align: center">
                    <strong><a href="{{ route('backend.user.users.edit', $user) }}">Управление пользователем</a></strong>
                    <br>
                    <a href="{{ route('backend.user.users.index') }}">Все пользователи</a>
                    <br>
                    <a href="{{ route('backend.site.index') }}">Панель управления</a>
                </p>

            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>
@endsection