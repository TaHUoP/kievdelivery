@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">
                <p>
                    {{ trans('email.you.registered', ['appname' => config('app.name')]) }}
                </p>

                <p>
                    {{ trans('email.confirm.registration') }}
                </p>

                <p>
                    {{ route('email_confirm', ['code' => $user->confirmation_code]) }}
                </p>

                <p>
                    {{ trans('email.confirm.registration-alternate', ['address' => route('email_confirmation_form')]) }}
                </p>

                <p>
                    {{ trans('email.registration-code', ['code' => $user->confirmation_code]) }}
                </p>
            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>

@endsection