<!doctype html>
<html>
<head><meta charset="utf-8"><title></title></head>
<body style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color:#ECEFF1; margin:0; padding:0; color:#263238;">
<table width="100%" bgcolor="#ECEFF1" cellpadding="0" cellspacing="0" border="0">
<tbody>
    <tr>
        <td style="padding:30px 0;">
            <table cellpadding="0" cellspacing="0" width="auto" border="0" align="center">
                <tbody>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 8px;overflow: hidden;">
                            <tbody>
                            <tr>
                                <td colspan="3" rowspan="3" bgcolor="#FFFFFF" style="padding:30px;">
                                    <a href="{{ Config::get('app.url') }}" style="display: block;margin: 0 auto 15px auto;text-align: center;font-size: 25px;text-decoration: none;color: #263238;">
                                        <img src="{{ Config::get('app.url') }}{{ Statics::asset('logo.png', true) }}" alt="" style="border:0; margin:0;width: auto;">
                                    </a>
                                    @yield('content')
                                    <p style="border-top: 1px solid #eceff1; line-height: 0px; margin: 30px 0;">&nbsp;</p>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr valign="top">
                                            <td>
                                                <p style="margin:0; color:#263238; font-size: 16px">
                                                    {{ trans('site.best_regards') }},
                                                    <a href="{{ Config::get('url') }}" style="color:#455a64; text-decoration:none; font-weight:bold;">KievDelivery.com</a>
                                                </p>
                                                @if (App::getLocale() == 'ru')
                                                    <ul>
                                                        <li>contact@kievdelivery.com</li>
                                                        <li>Тел. +1 (980) 7295075</li>
                                                        <li>Тел. +38 (067) 7088480</li>
                                                        <li>Skype: kievdelivery</li>
                                                    </ul>
                                                @else
                                                    <ul>
                                                        <li>contact@kievdelivery.com</li>
                                                        <li>Phone: +1 (980) 7295075</li>
                                                        <li>Phone: +38 (067) 7088480</li>
                                                        <li>Skype: kievdelivery</li>
                                                    </ul>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</tbody>
</table>
</body>
</html>