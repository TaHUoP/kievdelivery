
@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">

                <h3 style="font-size: 25px; text-align: center">Связатся с нами</h3>

                <p>
                    <b>Пользователь:</b>  <br>
                    @if ($user)
                        <a href="{{ route('backend.user.users.edit', $user) }}">{{ $user->profile->name }} {{ $user->profile->surname }}</a>
                    @else
                        {{ !empty($request['name']) ? $request['name'] : 'Нет данных' }}
                    @endif
                </p>

                <p>
                    <b>Email</b>:  <br>
                    {{ !empty($request['email']) ? $request['email'] : 'Нет данных' }}
                    <br><br>

                    <b>Сообщение</b>: <br>
                    {{ !empty($request['message']) ? nl2br($request['message']) : 'Нет данных' }}
                    <br><br>
                </p>

            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>

@endsection