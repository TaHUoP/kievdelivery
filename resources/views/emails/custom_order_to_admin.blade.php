@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 500px; width: 500px; max-width:500px;">

                <h3 style="font-size: 25px; text-align: center">Особый заказ</h3>

                <p>
                    <b>Пользователь:</b>  <br>
                    @if ($user)
                        <a href="{{ route('backend.user.users.edit', $user) }}">{{ $user->profile->name }} {{ $user->profile->surname }}</a>
                    @else
                        {{ !empty($request['name']) ? $request['name'] : 'Нет данных' }}
                    @endif
                </p>

                <p>
                    <b>Email</b>:  <br>
                    {{ !empty($request['email']) ? $request['email'] : 'Нет данных' }}
                    <br><br>

                    <b>Описание заказа</b>: <br>
                    {{ !empty($request['contact_info']) ? nl2br($request['contact_info']) : 'Нет данных' }}
                    <br><br>

                    <b>Адрес и контакты доставки</b>: <br>
                    {{ !empty($request['address']) ? nl2br($request['address']) : 'Нет данных' }}
                    <br><br>

                    <b>Дополнительные инструкции</b>: <br>
                    {{ !empty($request['instruction']) ? nl2br($request['instruction']) : 'Нет данных' }}
                    <br><br>

                    <b>Сообщение для получателя</b>: <br>
                    {{ !empty($request['comment']) ? nl2br($request['comment']) : 'Нет данных' }}
                    <br><br>
                </p>

            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>

@endsection