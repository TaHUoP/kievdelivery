@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">

                <p style="text-align: justify">
                    {{ trans('email.signup_welcome', ['name' => $user->profile->name . ' ' . $user->profile->surname]) }}
                </p>

                <p style="text-align: justify">
                    <strong>{{ trans('email.signup_important') }}</strong>
                    <br>{{ route('email_confirm', ['code' => $user->confirmation_code]) }}
                </p>

                <p style="text-align: justify">
                    {{ trans('email.signup_enter_a_code') }}
                    <br>{{ route('email_confirmation_form') }}<br>
                    <strong>Activation code:</strong> {{ $user->confirmation_code }}
                </p>

                <p style="text-align: justify">
                    {{ trans('email.signup_security') }}
                </p>

                <p style="text-align: justify">
                    {!! trans('email.signup_contact', ['contact_url' => '<a href="'. route('pages.contacts') .'">Contact Us</a>']) !!}
                </p>

            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>
@endsection