@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">

                {!! trans('email.checkout_status_fail', [
                    'name' => '<strong>' . (!empty($checkout->cart->user) ? ($checkout->cart->user->name . ' ' . $checkout->cart->surname) : ($checkout->last_name . ' ' . $checkout->first_name)) . '</strong>',
                    'id' => '<strong>' . $checkout->id . '</strong>',
                    'status' => '<strong>' . trans('site.checkout_status_fail'). '</strong>',
                    'coupon' => '<strong>5OFF</strong>',
                ]) !!}

            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>

@endsection