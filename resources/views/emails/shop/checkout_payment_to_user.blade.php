@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">

                <p style="text-align: justify">
                    {!! trans('email.checkout_payment_paid_info', [
                        'id' => '<strong>' . $checkout->id . '</strong>',
                        'name' => '<strong>' . (!empty($checkout->cart->user) ? ($checkout->cart->user->name . ' ' . $checkout->cart->user->surname) : ($checkout->last_name . ' ' . $checkout->first_name)) . '</strong>',
                    ]) !!}
                </p>

                <br>

                <strong>{{ trans('site.details_of_the_order') }}</strong>
                <table>
                    <tr>
                        <td>{{ trans('site.order_id') }}:</td>
                        <td>#<?=$checkout->id?></td>
                    </tr>
                    <tr>
                        <td>{{ trans('site.order_date') }}:</td>
                        <td>{{ date('d.m.Y', strtotime($checkout->delivery->date)) }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('site.order_status') }}:</td>
                        <td>{{ trans('app.status.pending') }}</td>
                    </tr>
                </table>

                <p>
                    <strong style="font-size: 18px">{{ trans('shop.checkouts.list-of-products') }}</strong>
                </p>

                <table style="padding: 15px">
                    <tr>
                        <th style="text-align: left">&nbsp;</th>
                        <th style="text-align: left">{{ trans('app.attr.name_title') }}</th>
                        <th style="text-align: left">{{ trans('app.quantity') }}</th>
                    </tr>
                    @foreach($checkout->cart->products as $relation)
                        <tr>
                            <td valign="top" style="padding-right: 15px; border-bottom: dashed 1px silver">
                                <img src="{{ Config::get('app.url') }}{!! Shop::getImagePreview($relation->product->getDefaultImage(), true, true) !!}" alt="" style="width: 100px;">
                            </td>
                            <td valign="top" style="border-bottom: dashed 1px silver">
                                <br>
                                <a href="{{ route('shop.product', ['url' => $relation->product->alias]) }}">
                                    {{ Translate::get($relation->product->translations, 'name') }}
                                </a><br>
                                {{ trans('app.price') }}: {!! Shop::getSalePrice($relation->product) !!}
                            </td>
                            <td align="center" style="border-bottom: dashed 1px silver">{{ $relation->amount }}</td>
                        </tr>
                    @endforeach
                </table>

                <p>
                    <b>{{ trans('shop.delivery-price') }}:</b>
                    {{--@if($checkout->cart->total_price > Config::get('static.deliveryPriceBorder'))--}}
                        {{--<span style="color: green">{{ trans('app.free') }}</span>--}}
                    {{--@else--}}
                        {{ Currency::getMoneyFormat($checkout->delivery->delivery_price, Currency::getUserCurrency(), 2) }}
                    {{--@endif--}}

                    <br>

                    <b>{{ trans('site.your-discount') }}:</b>
                    <span style="color: green">
                        {{ Currency::getMoneyFormat($checkout->cart->discount, Currency::getUserCurrency(), 2) }}
                    </span>
                </p>

                <p style="font-size: 18px; margin-top: 25px;">
                    <b>{{ trans('shop.checkouts.sum-of-order') }}:</b>
                    {{--@if($checkout->cart->total_price > Config::get('static.deliveryPriceBorder'))--}}
                        {{--{{ Currency::getMoneyFormat($checkout->cart->total_price - $checkout->cart->discount, Currency::getUserCurrency(), 2) }}--}}
                    {{--@else--}}
                        {{ Currency::getMoneyFormat($checkout->total, Currency::getUserCurrency(), 2) }}
                    {{--@endif--}}
                </p>


                <hr>


                <strong>{{ trans('site.information_about_the_payer') }}</strong>
                <table>
                    <tr>
                        <td>{{ trans('app.attr.name') }}:</td>
                        <td>{{ $checkout->first_name }} {{ $checkout->last_name }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('app.attr.email') }}:</td>
                        <td>{{ $checkout->email }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('app.attr.phone') }}:</td>
                        <td>{{ $checkout->phone }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('site.order_address_from') }}:</td>
                        <td>{{ $checkout->country }}, {{ $checkout->address }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('site.ip_address') }}:</td>
                        <td>{{ $ip_address }}</td>
                    </tr>
                </table>

                <br>

                <strong>{{ trans('site.delivery_address') }}:</strong>
                <table>
                    <tr>
                        <td>{{ trans('app.attr.name') }}:</td>
                        <td>{{ $checkout->delivery->first_name }} {{ $checkout->delivery->last_name }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('app.attr.phone') }}:</td>
                        <td>{{ $checkout->delivery->phone }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('site.order_address_to') }}:</td>
                        <td>{{ $checkout->delivery->city }}, {{ $checkout->delivery->address }}</td>
                    </tr>
                </table>

                <br>

                <strong>{{ trans('site.text_postcard') }}:</strong>
                <p style="margin-top: 0">
                    {{ !empty($checkout->delivery->comment_recipient) ? $checkout->delivery->comment_recipient : trans('app.no-data') }}
                </p>


                <strong>{{ trans('site.instructions_for_kiev_delivery') }}:</strong>
                <p style="margin-top: 0">
                    {{ !empty($checkout->delivery->comment_note) ? $checkout->delivery->comment_note : trans('app.no-data') }}
                </p>

                <p><br></p>

                <p style="text-align: justify">{!! trans('email.checkout_payment_questions') !!}</p>

                <p style="text-align: justify">
                    {!! trans('email.checkout_payment_guarantees', [
                        'name' => '<strong>' . (!empty($checkout->cart->user) ? ($checkout->cart->user->name . ' ' . $checkout->cart->surname) : ($checkout->last_name . ' ' . $checkout->first_name)) . '</strong>',
                    ]) !!}
                </p>

            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>

@endsection