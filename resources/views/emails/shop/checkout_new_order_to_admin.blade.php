@extends('emails._layout')

@section('content')

    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 10px;overflow: hidden;margin: 0; font-size: 15px;">
        <tbody>
        <tr valign="top">
            <td style="min-width: 250px; max-width:500px;">
                <p>
                    Новый заказ <strong>#{{ $checkout->id }}</strong> от пользователя
                    <strong>{{ (!empty($checkout->cart->user) ? ($checkout->cart->user->name . ' ' . $checkout->cart->surname) : ($checkout->last_name . ' ' . $checkout->first_name)) }}</strong>
                </p>


                <hr>

                <table style="padding: 15px">
                    <tr>
                        <th style="text-align: left">&nbsp;</th>
                        <th style="text-align: left">{{ trans('app.attr.name_title') }}</th>
                        <th style="text-align: left">{{ trans('app.quantity') }}</th>
                    </tr>
                    @foreach($checkout->cart->products as $relation)
                        <tr>
                            <td valign="top" style="padding-right: 15px; border-bottom: dashed 1px silver">
                                <img src="{{ Config::get('app.url') }}{!! Shop::getImagePreview(Shop::getDefaultImage($relation->product), true, true) !!}" alt="" style="width: 100px;">
                            </td>
                            <td valign="top" style="border-bottom: dashed 1px silver">
                                <br>
                                <a href="{{ route('shop.product', ['url' => $relation->product->alias]) }}">
                                    {{ Translate::get($relation->product->translations, 'name') }}
                                </a> <br>
                                {{ trans('app.price') }}: {!! Shop::getSalePrice($relation->product) !!}
                            </td>
                            <td align="center" style="border-bottom: dashed 1px silver">{{ $relation->amount }}</td>
                        </tr>
                    @endforeach
                </table>

                <p>
                    <b>{{ trans('shop.delivery-price') }}:</b>
                    {{--@if($checkout->cart->total_price > Config::get('static.deliveryPriceBorder'))--}}
                        {{--<span style="color: green">{{ trans('app.free') }}</span>--}}
                    {{--@else--}}
                        {{ Currency::getMoneyFormat($checkout->delivery->delivery_price, Currency::getUserCurrency(), 2) }}
                    {{--@endif--}}

                    <br>

                    <b>{{ trans('site.your-discount') }}:</b>
                    <span style="color: green">
                        {{ Currency::getMoneyFormat($checkout->cart->discount, Currency::getUserCurrency(), 2) }}
                    </span>
                </p>

                <p style="font-size: 18px; margin-top: 25px;">
                    <b>{{ trans('shop.checkouts.sum-of-order') }}:</b>
                    {{--@if($checkout->cart->total_price > Config::get('static.deliveryPriceBorder'))--}}
                        {{--{{ Currency::getMoneyFormat($checkout->cart->total_price - $checkout->cart->discount, Currency::getUserCurrency(), 2) }}--}}
                    {{--@else--}}
                        {{ Currency::getMoneyFormat($checkout->total_price, Currency::getUserCurrency(), 2) }}
                    {{--@endif--}}
                </p>

                <hr>

                <br>
                <p style="text-align: center">
                    <strong><a href="{{ route('backend.shop.checkouts.edit', $checkout) }}">Управление заказом #{{ $checkout->id }}</a></strong>
                    <br>
                    <a href="{{ route('backend.shop.checkouts.index') }}">Все заказы</a>
                    <br>
                    <a href="{{ route('backend.site.index') }}">Панель управления</a>
                </p>
            </td>
            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
        </tr>
        </tbody>
    </table>

@endsection