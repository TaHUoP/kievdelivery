<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToCheckoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_systems', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_active')->default(0);
        });

        Schema::create('product_payment_system', function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('payment_system_id')->unsigned();
        });

        $paymentSystems = [
            [
                'name' => 'paypal',
                'is_active' => true
            ],
            [
                'name' => 'platon',
                'is_active' => true
            ]
        ];

        \App\Models\Shop\PaymentSystem::insert($paymentSystems);

        foreach (\App\Models\Shop\Product::all() as $product) {
            $product->paymentSystems()->sync([1, 2]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_systems');
        Schema::dropIfExists('product_payment_system');
    }
}
