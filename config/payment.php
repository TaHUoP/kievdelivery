<?php

return [
    'methods' => [

        'platon' => [

            'form_url' => 'https://secure.platononline.com/payment/auth',

            'api_url' => 'https://secure.platononline.com/post/',

            'password' => env('PLATON_PASSWORD'),

            'secret' => env('PLATON_SECRET'),
        ],

    ],

];