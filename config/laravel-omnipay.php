<?php

return [

	// The default gateway to use
	'default' => 'paypal',

	// Add in each gateway here
	'gateways' => [
        /*'paypal' => [
            'driver'  => 'PayPal_Express',
            'options' => [
                'username'  => env('OMNIPAY_PAYPAL_EXPRESS_USERNAME'),
                'password'  => env('OMNIPAY_PAYPAL_EXPRESS_PASSWORD'),
                'signature' => env('OMNIPAY_PAYPAL_EXPRESS_SIGNATURE'),
                'solutionType' => env('OMNIPAY_PAYPAL_EXPRESS_SOLUTION_TYPE'),
                'landingPage'    => env('OMNIPAY_PAYPAL_EXPRESS_LANDING_PAGE'),
                'headerImageUrl' => env('OMNIPAY_PAYPAL_EXPRESS_HEADER_IMAGE_URL'),
                'testMode' => true,
                'currency' => env('CURRENCY')
            ],
        ],*/
        'paypal' => [
            'driver'  => 'PayPal_Rest',
            'options' => [
                'clientId' => env('OMNIPAY_PAYPAL_REST_CLIENT_ID'),
                'secret' => env('OMNIPAY_PAYPAL_REST_SECRET'),
                'testMode' => env('OMNIPAY_PAYPAL_TEST_MODE', true),
                'currency' => env('CURRENCY')
            ],
        ],
        'way_for_pay' => [
            'options' => [
                'merchant_account' => env('MERCHANT_ACCOUNT'),
                'merchant_key' => env('MERCHANT_KEY'),
                'merchant_domain_name' => env('MERCHANT_DOMAIN_NAME'),
                'currency' => env('CURRENCY')
            ]
        ],
	]

];