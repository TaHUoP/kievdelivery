// @TODO: Отправлять данные через ajax. Например удаление
// @TODO: Возможность выбрать все чекбоксы
// @TODO: Возможность конфигурировать (например заменить плагин PNotify)

/**
 * cPanelApi - набор хелперов и обработчиков для облегчения работы
 *
 * ПРИМЕРЫ:
 *
 * // Работа с Csrf токеном
 * cPanelApi.csrf().getParam();
 * cPanelApi.csrf().getToken();
 * cPanelApi.csrf().setToken('value');
 * cPanelApi.csrf().refreshToken();
 *
 * // Уведомления
 * cPanelApi.notify().success('Message success');
 * cPanelApi.notify().danger('Message danger');
 * cPanelApi.notify().warning('Message warning');
 *
 * // Загрузка
 * cPanelApi.loading().start('#grid');
 * cPanelApi.loading().stop('#grid');
 *
 * // Работа с URL
 * cPanelApi.url().getQueryParams('http://site.ru/test?param1=1&param2=2');
 * cPanelApi.url().set('/backend?param1=1&param2=2');
 *
 * Работа с GRID
 * cPanelApi.grid().refresh('#grid', '/test');
 *
 * // Генерация guid (статистически уникальный 128-битный идентификатор)
 * cPanelApi.guid();
 *
 * // Работа с формами
 * cPanelApi.form().clearErrors($form)
 * cPanelApi.form().renderErrors($form, errors)
 * cPanelApi.form().getElementName(name)
 *
 * // Инициализация плагина
 * cPanelApi.initPlugin('my-plugin', function() {
 *     console.log('Init My Plugin');
 * }, true);
 * cPanelApi.initPlugin('my-plugin'); // Рефреш
 *
 * // Подтверждение запроса
 * <button type="button" data-confirm="Confirm..." data-method="POST"></button>
 */
var cPanelApi = (function ($) {

    var _params = {

        reloadableScripts: [],

        plugins: {},

        notify: {
            delay: 3000,
            hide: true,
            confirm: {
                prompt: false
            },
            buttons: {
                closer: true,
                sticker: false
            }
        }

    };

    var pub = {

        // Подтверждение
        confirm: function (message, ok, cancel) {
            if (confirm(message)) {
                !ok || ok();
            } else {
                !cancel || cancel();
            }
        },

        // Csrf
        csrf: function () {
            return {
                // Имя Csrf токена
                getParam: function () {
                    return '_token';
                },

                // Значения Csrf токена
                getToken: function () {
                    return $('meta[name="' + pub.csrf().getParam() + '"]').attr('content');
                },

                // Установить новый Csrf токен
                setToken: function (value) {
                    $('meta[name="' + pub.csrf().getParam() + '"]').attr('content', value);
                },

                // Обновить Csrf Token
                refreshToken: function () {
                    var token = pub.csrf().getToken();
                    if (token) {
                        $('form input[name="' + pub.csrf().getParam() + '"]').val(token);
                    }
                }
            }
        },

        // Уведомления
        notify: function () {
            return {
                success: function (message) {
                    _params.notify['icon'] = 'icon-checkmark4';
                    _params.notify['addclass'] = 'bg-success';
                    _params.notify['type'] = 'success';
                    _params.notify['text'] = message;
                    new PNotify(_params.notify);
                },
                danger: function (message) {
                    _params.notify['icon'] = 'icon-cancel-circle2';
                    _params.notify['addclass'] = 'bg-danger';
                    _params.notify['type'] = 'danger';
                    _params.notify['text'] = message;
                    new PNotify(_params.notify);
                },
                warning: function (message) {
                    _params.notify['addclass'] = 'bg-warning';
                    _params.notify['icon'] = 'icon-spam';
                    _params.notify['type'] = 'warning';
                    _params.notify['text'] = message;
                    new PNotify(_params.notify);
                }
            }
        },

        // Загрузка
        loading: function () {
            return {
                start: function (selector) {
                    $(selector).block({
                        message: '<i class="icon-spinner2 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait',
                            'box-shadow': '0 0 0 1px #ddd'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                stop: function (selector) {
                    $(selector).unblock();
                }
            }
        },

        // Работа с Url
        url: function () {
            return {
                // Формирует объект GET параметров из URL строки
                getQueryParams: function (url) {
                    var pos = url.indexOf('?');
                    if (pos < 0) {
                        return {};
                    }

                    var pairs = url.substring(pos + 1).split('&'),
                        params = {},
                        pair,
                        i;

                    for (i = 0; i < pairs.length; i++) {
                        pair = pairs[i].split('=');
                        var name = decodeURIComponent(pair[0]);
                        var value = decodeURIComponent(pair[1]);
                        if (name.length) {
                            if (params[name] !== undefined) {
                                if (!$.isArray(params[name])) {
                                    params[name] = [params[name]];
                                }
                                params[name].push(value || '');
                            } else {
                                params[name] = value || '';
                            }
                        }
                    }

                    return params;
                },

                // Установить новый URL
                set: function (url, title) {
                    title = title || null;
                    if (title) {
                        document.title = title;
                    }
                    window.history.pushState(null, title, url);
                }
            }
        },

        // Работа с GRID
        grid: function () {
            return {
                refresh: function (selector, url) {
                    $(selector).block({
                        message: '<i class="icon-spinner2 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait',
                            'box-shadow': '0 0 0 1px #ddd'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                    $.get(url, function (response) {
                        $(selector).html(response);
                    }).done(function () {
                        $(selector).unblock();
                    });
                }
            }
        },

        // GUID (Globally Unique Identifier)
        guid: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        },

        // Работа с формами
        form: function () {
            return {
                // Очистка ошибок
                clearErrors: function ($form) {
                    var $elements = $form.find('.form-group');
                    $elements.each(function () {
                        var $this = $(this);
                        $this.removeClass('has-error');
                        $this.find('.help-block').text('').removeClass('validation-error-label');
                    });
                },

                // Рендер ошибок
                renderErrors: function ($form, errors) {
                    for (var error in errors) {

                        // Получение имени атрибута
                        var elementName = pub.form().getElementName(error);

                        // Подключение необходимых классов
                        var $element = $form.find("[name='" + elementName + "']");

                        var $elementGroup = $element.parents('.form-group');
                        var $elementError = $elementGroup.find('.help-block');
                        var errorMessage = errors[error][0];

                        $elementGroup.addClass('has-error');
                        $elementError.addClass('validation-error-label');
                        $elementError.text(errorMessage);
                    }
                },

                // Получение имени элемента
                getElementName: function (name) {
                    var elementName = name;
                    if (name.indexOf('.') + 1) {
                        var nameSegments = name.split('.');
                        elementName = nameSegments.shift();
                        for (var i in nameSegments) {
                            elementName += '[' + nameSegments[i] + ']';
                        }
                    }
                    return elementName;
                }
            }
        },

        // Имитация метода
        handleAction: function ($e, event) {
            var method = $e.data('method'),
                $form = $e.closest('form'),
                action = $e.attr('href'),
                params = $e.data('params');

            if (method) {
                if (!action || !action.match(/(^\/|:\/\/)/)) {
                    action = window.location.href;
                }
                $form = $('<form/>', {method: 'POST', action: action});
                var target = $e.attr('target');
                if (target) {
                    $form.attr('target', target);
                }
                if (!method.match(/(get|post)/i)) {
                    $form.append($('<input/>', {name: '_method', value: method, type: 'hidden'}));
                    method = 'POST';
                }
                if (!method.match(/(get|head|options)/i)) {
                    var csrfParam = pub.csrf().getParam();
                    if (csrfParam) {
                        $form.append($('<input/>', {
                            name: csrfParam,
                            value: pub.csrf().getToken(),
                            type: 'hidden'
                        }));
                    }
                }
                $form.hide().appendTo('body');

                if (params && $.isPlainObject(params)) {
                    $.each(params, function (idx, obj) {
                        $form.append($('<input/>').attr({name: idx, value: obj, type: 'hidden'}));
                    });
                }

                $form.trigger('submit');
            }
        },

        // Инициализация плагина
        // Первая инициализация плагина initPlugin('name', function() { ... }, true)
        // Где третий аргумент true, запускает функцию инициализации сразу
        // Вызов функции только с именем  initPlugin('name') сделает рефреш
        initPlugin: function (name, initCallback, initStart) {
            initStart = initStart || false;
            if (typeof initCallback === 'function') {
                if (_params.plugins.hasOwnProperty(name)) {
                    throw new SyntaxError('Plugin "' + name + '" is already initialized');
                }
                _params.plugins[name] = initCallback;
                if (initStart) {
                    _params.plugins[name]();
                }
            } else {
                var initPlugin = function (name) {
                    if (_params.plugins.hasOwnProperty(name)) {
                        _params.plugins[name]();
                    } else {
                        throw new SyntaxError('Plugin "' + name + '" is not initialized');
                    }
                };
                if (Array.isArray(name)) {
                    for (var i in name) {
                        initPlugin(name[i]);
                    }
                } else {
                    initPlugin(name);
                }
            }
        },

        // Инициализация модуля
        initModule: function (module) {
            if (module.isActive === undefined || module.isActive) {
                if ($.isFunction(module.init)) {
                    module.init();
                }
                $.each(module, function () {
                    if ($.isPlainObject(this)) {
                        pub.initModule(this);
                    }
                });
            }
        },

        // Общая инициализация
        init: function () {
            initCsrfHandler();
            initRedirectHandler();
            initScriptFilter();
            initDataMethods();
            initGridHandlers();
        }
    };

    // Обработчик AJAX редиректа
    function initRedirectHandler() {
        $(document).ajaxComplete(function (event, xhr, settings) {
            var url = xhr.getResponseHeader('X-Redirect');
            if (url) {
                window.location = url;
            }
        });
    }

    // Автоматически подставляет Csrf токен в запрос
    function initCsrfHandler() {
        $.ajaxPrefilter(function (options, originalOptions, xhr) {
            if (!options.crossDomain && pub.csrf().getParam()) {
                xhr.setRequestHeader('X-CSRF-Token', pub.csrf().getToken());
            }
        });
        pub.csrf().refreshToken();
    }

    // Обработчик события "подтверждение"
    // <button type="button" data-confirm="Confirm..." data-method="POST"></button>
    function initDataMethods() {
        var handler = function (event) {
            var $this = $(this),
                method = $this.data('method'),
                message = $this.data('confirm');
            if (method) {
                if (method === undefined && message === undefined) {
                    return true;
                }
                if (message !== undefined) {
                    $.proxy(pub.confirm, this)(message, function () {
                        pub.handleAction($this, event);
                    });
                } else {
                    pub.handleAction($this, event);
                }
                event.stopImmediatePropagation();
                return false;
            } else {
                $.proxy(pub.confirm, this)(message, function () {
                    pub.handleAction($this, event);
                }, function () {
                    event.stopImmediatePropagation();
                    return false;
                });
            }
        };

        $(document).on('click', '[data-confirm]', handler);
    }

    // Фильтр повторной загрузки скриптов
    function initScriptFilter() {
        var hostInfo = location.protocol + '//' + location.host;

        var loadedScripts = $('script[src]').map(function () {
            return this.src.charAt(0) === '/' ? hostInfo + this.src : this.src;
        }).toArray();

        $.ajaxPrefilter('script', function (options, originalOptions, xhr) {
            if (options.dataType == 'jsonp') {
                return;
            }

            var url = options.url.charAt(0) === '/' ? hostInfo + options.url : options.url;
            if ($.inArray(url, loadedScripts) === -1) {
                loadedScripts.push(url);
            } else {
                var isReloadable = $.inArray(url, $.map(_params.reloadableScripts, function (script) {
                        return script.charAt(0) === '/' ? hostInfo + script : script;
                    })) !== -1;
                if (!isReloadable) {
                    xhr.abort();
                }
            }
        });

        $(document).ajaxComplete(function (event, xhr, settings) {
            var styleSheets = [];
            $('link[rel=stylesheet]').each(function () {
                if ($.inArray(this.href, _params.reloadableScripts) !== -1) {
                    return;
                }
                if ($.inArray(this.href, styleSheets) == -1) {
                    styleSheets.push(this.href)
                } else {
                    $(this).remove();
                }
            })
        });
    }

    // Обработчик события для обновления grid блока
    function initGridHandlers() {
        var handlerReload = function (event) {
            var $this = $(this);
            if ($this.data('selector') && $this.data('url')) {
                pub.grid().refresh($this.data('selector'), $this.data('url'));
            }
            return false;
        };

        $(document).on('click', '[data-action="reload"]', handlerReload);
    }

    return pub;

})(jQuery);


$(function () {
    cPanelApi.initModule(cPanelApi);
});