$(function() {


    // Init Fancytree
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('fancytree', function() {

        $(".tree-default").fancytree();
        $("#categories-tree").fancytree();
        $("#categories-tree ul.ui-fancytree").attr('id', 'sort');

    }, true);


    // Init Select2
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('select2', function() {

        // Select with search
        $('.select-search').select2();

        // Initialize with tags
        $(".select-multiple-drag").select2({
            containerCssClass: 'sortable-target'
        });

        $('.select').select2({
            minimumResultsForSearch: Infinity
        });

        $(".select-ajax").select2({
            containerCssClass: 'border-bottom-ddd',
            ajax: {
                url: function () {
                    return $(this).data('url');
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term // search term
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: function (item) {
                if (item.loading) {
                    return item.text;
                }
                return "<div class='select2-result-repository__statistics'>" + item.text + "</div>";
            },
            templateSelection: function (item) {
                return item.text;
            }
        });
    }, true);


    // Init Switchery
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('switchery', function() {

        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');
            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

    }, true);


    // Init Summernote
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('summernote', function() {

        function sendFile(url, file, editor) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    editor.summernote('insertImage', response);
                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });
        }

        // Edit
        $('#edit').on('click', function() {
            $('.click2edit').summernote({focus: true});
        });


        // Save
        $('#save').on('click', function() {
            var $target = $('.click2edit');
            var aHTML = $target.code();
            $target.destroy();
        });

        return function() {

            // Summernote
            $('.summernote').each(function () {
                var $textArea = $(this);
                var test = $textArea.summernote({
                    //lang: 'ru-RU',
                    fontNames: ['Roboto', 'Arial', 'Verdana', 'Georgia', 'Impact', 'Courier New', 'Times New Roman'],
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                        ['fontsize', ['fontsize', 'fontname', 'height']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['picture', ['link', 'picture', 'video']],
                        ['codeview', ['codeview', 'fullscreen']]
                    ],
                    callbacks: {
                        onChange: function(contents) {
                            $textArea.val(contents);
                            $textArea.change();
                        },
                        onImageUpload: function(files) {
                            sendFile($textArea.data('upload'), files[0], $(this));
                        }
                    }
                });
            });

        }();
    }, true);


    // Init Summernote min
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('summernote-min', function() {

        // Edit
        $('#edit').on('click', function() {
            $('.click2edit').summernote({focus: true});
        });

        // Save
        $('#save').on('click', function() {
            var $target = $('.click2edit');
            var aHTML = $target.code();
            $target.destroy();
        });

        return function() {

            // Summernote
            $('.summernote-min').each(function () {
                var $textArea = $(this);
                var test = $textArea.summernote({
                    //lang: 'ru-RU',
                    fontNames: ['Roboto', 'Arial', 'Verdana', 'Georgia', 'Impact', 'Courier New', 'Times New Roman'],
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                        ['codeview', ['codeview', 'fullscreen']]
                    ],
                    callbacks: {
                        onChange: function(contents) {
                            $textArea.val(contents);
                            $textArea.change();
                        },
                    }
                });
            });

        }();
    }, true);


    // Init uniform
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('uniform', function() {

        // Checkboxes, radios
        $(".styled").uniform({ radioClass: 'choice' });

        // File input
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-primary'
        });

        // Styled checkboxes/radios
        $(".link-dialog input[type=checkbox], .note-modal-form input[type=radio]").uniform({
            radioClass: 'choice'
        });

        // Styled file input
        $(".note-image-input").uniform({
            fileButtonClass: 'action btn bg-warning-400'
        });

    }, true);


    // Init datetime
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('datetime', function() {

        // Daterange picker
        $('.daterange-single').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD.MM.YYYY'
            }
        });

    }, true);


    // Init dropzone
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('dropzone', function() {

        // Defaults
        Dropzone.autoDiscover = false;

        $('#images-uploads').dropzone({
            maxFilesize: 100,
            paramName: "uploadfile",
            maxThumbnailFilesize: 99999,
            previewsContainer: '.dropzone',
            previewTemplate: $('.preview').html(),
            sending: function (file, xhr, formData) {
                formData.append(cPanelApi.csrf().getParam(), cPanelApi.csrf().getToken());
            },
            init: function () {
                this.on('completemultiple', function (file, response) {
                    $('.dropzone').sortable('enable');
                });
                this.on('success', function (file, response) {
                    var $file = $(file.previewElement);
                    $file.addClass('dz-doned');
                    setTimeout(function () {
                        $file.removeClass('dz-doned');
                    }, 4500);
                    $file.attr('id', response.id);

                    cPanelApi.notify().success(response.message);
                });
                this.on('error', function (file, response) {
                    var $file = $(file.previewElement);

                    $file.addClass('dz-failed');
                    setTimeout(function () {
                        $file.remove();
                    }, 4000);

                    cPanelApi.notify().danger(response.message);
                });
            }
        });


    }, true);


    // Init sortable
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('sortable', function() {

        $('#sort').sortable();

        $(".sortable-target .select2-selection__rendered").sortable({
            containment: '.sortable-target',
            items: '.select2-selection__choice:not(.select2-search--inline)'
        });

        // Сортировка изображений
        $('.sortable').sortable({
            containment: '#tab-images',
            update: function (event, ui) {
                var $this = $(this);
                var ids = $this.sortable('toArray', {
                    attribute: 'id'
                });
                $.ajax({
                    url: $this.data('url'),
                    type: 'POST',
                    data: {ids: ids},
                    success: function (response) {
                        cPanelApi.notify().success(response.message);
                    },
                    error: function (xhr, str) {
                        cPanelApi.notify().success(xhr.responseJSON.message);
                    }
                });
            }
        });

    }, true);


    // Init touchspin
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('touchspin', function() {

        // Vertical spinners
        $(".touchspin-vertical").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'icon-arrow-up22',
            verticaldownclass: 'icon-arrow-down22'
        });

    }, true);


    // Init tables
    // -----------------------------------------------------------------------------------------------------------------
    cPanelApi.initPlugin('tables', function() {

        $('.table tr:last-child').find('.dropdown').addClass('dropup');

        // Grab first letter and insert to the icon
        $(".table tr").each(function (i) {
            // Title
            var $title = $(this).find('.letter-icon-title'),
                letter = $title.eq(0).text().charAt(0).toUpperCase();
            // Icon
            var $icon = $(this).find('.letter-icon');
            $icon.eq(0).text(letter);
        });

    }, true);

});