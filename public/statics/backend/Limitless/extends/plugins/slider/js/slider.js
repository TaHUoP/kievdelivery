$(function () {

    // Загрузка файлов
    var elfinderSelector = '#slider-elfinder';
    var slideId = null;

    $(elfinderSelector).elfinder({
        // lang: 'ru',
        url: $(elfinderSelector).data('url'),
        commands : [
            'custom','open', 'reload', 'home', 'up', 'back', 'forward', 'getfile', 'quicklook',
            'download', 'rm', 'duplicate', 'rename', 'mkdir', 'mkfile', 'upload', 'edit', 'extract',
            'archive', 'search', 'info', 'view', 'help', 'resize', 'sort', 'netmount'
        ],
        contextmenu : {
            navbar : ['open', '|', 'duplicate', '|', 'rm', '|', 'info'],
            cwd    : ['reload', 'back', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'info'],
            files  : ['getfile', '|', 'custom', 'quicklook', '|', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info']
        },
        uiOptions : {
            toolbar : [
                ['back', 'forward'],
                ['reload'],
                ['mkdir', 'mkfile', 'upload'],
                ['open', 'download', 'getfile'],
                ['info'],
                ['quicklook'],
                ['rm'],
                ['duplicate', 'rename', 'edit', 'resize'],
                ['extract', 'archive'],
                ['search'],
                ['view']
            ]
        },
        getFileCallback: function (file) {

            if (file.mime !== 'image/jpeg' && file.mime !== 'image/png') {
                cPanelApi.notify().warning('This is not an image');
                return false;
            }

            var $targetSlide =  $('[data-id=' + slideId + ']'),
                $targetSlideImage = $targetSlide.find('.thumb-image'),
                pathToUrl = '/' + file.path;

            $targetSlideImage.css("background-image", 'url(' + pathToUrl + ')');

            $targetSlide.each(function () {
                var $this = $(this);
                $this.find('input[name^="slider\[img\]"]').val(pathToUrl);
            });

            $('#modal-file-manager').modal('hide');
        }
    }).elfinder('instance');


    // Выбрать изображение
    $(document).on('click', '.btn.slide-upload', function () {
        var $this = $(this),
            $targetSlide = $this.closest('.slide');

        slideId = $targetSlide.data('id');

        $('#modal-file-manager').modal('show');
    });


    // Удалить слайд
    $(document).on('click', '.btn.slide-remove', function () {
        var $this = $(this),
            $targetSlidesList = $('.slide-list'),
            $targetSlide = $this.closest('.slide');
        $('[data-id=' + $targetSlide.data('id') + ']').remove();

        var countSlides = $targetSlidesList.first().find('.slide').length;

        if (!countSlides) {
            $('#no-data').removeClass('hide');
        }
    });


    // Добавить новый слайд
    $(document).on('click', '#slide-add', function () {
        var $this = $(this),
            $slideTemplatesByLocale = $('.slide-template'),
            $targetSlidesList = $('.slide-list');
        var uuid = cPanelApi.guid();
        $targetSlidesList.each(function () {
            var $this = $(this);
            $slideTemplatesByLocale.each(function () {
                var $thisSlideLocale = $(this);
                if ($this.data('locale') === $thisSlideLocale.data('locale')) {
                    var $newSlide = $thisSlideLocale.clone(true);
                    $newSlide.show();
                    $newSlide.attr('data-id', uuid);
                    $newSlide.removeClass('slide-template');
                    $newSlide.find('input, select').each(function () {
                        var $this = $(this);
                        var newName = $this.attr('name').replace(/[0]/g, uuid);
                        $this.attr('name', newName);
                        $this.removeAttr('disabled');
                    });
                    $this.append($newSlide);
                }
            });
        });
        $('.btn-save').removeClass('hide');
        $('#no-data').addClass('hide');
    });

});