$(function() {

    // Изображение - Обработка формы
    $(document).on('submit', '.form-media-image', function (event) {
        event.preventDefault();

        console.log ('In dev');

        /*var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {

                // Сброс ошибок
                clearErrors($form);

                var $modal = $('#modal-image-form-create');
                var oldValue = $modal.find(".select-ajax").val();

                // Reset формы
                $modal.modal('hide').find('form')[0].reset();

                $modal.find(".select-ajax").val(oldValue);

                // Закрыть окно
                $('#modal-image-form-edit').modal('hide');

                var $reload = $('#grid-media-images').find('[data-action="reload"]');
                gridRefresh('#grid-media-images', $reload.data('url'));
            },
            error: function (xhr, str) {

                // Сброс ошибок
                clearErrors($form);

                // Рендер ошибок
                renderErrors($form, xhr.responseJSON);
            }
        });*/
    });

    // Открыть окно для редактирования видео
    $(document).on('click', '#tab-videos .video-preview', function () {

        console.log ('VIDEO MODAL');

        /*var $this = $(this);
        var url = $this.data('url');
        if ($this.hasClass('new')) {
            url = $this.data('url').replace(new RegExp("_id", 'g'), $this.attr('id'));
        }
        $.get(url, function (response) {
            $('#modal-video-form').remove();
            $('body').append(response);
            $('#modal-video-form').modal('show');
            cPanelApi.initPlugin(['select2', 'switchery']);
        });
        return false;*/
    });

    // Видео - Обработка формы
    $(document).on('submit', '.form-media-video', function (event) {
        event.preventDefault();

        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (response) {

                // Сброс ошибок
                cPanelApi.form().clearErrors($form);

                var $modal = $('#modal-video-form-create');
                var oldValue = $modal.find(".select-ajax").val();

                // Reset формы
                $modal.modal('hide').find('form')[0].reset();

                $modal.find(".select-ajax").val(oldValue);

                // Закрыть окно
                $('#modal-video-form-edit').modal('hide');

                var $reload = $('#grid-media-videos').find('[data-action="reload"]');
                cPanelApi.grid().refresh('#grid-media-videos', $reload.data('url'));
            },
            error: function (xhr, str) {
                if (xhr.status == 422) {
                    cPanelApi.form().clearErrors($form);
                    cPanelApi.form().renderErrors($form, xhr.responseJSON);
                } else {
                    cPanelApi.notify().danger(xhr.responseJSON.message);
                }
            }
        });
    });

    // Удалить видео
    $(document).on('click', '#tab-videos .video-preview ---', function () {

        console.log ('VIDEO DELETE');

        /*var $this = $(this);
         var url = $this.data('url');
         var $videoPreview = $this.closest('.video-preview');
         if ($videoPreview.hasClass('new')) {
         url = $this.data('url').replace(new RegExp("_id", 'g'), $this.closest('.video-preview').attr('id'));
         }
         $.ajax({
         type: 'DELETE',
         url: url,
         dataType: 'json',
         success: function (response) {
         $this.closest('.video-preview').remove();
         cPanelApi.notify().success(response.message);
         },
         error: function (xhr, str) {
         cPanelApi.notify().danger(xhr.responseJSON.message);
         }
         });
         return false;*/
    });

}());