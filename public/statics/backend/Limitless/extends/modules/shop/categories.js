$(function() {

    // Форма "Создать категорию"
    $(document).on('click', '#btn-categories-create', function (e) {
        e.preventDefault();
        var $this = $(this);
        cPanelApi.loading().start('#grid-categories');
        $.ajax({
            url: $this.attr('href'),
            type: 'GET',
            success: function (response) {
                $('#grid-categories').html(response);
                cPanelApi.initPlugin(['select2', 'fancytree', 'switchery', 'summernote-min']);
                cPanelApi.url().set($this.attr('href'));
            },
            error: function (xhr, str) {

            }
        }).always(function () {
            cPanelApi.loading().stop('#grid-categories');
        });
        return false;
    });


    // Форма "Редактировать категорию"
    $(document).on('click', '#categories-tree li', function (e) {
        var $this = $(this);
        var $info = $($this.find('[name="category_id"]')[0]);
        cPanelApi.loading().start('#grid-categories');
        $.ajax({
            url: $info.data('url'),
            type: 'GET',
            success: function (response) {
                $('#grid-categories').html(response);
                cPanelApi.initPlugin(['select2', 'fancytree', 'switchery', 'summernote-min']);
                cPanelApi.url().set($info.data('url'));
            }
        }).always(function () {
            cPanelApi.loading().stop('#grid-categories');
        });
        return false;
    });


    // Отправить форму
    $(document).on('submit', '#form-category', function (e) {
        e.preventDefault();
        var $form = $('#form-category');
        var formData = new FormData($form[0]);
        cPanelApi.loading().start('#grid-categories');
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                $('#grid-categories').html(response);
                cPanelApi.initPlugin(['select2', 'fancytree', 'switchery', 'summernote-min']);
                cPanelApi.notify().success('Данные успешно сохранены');
            },
            error: function (xhr, str) {
                if (xhr.status == 422) {
                    cPanelApi.form().clearErrors($form);
                    cPanelApi.form().renderErrors($form, xhr.responseJSON);
                } else {
                    if(typeof xhr.responseJSON == 'undefined') {
                        cPanelApi.notify().danger('Возникла критическая ошибка');
                    } else{
                        cPanelApi.notify().danger(xhr.responseJSON.message);
                    }
                }
            }
        }).always(function () {
            cPanelApi.loading().stop('#grid-categories');
        });
        return false;
    });


    // Удалить категорию
    $(document).on('click', '#btn-categories-delete', function (e) {
        e.preventDefault();
        var $this = $(this);
        cPanelApi.loading().start('#grid-categories');
        $.ajax({
            url: $this.data('url'),
            type: 'POST',
            data: {
                _method: 'DELETE'
            },
            success: function (response) {
                $('#grid-categories').html(response);
                cPanelApi.initPlugin(['select2', 'fancytree', 'switchery', 'summernote-min']);
                cPanelApi.notify().success('Данные успешно удалены');
            },
            error: function (xhr, str) {
                if(typeof xhr.responseJSON == 'undefined') {
                    cPanelApi.notify().danger('Возникла критическая ошибка');
                } else{
                    cPanelApi.notify().danger(xhr.responseJSON.message);
                }
            }
        }).always(function () {
            cPanelApi.loading().stop('#grid-categories');
        });
        return false;
    });

});