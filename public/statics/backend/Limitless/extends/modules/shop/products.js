$(function() {
    
    // Сохранение фильтра для товара
    $(document).on('change', '#product-filter select', function (e) {
        e.preventDefault();
        var $this = $(this);
        var data = {};
        data[$this.attr('name')] = $this.val();
        $.ajax({
            type: 'POST',
            url: $('#product-filter').data('url'),
            dataType: 'json',
            data: data,
            success: function (response) {
                cPanelApi.notify().success(response.message);
            },
            error: function (xhr, str) {
                if(typeof xhr.responseJSON == 'undefined') {
                    cPanelApi.notify().danger('Возникла критическая ошибка');
                } else{
                    cPanelApi.notify().danger(xhr.responseJSON.message);
                }
            }
        });
        return true;
    });

    // TODO: рефактор по типу как в плагине "Слайдер" через guid

    // Загрузка файлов
    var elfinderSelector = '#elfinder-files';
    var $productFile = null;

    $(elfinderSelector).elfinder({
        // lang: 'ru',
        url: $(elfinderSelector).data('url'),
        commands : [
            'custom','open', 'reload', 'home', 'up', 'back', 'forward', 'getfile', 'quicklook',
            'download', 'rm', 'duplicate', 'rename', 'mkdir', 'mkfile', 'upload', 'edit', 'extract',
            'archive', 'search', 'info', 'view', 'help', 'resize', 'sort', 'netmount'
        ],
        contextmenu : {
            navbar : ['open', '|', 'duplicate', '|', 'rm', '|', 'info'],
            cwd    : ['reload', 'back', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'info'],
            files  : ['getfile', '|', 'custom', 'quicklook', '|', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info']
        },
        uiOptions : {
            toolbar : [
                ['back', 'forward'],
                ['reload'],
                ['mkdir', 'mkfile', 'upload'],
                ['open', 'download', 'getfile'],
                ['info'],
                ['quicklook'],
                ['rm'],
                ['duplicate', 'rename', 'edit', 'resize'],
                ['extract', 'archive'],
                ['search'],
                ['view']
            ]
        },
        getFileCallback: function (file) {

            $productFile.find("input[name*='name']").val(file.name);
            $productFile.find("input[name*='url']").val('/' + file.path);

            $('#modal-file-manager').modal('hide');
        },
        handlers: {
            /*select : function(event, elfinderInstance) {
             console.log(event.data);
             console.log(event.data.selected);
             console.log(elfinderInstance);
             },*/
            /*dblclick : function(event, elfinderInstance) {
             console.log(event.data);
             console.log(elfinderInstance);
             },*/
        }
    }).elfinder('instance');


    // Открыть окно с FTP менеджером и обозначить ячейку
    $(document).on('click', '.file-manager-cell-choice', function () {
        var $this = $(this);
        $('#modal-file-manager').modal('show');
        $productFile = $this.closest('.file-manager-cell');
    });


    // Добавить ячейку для выбора файла
    $(document).on('click', '#file-manager-cell-add', function () {

        var container = $('.panel-body');

        var maketClass = 'file-manager-cell-maket';
        var realClass = 'file-manager-cell-real';

        // TODO: аппенд ту сделать еще вводной переменной и там остальные переменные что в самом низу

        // input's Должны иметь класс "input_disabled", желательно аттрибут disabled
        // maketClass Должен иметь аттрибут data-counter = 0
        // realClass соответсвенно data-counter это номер по-порядку от 1го ....
        // у аттрибутов нейм "js_count_me" это переменная которую нужно заменить порядковым номером data-counter

        // Найти макет
        var maket = container.find('.' + maketClass);

        // Клонировать макет
        var clone = maket.clone(true);

        // Убрать класс макет, добавить класс реального обьекта
        clone.removeClass('hide').removeClass(maketClass).addClass(realClass);

        // Убрать класс .input_disabled и атрибут disabled
        var cloneChildren = clone.find('.input_disabled');
        cloneChildren.removeClass('input_disabled').removeAttr('disabled');

        // Взять реальные объекты
        var realProductFiles = container.find('.' + realClass);

        var maximum = null;

        // Найти максимальный data-counter этих обьектов
        for (var i = 0; i < realProductFiles.length; i++) {
            var value = $(realProductFiles[i]).data('counter');
            maximum = (value > maximum) ? value : maximum;
        }

        // Новый Index должен быть на один больше
        var newIndex = maximum + 1;

        // Изменить значение переменной "js_count_me" на новосозданный Index
        for (var i = 0; i < cloneChildren.length; i++) {
            var child = $(cloneChildren[i]);
            var nameArray = child.attr('name').split('js_count_me');
            child.attr('name', nameArray.join(newIndex));
        }

        // Изменить клонированному обьекту атрибут data-counter с 0 на новосозданный Index
        clone.data('counter', newIndex);

        clone.appendTo('#tab-files .panel-body');

        if ($('#tab-files .file-manager-cell:not(.hide)').length) {
            $('#tab-files .file-manager-help').addClass('hide');
        } else {
            $('#tab-files .file-manager-help').removeClass('hide');
        }

        return false;
    });


    // Удалить ячейку с файлом
    $(document).on('click', '#tab-files .file-manager-cell-remove', function () {
        var $this = $(this);
        $this.closest('.file-manager-cell').remove();
        if ($('#tab-files .file-manager-cell:not(.hide)').length) {
            $('#tab-files .file-manager-help').addClass('hide');
        } else {
            $('#tab-files .file-manager-help').removeClass('hide');
        }
        return false;
    });

    // Удалить ячейку с файлом
    $(document).on('click', '#product-filter-params input', function (e) {
        let checkbox = $(this)
        // e.preventDefault();
        var data = {
            property_id: checkbox.data("property-id"),
            value_id: checkbox.data("value-id")
        };
        $.ajax({
            type: 'POST',
            url: $('#product-filter-params').data('url'),
            dataType: 'json',
            data: data,
            success: function (response) {
                cPanelApi.notify().success(response.message);
            },
            error: function (xhr, str) {
                if(typeof xhr.responseJSON == 'undefined') {
                    cPanelApi.notify().danger('Возникла критическая ошибка');
                } else{
                    cPanelApi.notify().danger(xhr.responseJSON.message);
                }
            }
        });
        return true;
    });

});