$(function () {

    // Добавить товар
    $(document).on('keydown', '#checkout-product-add', function (event) {
        var $this = $(this);
        if (parseInt(event.keyCode) === 13) {
            event.preventDefault();
            var product_id = parseInt($this.val());
            var url = $this.attr('data-url');
            var checkout_id = $this.attr('data-checkout-id');
            cPanelApi.loading().start('#grid-products-list');
            $.post(url, {
                'product_id': product_id,
                'checkout_id': checkout_id
            }, function (response) {
                $('#grid-products-list').html(response);
                cPanelApi.initPlugin('touchspin');
            }).fail(function (response) {

                // TODO: обработать ответ
                console.log(response);
                cPanelApi.notify().danger('Возникла критическая ошибка');

            }).always(function () {
                cPanelApi.loading().stop('#grid-products-list');
            });
        }
    });


    // Редактировать товар
    $(document).on('change', '.product_amount_change', function (event) {
        var $this = $(this);
        var amount = parseInt($this.val());
        var url = $this.attr('data-url');
        cPanelApi.loading().start('#grid-products-list');
        $.post(url, {
            _method: 'PUT',
            amount: amount
        }, function (response) {
            $('#grid-products-list').html(response);
            cPanelApi.initPlugin('touchspin');
        }).fail(function (response) {

            // TODO: обработать ответ
            console.log(response);
            cPanelApi.notify().danger('Возникла критическая ошибка');

        }).always(function () {
            cPanelApi.loading().stop('#grid-products-list');
        });
    });


    // Удалить товар
    $(document).on('click', '.delete-product', function (event) {
        var url = $(this).attr('data-url');
        var checkout_id = $('#add_to_cart').attr('data-checkout-id');
        cPanelApi.loading().start('#grid-products-list');
        $.post(url, {
            _method: 'DELETE',
            checkout_id: checkout_id
        }, function (response) {
            $('#grid-products-list').html(response);
            cPanelApi.initPlugin('touchspin');
        }).fail(function (response) {

            // TODO: обработать ответ
            console.log(response);
            cPanelApi.notify().danger('Возникла критическая ошибка');

        }).always(function () {
            cPanelApi.loading().stop('#grid-products-list');
        });
    });


    $(document).on('keydown', '.product_amount_change', function (event) {
        if (parseInt(event.keyCode) === 13) {
            event.preventDefault();
            return false;
        }
    });

});