$(function() {


    // Открыть окно для редактирования свойства
    $(document).on('click', '.property-edit', function () {
        var $this = $(this);
        $.get($this.data('url'), function (response) {
            $('#modal-property-form-edit').remove();
            $('body').append(response);
            $('#modal-property-form-edit').modal('show');
            cPanelApi.initPlugin(['select2', 'switchery']);
        });
        return false;
    });


    // Открыть окно для редактирования значения
    $(document).on('click', '.value-edit', function () {
        var $this = $(this);
        $.get($this.data('url'), function (response) {
            $('#modal-value-form-edit').remove();
            $('body').append(response);
            $('#modal-value-form-edit').modal('show');
            cPanelApi.initPlugin(['select2', 'switchery']);
        });
        return false;
    });


    // Свойтво - Обработка формы
    $(document).on('submit', '.form-property', function (event) {
        event.preventDefault();
        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (response) {

                // Сброс ошибок
                cPanelApi.form().clearErrors($form);

                // Reset формы
                $('#modal-property-form-create').modal('hide').find('form')[0].reset();

                // Закрыть окно
                $('#modal-property-form-edit').modal('hide');

                var $reload = $('#grid-filter').find('[data-action="reload"]');
                cPanelApi.grid().refresh('#grid-filter', $reload.data('url'));

                cPanelApi.notify().success(response.message);
            },
            error: function (xhr, str) {
                if (xhr.status == 422) {
                    cPanelApi.form().clearErrors($form);
                    cPanelApi.form().renderErrors($form, xhr.responseJSON);
                } else {
                    if(typeof xhr.responseJSON == 'undefined') {
                        cPanelApi.notify().danger('Возникла критическая ошибка');
                    } else{
                        cPanelApi.notify().danger(xhr.responseJSON.message);
                    }
                }
            }
        });
    });


    // Значение - Обработка формы
    $(document).on('submit', '.form-value', function (event) {
        event.preventDefault();
        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (response) {

                // Сброс ошибок
                cPanelApi.form().clearErrors($form);

                var $modalValue = $('#modal-value-form-create');
                var oldValue = $modalValue.find(".select-ajax").val();

                // Reset формы
                $modalValue.modal('hide').find('form')[0].reset();

                $modalValue.find(".select-ajax").val(oldValue);

                // Закрыть окно
                $('#modal-value-form-edit').modal('hide');

                var $reload = $('#grid-filter').find('[data-action="reload"]');
                cPanelApi.grid().refresh('#grid-filter', $reload.data('url'));

                cPanelApi.notify().success(response.message);
            },
            error: function (xhr, str) {
                if (xhr.status == 422) {
                    cPanelApi.form().clearErrors($form);
                    cPanelApi.form().renderErrors($form, xhr.responseJSON);
                } else {
                    if(typeof xhr.responseJSON == 'undefined') {
                        cPanelApi.notify().danger('Возникла критическая ошибка');
                    } else{
                        cPanelApi.notify().danger(xhr.responseJSON.message);
                    }
                }
            }
        });
    });


    // Удалить свойство или значение
    $(document).on('click', '.btn-filter-destroy', function () {
        var $this = $(this);
        $.ajax({
            url: $this.data('url'),
            type: 'DELETE',
            success: function (response) {

                $('#modal-property-form-edit').modal('hide');
                $('#modal-value-form-edit').modal('hide');

                var $reload = $('#grid-filter').find('[data-action="reload"]');
                cPanelApi.grid().refresh('#grid-filter', $reload.data('url'));

                cPanelApi.notify().success(response.message);
            },
            error: function (xhr, str) {
                if(typeof xhr.responseJSON == 'undefined') {
                    cPanelApi.notify().danger('Возникла критическая ошибка');
                } else{
                    cPanelApi.notify().danger(xhr.responseJSON.message);
                }
            }
        });
        return false;
    });

});