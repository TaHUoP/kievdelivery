$(function() {

    // Открыть окно для редактирования изображения
    $(document).on('click', '#tab-images .image-preview', function () {
        var $this = $(this);

        if ($this.data('modal') === false) {
            return false;
        }

        var url = $this.data('url');
        if ($this.hasClass('new')) {
            url = $this.data('url').replace(new RegExp("_id", 'g'), $this.attr('id'));
        }
        $.get(url, function (response) {
            $('#modal-image-form').remove();
            $('body').append(response);
            $('#modal-image-form').modal('show');
            cPanelApi.initPlugin(['select2', 'switchery']);
        });
        return false;
    });


    // Изображение - Обработка формы
    $(document).on('submit', '#modal-image-form .form-image', function (event) {
        event.preventDefault();
        var $form = $(this);
        var formData = new FormData(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {

                // Сброс ошибок
                cPanelApi.form().clearErrors($form);

                // Закрыть окно
                $('#modal-image-form').modal('hide');

                cPanelApi.notify().success(response.message);

                if (formData.get("default") === 'on') {
                    $('#images-uploads > .image-preview').removeClass('is-main-photo');
                    $('#images-uploads').find('#' + $form.data('id')).addClass('is-main-photo');
                }
            },
            error: function (xhr, str) {
                if (xhr.status == 422) {
                    cPanelApi.form().clearErrors($form);
                    cPanelApi.form().renderErrors($form, xhr.responseJSON);
                } else {
                    if(typeof xhr.responseJSON == 'undefined') {
                        cPanelApi.notify().danger('Возникла критическая ошибка');
                    } else{
                        cPanelApi.notify().danger(xhr.responseJSON.message);
                    }
                }
            }
        });
    });


    // Удалить изображение
    $(document).on('click', '#tab-images .image-preview .dz-remove', function () {
        var $this = $(this);
        var url = $this.data('url');
        var $imagePreview = $this.closest('.image-preview');
        if ($imagePreview.hasClass('new')) {
            url = $this.data('url').replace(new RegExp("_id", 'g'), $this.closest('.image-preview').attr('id'));
        }
        $.ajax({
            type: 'DELETE',
            url: url,
            dataType: 'json',
            success: function (response) {
                $this.closest('.image-preview').remove();
                cPanelApi.notify().success(response.message);
            },
            error: function (xhr, str) {
                if(typeof xhr.responseJSON == 'undefined') {
                    cPanelApi.notify().danger('Возникла критическая ошибка');
                } else{
                    cPanelApi.notify().danger(xhr.responseJSON.message);
                }
            }
        });
        return false;
    });

});