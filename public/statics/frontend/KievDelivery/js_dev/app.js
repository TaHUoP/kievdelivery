// Отлаживаем события
var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();


$(window).resize(function () {
    var win = $(this).width();
    waitForFinalEvent(function () {
        if(win >= 992) {
            var body = $('body');
            var menu = $('.l-mobile-menu');
            body.removeClass('no-scroll');
            menu.removeClass('is-open');
        }
    }, 100, 'window-resize');
});


$(function () {

    var token = $('meta[name=_token]').attr("content");

    $('.reviews-block-form-rating').each(function () {
        var $this = $(this);
        addRatingForForm(buildItem($this), 3);
    });


    $(document).on('click', '.nav-mobile-button', function () {
        var body = $('body'),
            menu = $('.l-mobile-menu');
        body.toggleClass('no-scroll', 300);
        menu.toggleClass('is-open', 300);
    });


    $(document).on('click', '.l-product-present-image', function () {
        var $this = $(this);
        // var url = $this.css('background-image');
        //var currentItem = url.replace(/url((.*?))/g, "").replace(/\(/g, '').replace(/\)/g, '').replace(/\"/g, '');

        var currentItem = $this.data('url');

        // Галерая изображений товара
        var $productPresetView = $('.l-product-present-view');
        if ($productPresetView.length) {
            var images = [];
            $('.l-product-present-view .swiper-slide').each(function (index) {
                var $this = $(this);
                // var url = $this.css('background-image');
                // var _url = url.replace(/url((.*?))/g, "").replace(/\(/g, '').replace(/\)/g, '').replace(/\"/g, '');
                var _url = $this.data('url');
                images.push({
                    src: _url
                });
            });

            if (currentItem) {
                var newImages = [];
                images.unshift({
                    src: currentItem
                });
                for (var img in images) {
                    var isHave = false;
                    for (var _img in newImages) {
                        if (images[_img].src == images[img].src) {
                            isHave = true;
                            break;
                        }
                    }
                    if (!isHave) {
                        newImages.push(images[img]);
                    }
                }
                images = newImages;
            }

            $.magnificPopup.open({
                items: images,
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        }
    });


    $(document).on('click', '#product-preview .swiper-slide', function () {
        var $this = $(this);
        $('.l-product-present-image')
            .css('background-image', $this.css('background-image'))
            .data('url', $this.data('url'));
        $('#product-preview .swiper-slide').removeClass('is-active');
        $this.addClass('is-active');
    });


    $(document).on('click', '.cart-add-amount-button', function () {
        var $this = $(this);
        var amount = $('#product-amount');
        var value = amount.val() ? parseInt(amount.val()) : 1;
        if (value < 1) {
            value = 1;
        }
        if ($this.data('amount') == 'plus') {
            amount.val(value + 1);
        } else {
            if (value > 1) {
                amount.val(value - 1);
            }
        }
    });


    /**
     * Раскртытие подкатегорий в мобильном меню
     */
    $(document).on('click', '.l-mobile-menu .nav-horizontal > li > a', function (e) {
        var sibling = $(this).siblings('.dropdown');
        if(sibling.length) {
            e.preventDefault();
            sibling.toggle();
        }
    });

    /**
     * Вставляем название категории мобильного меню в выпадающий список
     */
    $('.l-mobile-menu .nav-horizontal > li > a').each(function () {
        var $this = $(this);
        var $sibling = $this.siblings('.dropdown');
        if($sibling.length) {
            var cloned = $this.clone();
            $sibling.prepend(cloned);
            $(cloned).wrap('<li></li>');
        }
    });


    if ($('.l-photoDeliveries').length) {
        $('.l-photoDeliveries').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    // return item.el.attr('title');
                    return null;
                }
            }
        });
    }

    if ($('.popup-youtube, .popup-vimeo, .popup-gmaps').length) {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }


    if ($('.l-slider .swiper-container').length) {
        var swiperIndex = new Swiper(".l-slider .swiper-container", {
            pagination: ".swiper-pagination",
            slidesPerView: 1,
            paginationClickable: !0,
            spaceBetween: 0,
            // nextButton: ".swiper-button-next",
            // prevButton: ".swiper-button-prev",
            grabCursor: !0,
            loop: 1,
            autoplay: 5000,
            speed: 1000
        });
    }


    if ($('.l-review .swiper-container').length) {
        var swiperReview = new Swiper(".l-review .swiper-container", {
            slidesPerView: 3,
            spaceBetween: 50,
            pagination: ".swiper-pagination",
            paginationClickable: !0,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            grabCursor: !0,
            //loop: 1,
            autoplay: 5000,
            speed: 1000,
            breakpoints: {
                768: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 20
                },
                992: {
                    slidesPerView: 2,
                    spaceBetweenSlides: 20
                }
            }
        })
    }


    if ($('.l-product .swiper-container').length) {
        var swiperProduct = new Swiper(".l-product .swiper-container", {
            slidesPerView: 4,
            spaceBetween: 15,
            pagination: ".swiper-pagination",
            paginationClickable: !0,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            grabCursor: !0,
            loop: 0,
            autoplay: 5000,
            speed: 1000,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetweenSlides: 20
                }
            }
        })
    }

    if ($('#loader-test').length) {
        loading(true);
        setTimeout(function () {
            loading(false);
        }, 5000)
    }


    /**
     * Запрещает ввод всех символов, кроме числе в текстовое поле
     */
    $("input[type='number'], .input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    // Календарь
    if ($('.datapicker').length) {
        new Pikaday({
            field: document.getElementById('datepicker'),
            format: 'DD.MM.YYYY',
            formatStrict: 'DD.MM.YYYY',
            onSelect: function () {
                console.log(this.getMoment().format('yyyy.mm.dd'));
            }
        });
    }


    // Редирект при фильтре
    /*$('#filter-form select').change(function() {
        var $this = $(this);
        console.log($this);
        window.location.href = $this.val();
    });*/


    // Купить сразу
    if ($('.popup-with-form').length) {
        $(document).on('click', '.popup-with-form', function () {
            var $this = $(this);
            loading(true);
            $.post($this.data('url'), {
                _token: token,
                product_id: $('.l-product').data('id'),
            }, function(response) {
                $.notify(response.message, {
                    type: 'success',
                    showProgressbar: false
                });
                if (typeof response.url != 'undefined') {
                    window.location.href = response.url;
                }
            }).fail(function () {
                $.notify(jsData.messages.critical_error, {
                    type: 'danger',
                    showProgressbar: false
                });
            }).always(function () {
                loading(false);
            });
        });
    }

    customRadio();
    customCheckbox();
    disabledInputs();

    /**
     * Скрыть текст, к-й превышает опр. кол-во символов
     */
    deployText({
        target: $('.deploy-text'),
        maxLength: 300
    });

    /**
     * Развернуть, свернуть скрытый текст
     */
    $(document).on('click', '.hidden-text-btn', function () {
        $(this)
            .toggleClass('is-active')
            .siblings('.hidden-text')
            .toggle();
    })
});

/**
 * Ф-я показывает часть текста и добавляет кнопку развернуть
 */
function deployText(option) {
    var deployText = typeof option.target == 'object' ? option.target : null || $('.deploy-text'); // Проверка на класс
    var maxLength = typeof option.maxLength == 'number' ? option.maxLength : null || 300; // Проверка на кол-во символов
    if(deployText.length) { // Если на странице есть элементы
        deployText.each(function () { // Проходим по элементам
            var $this = $(this); // этот элемент
            var fullText = $this.text(); // Кол-во символов в элементе
            if(fullText.length > (maxLength + (maxLength * 0.3))) { // Проверка на допустимость символов
                var shortText = ''; // Краткий текст
                var hiddenText = '<span class="hidden-text">'; // скрытый текст
                // Прохлдим по цклу
                var i = 0;
                while (true) {
                    shortText+= fullText[i];
                    if(i > maxLength && fullText[i] == '.') {
                        break;
                    }
                    i++;
                }
                // Записываем в скрытый блок текст
                hiddenText += fullText.slice(shortText.length, -1);

                hiddenText += '</span>';
                // Создаем кнопку для раскрытия текста
                var btnText = $this.data('btn-text') || 'Read full';
                var btn = '<button class="hidden-text-btn">' + btnText +'</button>';
                // Перезаписываем в блок
                $this.html(shortText + hiddenText + btn);
            }
        });
    }
}

/**
 * Disabled style
 */
function disabledInputs() {
    var disInputs = document.querySelectorAll('input[disabled]');
    for (var i = 0; i < disInputs.length; i++) {
        var disInput = disInputs[i];
        if (disInput) {
            disInput.parentElement.classList.add('is-disabled');
        }
    }
}


/**
 * Custom Input Checkbox
 * @param radioName
 */
function customCheckbox(radioName) {
    $('input[type=checkbox]:not(".default")').each(function () {
        var $this = $(this);
        $this.addClass('checkbox');
        $('<span class="checkbox"></span>').insertAfter($this);
        if ($this.is(':checked')) {
            $this.next('span.checkbox').addClass('selected');
        }
        $this.fadeTo(0, 0);
        $this.change(function () {
            $this.next('span.checkbox').toggleClass('selected');
        });
    });
}


/**
 * Custom Input Radio
 * @param radioName
 */
function customRadio(radioName) {
    var radioButton = 'input[type=radio]';
    $(radioButton).each(function () {
        var $this = $(this);
        $this.wrap("<span class='radio'></span>");
        if ($this.is(':checked')) {
            $this.parent().addClass("selected");
        }
    });
    $(document).on('click', radioButton ,function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parent().addClass("selected");
        }
        $(radioButton).not(this).each(function () {
            var $this = $(this);
            $this.parent().removeClass("selected");
        });
    });
}


/**
 * Процесс загрузки
 * @param load
 * @param selector
 */
function loading(load, selector) {
    var $block;

    load = (load === false) ? false : true;
    selector = selector || false;

    if (!selector) {
        selector = 'body';
    }

    $block = $(selector);

    if (!$block.length) {
        $block = $('body');
    }

    if (load) {
        $('html').addClass('no-scroll');
        $block.css({
            'position': 'relative'
        });
        $block.append('<div class="loading"><div class="loader"></div></div>');
    } else {
        $('html').removeClass('no-scroll');
        $block.css({
            'position': 'auto'
        });
        $(selector + ' .loading').remove();
    }
}



/**
 * Звездный рейтинг
 * @param shop
 * @returns {HTMLElement}
 */
function buildItem(shop) {
    var item = document.createElement('div');
    var html = '<div class="c-item__img"></div>' + '<div class="c-item__details">' + '<ul class="c-rating"></ul>' + '</div>';
    item.classList.add('c-item');
    item.innerHTML = html;
    shop.append(item);
    return item;
}

function addRatingForForm(_item, _rating) {
    var ratingElement = _item.querySelector('.c-rating');
    var currentRating = _rating;
    var maxRating = 5;
    var callback = function (rating) {
        $('#review-rating').val(rating);
    };
    var r = rating(ratingElement, currentRating, maxRating, callback);
}

//$('#form-send-cart').submit();


