$(document).ready(function () {
    var cityForm = $('#city-form');

    $('#city-select').on('change', function () {
        cityForm.submit();
    });

    $('#city-select-modal').on('change', function () {
        $('#city-form-modal').submit();
    });

    $('#checkout-city-select').on('change', function () {
        var route = cityForm.attr('action'),
            city_id = $(this).val(),
            deliveryPrice,
            totalDeliveryPrice = 0,
            finalPrice;

        $.ajax({
            url: route,
            method: 'POST',
            data:  { city_id: city_id },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                // if (totalItemsPrice > 50 && response.cityFee) {
                    totalDeliveryPrice = response.cityFee;
                // } else {
                //     if (response.deliveryPrice) {
                //         totalDeliveryPrice = parseFloat(response.cityFee) + parseFloat(response.deliveryPrice);
                //     }
                // }
                finalPrice = parseFloat(totalDeliveryPrice) + parseFloat(totalItemsPrice);
                $('#delivery-price').text('$ ' + parseFloat(totalDeliveryPrice).toFixed(2));
                $('#total-price').text('$ ' + parseFloat(finalPrice).toFixed(2));
            },
            error: function (response) {

            }
        });
    });

    $(".select-city").select2({
        searchInputPlaceholder: 'Enter City name',
        matcher: matchCustom,
        width: 'resolve'
    });

    (function($) {

    	var Defaults = $.fn.select2.amd.require('select2/defaults');

      $.extend(Defaults.defaults, {
      	searchInputPlaceholder: ''
      });

      var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');

      var _renderSearchDropdown = SearchDropdown.prototype.render;

      SearchDropdown.prototype.render = function(decorated) {

        // invoke parent method
        var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));

        this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));

        return $rendered;
      };

    })(window.jQuery);

    function matchCustom(params, data) {
        // If there are no search terms, return all of the data
        if ($.trim(params.term) === '') {
            return data;
        }

        // Do not display the item if there is no 'text' property
        if (typeof data.text === 'undefined') {
            return null;
        }

        // `params.term` should be the term that is used for searching
        // `data.text` is the text that is displayed for the data object
        if (data.element.dataset.keywords.toUpperCase().indexOf(params.term.toUpperCase()) > -1) {
            var modifiedData = $.extend({}, data, true);

            // You can return modified objects from here
            // This includes matching the `children` how you want in nested data sets
            return modifiedData;
        }

        // Return `null` if the term should not be displayed
        return null;
    }

    $('.close, .overlay, .decline').on('click' , function(){
        $('.overlay, .modal-window').fadeOut(400);
    })
});