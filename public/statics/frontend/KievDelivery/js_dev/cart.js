$(function () {
    var cartControlUrl = '/cart/control';
    var checkCouponUrl = '/check-coupon';
    var deleteCouponUrl = '/delete-coupon';

    var token = $('meta[name=_token]').attr("content");

    // Изменение кол-во товаров
    $(document).on('change', '.cart-products-amount', function () {

        var $this = $(this);
        var productId = $this.parents('.l-cart-list-item').data('id');

        request('change', productId, {amount: $this.val()}, function (response) {
            $('#cart-section').html(response);
            request(null, productId, {amount: $this.val()}, function (response) {
                $('.cart-mini').html(response);
            });
        });

    });


    // Удаление товара
    $(document).on('click', '.l-cart-list-item-remove', function () {
        if (confirm('Confirm...')) {
            var $this = $(this);
            var productId = $this.parents('.l-cart-list-item').data('id');
            request('remove', productId, {}, function (response) {
                $('#cart-section').html(response);
                request(null, productId, {amount: $this.val()}, function (response) {
                    $('.cart-mini').html(response);
                });
            });
        }
    });


    // Добавить в корзину
    $(document).on('click', '.add-to-cart', function () {

        var productId, amount = 1;
        var $this = $(this);
        var $targetProductAmount = $('#product-amount');

        if ($targetProductAmount.length) {
            productId = parseInt($this.parents('.l-product').data('id'));
            amount = $targetProductAmount.val();
            $targetProductAmount.val(1);
        } else {
            productId = parseInt($this.parents('.product-short').data('id'));
        }

        request('add', productId, {amount: amount}, function (response) {
            $.notify(jsData.messages.cart_add_success, {
                showProgressbar: false
            });

            if (location.pathname == '/cart') {
                location.reload();
            }

            if (currentLang != "en")
                response = response.replace("cart", currentLang + "/cart")

            console.log(response)
            $('.cart-mini').html(response);
            // mob cart
            let getCartPrice = $('.cart-mini-info-price').html()
            $('.js-cart-mob__price').html(getCartPrice)
            $('.js-cart-mob__has-product').addClass('cart-mob__has-product')

        });
    });


    // Добавить купон
    $(document).on('click', '#add-coupon', function () {
        var couponCode = $('#coupon-code').val();
        loading(true);

        $.post(checkCouponUrl, {
            _token: token,
            coupon: couponCode,
            lang: currentLang
        }, function (response) {
            $('#finance-part').html(response);
        }).fail(function (response) {
            $.notify(jsData.messages.coupon_no_discounts, {
                type: 'warning',
                showProgressbar: false
            });
        }).done(function(response) {
            customCheckbox();
        }).always(function () {
            loading(false);

            //customRadio();
            disabledInputs();
        });
    });

    // Удалить купон
    $(document).on('click', '#delete-coupon', function () {
        loading(true);

        $.post(deleteCouponUrl, {
            _token: token,
            _method: 'delete',
            lang: currentLang
        }, function (response) {
            $('#finance-part').html(response);
        }).fail(function (response) {
            $.notify(jsData.messages.critical_error, {
                type: 'warning',
                showProgressbar: false
            });
        }).always(function () {
            loading(false);

            //customRadio();
            customCheckbox();
            disabledInputs();
        });
    });



    $(document).on('click', '#change-coupon', function () {
        $('.coupon-toggle').toggleClass('hidden');
    });

    /**
     * Запрос к серверу
     *
     * @param action
     * @param productId
     * @param extraData
     * @param successFunction
     */
    function request(action, productId, extraData, successFunction) {
        loading(true);
        $.post(cartControlUrl, {
            _token: token,
            action: action,
            productId: productId,
            data: extraData,
            lang: currentLang
        }, successFunction).fail(function () {
            $.notify(jsData.messages.critical_error, {
                type: 'danger',
                showProgressbar: false
            });
        }).always(function () {
            loading(false);
        });
    }

});