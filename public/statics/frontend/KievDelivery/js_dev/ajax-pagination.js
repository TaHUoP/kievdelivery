jQuery(document).ready(function($) {
    var current_order = $('select[name=filter\\[order\\]]').find(':selected').data('order');
    var current_per_page = $('select[name=filter\\[count\\]]').find(':selected').html();
    var current_page = $('.pagination li.active a').data('page');
    var current_price_range = $('.price-filter li.is-active a').data('price_range');
    if(typeof current_price_range == 'undefined')
        current_price_range = 0;
    var url = $('.ajax-pagination a').attr('href');

    function loadPage(page, per_page, order, price_range, more, render_pagination){
        var done = false;

        $.ajax({
            url : [url, 'sort_price_' + order, 'count_' + per_page, 'price_' + price_range].join('/') + '?page=' + page
        }).done(function (data) {
            data = JSON.parse(data);

            if(data.last_page <= page) {
                $('.ajax-pagination').hide();
            }else{
                $('.ajax-pagination').show();
            }

            if(more){
                $('.grid-products .product-short').last().after(data.page);
            }else{
                $('.grid-products .product-short').remove();
                $('.grid-products').prepend(data.page);
            }

            if(render_pagination){
                var pagination_html = $(data.pagination).html();
                if(typeof pagination_html == 'undefined')
                    pagination_html = '';
                $(".pagination").html(pagination_html);
            }

            current_order = order;
            current_per_page = per_page;
            current_page = page;
            current_price_range = price_range;

            if(!more)
                $('.pagination li').removeClass('active');
            $('.pagination a[data-page=' + (current_page) + ']').parent().addClass('active');

            $('.price-filter li').removeClass('is-active');
            $('.price-filter a[data-price_range=' + price_range + ']').parent().addClass('is-active');

            $('.ajax-pagination .preloader').removeClass('ajax-loading');
            done = true;
        }).fail(function () {
            $('.pagination a[data-page=' + (current_page) + ']').parent().removeClass('active');

            $('.price-filter a[data-price_range=price_' + (current_page) + ']').parent().addClass('active');

            $('.ajax-pagination .preloader').removeClass('ajax-loading');
        });

        return done;
    }

    $(document).on('click', '.ajax-pagination', function(e) {
        e.preventDefault();
        $('.ajax-pagination .preloader').addClass('ajax-loading');

        loadPage(current_page + 1, current_per_page, current_order, current_price_range, true, false);
    });

    $(document).on('click', '.pagination a', function(e) {
        e.preventDefault();
        $(this).parent().addClass('active');
        var page = $(this).data('page');

        if (page != 1) {
            $('.category-info').hide();
        } else {
            $('.category-info').show();
        }

        loadPage(page, current_per_page, current_order, current_price_range, false, false);
    });

    $(document).on('click', '.price-filter a', function(e) {
        e.preventDefault();
        $(this).parent().addClass('is-active');

        loadPage(1, current_per_page, current_order, $(this).data('price_range'), false, true);
    });

    $(document).on('change', 'select[name=filter\\[order\\]]', function(e) {
        e.preventDefault();

        loadPage(1, current_per_page, $(this).find(':selected').data('order'), current_price_range, false, false);
    });

    $(document).on('change', 'select[name=filter\\[count\\]]', function(e) {
        e.preventDefault();

        loadPage(1, $(this).find(':selected').html(), current_order, current_price_range, false, true);
    });
});