var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


// elixir(function(mix) {
//     mix.stylesIn(['build.scss'],
//         'resources/views/frontend/Rupes/client/src/css',
//         'public/statics/frontend/Rupes/css');
// });

'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

var path = {
    build: {
        html: 'build/',
        js: 'public/statics/frontend/KievDelivery/js/',
        css: 'public/statics/frontend/KievDelivery/css/',
        img: 'public/statics/frontend/KievDelivery/img/'
    },
    src: {
        js: 'public/statics/frontend/KievDelivery/js_dev/*.js',
        style: 'resources/views/frontend/KievDelivery/client/src/scss/build.scss',
        img: 'resources/views/frontend/KievDelivery/client/src/img/**/*.*'
    },
    watch: {
        js: 'resources/views/frontend/KievDelivery/client/src/js/**/*.js',
        style: 'resources/views/frontend/KievDelivery/client/src/scss/**/*.scss',
        img: 'resources/views/frontend/KievDelivery/client/src/img/**/*.*'
    },
    clean: './build'
};


gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        //.pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(rename('styles.min.css'))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)); //И в build
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(uglify()) //Сожмем наш js
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)); //И бросим в build
});

gulp.task('build', [
    'js:build',
    'style:build',
    'image:build',
]);

gulp.task('watch', function(){
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
});