<?php


class CatalogFilterTest extends TestCase
{

    /**
     * @return void
     */
    public function testParseFilter()
    {
        $service = $this->getService();
        self::assertEquals(4, $service->parseFilter()->count());
    }

    /**
     * @return void
     */
    public function testIsFilter()
    {
        $service = $this->getService();
        $service->parseFilter();
        $service->addIdToFilterParams();
        self::assertTrue($service->isFilter());
    }

    /**
     * @return void
     */
    public function testAddIdToFilterParams()
    {
        $service = $this->getService();
        $service->parseFilter();
        self::assertEquals("red", $service->addIdToFilterParams()->first()->getAlias());
        self::assertEquals(30, $service->addIdToFilterParams()->last()->getId());
    }

    /**
     * @return void
     */
    public function testCropQueryQuery()
    {
        $service = $this->getService();
        $service->parseFilter();
        $service->addIdToFilterParams();
        self::assertEquals("other/text?params=test", $service->cropQuery());
    }

    public function testGetProductsIds()
    {
        $service = $this->getService();
        $service->parseFilter();
        $service->addIdToFilterParams();
        self::assertTrue($service->getProductsIds()->count() > 0);
    }

    public function testAddIsActiveToValues()
    {
        $service = $this->getService();
        $service->parseFilter();
        $service->addIdToFilterParams();
        self::assertTrue($service->addIsActiveToValues()
            ->getPropertiesValues()->first()
            ->getFilterValues()->first()->isActive()
        );

    }

    /**
     * @return \App\Modules\CatalogFilterModule\Services\FilterService
     */
    protected function getService(): \App\Modules\CatalogFilterModule\Services\FilterService
    {
        $service = new \App\Modules\CatalogFilterModule\Services\FilterService();
        $service->init(
            new \App\Modules\CatalogFilterModule\Services\PropertiesValuesService(51),
            "red/0-40/other/text?params=test"
        );
        return $service;
    }
}
