<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FilterPropertiesServiceTest extends TestCase
{
    /**
     * @return void
     */
    public function testRawResult()
    {
        $service = new \App\Modules\CatalogFilterModule\Services\PropertiesValuesService(51);
        $this->assertArrayHasKey(0, $service->getRawResults());
    }

    public function testCollectionPropsValues()
    {
        $service = new \App\Modules\CatalogFilterModule\Services\PropertiesValuesService(51);
        self::assertTrue( !$service->getPropertiesValues()->isEmpty());
        self::assertTrue(is_string($service->getPropertiesValues()->shift()->getName()));
    }


}
