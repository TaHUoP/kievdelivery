<?php

use App\Modules\LandingModule\Models\LandingModel;
use App\Modules\LandingModule\Models\LandingTranslateModel;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LandingModuleTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCrudLanding()
    {
        list($model, $landing) = $this->getModels();
        self::assertNotNull($landing);
        self::assertEquals($model->getUrl(), $landing->getUrl());
        self::assertEquals($model->getHash(), $landing->getHash());
    }

    public function testCrudLandingsTranslate()
    {
        list($model, $landing) = $this->getModels();
        $langs = \Waavi\Translation\Models\Language::all();
        $translate = new LandingTranslateModel();
        $translate->setLandingId($landing->getId());
        $translate->setLangId($langs->first()->id);
        $param = "test";
        $translate->setDescription(collect([new \App\Modules\LandingModule\Models\ParamModel($param)]));
        $translate->save();
        self::assertEquals(
            $param,
            $landing->translations->first()->getDescription()->first()->getParam()
        );

    }

    /**
     * @return LandingModel[]
     */
    protected function getModels(): array
    {
        $model = new LandingModel();
        $url = "http://test.ua";
        $model->setUrl($url);
        $model->setHash(md5($url));
        $model->setNoIndex(LandingModel::ENUM_ON);
        $model->save();
        /** @var LandingModel $landing */
        $landing = LandingModel::all()->last();
        return array($model, $landing);
    }
}
