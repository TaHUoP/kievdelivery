<?php

use App\Modules\I18nModule\Services\I18nService;

class I18nModuleTest extends TestCase
{
    /**
     * @return void
     */
    public function testRawUrls()
    {
        /** @var I18nService $service */
        $service = app(I18nService::class);
        $rawUrl = "http://test.com.ua/catalog/product";
        self::assertEquals($rawUrl, $service->rawUrl($rawUrl));

        $url = "http://test.com.ua/ru/catalog/product";
        self::assertEquals($rawUrl, $service->rawUrl($url));

        $url = "http://test.com.ua/uk/catalog/product";
        self::assertEquals($rawUrl, $service->rawUrl($url));
    }
}
