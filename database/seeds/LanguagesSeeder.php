<?php

use Illuminate\Database\Seeder;

/**
 * Languages Seeder
 * php artisan db:seed --class=LanguagesSeeder
 */
class LanguagesSeeder extends Seeder
{
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('translator_languages')->insert([
                [
                    'id' => 1,
                    'locale' => 'ru',
                    'name' => 'Русский',
                    'default' => 1,
                    'active' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 2,
                    'locale' => 'en',
                    'name' => 'English',
                    'default' => 0,
                    'active' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
