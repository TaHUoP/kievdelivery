<?php

use Illuminate\Database\Seeder;

/**
 * Languages Seeder
 * php artisan db:seed --class=PriceTypesSeeder
 */
class PriceTypesSeeder extends Seeder
{
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('price_types')->insert([
                [
                    'id' => 1,
                    'alias' => 'no_fee',
                    'fee' => 0,
                ],
                [
                    'id' => 2,
                    'alias' => 'medium_fee',
                    'fee' => 15,
                ],
                [
                    'id' => 3,
                    'alias' => 'high_fee',
                    'fee' => 25,
                ],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
