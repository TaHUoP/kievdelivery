<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('cities')->truncate();
            DB::table('cities')->insert([
                'id' => 1,
                'alias' => 'kiev',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 2,
                'alias' => 'vinnytsia',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 3,
                'alias' => 'dnipro',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 4,
                'alias' => 'donetsk',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 5,
                'alias' => 'zhytomyr',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 6,
                'alias' => 'zaporizhia',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 7,
                'alias' => 'ivano-frankivsk',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 8,
                'alias' => 'kropyvnytsyi',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 9,
                'alias' => 'mariupil',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 10,
                'alias' => 'lugansk',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 11,
                'alias' => 'lutsk',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 12,
                'alias' => 'lviv',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 13,
                'alias' => 'mykolaiv',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 14,
                'alias' => 'odessa',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 15,
                'alias' => 'poltava',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 16,
                'alias' => 'rivne',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 17,
                'alias' => 'sumy',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 18,
                'alias' => 'sevastopil',
                'price_type_id' => 1,
                'is_region_center' => 0
            ]);
            DB::table('cities')->insert([
                'id' => 19,
                'alias' => 'ternopil',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 20,
                'alias' => 'kharkiv',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 21,
                'alias' => 'kherson',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 22,
                'alias' => 'khmelnytskiy',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 23,
                'alias' => 'cherkasy',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 24,
                'alias' => 'chernihiv',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 25,
                'alias' => 'chernivtsi',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 26,
                'alias' => 'uzhgorod',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::table('cities')->insert([
                'id' => 27,
                'alias' => 'simferopil',
                'price_type_id' => 1,
                'is_region_center' => 1
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
