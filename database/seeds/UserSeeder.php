<?php

use Illuminate\Database\Seeder;

/**
 * Items Seeder
 * php artisan db:seed --class=UserSeeder
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws Exception
     */
    public function run()
    {
        try {

            $date = date('Y-m-d H:i:s');

            DB::beginTransaction();

            DB::table('users')->insert([
                [
                    'id' => 1,
                    'email' => 'admin@admin.ru',
                    'password' => Hash::make('admin'),
                    'phone' => '380630000000',
                    'access_cpanel' => 1,
                    'status' => 1,
                    'visit_at' => $date,
                    'created_at' => $date,
                    'updated_at' => $date,
                ],
            ]);

            DB::table('users_profile')->insert([
                [
                    'id' => 1,
                    'user_id' => 1,
                    'name' => 'Admin',
                    'surname' => 'Admin',
                    'avatar_url' => null,
                ],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
