<?php

use Illuminate\Database\Seeder;
use App\Models\City;
use App\Models\Content;

class RegionTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('regions_translate')->truncate();

            // RU
            DB::table('regions_translate')->insert([
                'region_id' => 1,
                'lang_id' => 1,
                'name' => 'Одеська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 2,
                'lang_id' => 1,
                'name' => 'Дніпровська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 3,
                'lang_id' => 1,
                'name' => 'Чернігівська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 4,
                'lang_id' => 1,
                'name' => 'Харківська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 5,
                'lang_id' => 1,
                'name' => 'Житомирська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 6,
                'lang_id' => 1,
                'name' => 'Полтавська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 7,
                'lang_id' => 1,
                'name' => 'Херсонська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 8,
                'lang_id' => 1,
                'name' => 'Київська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 9,
                'lang_id' => 1,
                'name' => 'Запоріжська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 10,
                'lang_id' => 1,
                'name' => 'Луганська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 11,
                'lang_id' => 1,
                'name' => 'Донецька обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 12,
                'lang_id' => 1,
                'name' => 'Вінницька обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 13,
                'lang_id' => 1,
                'name' => 'АР Крим обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 14,
                'lang_id' => 1,
                'name' => 'Кіровоградська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 15,
                'lang_id' => 1,
                'name' => 'Миколаївська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 16,
                'lang_id' => 1,
                'name' => 'Сумська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 17,
                'lang_id' => 1,
                'name' => 'Львовська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 18,
                'lang_id' => 1,
                'name' => 'Черкаська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 19,
                'lang_id' => 1,
                'name' => 'Хмельницька обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 20,
                'lang_id' => 1,
                'name' => 'Волинська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 21,
                'lang_id' => 1,
                'name' => 'Ровенська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 22,
                'lang_id' => 1,
                'name' => 'Івано-Франківська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 23,
                'lang_id' => 1,
                'name' => 'Тернопільска обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 24,
                'lang_id' => 1,
                'name' => 'Закарпатська обл.',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 25,
                'lang_id' => 1,
                'name' => 'Чернівецька обл.',
            ]);

            // EN

            DB::table('regions_translate')->insert([
                'region_id' => 1,
                'lang_id' => 2,
                'name' => 'Odesa Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 2,
                'lang_id' => 2,
                'name' => 'Dnipro Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 3,
                'lang_id' => 2,
                'name' => 'Chernihiv Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 4,
                'lang_id' => 2,
                'name' => 'Kharkiv Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 5,
                'lang_id' => 2,
                'name' => 'Zhytomyr Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 6,
                'lang_id' => 2,
                'name' => 'Poltava Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 7,
                'lang_id' => 2,
                'name' => 'Kherson Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 8,
                'lang_id' => 2,
                'name' => 'Kyiv Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 9,
                'lang_id' => 2,
                'name' => 'Zaporizhzhia Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 10,
                'lang_id' => 2,
                'name' => 'Lugansk Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 11,
                'lang_id' => 2,
                'name' => 'Donetsk Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 12,
                'lang_id' => 2,
                'name' => 'Vinnytsya Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 13,
                'lang_id' => 2,
                'name' => 'Krym Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 14,
                'lang_id' => 2,
                'name' => 'Kirovograd Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 15,
                'lang_id' => 2,
                'name' => 'Mykolaiv Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 16,
                'lang_id' => 2,
                'name' => 'Sumy Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 17,
                'lang_id' => 2,
                'name' => 'Lviv Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 18,
                'lang_id' => 2,
                'name' => 'Cherkasy Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 19,
                'lang_id' => 2,
                'name' => 'Khmelnytskyi Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 20,
                'lang_id' => 2,
                'name' => 'Volyn Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 21,
                'lang_id' => 2,
                'name' => 'Rivne Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 22,
                'lang_id' => 2,
                'name' => 'Ivano-frankivsk Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 23,
                'lang_id' => 2,
                'name' => 'Ternopil Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 24,
                'lang_id' => 2,
                'name' => 'Zakarpattja Region',
            ]);
            DB::table('regions_translate')->insert([
                'region_id' => 25,
                'lang_id' => 2,
                'name' => 'Chernivtsi Region',
            ]);

            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
