<?php

use Illuminate\Database\Seeder;

/**
 * Class ReviewsTableSeeder
 *
 * php artisan db:seed --class=ReviewsTableSeeder
 */
class ReviewsTableSeeder extends Seeder
{

    public function run()
    {
        try {

            DB::beginTransaction();

            // Контент
            //DB::table('reviews')->insert($this->generateContents()['contents']);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function generateContents()
    {
        $result = [];
        $result['contents'] = [];

        for($i = 1; $i <= 23; $i++) {
            $result['contents'][] = [
                'user_id' => 1,
                'confirmed' => rand(0, 1),
                'text' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        return $result;
    }
}
