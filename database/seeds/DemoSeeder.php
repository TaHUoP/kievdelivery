<?php

use Illuminate\Database\Seeder;
use App\Models\Content;

/**
 * Demo Seeder
 * php artisan db:seed --class=DemoSeeder
 */
class DemoSeeder extends Seeder
{
    public function run()
    {
        try {

            DB::beginTransaction();

            // Контент
            //DB::table('contents')->insert($this->generateContents()['contents']);
            //DB::table('contents_translate')->insert($this->generateContents()['translations']);

            // --------------------------------------------

            // Логи
            //DB::table('log')->insert($this->generateLogs());

            // --------------------------------------------

            // Виджеты
            //DB::table('widgets')->insert($this->generateWidgets());

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Логи
     * @return array
     */
    public function generateLogs()
    {
        $result = [];
        for($i = 1; $i <= 100; $i++) {
            $result[] = [
                'level' => 1,
                'category' => 'application',
                'created_at' => date('Y-m-d H:i:s'),
                'message' => uniqid(true),
            ];
        }
        return $result;
    }

    /**
     * Контент
     * @return array
     */
    public function generateContents()
    {
        $result = [];
        $result['contents'] = [];
        $result['translations'] = [];

        $types = [Content::TYPE_PAGE, Content::TYPE_NEWS];

        for($i = 1; $i <= 100; $i++) {
            $result['contents'][] = [
                'id' => $i,
                'url' => 'info-' . $i,
                'type' => $types[array_rand($types)],
                'visible' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $result['translations'][] = [
                'content_id' => $i,
                'lang_id' => 1,
                'title' => 'Информация - ' . $i,
                'meta_title' => 'Информация - ' . $i,
                'meta_description' => 'Информация - ' . $i,
                'meta_keywords' => 'Информация - ' . $i,
                'text' => 'Информация - ' . $i,
            ];
            $result['translations'][] = [
                'content_id' => $i,
                'lang_id' => 2,
                'title' => 'Info - ' . $i,
                'meta_title' => 'Info - ' . $i,
                'meta_description' => 'Info - ' . $i,
                'meta_keywords' => 'Info - ' . $i,
                'text' => 'Info - ' . $i,
            ];
        }
        return $result;
    }

    /**
     * Виджеты
     * @return array
     */
    public function generateWidgets()
    {
        $result = [];
        for($i = 1; $i <= 100; $i++) {
            $result[] = [
                'key' => uniqid('true'),
                'lang_id' => 1,
                'visible' => 1,
                'description' => 'Описание - ' . $i,
                'value' => 'Значение - ' . $i,
            ];
            $result[] = [
                'key' => uniqid('true'),
                'lang_id' => 2,
                'visible' => 1,
                'description' => 'Description - ' . $i,
                'value' => 'Value - ' . $i,
            ];
        }
        return $result;
    }

}