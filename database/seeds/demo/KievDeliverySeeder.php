<?php

use Illuminate\Database\Seeder;

/**
 * Kiev Delivery Seeder
 * php artisan db:seed --class=KievDeliverySeeder
 */
class KievDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws Exception
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            /***************************************
             * Категории
             ***************************************/
            DB::table('shop_categories')->insert([
                ['id' => 1, 'alias' => 'root', '_lft' => 1, '_rgt' => 48, 'parent_id' => null, 'visible' => 1],
                ['id' => 2, 'alias' => 'flowers', '_lft' => 2, '_rgt' => 11, 'parent_id' => 1, 'visible' => 1],
                ['id' => 3, 'alias' => 'occasion', '_lft' => 12, '_rgt' => 29, 'parent_id' => 1, 'visible' => 1],
                ['id' => 4, 'alias' => 'wow', '_lft' => 30, '_rgt' => 31, 'parent_id' => 1, 'visible' => 1],
                ['id' => 5, 'alias' => 'gifts-and-foods', '_lft' => 32, '_rgt' => 45, 'parent_id' => 1, 'visible' => 1],
                ['id' => 6, 'alias' => 'other', '_lft' => 46, '_rgt' => 47, 'parent_id' => 1, 'visible' => 1],
                ['id' => 7, 'alias' => 'best-sellers', '_lft' => 3, '_rgt' => 4, 'parent_id' => 2, 'visible' => 1],
                ['id' => 8, 'alias' => 'roses', '_lft' => 5, '_rgt' => 6, 'parent_id' => 2, 'visible' => 1],
                ['id' => 9, 'alias' => 'tulips', '_lft' => 7, '_rgt' => 8, 'parent_id' => 2, 'visible' => 1],
                ['id' => 10, 'alias' => 'new-arrivals', '_lft' => 9, '_rgt' => 10, 'parent_id' => 2, 'visible' => 1],
                ['id' => 11, 'alias' => 'love-and-romance', '_lft' => 13, '_rgt' => 14, 'parent_id' => 3, 'visible' => 1],
                ['id' => 12, 'alias' => 'birthday', '_lft' => 15, '_rgt' => 16, 'parent_id' => 3, 'visible' => 1],
                ['id' => 13, 'alias' => 'holiday', '_lft' => 17, '_rgt' => 18, 'parent_id' => 3, 'visible' => 1],
                ['id' => 14, 'alias' => 'miss-you', '_lft' => 19, '_rgt' => 20, 'parent_id' => 3, 'visible' => 1],
                ['id' => 15, 'alias' => 'corporate', '_lft' => 21, '_rgt' => 22, 'parent_id' => 3, 'visible' => 1],
                ['id' => 16, 'alias' => 'get-well', '_lft' => 23, '_rgt' => 24, 'parent_id' => 3, 'visible' => 1],
                ['id' => 17, 'alias' => 'i-am-sorry', '_lft' => 25, '_rgt' => 26, 'parent_id' => 3, 'visible' => 1],
                ['id' => 18, 'alias' => 'sympathy-and-funerals', '_lft' => 27, '_rgt' => 28, 'parent_id' => 3, 'visible' => 1],
                ['id' => 19, 'alias' => 'baskets-and-sets', '_lft' => 33, '_rgt' => 34, 'parent_id' => 5, 'visible' => 1],
                ['id' => 20, 'alias' => 'stuffed-toys', '_lft' => 35, '_rgt' => 36, 'parent_id' => 5, 'visible' => 1],
                ['id' => 21, 'alias' => 'sweets-and-foods', '_lft' => 37, '_rgt' => 38, 'parent_id' => 5, 'visible' => 1],
                ['id' => 22, 'alias' => 'perfumes-and-skin-care', '_lft' => 39, '_rgt' => 40, 'parent_id' => 5, 'visible' => 1],
                ['id' => 23, 'alias' => 'exclusive-and-personal', '_lft' => 41, '_rgt' => 42, 'parent_id' => 5, 'visible' => 1],
                ['id' => 24, 'alias' => 'electronics', '_lft' => 43, '_rgt' => 44, 'parent_id' => 5, 'visible' => 1],
            ]);


            DB::table('shop_categories_translate')->insert([
                ['lang_id' => 1, 'category_id' => 1, 'name' => 'Root'],
                ['lang_id' => 1, 'category_id' => 2, 'name' => 'Цветы'],
                ['lang_id' => 1, 'category_id' => 3, 'name' => 'Повод'],
                ['lang_id' => 1, 'category_id' => 4, 'name' => 'WOW'],
                ['lang_id' => 1, 'category_id' => 5, 'name' => 'Подарки и наборы'],
                ['lang_id' => 1, 'category_id' => 6, 'name' => 'Другое'],
                ['lang_id' => 1, 'category_id' => 7, 'name' => 'Лучшие продажи'],
                ['lang_id' => 1, 'category_id' => 8, 'name' => 'Розы'],
                ['lang_id' => 1, 'category_id' => 9, 'name' => 'Тюльпаны'],
                ['lang_id' => 1, 'category_id' => 10, 'name' => 'Новинки'],
                ['lang_id' => 1, 'category_id' => 11, 'name' => 'Любовь и романы'],
                ['lang_id' => 1, 'category_id' => 12, 'name' => 'День рождения'],
                ['lang_id' => 1, 'category_id' => 13, 'name' => 'Праздники'],
                ['lang_id' => 1, 'category_id' => 14, 'name' => 'Скучаю по тебе'],
                ['lang_id' => 1, 'category_id' => 15, 'name' => 'Корпоративные'],
                ['lang_id' => 1, 'category_id' => 16, 'name' => 'Выздоравливай'],
                ['lang_id' => 1, 'category_id' => 17, 'name' => 'Извинения'],
                ['lang_id' => 1, 'category_id' => 18, 'name' => 'Сочувствие и похорны'],
                ['lang_id' => 1, 'category_id' => 19, 'name' => 'Корзины и наборы'],
                ['lang_id' => 1, 'category_id' => 20, 'name' => 'Мягкие игрушки'],
                ['lang_id' => 1, 'category_id' => 21, 'name' => 'Сладости и продукты'],
                ['lang_id' => 1, 'category_id' => 22, 'name' => 'Духи и уход за кожей'],
                ['lang_id' => 1, 'category_id' => 23, 'name' => 'Эксклюзив'],
                ['lang_id' => 1, 'category_id' => 24, 'name' => 'Электроника'],
            ]);


            /***************************************
             * Товары
             ***************************************/
            DB::table('shop_products')->insert([
                [
                    'id' => 1,
                    'alias' => 'product_1',
                    'vendor_code' => 1,
                    'amount' => 7,
                    'price' => 17,
                    'price_old' => 20,
                    'visible' => 1,
                ],
                [
                    'id' => 2,
                    'alias' => 'product_2',
                    'vendor_code' => 2,
                    'amount' => 7,
                    'price' => 45,
                    'price_old' => 47,
                    'visible' => 1,
                ],
                [
                    'id' => 3,
                    'alias' => 'product_3',
                    'vendor_code' => 3,
                    'amount' => 7,
                    'price' => 123,
                    'price_old' => 125,
                    'visible' => 1,
                ],
                [
                    'id' => 4,
                    'alias' => 'product_4',
                    'vendor_code' => 4,
                    'amount' => 7,
                    'price' => 2,
                    'price_old' => 5,
                    'visible' => 1,
                ],
                [
                    'id' => 5,
                    'alias' => 'product_5',
                    'vendor_code' => 5,
                    'amount' => 7,
                    'price' => 56,
                    'price_old' => 57,
                    'visible' => 1,
                ],
                [
                    'id' => 6,
                    'alias' => 'product_6',
                    'vendor_code' => 6,
                    'amount' => 7,
                    'price' => 24,
                    'price_old' => 25,
                    'visible' => 1,
                ],
                [
                    'id' => 7,
                    'alias' => 'product_7',
                    'vendor_code' => 7,
                    'amount' => 7,
                    'price' => 78,
                    'price_old' => 80,
                    'visible' => 1,
                ],
                [
                    'id' => 8,
                    'alias' => 'product_8',
                    'vendor_code' => 8,
                    'amount' => 7,
                    'price' => 25,
                    'price_old' => 27,
                    'visible' => 1,
                ],
            ]);


            DB::table('shop_products_translate')->insert([
                [
                    'id' => 1,
                    'lang_id' => 1,
                    'product_id' => 1,
                    'name' => 'Продукт 1',
                ],
                [
                    'id' => 2,
                    'lang_id' => 1,
                    'product_id' => 2,
                    'name' => 'Продукт 2',
                ],
                [
                    'id' => 3,
                    'lang_id' => 1,
                    'product_id' => 3,
                    'name' => 'Продукт 3',
                ],
                [
                    'id' => 4,
                    'lang_id' => 1,
                    'product_id' => 4,
                    'name' => 'Продукт 4',
                ],
                [
                    'id' => 5,
                    'lang_id' => 1,
                    'product_id' => 5,
                    'name' => 'Продукт 5',
                ],
                [
                    'id' => 6,
                    'lang_id' => 1,
                    'product_id' => 6,
                    'name' => 'Продукт 6',
                ],
                [
                    'id' => 7,
                    'lang_id' => 1,
                    'product_id' => 7,
                    'name' => 'Продукт 7',
                ],
                [
                    'id' => 8,
                    'lang_id' => 1,
                    'product_id' => 8,
                    'name' => 'Продукт 8',
                ],
            ]);

            DB::table('shop_product_images')->insert([
                ['product_id' => 1, 'preview_url' => 'item-1.jpg', 'default' => 1],
                ['product_id' => 2, 'preview_url' => 'item-2.jpg', 'default' => 1],
                ['product_id' => 3, 'preview_url' => 'item-3.jpg', 'default' => 1],
                ['product_id' => 4, 'preview_url' => 'item-4.jpg', 'default' => 1],
                ['product_id' => 5, 'preview_url' => 'item-1.jpg', 'default' => 1],
                ['product_id' => 6, 'preview_url' => 'item-2.jpg', 'default' => 1],
                ['product_id' => 7, 'preview_url' => 'item-3.jpg', 'default' => 1],
                ['product_id' => 8, 'preview_url' => 'item-4.jpg', 'default' => 1],
            ]);


            /***************************************
             * Свойства
             ***************************************/
            DB::table('shop_properties')->insert([
                [
                    'id' => 2,
                    'alias' => 'weight',
                    'type' => 'select',
                ],
                [
                    'id' => 3,
                    'alias' => 'color',
                    'type' => 'select',
                ],
            ]);

            DB::table('shop_properties_translate')->insert([
                [
                    'id' => 2,
                    'lang_id' => 1,
                    'property_id' => 2,
                    'name' => 'Вес',
                ],
                [
                    'id' => 3,
                    'lang_id' => 1,
                    'property_id' => 3,
                    'name' => 'Цвет',
                ],
            ]);


            /***************************************
             * Значения
             ***************************************/
            DB::table('shop_values')->insert([
                [
                    'id' => 4,
                    'alias' => 'to_one',
                ],
                [
                    'id' => 5,
                    'alias' => 'to_five',
                ],
                [
                    'id' => 6,
                    'alias' => 'five_more',
                ],
                [
                    'id' => 7,
                    'alias' => 'red',
                ],
                [
                    'id' => 8,
                    'alias' => 'blue',
                ],
                [
                    'id' => 9,
                    'alias' => 'white',
                ],
            ]);

            DB::table('shop_values_translate')->insert([
                [
                    'id' => 4,
                    'lang_id' => 1,
                    'value_id' => 4,
                    'name' => 'До одного кг',
                ],
                [
                    'id' => 5,
                    'lang_id' => 1,
                    'value_id' => 5,
                    'name' => 'До пяти кг',
                ],
                [
                    'id' => 6,
                    'lang_id' => 1,
                    'value_id' => 6,
                    'name' => '5 кг и более',
                ],
                [
                    'id' => 7,
                    'lang_id' => 1,
                    'value_id' => 7,
                    'name' => 'Красный',
                ],
                [
                    'id' => 8,
                    'lang_id' => 1,
                    'value_id' => 8,
                    'name' => 'Синий',
                ],
                [
                    'id' => 9,
                    'lang_id' => 1,
                    'value_id' => 9,
                    'name' => 'Белый',
                ],
            ]);


            DB::table('shop_products_properties_values')->insert([
                [
                    'product_id' => 1,
                    'property_id' => 2,
                    'value_id' => 4,
                    'custom_value' => null,
                ],
                [
                    'product_id' => 1,
                    'property_id' => 3,
                    'value_id' => 7,
                    'custom_value' => null,
                ],

                [
                    'product_id' => 2,
                    'property_id' => 3,
                    'value_id' => 8,
                    'custom_value' => null,
                ],

                [
                    'product_id' => 3,
                    'property_id' => 3,
                    'value_id' => 9,
                    'custom_value' => null,
                ],

                [
                    'product_id' => 4,
                    'property_id' => 3,
                    'value_id' => 8,
                    'custom_value' => null,
                ],

                [
                    'product_id' => 5,
                    'property_id' => 3,
                    'value_id' => 7,
                    'custom_value' => null,
                ],

                [
                    'product_id' => 6,
                    'property_id' => 3,
                    'value_id' => 9,
                    'custom_value' => null,
                ],

                [
                    'product_id' => 7,
                    'property_id' => 3,
                    'value_id' => 8,
                    'custom_value' => null,
                ],
            ]);


            /***************************************
             * Связи
             ***************************************/
            DB::table('shop_properties_values')->insert([
                ['property_id' => 2, 'value_id' => 4,],
                ['property_id' => 2, 'value_id' => 5,],
                ['property_id' => 2, 'value_id' => 6,],

                ['property_id' => 3, 'value_id' => 7,],
                ['property_id' => 3, 'value_id' => 8,],
                ['property_id' => 3, 'value_id' => 9,],
            ]);

            DB::table('shop_categories_properties')->insert([
                ['category_id' => 1, 'property_id' => 2],
                ['category_id' => 1, 'property_id' => 3],

                ['category_id' => 2, 'property_id' => 2],
                ['category_id' => 2, 'property_id' => 3],

                ['category_id' => 3, 'property_id' => 2],
                ['category_id' => 3, 'property_id' => 3],

                ['category_id' => 4, 'property_id' => 2],
                ['category_id' => 4, 'property_id' => 3],

                ['category_id' => 5, 'property_id' => 2],
                ['category_id' => 5, 'property_id' => 3],
            ]);

            DB::table('shop_product_categories')->insert([

                ['product_id' => 2, 'category_id' => 1],
                ['product_id' => 2, 'category_id' => 2],
                ['product_id' => 2, 'category_id' => 8],

                ['product_id' => 3, 'category_id' => 1],
                ['product_id' => 3, 'category_id' => 2],
                ['product_id' => 3, 'category_id' => 8],

                ['product_id' => 4, 'category_id' => 1],
                ['product_id' => 4, 'category_id' => 2],
                ['product_id' => 4, 'category_id' => 9],

                ['product_id' => 5, 'category_id' => 1],
                ['product_id' => 5, 'category_id' => 2],
                ['product_id' => 5, 'category_id' => 10],

                ['product_id' => 6, 'category_id' => 1],
                ['product_id' => 6, 'category_id' => 4],

                ['product_id' => 7, 'category_id' => 1],
                ['product_id' => 7, 'category_id' => 4],

                ['product_id' => 8, 'category_id' => 1],
                ['product_id' => 8, 'category_id' => 6],

            ]);




            DB::table('contents')->insert([
                [
                    'id' => 10000,
                    'url' => 'about',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10001,
                    'url' => 'delivery-policy',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10002,
                    'url' => 'faq',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10003,
                    'url' => 'return',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10004,
                    'url' => 'agreement',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10005,
                    'url' => 'delivery-cities',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10006,
                    'url' => 'guide',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10007,
                    'url' => 'guarantees',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10008,
                    'url' => 'affiliate',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10009,
                    'url' => 'service',
                    'type' => 'page',
                    'visible' => 1,
                ],
                [
                    'id' => 10010,
                    'url' => 'secure-payment',
                    'type' => 'page',
                    'visible' => 1,
                ],
            ]);











            DB::table('contents_translate')->insert([
                [
                    'content_id' => 10000,
                    'lang_id' => 1,
                    'title' => 'О нас',
                    'meta_title' => 'О нас',
                    'meta_description' => 'О нас',
                    'meta_keywords' => 'О нас',
                    'text' => 'О нас',
                ],
                [
                    'content_id' => 10001,
                    'lang_id' => 1,
                    'title' => 'Политика доставки',
                    'meta_title' => 'Политика доставки',
                    'meta_description' => 'Политика доставки',
                    'meta_keywords' => 'Политика доставки',
                    'text' => 'Политика доставки',
                ],
                [
                    'content_id' => 10002,
                    'lang_id' => 1,
                    'title' => 'ЧАВо',
                    'meta_title' => 'ЧАВо',
                    'meta_description' => 'ЧАВо',
                    'meta_keywords' => 'ЧАВо',
                    'text' => 'ЧАВо',
                ],
                [
                    'content_id' => 10003,
                    'lang_id' => 1,
                    'title' => 'Замена и обмен',
                    'meta_title' => 'Замена и обмен',
                    'meta_description' => 'Замена и обмен',
                    'meta_keywords' => 'Замена и обмен',
                    'text' => 'Замена и обмен',
                ],
                [
                    'content_id' => 10004,
                    'lang_id' => 1,
                    'title' => 'Условия использования',
                    'meta_title' => 'Условия использования',
                    'meta_description' => 'Условия использования',
                    'meta_keywords' => 'Условия использования',
                    'text' => 'Условия использования',
                ],
                [
                    'content_id' => 10005,
                    'lang_id' => 1,
                    'title' => 'Города доставок',
                    'meta_title' => 'Города доставок',
                    'meta_description' => 'Города доставок',
                    'meta_keywords' => 'Города доставок',
                    'text' => 'Города доставок',
                ],
                [
                    'content_id' => 10006,
                    'lang_id' => 1,
                    'title' => 'Гид по Цветам и Уходу',
                    'meta_title' => 'Гид по Цветам и Уходу',
                    'meta_description' => 'Гид по Цветам и Уходу',
                    'meta_keywords' => 'Гид по Цветам и Уходу',
                    'text' => 'Гид по Цветам и Уходу',
                ],
                [
                    'content_id' => 10007,
                    'lang_id' => 1,
                    'title' => 'Наши Гарантии',
                    'meta_title' => 'Наши Гарантии',
                    'meta_description' => 'Наши Гарантии',
                    'meta_keywords' => 'Наши Гарантии',
                    'text' => 'Наши Гарантии',
                ],
                [
                    'content_id' => 10008,
                    'lang_id' => 1,
                    'title' => 'Партнерская Программа',
                    'meta_title' => 'Партнерская Программа',
                    'meta_description' => 'Партнерская Программа',
                    'meta_keywords' => 'Партнерская Программа',
                    'text' => 'Партнерская Программа',
                ],
                [
                    'content_id' => 10009,
                    'lang_id' => 1,
                    'title' => 'Сервис и Качество',
                    'meta_title' => 'Сервис и Качество',
                    'meta_description' => 'Сервис и Качество',
                    'meta_keywords' => 'Сервис и Качество',
                    'text' => 'Сервис и Качество',
                ],
                [
                    'content_id' => 10010,
                    'lang_id' => 1,
                    'title' => 'Безопасная оплата',
                    'meta_title' => 'Безопасная оплата',
                    'meta_description' => 'Безопасная оплата',
                    'meta_keywords' => 'Безопасная оплата',
                    'text' => 'Безопасная оплата',
                ],
            ]);

            /***************************************
             * Купоны
             ***************************************/
            DB::table('shop_coupons')->insert([
                ['id' => 1, 'name' => 'Купон на -10%','code' => 'onetwo', 'percentage' => 10,
                    'ttl' => 60,'quantity'=>100, 'date_expiry' => '2016-06-30 16:47:05.0',
                    'created_at' => '2016-06-28 16:47:12', 'updated_at' => '2016-06-28 16:47:12'],
                ['id' => 2, 'name' => 'Купон на -20%','code' => '123', 'percentage' => 20,
                    'ttl' => 75,'quantity'=>88, 'date_expiry' => '2016-06-30 16:47:05.0',
                    'created_at' => '2016-06-28 16:47:12', 'updated_at' => '2016-06-28 16:47:12'],
                ['id' => 3, 'name' => 'Купон на -30%','code' => 'triz', 'percentage' => 30,
                    'ttl' => 20,'quantity'=>25, 'date_expiry' => '2016-06-30 16:47:05.0',
                    'created_at' => '2016-06-28 16:47:12', 'updated_at' => '2016-06-28 16:47:12'],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
