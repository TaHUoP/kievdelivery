<?php

use Illuminate\Database\Seeder;

/**
 * Shop Seeder
 * php artisan db:seed --class=ShopSeeder
 */
class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws Exception
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            /***************************************
             * Категории
             ***************************************/
            DB::table('shop_categories')->insert([
                [
                    'id' => 1,
                    'alias' => 'flowers',
                ],
                [
                    'id' => 2,
                    'alias' => 'auto',
                ],
                [
                    'id' => 3,
                    'alias' => 'furniture',
                ]
            ]);


            DB::table('shop_categories_translate')->insert([
                [
                    'id' => 1,
                    'lang_id' => 1,
                    'category_id' => 1,
                    'name' => 'Цветы',
                ],
                [
                    'id' => 2,
                    'lang_id' => 1,
                    'category_id' => 2,
                    'name' => 'Автомобили',
                ],
                [
                    'id' => 3,
                    'lang_id' => 1,
                    'category_id' => 3,
                    'name' => 'Мебель',
                ],
            ]);


            /***************************************
             * Товары
             ***************************************/
            DB::table('shop_products')->insert([
                [
                    'id' => 1,
                    'alias' => 'vaz_2101',
                    'vendor_code' => 1,
                    'amount' => 1
                ],
                [
                    'id' => 2,
                    'alias' => 'roses',
                    'vendor_code' => 2,
                    'amount' => 7
                ],
                [
                    'id' => 3,
                    'alias' => 'desk',
                    'vendor_code' => 3,
                    'amount' => 7
                ],
            ]);


            DB::table('shop_products_translate')->insert([
                [
                    'id' => 1,
                    'lang_id' => 1,
                    'product_id' => 1,
                    'name' => 'Ваз 2101',
                ],
                [
                    'id' => 2,
                    'lang_id' => 1,
                    'product_id' => 2,
                    'name' => 'Розы',
                ],
                [
                    'id' => 3,
                    'lang_id' => 1,
                    'product_id' => 3,
                    'name' => 'Стол',
                ],
            ]);


            /***************************************
             * Свойства
             ***************************************/
            DB::table('shop_properties')->insert([
                [
                    'id' => 1,
                    'alias' => 'color',
                    'type' => 'select',
                ],
                [
                    'id' => 2,
                    'alias' => 'weight',
                    'type' => 'select',
                ],
                [
                    'id' => 3,
                    'alias' => 'speed',
                    'type' => 'select',
                ],
                [
                    'id' => 4,
                    'alias' => 'type',
                    'type' => 'select',
                ]
            ]);

            DB::table('shop_properties_translate')->insert([
                [
                    'id' => 1,
                    'lang_id' => 1,
                    'property_id' => 1,
                    'name' => 'Цвет',
                ],
                [
                    'id' => 2,
                    'lang_id' => 1,
                    'property_id' => 2,
                    'name' => 'Вес',
                ],
                [
                    'id' => 3,
                    'lang_id' => 1,
                    'property_id' => 3,
                    'name' => 'Скорость',
                ],
                [
                    'id' => 4,
                    'lang_id' => 1,
                    'property_id' => 4,
                    'name' => 'Тип',
                ]
            ]);


            /***************************************
             * Значения
             ***************************************/
            DB::table('shop_values')->insert([
                [
                    'id' => 1,
                    'alias' => 'red',
                ],
                [
                    'id' => 2,
                    'alias' => 'blue',
                ],
                [
                    'id' => 3,
                    'alias' => '1kg-5kg',
                ],
                [
                    'id' => 4,
                    'alias' => '5_plus',
                ],
                [
                    'id' => 5,
                    'alias' => '1-100_km_v_chas',
                ],
                [
                    'id' => 6,
                    'alias' => '100-200_km_v_chas',
                ],
                [
                    'id' => 7,
                    'alias' => 'tree',
                ],
                [
                    'id' => 8,
                    'alias' => 'metal',
                ],
            ]);

            DB::table('shop_values_translate')->insert([
                [
                    'id' => 1,
                    'lang_id' => 1,
                    'value_id' => 1,
                    'name' => 'Красный',
                ],
                [
                    'id' => 2,
                    'lang_id' => 1,
                    'value_id' => 2,
                    'name' => 'Синий',
                ],
                [
                    'id' => 3,
                    'lang_id' => 1,
                    'value_id' => 3,
                    'name' => '1кг - 5кг',
                ],
                [
                    'id' => 4,
                    'lang_id' => 1,
                    'value_id' => 4,
                    'name' => '5 и более',
                ],
                [
                    'id' => 5,
                    'lang_id' => 1,
                    'value_id' => 5,
                    'name' => '1км - 100км в час',
                ],
                [
                    'id' => 6,
                    'lang_id' => 1,
                    'value_id' => 6,
                    'name' => '100км - 200км в час',
                ],
                [
                    'id' => 7,
                    'lang_id' => 1,
                    'value_id' => 7,
                    'name' => 'Дерево',
                ],
                [
                    'id' => 8,
                    'lang_id' => 1,
                    'value_id' => 8,
                    'name' => 'Металл',
                ],
            ]);


            DB::table('shop_products_properties_values')->insert([
                [
                    'product_id' => 1,
                    'property_id' => 1,
                    'value_id' => 2,
                    'custom_value' => null,
                ],
                [
                    'product_id' => 1,
                    'property_id' => 2,
                    'value_id' => 4,
                    'custom_value' => null,
                ],
                [
                    'product_id' => 1,
                    'property_id' => 3,
                    'value_id' => null,
                    'custom_value' => ' + 100500 км в час',
                ],
            ]);


            /***************************************
             * Связи
             ***************************************/
            DB::table('shop_properties_values')->insert([
                ['property_id' => 1, 'value_id' => 1,],
                ['property_id' => 1, 'value_id' => 2,],

                ['property_id' => 2, 'value_id' => 3,],
                ['property_id' => 2, 'value_id' => 4,],

                ['property_id' => 3, 'value_id' => 5,],
                ['property_id' => 3, 'value_id' => 6,],

                ['property_id' => 4, 'value_id' => 7,],
                ['property_id' => 4, 'value_id' => 8,],
            ]);

            DB::table('shop_categories_properties')->insert([
                ['category_id' => 1, 'property_id' => 1],
                ['category_id' => 1, 'property_id' => 2],

                ['category_id' => 2, 'property_id' => 1],
                ['category_id' => 2, 'property_id' => 2],
                ['category_id' => 2, 'property_id' => 3],

                ['category_id' => 3, 'property_id' => 1],
                ['category_id' => 3, 'property_id' => 2],
                ['category_id' => 3, 'property_id' => 4],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
