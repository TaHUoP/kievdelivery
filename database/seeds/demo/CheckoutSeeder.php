<?php

use Illuminate\Database\Seeder;

/**
 * Items Seeder
 * php artisan db:seed --class=CheckoutSeeder
 */
class CheckoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws Exception
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('shop_cart')->insert([
                [
                    'id' => 1,
                    'hash' => md5(uniqid(true)),
                    'user_id' => 1,
                    'processed' => 1,
                    'total_price' => 0,
                    'discount' => null,
                    'discount_code' => null,
                ],
                [
                    'id' => 2,
                    'hash' => md5(uniqid(true)),
                    'user_id' => 2,
                    'processed' => 1,
                    'total_price' => 0,
                    'discount' => null,
                    'discount_code' => null,
                ],
                [
                    'id' => 3,
                    'hash' => md5(uniqid(true)),
                    'user_id' => 3,
                    'processed' => 1,
                    'total_price' => 0,
                    'discount' => null,
                    'discount_code' => null,
                ],
                [
                    'id' => 4,
                    'hash' => md5(uniqid(true)),
                    'user_id' => 4,
                    'processed' => 1,
                    'total_price' => 0,
                    'discount' => null,
                    'discount_code' => null,
                ],
                [
                    'id' => 5,
                    'hash' => md5(uniqid(true)),
                    'user_id' => 5,
                    'processed' => 1,
                    'total_price' => 0,
                    'discount' => null,
                    'discount_code' => null,
                ],
            ]);

            DB::table('shop_checkout')->insert([
                [
                    'id' => 1,
                    'cart_id' => 1,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'email' => 'mail@mail.ru',
                    'address' => null,
                    'phone' => null,
                    'status' => 0,
                    'paid' => 0,
                    'payment_system' => 'cach',
                    'payment_data' => null,
                ],
                [
                    'id' => 2,
                    'cart_id' => 2,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'email' => 'mail@mail.ru',
                    'address' => null,
                    'phone' => null,
                    'status' => 0,
                    'paid' => 0,
                    'payment_system' => 'cach',
                    'payment_data' => null,
                ],
                [
                    'id' => 3,
                    'cart_id' => 3,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'email' => 'mail@mail.ru',
                    'address' => null,
                    'phone' => null,
                    'status' => 0,
                    'paid' => 0,
                    'payment_system' => 'cach',
                    'payment_data' => null,
                ],
                [
                    'id' => 4,
                    'cart_id' => 4,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'email' => 'mail@mail.ru',
                    'address' => null,
                    'phone' => null,
                    'status' => 0,
                    'paid' => 0,
                    'payment_system' => 'cach',
                    'payment_data' => null,
                ],
                [
                    'id' => 5,
                    'cart_id' => 5,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'email' => 'mail@mail.ru',
                    'address' => null,
                    'phone' => null,
                    'status' => 0,
                    'paid' => 0,
                    'payment_system' => 'cach',
                    'payment_data' => null,
                ],
            ]);

            DB::table('shop_checkout_delivery')->insert([
                [
                    'id' => 1,
                    'checkout_id' => 1,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'address' => 'Адрес',
                    'phone' => 'Телефон',
                    'country' => 'Страна',
                    'city' => 'Город',
                    'date' => null,
                    'comment_recipient' => null,
                    'comment_note' => null,
                    'shipment' => null,
                ],
                [
                    'id' => 2,
                    'checkout_id' => 2,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'address' => 'Адрес',
                    'phone' => 'Телефон',
                    'country' => 'Страна',
                    'city' => 'Город',
                    'date' => null,
                    'comment_recipient' => null,
                    'comment_note' => null,
                    'shipment' => null,
                ],
                [
                    'id' => 3,
                    'checkout_id' => 3,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'address' => 'Адрес',
                    'phone' => 'Телефон',
                    'country' => 'Страна',
                    'city' => 'Город',
                    'date' => null,
                    'comment_recipient' => null,
                    'comment_note' => null,
                    'shipment' => null,
                ],
                [
                    'id' => 4,
                    'checkout_id' => 4,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'address' => 'Адрес',
                    'phone' => 'Телефон',
                    'country' => 'Страна',
                    'city' => 'Город',
                    'date' => null,
                    'comment_recipient' => null,
                    'comment_note' => null,
                    'shipment' => null,
                ],
                [
                    'id' => 5,
                    'checkout_id' => 5,
                    'first_name' => 'Имя',
                    'last_name' => 'Фамилия',
                    'address' => 'Адрес',
                    'phone' => 'Телефон',
                    'country' => 'Страна',
                    'city' => 'Город',
                    'date' => null,
                    'comment_recipient' => null,
                    'comment_note' => null,
                    'shipment' => null,
                ],

            ]);

            DB::table('shop_cart_products')->insert([
                [
                    'id' => 1,
                    'cart_id' => 1,
                    'product_id' => 1,
                    'amount' => 1,
                ],
                [
                    'id' => 2,
                    'cart_id' => 2,
                    'product_id' => 2,
                    'amount' => 1,
                ],
                [
                    'id' => 3,
                    'cart_id' => 3,
                    'product_id' => 3,
                    'amount' => 1,
                ],
                [
                    'id' => 4,
                    'cart_id' => 4,
                    'product_id' => 4,
                    'amount' => 1,
                ],
                [
                    'id' => 5,
                    'cart_id' => 5,
                    'product_id' => 5,
                    'amount' => 1,
                ],
            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
