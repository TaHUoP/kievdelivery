<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;

/**
 * Items Seeder
 * php artisan db:seed --class=CountrySeeder
 */
class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws Exception
     */
    public function run()
    {

        try {
            ob_start();
            include 'countries.sql';
            $sql = ob_get_contents();
            ob_end_clean();

            DB::connection()->getPdo()->exec($sql);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }
}
