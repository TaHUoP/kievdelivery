<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('regions')->truncate();
            DB::table('regions')->insert([
                'id' => 1,
                'alias' => 'odesa',
            ]);
            DB::table('regions')->insert([
                'id' => 2,
                'alias' => 'dnipro',
            ]);
            DB::table('regions')->insert([
                'id' => 3,
                'alias' => 'chernihiv',
            ]);
            DB::table('regions')->insert([
                'id' => 4,
                'alias' => 'kharkiv',
            ]);
            DB::table('regions')->insert([
                'id' => 5,
                'alias' => 'zhytomyr',
            ]);
            DB::table('regions')->insert([
                'id' => 6,
                'alias' => 'poltava',
            ]);
            DB::table('regions')->insert([
                'id' => 7,
                'alias' => 'kherson',
            ]);
            DB::table('regions')->insert([
                'id' => 8,
                'alias' => 'kyiv',
            ]);
            DB::table('regions')->insert([
                'id' => 9,
                'alias' => 'zaporizhzhia',
            ]);
            DB::table('regions')->insert([
                'id' => 10,
                'alias' => 'lugansk',
            ]);
            DB::table('regions')->insert([
                'id' => 11,
                'alias' => 'donetsk',
            ]);
            DB::table('regions')->insert([
                'id' => 12,
                'alias' => 'vinnytsya',
            ]);
            DB::table('regions')->insert([
                'id' => 13,
                'alias' => 'krym',
            ]);
            DB::table('regions')->insert([
                'id' => 14,
                'alias' => 'kirovograd',
            ]);
            DB::table('regions')->insert([
                'id' => 15,
                'alias' => 'mykolaiv',
            ]);
            DB::table('regions')->insert([
                'id' => 16,
                'alias' => 'sumy',
            ]);
            DB::table('regions')->insert([
                'id' => 17,
                'alias' => 'lviv',
            ]);
            DB::table('regions')->insert([
                'id' => 18,
                'alias' => 'cherkasy',
            ]);
            DB::table('regions')->insert([
                'id' => 19,
                'alias' => 'khmelnytskyi',
            ]);
            DB::table('regions')->insert([
                'id' => 20,
                'alias' => 'volyn',
            ]);
            DB::table('regions')->insert([
                'id' => 21,
                'alias' => 'rivne',
            ]);
            DB::table('regions')->insert([
                'id' => 22,
                'alias' => 'ivano-frankivsk',
            ]);
            DB::table('regions')->insert([
                'id' => 23,
                'alias' => 'ternopil',
            ]);
            DB::table('regions')->insert([
                'id' => 24,
                'alias' => 'zakarpattja',
            ]);
            DB::table('regions')->insert([
                'id' => 25,
                'alias' => 'chernivtsi',
            ]);
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
