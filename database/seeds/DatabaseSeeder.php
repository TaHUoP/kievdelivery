<?php

use Illuminate\Database\Seeder;

/**
 * Запуск всех сидеров
 *
 * Внимание:
 * В случае ошибки: Class DatabaseSeeder does not exist
 *
 * Запустить команду: composer dump-autoload
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws Exception
     */
    public function run()
    {
        try {

            DB::beginTransaction();

//            $this->call('LanguagesSeeder');
//            $this->call('CountrySeeder');
//            $this->call('UserSeeder');
//            $this->call('KievDeliverySeeder');
//            $this->call('PluginsSeeder');
//            $this->call('RegionSeeder');
//            $this->call('CitySeeder');
            $this->call('CityTranslationSeeder');
            $this->call('RegionTranslationSeeder');

            //$this->call('DemoSeeder');
            //$this->call('ShopSeeder');
            //$this->call('CheckoutSeeder');

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
