<?php

use Illuminate\Database\Seeder;
use App\Models\City;
use App\Models\Content;

class CityTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::transaction(function () {
        //     DB::statement('SET FOREIGN_KEY_CHECKS=0');
        //     DB::table('cities_translate')->truncate();

        //     // RU
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 1,
        //         'lang_id' => 1,
        //         'name' => 'Київ',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 2,
        //         'lang_id' => 1,
        //         'name' => 'Вінниця',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 3,
        //         'lang_id' => 1,
        //         'name' => 'Дніпро',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 4,
        //         'lang_id' => 1,
        //         'name' => 'Донецьк',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 5,
        //         'lang_id' => 1,
        //         'name' => 'Житомир',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 6,
        //         'lang_id' => 1,
        //         'name' => 'Запоріжжя',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 7,
        //         'lang_id' => 1,
        //         'name' => 'Івано-Франківськ',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 8,
        //         'lang_id' => 1,
        //         'name' => 'Кропивницький',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 9,
        //         'lang_id' => 1,
        //         'name' => 'Мариуполь',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 10,
        //         'lang_id' => 1,
        //         'name' => 'Луганськ',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 11,
        //         'lang_id' => 1,
        //         'name' => 'Луцьк',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 12,
        //         'lang_id' => 1,
        //         'name' => 'Львів',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 13,
        //         'lang_id' => 1,
        //         'name' => 'Миколаїв',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 14,
        //         'lang_id' => 1,
        //         'name' => 'Одеса',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 15,
        //         'lang_id' => 1,
        //         'name' => 'Полтава',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 16,
        //         'lang_id' => 1,
        //         'name' => 'Рівне',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 17,
        //         'lang_id' => 1,
        //         'name' => 'Суми',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 18,
        //         'lang_id' => 1,
        //         'name' => 'Севастопіль',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 19,
        //         'lang_id' => 1,
        //         'name' => 'Тернопіль',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 20,
        //         'lang_id' => 1,
        //         'name' => 'Харків',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 21,
        //         'lang_id' => 1,
        //         'name' => 'Херсон',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 22,
        //         'lang_id' => 1,
        //         'name' => 'Хмельницький',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 23,
        //         'lang_id' => 1,
        //         'name' => 'Черкаси',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 24,
        //         'lang_id' => 1,
        //         'name' => 'Чернігів',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 25,
        //         'lang_id' => 1,
        //         'name' => 'Чернівці',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 26,
        //         'lang_id' => 1,
        //         'name' => 'Ужгород',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 27,
        //         'lang_id' => 1,
        //         'name' => 'Сімферополь',
        //     ]);

        //     // EN
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 1,
        //         'lang_id' => 2,
        //         'name' => 'Kiev',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 2,
        //         'lang_id' => 2,
        //         'name' => 'Vinnytsia',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 3,
        //         'lang_id' => 2,
        //         'name' => 'Dnipro',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 4,
        //         'lang_id' => 2,
        //         'name' => 'Donetsk',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 5,
        //         'lang_id' => 2,
        //         'name' => 'Zhytomyr',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 6,
        //         'lang_id' => 2,
        //         'name' => 'Zaporizhia',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 7,
        //         'lang_id' => 2,
        //         'name' => 'Ivano-frankivsk',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 8,
        //         'lang_id' => 2,
        //         'name' => 'Kropyvnytsyi',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 9,
        //         'lang_id' => 2,
        //         'name' => 'Mariupil',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 10,
        //         'lang_id' => 2,
        //         'name' => 'Lugansk',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 11,
        //         'lang_id' => 2,
        //         'name' => 'Lutsk',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 12,
        //         'lang_id' => 2,
        //         'name' => 'Lviv',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 13,
        //         'lang_id' => 2,
        //         'name' => 'Mykolaiv',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 14,
        //         'lang_id' => 2,
        //         'name' => 'Odessa',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 15,
        //         'lang_id' => 2,
        //         'name' => 'Poltava',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 16,
        //         'lang_id' => 2,
        //         'name' => 'Rivne',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 17,
        //         'lang_id' => 2,
        //         'name' => 'Sumy',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 18,
        //         'lang_id' => 2,
        //         'name' => 'Sevastopil',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 19,
        //         'lang_id' => 2,
        //         'name' => 'Ternopil',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 20,
        //         'lang_id' => 2,
        //         'name' => 'Kharkiv',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 21,
        //         'lang_id' => 2,
        //         'name' => 'Kherson',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 22,
        //         'lang_id' => 2,
        //         'name' => 'Khmelnytskiy',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 23,
        //         'lang_id' => 2,
        //         'name' => 'Cherkasy',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 24,
        //         'lang_id' => 2,
        //         'name' => 'Chernihiv',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 25,
        //         'lang_id' => 2,
        //         'name' => 'Chernivtsi',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 26,
        //         'lang_id' => 2,
        //         'name' => 'Uzhgorod',
        //     ]);
        //     DB::table('cities_translate')->insert([
        //         'city_id' => 27,
        //         'lang_id' => 2,
        //         'name' => 'Simferopil',
        //     ]);
        //     DB::statement('SET FOREIGN_KEY_CHECKS=1');
        // });
        $cities = City::all();
        foreach ($cities as $city) {
            $content = Content::where('url', $city->alias)->first();

            if ($content) {
                $ru = $content->translations->keyBy('lang_id')->get('1');
                $eng = $content->translations->keyBy('lang_id')->get('2');
                if ($ru && $ru->meta_title) {
                    \App\Models\Translate\Cities::where('city_id', $city->id)
                        ->where('lang_id', 1)
                        ->update([ 'meta_title' => $ru->meta_title, 'meta_description' => $ru->meta_description, 'meta_keywords' => $ru->meta_keywords ]);
                } else {

                }
                if ($eng && $eng->meta_title) {
                    \App\Models\Translate\Cities::where('city_id', $city->id)
                        ->where('lang_id', 2)
                        ->update([ 'meta_title' => $eng->meta_title, 'meta_description' => $eng->meta_description, 'meta_keywords' => $eng->meta_keywords ]);
                } else {

                }
            }
        }
    }
}
