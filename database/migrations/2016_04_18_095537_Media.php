<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Media
 * php artisan migrate --path=database/migrations
 */
class Media extends Migration
{
    public function up()
    {
        Schema::create('media_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('preview_url');
            $table->string('review_url');
            $table->string('type');
            $table->longText('file_info');
            $table->boolean('visible')->default(0);
            $table->timestamps();
        });

        Schema::create('media_files_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('media_file_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->string('title');
            $table->longText('description');
            $table->timestamps();

            $table->foreign('media_file_id')
                ->references('id')
                ->on('media_files')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('media_files_translate');
        Schema::drop('media_files');
    }
}
