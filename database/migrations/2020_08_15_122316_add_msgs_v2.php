<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMsgsV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $models = [
            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.link.title", 'text'=>"Filter URL", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.link.title", 'text'=>"Filter URL", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.link.title", 'text'=>"Filter URL", 'unstable'=>1
            ]),


            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.status.noindex", 'text'=>"Status noindex", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.status.noindex", 'text'=>"Status noindex", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.status.noindex", 'text'=>"Status noindex", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.date.created", 'text'=>"Date Created", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.date.created", 'text'=>"Date Created", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.date.created", 'text'=>"Date Created", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.date.updated", 'text'=>"Date Updated", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.date.updated", 'text'=>"Date Updated", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.date.updated", 'text'=>"Date Updated", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.title.part", 'text'=>"Title part", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.title.part", 'text'=>"Title part", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.title.part", 'text'=>"Title part", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.keywords.part", 'text'=>"Keywords part", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.keywords.part", 'text'=>"Keywords part", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.keywords.part", 'text'=>"Keywords part", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.description.part", 'text'=>"Description part", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.description.part", 'text'=>"Description part", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.description.part", 'text'=>"Description part", 'unstable'=>1
            ]),
        ];

        foreach ($models as $model) {
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
