<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Content
 * php artisan migrate --path=database/migrations
 */
class Content extends Migration
{
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('type')->default('page');
            $table->smallInteger('visible')->default(0);
            $table->timestamps();
        });

        Schema::create('contents_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->string('title');
            $table->string('info')->nullable();
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');
            $table->longText('text');
            $table->timestamps();

            $table->foreign('content_id')
                ->references('id')
                ->on('contents')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });


        Schema::create('contents_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->string('preview_url');
            $table->string('review_url');
            $table->boolean('default');
            $table->integer('order');
            $table->timestamps();

            $table->foreign('content_id')
                ->references('id')
                ->on('contents')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('contents_images');
        Schema::drop('contents_translate');
        Schema::drop('contents');
    }
}
