<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Countries extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('visible')->default(0);
            $table->integer('priority');
            $table->string('alias');
            $table->timestamps();
        });

        Schema::create('countries_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('countries_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('countries_id')
                ->references('id')
                ->on('countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries_translate');
        Schema::drop('countries');
    }
}
