<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Users
 * php artisan migrate --path=database/migrations
 */
class Users extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('password', 60);
            $table->boolean('access_cpanel');
            $table->string('confirmation_code')->nullable();
            $table->smallInteger('status')->default(0);
            $table->rememberToken();
            $table->timestamp('visit_at')->nullable();
            $table->timestamps();
        });

        Schema::create('users_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('avatar_url')->nullable();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('users_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('application')->nullable();
            $table->string('module')->nullable();
            $table->string('controller')->nullable();
            $table->string('action')->nullable();
            $table->longText('data')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('hash')->nullable();
            $table->string('ip')->nullable();
            $table->timestamp('created_at');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::drop('users_actions');
        Schema::drop('users_profile');
        Schema::drop('password_resets');
        Schema::drop('users');
    }
}
