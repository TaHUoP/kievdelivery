<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToSeoSpecificationsTranslateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seo_specifications_translate', function (Blueprint $table) {
            $table->string('title')->nullable()->after('lang_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seo_specifications_translate', function (Blueprint $table) {
            $table->dropColumn('title');
        });
    }
}
