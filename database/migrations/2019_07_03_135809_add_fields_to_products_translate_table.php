<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProductsTranslateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_products_translate', function (Blueprint $table) {
            $table->longText('delivery_policy');
            $table->longText('guarantees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_products_translate', function (Blueprint $table) {
            $table->dropColumn(['delivery_policy', 'guarantees']);
        });
    }
}
