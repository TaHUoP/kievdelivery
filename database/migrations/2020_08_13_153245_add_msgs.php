<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMsgs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $models = [
            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.create", 'text'=>"Landing Create", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.create", 'text'=>"Landing Create", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.create", 'text'=>"Landing Create", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.control", 'text'=>"Landing Control", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.control", 'text'=>"Landing Control", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.control", 'text'=>"Landing Control", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.all", 'text'=>"All Landing", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.all", 'text'=>"All Landing", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.all", 'text'=>"All Landing", 'unstable'=>1
            ]),

            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.edit", 'text'=>"Landing Edit", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.edit", 'text'=>"Landing Edit", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "landings.edit", 'text'=>"Landing Edit", 'unstable'=>1
            ]),
        ];

        foreach ($models as $model) {
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
