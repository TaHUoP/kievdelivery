<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->string('email')->after('name');
            $table->text('admin_answer')->nullable()->after('email');
            $table->string('city_delivery')->after('rating')->nullable();
        });

        foreach (\App\Models\Review::has('user')->get() as $review) {
            $review->update([
                'name' => $review->user->name . ' ' . $review->user->surname,
                'email' => $review->user->email
            ]);
        }

        Schema::table('reviews', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->unsigned();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->dropColumn(['name', 'email', 'city_delivery', 'parent_review_id']);
        });
    }
}
