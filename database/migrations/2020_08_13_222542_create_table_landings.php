<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLandings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_seo_landings', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string("hash", 150)->nullable(false)->unique();
            $table->string("url")->nullable(false);
            $table->enum('no_index', [
                \App\Modules\LandingModule\Models\LandingModel::ENUM_ON,
                \App\Modules\LandingModule\Models\LandingModel::ENUM_OFF,
            ])->default(\App\Modules\LandingModule\Models\LandingModel::ENUM_OFF);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_seo_landings');
    }
}
