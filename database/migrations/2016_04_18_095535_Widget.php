<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Widget
 * php artisan migrate --path=database/migrations
 */
class Widget extends Migration
{
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->integer('lang_id')->unsigned();
            $table->smallInteger('visible')->default(0);
            $table->string('description');
            $table->longText('value');
            $table->unique(['key', 'lang_id']);
            $table->timestamps();

            $table->foreign('lang_id')
                ->references('id')->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('widgets');
    }
}
