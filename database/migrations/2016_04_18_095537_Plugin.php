<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Plugin
 * php artisan migrate --path=database/migrations
 */
class Plugin extends Migration
{
    public function up()
    {
        Schema::create('plugins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('type');
            $table->boolean('visible')->default(0);
            $table->string('description');
            $table->longText('value')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['key']);
        });
    }

    public function down()
    {
        Schema::drop('plugins');
    }
}
