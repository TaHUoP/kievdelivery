<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLandingsTranslate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_seo_landings_translate', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer("landing_id")->unsigned();
            $table->integer("lang_id")->unsigned();
            $table->text("title");
            $table->text("keywords");
            $table->text("description");
            $table->text("page_description");
            $table->timestamps();

            $table->foreign('landing_id')->references('id')->on('shop_seo_landings');
            $table->foreign('lang_id')->references('id')->on('translator_languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_seo_landings_translate');
    }
}
