<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Languages
 * php artisan migrate --path=database/migrations
 */
class Languages extends Migration
{
    public function up()
    {
        Schema::create('translator_languages', function ($table) {
            $table->increments('id');
            $table->string('locale', 10)->unique();
            $table->string('name');
            $table->smallInteger('default')->default(0);
            $table->smallInteger('active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('translator_translations', function ($table) {
            $table->increments('id');
            $table->string('locale', 10);
            $table->string('namespace', 150)->default('*');
            $table->string('group', 150);
            $table->string('item', 150);
            $table->text('text');
            $table->boolean('unstable')->default(false);
            $table->boolean('locked')->default(false);
            $table->timestamps();
            $table->foreign('locale')->references('locale')->on('translator_languages');
            $table->unique(['locale', 'namespace', 'group', 'item']);
        });
    }

    public
    function down()
    {
        Schema::drop('translator_languages');
        Schema::drop('translator_translations');
    }
}
