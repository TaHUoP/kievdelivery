<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Kalnoy\Nestedset\NestedSet;

/**
 * Shop
 * php artisan migrate --path=database/migrations
 */
class Shop extends Migration
{
    public function up()
    {
        // Категория
        Schema::create('shop_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique();
            $table->integer('order');
            $table->tinyInteger('rating');
            $table->boolean('recommended')->default(0);
            $table->boolean('visible')->default(0);
            $table->timestamps();
            NestedSet::columns($table);

            $table->foreign('parent_id')
                ->references('id')
                ->on('shop_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('shop_categories_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lang_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');
            $table->longText('description_short');
            $table->longText('description_full');
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('shop_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')
                ->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Товар
        Schema::create('shop_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendor_code'); // артикул
            $table->string('alias')->unique();
            $table->integer('amount')->default(0);
            $table->boolean('visible')->default(0);
            $table->boolean('main')->default(0);
            $table->boolean('pre_order')->default(0);
            $table->decimal('price', 14, 4)->default(0);
            $table->decimal('price_old', 14, 4)->default(0);
            $table->tinyInteger('rating')->default(0);
            $table->timestamps();
        });

        Schema::create('shop_products_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lang_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('name');
            $table->longText('description_short');
            $table->longText('description_full');
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')
                ->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Файлы к товару
        Schema::create('shop_product_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('url');
            $table->string('name');

            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Изображения к товару
        Schema::create('shop_product_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('preview_url');
            $table->string('review_url');
            $table->boolean('visible');
            $table->boolean('default');
            $table->integer('order');
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('shop_product_images_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->string('title');
            $table->string('alt');
            $table->timestamps();

            $table->foreign('image_id')
                ->references('id')
                ->on('shop_product_images')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')
                ->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Категории товара
        Schema::create('shop_product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('shop_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Свойства
        Schema::create('shop_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique();
            $table->string('type');
            $table->boolean('is_filter')->default(0);
            $table->timestamps();
        });

        Schema::create('shop_properties_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lang_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('shop_properties')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')
                ->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Значения
        Schema::create('shop_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique();
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('shop_values_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('value_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->timestamps();

            $table->foreign('value_id')
                ->references('id')
                ->on('shop_values')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')
                ->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });


        # -------------------------------

        // Корзина
        Schema::create('shop_cart', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash')->unique();
            $table->integer('user_id')->unsigned()->nullable();
            $table->boolean('processed')->default(0);
            $table->decimal('total_price', 14, 4);
            $table->decimal('discount', 14, 4);
            $table->string('discount_code')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Товары в корзине
        Schema::create('shop_cart_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('amount')->default(0);
            $table->unique(['cart_id', 'product_id']);

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('cart_id')
                ->references('id')
                ->on('shop_cart')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Оформление заказа
        Schema::create('shop_checkout', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned()->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('address');
            $table->string('phone');
            $table->string('country');
            $table->integer('status')->default(0);
            $table->boolean('paid')->default(0);
            $table->string('payment_system');
            $table->longText('payment_data')->nullable();
            $table->timestamps();

            $table->foreign('cart_id')
                ->references('id')
                ->on('shop_cart')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Доставка
        Schema::create('shop_checkout_delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('checkout_id')->unsigned()->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address');
            $table->decimal('delivery_price', 14, 4);
            $table->string('phone');
            $table->string('country');
            $table->string('city');
            $table->date('date');
            $table->text('comment_recipient')->nullable(); // Комментарий для получателя
            $table->text('comment_note')->nullable(); // Комментарий - заметка
            $table->boolean('shipment')->default(0);
            $table->timestamps();

            $table->foreign('checkout_id')
                ->references('id')
                ->on('shop_checkout')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Связь: Товар - Свойство - Значение
        Schema::create('shop_products_properties_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->integer('value_id')->nullable()->unsigned();
            $table->text('custom_value');
            $table->string('custom_value_type')->default('integer');
            $table->integer('order');
            $table->timestamps();
            $table->unique(['product_id', 'property_id']);

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('property_id')
                ->references('id')
                ->on('shop_properties')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('value_id')
                ->references('id')
                ->on('shop_values')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Связь: Свойство - Значение
        Schema::create('shop_properties_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();
            $table->integer('value_id')->unsigned();
            $table->integer('order');
            $table->timestamps();

            $table->foreign('property_id')
                ->references('id')
                ->on('shop_properties')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('value_id')
                ->references('id')
                ->on('shop_values')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        # -------------------------------

        // Связь: Категория - свойство
        Schema::create('shop_categories_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->integer('order');
            $table->timestamps();

            $table->unique(['category_id', 'property_id']);

            $table->foreign('category_id')
                ->references('id')
                ->on('shop_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('property_id')
                ->references('id')
                ->on('shop_properties')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('shop_categories_translate');
        Schema::drop('shop_products_translate');
        Schema::drop('shop_properties_translate');
        Schema::drop('shop_values_translate');

        Schema::drop('shop_categories_properties');
        Schema::drop('shop_products_properties_values');
        Schema::drop('shop_properties_values');

        Schema::drop('shop_categories');
        Schema::drop('shop_products');
        Schema::drop('shop_properties');
        Schema::drop('shop_values');

        Schema::drop('shop_checkout_delivery');
        Schema::drop('shop_checkout');
        Schema::drop('shop_cart_products');
        Schema::drop('shop_cart');
    }
}
