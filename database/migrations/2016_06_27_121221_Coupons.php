<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Shop
 * php artisan migrate --path=database/migrations
 */
class Coupons extends Migration
{
    public function up()
    {
        Schema::create('shop_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->integer('quantity');
            $table->integer('percentage');
            $table->integer('ttl');
            $table->dateTime('date_expiry');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('shop_coupons');
    }
}
