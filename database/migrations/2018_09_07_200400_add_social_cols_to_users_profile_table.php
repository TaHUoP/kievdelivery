<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialColsToUsersProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_profile', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable()->change();
            $table->string('provider')->nullable();
            $table->text('token')->nullable();
            $table->string('email')->nullable();
            $table->string('profile_id')->nullable();
            $table->string('profile_url', 256)->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('password', 60)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_profile', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->dropColumn('provider');
            $table->dropColumn('profile_id');
            $table->dropColumn('token');
            $table->dropColumn('email');
            $table->dropColumn('profile_url');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('password', 60)->change();
        });
    }
}
