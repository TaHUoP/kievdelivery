<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Seo
 * php artisan migrate --path=database/migrations
 */
class Seo extends Migration
{
    public function up()
    {
        Schema::create('seo_specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->longText('specify_head')->nullable();
            $table->longText('specify_footer')->nullable();
            $table->timestamps();
        });

        Schema::create('seo_specifications_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seo_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->string('meta_title')->nullable();;
            $table->string('meta_description')->nullable();;
            $table->string('meta_keywords')->nullable();;
            $table->longText('text')->nullable();;
            $table->timestamps();

            $table->foreign('seo_id')
                ->references('id')
                ->on('seo_specifications')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('lang_id')
                ->references('id')->on('translator_languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('seo_specifications_translate');
        Schema::drop('seo_specifications');
    }
}
