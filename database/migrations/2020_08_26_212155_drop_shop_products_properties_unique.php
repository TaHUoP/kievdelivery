<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropShopProductsPropertiesUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_products_properties_values', function (Blueprint $table) {
            $table->dropForeign("shop_products_properties_values_product_id_foreign");
            $table->dropForeign("shop_products_properties_values_property_id_foreign");
            $table->dropForeign("shop_products_properties_values_value_id_foreign");
            $table->dropUnique("shop_products_properties_values_product_id_property_id_unique");

            $table->foreign('product_id')
                ->references('id')
                ->on('shop_products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('property_id')
                ->references('id')
                ->on('shop_properties')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('value_id')
                ->references('id')
                ->on('shop_values')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
