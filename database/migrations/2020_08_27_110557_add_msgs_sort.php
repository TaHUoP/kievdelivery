<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMsgsSort extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $models = [
            new \Waavi\Translation\Models\Translation([
                'locale' => "ru", 'namespace' => '*', 'group' => 'shop', 'item' => "attr.value.sort", 'text'=>"Sort", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "en", 'namespace' => '*', 'group' => 'shop', 'item' => "attr.value.sort", 'text'=>"Sort", 'unstable'=>1
            ]),
            new \Waavi\Translation\Models\Translation([
                'locale' => "uk", 'namespace' => '*', 'group' => 'shop', 'item' => "attr.value.sort", 'text'=>"Sort", 'unstable'=>1
            ])
        ];

        foreach ($models as $model) {
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
